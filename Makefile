files = src/test/*.js src/modules/*/test/*.js src/modules/*/*.js
srcfiles = src/*.js src/modules/*/*.js
test:
	@node node_modules/eslint/bin/eslint $(srcfiles) $(files)
	@node node_modules/lab/bin/lab -t 100 -v -l $(files)
test-html:
	@node node_modules/lab/bin/lab -t 100 -r html -o coverage.html $(files)
test-bamboo-junit:
	@node node_modules/lab/bin/lab -t 100 -r junit -o test-reports/test-results.xml $(files)
	@node node_modules/eslint/bin/eslint -o test-reports/linter.txt src/**
test-bamboo-coverage:
	@node node_modules/lab/bin/lab -t 100 -r html -o coverage.html $(files)
test-bamboo-json:
	@node node_modules/lab/bin/lab -t 100 -r json -o test-reports/test-results.json $(files)
api-doc:
	@node node_modules/apidoc/bin/apidoc -i src/
test-init:
	@sh src/scripts/reset_test.sh
test-env:
	@node node_modules/lab/bin/lab -v -l src/test/env.js

.PHONY: test test-html test-cov test-cov-html test-bamboo-junit test-bamboo-coverage test-init test-env