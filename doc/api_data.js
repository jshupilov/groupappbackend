define({ "api": [
  {
    "type": "get",
    "url": "-",
    "title": "h. Collections",
    "name": "basicsCollections",
    "version": "1.7.4",
    "group": "1_Basics",
    "description": "<p>Some API services need to return multiple items in one response, like list of hotels or categories. A list of same type items is represented as a <code>Collection</code> in this API. A <code>Collection</code> has a predefined high level structure, which is described above.</p> <p>If interacting with a API endpoint, which returns a collection, then there are also some query parameters available for controlling the output, these parameters are described below.</p> ",
    "parameter": {
      "examples": [
        {
          "title": "Collection sample:",
          "content": " {\n  \"href\": \"http://.......\",\n  \"offset\": 2,\n  \"limit\": 10,\n  \"total\": 21,\n  \"next\": \"http:.........\" ,\n  \"prev\": \"http:.........\" ,\n  \"items\": [\n    {....item 1},\n    {....item 2}\n  ],\n  \"parent\": {....parent item}\n }",
          "type": "json"
        },
        {
          "title": "Request url sample:",
          "content": " http://...../categories?limit=10&offset=20\n ==> This will return 10 items starting from item 21",
          "type": "html"
        }
      ],
      "fields": {
        "Collection structure": [
          {
            "group": "Collection structure",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>A URL, which points to a service, which returns this collection</p> "
          },
          {
            "group": "Collection structure",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>An offset for pagination, if set, then response will skip the first <code>N</code> values.</p> "
          },
          {
            "group": "Collection structure",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>A limit for maximum number of items returned in one response</p> "
          },
          {
            "group": "Collection structure",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": "<p>Total number of items in this collection, without limit and offset</p> "
          },
          {
            "group": "Collection structure",
            "type": "Number",
            "optional": true,
            "field": "next",
            "description": "<p>Href to next page in this collection - if next page exists</p> "
          },
          {
            "group": "Collection structure",
            "type": "Number",
            "optional": true,
            "field": "prev",
            "description": "<p>Href to previous page in this collection - if previous page exists</p> "
          },
          {
            "group": "Collection structure",
            "type": "Object[]",
            "optional": false,
            "field": "items",
            "description": "<p>An array of items, which are included in this collection. The contents of this array is controlled by <code>offset</code> and <code>limit</code> query parameters</p> "
          },
          {
            "group": "Collection structure",
            "type": "Object",
            "optional": true,
            "field": "parent",
            "description": "<p>An optional parent item for items in the collection</p> "
          }
        ],
        "Collection query parameters": [
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>A limit for maximum number of items returned in one response</p> "
          },
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>An offset for pagination, if set, then response will skip the first <code>N</code> values.</p> "
          }
        ]
      }
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "-",
    "title": "8. Collections",
    "name": "basicsCollections",
    "version": "0.0.1",
    "group": "1_Basics",
    "description": "<p>Some API services need to return multiple items in one response, like list of hotels or categories. A list of same type items is represented as a <code>Collection</code> in this API. A <code>Collection</code> has a predefined high level structure, which is described above.</p> <p>If interacting with a API endpoint, which returns a collection, then there are also some query parameters available for controlling the output, these parameters are described below.</p> ",
    "parameter": {
      "examples": [
        {
          "title": "Collection sample:",
          "content": " {\n  \"href\": \"http://.......\",\n  \"offset\": 2,\n  \"limit\": 10,\n  \"total\": 21,\n  \"next\": \"http:.........\" ,\n  \"prev\": \"http:.........\" ,\n  \"items\": [\n    {....item 1},\n    {....item 2}\n  ]\n }",
          "type": "json"
        },
        {
          "title": "Request url sample:",
          "content": " http://...../categories?limit=10&offset=20\n ==> This will return 10 items starting from item 21",
          "type": "html"
        }
      ],
      "fields": {
        "Collection structure": [
          {
            "group": "Collection structure",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>A URL, which points to a service, which returns this collection</p> "
          },
          {
            "group": "Collection structure",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>An offset for pagination, if set, then response will skip the first <code>N</code> values.</p> "
          },
          {
            "group": "Collection structure",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>A limit for maximum number of items returned in one response</p> "
          },
          {
            "group": "Collection structure",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": "<p>Total number of items in this collection, without limit and offset</p> "
          },
          {
            "group": "Collection structure",
            "type": "Number",
            "optional": true,
            "field": "next",
            "description": "<p>Href to next page in this collection - if next page exists</p> "
          },
          {
            "group": "Collection structure",
            "type": "Number",
            "optional": true,
            "field": "prev",
            "description": "<p>Href to previous page in this collection - if previous page exists</p> "
          },
          {
            "group": "Collection structure",
            "type": "Object[]",
            "optional": false,
            "field": "items",
            "description": "<p>An array of items, which are included in this collection. The contents of this array is controlled by <code>offset</code> and <code>limit</code> query parameters</p> "
          }
        ],
        "Collection query parameters": [
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>A limit for maximum number of items returned in one response</p> "
          },
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>An offset for pagination, if set, then response will skip the first <code>N</code> values.</p> "
          }
        ]
      }
    },
    "filename": "src/modules/common/_apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "-",
    "title": "j. Errors",
    "name": "basicsErrors",
    "version": "1.7.4",
    "group": "1_Basics",
    "description": "<p>When an error occurs, the API returns a error message with a JSON body</p> ",
    "parameter": {
      "examples": [
        {
          "title": "Example error object",
          "content": "{\n    \"module\": \"App\",\n    \"code\": \"app-inv-route\",\n    \"message\": \"Invalid path\",\n    \"developerMessage\": \"This path does not exist\",\n    \"moreInfo\": \"https://localhost:8000/doc/#errors-app-inv-route\",\n    \"statusCode\": 400,\n    \"isCustomError\": true,\n    \"isRetriable\": true,\n    \"debuggingData\": {\n        \"requestedPath\": \"/v1/test-hgid/app/inits\"\n    },\n    \"localized\": {\n        \"en_GB\": {\n            \"userMessage\": \"Invalid path\"\n        }\n    }\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Error object structure": [
          {
            "group": "Error object structure",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": ""
          },
          {
            "group": "Error object structure",
            "type": "String",
            "optional": false,
            "field": "error.module",
            "description": "<p>Which module gave the error</p> "
          },
          {
            "group": "Error object structure",
            "type": "String",
            "optional": false,
            "field": "error.message",
            "description": "<p>Error message for the end-user (untranslated)</p> "
          },
          {
            "group": "Error object structure",
            "type": "String",
            "optional": false,
            "field": "error.developerMessage",
            "description": "<p>Error message for the developer</p> "
          },
          {
            "group": "Error object structure",
            "type": "String",
            "optional": false,
            "field": "error.moreInfo",
            "description": "<p>Link to a page with more information about this error</p> "
          },
          {
            "group": "Error object structure",
            "type": "Number",
            "optional": false,
            "field": "error.statusCode",
            "defaultValue": "500",
            "description": "<p>The http status code, which was given</p> "
          },
          {
            "group": "Error object structure",
            "type": "Boolean",
            "optional": false,
            "field": "error.isCustomError",
            "defaultValue": "true",
            "description": "<p>Boolean indicating, that this error was handled by the system</p> "
          },
          {
            "group": "Error object structure",
            "type": "Boolean",
            "optional": false,
            "field": "error.isRetriable",
            "defaultValue": "false",
            "description": "<p>Boolean indicating, that this error is retriable or not</p> "
          },
          {
            "group": "Error object structure",
            "type": "Object",
            "optional": false,
            "field": "error.debuggingData",
            "description": "<p>Object, which has some information, which can help debug and fix the cause of this error</p> "
          },
          {
            "group": "Error object structure",
            "type": "Object",
            "optional": false,
            "field": "error.localized",
            "description": "<p>Localized information about this error</p> "
          },
          {
            "group": "Error object structure",
            "type": "Object",
            "optional": false,
            "field": "error.localized.LANG",
            "description": "<p>Information in given <code>LANG</code></p> "
          },
          {
            "group": "Error object structure",
            "type": "Object",
            "optional": false,
            "field": "error.localized.LANG.userMessage",
            "description": "<p>Translated message, to show to the end-user</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "app-inv-route",
            "description": "<p><span id=\"error_app-inv-route\"></span> This path does not exist</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "com-fields-type",
            "description": "<p><span id=\"error_com-fields-type\"></span> Some fields did not match expected type</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-query-error",
            "description": "<p><span id=\"error_cat-query-error\"></span> Error occurred querying categories</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-cnt-query-error",
            "description": "<p><span id=\"error_cat-cnt-query-error\"></span> Error occurred querying hotel count in categories</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-inv-type",
            "description": "<p><span id=\"error_cat-inv-type\"></span> Invalid category type provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ai-not-found",
            "description": "<p><span id=\"error_ai-not-found\"></span> Invalid hotel group id or uuid provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p><span id=\"error_hg-not-found\"></span> Invalid hotel group id provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-err-save",
            "description": "<p><span id=\"error_hg-err-save\"></span> Error occurred while saving hotel group to database</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-err-del",
            "description": "<p><span id=\"error_hg-err-del\"></span> Error occurred while deleting hotel group from database</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-err-format",
            "description": "<p><span id=\"error_hg-err-format\"></span> Error occurred while formatting data</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-err-up",
            "description": "<p><span id=\"error_hg-err-up\"></span> Error occurred while updating hotel group</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-hotel-nf",
            "description": "<p><span id=\"error_hs-hotel-nf\"></span> Hotel, with given parameters was not found</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-save",
            "description": "<p><span id=\"error_hs-err-save\"></span> Error occurred while saving hotel to database</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-src-err",
            "description": "<p><span id=\"error_hs-src-err\"></span> Error searching hotels</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-inv-loc-inp",
            "description": "<p><span id=\"error_hs-inv-loc-inp\"></span> Invalid location search input</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-del",
            "description": "<p><span id=\"error_hs-err-del\"></span> Error deleting item(s) from search index</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-for",
            "description": "<p><span id=\"error_hs-err-for\"></span> Error formatting hotel</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-ip",
            "description": "<p><span id=\"error_hs-err-ip\"></span> Invalid request ip address</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-up-img",
            "description": "<p><span id=\"error_iu-err-up-img\"></span> Error occurred while uploading the image to CDN</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-no-img",
            "description": "<p><span id=\"error_iu-err-no-img\"></span> The image was not found in given path</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-inv-img-obj",
            "description": "<p><span id=\"error_iu-inv-img-obj\"></span> Invalid image object passed fot parsing</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-del-imgs",
            "description": "<p><span id=\"error_iu-err-del-imgs\"></span> Error occurred, trying to delete images</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-inv-mc",
            "description": "<p><span id=\"error_iu-err-inv-mc\"></span> Invalid media configuration given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-inv-tr",
            "description": "<p><span id=\"error_iu-err-inv-tr\"></span> Invalid transformation given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-inv-tr-tp",
            "description": "<p><span id=\"error_iu-err-inv-tr-tp\"></span> Invalid transformation type given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-account-add",
            "description": "<p><span id=\"error_ps-account-add\"></span> Account, with given parameters can not be added</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-account-auth",
            "description": "<p><span id=\"error_ps-account-auth\"></span> Account, with given parameters can not be authenticated</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-app-get",
            "description": "<p><span id=\"error_ps-app-get\"></span> Application, with given parameters can not be found</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-acc-get",
            "description": "<p><span id=\"error_ps-acc-get\"></span> Account, with given parameters can not be found</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-session-add",
            "description": "<p><span id=\"error_ps-session-add\"></span> Application, with given parameters can not be found</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-session-out",
            "description": "<p><span id=\"error_ps-session-out\"></span> Application can not update sessionEnd</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-session-get",
            "description": "<p><span id=\"error_ps-session-get\"></span> Application can not get last session details for this profile</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-session-inv",
            "description": "<p><span id=\"error_ps-session-inv\"></span> You have invalid session details</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-tenant-verify",
            "description": "<p><span id=\"error_ps-tenant-verify\"></span> Application can not verify user account</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-token-add",
            "description": "<p><span id=\"error_ps-token-add\"></span> Application can not add to user account custom data</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-verify-inv",
            "description": "<p><span id=\"error_ps-verify-inv\"></span> Verification key is invalid</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-verify-failed",
            "description": "<p><span id=\"error_ps-verify-failed\"></span> This page does not exist</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-resend-failed",
            "description": "<p><span id=\"error_ps-resend-failed\"></span> Email verification resending failed.</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-resend-inv",
            "description": "<p><span id=\"error_ps-resend-inv\"></span> Email verification resending not valid. Check PayLoad details. Login information is necessary</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-sav-ix",
            "description": "<p><span id=\"error_sr-err-sav-ix\"></span> Error saving to search index</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-lis-ix",
            "description": "<p><span id=\"error_sr-err-lis-ix\"></span> Error listing search indexes</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-src",
            "description": "<p><span id=\"error_sr-err-src\"></span> Error searching</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-new",
            "description": "<p><span id=\"error_sr-err-new\"></span> Error creating search index</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-del",
            "description": "<p><span id=\"error_sr-err-del\"></span> Error deleting search index</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-itm-err-del",
            "description": "<p><span id=\"error_sr-itm-err-del\"></span> Error deleting item from search index</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-hgid",
            "description": "<p><span id=\"error_au-inv-hgid\"></span> Invalid HotelGroup Id provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-ver",
            "description": "<p><span id=\"error_au-inv-ver\"></span> Invalid API version given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-no-uuid",
            "description": "<p><span id=\"error_au-no-uuid\"></span> No UUID given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-uuid",
            "description": "<p><span id=\"error_au-inv-uuid\"></span> Invalid UUID given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-path",
            "description": "<p><span id=\"error_au-inv-path\"></span> Invalid api call URL</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-hrefqp",
            "description": "<p><span id=\"error_au-inv-hrefqp\"></span> Invalid queryParams provided, must be an object</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-linkmethod",
            "description": "<p><span id=\"error_au-inv-linkmethod\"></span> Invalid method provided for link, must be on of GET/PUT/POST/DELETE</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-req-method",
            "description": "<p><span id=\"error_au-inv-req-method\"></span> Invalid request method</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-loc-inp",
            "description": "<p><span id=\"error_au-inv-loc-inp\"></span> Invalid location search input</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-req-bd",
            "description": "<p><span id=\"error_au-inv-req-bd\"></span> Invalid payload</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-req-ct",
            "description": "<p><span id=\"error_au-inv-req-ct\"></span> Invalid request content type</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-https-req",
            "description": "<p><span id=\"error_au-https-req\"></span> Use of https protocol is required</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-ver-req",
            "description": "<p><span id=\"error_au-ver-req\"></span> Use of x-cardola-verify header is required</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-ver-inv",
            "description": "<p><span id=\"error_au-ver-inv\"></span> Invalid x-cardola-verify header value</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-verifykey",
            "description": "<p><span id=\"error_au-inv-verifykey\"></span> Invalid Verify Key provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-len-verifykey",
            "description": "<p><span id=\"error_au-len-verifykey\"></span> Invalid Verify Key length. Length must be more then 10 symbols</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-ol2pi-hl",
            "description": "<p><span id=\"error_sr-err-ol2pi-hl\"></span> Error converting offset and limit to pages, it took too many cycles</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p><span id=\"error_db-cc-db\"></span> No database connection</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p><span id=\"error_db-query-fails\"></span> Database query failed</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-no-langcode",
            "description": "<p><span id=\"error_lu-no-langcode\"></span> No language code provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-inv-langcode",
            "description": "<p><span id=\"error_lu-inv-langcode\"></span> Invalid language code provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-no-countrycode",
            "description": "<p><span id=\"error_lu-no-countrycode\"></span> No country code provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-inv-countrycode",
            "description": "<p><span id=\"error_lu-inv-countrycode\"></span> Invalid country code provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-no-trans",
            "description": "<p><span id=\"error_lu-no-trans\"></span> No translation found</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "system-error",
            "description": "<p><span id=\"error_system-error\"></span> There is a problem with the code</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-transf-merge-fail",
            "description": "<p><span id=\"error_lu-transf-merge-fail\"></span> Failed to merge translations with defaults</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-vdt-inv-val",
            "description": "<p><span id=\"error_lu-vdt-inv-val\"></span> Invalid translation object value</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-vdt-key-miss",
            "description": "<p><span id=\"error_lu-vdt-key-miss\"></span> Key missing from translation object</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-vdt-inv-lang",
            "description": "<p><span id=\"error_lu-vdt-inv-lang\"></span> Invalid language in translations</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-vdt-inv-tpl",
            "description": "<p><span id=\"error_lu-vdt-inv-tpl\"></span> Invalid translation value template</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "mv-inv-val-type",
            "description": "<p><span id=\"error_mv-inv-val-type\"></span> Invalid validation type given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "mv-inv-type",
            "description": "<p><span id=\"error_mv-inv-type\"></span> Invalid data type given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "mv-inv-val",
            "description": "<p><span id=\"error_mv-inv-val\"></span> Invalid value was given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "mv-key-miss",
            "description": "<p><span id=\"error_mv-key-miss\"></span> Required key is missing</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "mv-src-mis-prefix",
            "description": "<p><span id=\"error_mv-src-mis-prefix\"></span> The image public_id should start with a certain prefix</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cv-inv-type",
            "description": "<p><span id=\"error_cv-inv-type\"></span> Category has invalid data type</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cv-inv-val",
            "description": "<p><span id=\"error_cv-inv-val\"></span> Category has invalid data type</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cv-inv-val-type",
            "description": "<p><span id=\"error_cv-inv-val-type\"></span> Invalid validation type given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cv-key-miss",
            "description": "<p><span id=\"error_cv-key-miss\"></span> Missing a required key in category</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cv-inv-lang",
            "description": "<p><span id=\"error_cv-inv-lang\"></span> Invalid language provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cv-nam-empty",
            "description": "<p><span id=\"error_cv-nam-empty\"></span> Category name cannot be empty string</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-dbl-catid",
            "description": "<p><span id=\"error_hgv-dbl-catid\"></span> Two categories have the same categoryId</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cv-cat-no-defl",
            "description": "<p><span id=\"error_cv-cat-no-defl\"></span> Category is missing translations in default language</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "msv-inv-val",
            "description": "<p><span id=\"error_msv-inv-val\"></span> Invalid email template key value</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "msv-key-miss",
            "description": "<p><span id=\"error_msv-key-miss\"></span> Required email template key is missing</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "msv-inv-tpl",
            "description": "<p><span id=\"error_msv-inv-tpl\"></span> Invalid email template given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "msv-inv-type",
            "description": "<p><span id=\"error_msv-inv-type\"></span> Invalid email template key type</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "msv-non-ex-val",
            "description": "<p><span id=\"error_msv-non-ex-val\"></span> This value not exists</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "msv-inv-frm-email",
            "description": "<p><span id=\"error_msv-inv-frm-email\"></span> Invalid sender email address given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "msv-inv-to-email",
            "description": "<p><span id=\"error_msv-inv-to-email\"></span> Invalid email address given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "msv-inv-lang-code",
            "description": "<p><span id=\"error_msv-inv-lang-code\"></span> Invalid language code provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "msv-tpld-inv-val",
            "description": "<p><span id=\"error_msv-tpld-inv-val\"></span> Email template data has invalid information</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "msv-tpld-mis-val",
            "description": "<p><span id=\"error_msv-tpld-mis-val\"></span> Email template is missing some inpu data</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-key-miss",
            "description": "<p><span id=\"error_hgv-key-miss\"></span> A key is missing from the data set</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-inv-type",
            "description": "<p><span id=\"error_hgv-inv-type\"></span> A value has a wrong data type</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-inv-val",
            "description": "<p><span id=\"error_hgv-inv-val\"></span> A value has an invalid value</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-inv-val-type",
            "description": "<p><span id=\"error_hgv-inv-val-type\"></span> Invalid validation type provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-inv-qty",
            "description": "<p><span id=\"error_hgv-inv-qty\"></span> A key items count is invalid</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-hgId-exists",
            "description": "<p><span id=\"error_hgv-hgId-exists\"></span> Hotel group with this id already exists</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-inv-def-lan",
            "description": "<p><span id=\"error_hgv-inv-def-lan\"></span> Invalid default language</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-inv-trans-lang",
            "description": "<p><span id=\"error_hgv-inv-trans-lang\"></span> Invalid language code for translations given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-trans-no-def",
            "description": "<p><span id=\"error_hgv-trans-no-def\"></span> No translations given for default language</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-trans-nonstr",
            "description": "<p><span id=\"error_hgv-trans-nonstr\"></span> Translation must be provided as string</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-trans-empty",
            "description": "<p><span id=\"error_hgv-trans-empty\"></span> Translations cannot be empty strings</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-men-nav-inv",
            "description": "<p><span id=\"error_hgv-men-nav-inv\"></span> The menu item action.navigation structure is incorrect</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-men-inv-cat",
            "description": "<p><span id=\"error_hgv-men-inv-cat\"></span> The menu item is pointing to nonexistent category</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-men-too-deep",
            "description": "<p><span id=\"error_hgv-men-too-deep\"></span> The menu item has a children, which points to a parent or there are more than 100 levels of children</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-men-inv-lang",
            "description": "<p><span id=\"error_hgv-men-inv-lang\"></span> The menu item has a localized translation with an invalid language key</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-inf-miss-def-lang",
            "description": "<p><span id=\"error_hgv-inf-miss-def-lang\"></span> Hotel group information id not given in default language</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-inf-inv-lang",
            "description": "<p><span id=\"error_hgv-inf-inv-lang\"></span> Hotel group information is provided with invalid langauge key</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-inv-parcat",
            "description": "<p><span id=\"error_hgv-inv-parcat\"></span> The parent category Id does not exist</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-dbl-men-id",
            "description": "<p><span id=\"error_hgv-dbl-men-id\"></span> Two main menu items have a same menuItemId</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hgv-emt-no-def-lang",
            "description": "<p><span id=\"error_hgv-emt-no-def-lang\"></span> The email template is not provided in default langauge</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "mss-rndr-err",
            "description": "<p><span id=\"error_mss-rndr-err\"></span> Error rendering given template</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "mss-snd-inv-lang",
            "description": "<p><span id=\"error_mss-snd-inv-lang\"></span> Invalid language code given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "mss-snd-mis-lang",
            "description": "<p><span id=\"error_mss-snd-mis-lang\"></span> Email template does not support this language</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "mss-snd-err",
            "description": "<p><span id=\"error_mss-snd-err\"></span> Error sending email</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-as-add",
            "description": "<p><span id=\"error_ps-as-add\"></span> Account store, with given parameters can not be added</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-dir-get",
            "description": "<p><span id=\"error_ps-dir-get\"></span> Cannot get Stormpath directory</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-dir-add",
            "description": "<p><span id=\"error_ps-dir-add\"></span> Directory, with given parameters can not be added</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-dir-upd",
            "description": "<p><span id=\"error_ps-dir-upd\"></span> Directory, with given parameters can not be updated</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-customdata-sav",
            "description": "<p><span id=\"error_ps-customdata-sav\"></span> Application can not get information of account custom data</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-reset-failed",
            "description": "<p><span id=\"error_ps-reset-failed\"></span> Email is invalid or does not exist in db.</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-reset-inv",
            "description": "<p><span id=\"error_ps-reset-inv\"></span> Password reset is not valid. Check PayLoad details. Email is required</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-no-acc",
            "description": "<p><span id=\"error_ps-no-acc\"></span> User with this email does not exist</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-inv-pass",
            "description": "<p><span id=\"error_ps-inv-pass\"></span> Invalid password entered</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-inv-body",
            "description": "<p><span id=\"error_ps-inv-body\"></span> Request body has invalid value</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-mis-inp",
            "description": "<p><span id=\"error_ps-mis-inp\"></span> Missing input parameter</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-inv-inp",
            "description": "<p><span id=\"error_ps-inv-inp\"></span> Invalid input parameter</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-weak-pass",
            "description": "<p><span id=\"error_ps-weak-pass\"></span> Too weak password</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-inv-email",
            "description": "<p><span id=\"error_ps-inv-email\"></span> Email is invalid</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-inv-gn",
            "description": "<p><span id=\"error_ps-inv-gn\"></span> Given name is invalid</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-inv-sn",
            "description": "<p><span id=\"error_ps-inv-sn\"></span> Surname is invalid</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-ex-email",
            "description": "<p><span id=\"error_ps-ex-email\"></span> Email is not unique!</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-set-inv-inp",
            "description": "<p><span id=\"error_ps-set-inv-inp\"></span> Invalid input type</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-frmt-res",
            "description": "<p><span id=\"error_ps-frmt-res\"></span> Error occurred while formatting the response</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-resettoken-inv",
            "description": "<p><span id=\"error_ps-resettoken-inv\"></span> Error occurred with token validation</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-resetpass-inv",
            "description": "<p><span id=\"error_ps-resetpass-inv\"></span> Error occurred with inputs, missing input parameters</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-resetpassmatch-inv",
            "description": "<p><span id=\"error_ps-resetpassmatch-inv\"></span> Passwords do not match</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "com-fields-miss",
            "description": "<p><span id=\"error_com-fields-miss\"></span> Missing fields in body</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-non-str",
            "description": "<p><span id=\"error_hv-non-str\"></span> Field must be provided as string</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-non-bool",
            "description": "<p><span id=\"error_hv-non-bool\"></span> Field must be provided as boolean</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-key-miss",
            "description": "<p><span id=\"error_hv-key-miss\"></span> A key is missing from the data set</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-key-empty",
            "description": "<p><span id=\"error_hv-key-empty\"></span> Key cannot be empty</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-inv-type",
            "description": "<p><span id=\"error_hv-inv-type\"></span> A value has a wrong data type</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-inv-val",
            "description": "<p><span id=\"error_hv-inv-val\"></span> A value has an invalid value</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-inv-val-type",
            "description": "<p><span id=\"error_hv-inv-val-type\"></span> Invalid validation type provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-inv-email",
            "description": "<p><span id=\"error_hv-inv-email\"></span> Hotel email is invalid.</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-inv-url",
            "description": "<p><span id=\"error_hv-inv-url\"></span> Hotel web site url is invalid.</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-inv-lang",
            "description": "<p><span id=\"error_hv-inv-lang\"></span> Invalid language code for localization given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-nodef-lang",
            "description": "<p><span id=\"error_hv-nodef-lang\"></span> Hotel localization default language is miss</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-hId-exists",
            "description": "<p><span id=\"error_hv-hId-exists\"></span> Hotel with this id already exists</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hv-hgId-exists",
            "description": "<p><span id=\"error_hv-hgId-exists\"></span> Hotel Group with this id already exists</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "test-code",
            "description": "<p><span id=\"error_test-code\"></span> Mes2</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "je-inv-job-cd",
            "description": "<p><span id=\"error_je-inv-job-cd\"></span> Invalid job code given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-cache",
            "description": "<p><span id=\"error_au-inv-cache\"></span> Invalid cache type provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-err-etag",
            "description": "<p><span id=\"error_au-err-etag\"></span> Cannot apply etag, maybe empty reponse body?</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cu-string-inv",
            "description": "<p><span id=\"error_cu-string-inv\"></span> TypeError: Bad input string</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-publish-fail",
            "description": "<p><span id=\"error_hg-publish-fail\"></span> Publish hotelGroup failed</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-unpublish-fail",
            "description": "<p><span id=\"error_hg-unpublish-fail\"></span> Unpublish hotelGroup failed</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hd-hotel-ne",
            "description": "<p><span id=\"error_hd-hotel-ne\"></span> Hotel, with given parameters was not found</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ver-no-entity",
            "description": "<p><span id=\"error_ver-no-entity\"></span> Entity not specified</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ver-no-status",
            "description": "<p><span id=\"error_ver-no-status\"></span> Status missing in item data</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ver-unk-table",
            "description": "<p><span id=\"error_ver-unk-table\"></span> Unknown table specified for add item</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ver-inv-val-type",
            "description": "<p><span id=\"error_ver-inv-val-type\"></span> Invalid validation type provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ver-inv-type",
            "description": "<p><span id=\"error_ver-inv-type\"></span> A value has a wrong data type</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ver-inv-status",
            "description": "<p><span id=\"error_ver-inv-status\"></span> A value has a invalid status</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ver-inv-applied",
            "description": "<p><span id=\"error_ver-inv-applied\"></span> A value can not be applied</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ver-inv-guid",
            "description": "<p><span id=\"error_ver-inv-guid\"></span> A value has a invalid guid</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "val-inv-iso-date",
            "description": "<p><span id=\"error_val-inv-iso-date\"></span> Invalid date format</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ver-mis-sched",
            "description": "<p><span id=\"error_ver-mis-sched\"></span> Scheduled at key missing</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ver-no-change",
            "description": "<p><span id=\"error_ver-no-change\"></span> New item is same as old one</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ver-no-prev",
            "description": "<p><span id=\"error_ver-no-prev\"></span> Previous version not found in system</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ss-err-hpost",
            "description": "<p><span id=\"error_ss-err-hpost\"></span> Error posting the hash to baking endpoint</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "htl-map-error",
            "description": "<p><span id=\"error_htl-map-error\"></span> Error occurred while mapping hotel data</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "htl-imp-err",
            "description": "<p><span id=\"error_htl-imp-err\"></span> Unable to import hotel</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "res-cd-save",
            "description": "<p><span id=\"error_res-cd-save\"></span> Can not save reservation key to Stormpath custom data</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "res-not-auth",
            "description": "<p><span id=\"error_res-not-auth\"></span> This user not authenticated. Please log in and try again</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "res-inv-email",
            "description": "<p><span id=\"error_res-inv-email\"></span> This profile email is invalid</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "imp-comm-err",
            "description": "<p><span id=\"error_imp-comm-err\"></span> Unable to make http request</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "imp-parse-fail",
            "description": "<p><span id=\"error_imp-parse-fail\"></span> Wrong input content format</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "int-img-err",
            "description": "<p><span id=\"error_int-img-err\"></span> Unable to parse image object</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "http-inv-res",
            "description": "<p><span id=\"error_http-inv-res\"></span> Invalid response from http(s) request</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "u-err-no-img",
            "description": "<p><span id=\"error_u-err-no-img\"></span> Error when comparing images</p> "
          }
        ]
      }
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "-",
    "title": "j. Errors",
    "name": "basicsErrors",
    "version": "0.4.7",
    "group": "1_Basics",
    "description": "<p>When an error occurs, the API returns a error message with a JSON body</p> ",
    "parameter": {
      "examples": [
        {
          "title": "Example error object",
          "content": "{\n    \"module\": \"App\",\n    \"code\": \"app-inv-route\",\n    \"message\": \"Invalid path\",\n    \"developerMessage\": \"This path does not exist\",\n    \"moreInfo\": \"https://localhost:8000/doc/#errors-app-inv-route\",\n    \"statusCode\": 400,\n    \"isCustomError\": true,\n    \"debuggingData\": {\n        \"requestedPath\": \"/v1/test-hgid/app/inits\"\n    },\n    \"localized\": {\n        \"en_GB\": {\n            \"userMessage\": \"Invalid path\"\n        }\n    }\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Error object structure": [
          {
            "group": "Error object structure",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": ""
          },
          {
            "group": "Error object structure",
            "type": "String",
            "optional": false,
            "field": "error.module",
            "description": "<p>Which module gave the error</p> "
          },
          {
            "group": "Error object structure",
            "type": "String",
            "optional": false,
            "field": "error.message",
            "description": "<p>Error message for the end-user (untranslated)</p> "
          },
          {
            "group": "Error object structure",
            "type": "String",
            "optional": false,
            "field": "error.developerMessage",
            "description": "<p>Error message for the developer</p> "
          },
          {
            "group": "Error object structure",
            "type": "String",
            "optional": false,
            "field": "error.moreInfo",
            "description": "<p>Link to a page with more information about this error</p> "
          },
          {
            "group": "Error object structure",
            "type": "Number",
            "optional": false,
            "field": "error.statusCode",
            "defaultValue": "500",
            "description": "<p>The http status code, which was given</p> "
          },
          {
            "group": "Error object structure",
            "type": "Boolean",
            "optional": false,
            "field": "error.isCustomError",
            "defaultValue": "true",
            "description": "<p>Boolean indicating, that this error was handled by the system</p> "
          },
          {
            "group": "Error object structure",
            "type": "Object",
            "optional": false,
            "field": "error.debuggingData",
            "description": "<p>Object, which has some information, which can help debug and fix the cause of this error</p> "
          },
          {
            "group": "Error object structure",
            "type": "Object",
            "optional": false,
            "field": "error.localized",
            "description": "<p>Localized information about this error</p> "
          },
          {
            "group": "Error object structure",
            "type": "Object",
            "optional": false,
            "field": "error.localized.LANG",
            "description": "<p>Information in given <code>LANG</code></p> "
          },
          {
            "group": "Error object structure",
            "type": "Object",
            "optional": false,
            "field": "error.localized.LANG.userMessage",
            "description": "<p>Translated message, to show to the end-user</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "app-inv-route",
            "description": "<p><span id=\"error_app-inv-route\"></span> This path does not exist</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "com-fields-type",
            "description": "<p><span id=\"error_com-fields-type\"></span> Some fields did not match expected type</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-query-error",
            "description": "<p><span id=\"error_cat-query-error\"></span> Error occurred querying categories</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-cnt-query-error",
            "description": "<p><span id=\"error_cat-cnt-query-error\"></span> Error occurred querying hotel count in categories</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-inv-type",
            "description": "<p><span id=\"error_cat-inv-type\"></span> Invalid category type provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ai-not-found",
            "description": "<p><span id=\"error_ai-not-found\"></span> Invalid hotel group id or uuid provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p><span id=\"error_hg-not-found\"></span> Invalid hotel group id provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-err-save",
            "description": "<p><span id=\"error_hg-err-save\"></span> Error occurred while saving hotel group to database</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-err-del",
            "description": "<p><span id=\"error_hg-err-del\"></span> Error occurred while deleting hotel group from database</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-err-format",
            "description": "<p><span id=\"error_hg-err-format\"></span> Error occurred while formatting data</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-err-up",
            "description": "<p><span id=\"error_hg-err-up\"></span> Error occurred while updating hotel group</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-hotel-nf",
            "description": "<p><span id=\"error_hs-hotel-nf\"></span> Hotel, with given parameters was not found</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-save",
            "description": "<p><span id=\"error_hs-err-save\"></span> Error occurred while saving hotel to database</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-src-err",
            "description": "<p><span id=\"error_hs-src-err\"></span> Error searching hotels</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-del",
            "description": "<p><span id=\"error_hs-err-del\"></span> Error deleting item(s) from search index</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-for",
            "description": "<p><span id=\"error_hs-err-for\"></span> Error formatting hotel</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-up-img",
            "description": "<p><span id=\"error_iu-err-up-img\"></span> Error occurred while uploading the image to CDN</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-no-img",
            "description": "<p><span id=\"error_iu-err-no-img\"></span> The image was not found in given path</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-inv-img-obj",
            "description": "<p><span id=\"error_iu-inv-img-obj\"></span> Invalid image object passed fot parsing</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-del-imgs",
            "description": "<p><span id=\"error_iu-err-del-imgs\"></span> Error occurred, trying to delete images</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-inv-mc",
            "description": "<p><span id=\"error_iu-err-inv-mc\"></span> Invalid media configuration given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-inv-tr",
            "description": "<p><span id=\"error_iu-err-inv-tr\"></span> Invalid transformation given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "iu-err-inv-tr-tp",
            "description": "<p><span id=\"error_iu-err-inv-tr-tp\"></span> Invalid transformation type given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-account-add",
            "description": "<p><span id=\"error_ps-account-add\"></span> Account, with given parameters can not be added</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-account-auth",
            "description": "<p><span id=\"error_ps-account-auth\"></span> Account, with given parameters can not be authenticated</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-app-get",
            "description": "<p><span id=\"error_ps-app-get\"></span> Application, with given parameters can not be found</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-acc-get",
            "description": "<p><span id=\"error_ps-acc-get\"></span> Account, with given parameters can not be found</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-session-add",
            "description": "<p><span id=\"error_ps-session-add\"></span> Application, with given parameters can not be found</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-session-out",
            "description": "<p><span id=\"error_ps-session-out\"></span> Application can not update sessionEnd</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-session-get",
            "description": "<p><span id=\"error_ps-session-get\"></span> Application can not get last session details for this profile</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-tenant-verify",
            "description": "<p><span id=\"error_ps-tenant-verify\"></span> Application can not verify user account</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-token-add",
            "description": "<p><span id=\"error_ps-token-add\"></span> Application can not add to user account custom data</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-verify-inv",
            "description": "<p><span id=\"error_ps-verify-inv\"></span> Verification key is invalid</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-verify-failed",
            "description": "<p><span id=\"error_ps-verify-failed\"></span> This page does not exist</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-resend-failed",
            "description": "<p><span id=\"error_ps-resend-failed\"></span> Email verification resending failed.</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-resend-inv",
            "description": "<p><span id=\"error_ps-resend-inv\"></span> Email verification resending not valid. Check PayLoad details. Login information is necessary</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-sav-ix",
            "description": "<p><span id=\"error_sr-err-sav-ix\"></span> Error saving to search index</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-lis-ix",
            "description": "<p><span id=\"error_sr-err-lis-ix\"></span> Error listing search indexes</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-src",
            "description": "<p><span id=\"error_sr-err-src\"></span> Error searching</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-new",
            "description": "<p><span id=\"error_sr-err-new\"></span> Error creating search index</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-del",
            "description": "<p><span id=\"error_sr-err-del\"></span> Error deleting search index</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-itm-err-del",
            "description": "<p><span id=\"error_sr-itm-err-del\"></span> Error deleting item from search index</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-hgid",
            "description": "<p><span id=\"error_au-inv-hgid\"></span> Invalid HotelGroup Id provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-ver",
            "description": "<p><span id=\"error_au-inv-ver\"></span> Invalid API version given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-no-uuid",
            "description": "<p><span id=\"error_au-no-uuid\"></span> No UUID given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-uuid",
            "description": "<p><span id=\"error_au-inv-uuid\"></span> Invalid UUID given</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-path",
            "description": "<p><span id=\"error_au-inv-path\"></span> Invalid api call URL</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-hrefqp",
            "description": "<p><span id=\"error_au-inv-hrefqp\"></span> Invalid queryParams provided, must be an object</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-linkmethod",
            "description": "<p><span id=\"error_au-inv-linkmethod\"></span> Invalid method provided for link, must be on of GET/PUT/POST/DELETE</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-req-method",
            "description": "<p><span id=\"error_au-inv-req-method\"></span> Invalid request method</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-loc-inp",
            "description": "<p><span id=\"error_au-inv-loc-inp\"></span> Invalid location search input</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-req-bd",
            "description": "<p><span id=\"error_au-inv-req-bd\"></span> Invalid payload</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-req-ct",
            "description": "<p><span id=\"error_au-inv-req-ct\"></span> Invalid request content type</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-https-req",
            "description": "<p><span id=\"error_au-https-req\"></span> Use of https protocol is required</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-ver-req",
            "description": "<p><span id=\"error_au-ver-req\"></span> Use of x-cardola-verify header is required</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-ver-inv",
            "description": "<p><span id=\"error_au-ver-inv\"></span> Invalid x-cardola-verify header value</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-verifykey",
            "description": "<p><span id=\"error_au-inv-verifykey\"></span> Invalid Verify Key provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-len-verifykey",
            "description": "<p><span id=\"error_au-len-verifykey\"></span> Invalid Verify Key length. Length must be more then 10 symbols</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-ol2pi-hl",
            "description": "<p><span id=\"error_sr-err-ol2pi-hl\"></span> Error converting offset and limit to pages, it took too many cycles</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p><span id=\"error_db-cc-db\"></span> No database connection</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p><span id=\"error_db-query-fails\"></span> Database query failed</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-no-langcode",
            "description": "<p><span id=\"error_lu-no-langcode\"></span> No language code provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-inv-langcode",
            "description": "<p><span id=\"error_lu-inv-langcode\"></span> Invalid language code provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-no-countrycode",
            "description": "<p><span id=\"error_lu-no-countrycode\"></span> No country code provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-inv-countrycode",
            "description": "<p><span id=\"error_lu-inv-countrycode\"></span> Invalid country code provided</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "lu-no-trans",
            "description": "<p><span id=\"error_lu-no-trans\"></span> No translation found</p> "
          }
        ]
      }
    },
    "filename": "src/modules/common/_apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "/images/<width>x<height>/sample.jpg",
    "title": "e. Images",
    "name": "basicsImages",
    "version": "1.7.4",
    "group": "1_Basics",
    "description": "<p>If a resource must return a reference to a image, it will return an Object, which contains url&#39;s, with tokens, to the image</p> <p>Tokens in the url are wrapped between <code>&lt;</code> and <code>&gt;</code> (as seen in the example url)</p> <p>NB! I a request is made to the final url of the image, then it might take couple of seconds to respond, because if the image is requested with a size, that has never been requested before, the server will render the image to that size and return it.</p> ",
    "parameter": {
      "fields": {
        "Image object": [
          {
            "group": "Image object",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>Image object</p> "
          },
          {
            "group": "Image object",
            "type": "String",
            "optional": false,
            "field": "image.highRes",
            "description": "<p>URL to high resolution image, with tokens</p> "
          },
          {
            "group": "Image object",
            "type": "String",
            "optional": false,
            "field": "image.lowRes",
            "description": "<p>URL to low resolution image, with tokens</p> "
          }
        ],
        "Tokens": [
          {
            "group": "Tokens",
            "type": "Number",
            "optional": false,
            "field": "width",
            "description": "<p>The required width of the image in pixels</p> <p>Example: &quot;100&quot; to get image with width of 100 pixels</p> "
          },
          {
            "group": "Tokens",
            "type": "Number",
            "optional": false,
            "field": "height",
            "description": "<p>The required height of the image in pixels</p> <p>Example: &quot;90&quot; to get image with height of 90 pixels</p> "
          }
        ]
      }
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "/images/<width>x<height>/sample.jpg",
    "title": "5. Images",
    "name": "basicsImages",
    "version": "0.0.1",
    "group": "1_Basics",
    "description": "<p>If a resource must return a reference to a image, it will return an url to the image. This url contains tokens, which must be replaced by the client to get the working url.</p> <p>Tokens in the url are wrapped between <code>&lt;</code> and <code>&gt;</code> (as seen in the example url)</p> <p>NB! I a request is made to the final url of the image, then it might take couple of seconds to respond, because if the image is requested with a size, that has never been requested before, the server will render the image to that size and return it.</p> ",
    "parameter": {
      "fields": {
        "Tokens": [
          {
            "group": "Tokens",
            "type": "Number",
            "optional": false,
            "field": "width",
            "description": "<p>The required width of the image in pixels</p> <p>Example: &quot;100&quot; to get image with width of 100 pixels</p> "
          },
          {
            "group": "Tokens",
            "type": "Number",
            "optional": false,
            "field": "height",
            "description": "<p>The required height of the image in pixels</p> <p>Example: &quot;90&quot; to get image with height of 90 pixels</p> "
          }
        ]
      }
    },
    "filename": "src/modules/common/_apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "-",
    "title": "a. Introduction",
    "name": "basicsIntroduction",
    "version": "1.7.4",
    "group": "1_Basics",
    "description": "<p>Cardola Group App API provides RESTful services for client applications. In this section we cover some of the basics to get you started.</p> <p>The Cardola Group API will adhere to the following conventions:</p> <ul> <li>Request / Response headers shall indicate the use of JSON and UTF-8 character set in payload.<ul> <li>Request will have a header: <strong>Accept: application/json</strong></li> <li>Response will include a header: <strong>Content-Type: application/json; charset=utf-8</strong></li> </ul> </li> <li>The API has versions, the version of the API is specified in the URL</li> <li>All references to time shall be expressed in accordance with <strong>ISO8601</strong></li> <li>All references to a language shall be expressed in accordance with <strong>ISO 639-1</strong> and include region designator (Example &#39;en_GB&#39;)</li> <li>All access to data specific to a user will be made over SSL</li> <li>Mobile applications must not assume that only the JSON fields / collections they require have been returned and should successfully ignore data name value pairs within the JSON it does not require</li> <li>Mobile applications must fail gracefully and report errors to the user</li> <li>Caching of non-encrypted data will facilitate caching through the use of the cache control tags ETag’s , Cache-Control:public, max-age=<seconds> and expires: <date/time> information</li> <li>All requests, except &quot;app/registration&quot; , shall include a header <strong>X-Cardola-Uuid</strong> with the UUID value, which was given to the app from &quot;/registration&quot; API call.</li> <li>All requests, which handle delicate data or access limited services shall include a header <strong>X-Cardola-Verify</strong>, which is a checksum for the request (<a href=\"#api-1_Basics-basicsRequest\">More here</a>).</li> </ul> ",
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "/",
    "title": "1. Introduction",
    "name": "basicsIntroduction",
    "version": "0.0.1",
    "group": "1_Basics",
    "description": "<p>Cardola Group App API provides RESTful services for client applications. In this section we cover some of the basics to get you started.</p> <p>The Cardola Group API will adhere to the following conventions:</p> <ul> <li>Request / Response headers shall indicate the use of JSON and UTF-8 character set in payload.</li> <li><ul> <li>Request will have a header: <strong>Accept: application/json</strong></li> </ul> </li> <li><ul> <li>Response will include a header: <strong>Content-Type: application/json; charset=utf-8</strong></li> </ul> </li> <li>The API has versions, the version of the API is specified in the URL</li> <li>All references to time shall be expressed in accordance with <strong>ISO8601</strong></li> <li>All references to a language shall be expressed in accordance with <strong>ISO3166</strong></li> <li>All access to data specific to a user will be made over SSL</li> <li>All access to data that may be available to anyone downloading the application and browsing the hotels shall be sent in the clear (without SSL)</li> <li>Mobile applications must not assume that only the JSON fields / collections they require have been returned and should successfully ignore data name value pairs within the JSON it does not require</li> <li>Preferred languages will be sent in all headers by the mobile apps and take advantage of the weighting system provided.</li> <li><ul> <li>If only a single language has been selected by the user the header will contain in the case of Russian being the selected language:</li> </ul> </li> <li><ul> <li><ul> <li>Accept-Language: ru</li> </ul> </li> </ul> </li> <li>If German is the users first choice and Estonian second it will be:</li> <li><ul> <li>Accept-Language: da; q=0.7, ee; q=0.3</li> </ul> </li> <li>If Russian 1st, English 2nd and German 3rd it will be:</li> <li><ul> <li>Accept-Language: ru; q=0.5, en; q=0.3, da; q=0.2</li> </ul> </li> <li>Mobile applications must fail gracefully and report errors to the user</li> <li>Caching of non-encrypted data will facilitate caching through the use of the cache control tags ETag’s , Cache-Control:public, max-age=<seconds> and expires: <date/time> information</li> <li>All requests, except &quot;registration&quot; , shall include an header x-cardola-uuid with the UUID value, which was given to the app from &quot;/registration&quot; API call.</li> </ul> <p>The Base url for LIVE API is as follows:</p> ",
    "filename": "src/modules/common/_apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "-",
    "title": "d. Languages",
    "name": "basicsLangs",
    "version": "1.7.4",
    "group": "1_Basics",
    "description": "<p>All resources, which are returned from the REST API are returned in all supported languages. It is up to the client application to display the content in a right language.</p> <p>Every resource object, which is returned from the REST API, includes a <code>localized</code> Object, which holds translated information about this resource. Note that not all of the information is localized. The <code>localized</code> object has a key for every language, into which this resource is translated into, if a key for some language is missing, it means that the resource is not translated into this language.</p> <p>When language is represented in the API response, it has the following structure:</p> ",
    "parameter": {
      "fields": {
        "Language object": [
          {
            "group": "Language object",
            "type": "Object",
            "optional": false,
            "field": "language",
            "description": "<p>Language object</p> "
          },
          {
            "group": "Language object",
            "type": "String",
            "size": "5",
            "optional": false,
            "field": "language.code",
            "description": "<p>Language code in ISO 639-1 plus region designator (Example <code>en_GB</code>)</p> "
          },
          {
            "group": "Language object",
            "type": "String",
            "optional": false,
            "field": "language.englishName",
            "description": "<p>Language name in english</p> "
          },
          {
            "group": "Language object",
            "type": "String",
            "optional": false,
            "field": "language.nativeName",
            "description": "<p>Language name in its own language</p> "
          },
          {
            "group": "Language object",
            "type": "Boolean",
            "optional": false,
            "field": "language.rtl",
            "description": "<p>Boolean indicating if this language is written from Right to Left</p> "
          }
        ]
      }
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "-",
    "title": "c. Links",
    "name": "basicsLinks",
    "version": "1.7.4",
    "group": "1_Basics",
    "description": "<p>If a resource needs to reference to another resource (or an actions), it will use a <code>Link</code> object.</p> ",
    "parameter": {
      "fields": {
        "Link object": [
          {
            "group": "Link object",
            "type": "Object",
            "optional": false,
            "field": "link",
            "description": "<p>Link object</p> "
          },
          {
            "group": "Link object",
            "type": "String",
            "optional": false,
            "field": "link.href",
            "description": "<p>The URL to REST service, which returns the linked resource or does an action.</p> "
          },
          {
            "group": "Link object",
            "type": "String",
            "allowedValues": [
              "GET",
              "PUT",
              "POST",
              "DELETE"
            ],
            "optional": true,
            "field": "link.method",
            "defaultValue": "GET",
            "description": "<p>The request method to use, if this is missing then GET must be used</p> "
          }
        ]
      }
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "/:path",
    "title": "b. Request structure",
    "name": "basicsRequest",
    "version": "1.7.4",
    "group": "1_Basics",
    "description": "<p>The REST api has a very specific URL structure, which specifies which version of the API are you working with and which hotel group data are you working with.</p> <p>Also bear in mind that the <strong>method</strong> of the request (GET/PUT/POST/DELETE) is also a vital component of the RESTful services.</p> ",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>The API version to use. Example: &quot;v1&quot;.</p> <p>List of possible API versions are listed on top right corner of this page. If you want to use version &quot;1.0.1&quot;, then you specify the <code>:version</code> parameter in the URL as <code>v1</code></p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hgId",
            "description": "<p>The hotel group Id. Example: &quot;test-hgid&quot;</p> "
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>The path, which determines the REST API service, which is requested. For example &quot;app/init&quot; or &quot;categories&quot;</p> "
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-Cardola-Uuid",
            "description": "<p>UUID value, which has been assigned to this application instance through /registration call.</p> "
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-Cardola-Verify",
            "description": "<p>The Hash checksum of the request. The hash is calculated using the API KEY provided by Cardola.</p> <p><strong>The calculation algorithm uses following inputs:</strong></p> <ul> <li><strong>url</strong> is the full url including query parameters</li> <li><strong>uuid</strong> is the UUID of the app, which has been retrieved from /app/registration (value used in the X-Cardola-Uuid header)<ul> <li><strong>NB!</strong> This value is empty for <strong>app/registration</strong> service, because the app does not know it yet</li> </ul> </li> <li><strong>apiKey</strong> is an API KEY provided by Cardola</li> <li><strong>randomSalt</strong> is a random string with a fixed length of 10</li> </ul> <p><strong>The method of calculating the checksum:</strong></p> <ul> <li>a = sha256(url + apiKey)</li> <li>b = sha256(a + randomSalt)</li> <li>c = sha256(b + uuid)</li> <li>checksum = toHex(c) + randomSalt (toHex converts each character code from decimal to hexadecimal)</li> </ul> <p><strong>Sample:</strong></p> <p>Data:</p> <ul> <li>url: <code>https://localhost:8000/v1/test-hgid/profiles/status</code></li> <li>uuid: <code>b4cf2e90-b134-11e4-af5d-5d230ac74c0b</code></li> <li>apiKey: <code>test-api-key</code></li> <li>randomSalt: <code>5324b42a7f</code> Resulting hash: <code>fefa828c04beba4be0100ffae77b4d3837979deef6ab8bf1b17c01f861ff477fdc9ad44edb</code></li> </ul> <p>Calculation:</p> <ul> <li>a = sha256(&quot;<a href=\"https://localhost:8000/v1/test-hgid/profiles/status\">https://localhost:8000/v1/test-hgid/profiles/status</a>&quot; + &quot;test-api-key&quot;) = <code>-_ï\\u0000åÁñÑÏviÉÅüîµs®¾{èìï¼xu)V\\u001fYD</code></li> <li>b = sha256(a + &quot;5324b42a7f&quot;) = <code>Ö\\u0006ëÜ\\u0006\\u000ecÝO5ðÇDµ&gt;nMã\\u0016§&gt;ìfã3ÔD</code></li> <li>c = sha256(b + &quot;b4cf2e90-b134-11e4-af5d-5d230ac74c0b&quot;) = <code>ÎøÃV?_¾øÃdçøt&gt;1\\u0013±Çü0à\\u0003¬rnÜ@Å¢</code></li> <li>checksum = toHex(c) + randomSalt = &quot;cef8c3563f5fbef8c364e7f8743e3113b1c7fc30e00398ac72856edc40c5a2e3&quot; + &quot;5324b42a7f&quot; = <code>cef8c3563f5fbef8c364e7f8743e3113b1c7fc30e00398ac72856edc40c5a2e35324b42a7f</code></li> </ul> "
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "X-Cardola-Uuid: b4cf2e90-b134-11e4-af5d-5d230ac74c0b\nX-Cardola-Verify: cef8c3563f5fbef8c364e7f8743e3113b1c7fc30e00398ac72856edc40c5a2e35324b42a7f",
          "type": "html"
        }
      ]
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "-",
    "title": "g. Resources",
    "name": "basicsResources",
    "version": "1.7.4",
    "group": "1_Basics",
    "description": "<p>As a RESTful service, this API operates with resources. A resource is an object, with a certain type. In this API we operate with resources like <code>Hotel</code> and <code>Category</code></p> <p>All resources have their own data structure, but they also share some common fields</p> ",
    "parameter": {
      "fields": {
        "Common fields on resources": [
          {
            "group": "Common fields on resources",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>A URL, which points to a service, which returns only this resource, with detail information.</p> "
          }
        ]
      }
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "-",
    "title": "i. Security",
    "name": "basicsSecurity",
    "version": "1.7.4",
    "group": "1_Basics",
    "description": "<p>Some API services are not public and they handle delicate data, which must be secured. Therefore we have defined security levels, which can be required by some of the services. In this documentation the services, which require some security (more than level 0), have a note about the level, they require.</p> <p>These security levels are:</p> <ul> <li><strong>0</strong> - No extra security required</li> <li><strong>1</strong> - Use of <code>x-cardola-uuid</code> header is required (<a href=\"#api-1_Basics-basicsRequest\">About</a>)</li> <li><strong>2</strong> - Requirements of level <strong>1</strong> + Use of <code>https</code> protocol is required</li> <li><strong>3</strong> - Requirements of level <strong>2</strong> + Use of <code>x-cardola-verify</code> header is required (<a href=\"#api-1_Basics-basicsRequest\">About</a>)</li> </ul> ",
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "-",
    "title": "f. Testing",
    "name": "basicsTesting",
    "version": "1.7.4",
    "group": "1_Basics",
    "description": "<p>To test the the RESTful services you must use our testing server <code>http://group-app.herokuapp.com</code> instead of <code>http://api.cadrola.com</code>.</p> <p>For testing purposes you DO NOT need to use the <code>X-Cardola-Verify</code> header. So if you do not provide this header, it will work. If you do provide this header, then it is validated and must have a correct value, as described in <a href=\"#api-1_Basics-basicsRequest\">here</a></p> <p>We have setup a demo Hotel Group called <code>Cardola Luxury hotels &amp; resorts</code>, with a Hotel Group Id (<code>:hgid</code> part in the URL) value of <code>test-hgid</code>.</p> <p>For example, if you want to register your app (first thing you must do! <a href=\"#api-2_Initialization-appRegistration\">/app/registration</a>), you must use the url: <code>http://group-app.herokuapp.com/v1/test-hgid/app/registration</code></p> ",
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "1_Basics"
  },
  {
    "type": "get",
    "url": "/app/init",
    "title": "2. Get initial data",
    "name": "appInit",
    "version": "1.7.4",
    "group": "2_Initialization",
    "description": "<p>When client application has the UUID, then every time the application is loaded, it must make a call to this service to get the vital information, which it will need to build the UI and provide content to end-user.</p> ",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>The href of current request/resource</p> "
          },
          {
            "group": "Success 200",
            "type": "Language[]",
            "optional": false,
            "field": "supportedLanguages",
            "description": "<p>List of Language objects, which are supported by the hotel group (<a href=\"#api-1_Basics-basicsLangs\">About languages</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Language",
            "optional": false,
            "field": "defaultLanguage",
            "description": "<p>The default language object (<a href=\"#api-1_Basics-basicsLangs\">About languages</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "translations",
            "description": "<p>Object, which contain translations of the UI phrases.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "gaTrackingId",
            "description": "<p>Google Analytics Tracking Id, value is string or null</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "translations.localized",
            "description": "<p>Object containing localized translations for every language.</p> "
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "translations.localized.LANGUAGE_CODE",
            "description": "<p>An Object containing translations for a language</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "translations.LANGUAGE_CODE.KEY",
            "description": "<p>A translated string with a given <code>KEY</code></p> "
          },
          {
            "group": "Success 200",
            "type": "Link",
            "optional": false,
            "field": "signIn",
            "description": "<p>A Link object, which points to a sing in action (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Link",
            "optional": false,
            "field": "signUp",
            "description": "<p>A Link object, which points to a sing up in action (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Link",
            "optional": false,
            "field": "signOut",
            "description": "<p>A Link object, which points to a sing out in action (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Link",
            "optional": false,
            "field": "mainMenu",
            "description": "<p>A Link object, which points to a service, returning main menu structure (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "hotelGroupInfo",
            "description": "<p>A Object containing info about hotel group</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "hotelGroupInfo.localized",
            "description": "<p>A Object containing info in certain language</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "hotelGroupInfo.localized.LANGUAGE",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "hotelGroupInfo.localized.LANGUAGE.name",
            "description": "<p>Hotel name in certain language</p> "
          },
          {
            "group": "Success 200",
            "type": "Image",
            "optional": false,
            "field": "hotelGroupInfo.localized.LANGUAGE.logo",
            "description": "<p>Hotel logo as Image object (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Image",
            "optional": false,
            "field": "hotelGroupInfo.localized.LANGUAGE.backgroundImage",
            "description": "<p>The background image to use for this hotel group.</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "theme",
            "description": "<p>Object containing theme information</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.primaryColor",
            "description": "<p>Object containing information about one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.secondaryColor",
            "description": "<p>Object containing information about one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.primaryTextColor",
            "description": "<p>Object containing information about one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.secondaryTextColor",
            "description": "<p>Object containing information about one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.backgroundColor",
            "description": "<p>Object containing information about primary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.inputColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.backgroundTextColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.inputTextColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.primaryImageTextColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.secondaryImageTextColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.backgroundSubtextColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.heroTextColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.heroSubtextColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.placeholderTextColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.progressIndicatorColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.primaryIconColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          },
          {
            "group": "Success 200",
            "type": "Color",
            "optional": false,
            "field": "theme.secondaryIconColor",
            "description": "<p>Object containing information about secondary one theme color (See the color object definition)</p> "
          }
        ],
        "Color object": [
          {
            "group": "Color object",
            "type": "String",
            "optional": false,
            "field": "r",
            "description": "<p>Value for Red color (in RGB) from 0-255</p> "
          },
          {
            "group": "Color object",
            "type": "Integer",
            "optional": false,
            "field": "g",
            "description": "<p>Value for Green color (in RGB) from 0-255</p> "
          },
          {
            "group": "Color object",
            "type": "Integer",
            "optional": false,
            "field": "b",
            "description": "<p>Value for Blue color (in RGB) from 0-255</p> "
          },
          {
            "group": "Color object",
            "type": "Float",
            "optional": false,
            "field": "alpha",
            "description": "<p>The alpha/opacity value for this color from 0-1</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " {\n  \"supportedLanguages\": [\n    {\n      \"code\": \"ar_SA\",\n      \"englishName\": \"Arabic\",\n      \"nativeName\": \"العربية\",\n      \"rtl\": true,\n      \"countryEnglishName\": \"Saudi Arabia\"\n    },\n    {\n      \"code\": \"de_DE\",\n      \"englishName\": \"German\",\n      \"nativeName\": \"Deutsch\",\n      \"rtl\": false,\n      \"countryEnglishName\": \"Germany\"\n    }\n  ],\n  \"gaTrackingId\": \"UA-70491234-1\",\n  \"defaultLanguage\": {\n    \"code\": \"en_GB\",\n    \"englishName\": \"English\",\n    \"nativeName\": \"English\",\n    \"rtl\": false,\n    \"countryEnglishName\": \"United Kingdom of Great Britain and Northern Ireland\"\n  },\n  \"translations\": {\n    \"localized\": {\n      \"fr_FR\": {\n        \"NoneCell\": \"None\",\n        \"MapButton\": \"Map\",\n        \"PhoneText\": \"Phone\",\n        \"WebButton\": \"WWW\",\n        \"CallButton\": \"Call\",\n        \"DoneButton\": \"Done\",\n        \"EmailField\": \"Email Address\",\n        \"SearchText\": \"Search\",\n        \"SkipButton\": \"Skip\",\n        \"ShowAllCell\": \"Show all\",\n        \"SignInButton\": \"Sign in\",\n        \"SignUpButton\": \"Sign up\",\n        \"SurnameField\": \"Surname\",\n        \"VerifyButton\": \"Verify details\",\n        \"NoResultsCell\": \"No results\",\n        \"PasswordField\": \"Password\",\n        \"SignOutButton\": \"Sign out\",\n        \"FirstNameField\": \"First Name\",\n        \"LanguageButton\": \"Language\",\n        \"ProcessingText\": \"Processing\",\n        \"SettingsButton\": \"Settings\",\n        \"ForgottenButton\": \"Forgotten password\",\n        \"EmailConfirmField\": \"Confirm Email Address\",\n        \"ExploreHotelButton\": \"Explore Hotel\",\n        \"InvalidEmailFormat\": \"Invalid email address format. Please correct and try again.\",\n        \"InvalidEntryFormat\": \"Invalid entry. Please correct and try again.\",\n        \"InvalidEmailConfirm\": \"Your emails do not match. Please try again.\",\n        \"PasswordConfirmField\": \"Confirm Password\",\n        \"CorrectPasswordFormat\": \"Passwords must be at least 8 characters long and include both upper and lower-case letters and numbers.\",\n        \"CurrentLocationButton\": \"Current location\",\n        \"InvalidPasswordFormat\": \"Weak password. See requirements.\",\n        \"CheckConnectionMessage\": \"Please check your internet connection.\",\n        \"InvalidPasswordConfirm\": \"Your passwords do not match. Please try again.\",\n        \"TermsAndConditionsHint\": \"By submitting your application you agree to be bound by the terms and conditions below.\",\n        \"TermsAndConditionsText\": \"Please read and review the terms and conditions before using this service.\\n\\nUser acknowledges and agrees that the Services are for personal use and agrees not to use the Services in a manner prohibited by any federal or state law or regulation.\\n\\nUser acknowledges that there is content on the Internet or otherwise available through the Services which may be offensive, or which may not be in compliance with all local laws, regulations and other rules. We assume no responsibility for and exercises no control over the content contained on the Internet or is otherwise available through the Services. All content accessed or received by the User is used by User at his or her own risk, and we and our employees shall have no liability resulting from the access or use of such content by the User.\",\n        \"NoAvailableRouteMessage\": \"There is no route available.\",\n        \"PreferredLanguageHeader\": \"Preferred language\",\n        \"TermsAndConditionsButton\": \"Terms and conditions\",\n        \"ThirdPreferredLanguageHeader\": \"Third choice (optional)\",\n        \"SecondPreferredLanguageHeader\": \"Second choice (optional)\",\n        \"TermsAndConditionsButtonShort\": \"T&Cs\",\n        \"LocationServiceDisabledMessage\": \"Please enable location services.\",\n        \"NoHotelsInNearestRadiusMessage\": \"There are no hotels in your radius.\"\n      },\n      \"it_IT\": {\n        \"NoneCell\": \"None\",\n        \"MapButton\": \"Map\",\n        \"PhoneText\": \"Phone\",\n        \"WebButton\": \"WWW\",\n        \"CallButton\": \"Call\",\n        \"DoneButton\": \"Done\",\n        \"EmailField\": \"Email Address\",\n        \"SearchText\": \"Search\",\n        \"SkipButton\": \"Skip\",\n        \"ShowAllCell\": \"Show all\",\n        \"SignInButton\": \"Sign in\",\n        \"SignUpButton\": \"Sign up\",\n        \"SurnameField\": \"Surname\",\n        \"VerifyButton\": \"Verify details\",\n        \"NoResultsCell\": \"No results\",\n        \"PasswordField\": \"Password\",\n        \"SignOutButton\": \"Sign out\",\n        \"FirstNameField\": \"First Name\",\n        \"LanguageButton\": \"Language\",\n        \"ProcessingText\": \"Processing\",\n        \"SettingsButton\": \"Settings\",\n        \"ForgottenButton\": \"Forgotten password\",\n        \"EmailConfirmField\": \"Confirm Email Address\",\n        \"ExploreHotelButton\": \"Explore Hotel\",\n        \"InvalidEmailFormat\": \"Invalid email address format. Please correct and try again.\",\n        \"InvalidEntryFormat\": \"Invalid entry. Please correct and try again.\",\n        \"InvalidEmailConfirm\": \"Your emails do not match. Please try again.\",\n        \"PasswordConfirmField\": \"Confirm Password\",\n        \"CorrectPasswordFormat\": \"Passwords must be at least 8 characters long and include both upper and lower-case letters and numbers.\",\n        \"CurrentLocationButton\": \"Current location\",\n        \"InvalidPasswordFormat\": \"Weak password. See requirements.\",\n        \"CheckConnectionMessage\": \"Please check your internet connection.\",\n        \"InvalidPasswordConfirm\": \"Your passwords do not match. Please try again.\",\n        \"TermsAndConditionsHint\": \"By submitting your application you agree to be bound by the terms and conditions below.\",\n        \"TermsAndConditionsText\": \"Please read and review the terms and conditions before using this service.\\n\\nUser acknowledges and agrees that the Services are for personal use and agrees not to use the Services in a manner prohibited by any federal or state law or regulation.\\n\\nUser acknowledges that there is content on the Internet or otherwise available through the Services which may be offensive, or which may not be in compliance with all local laws, regulations and other rules. We assume no responsibility for and exercises no control over the content contained on the Internet or is otherwise available through the Services. All content accessed or received by the User is used by User at his or her own risk, and we and our employees shall have no liability resulting from the access or use of such content by the User.\",\n        \"NoAvailableRouteMessage\": \"There is no route available.\",\n        \"PreferredLanguageHeader\": \"Preferred language\",\n        \"TermsAndConditionsButton\": \"Terms and conditions\",\n        \"ThirdPreferredLanguageHeader\": \"Third choice (optional)\",\n        \"SecondPreferredLanguageHeader\": \"Second choice (optional)\",\n        \"TermsAndConditionsButtonShort\": \"T&Cs\",\n        \"LocationServiceDisabledMessage\": \"Please enable location services.\",\n        \"NoHotelsInNearestRadiusMessage\": \"There are no hotels in your radius.\"\n      },\n      \"ru_RU\": {\n        \"NoneCell\": \"Не выбран\",\n        \"MapButton\": \"Карта\",\n        \"PhoneText\": \"Телефон\",\n        \"WebButton\": \"WWW\",\n        \"CallButton\": \"Позвонить\",\n        \"DoneButton\": \"Готово\",\n        \"EmailField\": \"Адрес электронной почты\",\n        \"SearchText\": \"Поиск\",\n        \"SkipButton\": \"Пропустить\",\n        \"ShowAllCell\": \"Показать все\",\n        \"SignInButton\": \"Войти в систему\",\n        \"SignUpButton\": \"Зарегистрироваться\",\n        \"SurnameField\": \"Фамилия\",\n        \"VerifyButton\": \"Проверьте детали\",\n        \"NoResultsCell\": \"Нет результатов\",\n        \"PasswordField\": \"Пароль\",\n        \"SignOutButton\": \"Выйти из системы\",\n        \"FirstNameField\": \"Имя\",\n        \"LanguageButton\": \"Язык\",\n        \"ProcessingText\": \"Обработка\",\n        \"SettingsButton\": \"Настройки\",\n        \"ForgottenButton\": \"Забыли пароль\",\n        \"EmailConfirmField\": \"Подтвердить адрес электронной почты\",\n        \"ExploreHotelButton\": \"Посмотреть Отель\",\n        \"InvalidEmailFormat\": \"Неправильный формат адреса электронной почты. Пожалуйста, исправьте и попробуйте снова.\",\n        \"InvalidEntryFormat\": \"Неправильный ввод. Подалуйста, исправьте и попробуйье снова.\",\n        \"InvalidEmailConfirm\": \"Адреса электронной почты не совпадают. Попробуйте снова.\",\n        \"PasswordConfirmField\": \"Подтвердить пароль\",\n        \"CorrectPasswordFormat\": \"Пароль должен быть не короче 8 символов, содержать заглавные и строчные буквы и как минимум одну цифру.\",\n        \"CurrentLocationButton\": \"Текущее местоположение\",\n        \"InvalidPasswordFormat\": \"Пароль не соответствует требованиям.\",\n        \"CheckConnectionMessage\": \"Пожалуйста, проверьте подключение к интернету.\",\n        \"InvalidPasswordConfirm\": \"Ваши пароли не совпадают. Попробуйте снова.\",\n        \"TermsAndConditionsHint\": \"При регистрации вы соглашаетесь с положениями и условиями, описанными ниже.\",\n        \"TermsAndConditionsText\": \"Пожалуйста, прочитайте и рассмотрите условия, прежде чем использовать эту услугу.\\n\\nПользователь признает и соглашается с тем, что Услуги предназначены для личного использования и обязуется не использовать Услуги в манере, запрещенной любым федеральным или государственным закона или нормативным актом.\\n\\nПользователь признает, что есть материалы, доступные в Интернете или иным образом с помощью данной услуги, которые могут быть оскорбительными, или не соответствовать всем местным законам, правилам и другим нормам. Мы не несем ответственность и не осуществляем никакого контроля над материалом, содержащимся в Интернете или иным способом доступным с использованием Услуги. Все материалы, просмотренные или полученные Пользователем, используются на его или её собственный страх и риск, и мы и наши сотрудники не несём никакой ответственности за последствия использования такого контента пользователем.\",\n        \"NoAvailableRouteMessage\": \"Путь не найден.\",\n        \"PreferredLanguageHeader\": \"Основной язык\",\n        \"TermsAndConditionsButton\": \"Положения и условия\",\n        \"ThirdPreferredLanguageHeader\": \"Третий язык (по желанию)\",\n        \"SecondPreferredLanguageHeader\": \"Второй язык (по желанию)\",\n        \"TermsAndConditionsButtonShort\": \"Правила\",\n        \"LocationServiceDisabledMessage\": \"Пожалуйста, включите службы геолокации.\",\n        \"NoHotelsInNearestRadiusMessage\": \"В непосредственной близости от вас отелей не найдено.\"\n      }\n    }\n  },\n  \"hotelGroupInfo\": {\n    \"localized\": {\n      \"ar_SA\": {\n        \"name\": \"The Leading Hotels of the World\",\n        \"logo\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/v1/heroku-dev/group-app/test-hgid/logo1428324117000\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/logo1428324117000\"\n        },\n        \"backgroundImage\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg\"\n        }\n      },\n      \"en_GB\": {\n        \"name\": \"The Leading Hotels of the World\",\n        \"logo\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/v1/heroku-dev/group-app/test-hgid/logo1428324117000\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/logo1428324117000\"\n        },\n        \"backgroundImage\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg\"\n        }\n      },\n      \"fr_FR\": {\n        \"name\": \"The Leading Hotels of the World\",\n        \"logo\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/v1/heroku-dev/group-app/test-hgid/logo1428324117000\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/logo1428324117000\"\n        },\n        \"backgroundImage\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg\"\n        }\n      },\n      \"ru_RU\": {\n        \"name\": \"The Leading Hotels of the World\",\n        \"logo\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/v1/heroku-dev/group-app/test-hgid/logo1428324117000\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/logo1428324117000\"\n        },\n        \"backgroundImage\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg\"\n        }\n      },\n      \"et_EE\": {\n        \"name\": \"The Leading Hotels of the World\",\n        \"logo\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/v1/heroku-dev/group-app/test-hgid/logo1428324117000\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/logo1428324117000\"\n        },\n        \"backgroundImage\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg\"\n        }\n      }\n    }\n  },\n  \"theme\": {\n    \"inputColor\": {\n      \"normal\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 0.9\n      },\n      \"disabled\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 0.25\n      },\n      \"selected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 226,\n          \"g\": 226,\n          \"r\": 226,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      }\n    },\n    \"primaryColor\": {\n      \"normal\": {\n          \"b\": 254,\n          \"g\": 128,\n          \"r\": 1,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 0,\n          \"g\": 0,\n          \"r\": 0,\n          \"alpha\": 0.25\n      },\n      \"selected\": {\n          \"b\": 255,\n          \"g\": 189,\n          \"r\": 126,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 211,\n          \"r\": 170,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 255,\n          \"g\": 189,\n          \"r\": 126,\n          \"alpha\": 1\n      }\n    },\n    \"heroTextColor\": {\n      \"normal\": {\n          \"b\": 166,\n          \"g\": 221,\n          \"r\": 253,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 0.5\n      },\n      \"selected\": {\n          \"b\": 183,\n          \"g\": 152,\n          \"r\": 20,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 211,\n          \"r\": 170,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 183,\n          \"g\": 152,\n          \"r\": 20,\n          \"alpha\": 1\n      }\n    },\n    \"inputTextColor\": {\n      \"normal\": {\n          \"b\": 60,\n          \"g\": 60,\n          \"r\": 60,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 0,\n          \"g\": 0,\n          \"r\": 0,\n          \"alpha\": 0.25\n      },\n      \"selected\": {\n          \"b\": 60,\n          \"g\": 60,\n          \"r\": 60,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 60,\n          \"g\": 60,\n          \"r\": 60,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 60,\n          \"g\": 60,\n          \"r\": 60,\n          \"alpha\": 1\n      }\n    },\n    \"secondaryColor\": {\n      \"normal\": {\n          \"b\": 183,\n          \"g\": 152,\n          \"r\": 20,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 0,\n          \"g\": 0,\n          \"r\": 0,\n          \"alpha\": 0.25\n      },\n      \"selected\": {\n          \"b\": 219,\n          \"g\": 203,\n          \"r\": 139,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 237,\n          \"g\": 229,\n          \"r\": 196,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 219,\n          \"g\": 203,\n          \"r\": 139,\n          \"alpha\": 1\n      }\n    },\n    \"backgroundColor\": {\n      \"normal\": {\n          \"b\": 0,\n          \"g\": 0,\n          \"r\": 0,\n          \"alpha\": 0.9\n      },\n      \"disabled\": {\n          \"b\": 0,\n          \"g\": 0,\n          \"r\": 0,\n          \"alpha\": 0.25\n      },\n      \"selected\": {\n          \"b\": 254,\n          \"g\": 128,\n          \"r\": 1,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 189,\n          \"r\": 126,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 254,\n          \"g\": 128,\n          \"r\": 1,\n          \"alpha\": 1\n      }\n    },\n    \"heroSubtextColor\": {\n      \"normal\": {\n          \"b\": 227,\n          \"g\": 227,\n          \"r\": 227,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 0,\n          \"g\": 0,\n          \"r\": 0,\n          \"alpha\": 0.25\n      },\n      \"selected\": {\n          \"b\": 60,\n          \"g\": 60,\n          \"r\": 60,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 60,\n          \"g\": 60,\n          \"r\": 60,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 60,\n          \"g\": 60,\n          \"r\": 60,\n          \"alpha\": 1\n      }\n    },\n    \"primaryIconColor\": {\n      \"normal\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 0.5\n      },\n      \"selected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 211,\n          \"r\": 170,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      }\n    },\n    \"primaryTextColor\": {\n      \"normal\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 0.5\n      },\n      \"selected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      }\n    },\n    \"secondaryIconColor\": {\n      \"normal\": {\n          \"b\": 253,\n          \"g\": 127,\n          \"r\": 1,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 0.5\n      },\n      \"selected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 211,\n          \"r\": 170,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      }\n    },\n    \"secondaryTextColor\": {\n      \"normal\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 0.5\n      },\n      \"selected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      }\n    },\n    \"backgroundTextColor\": {\n      \"normal\": {\n          \"b\": 227,\n          \"g\": 227,\n          \"r\": 227,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 0.5\n      },\n      \"selected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      }\n    },\n    \"placeholderTextColor\": {\n      \"normal\": {\n          \"b\": 254,\n          \"g\": 128,\n          \"r\": 1,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 0,\n          \"g\": 0,\n          \"r\": 0,\n          \"alpha\": 0.25\n      },\n      \"selected\": {\n          \"b\": 254,\n          \"g\": 128,\n          \"r\": 1,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 254,\n          \"g\": 128,\n          \"r\": 1,\n          \"alpha\": 1\n      }\n    },\n    \"primaryImageTextColor\": {\n      \"normal\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 0.5\n      },\n      \"selected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      }\n    },\n    \"backgroundSubtextColor\": {\n      \"normal\": {\n          \"b\": 254,\n          \"g\": 128,\n          \"r\": 1,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 0.5\n      },\n      \"selected\": {\n          \"b\": 255,\n          \"g\": 189,\n          \"r\": 126,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 255,\n          \"g\": 189,\n          \"r\": 126,\n          \"alpha\": 1\n      }\n    },\n    \"progressIndicatorColor\": {\n      \"normal\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"selected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      }\n    },\n    \"secondaryImageTextColor\": {\n      \"normal\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"disabled\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 0.5\n      },\n      \"selected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlighted\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      },\n      \"highlightedSelected\": {\n          \"b\": 255,\n          \"g\": 255,\n          \"r\": 255,\n          \"alpha\": 1\n      }\n    },\n  },\n  \"mainMenu\": {\n    \"href\": \"https://api-dev.cardola.net/v1/test-hgid/app/main-menu\"\n  },\n  \"href\": \"https://api-dev.cardola.net/v1/test-hgid/app/init\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "2_Initialization",
    "header": {
      "fields": {
        "Global headers": [
          {
            "group": "Global headers",
            "type": "string",
            "optional": true,
            "field": "x-cardola-uuid",
            "description": "<p>The unique ID of the API, fetched from app/registration response.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": " { \"x-cardola-uuid\": \"test-uuid-for-testing\" }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p>Cannot connect to database <a href=\"#error_db-cc-db\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p>Error querying information from database <a href=\"#error_db-query-fails\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p>Hotel group not found (<a href=\"#error_hg-not-found\">Definition</a>)</p> "
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/app/init",
    "title": "2. Get initial data",
    "name": "appInit",
    "version": "0.4.7",
    "group": "2_Initialization",
    "description": "<p>When client application has the UUID, then every time the application is loaded, it must make a call to this service to get the vital information, which it will need to build the UI and provide content to end-user.</p> ",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>The href of current request/resource</p> "
          },
          {
            "group": "Success 200",
            "type": "Language[]",
            "optional": false,
            "field": "supportedLanguages",
            "description": "<p>The list of language objects, which are supported by this hotel group. Elements in this object have a key in the format of ISO 639-1 with region designator</p> "
          },
          {
            "group": "Success 200",
            "type": "Language",
            "optional": false,
            "field": "supportedLanguages.LANGUAGE_CODE",
            "description": "<p>Supported language object (<a href=\"#api-1_Basics-basicsLangs\">About languages</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "defaultLanguage",
            "description": "<p>The default language for the hotel group as ISO 639-1 with region designator</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "translations",
            "description": "<p>Object, which contain translations of the UI phrases. Elements in this object have a key in the format of ISO 639-1 with region designator, and items under each key are translations in this language</p> "
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "translations.LANGUAGE_CODE",
            "description": "<p>An Object containing translations for a language defined by <code>LANGUAGE_CODE</code></p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "translations.LANGUAGE_CODE.KEY",
            "description": "<p>A translated string with a given <code>KEY</code></p> "
          },
          {
            "group": "Success 200",
            "type": "Link",
            "optional": false,
            "field": "signIn",
            "description": "<p>A Link object, which points to a sing in action (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Link",
            "optional": false,
            "field": "signUp",
            "description": "<p>A Link object, which points to a sing up in action (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Link",
            "optional": false,
            "field": "signOut",
            "description": "<p>A Link object, which points to a sing out in action (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Link",
            "optional": false,
            "field": "mainMenu",
            "description": "<p>A Link object, which points to a service, returning main menu structure (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "hotelGroupInfo",
            "description": "<p>A Object containing info about hotel group</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "hotelGroupInfo.localized",
            "description": "<p>A Object containing info in certain language</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "hotelGroupInfo.localized.LANGUAGE",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "hotelGroupInfo.localized.LANGUAGE.name",
            "description": "<p>Hotel name in certain language</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "hotelGroupInfo.localized.LANGUAGE.logo",
            "description": "<p>Hotel logo</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " {\n  \"supportedLanguages\": {\n    \"en_GB\": {\n      \"code\": \"en_GB\",\n      \"englishName\": \"English\",\n      \"nativeName\": \"English\",\n      \"rtl\": false\n    },\n    \"fr_FR\": {\n      \"code\": \"fr_FR\",\n      \"englishName\": \"French\",\n      \"nativeName\": \"Français\",\n      \"rtl\": false\n    },\n    \"et_EE\": {\n      \"code\": \"et_EE\",\n      \"englishName\": \"English\",\n      \"nativeName\": \"Eesti keel\",\n      \"rtl\": false\n    },\n    \"ru_RU\": {\n      \"code\": \"ru_RU\",\n      \"englishName\": \"Russian\",\n      \"nativeName\": \"По русски\",\n      \"rtl\": false\n    },\n    \"ar_SA\": {\n      \"code\": \"ar_SA\",\n      \"englishName\": \"Arabic\",\n      \"nativeName\": \"al-ʿArabiyyah\",\n      \"rtl\": true\n    }\n  },\n  \"defaultLanguage\": \"en_GB\",\n  \"translations\": {\n    \"ar_SA\": {\n      \"skip\": \"تخطى\",\n      \"hello\": \"مرحبا\",\n      \"signIn\": \"تسجيل الدخول\",\n      \"signUp\": \"سجل\",\n      \"language\": \"لغة\",\n      \"enterEmail\": \"من فضلك ادخل بريدك الالكتروني\",\n      \"enterPassword\": \"الرجاء إدخال كلمة المرور\",\n      \"enterPasswordConfirmation\": \"الرجاء إعادة كتابة كلمة المرور الخاصة بك\"\n    },\n    \"en_GB\": {\n      \"skip\": \"Skip\",\n      \"hello\": \"Hello\",\n      \"signIn\": \"Sign in\",\n      \"signUp\": \"Sign up\",\n      \"language\": \"Language\",\n      \"enterEmail\": \"Please enter your email\",\n      \"enterPassword\": \"Please enter your password\",\n      \"enterPasswordConfirmation\": \"Please retype your password\"\n    },\n    \"et_EE\": {\n      \"skip\": \"Mine edasi\",\n      \"hello\": \"Tere\",\n      \"signIn\": \"Logi sisse\",\n      \"signUp\": \"Registreeri\",\n      \"language\": \"Keel\",\n      \"enterEmail\": \"Palun sisesta oma email\",\n      \"enterPassword\": \"Palun sisesta oma parool\",\n      \"enterPasswordConfirmation\": \"Palun korda parooli\"\n    },\n    \"fr_FR\": {\n      \"skip\": \"Sauter\",\n      \"hello\": \"Bonjour\",\n      \"signIn\": \"Connectez-vous\",\n      \"signUp\": \"Inscrivez-vous\",\n      \"language\": \"Langue\",\n      \"enterEmail\": \"Se il vous plaît entrer votre e-mail\",\n      \"enterPassword\": \"Se il vous plaît entrer votre mot de passe\",\n      \"enterPasswordConfirmation\": \"Se il vous plaît retaper votre mot de passe\"\n    },\n    \"ru_RU\": {\n      \"skip\": \"Пропустить\",\n      \"hello\": \"Привет\",\n      \"signIn\": \"войти в систему\",\n      \"signUp\": \"зарегистрироваться\",\n      \"language\": \"Язык\",\n      \"enterEmail\": \"Пожалуйста, введите адрес электронной почты\",\n      \"enterPassword\": \"Пожалуйста, введите пароль\",\n      \"enterPasswordConfirmation\": \"Пожалуйста, еще раз введите пароль\"\n    }\n  },\n  \"hotelGroupInfo\": {\n    \"localized\": {\n      \"en_GB\": {\n        \"name\": \"The Leading Hotels of the World\",\n        \"logo\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo\"\n        }\n      },\n      \"fr_FR\": {\n        \"name\": \"The Leading Hotels of the World\",\n        \"logo\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo\"\n        }\n      },\n      \"et_EE\": {\n        \"name\": \"The Leading Hotels of the World\",\n        \"logo\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo\"\n        }\n      },\n      \"ru_RU\": {\n        \"name\": \"The Leading Hotels of the World\",\n        \"logo\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo\"\n        }\n      },\n      \"ar_SA\": {\n        \"name\": \"The Leading Hotels of the World\",\n        \"logo\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo\"\n        }\n      }\n    }\n  },\n  \"signIn\": {\n    \"href\": \"http://localhost:8000/v1/test-hgid/profiles/signin\",\n    \"method\": \"POST\"\n  },\n  \"signUp\": {\n    \"href\": \"http://localhost:8000/v1/test-hgid/profiles/signup\",\n    \"method\": \"POST\"\n  },\n  \"signOut\": {\n    \"href\": \"http://localhost:8000/v1/test-hgid/profiles/signout\",\n    \"method\": \"GET\"\n  },\n  \"mainMenu\": {\"href\": \"http://localhost:8000/v1/test-hgid/app/main-menu\"},\n  \"href\": \"http://localhost:8000/v1/test-hgid/app/init\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/common/_apidoc.js",
    "groupTitle": "2_Initialization",
    "header": {
      "fields": {
        "Global headers": [
          {
            "group": "Global headers",
            "type": "string",
            "optional": true,
            "field": "x-cardola-uuid",
            "description": "<p>The unique ID of the API, fetched from app/registration response.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": " { \"x-cardola-uuid\": \"test-uuid-for-testing\" }",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/app/main-menu",
    "title": "3. Get the main menu",
    "name": "appMainMenu",
    "version": "1.7.4",
    "group": "2_Initialization",
    "description": "<p>Get the structure of the main menu</p> ",
    "examples": [
      {
        "title": "Example usage:",
        "content": "GET /v1/test-hgid/app/main-menu HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>The href of current request/resource</p> "
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "items",
            "description": "<p>The items of the main menu.</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "items.localized",
            "description": "<p>Localized values of this menu item Elements in this object have a key in the format of ISO 639-1 with region designator.</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "items.localized.LANGUAGE_CODE.name",
            "description": "<p>The menu item name</p> "
          },
          {
            "group": "Success 200",
            "type": "Image",
            "optional": false,
            "field": "items.localized.LANGUAGE_CODE.buttonImage",
            "description": "<p>The menu item image as Image object (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Image",
            "optional": false,
            "field": "items.localized.LANGUAGE_CODE.backgroundImage",
            "description": "<p>The background image for the target page, given as Image object (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Image[]",
            "optional": false,
            "field": "items.localized.LANGUAGE_CODE.slideshowImages",
            "description": "<p>The slideshow images for slideshow page, given as Array of Image objects (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "mainMenu.action",
            "description": "<p>The object defining the item action</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "allowedValues": [
              "navigation",
              "slideshow"
            ],
            "optional": false,
            "field": "mainMenu.action.type",
            "description": "<p>The type of action this item does <code>navigation</code> means in-app navigation, if this item <code>slideshow</code> then it is will be navigate to separate page with slideshow</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mainMenu.action.href",
            "description": "<p>The href, which gives data for the target view</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "allowedValues": [
              "categoryGridView",
              "nearMeMapView",
              "resultListView",
              "slideshow"
            ],
            "optional": false,
            "field": "mainMenu.action.viewType",
            "description": "<p>The view type, which should be rendered if user navigates to this menu item</p> "
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": true,
            "field": "items.children",
            "description": "<p>Children menu items <strong>NOT USED CURRENTLY</strong></p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": true,
            "field": "customItems",
            "description": "<p>Custom items</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": true,
            "field": "customItems.search",
            "description": "<p>Search menu item description (Same structure as object in <code>items</code>)</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Example:",
          "content": " {\n    \"items\": [\n        {\n            \"action\": {\n                \"type\": \"navigation\",\n                \"href\": \"https://localhost:8000/v1/test-hgid/categories?type=regions\",\n                \"viewType\": \"categoryGridView\"\n            },\n            \"children\": [],\n            \"localized\": {\n                \"ar_SA\": {\n                    \"name\": \"المناطق\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"en_GB\": {\n                    \"name\": \"Regions\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"et_EE\": {\n                    \"name\": \"Piirkonnad\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"fr_FR\": {\n                    \"name\": \"Régions\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"ru_RU\": {\n                    \"name\": \"Регионы\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                }\n            }\n        },\n        {\n            \"action\": {\n                \"type\": \"navigation\",\n                \"href\": \"https://localhost:8000/v1/test-hgid/categories?type=tags\",\n                \"viewType\": \"categoryGridView\"\n            },\n            \"children\": [],\n            \"localized\": {\n                \"ar_SA\": {\n                    \"name\": \"تجربة\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"en_GB\": {\n                    \"name\": \"Experience\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"et_EE\": {\n                    \"name\": \"Elamus\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"fr_FR\": {\n                    \"name\": \"Expérience\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"ru_RU\": {\n                    \"name\": \"Oпыт\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                }\n            }\n        },\n        {\n            \"action\": {\n                \"type\": \"navigation\",\n                \"href\": \"https://localhost:8000/v1/test-hgid/categories?type=brands\",\n                \"viewType\": \"categoryGridView\"\n            },\n            \"children\": [],\n            \"localized\": {\n                \"ar_SA\": {\n                    \"name\": \"العلامات التجارية\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"en_GB\": {\n                    \"name\": \"Brands\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"et_EE\": {\n                    \"name\": \"Brändid\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"fr_FR\": {\n                    \"name\": \"Marques\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"ru_RU\": {\n                    \"name\": \"Бренды\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                }\n            }\n        },\n        {\n            \"action\": {\n                \"type\": \"navigation\",\n                \"href\": \"https://localhost:8000/v1/test-hgid/hotels?lat=<latitude>&long=<longitude>&limit=100000\",\n                \"viewType\": \"nearMeMapView\"\n            },\n            \"children\": [],\n            \"localized\": {\n                \"ar_SA\": {\n                    \"name\": \"بالقرب مني\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"en_GB\": {\n                    \"name\": \"Near me\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"et_EE\": {\n                    \"name\": \"Lähedal\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"fr_FR\": {\n                    \"name\": \"Near me\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"ru_RU\": {\n                    \"name\": \"Рядом со мной\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                }\n            }\n        },\n        {\n            \"action\": {\n                \"type\": \"navigation\",\n                \"href\": \"https://localhost:8000/v1/test-hgid/hotels\",\n                \"viewType\": \"resultListView\"\n            },\n            \"children\": [],\n            \"localized\": {\n                \"ar_SA\": {\n                    \"name\": \"عرض كل\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"en_GB\": {\n                    \"name\": \"Show all\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"et_EE\": {\n                    \"name\": \"Näita kõiki\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"fr_FR\": {\n                    \"name\": \"Montrer tous\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"ru_RU\": {\n                    \"name\": \"Показать все\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                }\n            }\n        },\n        {\n            \"menuItemId\": \"mm-ss\",\n            \"action\": {\n                \"type\": \"slideshow\",\n                \"navigation\": {\n                   \"viewType\": \"slideshow\"\n                }\n            },\n            \"children\": [],\n            \"localized\": {\n                \"en_GB\": {\n                   \"name\": \"Slideshow\",\n                   \"slideshowImages\": [{\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }],\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\"\n                    }\n                },\n                \"de_DE\": {\n                   \"name\": \"Diashow\",\n                   \"slideshowImages\": [{\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }],\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\"\n                    }\n                },\n                \"ru_RU\": {\n                   \"name\": \"Cлайд-шоу\",\n                   \"slideshowImages\": [{\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }],\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\"\n                    }\n                },\n                \"et_EE\": {\n                   \"name\": \"Slideshow\",\n                   \"slideshowImages\": [{\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }],\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\"\n                    }\n                },\n                \"ar_SA\": {\n                   \"name\": \"عرض الشرائحة\",\n                   \"slideshowImages\": [{\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }],\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image\"\n                    }\n                }\n            }\n        }\n    ],\n    \"customItems\": {\n        \"search\": {\n            \"action\": {\n                \"type\": \"search\",\n                \"href\": \"https://localhost:8000/v1/test-hgid/hotels?lat=<latitude>&long=<longitude>&search=<search>\",\n                \"viewType\": \"search\"\n            },\n            \"children\": [],\n            \"localized\": {\n                \"en_GB\": {\n                    \"name\": \"Search\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/placeholder\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/placeholder\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                },\n                \"et_EE\": {\n                    \"name\": \"Otsing\",\n                    \"buttonImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/placeholder\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/placeholder\"\n                    },\n                    \"backgroundImage\": {\n                        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg\",\n                        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg\"\n                    }\n                }\n            }\n        }\n    },\n    \"href\": \"https://localhost:8000/v1/test-hgid/app/main-menu\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "2_Initialization",
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p>Cannot connect to database <a href=\"#error_db-cc-db\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p>Error querying information from database <a href=\"#error_db-query-fails\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p>Hotel group not found (<a href=\"#error_hg-not-found\">Definition</a>)</p> "
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/app/registration",
    "title": "3. Get the main menu",
    "name": "appMainMenu",
    "version": "0.0.1",
    "group": "2_Initialization",
    "description": "<p>Get the structure of the main menu</p> ",
    "examples": [
      {
        "title": "Example usage:",
        "content": "GET /v1/test-hgid/app/main-menu HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>The href of current request/resource</p> "
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "items",
            "description": "<p>The items of the main menu.</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "items.localized",
            "description": "<p>Localized values of this menu item Elements in this object have a key in the format of ISO 639-1 with region designator.</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "items.localized.LANGUAGE_CODE.name",
            "description": "<p>The menu item name</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "items.localized.LANGUAGE_CODE.icon",
            "description": "<p>The menu item icon (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "mainMenu.action",
            "description": "<p>The object defining the item action</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "allowedValues": [
              "navigation"
            ],
            "optional": false,
            "field": "mainMenu.action.type",
            "description": "<p>The type of action this item does <code>navigation</code> means in-app navigation</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mainMenu.action.href",
            "description": "<p>The href, which gives data for the target view</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "allowedValues": [
              "categoryGrid",
              "hotelsOnMap",
              "hotelList"
            ],
            "optional": false,
            "field": "mainMenu.action.viewType",
            "description": "<p>The view type, which should be rendered if user navigates to this menu item</p> "
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": true,
            "field": "items.children",
            "description": "<p>Children menu items <strong>NOT USED CURRENTLY</strong></p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Example:",
          "content": "{\n  \"items\": [\n    {\n      \"action\": {\n        \"type\": \"navigation\",\n        \"href\": \"http://localhost:8000/v1/test-hgid/categories?type=regions\",\n        \"viewType\": \"categoryGrid\"\n      },\n      \"children\": [],\n      \"localized\": {\n        \"ar_SA\": {\n          \"name\": \"المناطق\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/globe.png\"\n        },\n        \"en_GB\": {\n          \"name\": \"Regions\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/globe.png\"\n        },\n        \"et_EE\": {\n          \"name\": \"Piirkonnad\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/globe.png\"\n        },\n        \"fr_FR\": {\n          \"name\": \"Régions\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/globe.png\"\n        },\n        \"ru_RU\": {\n          \"name\": \"Регионы\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/globe.png\"\n        }\n      }\n    },\n    {\n      \"action\": {\n        \"type\": \"navigation\",\n        \"href\": \"http://localhost:8000/v1/test-hgid/categories?type=tags\",\n        \"viewType\": \"categoryGrid\"\n      },\n      \"children\": [],\n      \"localized\": {\n        \"ar_SA\": {\n          \"name\": \"أنواع\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/umbrella.png\"\n        },\n        \"en_GB\": {\n          \"name\": \"Types\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/umbrella.png\"\n        },\n        \"et_EE\": {\n          \"name\": \"Tüübid\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/umbrella.png\"\n        },\n        \"fr_FR\": {\n          \"name\": \"Types\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/umbrella.png\"\n        },\n        \"ru_RU\": {\n          \"name\": \"Виды\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/umbrella.png\"\n        }\n      }\n    },\n    {\n      \"action\": {\n        \"type\": \"navigation\",\n        \"href\": \"http://localhost:8000/v1/test-hgid/categories?type=brands\",\n        \"viewType\": \"categoryGrid\"\n      },\n      \"children\": [],\n      \"localized\": {\n        \"ar_SA\": {\n          \"name\": \"العلامات التجارية\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/x.png\"\n        },\n        \"en_GB\": {\n          \"name\": \"Brands\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/x.png\"\n        },\n        \"et_EE\": {\n          \"name\": \"Brändid\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/x.png\"\n        },\n        \"fr_FR\": {\n          \"name\": \"Marques\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/x.png\"\n        },\n        \"ru_RU\": {\n          \"name\": \"Бренды\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/x.png\"\n        }\n      }\n    },\n    {\n      \"action\": {\n        \"type\": \"navigation\",\n        \"href\": \"http://localhost:8000/v1/test-hgid/hotels?lat=<latitude>&long=<longitude>&radius=<radius>\",\n        \"viewType\": \"hotelsOnMap\"\n      },\n      \"children\": [],\n      \"localized\": {\n        \"ar_SA\": {\n          \"name\": \"بالقرب مني\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/location.png\"\n        },\n        \"en_GB\": {\n          \"name\": \"Near me\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/location.png\"\n        },\n        \"et_EE\": {\n          \"name\": \"Lähedal\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/location.png\"\n        },\n        \"fr_FR\": {\n          \"name\": \"Near me\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/location.png\"\n        },\n        \"ru_RU\": {\n          \"name\": \"Рядом со мной\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/location.png\"\n        }\n      }\n    },\n    {\n      \"action\": {\n        \"type\": \"navigation\",\n        \"href\": \"http://localhost:8000/v1/test-hgid/hotels\",\n        \"viewType\": \"hotelList\"\n      },\n      \"children\": [],\n      \"localized\": {\n        \"ar_SA\": {\n          \"name\": \"عرض كل\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/all.png\"\n        },\n        \"en_GB\": {\n          \"name\": \"Show all\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/all.png\"\n        },\n        \"et_EE\": {\n          \"name\": \"Näita kõiki\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/all.png\"\n        },\n        \"fr_FR\": {\n          \"name\": \"Montrer tous\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/all.png\"\n        },\n        \"ru_RU\": {\n          \"name\": \"Показать все\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/all.png\"\n        }\n      }\n    }\n  ],\n  \"href\": \"http://localhost:8000/v1/test-hgid/app/main-menu\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/common/_apidoc.js",
    "groupTitle": "2_Initialization"
  },
  {
    "type": "get",
    "url": "/app/registration",
    "title": "1. Register the APP",
    "name": "appRegistration",
    "version": "1.7.4",
    "group": "2_Initialization",
    "description": "<p>When a client application is setup and launched for the first time, it needs to get a UUID - which it must store locally. UUID is used to identify the application instance, and it must be included in the header of every other REST request as X-Cardola-Uuid.</p> ",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>Unique User Id</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>The href of current request/resource</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Example:",
          "content": "HTTP/1.1 200 OK\n{\n   \"uuid\": \"1234-5678-1234-5678\",\n   \"href\": \"http://api.cardola.com/v1/test-hgid/app/registration\"\n}",
          "type": "html"
        }
      ]
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "2_Initialization"
  },
  {
    "type": "post",
    "url": "/app/registration",
    "title": "1.1 POST Register the APP",
    "name": "appRegistration2",
    "version": "1.7.4",
    "group": "2_Initialization",
    "description": "<p>When a client application is setup and launched for the first time, it needs to get a UUID - which it must store locally. UUID is used to identify the application instance, and it must be included in the header of every other REST request as X-Cardola-Uuid.</p> ",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>Unique User Id</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>The href of current request/resource</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Example:",
          "content": "HTTP/1.1 200 OK\n{\n   \"uuid\": \"1234-5678-1234-5678\",\n   \"href\": \"http://api.cardola.com/v1/test-hgid/app/registration\"\n}",
          "type": "html"
        }
      ]
    },
    "parameter": {
      "fields": {
        "POST body": [
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "manufacturer",
            "description": "<p>Manufacturer name ex: Apple</p> "
          },
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "model",
            "description": "<p>Model name of the device ex: iPhone 3s</p> "
          },
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "os",
            "description": "<p>Operating system ex: iOS 8.0</p> "
          },
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "appVersion",
            "description": "<p>Application version ex: 1.2</p> "
          },
          {
            "group": "POST body",
            "type": "Boolean",
            "optional": false,
            "field": "isTablet",
            "description": "<p>isTablet ex: true</p> "
          },
          {
            "group": "POST body",
            "type": "Array",
            "optional": false,
            "field": "configuredLanguages",
            "description": "<p>Language preferences presented as String in Array</p> "
          },
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "carrierName",
            "description": "<p>carrierName ex: EMT</p> "
          },
          {
            "group": "POST body",
            "type": "Object",
            "optional": false,
            "field": "screenResolution",
            "description": "<p>Resolution of the device, should containt width and height</p> "
          },
          {
            "group": "POST body",
            "type": "Number",
            "optional": false,
            "field": "screenResolution.width",
            "description": "<p>Width of the device display ex: 1024</p> "
          },
          {
            "group": "POST body",
            "type": "Number",
            "optional": false,
            "field": "screenResolution.height",
            "description": "<p>Height of the device display ex: 2048</p> "
          },
          {
            "group": "POST body",
            "type": "Number",
            "optional": false,
            "field": "screenResolution.scale",
            "defaultValue": "1",
            "description": "<p>Scale of the screen</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "com-fields-miss",
            "description": "<p>Missing fields in body (<a href=\"#error_com-fields-miss\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "com-fields-type",
            "description": "<p>Some fields did not match expected type (<a href=\"#error_com-fields-type\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p>Cannot connect to database <a href=\"#error_db-cc-db\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p>Error querying information from database <a href=\"#error_db-query-fails\">Definition</a></p> "
          }
        ]
      }
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "2_Initialization"
  },
  {
    "type": "get",
    "url": "/app/test",
    "title": "4. Test service",
    "name": "appTestrequest",
    "version": "1.7.4",
    "group": "2_Initialization",
    "description": "<p>Test service, to test the integration. It will validate the <code>x-cardola-verify</code> header if it is set. Also, it will return the body of the request, as it understood it.</p> ",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "receivedBody",
            "description": "<p>The parsed body of the request</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "signatureVerified",
            "defaultValue": "true",
            "description": "<p>If x-cardola-verify header was given and it was valid, it is set to true, if invalid, then error is returned.</p> "
          }
        ]
      }
    },
    "filename": "src/modules/common/apidoc.js",
    "groupTitle": "2_Initialization"
  },
  {
    "type": "get",
    "url": "-",
    "title": "1. Category resource",
    "name": "categoryResource",
    "version": "1.7.4",
    "group": "3_Categories",
    "description": "<p>Category resource deals with different <code>type</code>s of categories. In this system we have mostly these category types:</p> <ul> <li>Regions (Europe,Asia,North America ....)</li> <li>Countries (England, Estonia, USA ...)</li> <li>Brands (MC Hotels, Hilton ...)</li> <li>Tags (Romantic vacation, Golf, Luxurious)</li> </ul> <p>Categories can have subcategories, which normally are of same type, but it is not a rule. For example categories of type <code>regions</code> have subcategories of type <code>countries</code></p> ",
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-inv-type",
            "description": "<p>Invalid category type is requested (<a href=\"#error_cat-inv-type\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-cnt-query-error",
            "description": "<p>Error querying category usage information (<a href=\"#error_cat-cnt-query-error\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p>Cannot connect to database <a href=\"#error_db-cc-db\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p>Error querying information from database <a href=\"#error_db-query-fails\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p>Hotel group not found (<a href=\"#error_hg-not-found\">Definition</a>)</p> "
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Example structure",
          "content": " {\n    \"id\": \"tag-cc\",\n    \"type\": \"tags\",\n    \"backgroundImage\": {\n      \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n      \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n    },\n    \"localized\": {\n      \"en_GB\": {\n        \"name\": \"City Centre\",\n        \"buttonImage\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n        }\n      },\n      \"et_EE\": {\n        \"name\": \"Kesklinn\",\n        \"buttonImage\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n        }\n      }\n    },\n    \"hotels\": 5,\n    \"children\": 0,\n    \"hasChildren\": false,\n    \"getHotels\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-cc/hotels\"},\n    \"getChildren\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-cc/subcategories\"},\n    \"brochure\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-cc/hotels?detail=brochure&limit=1&offset=0\"}\n  }",
          "type": "json"
        }
      ],
      "fields": {
        "Basic category structure": [
          {
            "group": "Basic category structure",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>An URL, which points to a service, which returns only this resource, with detail information.</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Link",
            "optional": false,
            "field": "brochure",
            "description": "<p>Link object, which points to a service, which returns hotels in this category one by one (paginated) with full detail information (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Basic category structure",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Identification code of this category</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Image",
            "optional": false,
            "field": "backgroundImage",
            "description": "<p>Image object for background image (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          },
          {
            "group": "Basic category structure",
            "type": "String",
            "allowedValues": [
              "regions",
              "countries",
              "brands",
              "tags"
            ],
            "optional": false,
            "field": "type",
            "description": "<p>Type of this category</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Object",
            "optional": false,
            "field": "localized",
            "description": "<p>Translated information about this category</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Object[]",
            "optional": false,
            "field": "localized.LANGUAGE",
            "description": "<p>Info about this category in language defined by <code>LANGUAGE</code></p> "
          },
          {
            "group": "Basic category structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.name",
            "description": "<p>Name of this category</p> "
          },
          {
            "group": "Basic category structure",
            "type": "String",
            "optional": true,
            "field": "localized.LANGUAGE.displayName",
            "description": "<p>Optional name to display instead of the <code>name</code>. This value is only used and populated for <code>parent</code> category objects</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Image",
            "optional": false,
            "field": "localized.LANGUAGE.buttonImage",
            "description": "<p>Image object for button image (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Number",
            "optional": false,
            "field": "hotels",
            "description": "<p>Number of hotels in this category</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Number",
            "optional": false,
            "field": "subCategories",
            "description": "<p>Number of subcategories</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Link",
            "optional": false,
            "field": "getSubCategories",
            "description": "<p><a href=\"#api-1_Basics-basicsLinks\">Link object</a> object, which points to a resource, which returns list of subcategories</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Link",
            "optional": false,
            "field": "getHotels",
            "description": "<p><a href=\"#api-1_Basics-basicsLinks\">Link object</a> object, which points to a resource, which returns list of hotels in this category or its subcategories.</p> "
          }
        ]
      }
    },
    "filename": "src/modules/categories/apidoc.js",
    "groupTitle": "3_Categories"
  },
  {
    "type": "get",
    "url": "-",
    "title": "1. Category resource",
    "name": "categoryResource",
    "version": "0.0.1",
    "group": "3_Categories",
    "description": "<p>Category resource deals with different <code>type</code>s of categories. In this system we have mostly these category types:</p> <ul> <li>Regions (Europe,Asia,North America ....)</li> <li>Countries (England, Estonia, USA ...)</li> <li>Brands (MC Hotels, Hilton ...)</li> <li>Tags (Romantic vacation, Golf, Luxurious)</li> </ul> <p>Categories can have subcategories, which normally are of same type, but it is not a rule. For example categories of type <code>regions</code> have subcategories of type <code>countries</code></p> ",
    "parameter": {
      "examples": [
        {
          "title": "Example structure",
          "content": " {\n  \"id\": \"reg-eur-est\",\n  \"type\": \"regions\",\n  \"localized\": {\n    \"en_GB\": {\n      \"name\": \"Estonia\",\n      \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/e_oil_paint:100,w_<width>,h_<height>,c_pad/bge.jpg\",\n      \"backgroundImage\": {\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_fill/bge.jpg\",\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_fill/bge.jpg\"\n      }\n    },\n    \"et_EE\": {\n      \"name\": \"Eesti\",\n      \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/e_oil_paint:100,w_<width>,h_<height>,c_pad/bge.jpg\",\n      \"backgroundImage\": {\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_fill/bge.jpg\",\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_fill/bge.jpg\"\n      }\n    }\n  },\n  \"hotels\": 2,\n  \"children\": [],\n  \"getHotels\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/reg-eur-est/hotels\"},\n  \"brochure\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/reg-eur-est/hotels?detail=brochure&limit=1&offset=0\"}\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Basic category structure": [
          {
            "group": "Basic category structure",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>An URL, which points to a service, which returns only this resource, with detail information.</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Link",
            "optional": false,
            "field": "brochure",
            "description": "<p>Link object, which points to a service, which returns hotels in this category one by one (paginated) with full detail information (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Basic category structure",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Identification code of this category</p> "
          },
          {
            "group": "Basic category structure",
            "type": "String",
            "allowedValues": [
              "regions",
              "countries",
              "brands",
              "tags"
            ],
            "optional": false,
            "field": "type",
            "description": "<p>Type of this category</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Object",
            "optional": false,
            "field": "localized",
            "description": "<p>Translated information about this category</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Object[]",
            "optional": false,
            "field": "localized.LANGUAGE",
            "description": "<p>Info about this category in language defined by <code>LANGUAGE</code></p> "
          },
          {
            "group": "Basic category structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.name",
            "description": "<p>Name of this category</p> "
          },
          {
            "group": "Basic category structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.icon",
            "description": "<p>Icon image URL for this category (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Number",
            "optional": false,
            "field": "hotels",
            "description": "<p>Number of hotels in this category</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Number",
            "optional": false,
            "field": "subCategories",
            "description": "<p>Number of subcategories</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Link",
            "optional": false,
            "field": "getSubCategories",
            "description": "<p><a href=\"#api-1_Basics-basicsLinks\">Link object</a> object, which points to a resource, which returns list of subcategories</p> "
          },
          {
            "group": "Basic category structure",
            "type": "Link",
            "optional": false,
            "field": "getHotels",
            "description": "<p><a href=\"#api-1_Basics-basicsLinks\">Link object</a> object, which points to a resource, which returns list of hotels in this category or its subcategories.</p> "
          }
        ]
      }
    },
    "filename": "src/modules/categories/_apidoc.js",
    "groupTitle": "3_Categories"
  },
  {
    "type": "get",
    "url": "/categories",
    "title": "1.1 Get list of categories",
    "name": "getCategories",
    "version": "1.7.4",
    "group": "3_Categories",
    "description": "<p>This method returns a Collection of categories, which have at least one Hotel in it.</p> <p><a href=\"#api-1_Basics-basicsCollections\">About collections</a></p> ",
    "examples": [
      {
        "title": "Get all regions:",
        "content": "GET /v1/test-hgid/en/categories/?type=tags HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      }
    ],
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-inv-type",
            "description": "<p>Invalid category type is requested (<a href=\"#error_cat-inv-type\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-cnt-query-error",
            "description": "<p>Error querying category usage information (<a href=\"#error_cat-cnt-query-error\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p>Cannot connect to database <a href=\"#error_db-cc-db\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p>Error querying information from database <a href=\"#error_db-query-fails\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p>Hotel group not found (<a href=\"#error_hg-not-found\">Definition</a>)</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get all tags response:",
          "content": " {\n  \"href\": \"http://localhost:8000/v1/test-hgid/categories?type=tags\",\n  \"offset\": 0,\n  \"limit\": 10,\n  \"total\": 3,\n  \"items\": [\n    {\n      \"id\": \"tag-cc\",\n      \"type\": \"tags\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"City Centre\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"Kesklinn\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 5,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-cc/hotels\"},\n      \"getChildren\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-cc/subcategories\"},\n      \"brochure\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-cc/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"id\": \"tag-rus\",\n      \"type\": \"tags\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Russian\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"Vene\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 1,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-rus/hotels\"},\n      \"getChildren\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-rus/subcategories\"},\n      \"brochure\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-rus/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"id\": \"tag-his\",\n      \"type\": \"tags\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Historical\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"Ajalooline\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 2,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-his/hotels\"},\n      \"getChildren\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-his/subcategories\"},\n      \"brochure\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/tag-his/hotels?detail=brochure&limit=1&offset=0\"}\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/categories/apidoc.js",
    "groupTitle": "3_Categories",
    "parameter": {
      "fields": {
        "Category collection query parameters": [
          {
            "group": "Category collection query parameters",
            "type": "String",
            "allowedValues": [
              "region",
              "country",
              "brand"
            ],
            "optional": false,
            "field": "type",
            "description": "<p>Specifies the type of categories to return.</p> "
          }
        ],
        "Collection query parameters": [
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>A limit for maximum number of items returned in one response</p> "
          },
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>An offset for pagination, if set, then response will skip the first <code>N</code> values.</p> "
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/categories/:categoryId/subcategories",
    "title": "1.2 Get list of subcategories",
    "name": "getSubCategories",
    "version": "1.7.4",
    "group": "3_Categories",
    "description": "<p>This method returns a Collection of sub-categories of a category, which have at least one Hotel in it.</p> <p><a href=\"#api-1_Basics-basicsCollections\">About collections</a></p> ",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryId",
            "description": "<p>Category Id, which subcategories are required</p> "
          }
        ],
        "Collection query parameters": [
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>A limit for maximum number of items returned in one response</p> "
          },
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>An offset for pagination, if set, then response will skip the first <code>N</code> values.</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Get all subcategories (countries) under Europe:",
        "content": "GET /v1/test-hgid/en/categories/reg-eur/subcategories HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      }
    ],
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-inv-type",
            "description": "<p>Invalid category type is requested (<a href=\"#error_cat-inv-type\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "cat-cnt-query-error",
            "description": "<p>Error querying category usage information (<a href=\"#error_cat-cnt-query-error\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p>Cannot connect to database <a href=\"#error_db-cc-db\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p>Error querying information from database <a href=\"#error_db-query-fails\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p>Hotel group not found (<a href=\"#error_hg-not-found\">Definition</a>)</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get subcategories example response:",
          "content": " {\n  \"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur/subcategories\",\n  \"offset\": 0,\n  \"limit\": 10,\n  \"total\": 2,\n  \"items\": [\n    {\n      \"id\": \"reg-eur-est\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-est/bg\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-est/bg\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Estonia\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"Eesti\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 2,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-est/hotels\"},\n      \"getChildren\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-est/subcategories\"},\n      \"brochure\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-est/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"id\": \"reg-eur-uk\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-uk/bg\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-uk/bg\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"United Kingdom\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"Suurbritannia\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 0,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-uk/hotels\"},\n      \"getChildren\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-uk/subcategories\"},\n      \"brochure\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-uk/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"id\": \"reg-eur-it\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-it/bg\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-it/bg\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Italy\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"Itaalia\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 0,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-it/hotels\"},\n      \"getChildren\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-it/subcategories\"},\n      \"brochure\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-it/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"id\": \"reg-eur-sw\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-sw/bg\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-sw/bg\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Sweden\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"fr_FR\": {\n          \"name\": \"Suède\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 0,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sw/hotels\"},\n      \"getChildren\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sw/subcategories\"},\n      \"brochure\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sw/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"id\": \"reg-eur-nor\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-nor/bg\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-nor/bg\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Norway\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"fr_FR\": {\n          \"name\": \"Norvège\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 0,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-nor/hotels\"},\n      \"getChildren\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-nor/subcategories\"},\n      \"brochure\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-nor/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"id\": \"reg-eur-fin\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-fin/bg\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-fin/bg\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Finland\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"Soome\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 0,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fin/hotels\"},\n      \"getChildren\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fin/subcategories\"},\n      \"brochure\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fin/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"id\": \"reg-eur-sp\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-sp/bg\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-sp/bg\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Spain\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"Hispaania\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 0,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sp/hotels\"},\n      \"getChildren\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sp/subcategories\"},\n      \"brochure\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sp/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"id\": \"reg-eur-fr\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-fr/bg\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-fr/bg\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"France\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"fr_FR\": {\n          \"name\": \"France\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 0,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fr/hotels\"},\n      \"getChildren\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fr/subcategories\"},\n      \"brochure\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fr/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"id\": \"reg-eur-aus\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-aus/bg\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-aus/bg\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Austria\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"fr_FR\": {\n          \"name\": \"Autriche\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 0,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-aus/hotels\"},\n      \"getChildren\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-aus/subcategories\"},\n      \"brochure\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-aus/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"id\": \"reg-eur-ger\",\n      \"backgroundImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-ger/bg\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-ger/bg\"\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Germany\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"Saksamaa\",\n          \"buttonImage\": {\n            \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n            \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n          }\n        }\n      },\n      \"hotels\": 4,\n      \"children\": 0,\n      \"hasChildren\": false,\n      \"getHotels\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-ger/hotels\"},\n      \"getChildren\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-ger/subcategories\"},\n      \"brochure\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-ger/hotels?detail=brochure&limit=1&offset=0\"}\n    }\n  ],\n  \"parent\": {\n    \"id\": \"reg-eur\",\n    \"type\": \"regions\",\n    \"backgroundImage\": {\n      \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur/bg\",\n      \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur/bg\"\n    },\n    \"localized\": {\n      \"en_GB\": {\n        \"name\": \"Europe\",\n        \"buttonImage\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n        },\n        \"displayName\": \"Show all\"\n      },\n      \"et_EE\": {\n        \"name\": \"Euroopa\",\n        \"buttonImage\": {\n          \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\",\n          \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder\"\n        },\n        \"displayName\": \"Näita kõiki\"\n      }\n    },\n    \"children\": 0,\n    \"hasChildren\": false,\n    \"getHotels\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur/hotels\"},\n    \"getChildren\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur/subcategories\"},\n    \"brochure\": {\"href\": \"http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur/hotels?detail=brochure&limit=1&offset=0\"}\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/categories/apidoc.js",
    "groupTitle": "3_Categories"
  },
  {
    "type": "get",
    "url": "/categories/:categoryId/hotels",
    "title": "1.3 Get hotels in a category",
    "name": "getCategoryHotels",
    "version": "1.7.4",
    "group": "4_Hotels",
    "description": "<p>This method returns a Collection of Hotel objects, which belong to given category.</p> <p><a href=\"#api-1_Basics-basicsCollections\">About collections</a></p> ",
    "examples": [
      {
        "title": "Get category hotels request example:",
        "content": "GET /v1/test-hgid/en/categories/2/hotels HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      }
    ],
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-src-err",
            "description": "<p>Error searching hotels (<a href=\"#error_hs-src-err\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-for",
            "description": "<p>Error formatting hotel (<a href=\"#error_hs-err-for\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-src",
            "description": "<p>Error searching (<a href=\"#error_sr-err-src\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p>Cannot connect to database <a href=\"#error_db-cc-db\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p>Error querying information from database <a href=\"#error_db-query-fails\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p>Hotel group not found (<a href=\"#error_hg-not-found\">Definition</a>)</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get category hotels response example:",
          "content": " {\n  \"href\": \"http://localhost:8000/v1/test-hgid/categories/reg-eur/hotels\",\n  \"offset\": 0,\n  \"limit\": 10,\n  \"total\": 1,\n  \"items\": [\n    {\n\t    \"href\": \"http://localhost:8000/v1/test-hgid/hotels/ernst\",\n\t    \"id\": \"ernst\",\n\t    \"coordinates\": {\n\t        \"lat\": 50.941857,\n\t        \"long\": 6.956522\n\t    },\n\t    \"localized\": {\n\t        \"en_GB\": {\n\t            \"name\": \"Excelsior Hotel Ernst\",\n\t            \"location\": {\n\t                \"city\": \"Cologne\",\n\t                \"country\": \"Germany\",\n\t                \"fullAddress\": \"Domplatz/Trankgasse 1-5, 50667 Cologne, Germany\",\n\t                \"shortAddress\": \"Alstadt-Nord, Cologne, Germany\"\n\t            },\n\t            \"buttonImage\": {\n\t                \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image\",\n\t                \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image\"\n\t            }\n\t        }\n\t    }\n\t\t}\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/hotels/apidoc.js",
    "groupTitle": "4_Hotels",
    "parameter": {
      "fields": {
        "Collection query parameters": [
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>A limit for maximum number of items returned in one response</p> "
          },
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>An offset for pagination, if set, then response will skip the first <code>N</code> values.</p> "
          }
        ],
        "Hotel collection query parameters": [
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "-180 - 180",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude of a location, near which the hotel should be located. <strong>NB! Should be used along with <code>long</code> and <code>radius</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "-180 - 180",
            "optional": false,
            "field": "long",
            "description": "<p>Longitude of a location, near which the hotel should be located. <strong>NB! Should be used along with <code>lat</code> and <code>radius</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "120 - 637100",
            "optional": false,
            "field": "radius",
            "defaultValue": "10000",
            "description": "<p>The radius in meters, inside which the hotels should be located (maximum distance from given coordinate - <code>lat</code> &amp; <code>long</code>). 120 is the least effective radius, which works, values under it match with nothing <strong>NB! Should be used along with <code>lat</code> and <code>long</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "String",
            "optional": false,
            "field": "search",
            "description": "<p>A search phrase. If given, then a search is performed, and only hotels, which match the <code>search phrase</code>, will be returned. The string passed with this parameter must be url encoded and in UTF-8 encoding. (About urlencode)[<a href=\"http://www.w3schools.com/tags/ref_urlencode.asp\">http://www.w3schools.com/tags/ref_urlencode.asp</a>]</p> "
          }
        ],
        "Hotel query parameters": [
          {
            "group": "Hotel query parameters",
            "type": "String",
            "allowedValues": [
              "full",
              "brochure"
            ],
            "optional": true,
            "field": "detail",
            "description": "<p>Specifies the amount of detail, which should be returned for each hotel.</p> <p>If this parameter is given, with a value of <code>full</code> (<strong>not available yet</strong>), then Hotel object(s) will be returned in full detail.</p> <p>If this parameter is given, with a value of <code>brochure</code>, then Hotel object(s) will be returned with details, which are needed for displaying a brochure.</p> <p>If this parameter is omitted then only basic information is returned about hotel.</p> "
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/categories/:categoryId/hotels",
    "title": "1.3 Get hotels in a category",
    "name": "getCategoryHotels",
    "version": "0.0.1",
    "group": "4_Hotels",
    "description": "<p>This method returns a Collection of Hotel objects, which belong to given category.</p> <p><a href=\"#api-1_Basics-basicsCollections\">About collections</a></p> ",
    "examples": [
      {
        "title": "Get category hotels request example:",
        "content": "GET /v1/test-hgid/en/categories/2/hotels HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Get category hotels response example:",
          "content": " {\n  \"href\": \"http://localhost:8000/v1/test-hgid/categories/reg-eur/hotels\",\n  \"offset\": 0,\n  \"limit\": 10,\n  \"total\": 2,\n  \"items\": [\n    {\n      \"href\": \"http://localhost:8000/v1/test-hgid/hotels/1\",\n      \"id\": \"1\",\n      \"coordinates\": {\n        \"lat\": 59.438591,\n        \"long\": 24.747744\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Schlossle\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.png\",\n          \"location\": {\n            \"city\": \"Tallinn\",\n            \"country\": \"Estonia\",\n            \"fullAddress\": \"Pühavaimu 13/15, 10123 Tallinn, Estonia\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"Schlössle\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.jpg\",\n          \"location\": {\n            \"city\": \"Tallinn\",\n            \"country\": \"Eesti\",\n            \"fullAddress\": \"Pühavaimu 13/15, 10123 Tallinn, Eesti\"\n          }\n        }\n      },\n      \"brochure\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/reg-eur/hotels?detail=brochure&limit=1&offset=0\"}\n    },\n    {\n      \"href\": \"http://localhost:8000/v1/test-hgid/hotels/2\",\n      \"id\": \"2\",\n      \"coordinates\": {\n        \"lat\": 59.436968,\n        \"long\": 24.742877\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"St Petersbourg\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/stpeter_logo.png\",\n          \"location\": {\n            \"city\": \"Tallinn\",\n            \"country\": \"Estonia\",\n            \"fullAddress\": \"Rataskaevu 7, 10123 Tallinn, Estonia\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"St Petersbourg\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/stpeter_logo.png\",\n          \"location\": {\n            \"city\": \"Tallinn\",\n            \"country\": \"Eesti\",\n            \"fullAddress\": \"Rataskaevu 7, 10123 Tallinn, Eesti\"\n          }\n        }\n      },\n      \"brochure\": {\"href\": \"http://localhost:8000/v1/test-hgid/categories/reg-eur/hotels?detail=brochure&limit=1&offset=1\"}\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/hotels/_apidoc.js",
    "groupTitle": "4_Hotels",
    "parameter": {
      "fields": {
        "Collection query parameters": [
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>A limit for maximum number of items returned in one response</p> "
          },
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>An offset for pagination, if set, then response will skip the first <code>N</code> values.</p> "
          }
        ],
        "Hotel collection query parameters": [
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "-180 - 180",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude of a location, near which the hotel should be located. <strong>NB! Should be used along with <code>long</code> and <code>radius</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "-180 - 180",
            "optional": false,
            "field": "long",
            "description": "<p>Longitude of a location, near which the hotel should be located. <strong>NB! Should be used along with <code>lat</code> and <code>radius</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "0 - 6371",
            "optional": false,
            "field": "radius",
            "defaultValue": "10",
            "description": "<p>The radius in KM, inside which the hotels should be located (maximum distance from given coordinate - <code>lat</code> &amp; <code>long</code>). <strong>NB! Should be used along with <code>lat</code> and <code>long</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "String",
            "optional": false,
            "field": "search",
            "description": "<p>A search phrase. If given, then a search is performed, and only hotels, which match the <code>search phrase</code>, will be returned. The string passed with this parameter must be url encoded and in UTF-8 encoding. (About urlencode)[<a href=\"http://www.w3schools.com/tags/ref_urlencode.asp\">http://www.w3schools.com/tags/ref_urlencode.asp</a>]</p> "
          }
        ],
        "Hotel query parameters": [
          {
            "group": "Hotel query parameters",
            "type": "String",
            "allowedValues": [
              "full",
              "brochure"
            ],
            "optional": true,
            "field": "detail",
            "description": "<p>Specifies the amount of detail, which should be returned for each hotel.</p> <p>If this parameter is given, with a value of <code>full</code> (<strong>not available yet</strong>), then Hotel object(s) will be returned in full detail.</p> <p>If this parameter is given, with a value of <code>brochure</code>, then Hotel object(s) will be returned with details, which are needed for displaying a brochure.</p> <p>If this parameter is omitted then only basic information is returned about hotel.</p> "
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/hotels/:hotelId",
    "title": "1.2 Get a hotel",
    "name": "getHotel",
    "version": "1.7.4",
    "group": "4_Hotels",
    "description": "<p>This method returns a single Hotel, which has the given id.</p> ",
    "examples": [
      {
        "title": "Get a hotel (with 'brochure' details) request example:",
        "content": "GET /v1/test-hgid/en/hotels/33?detail=brochure HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      }
    ],
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-hotel-nf",
            "description": "<p>Hotel, with given parameters was not found (<a href=\"#error_hs-hotel-nf\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p>Cannot connect to database <a href=\"#error_db-cc-db\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p>Error querying information from database <a href=\"#error_db-query-fails\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p>Hotel group not found (<a href=\"#error_hg-not-found\">Definition</a>)</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get a hotel (with 'brochure' details) response example:",
          "content": "{\n\"href\": \"http://localhost:8000/v1/test-hgid/hotels/ernst\",\n\"id\": \"ernst\",\n\"coordinates\": {\n\t\"lat\": 50.941857,\n\t\"long\": 6.956522\n  },\n  \"localized\": {\n\t\"en_GB\": {\n\t  \"name\": \"Excelsior Hotel Ernst\",\n\t  \"location\": {\n\t\t\"city\": \"Cologne\",\n\t\t\"country\": \"Germany\",\n\t\t\"fullAddress\": \"Domplatz/Trankgasse 1-5, 50667 Cologne, Germany\",\n\t\t\"shortAddress\": \"Alstadt-Nord, Cologne, Germany\"\n\t  },\n\t  \"buttonImage\": {\n\t\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image\",\n\t\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image\"\n\t  },\n\t  \"contacts\": {\n\t\t\"primary\": {\n\t\t  \"email\": \" info@excelsior-hotel-ernst.de\",\n \"phone\": \"+49 (0)221 270 1\",\n \"website\": \"http://www.excelsiorhotelernst.com/en\"\n }\n },\n \"bookingUrl\": null,\n \"description\": [\n {\n\t\"text\": [\n\t\t{\n\t\t\t\"style\": null,\n\t\t\t\"value\": \"The Excelsior Hotel Ernst is Cologne’s Grand Hotel next to the Cathedral. We provide individualized services to respond to the uniqueness of every guest and employee. We maintain the tradition of hospitality and remain modern by applying innovation. As Grand Hotel in the heart of Cologne, we actively participate in urban life and we are committed to art and culture. Your individuality is our Excellence.\"\n\t\t}\n\t],\n\t\"heading\": [\n\t\t{\n\t\t\t\"style\": null,\n\t\t\t\"value\": \"About us\"\n\t\t}\n\t]\n},\n {\n\t\"text\": [\n\t\t{\n\t\t\t\"style\": null,\n\t\t\t\"value\": \"Carl Ernst, Royal Restorer of the central station, was builder and owner of the Hotel Ernst in the city center. The opening ceremony took place on May 16th 1863. In 1871, after as little as eight years, he sold the Hotel Ernst to Friedrich Kracht. Friedrich Kracht moved from Belgium to Cologne to manage the hotel but he died four years later. His wife and his son Carl took over the management of the house.  In those days, the Grand Hotel was already the first choice of prominent guests.\"\n\t\t}\n\t],\n\t\"heading\": [\n\t\t{\n\t\t\t\"style\": null,\n\t\t\t\"value\": \"History\"\n\t\t}\n\t]\n}\n ],\n \"gallery\": [\n {\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1\",\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1\"\n},\n {\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0\",\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0\"\n}\n ]\n }\n },\n \"menu\": [\n {\n    \"action\": \"web\",\n    \"isEnabled\": true,\n    \"data\": {\n        \"url\": \"http://www.corinthia.com/hotels/london/\"\n    }\n},\n {\n    \"action\": \"phone\",\n    \"isEnabled\": true,\n    \"data\": {\n        \"phone\": \"+372 628 6500\"\n    }\n},\n {\n    \"action\": \"map\",\n    \"isEnabled\": true,\n    \"data\": {\n        \"lat\": 51.506551,\n        \"long\": -0.124039\n    }\n},\n {\n    \"action\": \"share\",\n    \"isEnabled\": true,\n    \"data\": {\n        \"name\": \"Corinthia Hotel London\",\n        \"gallery\": [\n            {\n                \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1\",\n                \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1\"\n            },\n            {\n                \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0\",\n                \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0\"\n            }\n        ]\n    }\n},\n {\n    \"action\": \"book\",\n    \"isEnabled\": true,\n    \"data\": {\n        \"url\": \"https://booking.slh.com/en-GB/Room/Availability/?HotelCode=HUTLLTE&StartDate=<StartDate>&NoOfNights=<NoOfNights>&NoOfAdults=<NoOfAdults>\",\n        \"type\": \"url\"\n    }\n}\n ],\n \"vhSiteKey\": \"devtallinn\",\n \"gaTrackingId\": \"UA-XXXXX-XX\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/hotels/apidoc.js",
    "groupTitle": "4_Hotels",
    "parameter": {
      "fields": {
        "Hotel query parameters": [
          {
            "group": "Hotel query parameters",
            "type": "String",
            "allowedValues": [
              "full",
              "brochure"
            ],
            "optional": true,
            "field": "detail",
            "description": "<p>Specifies the amount of detail, which should be returned for each hotel.</p> <p>If this parameter is given, with a value of <code>full</code> (<strong>not available yet</strong>), then Hotel object(s) will be returned in full detail.</p> <p>If this parameter is given, with a value of <code>brochure</code>, then Hotel object(s) will be returned with details, which are needed for displaying a brochure.</p> <p>If this parameter is omitted then only basic information is returned about hotel.</p> "
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/hotels/:hotelId",
    "title": "1.2 Get a hotel",
    "name": "getHotel",
    "version": "0.0.1",
    "group": "4_Hotels",
    "description": "<p>This method returns a single Hotel, which has the given id.</p> ",
    "examples": [
      {
        "title": "Get a hotel (with 'brochure' details) request example:",
        "content": "GET /v1/test-hgid/en/hotels/33?detail=brochure HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Get a hotel (with 'brochure' details) response example:",
          "content": " {\n  \"href\": \"http://localhost:8000/v1/test-hgid/hotels/1\",\n  \"id\": \"1\",\n  \"coordinates\": {\n    \"lat\": 59.438591,\n    \"long\": 24.747744\n  },\n  \"localized\": {\n    \"en_GB\": {\n      \"name\": \"Schlossle\",\n      \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.png\",\n      \"location\": {\n        \"city\": \"Tallinn\",\n        \"country\": \"Estonia\",\n        \"fullAddress\": \"Pühavaimu 13/15, 10123 Tallinn, Estonia\"\n      },\n      \"stars\": 5,\n      \"contacts\": {\n        \"primary\": {\n          \"email\": \"sch@schlossle-hotels.com\",\n \"phone\": \"+372 699 7700\",\n \"website\": \"http://www.schloesslehotel.com\"\n }\n },\n \"description\": [\n {\n\t\"text\": [\n\t\t{\n\t\t\t\"styles\": [\"predefinedStyle1\"],\n\t\t\t\"inLineImages\": []\n\t\t},\n\t\t\"Tallinn\",\n\t\t\"in the 13th century was a thriving Hanseatic trading centre where merchants and nobles from all across Europe came to do business . This fortified medieval city was one of the most famous ports in the eastern world, home for goods brought from ships that sailed from as far away as Africa, and by horse and carriage from both east and west . Tallinn was and still is a trading city, and many of the old medieval buildings are a continuing tribute to that legacy \"\n\t],\n\t\"heading\": [\"History\"]\n},\n {\n\t\"text\": [\n\t\t{\n\t\t\t\"link\": \"http://www.schloesslehotel.com\",\n\t\t\t\"styles\": [\"predefinedStyle2\"]\n\t\t},\n\t\t\"The Schlössle Hotel\",\n\t\t\"is a luxurious 5-star hotel in Tallinn’s Old Town. The 23-room boutique hotel has luxurious furnishings and attentive staff to ensure that you will have a memorable stay in this charming hotel . Nestled in the heart of Tallinn ’ s Old Town, the Schlössle is a delightful boutique hotel that will take you back to Estonia ’ s medieval past . \"\n\t],\n\t\"heading\": [\"Services\"]\n}\n ],\n \"images\": [\n {\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle.jpg\",\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle.jpg\"\n},\n {\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle2.jpg\",\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle2.jpg\"\n}\n ]\n },\n \"et_EE\": {\n      \"name\": \"Schlössle\",\n      \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.jpg\",\n      \"location\": {\n        \"city\": \"Tallinn\",\n        \"country\": \"Eesti\",\n        \"fullAddress\": \"Pühavaimu 13/15, 10123 Tallinn, Eesti\"\n      },\n      \"stars\": 5,\n      \"contacts\": {\n        \"primary\": {\n          \"email\": \"sch@schlossle-hotels.com\",\n \"phone\": \"+372 699 7700\",\n \"website\": \"http://www.schloesslehotel.com/et/\"\n }\n },\n \"description\": [\n {\n\t\"text\": [\n\t\t{\n\t\t\t\"link\": \"http://www.schloesslehotel.com/et\",\n\t\t\t\"styles\": [\"predefinedStyle2\"]\n\t\t},\n\t\t\"Hotel Schlössle\",\n\t\t\" on luksuslik 5-e tärni hotell Tallinna vanalinnas Pühavaimu tänaval. Hotellis on 23 luksuslikult sisustatud hotellituba, hotelli tähelepanelik personal teeb kõik et viibimine hotellis oleks külalistele igati meeldiv.\"\n\t],\n\t\"heading\": [\"Teenused\"]\n}\n ],\n \"images\": [\n {\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle.jpg\",\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle.jpg\"\n},\n {\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle2.jpg\",\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle2.jpg\"\n}\n ]\n }\n }\n }",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/hotels/_apidoc.js",
    "groupTitle": "4_Hotels",
    "parameter": {
      "fields": {
        "Hotel query parameters": [
          {
            "group": "Hotel query parameters",
            "type": "String",
            "allowedValues": [
              "full",
              "brochure"
            ],
            "optional": true,
            "field": "detail",
            "description": "<p>Specifies the amount of detail, which should be returned for each hotel.</p> <p>If this parameter is given, with a value of <code>full</code> (<strong>not available yet</strong>), then Hotel object(s) will be returned in full detail.</p> <p>If this parameter is given, with a value of <code>brochure</code>, then Hotel object(s) will be returned with details, which are needed for displaying a brochure.</p> <p>If this parameter is omitted then only basic information is returned about hotel.</p> "
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/hotels/:hotelId/contacts",
    "title": "1.2 Get hotel contact details",
    "name": "getHotelContacts",
    "version": "1.7.4",
    "group": "4_Hotels",
    "description": "<p>Returns hotel contact information, which is customized,  based on the location of the client (detected from IP address)</p> ",
    "examples": [
      {
        "title": "Get hotel contacts details request example:",
        "content": "GET /v1/test-hgid/en/hotel/berna/contacts HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321",
        "type": "html"
      }
    ],
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-ip",
            "description": "<p>Invalid request ip address (<a href=\"#error_hs-err-ip\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p>Cannot connect to database <a href=\"#error_db-cc-db\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p>Error querying information from database <a href=\"#error_db-query-fails\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p>Hotel group not found (<a href=\"#error_hg-not-found\">Definition</a>)</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get a hotel contact details response example:",
          "content": "{\n    \"localized\": {\n        \"en_GB\": {\n            \"primary\": {\n                \"email\": \" info@excelsior-hotel-ernst.de\",\n                \"phone\": \"+49 (0)221 270 1\",\n                \"website\": \"http://www.excelsiorhotelernst.com/en\"\n            }\n        }\n    },\n    \"callCenterNumbers\": {\n      \"defaultNumber\": \"+33 33 444\",\n\t    \"continents\": {\n\t      \"Asia\": \"+13 44 22\",\n\t      \"Europe\": \"+23 44 33\"\n\t    },\n\t    \"countries\": {\n\t      \"Australia\": \"1 800 333 4444\",\n\t      \"United Kingdom\": \"0800 3333 222\",\n\t      \"United States\": \"1-877-666-3332\"\n\t    },\n\t    \"cities\": {\n\t      \"Moscow\": \"8 6621111\"\n\t    }\n    }\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/hotels/apidoc.js",
    "groupTitle": "4_Hotels"
  },
  {
    "type": "get",
    "url": "/hotels/",
    "title": "1.1 Get all hotels",
    "name": "getHotels",
    "version": "1.7.4",
    "group": "4_Hotels",
    "description": "<p>This method returns a Collection of <strong>all</strong> Hotel objects.</p> <p><a href=\"#api-1_Basics-basicsCollections\">About collections</a></p> ",
    "examples": [
      {
        "title": "Get all hotels request example:",
        "content": "GET /v1/test-hgid/en/hotels HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      },
      {
        "title": "Get all hotels near a location:",
        "content": "GET /v1/test-hgid/en/hotels?lat=59.438575&long=24.747423 HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      },
      {
        "title": "Search hotels with Luxury rooms:",
        "content": "GET /v1/test-hgid/en/hotels?search=luxury%20rooms HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      }
    ],
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-src-err",
            "description": "<p>Error searching hotels (<a href=\"#error_hs-src-err\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-for",
            "description": "<p>Error formatting hotel (<a href=\"#error_hs-err-for\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-src",
            "description": "<p>Error searching (<a href=\"#error_sr-err-src\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p>Cannot connect to database <a href=\"#error_db-cc-db\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p>Error querying information from database <a href=\"#error_db-query-fails\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p>Hotel group not found (<a href=\"#error_hg-not-found\">Definition</a>)</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Get all hotels response example:",
          "content": " {\n  \"href\": \"http://localhost:8000/v1/test-hgid/hotels\",\n  \"offset\": 0,\n  \"limit\": 10,\n  \"total\": 2,\n  \"items\": [\n    {\n\t    \"href\": \"http://localhost:8000/v1/test-hgid/hotels/ernst\",\n\t    \"id\": \"ernst\",\n\t    \"coordinates\": {\n\t        \"lat\": 50.941857,\n\t        \"long\": 6.956522\n\t    },\n\t    \"localized\": {\n\t        \"en_GB\": {\n\t            \"name\": \"Excelsior Hotel Ernst\",\n\t            \"location\": {\n\t                \"city\": \"Cologne\",\n\t                \"country\": \"Germany\",\n\t                \"fullAddress\": \"Domplatz/Trankgasse 1-5, 50667 Cologne, Germany\",\n\t                \"shortAddress\": \"Alstadt-Nord, Cologne, Germany\"\n\t            },\n\t            \"buttonImage\": {\n\t                \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image\",\n\t                \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image\"\n\t            }\n\t        }\n\t    }\n\t\t}\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/hotels/apidoc.js",
    "groupTitle": "4_Hotels",
    "parameter": {
      "fields": {
        "Collection query parameters": [
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>A limit for maximum number of items returned in one response</p> "
          },
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>An offset for pagination, if set, then response will skip the first <code>N</code> values.</p> "
          }
        ],
        "Hotel collection query parameters": [
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "-180 - 180",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude of a location, near which the hotel should be located. <strong>NB! Should be used along with <code>long</code> and <code>radius</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "-180 - 180",
            "optional": false,
            "field": "long",
            "description": "<p>Longitude of a location, near which the hotel should be located. <strong>NB! Should be used along with <code>lat</code> and <code>radius</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "120 - 637100",
            "optional": false,
            "field": "radius",
            "defaultValue": "10000",
            "description": "<p>The radius in meters, inside which the hotels should be located (maximum distance from given coordinate - <code>lat</code> &amp; <code>long</code>). 120 is the least effective radius, which works, values under it match with nothing <strong>NB! Should be used along with <code>lat</code> and <code>long</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "String",
            "optional": false,
            "field": "search",
            "description": "<p>A search phrase. If given, then a search is performed, and only hotels, which match the <code>search phrase</code>, will be returned. The string passed with this parameter must be url encoded and in UTF-8 encoding. (About urlencode)[<a href=\"http://www.w3schools.com/tags/ref_urlencode.asp\">http://www.w3schools.com/tags/ref_urlencode.asp</a>]</p> "
          }
        ],
        "Hotel query parameters": [
          {
            "group": "Hotel query parameters",
            "type": "String",
            "allowedValues": [
              "full",
              "brochure"
            ],
            "optional": true,
            "field": "detail",
            "description": "<p>Specifies the amount of detail, which should be returned for each hotel.</p> <p>If this parameter is given, with a value of <code>full</code> (<strong>not available yet</strong>), then Hotel object(s) will be returned in full detail.</p> <p>If this parameter is given, with a value of <code>brochure</code>, then Hotel object(s) will be returned with details, which are needed for displaying a brochure.</p> <p>If this parameter is omitted then only basic information is returned about hotel.</p> "
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/hotels/",
    "title": "1.1 Get all hotels",
    "name": "getHotels",
    "version": "0.0.1",
    "group": "4_Hotels",
    "description": "<p>This method returns a Collection of <strong>all</strong> Hotel objects.</p> <p><a href=\"#api-1_Basics-basicsCollections\">About collections</a></p> ",
    "examples": [
      {
        "title": "Get all hotels request example:",
        "content": "GET /v1/test-hgid/en/hotels HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      },
      {
        "title": "Get all hotels near a location:",
        "content": "GET /v1/test-hgid/en/hotels?lat=59.438575&long=24.747423 HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      },
      {
        "title": "Search hotels with Luxury rooms:",
        "content": "GET /v1/test-hgid/en/hotels?search=luxury%20rooms HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "html"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Get all hotels response example:",
          "content": " {\n  \"href\": \"http://localhost:8000/v1/test-hgid/hotels\",\n  \"offset\": 0,\n  \"limit\": 10,\n  \"total\": 2,\n  \"items\": [\n    {\n      \"href\": \"http://localhost:8000/v1/test-hgid/hotels/1\",\n      \"id\": \"1\",\n      \"coordinates\": {\n        \"lat\": 59.438591,\n        \"long\": 24.747744\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"Schlossle\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.png\",\n          \"location\": {\n            \"city\": \"Tallinn\",\n            \"country\": \"Estonia\",\n            \"fullAddress\": \"Pühavaimu 13/15, 10123 Tallinn, Estonia\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"Schlössle\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.jpg\",\n          \"location\": {\n            \"city\": \"Tallinn\",\n            \"country\": \"Eesti\",\n            \"fullAddress\": \"Pühavaimu 13/15, 10123 Tallinn, Eesti\"\n          }\n        }\n      },\n      \"brochure\": {\n        \"href\": \"http://localhost:8000/v1/test-hgid/hotels?detail=brochure&limit=1&offset=0\"\n      }\n    },\n    {\n      \"href\": \"http://localhost:8000/v1/test-hgid/hotels/2\",\n      \"id\": \"2\",\n      \"coordinates\": {\n        \"lat\": 59.436968,\n        \"long\": 24.742877\n      },\n      \"localized\": {\n        \"en_GB\": {\n          \"name\": \"St Petersbourg\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/stpeter_logo.png\",\n          \"location\": {\n            \"city\": \"Tallinn\",\n            \"country\": \"Estonia\",\n            \"fullAddress\": \"Rataskaevu 7, 10123 Tallinn, Estonia\"\n          }\n        },\n        \"et_EE\": {\n          \"name\": \"St Petersbourg\",\n          \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/stpeter_logo.png\",\n          \"location\": {\n            \"city\": \"Tallinn\",\n            \"country\": \"Eesti\",\n            \"fullAddress\": \"Rataskaevu 7, 10123 Tallinn, Eesti\"\n          }\n        }\n      },\n      \"brochure\": {\n        \"href\": \"http://localhost:8000/v1/test-hgid/hotels?detail=brochure&limit=1&offset=1\"\n      }\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/hotels/_apidoc.js",
    "groupTitle": "4_Hotels",
    "parameter": {
      "fields": {
        "Collection query parameters": [
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>A limit for maximum number of items returned in one response</p> "
          },
          {
            "group": "Collection query parameters",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>An offset for pagination, if set, then response will skip the first <code>N</code> values.</p> "
          }
        ],
        "Hotel collection query parameters": [
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "-180 - 180",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude of a location, near which the hotel should be located. <strong>NB! Should be used along with <code>long</code> and <code>radius</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "-180 - 180",
            "optional": false,
            "field": "long",
            "description": "<p>Longitude of a location, near which the hotel should be located. <strong>NB! Should be used along with <code>lat</code> and <code>radius</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "Number",
            "size": "0 - 6371",
            "optional": false,
            "field": "radius",
            "defaultValue": "10",
            "description": "<p>The radius in KM, inside which the hotels should be located (maximum distance from given coordinate - <code>lat</code> &amp; <code>long</code>). <strong>NB! Should be used along with <code>lat</code> and <code>long</code> query parameters.</strong></p> "
          },
          {
            "group": "Hotel collection query parameters",
            "type": "String",
            "optional": false,
            "field": "search",
            "description": "<p>A search phrase. If given, then a search is performed, and only hotels, which match the <code>search phrase</code>, will be returned. The string passed with this parameter must be url encoded and in UTF-8 encoding. (About urlencode)[<a href=\"http://www.w3schools.com/tags/ref_urlencode.asp\">http://www.w3schools.com/tags/ref_urlencode.asp</a>]</p> "
          }
        ],
        "Hotel query parameters": [
          {
            "group": "Hotel query parameters",
            "type": "String",
            "allowedValues": [
              "full",
              "brochure"
            ],
            "optional": true,
            "field": "detail",
            "description": "<p>Specifies the amount of detail, which should be returned for each hotel.</p> <p>If this parameter is given, with a value of <code>full</code> (<strong>not available yet</strong>), then Hotel object(s) will be returned in full detail.</p> <p>If this parameter is given, with a value of <code>brochure</code>, then Hotel object(s) will be returned with details, which are needed for displaying a brochure.</p> <p>If this parameter is omitted then only basic information is returned about hotel.</p> "
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "-",
    "title": "1. Hotels resource",
    "name": "hotelResource",
    "version": "1.7.4",
    "group": "4_Hotels",
    "description": "<p>Hotel resource is the main object in this API. It represents a hotel in a hotel group. By default a hotel is returned with basic set of properties and you have to specify the <code>?detail=</code> query parameter to get the Hotel object in more detail.</p> ",
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-hotel-nf",
            "description": "<p>Hotel, with given parameters was not found (<a href=\"#error_hs-hotel-nf\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-save",
            "description": "<p>Error occurred while saving hotel to database (<a href=\"#error_hs-err-save\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-src-err",
            "description": "<p>Error searching hotels (<a href=\"#error_hs-src-err\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-del",
            "description": "<p>Error deleting item(s) from search index (<a href=\"#error_hs-err-del\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hs-err-for",
            "description": "<p>Error formatting hotel (<a href=\"#error_hs-err-for\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "sr-err-src",
            "description": "<p>Error searching (<a href=\"#error_sr-err-src\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-cc-db",
            "description": "<p>Cannot connect to database <a href=\"#error_db-cc-db\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "db-query-fails",
            "description": "<p>Error querying information from database <a href=\"#error_db-query-fails\">Definition</a></p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p>Hotel group not found (<a href=\"#error_hg-not-found\">Definition</a>)</p> "
          }
        ]
      }
    },
    "filename": "src/modules/hotels/apidoc.js",
    "groupTitle": "4_Hotels",
    "parameter": {
      "fields": {
        "Basic hotel structure": [
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>An URL, which points to a service, which returns only this resource, with detail information.</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Link",
            "optional": false,
            "field": "brochure",
            "description": "<p>Link object, which points to a service, which returns hotels (with same filters as request has specified + paginated) with <code>brochure</code> information (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Identification code of this hotel</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Object",
            "optional": false,
            "field": "coordinates",
            "description": "<p>An object containing the coordinates of this hotel.</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Number",
            "size": "-90 - 90",
            "optional": false,
            "field": "coordinates.lat",
            "description": "<p>Latitude of this hotels location</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Number",
            "size": "-180 - 180",
            "optional": false,
            "field": "coordinates.long",
            "description": "<p>Longitude of this hotels location</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "contacts",
            "description": "<p>Url to get dynamic contact details by client ip address.</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Object",
            "optional": false,
            "field": "localized",
            "description": "<p>Object, where each key represents localized information about this hotel in a certain language</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE",
            "description": "<p>Information about this hotel in language, defined by <code>LANGUAGE</code> key.</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.name",
            "description": "<p>Name of this hotel</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE.location",
            "description": "<p>An object containing the location info of this hotel.</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.location.city",
            "description": "<p>Name of the city, where this hotel is at</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.location.country",
            "description": "<p>Name of the country, where this hotel is at</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.location.fullAddress",
            "description": "<p>Full address of the hotel</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.location.shortAddress",
            "description": "<p>Short address of the hotel (neighborhood, city, country)</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Image",
            "optional": false,
            "field": "localized.LANGUAGE.image",
            "description": "<p>Image object, gives primary image of the hotel, which should be shown in listings (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          }
        ],
        "'Brochure' hotel structure (Extra for basic)": [
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object[]",
            "optional": false,
            "field": "gallery",
            "description": "<p>Image objects collection, each one represents image in gallery (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object",
            "optional": false,
            "field": "localized",
            "description": ""
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE",
            "description": ""
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE.contacts",
            "description": "<p>Contact information for this hotel</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE.contacts.primary",
            "description": "<p>Object containing primary contact information to show to clients</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.contacts.primary.phone",
            "description": "<p>Contact phone number</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.contacts.primary.email",
            "description": "<p>Contact e-mail address</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.contacts.primary.website",
            "description": "<p>Website URL</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String/NULL",
            "optional": false,
            "field": "localized.LANGUAGE.bookingUrl",
            "description": "<p>Url to the direct booking form of this hotel, if missing, then it equals <code>NULL</code>. This url includes following tokens, which should be replaced and user should be directed to the final url:</p> <p><code>&lt;StartDate&gt;</code> Date, when the booking starts, it is date represented as <code>mm/dd/yyyy</code> and uriEncoded, meaning the <code>/</code> is converted to <code>%2f2</code> and the output format will be like <code>11%2f25%2f2015</code></p> <p><code>&lt;StartDateIso&gt;</code> Date, when the booking starts in ISO format, it is date represented as <code>yyyy-mm-dd</code> for example <code>2015-09-21</code></p> <p><em>NB! StartDate and StartDateIso are never together int the URL, it is either one or another</em></p> <p><code>&lt;NoOfNights&gt;</code> Integer, stating how many nights is the booking requested for</p> <p><code>&lt;NoOfAdults&gt;</code> Integer, stating for how many adults the booking is requested for</p> <p>Example value would be like:</p> <p><code>https://booking.slh.com/en-GB/Room/Availability/?HotelCode=HUTLLTE&amp;StartDate=&lt;StartDate&gt;&amp;NoOfNights=&lt;NoOfNights&gt;&amp;NoOfAdults=&lt;NoOfAdults&gt;</code></p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object[]",
            "optional": false,
            "field": "localized.LANGUAGE.description",
            "description": "<p>Array of description blocks as Objects</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String[]/Object[]",
            "optional": false,
            "field": "localized.LANGUAGE.description.heading",
            "description": "<p>Heading of this description block. Represented as list of strings or objects. All strings in this array should be glued together without a separator, Object elements should be ignored for now.</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String[]/Object[]",
            "optional": false,
            "field": "localized.LANGUAGE.description.text",
            "description": "<p>Body text of this description block Represented as list of strings or objects.</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object[]",
            "optional": false,
            "field": "localized.LANGUAGE.menu",
            "description": "<p>Hotel actions menu as array of Objects</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String",
            "allowedValues": [
              "web",
              "phone",
              "map",
              "share",
              "book"
            ],
            "optional": false,
            "field": "localized.LANGUAGE.menu.action",
            "description": "<p>Identifier of the action</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Boolean",
            "allowedValues": [
              "true",
              "false"
            ],
            "optional": false,
            "field": "localized.LANGUAGE.menu.isEnabled",
            "description": "<p>Controls if the button is usable (active) or in disabled state (inactive)</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.menu.name",
            "description": "<p>Display name for this action</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE.menu.data",
            "description": "<p>Contain properties of this action. Every action has custom properties.</p> <p><code>web</code> has <code>url</code> - url to hotel website might be string</p> <p><code>phone</code> has <code>phone</code> - phone number for calling to hotel</p> <p><code>map</code> has <code>lat</code> and <code>long</code> - hotel coordinate values. Latitude and Longitude</p> <p><code>share</code> has <code>name</code> and <code>gallery</code> - Name of hotel and hotel gallery for sharing in social networks</p> <p><code>book</code> has <code>url</code> and <code>type</code> - Url for booking in hotel and Type to know, which booking method is used. If type equal to <code>url</code>, then should find <code>url</code> property for booking</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String",
            "optional": false,
            "field": "vhSiteKey",
            "description": "<p>The VirtualHotel APP keyword, if virtual hotel app exists for this hotel, if not, then this value is <code>false</code>.</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String",
            "optional": false,
            "field": "gaTrackingId",
            "description": "<p>The Google Analytics tracking code (UA-....), if missing then this value is <code>false</code></p> "
          }
        ],
        "Hotel query parameters": [
          {
            "group": "Hotel query parameters",
            "type": "String",
            "allowedValues": [
              "full",
              "brochure"
            ],
            "optional": true,
            "field": "detail",
            "description": "<p>Specifies the amount of detail, which should be returned for each hotel.</p> <p>If this parameter is given, with a value of <code>full</code> (<strong>not available yet</strong>), then Hotel object(s) will be returned in full detail.</p> <p>If this parameter is given, with a value of <code>brochure</code>, then Hotel object(s) will be returned with details, which are needed for displaying a brochure.</p> <p>If this parameter is omitted then only basic information is returned about hotel.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Example hotel resource",
          "content": " {\n  \"href\": \"http://localhost:8000/v1/test-hgid/hotels/ernst\",\n  \"id\": \"ernst\",\n  \"coordinates\": {\n    \"lat\": 50.941857,\n    \"long\": 6.956522\n  },\n  \"localized\": {\n    \"en_GB\": {\n      \"name\": \"Excelsior Hotel Ernst\",\n      \"location\": {\n        \"city\": \"Cologne\",\n        \"country\": \"Germany\",\n        \"fullAddress\": \"Domplatz/Trankgasse 1-5, 50667 Cologne, Germany\",\n        \"shortAddress\": \"Alstadt-Nord, Cologne, Germany\"\n      },\n      \"buttonImage\": {\n        \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image\",\n        \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image\"\n      },\n      \"contacts\": {\n        \"primary\": {\n          \"email\": \" info@excelsior-hotel-ernst.de\",\n \"phone\": \"+49 (0)221 270 1\",\n \"website\": \"http://www.excelsiorhotelernst.com/en\"\n }\n },\n \"bookingUrl\": \"https://booking.slh.com/en-GB/Room/Availability/?HotelCode=HUBKIGI&StartDate=<StartDate>&NoOfNights=<NoOfNights>&NoOfAdults=<NoOfAdults>\",\n \"description\": [\n {\n\t\"text\": [\n\t\t{\n\t\t\t\"style\": null,\n\t\t\t\"value\": \"The Excelsior Hotel Ernst is Cologne’s Grand Hotel next to the Cathedral. We provide individualized services to respond to the uniqueness of every guest and employee. We maintain the tradition of hospitality and remain modern by applying innovation. As Grand Hotel in the heart of Cologne, we actively participate in urban life and we are committed to art and culture. Your individuality is our Excellence.\"\n\t\t}\n\t],\n\t\"heading\": [\n\t\t{\n\t\t\t\"style\": null,\n\t\t\t\"value\": \"About us\"\n\t\t}\n\t]\n},\n {\n\t\"text\": [\n\t\t{\n\t\t\t\"style\": null,\n\t\t\t\"value\": \"Carl Ernst, Royal Restorer of the central station, was builder and owner of the Hotel Ernst in the city center. The opening ceremony took place on May 16th 1863. In 1871, after as little as eight years, he sold the Hotel Ernst to Friedrich Kracht. Friedrich Kracht moved from Belgium to Cologne to manage the hotel but he died four years later. His wife and his son Carl took over the management of the house.  In those days, the Grand Hotel was already the first choice of prominent guests.\"\n\t\t}\n\t],\n\t\"heading\": [\n\t\t{\n\t\t\t\"style\": null,\n\t\t\t\"value\": \"History\"\n\t\t}\n\t]\n}\n ],\n \"gallery\": [\n {\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1\",\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1\"\n},\n {\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0\",\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0\"\n}\n ]\n }\n },\n \"menu\": [\n {\n    \"action\": \"web\",\n    \"isEnabled\": true,\n    \"data\": {\n        \"url\": \"http://www.corinthia.com/hotels/london/\"\n    }\n},\n {\n    \"action\": \"phone\",\n    \"isEnabled\": true,\n    \"data\": {\n        \"phone\": \"+372 628 6500\"\n    }\n},\n {\n    \"action\": \"map\",\n    \"isEnabled\": true,\n    \"data\": {\n        \"lat\": 51.506551,\n        \"long\": -0.124039\n    }\n},\n {\n    \"action\": \"share\",\n    \"isEnabled\": true,\n    \"data\": {\n        \"name\": \"Corinthia Hotel London\",\n        \"gallery\": [\n           {\n                \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1\",\n                \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1\"\n            },\n            {\n                \"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0\",\n                \"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0\"\n            }\n        ]\n    }\n},\n {\n    \"action\": \"book\",\n    \"isEnabled\": true,\n    \"data\": {\n        \"url\": \"https://booking.slh.com/en-GB/Room/Availability/?HotelCode=HUTLLTE&StartDate=<StartDate>&NoOfNights=<NoOfNights>&NoOfAdults=<NoOfAdults>\",\n        \"type\": \"url\"\n    }\n}\n ],\n \"vhSiteKey\": \"devtallinn\",\n \"gaTrackingId\": \"UA-XXXXX-XX\"\n }",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "-",
    "title": "1. Hotels resource",
    "name": "hotelResource",
    "version": "0.0.1",
    "group": "4_Hotels",
    "description": "<p>Hotel resource is the main object in this API. It represents a hotel in a hotel group. By default a hotel is returned with basic set of properties and you have to specify the <code>?detail=</code> query parameter to get the Hotel object in more detail.</p> ",
    "filename": "src/modules/hotels/_apidoc.js",
    "groupTitle": "4_Hotels",
    "parameter": {
      "fields": {
        "Basic hotel structure": [
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "href",
            "description": "<p>An URL, which points to a service, which returns only this resource, with detail information.</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Link",
            "optional": false,
            "field": "brochure",
            "description": "<p>Link object, which points to a service, which returns hotels (with same filters as request has specified + paginated) with <code>brochure</code> information (<a href=\"#api-1_Basics-basicsLinks\">About links</a>)</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Identification code of this hotel</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Object",
            "optional": false,
            "field": "coordinates",
            "description": "<p>An object containing the coordinates of this hotel.</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Number",
            "size": "-180 - 180",
            "optional": false,
            "field": "coordinates.lat",
            "description": "<p>Latitude of this hotels location</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Number",
            "size": "-180 - 180",
            "optional": false,
            "field": "coordinates.long",
            "description": "<p>Longitude of this hotels location</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Object",
            "optional": false,
            "field": "localized",
            "description": "<p>Object, where each key represents localized information about this hotel in a certain language</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE",
            "description": "<p>Information about this hotel in language, defined by <code>LANGUAGE</code> key.</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.name",
            "description": "<p>Name of this hotel</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.icon",
            "description": "<p>Icon image URL for this hotel (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE.location",
            "description": "<p>An object containing the location info of this hotel.</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.location.city",
            "description": "<p>Name of the city, where this hotel is at</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.location.country",
            "description": "<p>Name of the country, where this hotel is at</p> "
          },
          {
            "group": "Basic hotel structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.location.fullAddress",
            "description": "<p>Full address of the hotel</p> "
          }
        ],
        "'Brochure' hotel structure (Extra for basic)": [
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object",
            "optional": false,
            "field": "localized",
            "description": ""
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE",
            "description": ""
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Number",
            "optional": false,
            "field": "localized.LANGUAGE.stars",
            "description": "<p>Hotel&#39;s rating as number of stars</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE.contacts",
            "description": "<p>Contact information for this hotel</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE.contacts.primary",
            "description": "<p>Object containing primary contact information to show to clients</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE.contacts.primary.phone",
            "description": "<p>Contact phone number</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object",
            "optional": false,
            "field": "localized.LANGUAGE.contacts.primary.email",
            "description": "<p>Contact e-mail address</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.contacts.primary.website",
            "description": "<p>Website URL</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object[]",
            "optional": false,
            "field": "localized.LANGUAGE.description",
            "description": "<p>Array of description blocks as Objects</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String[]/Object[]",
            "optional": false,
            "field": "localized.LANGUAGE.description.heading",
            "description": "<p>Heading of this description block. Represented as list of strings or objects. All strings in this array should be glued together without a separator, Object elements should be ignored for now.</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String[]/Object[]",
            "optional": false,
            "field": "localized.LANGUAGE.description.text",
            "description": "<p>Body text of this description block Represented as list of strings or objects. All strings in this array should be glued together without a separator, Object elements should be ignored for now.</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "Object[]",
            "optional": false,
            "field": "localized.LANGUAGE.images",
            "description": "<p>Object, containing links to images</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.images.highRes",
            "description": "<p>The high-resolution image URL (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          },
          {
            "group": "'Brochure' hotel structure (Extra for basic)",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.images.lowRes",
            "description": "<p>The low-resolution image URL (<a href=\"#api-1_Basics-basicsImages\">About images</a>)</p> "
          }
        ],
        "Hotel query parameters": [
          {
            "group": "Hotel query parameters",
            "type": "String",
            "allowedValues": [
              "full",
              "brochure"
            ],
            "optional": true,
            "field": "detail",
            "description": "<p>Specifies the amount of detail, which should be returned for each hotel.</p> <p>If this parameter is given, with a value of <code>full</code> (<strong>not available yet</strong>), then Hotel object(s) will be returned in full detail.</p> <p>If this parameter is given, with a value of <code>brochure</code>, then Hotel object(s) will be returned with details, which are needed for displaying a brochure.</p> <p>If this parameter is omitted then only basic information is returned about hotel.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Example basic hotel resource",
          "content": " {\n  \"href\": \"http://localhost:8000/v1/test-hgid/hotels/1\",\n  \"id\": \"1\",\n  \"coordinates\": {\n    \"lat\": 59.438591,\n    \"long\": 24.747744\n  },\n  \"localized\": {\n    \"en_GB\": {\n      \"name\": \"Schlossle\",\n      \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.png\",\n      \"location\": {\n        \"city\": \"Tallinn\",\n        \"country\": \"Estonia\",\n        \"fullAddress\": \"Pühavaimu 13/15, 10123 Tallinn, Estonia\"\n      }\n    },\n    \"et_EE\": {\n      \"name\": \"Schlössle\",\n      \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.jpg\",\n      \"location\": {\n        \"city\": \"Tallinn\",\n        \"country\": \"Eesti\",\n        \"fullAddress\": \"Pühavaimu 13/15, 10123 Tallinn, Eesti\"\n      }\n    }\n  },\n  \"brochure\": {\n    \"href\": \"http://localhost:8000/v1/test-hgid/categories/reg-eur-est/hotels?detail=brochure&limit=1&offset=0\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Example brochure hotel resource",
          "content": " {\n  \"href\": \"http://localhost:8000/v1/test-hgid/hotels/1\",\n  \"id\": \"1\",\n  \"coordinates\": {\n    \"lat\": 59.438591,\n    \"long\": 24.747744\n  },\n  \"localized\": {\n    \"en_GB\": {\n      \"name\": \"Schlossle\",\n      \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.png\",\n      \"location\": {\n        \"city\": \"Tallinn\",\n        \"country\": \"Estonia\",\n        \"fullAddress\": \"Pühavaimu 13/15, 10123 Tallinn, Estonia\"\n      },\n      \"stars\": 5,\n      \"contacts\": {\n        \"primary\": {\n          \"email\": \"sch@schlossle-hotels.com\",\n \"phone\": \"+372 699 7700\",\n \"website\": \"http://www.schloesslehotel.com\"\n }\n },\n \"description\": [\n {\n\t\"text\": [\n\t\t{\n\t\t\t\"styles\": [\"predefinedStyle1\"],\n\t\t\t\"inLineImages\": []\n\t\t},\n\t\t\"Tallinn\",\n\t\t\"in the 13th century was a thriving Hanseatic trading centre where merchants and nobles from all across Europe came to do business . This fortified medieval city was one of the most famous ports in the eastern world, home for goods brought from ships that sailed from as far away as Africa, and by horse and carriage from both east and west . Tallinn was and still is a trading city, and many of the old medieval buildings are a continuing tribute to that legacy \"\n\t],\n\t\"heading\": [\"History\"]\n},\n {\n\t\"text\": [\n\t\t{\n\t\t\t\"link\": \"http://www.schloesslehotel.com\",\n\t\t\t\"styles\": [\"predefinedStyle2\"]\n\t\t},\n\t\t\"The Schlössle Hotel\",\n\t\t\"is a luxurious 5-star hotel in Tallinn’s Old Town. The 23-room boutique hotel has luxurious furnishings and attentive staff to ensure that you will have a memorable stay in this charming hotel . Nestled in the heart of Tallinn ’ s Old Town, the Schlössle is a delightful boutique hotel that will take you back to Estonia ’ s medieval past . \"\n\t],\n\t\"heading\": [\"Services\"]\n}\n ],\n \"images\": [\n {\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle.jpg\",\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle.jpg\"\n},\n {\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle2.jpg\",\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle2.jpg\"\n}\n ]\n },\n \"et_EE\": {\n      \"name\": \"Schlössle\",\n      \"icon\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.jpg\",\n      \"location\": {\n        \"city\": \"Tallinn\",\n        \"country\": \"Eesti\",\n        \"fullAddress\": \"Pühavaimu 13/15, 10123 Tallinn, Eesti\"\n      },\n      \"stars\": 5,\n      \"contacts\": {\n        \"primary\": {\n          \"email\": \"sch@schlossle-hotels.com\",\n \"phone\": \"+372 699 7700\",\n \"website\": \"http://www.schloesslehotel.com/et/\"\n }\n },\n \"description\": [\n {\n\t\"text\": [\n\t\t{\n\t\t\t\"link\": \"http://www.schloesslehotel.com/et\",\n\t\t\t\"styles\": [\"predefinedStyle2\"]\n\t\t},\n\t\t\"Hotel Schlössle\",\n\t\t\" on luksuslik 5-e tärni hotell Tallinna vanalinnas Pühavaimu tänaval. Hotellis on 23 luksuslikult sisustatud hotellituba, hotelli tähelepanelik personal teeb kõik et viibimine hotellis oleks külalistele igati meeldiv.\"\n\t],\n\t\"heading\": [\"Teenused\"]\n}\n ],\n \"images\": [\n {\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle.jpg\",\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle.jpg\"\n},\n {\n\t\"lowRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle2.jpg\",\n\t\"highRes\": \"http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle2.jpg\"\n}\n ]\n }\n }\n }",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/profiles/resendverificationemail",
    "title": "6. Resend verification email",
    "name": "profileResendverificationemail",
    "version": "1.7.4",
    "group": "5_Profiles",
    "description": "<p>Resends the verification email.</p> <p><strong>NB!</strong> Security level <code>3</code> is required (<a href=\"#api-1_Basics-basicsSecurity\">About security</a>)</p> ",
    "examples": [
      {
        "title": "Example request",
        "content": "POST /profiles/resendverificationemail HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f\n{\n \"email\": \"peter.griffin@yopmail.com\"\n}",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "POST body": [
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The email of the user account</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Response": [
          {
            "group": "Response",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Boolean indicating if any settings were saved or not</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Example response",
          "content": " {\n\t\t\"success\": true\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-resend-failed",
            "description": "<p>Resending failed (<a href=\"#error_ps-resend-failed\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-resend-inv",
            "description": "<p>Invalid input given (<a href=\"#error_ps-resend-inv\">Definition</a>)</p> "
          }
        ]
      }
    },
    "filename": "src/modules/profiles/apidoc.js",
    "groupTitle": "5_Profiles"
  },
  {
    "type": "post",
    "url": "/profiles/sendresetpassword",
    "title": "8. Reset password",
    "name": "profileResetpassword",
    "version": "1.7.4",
    "group": "5_Profiles",
    "description": "<p>Send the reset password email to the user. User receives an email, where he/she has a link to a webpage, where he/she enters the new password</p> <p><strong>NB!</strong> Security level <code>3</code> is required (<a href=\"#api-1_Basics-basicsSecurity\">About security</a>)</p> ",
    "parameter": {
      "fields": {
        "Post body": [
          {
            "group": "Post body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The email, which accounts password needs to be reset</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example request",
        "content": "POST /profiles/sendresetpassword HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f\n{\n \"email\": \"peter.griffin@yopmail.com\"\n}",
        "type": "curl"
      },
      {
        "title": "Example request",
        "content": "GET /profiles/sendresetpassword?email=asdasd@asdads.ee HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Example response",
          "content": " {\n\t\"success\": true\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "success",
            "defaultValue": "true",
            "description": "<p>Returned if password reset email was sent</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-reset-failed",
            "description": "<p>Invalid email given (<a href=\"#error_ps-reset-failed\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-reset-inv",
            "description": "<p>No email given (<a href=\"#error_ps-reset-inv\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-https-req",
            "description": "<p>Tls layer validation failed (<a href=\"#error_au-https-req\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-uuid",
            "description": "<p>Invalid uuid given (<a href=\"#error_au-inv-uuid\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-ver-req",
            "description": "<p>Invalid API version (<a href=\"#error_au-ver-req\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "au-inv-verifykey",
            "description": "<p>Invalid checksum value (<a href=\"#error_au-inv-verifykey\">Definition</a>)</p> "
          }
        ]
      }
    },
    "filename": "src/modules/profiles/apidoc.js",
    "groupTitle": "5_Profiles"
  },
  {
    "type": "get",
    "url": "-",
    "title": "1. Profile resource",
    "name": "profileResource",
    "version": "1.7.4",
    "group": "5_Profiles",
    "description": "<p>Profile resource represents the state and the settings of the user, whether he/she is logged in or not, with some metadata and settings.</p> ",
    "parameter": {
      "examples": [
        {
          "title": "Example structure for logged out user",
          "content": " {\n\t\"status\": \"signedOut\",\n\t\"account\": null,\n\t\"session\": null,\n\t\"settings\": {\n\t\t\"selectedLanguages\": [\n\t\t\t{\n\t\t\t\t\"code\": \"en_GB\",\n\t\t\t\t\"englishName\": \"English\",\n\t\t\t\t\"nativeName\": \"English\",\n\t\t\t\t\"rtl\": false,\n\t\t\t\t\"countryEnglishName\": \"United Kingdom of Great Britain and Northern Ireland\"\n\t\t\t}\n\t\t]\n\t},\n\t\"view\": {\n        \"localized\": {\n            \"en_GB\": {\n                \"profileMenu\": [\n                    {\n                        \"action\": \"settings\",\n                        \"isEnabled\": true,\n                        \"name\": \"Settings\",\n                        \"data\": {\n                            \"link\": {\n                                \"href\": \"https://localhost:8000/v1/test-hgid/profiles/60f244eb15c4eb344a0eaaca3b25f1b2f06f7c/settings\",\n                                \"method\": \"POST\"\n                            }\n                        }\n                    },\n                    {\n                        \"action\": \"languages\",\n                        \"isEnabled\": true,\n                        \"name\": \"Language\",\n                        \"data\": {\n                            \"link\": {\n                                \"href\": \"https://localhost:8000/v1/test-hgid/profiles/60f244eb15c4eb344a0eaaca3b25f1b2f06f7c/settings\",\n                                \"method\": \"POST\"\n                            }\n                        }\n                    },\n                    {\n                        \"action\": \"signOut\",\n                        \"isEnabled\": true,\n                        \"name\": \"Sign out\",\n                        \"data\": {\n                            \"link\": {\n                                \"href\": \"https://localhost:8000/v1/test-hgid/profiles/signout\",\n                                \"method\": \"GET\"\n                            }\n                        }\n                    },\n                    {\n                        \"action\": \"conditions\",\n                        \"isEnabled\": true,\n                        \"name\": \"T&Cs\",\n                        \"data\": {\n                            \"TermsAndConditionsText\": \"Please read and review the terms and conditions before using this service.\\n\\nUser acknowledges and agrees that the Services are for personal use and agrees not to use the Services in a manner prohibited by any federal or state law or regulation.\\n\\nUser acknowledges that there is content on the Internet or otherwise available through the Services which may be offensive, or which may not be in compliance with all local laws, regulations and other rules. We assume no responsibility for and exercises no control over the content contained on the Internet or is otherwise available through the Services. All content accessed or received by the User is used by User at his or her own risk, and we and our employees shall have no liability resulting from the access or use of such content by the User.\"\n                        }\n                    },\n                    {\n                        \"action\": \"signIn\",\n                        \"isEnabled\": true,\n                        \"name\": \"Sign in\",\n                        \"data\": {\n                            \"link\": {\n                                \"href\": \"https://localhost:8000/v1/test-hgid/profiles/signin\",\n                                \"method\": \"POST\"\n                            }\n                        }\n                    },\n                    {\n                        \"action\": \"signUp\",\n                        \"isEnabled\": true,\n                        \"name\": \"Sign up\",\n                        \"data\": {\n                            \"link\": {\n                                \"href\": \"https://localhost:8000/v1/test-hgid/profiles/signup\",\n                                \"method\": \"POST\"\n                            }\n                        }\n                    }\n                ]\n            }\n\t\t}\n\t},\n\t\"settingsHref\": {\n\t\t\"href\": \"https://localhost:8000/v1/test-hgid/profiles/6fa40ae916cfe144040cbd9d6364a1adf6343cade2bc40fa411978220cf81054b66b182950267cd99203f06aee40766a/settings\",\n\t\t\"method\": \"POST\"\n\t},\n\t\"signIn\": {\n\t\t\"href\": \"https://localhost:8000/v1/test-hgid/profiles/signin\",\n\t\t\"method\": \"POST\"\n\t},\n\t\"signUp\": {\n\t\t\"href\": \"https://localhost:8000/v1/test-hgid/profiles/signup\",\n\t\t\"method\": \"POST\"\n\t},\n\t\"signOut\": {\n\t\t\"href\": \"https://localhost:8000/v1/test-hgid/profiles/signout\",\n\t\t\"method\": \"GET\"\n\t}\n}",
          "type": "json"
        },
        {
          "title": "Example structure for logged in user",
          "content": "{\n\t\"status\": \"signedIn\",\n\t\"account\": {\n\t\t\"email\": \"Yos33emit2e@yopmail.com\",\n\t\t\"givenName\": \"dfg\",\n\t\t\"surname\": \"xcvxc\",\n\t\t\"fullName\": \"dfg xcvxc\",\n\t\t\"isEmailVerified\": false\n\t},\n\t\"session\": {\n\t\t\"sessionStart\": \"2015-03-20T12:49:17.827Z\",\n\t\t\"maxSessionEnd\": null\n\t},\n\t\"settings\": {\n\t\t\"selectedLanguages\": [\n\t\t\t{\n\t\t\t\t\"code\": \"en_GB\",\n\t\t\t\t\"englishName\": \"English\",\n\t\t\t\t\"nativeName\": \"English\",\n\t\t\t\t\"rtl\": false,\n\t\t\t\t\"countryEnglishName\": \"United Kingdom of Great Britain and Northern Ireland\"\n\t\t\t}\n\t\t]\n\t},\n\t\"settingsHref\": {\n\t\t\"href\": \"https://localhost:8000/v1/test-hgid/profiles/53f841bf43cfef1d5d5dbdee2d26e0f1f2697de2b6e31a/settings\",\n\t\t\"method\": \"POST\"\n\t},\n\t\"signIn\": {\n\t\t\"href\": \"https://localhost:8000/v1/test-hgid/profiles/signin\",\n\t\t\"method\": \"POST\"\n\t},\n\t\"signUp\": {\n\t\t\"href\": \"https://localhost:8000/v1/test-hgid/profiles/signup\",\n\t\t\"method\": \"POST\"\n\t},\n\t\"signOut\": {\n\t\t\"href\": \"https://localhost:8000/v1/test-hgid/profiles/signout\",\n\t\t\"method\": \"GET\"\n\t}\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Profile structure": [
          {
            "group": "Profile structure",
            "type": "String",
            "allowedValues": [
              "signedOut",
              "signedIn"
            ],
            "optional": false,
            "field": "status",
            "defaultValue": "signedOut",
            "description": "<p>Indicates if the user is signed in or not.</p> "
          },
          {
            "group": "Profile structure",
            "type": "Object",
            "optional": false,
            "field": "account",
            "defaultValue": "null",
            "description": "<p>Holds the information about the user account</p> "
          },
          {
            "group": "Profile structure",
            "type": "String",
            "optional": false,
            "field": "account.email",
            "description": "<p>The e-mail of the user</p> "
          },
          {
            "group": "Profile structure",
            "type": "String",
            "optional": false,
            "field": "account.givenName",
            "description": "<p>The first name of the user</p> "
          },
          {
            "group": "Profile structure",
            "type": "String",
            "optional": false,
            "field": "account.surname",
            "description": "<p>The surname of the user</p> "
          },
          {
            "group": "Profile structure",
            "type": "Object",
            "optional": false,
            "field": "account.fullName",
            "description": "<p>The full name of the user</p> "
          },
          {
            "group": "Profile structure",
            "type": "Boolean",
            "allowedValues": [
              "true",
              "false"
            ],
            "optional": false,
            "field": "account.isEmailVerified",
            "description": "<p>Boolean indicating, if the account email has been verified or not</p> "
          },
          {
            "group": "Profile structure",
            "type": "Object",
            "optional": false,
            "field": "session",
            "defaultValue": "null",
            "description": "<p>Session information</p> "
          },
          {
            "group": "Profile structure",
            "type": "String",
            "optional": false,
            "field": "session.sessionStart",
            "description": "<p>The timestamp of the session beginning in ISO-8601 standard (YYYY-MM-DDTHH:mm:ss.sssZ)</p> "
          },
          {
            "group": "Profile structure",
            "type": "String",
            "optional": false,
            "field": "session.maxSessionEnd",
            "defaultValue": "null",
            "description": "<p>The timestamp of the maximum session end in ISO-8601 standard (YYYY-MM-DDTHH:mm:ss.sssZ)</p> "
          },
          {
            "group": "Profile structure",
            "type": "Object",
            "optional": false,
            "field": "settings",
            "defaultValue": "{}",
            "description": "<p>Settings object</p> "
          },
          {
            "group": "Profile structure",
            "type": "Language[]",
            "optional": true,
            "field": "settings.selectedLanguages",
            "description": "<p>List of selected languages as objects (<a href=\"#api-1_Basics-basicsLangs\">About languages</a>)</p> "
          },
          {
            "group": "Profile structure",
            "type": "Object",
            "optional": false,
            "field": "view",
            "defaultValue": "{}",
            "description": "<p>Object, holding assets for view</p> "
          },
          {
            "group": "Profile structure",
            "type": "Object",
            "optional": false,
            "field": "view.localized",
            "description": "<p>Object, where each key represents localized information about view</p> "
          },
          {
            "group": "Profile structure",
            "type": "Object",
            "optional": false,
            "field": "view.localized.LANGUAGE",
            "description": "<p>Information about view in language, defined by <code>LANGUAGE</code> key.</p> "
          },
          {
            "group": "Profile structure",
            "type": "Object[]",
            "optional": false,
            "field": "view.localized.LANGUAGE.profileMenu",
            "description": "<p>Profile menu as list of menu items, different versions,  one for signedIn and second for signedOut user</p> "
          },
          {
            "group": "Profile structure",
            "type": "String",
            "allowedValues": [
              "settings",
              "languages",
              "signOut",
              "conditions",
              "signIn",
              "signUp"
            ],
            "optional": false,
            "field": "view.localized.LANGUAGE.profileMenu.action",
            "description": "<p>Action identifier</p> "
          },
          {
            "group": "Profile structure",
            "type": "String",
            "optional": false,
            "field": "localized.LANGUAGE.menu.name",
            "description": "<p>Display name for this action</p> "
          },
          {
            "group": "Profile structure",
            "type": "Boolean",
            "allowedValues": [
              "true",
              "false"
            ],
            "optional": false,
            "field": "localized.LANGUAGE.menu.isEnabled",
            "description": "<p>Controls if the button is usable (active) or in disabled state (inactive)</p> "
          },
          {
            "group": "Profile structure",
            "type": "Object",
            "optional": false,
            "field": "view.localized.LANGUAGE.profileMenu.data",
            "description": "<p>Contain properties of this action. Every action can have custom properties.</p> <p><code>settings</code> has <code>link</code> - link object contain two keys <code>href</code> equal to url and <code>method</code> can be equal to POST, GET, PUT ...</p> <p><code>languages</code> has <code>link</code> -link object contain two keys <code>href</code> equal to url and <code>method</code> can be equal to POST, GET, PUT ...</p> <p><code>signOut</code> has <code>link</code> - link object contain two keys <code>href</code> equal to url and <code>method</code> can be equal to POST, GET, PUT ...</p> <p><code>conditions</code> has <code>TermsAndConditionsText</code> - contain terms and conditions text</p> <p><code>signIn</code> has <code>name</code> and <code>link</code> - link object contain two keys <code>href</code> equal to url and <code>method</code> can be equal to POST, GET, PUT ...</p> <p><code>signUp</code> has <code>url</code> and <code>link</code> - link object contain two keys <code>href</code> equal to url and <code>method</code> can be equal to POST, GET, PUT ...</p> "
          }
        ]
      }
    },
    "filename": "src/modules/profiles/apidoc.js",
    "groupTitle": "5_Profiles"
  },
  {
    "type": "post",
    "url": "/profiles/:profileId/settings",
    "title": "7. Update settings",
    "name": "profileSettings",
    "version": "1.7.4",
    "group": "5_Profiles",
    "description": "<p>Changes settings of the profile.</p> <p><strong>NB!</strong> Security level <code>3</code> is required (<a href=\"#api-1_Basics-basicsSecurity\">About security</a>)</p> ",
    "parameter": {
      "fields": {
        "POST body": [
          {
            "group": "POST body",
            "type": "String[]",
            "optional": false,
            "field": "selectedLanguages",
            "description": "<p>An array of selected language codes</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example request",
        "content": "POST /profiles/signin HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f\n{\n\t\"selectedLanguages\": [\"fr_FR\"]\n}",
        "type": "curl"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Example response",
          "content": "{\n\t\"success\": true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-acc-get",
            "description": "<p>Cannot get/find account from Stormpath (<a href=\"#error_ps-acc-get\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "hg-not-found",
            "description": "<p>Error fetching hotel group information (<a href=\"#error_hg-not-found\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-set-inv-inp",
            "description": "<p>Selected languages are invalid (<a href=\"#error_ps-set-inv-inp\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-customdata-sav",
            "description": "<p>Error occurred while updating account settings (<a href=\"#error_ps-customdata-sav\">Definition</a>)</p> "
          }
        ]
      }
    },
    "filename": "src/modules/profiles/apidoc.js",
    "groupTitle": "5_Profiles"
  },
  {
    "type": "post",
    "url": "/profiles/signin",
    "title": "3. Sign in",
    "name": "profileSignin",
    "version": "1.7.4",
    "group": "5_Profiles",
    "description": "<p>Authenticates an account and creates a session for this account. Returns a Profile resource.</p> <p><strong>NB!</strong> Security level <code>3</code> is required (<a href=\"#api-1_Basics-basicsSecurity\">About security</a>)</p> ",
    "parameter": {
      "fields": {
        "POST body": [
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The email of the user account</p> "
          },
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>The password of the user account</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example request",
        "content": "POST /profiles/signin HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f\n{\n  \"email\": \"peter.griffin@yopmail.com\",\n  \"password\": \"0.aAasdasdd\"\n}",
        "type": "curl"
      }
    ],
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-mis-inp",
            "description": "<p>Missing some input data (<a href=\"#error_ps-mis-inp\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-acc-get",
            "description": "<p>Cannot get/find account from Stormpath (<a href=\"#error_ps-acc-get\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-tenant-verify",
            "description": "<p>Cannot auto-verify account (This is not verification of email!) (<a href=\"#error_ps-tenant-verify\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-inv-pass",
            "description": "<p>Invalid password (<a href=\"#error_ps-inv-pass\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-no-acc",
            "description": "<p>Invalid email - no account with this email (<a href=\"#error_ps-no-acc\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-account-auth",
            "description": "<p>Some system error preventing user to log in (<a href=\"#error_ps-account-auth\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-session-add",
            "description": "<p>Cannot start a session for the user (<a href=\"#error_ps-session-add\">Definition</a>)</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Response": [
          {
            "group": "Response",
            "type": "Profile",
            "optional": false,
            "field": "Profile",
            "description": "<p>object (<a href=\"#api-5_Profiles-profileResource\">About</a>)</p> "
          }
        ]
      }
    },
    "filename": "src/modules/profiles/apidoc.js",
    "groupTitle": "5_Profiles"
  },
  {
    "type": "get",
    "url": "/profiles/signout",
    "title": "5. Sign out",
    "name": "profileSignout",
    "version": "1.7.4",
    "group": "5_Profiles",
    "description": "<p>Terminates the current session (user is logged out). Returns a Profile resource.</p> <p><strong>NB!</strong> Security level <code>3</code> is required (<a href=\"#api-1_Basics-basicsSecurity\">About security</a>)</p> ",
    "examples": [
      {
        "title": "Example request",
        "content": "GET /profiles/signout HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "curl"
      }
    ],
    "success": {
      "fields": {
        "Response": [
          {
            "group": "Response",
            "type": "Profile",
            "optional": false,
            "field": "Profile",
            "description": "<p>object (<a href=\"#api-5_Profiles-profileResource\">About</a>)</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-session-out",
            "description": "<p>Error getting account information (<a href=\"#error_ps-session-out\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-acc-get",
            "description": "<p>Cannot get/find account from Stormpath (<a href=\"#error_ps-acc-get\">Definition</a>)</p> "
          }
        ]
      }
    },
    "filename": "src/modules/profiles/apidoc.js",
    "groupTitle": "5_Profiles"
  },
  {
    "type": "post",
    "url": "/profiles/signup",
    "title": "2. Sign up",
    "name": "profileSignup",
    "version": "1.7.4",
    "group": "5_Profiles",
    "description": "<p>Sign up for an account. If sign up is successful then automatically the sign-in process is also executed - meaning a session for the account is created. Returns a Profile resource.</p> <p><strong>NB!</strong> Security level <code>3</code> is required (<a href=\"#api-1_Basics-basicsSecurity\">About security</a>)</p> ",
    "parameter": {
      "fields": {
        "POST body": [
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The working and unique email for the user account</p> "
          },
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>The password for the user account</p> "
          },
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "givenName",
            "description": "<p>Given name of the person</p> "
          },
          {
            "group": "POST body",
            "type": "String",
            "optional": false,
            "field": "surname",
            "description": "<p>Surname of the person</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "system-error",
            "description": "<p>System error (error in implementation) (<a href=\"#error_system-error\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-inv-inp",
            "description": "<p>Invalid type of input given (<a href=\"#error_ps-inv-inp\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-mis-inp",
            "description": "<p>Missing some required input (<a href=\"#error_ps-mis-inp\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-session-add",
            "description": "<p>Couldn&#39;t start a session for the account (<a href=\"#error_ps-session-add\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-weak-pass",
            "description": "<p>The password is too weak (<a href=\"#error_ps-weak-pass\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-inv-email",
            "description": "<p>The email is invalid (<a href=\"#error_ps-inv-email\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-ex-email",
            "description": "<p>The email is not unique (<a href=\"#error_ps-ex-email\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-inv-gn",
            "description": "<p>The given name is invalid (<a href=\"#error_ps-inv-gn\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-inv-sn",
            "description": "<p>The surname is invalid (<a href=\"#error_ps-inv-sn\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-account-add",
            "description": "<p>There was a problem creating the account (<a href=\"#error_ps-account-add\">Definition</a>)</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example request",
        "content": "POST /profiles/signup HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f\n{\n\t\"givenName\": \"Peter\",\n\t\"surname\": \"Griffin\",\n\t\"email\": \"peter.griffin@yopmail.com\",\n\t\"password\": \"0.aAasdasdd\"\n}",
        "type": "curl"
      }
    ],
    "success": {
      "fields": {
        "Response": [
          {
            "group": "Response",
            "type": "Profile",
            "optional": false,
            "field": "Profile",
            "description": "<p>object (<a href=\"#api-5_Profiles-profileResource\">About</a>)</p> "
          }
        ]
      }
    },
    "filename": "src/modules/profiles/apidoc.js",
    "groupTitle": "5_Profiles"
  },
  {
    "type": "get",
    "url": "/profiles/status",
    "title": "4. Status",
    "name": "profileStatus",
    "version": "1.7.4",
    "group": "5_Profiles",
    "description": "<p>Get the profile and session status. This method should be called to get the status of the session and the settings, which apply. Call it once in a while to ensure that the session still exists (the session might expire or the administration might end the session) Returns a Profile resource.</p> <p><strong>NB!</strong> Security level <code>3</code> is required (<a href=\"#api-1_Basics-basicsSecurity\">About security</a>)</p> ",
    "examples": [
      {
        "title": "Example request",
        "content": "GET /profiles/status HTTP/1.1\nHost: api.cardola.com\nAccept: application/json\nX-Cardola-Uuid: 1234-5678-8765-4321\nX-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f",
        "type": "curl"
      }
    ],
    "success": {
      "fields": {
        "Response": [
          {
            "group": "Response",
            "type": "Profile",
            "optional": false,
            "field": "Profile",
            "description": "<p>object (<a href=\"#api-5_Profiles-profileResource\">About</a>)</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-session-get",
            "description": "<p>Error getting session information (<a href=\"#error_ps-session-get\">Definition</a>)</p> "
          },
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "ps-acc-get",
            "description": "<p>Error getting account information (<a href=\"#error_ps-acc-get\">Definition</a>)</p> "
          }
        ]
      }
    },
    "filename": "src/modules/profiles/apidoc.js",
    "groupTitle": "5_Profiles"
  },
  {
    "type": "get",
    "url": "-",
    "title": "1. Reservations resource",
    "name": "ReservationsRes",
    "version": "1.7.4",
    "group": "6_Reservations",
    "description": "<p>Reservation is a room reservation in a hotel. This resource represents all important details about this Reservation.</p> ",
    "examples": [
      {
        "title": "Reservation example",
        "content": " {\n\t\"startDate\": \"2015-08-28T11:21:53Z\",\n\t\"hotelId\": \"ernst\",\n\t\"hotelGroupId\": \"test-hgid\",\n\t\"reservation\": {\n\t\t\"UniqueID\": {\n\t\t\t\"ID\": \"38nfks0\"\n\t\t},\n\t\t\"RoomStays\": {\n\t\t\t\"RoomStay\": [\n\t\t\t\t{\n\t\t\t\t\t\"RoomTypes\": {\n\t\t\t\t\t\t\"RoomType\": [\n\t\t\t\t\t\t\t{\n\t\t\t\t\t\t\t\t\"NumberOfUnits\": 1,\n\t\t\t\t\t\t\t\t\"RoomDescription\": {\n\t\t\t\t\t\t\t\t\t\"Name\": \"Junior Suite\"\n\t\t\t\t\t\t\t\t}\n\t\t\t\t\t\t\t}\n\t\t\t\t\t\t]\n\t\t\t\t\t},\n\t\t\t\t\t\"RatePlans\": {\n\t\t\t\t\t\t\"RatePlan\": [\n\t\t\t\t\t\t\t{\n\t\t\t\t\t\t\t\t\"RatePlanName\": \"Stay & play (Breakfast included)\"\n\t\t\t\t\t\t\t}\n\t\t\t\t\t\t]\n\t\t\t\t\t},\n\t\t\t\t\t\"TimeSpan\": {\n\t\t\t\t\t\t\"Start\": \"2015-01-12\",\n\t\t\t\t\t\t\"End\": \"2015-01-15\",\n\t\t\t\t\t\t\"Duration\": 3\n\t\t\t\t\t},\n\t\t\t\t\t\"GuestCounts\": {\n\t\t\t\t\t\t\"GuestCount\": [\n\t\t\t\t\t\t\t{\n\t\t\t\t\t\t\t\t\"AgeQualifyingCode\": 10,\n\t\t\t\t\t\t\t\t\"Count\": 2\n\t\t\t\t\t\t\t},{\n\t\t\t\t\t\t\t\t\"AgeQualifyingCode\": 8,\n\t\t\t\t\t\t\t\t\"Count\": 1\n\t\t\t\t\t\t\t}\n\t\t\t\t\t\t]\n\t\t\t\t\t}\n\t\t\t\t}\n\t\t\t]\n\t\t}\n\t}\n}",
        "type": "json"
      }
    ],
    "filename": "src/modules/reservations/apidoc.js",
    "groupTitle": "6_Reservations",
    "parameter": {
      "fields": {
        "Reservation resource": [
          {
            "group": "Reservation resource",
            "type": "String",
            "optional": false,
            "field": "startDate",
            "description": "<p>Defines when reservation start.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "String",
            "optional": false,
            "field": "hotelId",
            "description": "<p>Identification code of hotel.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "String",
            "optional": false,
            "field": "hotelGroupId",
            "description": "<p>Identification code of hotel group.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Object",
            "optional": false,
            "field": "reservation",
            "description": "<p>Contain reservation details.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Object",
            "optional": false,
            "field": "reservation.UniqueID",
            "description": "<p>Used to provide PMS and/or CRS identifiers.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "String",
            "optional": false,
            "field": "reservation.UniqueID.ID",
            "description": "<p>A unique identifying value assigned by the creating system. This value is the actual reservation confirmation number.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Object",
            "optional": false,
            "field": "reservation.RoomStays",
            "description": "<p>Collection of room stays.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Object[]",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay",
            "description": "<p>Room stay associated with this reservation.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Object",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.RoomTypes",
            "description": ""
          },
          {
            "group": "Reservation resource",
            "type": "Object[]",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.RoomTypes.RoomType",
            "description": ""
          },
          {
            "group": "Reservation resource",
            "type": "Integer",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.RoomTypes.RoomType.NumberOfUnits",
            "description": "<p>The number of this type of room booked.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Object",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.RoomTypes.RoomType.RoomDescription",
            "description": ""
          },
          {
            "group": "Reservation resource",
            "type": "String",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.RoomTypes.RoomType.RoomDescription.Name",
            "description": "<p>Room Type Name</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Object",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.RatePlans",
            "description": "<p>Collection of Rate plans</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Object[]",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.RatePlans.RatePlan",
            "description": "<p>All details pertaining to a specific rate plan. If multiple rate plans are booked for a reservation and the channel supports reservations with more than one rate plan, a RatePlan element will be added for each unique rate plan booked on the reservation.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "String",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.RatePlans.RatePlan.RatePlanName",
            "description": "<p>Rate Type Name.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Object",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.TimeSpan",
            "description": ""
          },
          {
            "group": "Reservation resource",
            "type": "Object",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.TimeSpan.Start",
            "description": "<p>The starting value of the time span</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Object",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.TimeSpan.End",
            "description": "<p>The ending value of the time span.</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Integer",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.TimeSpan.Duration",
            "description": "<p>Number of nights</p> "
          },
          {
            "group": "Reservation resource",
            "type": "Object",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.GuestCounts",
            "description": ""
          },
          {
            "group": "Reservation resource",
            "type": "Object[]",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.GuestCounts.GuestCount",
            "description": ""
          },
          {
            "group": "Reservation resource",
            "type": "Integer",
            "allowedValues": [
              "8",
              "10"
            ],
            "optional": true,
            "field": "reservation.RoomStays.RoomStay.GuestCounts.GuestCount.AgeQualifyingCode",
            "defaultValue": "10",
            "description": "<p>A code representing a business rule that determines the charges for a guest based upon age range</p> <p>Allowed values:</p> <ul> <li>8 (Child)</li> <li>10 (Adult)</li> </ul> "
          },
          {
            "group": "Reservation resource",
            "type": "Integer",
            "optional": false,
            "field": "reservation.RoomStays.RoomStay.GuestCounts.GuestCount.Count",
            "description": "<p>Count of guests</p> "
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/profiles/xxx/reservations?hotel=<hotelId>",
    "title": "3. Get all reservations of a client",
    "name": "getReservations",
    "version": "1.7.4",
    "group": "6_Reservations",
    "description": "<p>Returns list of all the future reservations of a client. This method returns a Collection of Reservation objects.</p> <p><a href=\"#api-1_Basics-basicsCollections\">About collections</a></p> ",
    "parameter": {
      "fields": {
        "Query": [
          {
            "group": "Query",
            "type": "string",
            "optional": true,
            "field": "hotel",
            "description": "<p>Optionally an hotel Id to receive only reservations to cretain hotel</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "bk-src-inv-arg",
            "description": "<p>Invalid input arguments (<a href=\"#error_bk-src-inv-arg\">Definition</a>)</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Example response:",
          "content": " {\n  \"href\": \"http://localhost:8000/v1/test-hgid/profiles/89da89ua/reservations/?hotel=test-hotel\",\n  \"offset\": 0,\n  \"limit\": 10,\n  \"total\": 1,\n  \"items\": [\n    {\n      ... Reservation resource...\n    }\n  ]",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/reservations/apidoc.js",
    "groupTitle": "6_Reservations"
  },
  {
    "type": "get",
    "url": "/reservations/search?ref=<ref>&lastname=<lastname>",
    "title": "2. Search for reservations",
    "name": "searchReservations",
    "version": "1.7.4",
    "group": "6_Reservations",
    "description": "<p>Searches for reservations by entered reservation reference and last name. This method returns a Collection of Reservation objects.</p> <p><a href=\"#api-1_Basics-basicsCollections\">About collections</a></p> ",
    "parameter": {
      "fields": {
        "Query": [
          {
            "group": "Query",
            "type": "string",
            "optional": false,
            "field": "ref",
            "description": "<p>Reservation reference</p> "
          },
          {
            "group": "Query",
            "type": "string",
            "optional": false,
            "field": "lastname",
            "description": "<p>Last name of the booker</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Possible error codes": [
          {
            "group": "Possible error codes",
            "type": "String",
            "optional": false,
            "field": "bk-src-inv-arg",
            "description": "<p>Invalid input arguments (<a href=\"#error_bk-src-inv-arg\">Definition</a>)</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Example response:",
          "content": " {\n  \"href\": \"http://localhost:8000/v1/test-hgid/reservations/search/?ref=123ABC&name=smith\",\n  \"offset\": 0,\n  \"limit\": 10,\n  \"total\": 1,\n  \"items\": [\n    {\n      ... Reservation resource...\n    }\n  ]",
          "type": "json"
        }
      ]
    },
    "filename": "src/modules/reservations/apidoc.js",
    "groupTitle": "6_Reservations"
  }
] });