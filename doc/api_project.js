define({
  "name": "GroupAppBackend",
  "version": "1.7.4",
  "description": "Group App backend server, which provides RESTful services.",
  "title": "Cardola Group App API documentation",
  "url": "/:version/:hgId",
  "generator": {
    "version": "0.9.0",
    "time": "2016-01-14T12:54:27.143Z"
  },
  "apidoc": "0.2.0"
});