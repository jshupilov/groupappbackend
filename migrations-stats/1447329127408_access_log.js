exports.up = function(pgm) {
    pgm.createTable('access_log',
        {
            id: { type: 'bigserial', notNull: true, primaryKey: true },
            data: { type: 'jsonb'}
        }
    );
    pgm.sql("COMMENT ON TABLE access_log IS 'This table contain parse access logs'");
    pgm.createIndex('access_log',"(data -> 'timestamp')",
        {
            name: 'access_log_timestamps',
            method: 'btree'
        }
    );

};

exports.down = function(pgm) {
    pgm.dropTable('access_log');
};
