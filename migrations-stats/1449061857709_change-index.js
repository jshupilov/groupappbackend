exports.up = function(pgm) {
	pgm.dropIndex('access_log', null, {name: 'access_log_timestamps'});
	pgm.createIndex('access_log',"((data ->> 'timestamp')::bigint)",
		{
			name: 'access_log_timestamps_int',
			method: 'btree'
		}
	);
};

exports.down = function(pgm) {
	pgm.dropIndex('access_log', null, {name: 'access_log_timestamps_int'});
	pgm.createIndex('access_log',"(data -> 'timestamp')",
		{
			name: 'access_log_timestamps',
			method: 'btree'
		}
	);
};
