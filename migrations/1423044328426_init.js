exports.up = function(pgm, run) {

	pgm.createTable('hotel',
		{
			id: { type: 'bigserial', notNull: true, primaryKey: true },
			data: { type: 'jsonb'}
		}
	);
	pgm.sql("COMMENT ON TABLE hotel IS 'This table has data about all the hotels';");
	pgm.createIndex('hotel',"(data -> 'hotelId' -> 'id'::text)",
		{
			name: 'hotel_hotelId_idx',
			method: 'btree'
		}
	);
	pgm.createIndex('hotel',"(data -> 'hotelGroup' -> 'id'::text)",
		{
			name: 'hotel_hotelGroupId_idx',
			method: 'btree'
		}
	);

	pgm.createTable('hotel_group',
		{
			id: { type: 'bigserial', notNull: true, primaryKey: true },
			data: { type: 'jsonb'}
		}
	);

	pgm.createIndex('hotel_group',"(data -> 'hotelGroupId'::text)",
		{
			name: 'hotel_group_hotelGroupId_idx',
			method: 'btree'
		}
	);

	pgm.sql("COMMENT ON TABLE hotel_group IS 'This table has data about all the hotel groups';");

	pgm.createTable('app_instance',
		{
			id: { type: 'bigserial', notNull: true, primaryKey: true },
			data: { type: 'jsonb'}
		}
	);

	pgm.sql("COMMENT ON TABLE app_instance IS 'This table has information abaout every installation of the app';");

	pgm.createTable('profile_session',
		{
			id: { type: 'bigserial', notNull: true, primaryKey: true },
			data: { type: 'jsonb'}
		}
	);

	pgm.sql("COMMENT ON TABLE app_instance IS 'This table has information about profile sessions';");

	run();
};

exports.down = function(pgm, run) {

	pgm.dropTable('hotel');
	pgm.dropTable('hotel_group');
	pgm.dropTable('app_instance');
	pgm.dropTable('profile_session');
  run();
};
