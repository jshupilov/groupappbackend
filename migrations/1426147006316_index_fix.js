exports.up = function(pgm, run) {
	pgm.dropIndex('hotel', null, {name: 'hotel_hotelgroupid_idx'});
	pgm.dropIndex('hotel', null, {name: 'hotel_hotelid_idx'});
	pgm.createIndex('hotel',"(data -> 'hotelGroup' -> 'hotelGroupId'::text)",
		{
			name: 'hotel_hotelgroupid_idx',
			method: 'btree'
		}
	);
  run();
	pgm.createIndex('hotel',"(data -> 'hotelId'::text)",
		{
			name: 'hotel_hotelid_idx',
			method: 'btree'
		}
	);
	pgm.createIndex('hotel',"(data -> 'hotelId')",
		{
			name: 'hotel_hotelid_idx2',
			method: 'gin'
		}
	);
};

exports.down = function(pgm, run) {
	pgm.dropIndex('hotel', null, {name: 'hotel_hotelgroupid_idx'});
	pgm.dropIndex('hotel', null, {name: 'hotel_hotelid_idx'});
	pgm.dropIndex('hotel', null, {name: 'hotel_hotelid_idx2'});
	pgm.createIndex('hotel',"(data -> 'hotelGroup' -> 'id'::text)",
		{
			name: 'hotel_hotelgroupid_idx',
			method: 'btree'
		}
	);
	pgm.createIndex('hotel',"(data -> 'hotelId' -> 'id'::text)",
		{
			name: 'hotel_hotelid_idx',
			method: 'btree'
		}
	);
  run();
};