exports.up = function(pgm, run) {
	pgm.createIndex('app_instance',"(data -> 'uuid'::text)",
		{
			name: 'app_instance_uuid_idx',
			method: 'btree'
		}
	);
  run();
};

exports.down = function(pgm, run) {
	pgm.dropIndex('app_instance', null, {name: 'app_instance_uuid_idx'});
  run();
};
