var path = require('path');
var logic = require(path.join(process.cwd(), 'src/modules/versioning/migrations.js'));
var q = require('q');

exports.up = function(pgm, run) {
	var prom = [];
	var dest  = ['hotel', 'hotel_group'];

	var makeIdx = function(p, d){
		logic.versionIdx(d, pgm, function(){
			p.resolve();
		});
	};

	for(var i=0; i< dest.length; i++){
		var p = q.defer();
		prom.push(p.promise);
		makeIdx(p, dest[i]);
	}

	q.all(prom).done(function(){
		run();
	});


};

exports.down = function(pgm, run) {

  run();
};
