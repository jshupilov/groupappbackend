exports.up = function(pgm, run) {
  pgm.createTable('configuration',
      {
        id: { type: 'bigserial', notNull: true, primaryKey: true },
        data: { type: 'jsonb'}
      }
  );
  pgm.sql("COMMENT ON TABLE configuration IS 'This table holds dynamic configuration values';");
  pgm.createIndex('configuration',"(data -> 'key'::text)",
      {
        name: 'configuration_keyx',
        method: 'btree'
      }
  );
  run();
};

exports.down = function(pgm, run) {
  pgm.dropTable('configuration');
  run();
};
