exports.up = function(pgm, run) {
	pgm.createTable('ss_data',
		{
			id: { type: 'bigserial', notNull: true, primaryKey: true },
			data: { type: 'jsonb'}
		}
	);
	pgm.sql("COMMENT ON TABLE ss_data IS 'This table contains ss module data';");
	pgm.createIndex('ss_data',"(data -> 'id'::text)",
		{
			name: 'ss_data_id_idx',
			method: 'btree',
			unique: true
		}
	);
  run();
};

exports.down = function(pgm, run) {
	pgm.dropTable('ss_data');
  run();
};
