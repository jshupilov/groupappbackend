exports.up = function(pgm, run) {
  pgm.createTable('reservation',
      {
        id: { type: 'bigserial', notNull: true, primaryKey: true },
        data: { type: 'jsonb'}
      }
  );
  pgm.sql("COMMENT ON TABLE reservation IS 'This table holds hotel room reservations';");
  pgm.createIndex('reservation',"(data -> 'startDate'::text)",
      {
        name: 'reservation_startDatex',
        method: 'btree'
      }
  );
  pgm.createIndex('reservation',"(data -> 'hotelId'::text)",
      {
        name: 'reservation_hotelIdx',
        method: 'btree'
      }
  );
  pgm.createIndex('reservation',"(data -> 'hotelGroupId'::text)",
      {
        name: 'reservation_hotelGroupIdx',
        method: 'btree'
      }
  );
  pgm.createIndex('reservation',"(data -> 'referenceHash'::text)",
      {
        name: 'reservation_referenceHashx',
        method: 'btree'
      }
  );
  run();
};

exports.down = function(pgm, run) {
  pgm.dropTable('reservation');
  run();
};
