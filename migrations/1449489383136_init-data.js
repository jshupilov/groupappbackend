var dir = __dirname;
var fs = require('fs');
var q = require('q');
var glob = require('glob');
var path = require('path');
var cloudinary = require('cloudinary');

var profile = require(path.join(dir, '../src/modules/profiles/services'));
var reservations = require(path.join(dir, '../src/modules/reservations/services'));
var mediaServices = require(path.join(dir, '../src/modules/media/services'));
var hgServices = require(path.join(dir, '../src/modules/hotel_groups/services'));
var hotelsServices = require(path.join(dir, '../src/modules/hotels/services'));
var searchServices = require(path.join(dir, '../src/modules/search/services'));
var databaseUtils = require(path.join(dir, '../src/modules/utils/databaseUtils'));
var jobhandler = require(path.join(dir, '../src/modules/worker/jobHandler'));
var scheduler = require(path.join(dir, '../src/modules/worker/scheduler'));
searchServices.init();
var listenerFiles = glob.sync(path.join(dir, '../src/modules/*/listener.js'));
listenerFiles.forEach(function(listenerFile) {
	require(listenerFile);
});

var hg = null;

//production hg's only
if(process.env.NODE_ENV === 'production'){
	hg = 'slh';
}


function populate(callback){
	console.log('Hotel group specified?', hg ? hg : 'NO');
	console.log('Populating data...');
	/**
	 * For every hotel group
	 */
	var hgPromises = [];
	glob(dir + '/../data/hotel_groups/*', function(err, groupFiles) {
		groupFiles.forEach(function(groupFile) {
			var gfa = groupFile.split('/');
			var hgId = gfa.pop();

			if(hg && hg !== hgId){
				console.log('Skipping', hgId);
				return;
			}
			var hgPromise = q.defer();
			hgPromises.push(hgPromise.promise);
			var promises = [];


			console.log('Creating hotel group', hgId);
			/**
			 * Load hotel group data
			 */
			try{
				var data = JSON.parse(fs.readFileSync(groupFile + '/data.json', {encoding: 'utf-8'}));

				data.meta={
					'scheduledAt': "2015-07-10T11:40:24.404Z",
					'status': 'published'
				};

			}catch(e){
				console.log('Error parsing hotel group json', e);
				return hgPromise.reject(e);
			}

			/**
			 * Upload logo file
			 */
			var logoFiles = glob.sync(groupFile + '/logo.*');
			if(err) {
				console.log('Cannot find logo file');
			} else if(logoFiles.length > 0) {
				var upLogo = q.defer();
				promises.push(upLogo.promise);
				mediaServices.uploadHotelGroupImage(hgId, logoFiles[0], {public_id: 'logo'}, function(err, upRes) {
					if(err) {
						console.log('Cannot upload logo file', err);
						upLogo.reject();
						return;
					}

					for( var lang in data.info.localized){
						var localized = data.info.localized[lang];
						localized.logo = {};


						mediaServices.setImageObjectSource(localized.logo, upRes);
					}
					data.info.logo = {};
					mediaServices.setImageObjectSource(data.info.logo, upRes);



					upLogo.resolve(true);
				});
			}

			var bgFiles = glob.sync(groupFile + '/bg.*');
			if(err) {
				console.log('Cannot find bg file');
			} else if(bgFiles.length > 0) {
				var upBg = q.defer();
				promises.push(upBg.promise);
				mediaServices.uploadHotelGroupImage(hgId, bgFiles[0], {public_id: 'bg'}, function(err, upRes) {
					if(err) {
						console.log('Cannot upload logo file');
						upBg.reject();
						return;
					}
					for( var lang in data.info.localized){
						var localized = data.info.localized[lang];
						localized.backgroundImage = {};

						mediaServices.setImageObjectSource(localized.backgroundImage, upRes);
					}
					data.info.backgroundImage = {};
					mediaServices.setImageObjectSource(data.info.backgroundImage, upRes);

					upBg.resolve(true);
				});
			}

			var extraAssets = glob.sync(groupFile + '/extra_assets/*.*');
			data.mediaConfiguration.extraAssets = [];
			if(extraAssets.length > 0) {
				extraAssets.forEach(function(file){
					var upBgo = q.defer();
					var fn = file.split('/').pop().split('.').shift();
					promises.push(upBgo.promise);
					var public_id = 'extra_assets/' + fn;
					mediaServices.uploadHotelGroupImage(hgId, file, {public_id: public_id}, function(err, upRes) {
						if(err) {
							console.log('Cannot upload file', fn);
							upBgo.reject();
							return;
						}
						var img = {
							id: public_id
						};
						mediaServices.setImageObjectSource(img, upRes);
						data.mediaConfiguration.extraAssets.push(img);
						upBgo.resolve(true);
					});
				});

			}

			/**
			 * Upload placeholder file
			 */
			var phFiles = glob.sync(groupFile + '/placeholder.*');
			if(err) {
				console.log('Cannot find placeholder file');
			} else if(phFiles.length > 0) {
				var upPh = q.defer();
				promises.push(upPh.promise);
				mediaServices.uploadHotelGroupImage(hgId, phFiles[0], {public_id: 'placeholder'}, function(err, upRes) {
					if(err) {
						console.log('Cannot upload placeholder file');
						upPh.reject();
						return;
					}
					data.mediaConfiguration.placeholderImage = {};
					mediaServices.setImageObjectSource(data.mediaConfiguration.placeholderImage, upRes);
					upPh.resolve(true);
				});
			}

			/**
			 * Upload category images
			 * @param cat
			 */
			function im4cat(cat) {
				console.log('Images for category', cat.categoryId);
				//category background
				var bgFiles = glob.sync(groupFile + '/categories/' + cat.categoryId + '/bg.*');
				if(bgFiles.length > 0) {
					var catD = q.defer();
					promises.push(catD.promise);
					mediaServices.uploadHotelGroupImage(hgId, bgFiles[0], {public_id: 'categories/' + cat.categoryId + '/bg'}, function(err, upRes) {
						if(err) {
							return catD.reject();
						}

						for( var lang in cat.localized){
							var localized = cat.localized[lang];
							localized.backgroundImage = {};

							mediaServices.setImageObjectSource(localized.backgroundImage, upRes);
						}
						cat.backgroundImage = {};
						mediaServices.setImageObjectSource(cat.backgroundImage, upRes);
						catD.resolve(true);
					});
				}
				//category button image
				var bgFiles2 = glob.sync(groupFile + '/categories/' + cat.categoryId + '/button.*');
				if(bgFiles2.length > 0) {
					var catD2 = q.defer();
					promises.push(catD2.promise);
					mediaServices.uploadHotelGroupImage(hgId, bgFiles2[0], {public_id: 'categories/' + cat.categoryId + '/button'}, function(err, upRes) {
						if(err) {
							return catD2.reject();
						}
						for( var lang in cat.localized){
							var localized = cat.localized[lang];
							localized.buttonImage = {};

							mediaServices.setImageObjectSource(localized.buttonImage, upRes);
						}
						cat.buttonImage = {};
						mediaServices.setImageObjectSource(cat.buttonImage, upRes);
						catD2.resolve(true);
					});
				}
			}

			/**
			 * Images for main menu items
			 */
			function im4Mm(item, imageFile, bgFile, ssFiles) {
				if(imageFile){
					var mmD = q.defer();
					promises.push(mmD.promise);
					mediaServices.uploadHotelGroupImage(hgId, imageFile, {public_id: 'main_menu/' + item.menuItemId + '/image'}, function(err, upRes) {
						if(err) {
							return mmD.reject();
						}
						for( var lang in item.localized){
							var localized = item.localized[lang];
							localized.buttonImage = {};

							mediaServices.setImageObjectSource(localized.buttonImage, upRes);
						}
						item.buttonImage = {};
						mediaServices.setImageObjectSource(item.buttonImage, upRes);
						mmD.resolve(true);
					});
				}

				if(bgFile){
					console.log('HAS BG',item.menuItemId);
					var mmD2 = q.defer();
					promises.push(mmD2.promise);
					mediaServices.uploadHotelGroupImage(hgId, bgFile, {public_id: 'main_menu/' + item.menuItemId + '/bg'}, function(err, upRes) {
						if(err) {
							return mmD2.reject();
						}
						for( var lang in item.localized){
							var localized = item.localized[lang];
							localized.backgroundImage = {};

							mediaServices.setImageObjectSource(localized.backgroundImage, upRes);
						}
						item.backgroundImage = {};
						mediaServices.setImageObjectSource(item.backgroundImage, upRes);
						mmD2.resolve(true);
					});
				}


				function uplSlide(mmD3, img, file, index){
					mediaServices.uploadHotelGroupImage(hgId, file, {public_id: img.id}, function (err, upRes) {
						if (err) {
							console.log('Cannot upload file', err);
							return mmD3.reject();
						}
						mediaServices.setImageObjectSource(img, upRes);

						for (var lang in item.localized) {
							var localized = item.localized[lang];
							localized.slideshowImages[index] = img;
						}
						item.slideshowImages[index] = img;
						mmD3.resolve(true);
					});
				}

				if(ssFiles){
					if(ssFiles.length > 1) {
						console.log('HAS SS', item.menuItemId);
						item.slideshowImages = [];
						ssFiles.forEach(function (file, index) {
							var mmD3 = q.defer();
							var fn = 'slideshow/' + index;
							var public_id = 'main_menu/' + item.menuItemId + '/' + fn;
							var img = {	id: public_id };

							promises.push(mmD3.promise);
							uplSlide(mmD3, img, file, index);

						});
					}
				}

			}

			data.categories.forEach(function(cat) {
				im4cat(cat);
			});

			function parseMenuItem(item){
				console.log('Images for main menu item', item.menuItemId);
				//category button image
				var mmFiles = glob.sync(groupFile + '/main_menu/' + item.menuItemId + '/image.*');
				var mmFiles2 = glob.sync(groupFile + '/main_menu/' + item.menuItemId + '/bg.*');
				var mmFiles3 = glob.sync(groupFile + '/main_menu/' + item.menuItemId + '/ssImage*.*');
				im4Mm(item, mmFiles.length > 0 ? mmFiles[0] : null, mmFiles2.length > 0 ? mmFiles2[0] : null, mmFiles3.length > 0 ? mmFiles3 : null);
			}

			for(var m in data.mainMenu.items) {
				parseMenuItem(data.mainMenu.items[m]);
			}
			for(var c in data.mainMenu.customItems) {
				parseMenuItem(data.mainMenu.customItems[c]);
			}

			/**
			 * If all done, save in database
			 */
			var allDone1 = q.allSettled(promises);
			allDone1.catch(function() {
				console.log('Some hotel group promises failed');
			});
			allDone1.done(function() {
				console.log('Saving hotel group...');
				hgServices.createTestHotelGroup(data, function(err) {
					if(err) {
						if(err.data.code === 'ver-no-change'){
							console.log('No changes', data.hotelGroupId);
						}else{
							return console.log('Failed', data.hotelGroupId, err);
						}

					}
					console.log('OK, saving hotels...');

					var hotelDirs = glob.sync(groupFile + '/hotels/*');
					var groupPromises = [];
					if(hotelDirs.length > 0) {
						hotelDirs.forEach(function(hotelDir, hid) {
							var hotelPromise = q.defer();
							groupPromises.push(hotelPromise.promise);
							var hotelPromises = [];
							/**
							 * Load hotel data
							 */
							var hotelData = JSON.parse(fs.readFileSync(hotelDir + '/data.json', {encoding: 'utf-8'}));


							hotelData.meta={
								'scheduledAt': "2015-07-10T11:40:24.404Z",
								'status': 'published'
							};
							console.log('Hotel ', hotelData.hotelId);


							/**
							 * Load gallery
							 */
							var hiG = glob.sync(hotelDir + '/gallery*.*');
							hotelData.gallery = [];
							for( var lang in hotelData.localized) {
								hotelData.localized[lang].gallery = [];
							}
							if(hiG.length > 0) {
								hiG.forEach(function(galleryFile, index) {
									var hiD2 = q.defer();
									hotelPromises.push(hiD2.promise);
									mediaServices.uploadHotelImage(hgId, hotelData.hotelId, galleryFile, {public_id: 'gallery/' + index}, function(err, upRes) {
										if(err) {
											return hiD2.reject();
										}
										for( var lang in hotelData.localized){
											var localized = hotelData.localized[lang];
											localized.gallery[index] = {};

											mediaServices.setImageObjectSource(localized.gallery[index], upRes);
										}
										var img = {};
										mediaServices.setImageObjectSource(img, upRes);
										hotelData.gallery[index] = img;
										hiD2.resolve(true);
									});
								});
							}

							/**
							 * Save hotel to database
							 */

							var allDone = q.allSettled(hotelPromises);

							allDone.done(function() {
								console.log('Saving hotel...', hotelData.hotelId);
								hotelsServices.createOrUpdateHotel(hotelData, data, function(err) {
									if(err) {
										if(err.data.code !== 'ver-no-change'){
											hotelPromise.reject(err);
											return console.log('Failed', hotelData.hotelId, err);
										}else{
											console.log('No changes', hotelData.hotelId);
										}

									}
									console.log('OK', hotelData.hotelId);

									try{
										var rG = JSON.parse(fs.readFileSync(hotelDir + '/reservations.json', {encoding: 'utf-8'}));

									}catch(e){
										hotelPromise.resolve(1);
										return console.log('Reservation json is missing');
									}
									var resPromises = [];
									var doIt = function(resPromise, reservation){
										reservations.saveReservation(reservation.reference,
											reservation.hotelId,
											reservation.hotelGroupId,
											reservation.reservation,
											null,
											function(errRes){
												if(errRes){
													resPromise.reject(errRes);
													return console.log('Failed', reservation.reference.referenceNumber, errRes);
												}
												resPromise.resolve();
												return console.log('OK', reservation.reference.referenceNumber);
											}
										);
									};
									for(var resIndex in rG ){
										var reservation = rG[resIndex];
										var resPromise = q.defer();
										resPromises.push(resPromise.promise);

										console.log('Saving reservation ...', reservation.reference.referenceNumber);
										doIt(resPromise, reservation);
									}
									var resDone = q.allSettled(resPromises);

									resDone.done(function(){
										hotelPromise.resolve(1);
									}, function(err){
										hotelPromise.reject(err)
										return console.log('Some reservation failed', err);
									});
									resDone.catch(function () {
										return console.log('Some reservation failed', err);
									});
								});

							}, function(err){
								console.log('Test', err);

							});
							allDone.catch(function() {
								console.log('Some promises failed!', hotelData.hotelId);
							});
						});
					}
					q.allSettled(groupPromises).done(function() {
						hgPromise.resolve(1);
					}, function(err){
						console.log('Test', err);
					});
				});
			});
		});
		q.allSettled(hgPromises).done(function() {
			console.log('All done, time to hotel group goLive!');
			jobhandler.createJob('hotelGroupgolive', 'htlgrpgolive', {}, 0, function(err, job){
				if(err){
					console.log('Hotel Group golive scheduled job creation failed', err);
					callback(0);
				}
				scheduler.executeJobAndWaitForCompletion(job, function(err){
					if(err){
						console.log('Hotel Group golive job execution failed', err);
						callback(0);
					}
					jobhandler.createJob('hotelgolive','hotelgolive', {}, 0, function(err, job){
						if(err){
							console.log('Create scheduled job failed!', err);
							callback(0);
						}
						scheduler.executeJobAndWaitForCompletion(job, function(err){
							if (err) {
								console.log('Hotel go live went wrong', err);
							}
							console.log('Hotel goLive completed. All done'); //TODO: Remove
							callback(0);
						});
					});
				});
			});
		},function(e){
			console.log('Error!', e);
		});

	});
}

exports.up = function(pgm, done) {
	profile.init(function(err) {
		if(err) {
			logging.emerg('Profile init failed', err);
		} else {
			populate(done);
		}
	});
};

exports.down = function(pgm) {
	pgm.sql('TRUNCATE TABLE ss_data');
	pgm.sql('TRUNCATE TABLE reservation');
	pgm.sql('TRUNCATE TABLE profile_session');
	pgm.sql('TRUNCATE TABLE configuration');
	pgm.sql('TRUNCATE TABLE app_instance');
	pgm.sql('TRUNCATE TABLE hotel');
	pgm.sql('TRUNCATE TABLE hotel_v');
	pgm.sql('TRUNCATE TABLE hotel_group');
	pgm.sql('TRUNCATE TABLE hotel_group_v');
};
