'use strict';
var hgServices = require('../src/modules/hotel_groups/services');
var version = require('../src/modules/versioning/services');
var media = require('../src/modules/media/services');
var path = require('path');
require('../src/listeners');


exports.up = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}
		media.uploadHotelGroupImage(
				hotelGroup.data.hotelGroupId,
				path.join(__dirname, '../data/hotel_groups/slh/main_menu/mm-logo/image2.png'),
				{public_id: 'main_menu/mm-logo/image'},
				function(err, upRes){
					if(err){
						console.log('Error', err.message, err);
						throw err;
					}
					for(var lang in hotelGroup.data.mainMenu.customItems.logo.localized){
						media.setImageObjectSource(hotelGroup.data.mainMenu.customItems.logo.localized[lang].buttonImage, upRes);
					}

					hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
						if(err){
							console.log('Error', err.message, err);
							throw err;
						}
						version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
							if(err){
								console.log('Error', err.message, err);
								throw err;
							}
							console.log('Updated', res, resU);
							setTimeout(done, 1000);
						});

					});

				},
				hotelGroup.data.mainMenu.customItems.logo.localized.en_GB.buttonImage.source
		);

 	});
};

exports.down = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}
		media.uploadHotelGroupImage(
				hotelGroup.data.hotelGroupId,
				path.join(__dirname, '../data/hotel_groups/slh/main_menu/mm-logo/image.png'),
				{public_id: 'main_menu/mm-logo/image'},
				function(err, upRes){
					if(err){
						console.log('Error', err.message, err);
						throw err;
					}
					for(var lang in hotelGroup.data.mainMenu.customItems.logo.localized){
						media.setImageObjectSource(hotelGroup.data.mainMenu.customItems.logo.localized[lang].buttonImage, upRes);
					}

					hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
						if(err){
							console.log('Error', err.message, err);
							throw err;
						}
						version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
							if(err){
								console.log('Error', err.message, err);
								throw err;
							}
							console.log('Updated', res, resU);
							setTimeout(done, 1000);
						});

					});

				},
				hotelGroup.data.mainMenu.customItems.logo.localized.en_GB.buttonImage.source
		);
	})
};
