'use strict';
var hgServices = require('../src/modules/hotel_groups/services');
var version = require('../src/modules/versioning/services');
var media = require('../src/modules/media/services');
var path = require('path');
var q = require('q');
require('../src/listeners');


exports.up = function(pgm, done) {

	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}

		var promises = [];

		var p1 = q.defer();
		var p2 = q.defer();
		var p3 = q.defer();
		promises.push(p1.promise);
		promises.push(p2.promise);
		promises.push(p3.promise);

		//save if all done
		q.all(promises).done(function(){
			hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
				if(err){
					console.log('Error', err.message, err);
					throw err;
				}
				version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
					if(err){
						console.log('Error', err.message, err);
						throw err;
					}
					console.log('Updated', res, resU);
					setTimeout(done, 1000);
				});

			});
		}, function(err){
			console.log('Error', err.message, err);
			throw err;
		});

		/**
		 * 1. Update mm-ex image back to original
		 */
		media.uploadHotelGroupImage(
				hotelGroup.data.hotelGroupId,
				path.join(__dirname, '../data/hotel_groups/slh/main_menu/mm-ex/image3.png'),
				{public_id: 'main_menu/mm-ex/image'},
				function(err, upRes){
					if(err){
						return p1.reject(err);
					}
					for(var lang in hotelGroup.data.mainMenu.items[2].localized){
						media.setImageObjectSource(hotelGroup.data.mainMenu.items[2].localized[lang].buttonImage, upRes);
					}

					return p1.resolve();
				},
				hotelGroup.data.mainMenu.items[2].localized.en_GB.buttonImage.source
		);

		/**
		 * 2. Update phone text and update Map to Near Me and PrivacyButton
		 */
		try{
			hotelGroup.data.translations.localized.en_GB.PhonePageText = 'If you wish to amend or cancel your reservation please call us to arrange or visit SLH.com.\n' + hotelGroup.data.translations.localized.en_GB.PhonePageText;
			hotelGroup.data.translations.localized.en_GB.PrivacyButton = 'SLH privacy policy';

			hotelGroup.data.mainMenu.items[3].localized.en_GB.name = 'Near me';

			p2.resolve();
		}catch(e){
			p2.reject(err);
		}

		/**
		 * 3. Update categories/slh-exp-cice/button
		 */

		var updCat = null;
		hotelGroup.data.categories.forEach(function(cat) {
			if(cat.categoryId === 'slh-exp-cice') {
				updCat = cat;
			}
		});
		if(updCat){
			//removed, because it is doing wrong thing
			p3.resolve();
			//media.uploadHotelGroupImage(
			//		hotelGroup.data.hotelGroupId,
			//		path.join(__dirname, '../data/hotel_groups/slh/categories/slh-exp-cice/button2.png'),
			//		{public_id: 'categories/slh-exp-cice/button'},
			//		function(err, upRes){
			//			if(err){
			//				return p3.reject(err);
			//			}
			//			//update values
			//			for(var lang in updCat.localized){
			//				media.setImageObjectSource(updCat.localized[lang].buttonImage, upRes);
			//			}
			//
			//			return p3.resolve();
			//
			//		},
			//		updCat.localized.en_GB.buttonImage.source
			//);
		}else{
			p3.resolve();
		}



	});
};

exports.down = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}

		var promises = [];

		var p1 = q.defer();
		var p2 = q.defer();
		var p3 = q.defer();
		promises.push(p1.promise);
		promises.push(p2.promise);
		promises.push(p3.promise);

		//save if all done
		q.all(promises).done(function(){
			hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
				if(err){
					console.log('Error', err.message, err);
					throw err;
				}
				version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
					if(err){
						console.log('Error', err.message, err);
						throw err;
					}
					console.log('Updated', res, resU);
					setTimeout(done, 1000);
				});

			});
		}, function(err){
			console.log('Error', err.message, err);
			throw err;
		});

		/**
		 * 1. Update mm-ex image back to original (REVERT)
		 */
		media.uploadHotelGroupImage(
				hotelGroup.data.hotelGroupId,
				path.join(__dirname, '../data/hotel_groups/slh/main_menu/mm-ex/image2.png'),
				{public_id: 'main_menu/mm-ex/image'},
				function(err, upRes){
					if(err){
						return p1.reject(err);
					}
					for(var lang in hotelGroup.data.mainMenu.items[2].localized){
						media.setImageObjectSource(hotelGroup.data.mainMenu.items[2].localized[lang].buttonImage, upRes);
					}

					return p1.resolve();
				},
				hotelGroup.data.mainMenu.items[2].localized.en_GB.buttonImage.source
		);

		/**
		 * 2. Update phone text and update Map to Near Me and PrivacyButton (REVERT)
		 */
		try{
			hotelGroup.data.translations.localized.en_GB.PhonePageText = hotelGroup.data.translations.localized.en_GB.PhonePageText.replace('If you wish to amend or cancel your reservation please call us to arrange or visit SLH.com.\n', '');
			hotelGroup.data.mainMenu.items[3].localized.en_GB.name = 'Map';
			delete hotelGroup.data.translations.localized.en_GB.PrivacyButton;


			p2.resolve();
		}catch(e){
			p2.reject(err);
		}

		/**
		 * 3. Update categories/slh-exp-cice/button (REVERT)
		 */

		var updCat = null;
		hotelGroup.data.categories.forEach(function(cat) {
			if(cat.categoryId === 'slh-exp-cice') {
				updCat = cat;
			}
		});
		if(updCat){
			//removed, because it is doing wrong thing
			p3.resolve();
			//media.uploadHotelGroupImage(
			//		hotelGroup.data.hotelGroupId,
			//		path.join(__dirname, '../data/hotel_groups/slh/categories/slh-exp-cice/button.png'),
			//		{public_id: 'categories/slh-exp-cice/button'},
			//		function(err, upRes){
			//			if(err){
			//				return p3.reject(err);
			//			}
			//			//update values
			//			for(var lang in updCat.localized){
			//				media.setImageObjectSource(updCat.localized[lang].buttonImage, upRes);
			//			}
			//
			//			return p3.resolve();
			//
			//		},
			//		updCat.localized.en_GB.buttonImage.source
			//);
		}else{
			p3.resolve();
		}



	});
};
