'use strict';
var hgServices = require('../src/modules/hotel_groups/services');
var hotelServices = require('../src/modules/hotels/services');
var version = require('../src/modules/versioning/services');
var media = require('../src/modules/media/services');
var path = require('path');
var q = require('q');
require('../src/listeners');


exports.up = function(pgm, done) {

	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}
		hotelServices.getHotelsRaw('slh', function(err, res){
			if(err){
				console.log('Error', err.message, err);
				throw err;
			}
			var promises = [];
			var p1 = q.defer();
			promises.push(p1.promise);

			res.forEach(function(hotel, i){
				var p = q.defer();
				promises.push(p.promise);
				promises[i].then(function(){
					hotel.data.localized.forEach(function(localized){
						localized.bookingUrl = localized.bookingUrl.replace('https://www.yourreservation.net/tb3/index.cfm?bf=SLHchain&arrivalDate=', 'https://www.yourreservation.net/tb3/mobile/index.cfm?Bf=MSLHChain&arrivalDate=')
					});
					hotelServices.updateHotel(hotel.data, hotelGroup.data, function(err){

						if(err && err.data.code !== 'ver-no-change'){
							console.log('Error', err.message, err);
							return p.reject(err);
						}
						console.log('Updated hotel', hotel.data.hotelId);
						hotel = null;
						p.resolve();
					});
				});

			});
			p1.resolve();

			//save if all done
			q.all(promises).done(function(){
				version.handleGoLive('hotel_v', 'hotel', function(err, res){
					if(err){
						console.log('Error', err.message, err);
						throw err;
					}
					console.log('Updated', res, res);
					setTimeout(done, 1000);
				});
			}, function(err){
				console.log('Error', err.message, err);
				throw err;
			});

		});
	});


};

exports.down = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}
		hotelServices.getHotelsRaw('slh', function(err, res){
			if(err){
				console.log('Error', err.message, err);
				throw err;
			}
			var promises = [];
			var p1 = q.defer();
			promises.push(p1.promise);

			res.forEach(function(hotel, i){
				var p = q.defer();
				promises.push(p.promise);
				promises[i].then(function(){
					hotel.data.localized.forEach(function(localized){
						localized.bookingUrl = localized.bookingUrl.replace('https://www.yourreservation.net/tb3/mobile/index.cfm?Bf=MSLHChain&arrivalDate=', 'https://www.yourreservation.net/tb3/index.cfm?bf=SLHchain&arrivalDate=')
					});
					hotelServices.updateHotel(hotel.data, hotelGroup.data, function(err){

						if(err && err.data.code !== 'ver-no-change'){
							console.log('Error', err.message, err);
							return p.reject(err);
						}
						console.log('Updated hotel', hotel.data.hotelId);
						hotel = null;
						p.resolve();
					});
				});

			});
			p1.resolve();

			//save if all done
			q.all(promises).done(function(){
				version.handleGoLive('hotel_v', 'hotel', function(err, res){
					if(err){
						console.log('Error', err.message, err);
						throw err;
					}
					console.log('Updated', res, res);
					setTimeout(done, 1000);
				});
			}, function(err){
				console.log('Error', err.message, err);
				throw err;
			});

		});
	});

};
