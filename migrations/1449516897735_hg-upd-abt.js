'use strict';
var hgServices = require('../src/modules/hotel_groups/services');
var version = require('../src/modules/versioning/services');

require('../src/listeners');


exports.up = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}
		hotelGroup.data.translations.localized.en_GB.AboutButton = 'About SLH';
		hotelGroup.data.translations.localized.en_GB.AboutText = 'The Small Luxury Hotels of the World app has been created to match independently minded travellers with over 520 independently spirited hotels around the world. \n\nWith a collection spanning more than 80 countries the variety of hotels and the experiences they offer is exceptional and offers an authentic way to discover a destination or culture, safe in the knowledge that whether it is a cutting-edge design hotel, historic country mansion, city centre sanctuary or remote private island it is the very best of the best.\n\nWhether you are looking for inspiration or are ready to book your next hotel stay, the Small Luxury Hotels of the World app puts luxury hotels at your fingertips.\n\nKey features include:\n \n•	Finding your perfect luxury hotel has never been easier – search over 520 hotels by hotel name, destination or experience using interactive maps.\n\n•	Find all the information you need about each hotel including its rooms, restaurants, facilities, services and location – all with beautifully written copy and inspirational photographs.\n\n•	Use maps for turn-by-turn directions to navigate you to your hotel of choice.\n\n•	Book your stay using the app or click to call our local reservations team.\n\n•	Sign up to The Club of Small Luxury Hotels of the World to enjoy great benefits every time you book including room upgrades.\n\n•	Share your favourite hotels with your friends by email and social media including Twitter and Facebook.\n\nEach and every one of our hotels shares a commitment to the highest standards and most memorable experiences.  We look forward to welcoming you to one of our hotels in the very near future.';
		hotelGroup.data.viewConfiguration.profileMenu = [{
			"action": "conditions",
			"removeIfDisabled": false,
			"link": "http://www.slh.com/about-us/terms-of-use/"
		}, {
			"action": "privacy",
			"removeIfDisabled": false,
			"link": "http://www.slh.com/about-us/privacy-policy/"
		}, {
			"action": "about",
			"removeIfDisabled": false
		}];

		hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
			if(err){
				console.log('Error', err.message, err);
				throw err;
			}
			version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
				if(err){
					console.log('Error', err.message, err);
					throw err;
				}
				console.log('Updated', res, resU);
				setTimeout(done, 1000);
			});

		});
 	});
};

exports.down = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}
		delete hotelGroup.data.translations.localized.en_GB.AboutButton;
		delete hotelGroup.data.translations.localized.en_GB.AboutText;
		hotelGroup.data.viewConfiguration.profileMenu = [{
			"action": "conditions",
			"removeIfDisabled": false,
			"link": "http://www.slh.com/about-us/terms-of-use/"
		}, {
			"action": "privacy",
			"removeIfDisabled": false,
			"link": "http://www.slh.com/about-us/privacy-policy/"
		}];

		hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
			if(err){
				console.log('Error', err.message, err);
				throw err;
			}
			version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
				if(err){
					console.log('Error', err.message, err);
					throw err;
				}
				console.log('Updated', res, resU);
				setTimeout(done, 1000);
			});
		});
	})
};
