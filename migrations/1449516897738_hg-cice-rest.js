'use strict';
var hgServices = require('../src/modules/hotel_groups/services');
var version = require('../src/modules/versioning/services');
var media = require('../src/modules/media/services');
var path = require('path');
var q = require('q');
require('../src/listeners');


exports.up = function(pgm, done) {

	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}

		var promises = [];

		var p3 = q.defer();

		promises.push(p3.promise);


		//save if all done
		q.all(promises).done(function(){
			hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
				if(err && err.data.code !== 'ver-no-change'){
					console.log('Error', err.message, err);
					throw err;
				}
				version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
					if(err){
						console.log('Error', err.message, err);
						throw err;
					}
					console.log('Updated', res, resU);
					setTimeout(done, 1000);
				});

			});
		}, function(err){
			console.log('Error', err.message, err);
			throw err;
		});


		/**
		 * 3. Update categories/slh-exp-cice/button
		 */

		var updCat = null;
		hotelGroup.data.categories.forEach(function(cat) {
			if(cat.categoryId === 'slh-exp-cice') {
				updCat = cat;
			}
		});
		if(updCat){
			media.uploadHotelGroupImage(
					hotelGroup.data.hotelGroupId,
					path.join(__dirname, '../data/hotel_groups/slh/categories/slh-exp-cice/button.png'),
					{public_id: 'categories/slh-exp-cice/button'},
					function(err, upRes){
						if(err){
							return p3.reject(err);
						}
						//update values
						for(var lang in updCat.localized){
							media.setImageObjectSource(updCat.localized[lang].buttonImage, upRes);
						}

						return p3.resolve();

					},
					updCat.localized.en_GB.buttonImage.source
			);
		}else{
			p3.resolve();
		}



	});
};

exports.down = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}

		var promises = [];

		var p3 = q.defer();

		promises.push(p3.promise);


		//save if all done
		q.all(promises).done(function(){
			hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
				if(err && err.data.code !== 'ver-no-change'){
					console.log('Error', err.message, err);
					throw err;
				}
				version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
					if(err){
						console.log('Error', err.message, err);
						throw err;
					}
					console.log('Updated', res, resU);
					setTimeout(done, 1000);
				});

			});
		}, function(err){
			console.log('Error', err.message, err);
			throw err;
		});


		/**
		 * 3. Update categories/slh-exp-cice/button (REVERT)
		 */

		var updCat = null;
		hotelGroup.data.categories.forEach(function(cat) {
			if(cat.categoryId === 'slh-exp-cice') {
				updCat = cat;
			}
		});
		if(updCat){
			media.uploadHotelGroupImage(
					hotelGroup.data.hotelGroupId,
					path.join(__dirname, '../data/hotel_groups/slh/categories/slh-exp-cice/button_old.png'),
					{public_id: 'categories/slh-exp-cice/button'},
					function(err, upRes){
						if(err){
							return p3.reject(err);
						}
						//update values
						for(var lang in updCat.localized){
							media.setImageObjectSource(updCat.localized[lang].buttonImage, upRes);
						}

						return p3.resolve();

					},
					updCat.localized.en_GB.buttonImage.source
			);
		}else{
			p3.resolve();
		}



	});
};
