'use strict';
var hgServices = require('../src/modules/hotel_groups/services');
var version = require('../src/modules/versioning/services');
var media = require('../src/modules/media/services');
var path = require('path');
var q = require('q');
require('../src/listeners');


exports.up = function(pgm, done) {

	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}

		var promises = [];

		var p3 = q.defer();

		promises.push(p3.promise);


		//save if all done
		q.all(promises).done(function(){
			hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
				if(err && err.data.code !== 'ver-no-change'){
					console.log('Error', err.message, err);
					throw err;
				}
				version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
					if(err){
						console.log('Error', err.message, err);
						throw err;
					}
					console.log('Updated', res, resU);
					setTimeout(done, 1000);
				});

			});
		}, function(err){
			console.log('Error', err.message, err);
			throw err;
		});

		hotelGroup.data.translations.localized.en_GB.AboutText = 'Small Luxury Hotels of the World™ matches independently minded guests with independently spirited hotels.\n \nOur diverse collection of over 520 hotels in more than 80 countries around the world includes everything from cutting edge design hotels and city centre sanctuaries to historic country mansions and remote private islands – all our hotels are consistently different, however, they are all united by the fact that they offer the best locations, highest quality, personalised service and a truly authentic way to discover a destination.\n \nOnly in small, independently owned hotels can guests experience a true sense of history, of hospitality, of design and of nature, a true taste of the cuisine and a true feel for the local culture.\n \nFor more information visit slh.com';
		hotelGroup.data.translations.localized.en_GB.AboutButton = 'About Small Luxury Hotels of the World';


		/**
		 * 3. Update categories/slh-exp-cice/button
		 */

		var updCat = null;
		hotelGroup.data.categories.forEach(function(cat) {
			if(cat.categoryId === 'slh-exp-cice') {
				updCat = cat;
			}
		});
		if(updCat){
			media.uploadHotelGroupImage(
					hotelGroup.data.hotelGroupId,
					path.join(__dirname, '../data/hotel_groups/slh/categories/slh-exp-cice/button.png'),
					{public_id: 'categories/slh-exp-cice/button'},
					function(err, upRes){
						if(err){
							return p3.reject(err);
						}
						//update values
						for(var lang in updCat.localized){
							media.setImageObjectSource(updCat.localized[lang].buttonImage, upRes);
						}

						return p3.resolve();

					},
					updCat.localized.en_GB.buttonImage.source
			);
		}else{
			p3.resolve();
		}



	});
};

exports.down = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}

		var promises = [];

		var p3 = q.defer();

		promises.push(p3.promise);


		//save if all done
		q.all(promises).done(function(){
			hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
				if(err && err.data.code !== 'ver-no-change'){
					console.log('Error', err.message, err);
					throw err;
				}
				version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
					if(err){
						console.log('Error', err.message, err);
						throw err;
					}
					console.log('Updated', res, resU);
					setTimeout(done, 1000);
				});

			});
		}, function(err){
			console.log('Error', err.message, err);
			throw err;
		});


		/**
		 * 3. Update categories/slh-exp-cice/button (REVERT)
		 */

		hotelGroup.data.translations.localized.en_GB.AboutButton = 'About SLH';
		hotelGroup.data.translations.localized.en_GB.AboutText = 'The Small Luxury Hotels of the World app has been created to match independently minded travellers with over 520 independently spirited hotels around the world. \n\nWith a collection spanning more than 80 countries the variety of hotels and the experiences they offer is exceptional and offers an authentic way to discover a destination or culture, safe in the knowledge that whether it is a cutting-edge design hotel, historic country mansion, city centre sanctuary or remote private island it is the very best of the best.\n\nWhether you are looking for inspiration or are ready to book your next hotel stay, the Small Luxury Hotels of the World app puts luxury hotels at your fingertips.\n\nKey features include:\n \n•	Finding your perfect luxury hotel has never been easier – search over 520 hotels by hotel name, destination or experience using interactive maps.\n\n•	Find all the information you need about each hotel including its rooms, restaurants, facilities, services and location – all with beautifully written copy and inspirational photographs.\n\n•	Use maps for turn-by-turn directions to navigate you to your hotel of choice.\n\n•	Book your stay using the app or click to call our local reservations team.\n\n•	Sign up to The Club of Small Luxury Hotels of the World to enjoy great benefits every time you book including room upgrades.\n\n•	Share your favourite hotels with your friends by email and social media including Twitter and Facebook.\n\nEach and every one of our hotels shares a commitment to the highest standards and most memorable experiences.  We look forward to welcoming you to one of our hotels in the very near future.';

		var updCat = null;
		hotelGroup.data.categories.forEach(function(cat) {
			if(cat.categoryId === 'slh-exp-cice') {
				updCat = cat;
			}
		});
		if(updCat){
			media.uploadHotelGroupImage(
					hotelGroup.data.hotelGroupId,
					path.join(__dirname, '../data/hotel_groups/slh/categories/slh-exp-cice/button_old.png'),
					{public_id: 'categories/slh-exp-cice/button'},
					function(err, upRes){
						if(err){
							return p3.reject(err);
						}
						//update values
						for(var lang in updCat.localized){
							media.setImageObjectSource(updCat.localized[lang].buttonImage, upRes);
						}

						return p3.resolve();

					},
					updCat.localized.en_GB.buttonImage.source
			);
		}else{
			p3.resolve();
		}



	});
};
