'use strict';
var hgServices = require('../src/modules/hotel_groups/services');
var version = require('../src/modules/versioning/services');

require('../src/listeners');


exports.up = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}
		hotelGroup.data.mainMenu.items[3].localized.en_GB.name = 'Map';

		hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
			if(err){
				console.log('Error', err.message, err);
				throw err;
			}
			version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
				if(err){
					console.log('Error', err.message, err);
					throw err;
				}
				console.log('Updated', res, resU);
				setTimeout(done, 1000);
			});

		});
 	});
};

exports.down = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			console.log('Error', err.message, err);
			throw err;
		}
		hotelGroup.data.mainMenu.items[3].localized.en_GB.name = 'Near me';
		hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
			if(err){
				console.log('Error', err.message, err);
				throw err;
			}
			version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
				if(err){
					console.log('Error', err.message, err);
					throw err;
				}
				console.log('Updated', res, resU);
				setTimeout(done, 1000);
			});
		});
	})
};
