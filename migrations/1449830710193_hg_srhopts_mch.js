'use strict';
var hgServices = require('../src/modules/hotel_groups/services');
var version = require('../src/modules/versioning/services');

require('../src/listeners');

exports.up = function(pgm, done) {
	hgServices.getHotelGroup('mchotels', function(err, hotelGroup) {
		if(err) {
			if(err.data.code === 'hg-not-found'){
				return done();
			}
			console.log('Error', err.message, err);
			throw err;
		}
		hotelGroup.data.meta.appliedAt = null;
		hotelGroup.data.meta.scheduledAt = new Date(new Date().getTime() - 3600).toISOString();
		hotelGroup.data.searchConfiguration = {};
		if(hotelGroup.data.hotelGroupId == "slh") {
			hotelGroup.data.searchConfiguration.resultOrdering = 'random';
		}
		hgServices.updateHotelGroup(hotelGroup.data, function(err, resU) {
			if(err) {
				if(err.data.code != 'ver-no-change') {
					console.log('Error', err.message, err);
					throw err;
				}
			}
			version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res) {
				if(err) {
					console.log('Error', err.message, err);
					throw err;
				}
				console.log('Updated', res, resU);
				setTimeout(done, 1000);
			});

		});
	});
};

exports.down = function(pgm, done) {
	hgServices.getHotelGroup('mchotels', function(err, hotelGroup) {
		if(err) {
			if(err.data.code === 'hg-not-found'){
				return done();
			}
			console.log('Error', err.message, err);
			throw err;
		}
		delete hotelGroup.data.searchConfiguration;
		hgServices.updateHotelGroup(hotelGroup.data, function(err, resU) {
			if(err) {
				if(err.data.code != 'ver-no-change') {
					console.log('Error', err.message, err);
					throw err;
				}
			}
			version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res) {
				if(err) {
					console.log('Error', err.message, err);
					throw err;
				}
				console.log('Updated', res, resU);
				setTimeout(done, 1000);
			});
		});
	})
};

