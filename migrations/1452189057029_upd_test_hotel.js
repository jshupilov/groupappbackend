'use strict';
var hotels = require('../src/modules/hotels/services');
var hotelGr = require('../src/modules/hotel_groups/services');
var version = require('../src/modules/versioning/services');

function update(hgId, hData, cb) {
	hotelGr.getHotelGroup(hgId, function (err, hoteGrdata) {
		if (err) {
			console.log('Error', err.message, err);
			throw err;
		}
		hotels.createOrUpdateHotel(hData, hoteGrdata.data, function (err, res) {
			if (err && err.data.code !== 'ver-no-change') {
				console.log('Error', err.message, err);
				throw err;
			}
			version.handleGoLive('hotel_v', 'hotel', function (err, resGo) {
				if (err) {
					console.log('Error', err.message, err);
					throw err;
				}
				console.log('Updated', res, resGo);
				cb();
			});
		});
	});
}

exports.up = function (pgm, done) {
	hotels.getHotelRaw('test-hgid', 'schlossle', function (err, hotel) {
		if (err) {
			if (err.data.code === 'hs-hotel-nf') {
				return done();
			}
			console.log('Error', err.message, err);
			throw err;
		}
		hotel.data.localized.forEach(function (l) {
			l.bookingUrl = 'https://www.yourreservation.net/tb3/mobile/index.cfm?Bf=MSLHChain&arrivalDate=<StartDateIso>&nights=<NoOfNights>&adults=<NoOfAdults>&hotelcode=schlossle';
		});
		update('test-hgid', hotel.data, function () {
			setTimeout(done, 1000);
		});
	});
};

exports.down = function (pgm, done) {
	hotels.getHotelRaw('test-hgid', 'schlossle', function (err, hotel) {
		if (err) {
			if (err.data.code === 'hs-hotel-nf') {
				return done();
			}
			console.log('Error', err.message, err);
			throw err;
		}
		hotel.data.localized.forEach(function (l) {
			l.bookingUrl = 'https://booking.slh.com/en-GB/Room/Availability/?HotelCode=HUTLLTE&StartDate=<StartDate>&NoOfNights=<NoOfNights>&NoOfAdults=<NoOfAdults>';
		});
		update('test-hgid', hotel.data, function () {
			setTimeout(done, 1000);
		});
	});
};
