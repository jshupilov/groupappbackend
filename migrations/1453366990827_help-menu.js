'use strict';
var hgServices = require('../src/modules/hotel_groups/services');
var version = require('../src/modules/versioning/services');

require('../src/listeners');


exports.up = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			if(err.data.code === 'hg-not-found'){
				return done();
			}
			console.log('Error', err.message, err);
			throw err;
		}
		hotelGroup.data.viewConfiguration.profileMenu = [{
			"action": "help",
			"removeIfDisabled": false
		},{
			"action": "conditions",
			"removeIfDisabled": false,
			"link": "http://www.slh.com/about-us/terms-of-use/"
		}, {
			"action": "privacy",
			"removeIfDisabled": false,
			"link": "http://www.slh.com/about-us/privacy-policy/"
		}, {
			"action": "about",
			"removeIfDisabled": false
		}];

		hotelGroup.data.translations.localized.en_GB.Help = 'Please watch this space for hints and tips on getting the most from your new Small Luxury Hotels mobile application.';


		hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
			if(err){
				if(err.data.code === 'ver-no-change'){
					return done();
				}
				console.log('Error', err.message, err);
				throw err;
			}
			version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
				if(err){
					console.log('Error', err.message, err);
					throw err;
				}
				console.log('Updated', res, resU);
				setTimeout(done, 1000);
			});

		});
	});
};

exports.down = function(pgm, done) {
	hgServices.getHotelGroup('slh', function(err, hotelGroup){
		if(err){
			if(err.data.code === 'hg-not-found'){
				return done();
			}
			console.log('Error', err.message, err);
			throw err;
		}
		hotelGroup.data.viewConfiguration.profileMenu = [{
			"action": "conditions",
			"removeIfDisabled": false,
			"link": "http://www.slh.com/about-us/terms-of-use/"
		}, {
			"action": "privacy",
			"removeIfDisabled": false,
			"link": "http://www.slh.com/about-us/privacy-policy/"
		}, {
			"action": "about",
			"removeIfDisabled": false
		}];

		delete hotelGroup.data.translations.localized.en_GB.Help;


		hgServices.updateHotelGroup(hotelGroup.data, function(err, resU){
			if(err){
				if(err.data.code === 'ver-no-change'){
					return done();
				}
				console.log('Error', err.message, err);
				throw err;
			}
			version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
				if(err){
					console.log('Error', err.message, err);
					throw err;
				}
				console.log('Updated', res, resU);
				setTimeout(done, 1000);
			});
		});
	})
};
