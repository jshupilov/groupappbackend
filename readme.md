See [markdown cheat sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) to edit this file!

# Getting Started
1. Install node.js
  * http://nodejs.org/
2. Install dependencies
  * execute `npm install` in root folder of the project
3. Install PostgreSQL version 5.4 locally
  * http://postgresapp.com/
  * Create an empty database
  * For browsing data `PG Commander` is recommended
    * `https://eggerapps.at/pgcommander/`
4. Install Heroku toolbelt
  * https://toolbelt.heroku.com/
  * Verify if it works by typing to shell `heroku`
5. Create .env file, with following contents
```bash
HOST=localhost
PORT=8000

PUBLIC_PROTOCOL=https
PUBLIC_HOST=localhost
PUBLIC_PORT=8000

TLS_KEY=test_server.key
TLS_CERT=test_server.crt

CMD=./node_modules/node-dev/bin/node-dev

ALGOLIASEARCH_API_KEY=585114a9ed831a36745553ccdf054b29
ALGOLIASEARCH_API_KEY_SEARCH=f8f21708a90d76e9e16842708b0331f2
ALGOLIASEARCH_APPLICATION_ID=4HO68M543Z
ALGOLIASEARCH_PREFIX=dev_<ADD YOUR OWN UNIQUE IDENTIFIER>_

CLOUDINARY_URL=cloudinary://681393764758512:bSrjUNccN2SYGEc3CFIsmKmW45c@cardola-estonia
CLOUDINARY_BASE_FOLDER=<ADD YOUR OWN UNIQUE IDENTIFIER>

DATABASE_URL=postgres://localhost/group_app

STORMPATH_API_KEY_ID=2L9IEUADUOR01WP9ZL06CLXBL
STORMPATH_API_KEY_SECRET=HHiXBPQuaeO1tv/VBs2qnLecPUfmz1pib/56J4fO6sE
STORMPATH_URL=<ASK FOR APPLICATION URL>

STORMPATH_DIR_URL=<ASK FOR DIR URL>
STORMPATH_BASE_DIR_URL=<ASK FOR BASE DIR URL>

NEW_RELIC_LICENSE_KEY=091a4672cb0f25e6c9087cc8db91df6b50e78f04
NEW_RELIC_APP_NAME=Group_APP_API_<YOUR NAME>-dev
IP_WHITELIST=127.0.0.1

TEST_CHECKSUM=test

IRON_TOKEN=LO7tgEQQbRnGXFv1NlLI-5Uvrow
IRON_PROJECT_ID=<ASK FOR TOKEN>
IRON_HOST=mq-rackspace-lon.iron.io
HEROKU_API_TOKEN=3d5b0769-c35e-45c6-9812-d2021b1ec160

LEASEWEB_HOST=api.leasewebcdn.com
LEASEWEB_CUSTOMER=<LEASE_WEB_CUSTOMER_NUMBER>
LEASEWEB_SECRET=<LEASE_WEB_API_KEY>
LEASEWEB_ZONE=<LEASE_WEB_ZONE_ID>

```

6. Test, if environment is OK:
  * `npm run-script test-env`
7. Run the Postgres Database migration task (this should be run every time someone updates database)
  * `npm run-script pg-migrate up`
8. The demo data is stored in `data` folder, import it (this should be done only once)
  * `npm run-script import-test-data`
9. Run the tests to validate that everything is set up correct
   * `npm test`
   * Check if test result is green
10. Run the nodeJS server with `foreman`
  * `foreman start`
  * Validate that the server is running by hitting the endpoint, which is displayed in console
  * Should also work with `npm start`
  * server is automatically restarted if you change code

# Scripts and commands
- `npm test` - run the Eslint, unit tests and display results in shell
- `npm run-script test-html` - run the unit tests and export results to coverage.html file
- `npm run-script test-module {filePath}` - run the unit tests on a certain file or directory, without coverage and linting report
- `npm run-script test-env` - run the tests, which check, if environment variables are correct
- `npm run-script pg-migrate <CMD>` - run the database migration task [read more](https://github.com/theoephraim/node-pg-migrate)
possible commands are:
  - `npm run-script pg-migrate create {migration-name}` - creates a new migration file with the name you give it. Spaces and underscores will be replaced by dashes and a timestamp is prepended to your file name.
  - `npm run-script pg-migrate up` - runs all up migrations from the current state.
  - `npm run-script pg-migrate up {N}` - runs N up mi grations from the current state.
  - `npm run-script pg-migrate down` - runs a single down migration.
  - `npm run-script pg-migrate down {N}` - runs N down migrations from the current state.
- `npm run-script import-test-data {1/0 addWatermarks} {1/0 addTextPrefixes} [{hotelGroupId}]`- imports test data from /data directory (via event hooks also other needed data is created)
- `npm run-script crate-test-data {nrGroups} {nrHotels} {noTest}`- creates extra set of test data based on existing test data, accepts params (nrGroups, nrHotels, noTest)
- `npm run-script delete-data` - deletes all data from database (via event hooks also other related data is removed - search indexes and images in cloud, etc)
- `npm run-script reset-data {1/0 addWatermarks} {1/0 addTextPrefixes} [{hotelGroupId}]` - deletes all data and imports test data, same as running previous two commands
- `npm start` - starts the server

# Development
## Writing tests
All the code under src/ folder should be covered with unit tests 100%.
For testing, we use the following libraries:
- [eslint](http://eslint.org/) - For linting (validating the syntax) the code
- [lab](https://github.com/hapijs/lab) - For running tests
- [code](https://github.com/hapijs/code) - For assertion

## Writing API documentation
We use [apidoc](http://apidocjs.com/) for writing documentation.
Under module folder (src/modules/*/) we have apidoc.js for LATEST API documentation.
Also we have _apidoc.js file under module, where we copy over old versions of the api documentation - this is done to enable comparison of different versions.

## Updating database
Use `npm run-script pg-migrate create {migration-name}` to create a database migration script, then edit the file
under /migrations folder and add it to GIT. To execute the database migration script simply do `npm run-script pg-migrate up`.
Read more about the [pg-migrate here](https://github.com/theoephraim/node-pg-migrate).

## Creating .env variables
Make sure you improve the test file under src/test/env.js to check if new env variables are set.

## Logging
Use `npm install winston` and `npm install winston-papertrail` and place the following lines in the `dependencies` of your package.json file: `"winston": "^1.0.0"` and `"winston-papertrail": "^1.0.1"`
To configure logging with Papertrail. (https://papertrailapp.com)

## Scheduling tasks and doing work in background
We use a process model, which involves a clock process (see Procfile) ,messaging queue (ironMQ) and a worker.
The clock process (src/clock.js) schedules any periodic jobs at the right time to the "scheduled_jobs" queue.
You can also schedule new jobs through the worker/scheduler module.
CURRENTLY the clock process also listens to the new messages in the "scheduled_jobs" queue and either starts a new one-off dyno to complete the job (in Heroku) or creates a child process to complete the job (in local env) - for both of them the job is executed through src/worker.js file.
 It is structured so, because this way we can run one-off dynos and save resources.
 If a job is finished in the background is is put to the  "completed_jobs" queue.
 If there is an error executing the job, the message is put in the "failed_jobs" queue

## Entity versioning
Before versioning can be implemented, versioned table structure must be created. In order to do that, create new pg-migration file.
### Migration file
* 'src/modules/versioning/migrations.js' and 'q' must be required
* Should contain up and down methods.
* up - migrations.makeVersion('tableName', callback);
* down - migrations.dropVersion('tableName', callback);
* promises must be used and callback should resolve promises
* after all promises are done, run() method must be executed

### versioning implementation
Versioned information must be saved to Database via versioning/services method addItem(). With parameters: data, table, callback



# Deployment and Continuous integration
TODO

