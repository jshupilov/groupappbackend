14.01.2016 - Version 1.7.4
=========================
* Story
    * [WB-672] - Backend: Group App: Point "Schlossle" hotel to Trust Booker in test-hgid
        * **This only applies to dev and staging environment**
    * [WB-676] - Reports improvements
        * Added grid lines
        * Automatic sending of reports every monday
        * Changed job definition to use days instead of timestamps
    * [WB-701] - Error when comparing images (u-err-no-img) - should not block hotel update if gallery not empty

* Task
    * [WB-703] - Change booking engine URL
    * Some Unit tests improved for better stability



11.01.2016 - Version 1.7.3
=========================
* Bug
    * [WB-697] - Pictures are not updating from the web importer
    * [WB-698] - hotel/X/contacts sometimes returns phone nr of Germany

* Story
    * [WB-597] - Leaseweb requests should have retry logics


19.12.2015 - Version 1.7.2
=========================
** Story
    * [WB-522] - Report of first run of the app
    * [WB-523] - Report on hotel impressions
    * [WB-660] - Manual creation of reports (npm run-script)


17.12.2015 - Version 1.7.1
=========================
* Sub-task
    * [WB-639] - SLH: Unique qualities: 'nbsp;' is shown in the app
    * [WB-641] - SLH: Spa: Extra space before treatments
    * [WB-646] - SLH: Spa: Left alignment

* Bug
    * [WB-675] - Access logs downloader fails to DL big files

* Story
    * [WB-656] - Complete utils unit tests
    * [WB-668] - Make hotel search results in random order for slh
    * [WB-679] - Import golden taglines for every hotel
    * [WB-680] - slh.com importer should not import list of rooms

* Tasks
	* Some error translations fixes (. and !)
	* Some unit tests added and fixed for better test coverage
	* Web import script improved to support importing only listed hotels
	* Post install hook now does data migration automatically, which helps deliver near zero downtime releases
	* Testing of remote environment improved for better security
	* Requests logging fix to improve performance
	* Hotels blacklist population and hotels un publishing functionality now runs automatically (configuration fixed)



09.12.2015 - Version 1.7.0
=========================
This is the first release to the production environment.

* Tasks
	* SLH: About section added
	* User errors updated
	* SLH: City Center button image changed multiple times
	* SLH: Main menu Experiences image changed
	* SLH: Ninja logo made smaller
	* SLH: Phone page text fixed (weak -> week)
	* SLH: Phone page got more text about reservation cancellation
	* SLH: Privacy page title updated
	* SLH: Near me menu item updated to Map and back to Near me
	* SLH: Booking URL updated
	* SLH: About section updated
	* Some unit tests were optimized to be faster
	* SLH: Website hotels importer improved to use new booking URL
	* Hotel group images are also checked against existing image before updating
	* Removed some unneeded logging
	* Fixed clock process testing to avoid actual processes being executed while doing it
	* Database migrations now happen in the posinstall hook, so "npm install" command will also execute database migrations
	* Data updates how happen as database migration scripts, no changes to the original files will be done



04.12.2015 - Version 1.6.2
=========================

* Story
    * [WB-598] - Update slh website importer, so it will not upload images if they have not changed

* Task
    * SLH: [WB-661] - Change slh logo
    * [WB-662] - Add banner for Terms and Conditions on the main menu
    * [WB-667] - Update search configuration - will only search from hotel name, location and Experiences information
    * SLH: Custom profile menu with Privacy and T&C items
    * SLH: Experiences menu picture change
    * SLH: Ninja logo changed
    * SLH: New background image
    * SLH: Main logo updated
    * SLH: Booking link in hotel view moved to very left
    * SLH: Web importer: update to replace encoded characters & and whitespace
    * SLH: Web importer: Beach, Golf and Spa experiences got "resort" suffix
    * SLH: Web importer: Unesco converted to uppercase





01.12.2015 - Version 1.6.1
=========================

* Bug
    * [WB-547] - Error R14 (Memory quota exceeded) issues caused by 'Near Me' calls
    * [WB-585] - Cloudinary images are not updated
    * [WB-612] - Get slh.com importer gallery order from tabegoryData
    * [WB-613] - Caches unstable: Move cache clearing to the end of go-live process

* Story
    * [WB-518] - Trust Content importer should unpublish hotels in blacklist
    * [WB-520] - Update hotels black list from slh.com
    * [WB-525] - Disable cache warm-up
    * [WB-531] - Backend: GA: Return placeholder image in hotel's gallery if hotel doesn't have any images.
    * [WB-544] - Create reporting functionality
    * [WB-577] - Store Access logs in statistics database
    * [WB-587] - SLH: Replace the existing ninja logo in the Main menu with tagline
    * [WB-590] - Create access logs importer (files are in S3 and log file parsers exist)
    * [WB-611] - Create charting module
    * [WB-614] - Change booking engine URL
    * [WB-624] - Return google analytics tracking code for every hotel group
    * [WB-625] - Improve integration to make requests to slh.com in series not in parallel
    * [WB-626] - Add timestamps to app/registration data
    * [WB-643] - Add text content for hotel/contacts and new translation



20.11.2015 - Version 1.6.0
=========================

* Bug
    * [WB-542] - Floating point in timestamp of new relic log_url

* Story
    * [WB-519] - Make an updateData script
    * [WB-528] - Download hotels html from slh.com
    * [WB-529] - Parse slh.com hotel HTML page to fetch content for hotels.
    * [WB-532] - Change source of slh.com experiences
    * [WB-541] - Change search matching configuration
    * [WB-545] - GA: Main menu: Rename 'Near me'  to 'Map'
    * [WB-546] - Download access logs from leasweb
    * [WB-548] - SLH: Theming: Change Hero Text Default value
    * [WB-567] - Redirect direct traffic to herokuapp.com to cardola.net
    * [WB-568] - Remove slh background images of menu items
    * [WB-570] - SLH: Change categories' images



02.11.2015 - Version 1.5.0
=========================

* Story
    * [WB-466] - Create automatic process, to import hotels for SLH from FTP server
        * Importer is able to import hotels from remote FTP server through XML files
    * [WB-502] - Service to return phone number based on users location
        * Clients location is detected by the IP address, and according call center number is given
    * [WB-503] - Improve Trust Content importer to have a blacklist of hotels
        * **TODO: Use this blacklist in import process**
    * [WB-504] - Change short address format in Trust Content importer
        * Format will change in future
    * [WB-505] - Create experiences - hotels map
    * [WB-508] - Create description block from noteworthy services/facilities/recreation
    * [WB-515] - Import category map from slh.com
    * [WB-516] - SLH: Destination and experience icons/tiles need to be updated for the Conference



09.10.2015 - Version 1.4.5
=========================

* Story
    * [WB-396] - System must import data from TrustContent XML file
    * [WB-403] - Create Reservations storage system
    * [WB-506] - API must return logo in app/main-menu
    * [WB-511] - Change SLH background image
    * [WB-513] - Change SLH logo


22.09.2015 - Version 1.4.4
=========================

* Bug
    * [WB-444] - Cache warmup also tries to warm up requests with unsuccessful response

* Story
    * [WB-425] - General function for starting a session - createSession
        * System is able create user session with new way, using access-tokens
    * [WB-426] - General function for validating a session - validateSession
        * System is able create validate session with access-token


16.09.2015 - Version 1.4.3
=========================


* Story
    * [WB-423] - Update tile images and backgrounds
    * [WB-445] - The API should not return subcategories for categories with 1 hotel


* Task
    * [WB-452] - Remove SLH hotel websites



11.09.2015 - Version 1.4.2
=========================

* Bug
    * [WB-429] - Job scheduling tests are unstable
    * [WB-446] - New relic log url is broken
        * Link in New Relic to logs is now correct

* Story
    * [WB-168] - Implement dynamic configuration storage
        * The system now has a DB table, where it is possible to store configuration variables dynamically
        * **TODO: Reset data**
    * [WB-358] - Create background process, which applies versions
        * A background process is now able to apply versioned data changes
    * [WB-388] - Create MOCK responses to reservations API endpoints
        * Reservations API endpoints have mocked data as a response
    * [WB-390] - Secure Storage items should be saved in a DB table
        * Secure Storage system now stores data into DB table for later use - giving us better control over the secured data and a possibility to update the securing mechanism
        * **TODO: Reset data**
    * [WB-394] - If profileMenu or hotelMenu is false, the app should work
    * [WB-409] - Update the image link generation to return specific version
    * [WB-414] - SLH: WWW link is not required
        * **TODO: Reset data**
    * [WB-416] - Extend theming functionality to support new colors
        * **TODO: Reset data**
    * [WB-423] - Update tile images and backgrounds
        * **TODO: Reset data**
    * [WB-438] - API to return additional data in hotelDetailMenu
        * **TODO: Reset data**


* Task
    * [WB-412] - API to return specific profile menu items in the profiles/status call for slh
        * **TODO: Reset data**





26.08.2015 - Version 1.4.1
=========================

* Bug
    * [WB-382] - Cannot read property '_data' of undefined
        * Error in logging, which caused some requests to fail has been fixed

* Story
    * [WB-228] - Background task should clear the caches
        * CDN Caches are now cleared after data update
    * [WB-229] - An event should be triggered if any hotel group related data has changed
    * [WB-232] - The caches should be re-warmed after cleared
        * CDN Caches are now warmed up after data update and cache clearing
    * [WB-272] - Error messages should include a flag, which instructs client to retry the request
        * App's can now differentiate if they should retry the same request again if error occurred or not
    * [WB-310] - Migrate to a new CDN, LeaseWeb
        * The API now supports more flexible caching. The CDN caches most of the content
        and the clients can work with ETag's to re-validate their copy and save bandwidth
    * [WB-325] - Fix 3rd party errors and add timeout error
        * Error handling has been reviewed and made more clear
        * System is now checking for network errors in communication with 3rd parties
    * [WB-329] - Backend should be able to talk to Sabre CRS
        * System has a SDK to communicate with Sabre CRS
    * [WB-335] - SS: Retrieve the data from secure storage
    * [WB-359] - SS:Create a hash key for encrytion
    * [WB-360] - SS: Encrypt data with final hash key
        * The system now has its own secure storage mechanism for storing data securely
    * [WB-353] - Validate the meta object
    * [WB-363] - Implement SDK to communicate with Leaseweb CDN API
    * [WB-367] - Insert 3 hotels for SLH
    * [WB-368] - Update SLH theme colors
        * **TODO reset-data**
    * [WB-370] - Create update method for hotel data
    * [WB-371] - Create update method for hotel_group data
         * **TODO pg-migrate up**
    * [WB-376] - Versioning should disallow saving duplicate versions
        * Versioning module module now checks if duplicate data is about to be stored, and prevents it
        * Versioning module is now able to do a diff between different versions


09.10.2015 - Version 1.4.5
=========================

* Story
    * [WB-396] - System must import data from TrustContent XML file
    * [WB-403] - Create Reservations storage system
    * [WB-506] - API must return logo in app/main-menu
    * [WB-511] - Change SLH background image
    * [WB-513] - Change SLH logo



29.07.2015 - Version 1.4.0
=========================


* Bug
    * [WB-357] - Background worker (One off dyno) receives invalid arguments

* Story
    * [WB-162] - Data versioning functionality
        * Hotel and Hotel Group's data is now versioned, and they both have a live database table and versions table
        * NB! There is no automatic background process scheduled, which "applies" versions, currently this is triggered by code in data populating scripts
        * **TODO: pg-migrate up, reset data**
    * [WB-222] - Create a functionality to remove expired hotels from live table
    * [WB-250] - Reset data functionality should work with versioning
    * [WB-261] - Create a functionality to remove the hotel groups from live table
    * [WB-348] - Implement a PING api endpoint for server health checking
        * **TODO: Configure New Relic to ping this URL**

* Task
    * [WB-356] - Attach links to Papertrail logs for New Relic transactions
        * New Relic transaction snapshots now have a link to the logs in Papertrail, at the exact moment, when transaction happened



14.07.2015 - Version 1.3.0
=========================


* Bug
    * [WB-286] - createTestData.js script fails with "Uncaught TypeError: undefined is not a function" error
    * [WB-301] - Errors thrown from cryptoUtils are not handled
        * Errors are now handled and no unexpected errors should occur

* Story
    * [WB-226] - System needs a SDK to communicate with Papertrail
        * **TODO: If using this SDK, add the credentials to live systems**
    * [WB-295] - Errors handling improvement
        * Unused error codes were removed and some errors were specified to be more precise




01.07.2015 - Version 1.2.0
=========================

* Bug
    * [WB-143] - Account verification message must be correct
    * [WB-157] - Latitude cannot be above 90 degrees
    * [WB-204] - Change settings url's landing point to check if user signed in or not
    * [WB-249] - api-dev: test-hgid: environment has broken link

* Story
    * [WB-116] - Emails and templates functionality
    * [WB-123] - Hotel data should be validated
    * [WB-133] - Remove stars from hotels
    * [WB-137] - Logging must be done through Papertrail
        * **TODO: Create Papertrail system and add configuration to env**
    * [WB-142] - Default translations must be validated
    * [WB-145] - API should return details interactions buttons
    * [WB-150] - API to return profile menu items in the profiles/status call
    * [WB-153] - Emails sending
        * Emails are now sent through mandrill
        * **TODO: Update Stormpath directories not to send emails**
        * **TODO: Create Mandrill api key and add to env**
    * [WB-159] - Write unit tests for hotel validation
    * [WB-165] - API should return bookingUrl for every hotel
        * **TODO: reset data**
    * [WB-169] - Artificial delay must be random value
    * [WB-205] - Category names should not be saved in hotel data
        * **TODO: reset data**
    * [WB-209] - Create test file for validationUtils
    * [WB-213] - Hotel validation must be improved to support bookingUrl
    * [WB-220] - Implement default translations functionality
    * [WB-233] - The API should send out caching headers
        * **TODO: update CDN to support caching headers**
    * [WB-247] - Slideshow functionality

* Task
    * [WB-214] - Make test ignore 3rd parties
        * Tests now ignore global variable leaks, because these can happen in 3rd party libraries

