'use strict';
var newrelic;
if(process.env.NEW_RELIC_LICENSE_KEY){
	newrelic = require('newrelic');
}
var hapi = require('hapi');
var boom = require('boom');
var apiUtils = require('./modules/utils/apiUtils');
var logging = require('./modules/utils/logging');
var errors = require('./modules/utils/errors');
var profiles = require('./modules/profiles/services');
var fs = require('fs');

errors.defineErrorOnce('app-inv-route', 'Invalid path', 'This path does not exist', 'App', 400);

require('./listeners');

var apiVersion = 'v1';

// Create a server with a host and port
var server = new hapi.Server({
	connections: {
		routes: {
			cors: {
				matchOrigin: false,
				origin: ['*'],
				headers: ['Authorization', 'Content-Type', 'If-None-Match', 'x-cardola-uuid', 'x-cardola-verify']
			},
			timeout: {
				server: 20000, //25 seconds (Heroku will drop after 30 seconds)
				socket: 25000
			}
		}
	}

});

var config = {
	port: process.env.PORT,
	host: process.env.HOST
};
//if tls settings set
if( process.env.TLS_KEY
	&& process.env.TLS_CERT
	&& process.env.PUBLIC_PROTOCOL === 'https'
){
	config.tls = {
		key: fs.readFileSync(process.env.TLS_KEY),
		cert: fs.readFileSync(process.env.TLS_CERT)
	};
}
server.connection(config);

exports.modulesToLoad = [
	{
		register: require('./modules/common/module'),
		options: {}
	},
	{
		register: require('./modules/categories/module'),
		options: {}
	},
	{
		register: require('./modules/hotels/module'),
		options: {}
	},
	{
		register: require('./modules/profiles/module'),
		options: {}
	},
	{
		register: require('./modules/reservations/module')
	}
];
//callback for modules loading
exports.modulesLoaded = function(err) {
	if(err) {
		logging.info('Failed loading modules');
		return false;
	}
	return true;
};
var modulesRegistered = false;
//register modules
exports.registerModules = function() {
	if(modulesRegistered){
		return false;
	}
	modulesRegistered = true;
	errors.baseUrl = apiUtils.getBaseUrl();
	server.register(
		exports.modulesToLoad,
		{
			routes: {
				prefix: '/{apiVersion}/{hgid}'
			}
		},
		exports.modulesLoaded
	);

	server.views({
		engines: {
			html: require('handlebars')
		},
		path: 'src/templates'
	});
};

//validate request url
server.ext('onRequest', function(request, reply) {
	if( !request.headers.hasOwnProperty('x-cardola-origin')){
		return reply('Forwarding')
				.redirect(process.env.PUBLIC_PROTOCOL+ '://' + process.env.PUBLIC_HOST + request.url.path).permanent(true);
	}
	if( newrelic && process.env.APP_NAME){
		var logUrl = 'https://papertrailapp.com/systems/' + process.env.APP_NAME + '/events?time=' + Math.floor(new Date().getTime()/1000);
		newrelic.addCustomParameter('log_url', logUrl );
	}


	if( process.env.IP_WHITELIST && process.env.IP_WHITELIST.indexOf(request.info.remoteAddress) < 0 ){
		logging.warning('invalid IP: ' + request.info.remoteAddress);
		return reply('Use a valid host!')
			.code(305)
			.header('Location', process.env.PUBLIC_HOST);
	}
	return reply.continue();
});

server.ext('onPreHandler', function(request, reply) {

	var str = JSON.stringify({'method': request.method, 'path': request.url.path, 'headers': request.headers});

	logging.info('REQ: ' + str);
	try{
		request.apiRequest = apiUtils.createApiRequest(apiUtils.getCurrentHrefFromHapiRequest(request), request.headers, request.params, request.method, request.payload, request.info);
	}catch (e){
		return reply(errors.wrapToBoom(e));
	}
	
	if(request.apiRequest.urlObj.pathname.indexOf('/doc/') === 0) {
		if(request.apiRequest.headers.authorization !== 'Basic Y2FyZG9sYTpzMW0wbg==') {
			//cardola : s1m0n
			return reply(boom.unauthorized('Unauthorized', 'basic realm="Cardola doc"'));
		}
		return reply.continue();
	}
	if(request.apiRequest.urlObj.pathname.indexOf('/profiles/verify') === 0) {
		return reply.continue();
	} else {
		apiUtils.validateApiCall(request.apiRequest, function (err) {
			if (err) {
				return reply(err);
			} else {
				return reply.continue();
			}

		});
	}

});

server.on('request-error', function (request, err) {

	logging.err('Error response (500) sent for request: ' + request.id + ' because: ' + err.message);
});

server.ext('onPostHandler', function(request, reply) {
	if(request.response){
		if(request.response.isBoom){
			logging.info('RES (ERROR): ' + JSON.stringify(request.response));
			if(request.response.data){
				delete request.response.data.stack;
			}
		}else{
			logging.info('RES: ' + JSON.stringify({'statusCode': request.response.statusCode, 'headers': request.response.headers}));
		}
	}else{
		logging.info('RES: No response');
	}

	return reply.continue();
});

server.route({
	method: 'GET',
	path: '/doc/{param*}',
	handler: {
		directory: {
			path: 'doc'
		}
	}
});

server.route({
	method: 'GET',
	path: '/profiles/verify',
	handler: function(request, reply) {
		profiles.verify(request.apiRequest, function(err, res) {
			return reply.view('profiles/verified', {error: err, res: res});
		});
	}
});

//fallback route
server.route({
	method: '*',
	path: '/{p*}',
	handler: function(request, reply) {
		logging.notice('Path ' + request.path + ' was not found');
		reply(errors.newBoomError('app-inv-route', 400, {requestedPath: request.path}));
	}
});

//export
exports.server = server;
exports.apiVersion = apiVersion;
