'use strict';
var logging = require('./modules/utils/logging');
var scheduler = require('./modules/worker/scheduler');

var slhImporterStartHour = 1; //will start 1 o'clock at night (UTC time in Heroku)

exports.schedule = [
/**
 * Clock processes definitions:
 */

		//SLH importers
	{
		scheduleInCrontabFormat: '0 5 ' + slhImporterStartHour + ' * * *', // #1
		job: {
			name: 'Import SLH categories from website',
			code: 'importslhcategories',
			priority: 0, //does not really matter
			input: {
				hgid: 'slh'
			}
		}
	},
	{
		scheduleInCrontabFormat: '0 25 ' + slhImporterStartHour + ' * * *', // #2
		job: {
			name: 'Go live with hotel group changes caused by category changes',
			code: 'htlgrpgolive',
			priority: 0, //does not really matter
			input: {}
		}
	},
	{
		scheduleInCrontabFormat: '0 30 ' + slhImporterStartHour + ' * * *', // #3
		job: {
			name: 'Update hotels blacklist',
			code: 'updateBlacklist',
			priority: 0, //does not really matter
			input: {
				hotelGroupId: 'slh'
			}
		}
	},
	{
		scheduleInCrontabFormat: '0 35 ' + slhImporterStartHour + ' * * *', // #4
		job: {
			name: 'Go live with hotel group changes from updated blacklist',
			code: 'htlgrpgolive',
			priority: 0, //does not really matter
			input: {}
		}
	},
	{
		scheduleInCrontabFormat: '0 40 ' + slhImporterStartHour + ' * * *', // #5
		job: {
			name: 'Unpublish blacklisted hotels',
			code: 'unpublishBlacklistedHotels',
			priority: 0, //does not really matter
			input: {
				hotelGroupId: 'slh'
			}
		}
	},
	{
		scheduleInCrontabFormat: '0 50 ' + (slhImporterStartHour) + ' * * *', // #7
		job: {
			name: 'Go live with hotel changes from applied blacklist',
			code: 'hotelgolive',
			priority: 0, //does not really matter
			input: {}
		}
	},
	{
		scheduleInCrontabFormat: '0 0 ' + (slhImporterStartHour + 1) + ' * * *', // #6 //reserve whole hour for it, it might retry to download html files
		job: {
			name: 'Import hotels from website',
			code: 'websiteImport',
			priority: 0, //does not really matter
			input: {
				hotelGroupId: 'slh'
			}
		}
	},
	{
		scheduleInCrontabFormat: '0 0 ' + (slhImporterStartHour + 2) + ' * * *', // #7
		job: {
			name: 'Go live with hotel group and hotel changes from slh.com import',
			code: 'htlndgrouplive',
			priority: 0, //does not really matter
			input: {}
		}
	},

		//access logs - import after every 2 hours
	{
		scheduleInCrontabFormat: '0 10 */2 * * *', // #1
		job: {
			name: 'Download access logs from leaseweb',
			code: 'downloadAccessLogs',
			priority: 0, //does not really matter
			input: {}
		}
	},
	{
		scheduleInCrontabFormat: '0 40 */2 * * *', // #2
		job: {
			name: 'Import access logs into db',
			code: 'importAccessLogs',
			priority: 0, //does not really matter
			input: {}
		}
	},
	{
		scheduleInCrontabFormat: '0 30 12 * * 1', // Report every Monday 12:30  on midday
		job: {
			name: 'Report for Hotel Impressions',
			code: 'generateReportImpressions',
			priority: 0, //does not really matter
			input: {
				hotelGroupId: 'slh',
				days: 7,
				recipients: process.env.REPORTS_RECIPIENTS.split(',').map(function(email){
					return {email: email};
				})
			}
		}
	},
	{
		scheduleInCrontabFormat: '0 0 12 * * 1', // Report every Monday 12:00  on midday
		job: {
			name: 'Report for activations/first-runs',
			code: 'generateReportActivations',
			priority: 0, //does not really matter
			input: {
				hotelGroupId: 'slh',
				days: 7,
				recipients: process.env.REPORTS_RECIPIENTS.split(',').map(function(email){
					return {email: email};
				})
			}
		}
	}
];

exports.startCronjobs = function(){
	scheduler.handleStaticJobSchedule(
		exports.schedule,
		function(err) {
			if(err) {
				return logging.emerg('Clock process got an error at startup!', err);
			} else {
				logging.info('Clock process started');
				//needed for the process not to exit and foreman crash
				setInterval(function() {
					logging.info('Clock process alive...');
				}, 60 * 60 * 1000); //1h
			}
		}
	);
	scheduler.coordinateScheduledJobs();
};
