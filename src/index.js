'use strict';
var throng = require('throng');

var WORKERS = process.env.WEB_CONCURRENCY || 1;

function start() {
	var app = require('./app');
	var langUtils = require('./modules/utils/langUtils');
	var logging = require('./modules/utils/logging');
	var profile = require('./modules/profiles/services');
	app.registerModules();
	langUtils.init();
	profile.init(function(err) {
		if(err) {
			logging.emerg('Profile init failed', err);
		} else {
			app.server.start(function(err) {
				if(err) {
					logging.emerg('Error starting server: ', err);
				} else {
					logging.info('Server started at: ' + app.server.info.uri);
				}
			});
		}

	});

}

//Cluster
throng(start, {
	workers: WORKERS,
	lifetime: Infinity
});
