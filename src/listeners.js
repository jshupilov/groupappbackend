'use strict';
var glob = require('glob');
var path = require('path');

var listenerFiles = glob.sync( path.join(__dirname, '/modules/*/listener.js') );
listenerFiles.forEach(function(listenerFile){
	require(listenerFile);
});
