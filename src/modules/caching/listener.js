/**
 * Created by jevgenishupilov on 01/08/15.
 */
'use strict';
var eventUtils = require('../utils/eventUtils');
var logging = require('../utils/logging');
var services = require('./services');
//update hotel index when hotel is inserted
var modifiedHotelGroupIds = [];

eventUtils.emitter.on('hotelGroup.dataUpdated', function(data, callback){
	//purge cache when data is updated
	if(modifiedHotelGroupIds.indexOf(data.hotelGroupId) < 0){
		modifiedHotelGroupIds.push(data.hotelGroupId);
		return callback(null, true);
	}
	return callback(null, false);
});

//when go live process done, do the cache clearing
eventUtils.emitter.on('versioning.goLiveDone', function(callback){
	//take modified items and clear the array
	if(modifiedHotelGroupIds.length){
		var toPurge = modifiedHotelGroupIds;
		modifiedHotelGroupIds = [];
		services.purgeMultiple(toPurge, function(err, res){
			if(err){
				logging.err('Urls NOT purged: ', JSON.stringify(err));
				return callback(err, res);
			}
			services.getPurgeStatus(res.success, function(err, res){
				logging.info('Urls purged for hotel groups: ', toPurge, 'result:', res);
				return callback(err, res);
			});

		});
	}else{
		return callback(null, false);
	}

});
