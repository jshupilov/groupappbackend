/**
 * Created by jevgenishupilov on 01/08/15.
 */
'use strict';
var leasewebSDK = require('../utils/leasewebSDK');
var papertrailSDK = require('../utils/papertrailSDK');
var apiUtils = require('../utils/apiUtils');
function populateUrls(events){
	var urls = [];
	events.forEach(function(ev){

		var msg = ev.message.replace(/"/g, '').replace(/\s+$/, '');
		var info = {};
		msg = msg.split(' ');
		msg.forEach(function(item){
			var row = item.split('=');
			info[row[0]] = row[1];
		});
		if(urls.indexOf(info.path) < 0){
			urls.push(decodeURIComponent(info.path));
		}
	});
	return urls;
}
function nextEvents(res, events, cnt, callback){
	res.next(function(err, response){
		if(err){
			return callback(err);
		}
		cnt++;
		events = events.concat(response.Response.events);
		if(events.length < cnt * 100){
			return callback(null, events);
		}
		nextEvents(response, events, cnt, callback);
	});
}

/**
 * Warm up urls
 * @param hgId
 * @param callback
 */
exports.warmUp = function(hgId, callback){
	var maxAge = apiUtils.getCacheConfigurations().default['Cache-Control']['s-maxage'];
	papertrailSDK.search('heroku/router AND -(status=4 OR status=5) AND ' + hgId, function(err, res){
		if (err) {
			return callback(err);
		}
		var events = res.Response.events;
		var cnt = 1;
		var doIt = function(evs){
			if(evs.length > 0) {
				leasewebSDK.warmUp(populateUrls(evs), hgId, function (err2, response) {
					return callback(err2, response);
				});
			} else {
				return callback(err, res);
			}
		};

		if(events.length === 100) {
			nextEvents(res, events, cnt, function (err1, evs) {
				if (err1) {
					return callback(err1);
				} else {
					doIt(evs);
				}
			});
		} else {
			doIt(events);
		}
	}, {
		min_time: (new Date().getTime() / 1000) - maxAge * 2
	});

};

/**
 * Purge urls of a hotel group
 * @param hgId
 * @param callback
 */
exports.purge = function(hgId, callback){
	//var urls = ['.*' + hgId + '.*']; //regex is not supported!
	var urls = ['*']; //purge all for now
	leasewebSDK.purge(urls, function(err, res){
		return callback(err, res);
	});
};

/**
 * Purge multiple hotel group urls caches
 * @param hgIds
 * @param callback
 */
exports.purgeMultiple = function(hgIds, callback){
	var urls = [];
	hgIds.forEach(function(){
		urls.push('*');//temporary solution
	});
	leasewebSDK.purge(urls, function(err, res){
		return callback(err, res);
	});
};

/**
 * Get purge job status
 * @param jobId
 * @param callback
 */
exports.getPurgeStatus = function(jobId, callback){
	leasewebSDK.getPurgeStatus(jobId, callback);
};
