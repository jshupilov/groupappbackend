/**
 * Created by jevgenishupilov on 10/02/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var eventUtils = require('../../utils/eventUtils');
var eventsData = require('./events.json');


lab.experiment('Caching listeners', function(){
	var called;
	//modify functions
	lab.before(function(done){
		called = 0;
		testingTools.enableEvents();
		eventUtils.emitter.removeAllListeners('hotelGroup.dataUpdated');
		eventUtils.emitter.removeAllListeners('versioning.goLiveDone');
		eventUtils.emitter.removeAllListeners('hotelGroup.cacheCleared');
		testingTools.requireUncached('../modules/caching/listener');
		done();
	});

	//set functions back to originals
	lab.after(function(done){
		testingTools.disableEvents();
		done();
	});
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		if(called === 0) {
			res.setHeader('content-type', 'application/json');
			res.statusCode = 400;
			res.end('{"message":"Sad panda"}', 'utf8');
		} else {
			res.setHeader('content-type', 'application/json');
			res.statusCode = 200;
			res.end(JSON.stringify(eventsData), 'utf8');
		}
		called++;
	});

	lab.test('hotelGroup.dataUpdated should add new item to queue', function(done) {
		eventUtils.emitter.parallel('hotelGroup.dataUpdated', testingTools.getHotelGroupId(), function(err, added) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(added[0]).to.equal(true);
			done();
		});
	});

	lab.test('hotelGroup.dataUpdated should not add new item to queue if exists', function(done) {
		eventUtils.emitter.parallel('hotelGroup.dataUpdated', testingTools.getHotelGroupId(), function(err, added) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(added[0]).to.equal(false);
			done();
		});
	});

	lab.test('versioning.goLiveDone should give error from leaseweb', {timeout: 5000}, function(done){
		eventUtils.emitter.parallel('versioning.goLiveDone', function(err) {
			testingTools.expectError(err, 'lw-res-inv');
			done();
		});
	});

	lab.test('hotelGroup.dataUpdated should add new item to queue (for test data generation)', function(done) {
		eventUtils.emitter.parallel('hotelGroup.dataUpdated', testingTools.getHotelGroupId(), function(err, added) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(added[0]).to.equal(true);
			done();
		});
	});

	lab.test('versioning.goLiveDone should work', {timeout: 5000}, function(done){
		eventUtils.emitter.parallel('versioning.goLiveDone', function(err) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});
	});

	lab.test('versioning.goLiveDone should do nothing if nothing scheduled', {timeout: 5000}, function(done){
		eventUtils.emitter.parallel('versioning.goLiveDone', function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res[0]).to.equal(false);
			done();
		});
	});

});
