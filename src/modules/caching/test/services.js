/**
 * Created by jevgenishupilov on 14/08/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var eventsData = require('./events.json');

lab.experiment('Caching services.warmUp', function() {
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 400;
		res.end('{"message":"Sad panda"}', 'utf8');
	});
	lab.test('should give error if request to papertrail is failed', {timeout: 10000}, function(done) {
		services.warmUp(testingTools.getHotelGroupId(), function(err){
			testingTools.expectError(err, 'pt-res-inv');
			done();
		});
	});
});

lab.experiment('Caching services.warmUp', function() {

	var data, called;

	lab.before(function(done){
		called = 0;
		data = JSON.parse(JSON.stringify(eventsData));
		done();
	});
	lab.afterEach(function(done){
		eventsData = JSON.parse(JSON.stringify(data));
		done();
	});
	testingTools.mockResponsesForExperiment(lab, function(req, res) {
		res.setHeader('content-type', 'application/json');
		if(called === 0) {
			res.statusCode = 200;
			for (var i = eventsData.events.length; i < 100; i++) {
				var index = Math.floor(Math.random() * (3 - 0 + 1)) + 0;
				eventsData.events.push(eventsData.events[index]);
			}
			res.end(JSON.stringify(eventsData), 'utf8');
		} else {
			res.statusCode = 400;
			res.end('{"message":"Sad panda"}', 'utf8');
		}
		called++;
	});
	lab.test('should give error if next() request to papertrail is failed', {timeout: 10000}, function(done) {
		services.warmUp(testingTools.getHotelGroupId(), function(err){
			testingTools.expectError(err, 'pt-res-inv');
			done();
		});
	});
});

lab.experiment('Caching services.warmUp', function() {

	var data, called;
	lab.before(function(done){
		called = 0;
		data = JSON.parse(JSON.stringify(eventsData));
		done();
	});
	lab.afterEach(function(done){
		eventsData = JSON.parse(JSON.stringify(data));
		done();
	});
	testingTools.mockResponsesForExperiment(lab, function(req, res) {
		res.setHeader('content-type', 'application/json');
		res.statusCode = 200;
		if(called === 0) {
			var events = eventsData.events;
			for (var i = events.length; i < 100; i++) {
				var index = Math.floor(Math.random() * (3 + 1));
				events.push(events[index]);
			}
			res.end(JSON.stringify(eventsData), 'utf8');
		} else if(called === 1){
			res.end(JSON.stringify(eventsData), 'utf8');
		} else {
			res.end(JSON.stringify(data), 'utf8');
		}
		called++;
	});
	lab.test('should work if events length greater then 100', {timeout: 10000}, function(done) {

		services.warmUp(testingTools.getHotelGroupId(), function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
});

lab.experiment('Caching services.warmUp', function() {

	var data;
	lab.before(function(done){
		data = JSON.parse(JSON.stringify(eventsData));
		done();
	});
	lab.afterEach(function(done){
		eventsData = JSON.parse(JSON.stringify(data));
		done();
	});
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 200;
		res.end(JSON.stringify(eventsData), 'utf8');
	});
	lab.test('should work if events length less then 100', {timeout: 10000}, function(done) {
		services.warmUp(testingTools.getHotelGroupId(), function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
});

lab.experiment('Caching services.warmUp', function() {

	var data;
	lab.before(function(done){
		data = JSON.parse(JSON.stringify(eventsData));
		eventsData.events = [];
		done();
	});
	lab.afterEach(function(done){
		eventsData = JSON.parse(JSON.stringify(data));
		done();
	});
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 200;
		res.end(JSON.stringify(eventsData), 'utf8');
	});
	lab.test('should work if events is empty', {timeout: 10000}, function(done) {
		services.warmUp(testingTools.getHotelGroupId(), function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
});

lab.experiment('Caching services.purge', function() {
	lab.test('should work', {timeout: 10000}, function(done) {
		services.purge(testingTools.getHotelGroupId(), function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
});

lab.experiment('Caching services.purgeMultiple', function() {
	lab.test('should work', {timeout: 10000}, function(done) {
		services.purgeMultiple([testingTools.getHotelGroupId(), testingTools.getHotelGroupId()], function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
});

lab.experiment('Caching services.getPurgeStatus', function() {
	lab.test('should work', {timeout: 20000}, function(done) {
		services.purge(testingTools.getHotelGroupId(), function(err, purgeRes){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			services.getPurgeStatus(purgeRes.success, function(err, status){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(status).to.be.object();
				done();
			});
		});

	});
});
