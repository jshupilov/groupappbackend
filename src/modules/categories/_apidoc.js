/**
 * @apiDefine CategoryStructure
 * @apiVersion 0.0.1
 * @apiParam (Basic category structure) {String} href An URL, which points to a service,
 * which returns only this resource, with detail information.
 * @apiParam (Basic category structure) {Link} brochure Link object, which points to a service,
 * which returns hotels in this category one by one (paginated) with full detail information
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiParam (Basic category structure) {String} id Identification code of this category
 * @apiParam (Basic category structure) {String=regions,countries,brands,tags} type Type of this category
 * @apiParam (Basic category structure) {Object} localized Translated information about this category
 * @apiParam (Basic category structure) {Object[]} localized.LANGUAGE Info about this category in language
 * defined by `LANGUAGE`
 * @apiParam (Basic category structure) {String} localized.LANGUAGE.name Name of this category
 *
 * @apiParam (Basic category structure) {String} localized.LANGUAGE.icon Icon image URL for this category
 * ([About images](#api-1_Basics-basicsImages))
 *
 * @apiParam (Basic category structure) {Number} hotels Number of hotels in this category
 * @apiParam (Basic category structure) {Number} subCategories Number of subcategories
 * @apiParam (Basic category structure) {Link} getSubCategories [Link object](#api-1_Basics-basicsLinks) object,
 * which points to a resource, which returns list of subcategories
 * @apiParam (Basic category structure) {Link} getHotels [Link object](#api-1_Basics-basicsLinks) object,
 * which points to a resource, which returns list of hotels in this category or its subcategories.
 *
 *
 */

/**
 * @apiDefine CategoryCollectionQuery
 * @apiVersion 0.0.1
 * @apiParam (Category collection query parameters) {String=region,country,brand} type Specifies the type
 * of categories to return.
 */

/**
 * @api {get} - 1. Category resource
 * @apiName categoryResource
 * @apiVersion 0.0.1
 * @apiGroup 3_Categories
 * @apiDescription Category resource deals with different `type`s of categories.
 * In this system we have mostly these category types:
 * - Regions (Europe,Asia,North America ....)
 * - Countries (England, Estonia, USA ...)
 * - Brands (MC Hotels, Hilton ...)
 * - Tags (Romantic vacation, Golf, Luxurious)
 *
 * Categories can have subcategories, which normally are of same type, but it is not a rule.
 * For example categories of type `regions` have subcategories of type `countries`
 *
 * @apiUse CategoryStructure
 *
 * @apiParamExample {json} Example structure
 {
  "id": "reg-eur-est",
  "type": "regions",
  "localized": {
    "en_GB": {
      "name": "Estonia",
      "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/e_oil_paint:100,w_<width>,h_<height>,c_pad/bge.jpg",
      "backgroundImage": {
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_fill/bge.jpg",
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_fill/bge.jpg"
      }
    },
    "et_EE": {
      "name": "Eesti",
      "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/e_oil_paint:100,w_<width>,h_<height>,c_pad/bge.jpg",
      "backgroundImage": {
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_fill/bge.jpg",
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_fill/bge.jpg"
      }
    }
  },
  "hotels": 2,
  "children": [],
  "getHotels": {"href": "http://localhost:8000/v1/test-hgid/categories/reg-eur-est/hotels"},
  "brochure": {"href": "http://localhost:8000/v1/test-hgid/categories/reg-eur-est/hotels?detail=brochure&limit=1&offset=0"}
}
 *
 */
