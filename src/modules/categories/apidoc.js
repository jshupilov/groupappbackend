/**
 * @apiDefine CategoryStructure
 * @apiVersion 1.7.4
 * @apiParam (Basic category structure) {String} href An URL, which points to a service,
 * which returns only this resource, with detail information.
 * @apiParam (Basic category structure) {Link} brochure Link object, which points to a service,
 * which returns hotels in this category one by one (paginated) with full detail information
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiParam (Basic category structure) {String} id Identification code of this category
 * @apiParam (Basic category structure) {Image} backgroundImage Image object for background image
 * ([About images](#api-1_Basics-basicsImages))
 * @apiParam (Basic category structure) {String=regions,countries,brands,tags} type Type of this category
 * @apiParam (Basic category structure) {Object} localized Translated information about this category
 * @apiParam (Basic category structure) {Object[]} localized.LANGUAGE Info about this category in language
 * defined by `LANGUAGE`
 * @apiParam (Basic category structure) {String} localized.LANGUAGE.name Name of this category
 * @apiParam (Basic category structure) {String} [localized.LANGUAGE.displayName] Optional name to display instead of the `name`.
 * This value is only used and populated for `parent` category objects
 *
 * @apiParam (Basic category structure) {Image} localized.LANGUAGE.buttonImage Image object for button image
 * ([About images](#api-1_Basics-basicsImages))
 *
 * @apiParam (Basic category structure) {Number} hotels Number of hotels in this category
 * @apiParam (Basic category structure) {Number} subCategories Number of subcategories
 * @apiParam (Basic category structure) {Link} getSubCategories [Link object](#api-1_Basics-basicsLinks) object,
 * which points to a resource, which returns list of subcategories
 * @apiParam (Basic category structure) {Link} getHotels [Link object](#api-1_Basics-basicsLinks) object,
 * which points to a resource, which returns list of hotels in this category or its subcategories.
 *
 *
 */

/**
 * @apiDefine CategoryCollectionQuery
 * @apiVersion 1.7.4
 * @apiParam (Category collection query parameters) {String=region,country,brand} type Specifies the type
 * of categories to return.
 */

/**
 * @api {get} - 1. Category resource
 * @apiName categoryResource
 * @apiVersion 1.7.4
 * @apiGroup 3_Categories
 * @apiDescription Category resource deals with different `type`s of categories.
 * In this system we have mostly these category types:
 * - Regions (Europe,Asia,North America ....)
 * - Countries (England, Estonia, USA ...)
 * - Brands (MC Hotels, Hilton ...)
 * - Tags (Romantic vacation, Golf, Luxurious)
 *
 * Categories can have subcategories, which normally are of same type, but it is not a rule.
 * For example categories of type `regions` have subcategories of type `countries`
 *
 * @apiUse CategoryStructure
 * @apiUse daoErrors
 * @apiUse getHotelGroupErrors
 * @apiError (Possible error codes) {String} cat-inv-type Invalid category type is requested ([Definition](#error_cat-inv-type))
 * @apiError (Possible error codes) {String} cat-cnt-query-error Error querying category usage information ([Definition](#error_cat-cnt-query-error))
 *
 * @apiParamExample {json} Example structure
 {
    "id": "tag-cc",
    "type": "tags",
    "backgroundImage": {
      "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
      "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
    },
    "localized": {
      "en_GB": {
        "name": "City Centre",
        "buttonImage": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
        }
      },
      "et_EE": {
        "name": "Kesklinn",
        "buttonImage": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
        }
      }
    },
    "hotels": 5,
    "children": 0,
    "hasChildren": false,
    "getHotels": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-cc/hotels"},
    "getChildren": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-cc/subcategories"},
    "brochure": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-cc/hotels?detail=brochure&limit=1&offset=0"}
  }
 *
 */

/**
 * @api {get} /categories 1.1 Get list of categories
 * @apiName getCategories
 * @apiVersion 1.7.4
 * @apiGroup 3_Categories
 * @apiDescription This method returns a Collection of categories, which have at least one Hotel in it.
 *
 * [About collections](#api-1_Basics-basicsCollections)
 *
 * @apiUse CategoryCollectionQuery
 * @apiUse CollectionQueryParams
 *
 * @apiExample {html} Get all regions:
 * GET /v1/test-hgid/en/categories/?type=tags HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 * @apiUse daoErrors
 * @apiUse getHotelGroupErrors
 * @apiError (Possible error codes) {String} cat-inv-type Invalid category type is requested ([Definition](#error_cat-inv-type))
 * @apiError (Possible error codes) {String} cat-cnt-query-error Error querying category usage information ([Definition](#error_cat-cnt-query-error))
 *
 * @apiSuccessExample {json} Get all tags response:
 {
  "href": "http://localhost:8000/v1/test-hgid/categories?type=tags",
  "offset": 0,
  "limit": 10,
  "total": 3,
  "items": [
    {
      "id": "tag-cc",
      "type": "tags",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
      },
      "localized": {
        "en_GB": {
          "name": "City Centre",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "et_EE": {
          "name": "Kesklinn",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 5,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-cc/hotels"},
      "getChildren": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-cc/subcategories"},
      "brochure": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-cc/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "id": "tag-rus",
      "type": "tags",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
      },
      "localized": {
        "en_GB": {
          "name": "Russian",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "et_EE": {
          "name": "Vene",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 1,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-rus/hotels"},
      "getChildren": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-rus/subcategories"},
      "brochure": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-rus/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "id": "tag-his",
      "type": "tags",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
      },
      "localized": {
        "en_GB": {
          "name": "Historical",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "et_EE": {
          "name": "Ajalooline",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 2,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-his/hotels"},
      "getChildren": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-his/subcategories"},
      "brochure": {"href": "http://localhost:8000/v1/test-hgid/categories/tag-his/hotels?detail=brochure&limit=1&offset=0"}
    }
  ]
}
 *
 */

/**
 * @api {get} /categories/:categoryId/subcategories 1.2 Get list of subcategories
 * @apiName getSubCategories
 * @apiVersion 1.7.4
 * @apiGroup 3_Categories
 * @apiDescription This method returns a Collection of sub-categories of a category, which have at least one Hotel in it.
 *
 * [About collections](#api-1_Basics-basicsCollections)
 *
 * @apiUse CollectionQueryParams
 *
 * @apiParam {String} categoryId Category Id, which subcategories are required
 *
 * @apiExample {html} Get all subcategories (countries) under Europe:
 * GET /v1/test-hgid/en/categories/reg-eur/subcategories HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 * @apiUse daoErrors
 * @apiUse getHotelGroupErrors
 * @apiError (Possible error codes) {String} cat-inv-type Invalid category type is requested ([Definition](#error_cat-inv-type))
 * @apiError (Possible error codes) {String} cat-cnt-query-error Error querying category usage information ([Definition](#error_cat-cnt-query-error))
 *
 *
 * @apiSuccessExample {json} Get subcategories example response:
 {
  "href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur/subcategories",
  "offset": 0,
  "limit": 10,
  "total": 2,
  "items": [
    {
      "id": "reg-eur-est",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-est/bg",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-est/bg"
      },
      "localized": {
        "en_GB": {
          "name": "Estonia",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "et_EE": {
          "name": "Eesti",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 2,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-est/hotels"},
      "getChildren": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-est/subcategories"},
      "brochure": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-est/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "id": "reg-eur-uk",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-uk/bg",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-uk/bg"
      },
      "localized": {
        "en_GB": {
          "name": "United Kingdom",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "et_EE": {
          "name": "Suurbritannia",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 0,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-uk/hotels"},
      "getChildren": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-uk/subcategories"},
      "brochure": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-uk/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "id": "reg-eur-it",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-it/bg",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-it/bg"
      },
      "localized": {
        "en_GB": {
          "name": "Italy",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "et_EE": {
          "name": "Itaalia",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 0,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-it/hotels"},
      "getChildren": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-it/subcategories"},
      "brochure": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-it/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "id": "reg-eur-sw",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-sw/bg",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-sw/bg"
      },
      "localized": {
        "en_GB": {
          "name": "Sweden",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "fr_FR": {
          "name": "Suède",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 0,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sw/hotels"},
      "getChildren": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sw/subcategories"},
      "brochure": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sw/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "id": "reg-eur-nor",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-nor/bg",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-nor/bg"
      },
      "localized": {
        "en_GB": {
          "name": "Norway",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "fr_FR": {
          "name": "Norvège",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 0,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-nor/hotels"},
      "getChildren": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-nor/subcategories"},
      "brochure": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-nor/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "id": "reg-eur-fin",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-fin/bg",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-fin/bg"
      },
      "localized": {
        "en_GB": {
          "name": "Finland",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "et_EE": {
          "name": "Soome",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 0,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fin/hotels"},
      "getChildren": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fin/subcategories"},
      "brochure": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fin/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "id": "reg-eur-sp",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-sp/bg",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-sp/bg"
      },
      "localized": {
        "en_GB": {
          "name": "Spain",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "et_EE": {
          "name": "Hispaania",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 0,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sp/hotels"},
      "getChildren": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sp/subcategories"},
      "brochure": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-sp/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "id": "reg-eur-fr",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-fr/bg",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-fr/bg"
      },
      "localized": {
        "en_GB": {
          "name": "France",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "fr_FR": {
          "name": "France",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 0,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fr/hotels"},
      "getChildren": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fr/subcategories"},
      "brochure": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-fr/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "id": "reg-eur-aus",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-aus/bg",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-aus/bg"
      },
      "localized": {
        "en_GB": {
          "name": "Austria",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "fr_FR": {
          "name": "Autriche",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 0,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-aus/hotels"},
      "getChildren": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-aus/subcategories"},
      "brochure": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-aus/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "id": "reg-eur-ger",
      "backgroundImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-ger/bg",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur-ger/bg"
      },
      "localized": {
        "en_GB": {
          "name": "Germany",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        },
        "et_EE": {
          "name": "Saksamaa",
          "buttonImage": {
            "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
            "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
          }
        }
      },
      "hotels": 4,
      "children": 0,
      "hasChildren": false,
      "getHotels": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-ger/hotels"},
      "getChildren": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-ger/subcategories"},
      "brochure": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur-ger/hotels?detail=brochure&limit=1&offset=0"}
    }
  ],
  "parent": {
    "id": "reg-eur",
    "type": "regions",
    "backgroundImage": {
      "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur/bg",
      "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/categories/reg-eur/bg"
    },
    "localized": {
      "en_GB": {
        "name": "Europe",
        "buttonImage": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
        },
        "displayName": "Show all"
      },
      "et_EE": {
        "name": "Euroopa",
        "buttonImage": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/placeholder"
        },
        "displayName": "Näita kõiki"
      }
    },
    "children": 0,
    "hasChildren": false,
    "getHotels": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur/hotels"},
    "getChildren": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur/subcategories"},
    "brochure": {"href": "http://group-app2.herokuapp.com/v1/test-hgid/categories/reg-eur/hotels?detail=brochure&limit=1&offset=0"}
  }
}
 *
 */
