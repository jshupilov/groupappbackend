'use strict';
var databaseUtils = require('../utils/databaseUtils');

/**
 * Get used categories of given type
 * @param hotelGroupId
 * @param callback
 */
exports.getUsedCategories = function(hotelGroupId, callback) {

	databaseUtils.getConnection(function(err, client, done) {
		if(err) {
			return callback(err);
		}

		client.query(
			'SELECT ' +
			'category_id as id, ' +
			"COUNT(DISTINCT data -> 'hotelId') as hotels " +
			'FROM hotel, ' +
			"LATERAL jsonb_array_elements(data->'categoryIds') category_id " +
			"WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 " +
			'GROUP BY category_id',
			['"' + hotelGroupId + '"'],
			function(err, result) {
				done();
				if(err) {
					return callback(err);
				}
				callback(null, result);
			});
	});

};
