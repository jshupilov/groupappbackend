'use strict';
var services = require('./services');
var errors = require('../utils/errors');
var apiUtils = require('../utils/apiUtils');
var regionModule = {
    register: function(server, options, next) {
        // Add the route
        server.route({
            method: 'GET',
            path: '/categories',
            handler: function(request, reply) {
                services.getCategories(request.apiRequest, function(err, res) {
	                apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res));
                });
            }
        });

        server.route({
            method: 'GET',
            path: '/categories/{categoryId}/subcategories',
            handler: function(request, reply) {
                services.getCategories(request.apiRequest, function(err, res) {
	                apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res));
                });
            }
        });
        next();
    }
};

regionModule.register.attributes = {
    name: 'Categories',
    version: '1.0.0'
};

exports.register = regionModule.register;
exports.services = services;
