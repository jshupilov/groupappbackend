'use strict';
var hgServices = require('../hotel_groups/services');
var errors = require('../utils/errors');
var langUtils = require('../utils/langUtils');
var apiUtils = require('../utils/apiUtils');
var mediaServices = require('../media/services');
var collectionUtils = require('../utils/collectionUtils');
var hotelsServices = require('../hotels/services');
var searchServices = require('../search/services');
var check = require('check-types');
errors.defineError('cat-cnt-query-error', 'System error', 'Error occurred querying hotel count in categories', 'Categories');
errors.defineError('cat-inv-type', 'System error', 'Invalid category type provided', 'Categories', 400);


/**
 * Get category background image
 * @param category
 * @param language
 * @param hotelGroupData
 * @returns {*}
 */
exports.getCategoryBg = function(category, language, hotelGroupData){

	var bg, imgCat = 'categoryBg';
	/*
	Get category background from main menu item, if there is a relation
	 */
	function getBgFromMainMenuItem(){
		if(hotelGroupData.mainMenu && hotelGroupData.mainMenu.items){
			for( var i = 0; i < hotelGroupData.mainMenu.items.length; i++){
				var item = hotelGroupData.mainMenu.items[i];
				if( item.action
					&& item.action.navigation
					&& item.action.navigation.categoryType
					&& item.action.navigation.categoryType === category.type
				){
					if(item.localized
						&& item.localized[language]
						&& item.localized[language].backgroundImage
					){
						imgCat = 'menuPageBg';
						bg = item.localized[language].backgroundImage;
						return bg;
					}else if(item.backgroundImage){
						imgCat = 'menuPageBg';
						bg = item.backgroundImage;
						return bg;
					}
				}
			}
		}
		return false;
	}

	//get localized
	if(category.localized[language]
		&& category.localized[language].backgroundImage){
		bg = category.localized[language].backgroundImage;
		//get common
	}else if( category.backgroundImage ){
		bg = category.backgroundImage;
		//get from parent
	}else if( category.parentCategoryId ){

		for(var s in hotelGroupData.categories){
			if(hotelGroupData.categories[s].categoryId === category.parentCategoryId){
				//recurse same logic on parent item
				return exports.getCategoryBg(hotelGroupData.categories[s], language, hotelGroupData);
			}
		}
		if(!getBgFromMainMenuItem()){
			bg = hgServices.getLocalizedBackgroundImage(hotelGroupData, language);
			imgCat = 'bg';
		}

	}else{
		if(!getBgFromMainMenuItem()){
			bg = hgServices.getLocalizedBackgroundImage(hotelGroupData, language);
			imgCat = 'bg';
		}
	}

	return mediaServices.formatImageObject( bg, hgServices.getMediaConfigurationFromHotelGroup(hotelGroupData), imgCat);
};

/**
 * Format a category
 * @param category
 * @param type
 * @param apiRequest
 * @param hotelGroup
 * @returns {{id: *, type: *, localized: {}, hotels: number, children: Array}}
 */
exports.formatCategory = function(category, type, apiRequest, hotelGroup) {
	var outCat = {
		id: category.categoryId,
		type: type,
		localized: langUtils.getLocalizedDataForAllLanguages(category, function(localized, lang) {
			return {
				name: localized.name,
				buttonImage: mediaServices.formatImageObject(localized.buttonImage, hgServices.getMediaConfigurationFromHotelGroup(hotelGroup.data), 'categoryBtn'),
				backgroundImage: exports.getCategoryBg(category, lang, hotelGroup.data)
			};
		}),
		hotels: category.hotels ? category.hotels : 0,
		children: category.children ? category.children : 0,
		hasChildren: category.children > 0 && category.hotels > 1,
		getHotels: apiUtils.getServiceLink(apiRequest.hotelGroupId, '/categories/' + category.categoryId + '/hotels'),
		getChildren: apiUtils.getServiceLink(apiRequest.hotelGroupId, '/categories/' + category.categoryId + '/subcategories'),
		brochure: hotelsServices.getHotelBrochureLink(apiUtils.createApiRequestFromExisting('/categories/' + category.categoryId + '/hotels', {}, apiRequest), 0)
	};

	return outCat;
};

function formatParentCategory(category, type, apiRequest, hotelGroup, hotelsCount){
	category.children = 0;
	category.hotels = hotelsCount;
	var out = exports.formatCategory(category, type, apiRequest, hotelGroup);
	for(var lang in out.localized){
		var localized = out.localized[lang];
		localized.displayName = langUtils.getTranslation(hotelGroup.data.translations, 'ShowAllCell', lang);
	}

	return out;
}


/**
 * Format a category in a collection
 * @param category
 * @param type
 * @param apiRequest
 * @param hotelGroup
 * @returns {{id: *, type: *, localized: {}, hotels: number, children: Array}}
 */
function formatCategoryInCollection(category, type, apiRequest, hotelGroup) {
	var out = exports.formatCategory(category, type, apiRequest, hotelGroup);
	return out;
}

/**
 * Format a category for API result
 * @param categories
 * @param type
 * @param apiRequest
 * @param hotelGroup
 * @returns {Array}
 */
function formatCategoryCollection(categories, type, apiRequest, hotelGroup) {
	var out = [];
	categories.forEach(function(category) {
		out.push(formatCategoryInCollection(category, type, apiRequest, hotelGroup));
	});
	return out;
}

/**
 * Get categories
 * @param apiRequest
 * @param callback
 */
exports.getCategories = function(apiRequest, callback) {

	if(!apiRequest.params.categoryId) {
		if(!check.unemptyString(apiRequest.query.type)) {
			return callback(errors.newError('cat-inv-type'));
		}
	}

	var type = apiRequest.query.type;

	hgServices.getHotelGroup(apiRequest.hotelGroupId, function(err, hotelGroup) {
		if(err) {
			return callback(err);
		}
		var categories = [];
		var parentId = apiRequest.params.categoryId ? apiRequest.params.categoryId : null;
		var parentCategory = null;
		var childCount = {};
		hotelGroup.data.categories.forEach(function(category) {

			if(category.parentCategoryId){
				if(!childCount.hasOwnProperty(category.parentCategoryId)){
					childCount[category.parentCategoryId] = 0;
				}
				childCount[category.parentCategoryId]++;
			}
			if(parentId) {
				if(category.categoryId === parentId) {
					parentCategory = category;
				}
				if(category.parentCategoryId !== parentId) {
					return;
				}
			} else {
				if(category.type !== type || category.parentCategoryId) {
					return;
				}
			}
			categories.push(category);
		});

		categories.forEach(function(category) {
			category.children = childCount[category.categoryId] ? childCount[category.categoryId] : 0;
		});

		exports.getHotelsCountInCategories(apiRequest.hotelGroupId, function(err2, hotelsCount) {
			if(err2) {
				return callback(err2);
			}
			var cats = [];
			categories.forEach(function(category) {
				category.hotels = hotelsCount[category.categoryId] ? hotelsCount[category.categoryId] : 0;
				if(category.hotels > 0){
					cats.push(category);
				}
			});
			categories = cats;
			var count = categories.length;
			callback(
				null,
				collectionUtils.formatCollectionWithRequest(
					formatCategoryCollection(
						categories.splice(
							apiRequest.collectionQuery.offset,
							apiRequest.collectionQuery.offset + apiRequest.collectionQuery.limit
						),
						type,
						apiRequest,
						hotelGroup
					),
					apiRequest,
					count,
					parentCategory ? formatParentCategory(parentCategory, parentCategory.type, apiRequest, hotelGroup, hotelsCount[parentCategory.categoryId]) : null
				)
			);
		});

	});

};

/**
 * Get hotels count in categories
 * @param hotelGroupId
 * @param callback
 */
exports.getHotelsCountInCategories = function(hotelGroupId, callback) {

	searchServices.searchHotels({}, hotelGroupId, function(err, result){
		if(err) {
			return callback(errors.newError('cat-cnt-query-error', {originalError: err}));
		}
		callback(null, result.facets.categoryIds);
	});
};
