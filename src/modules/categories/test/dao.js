'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var dao = require('../dao');

lab.experiment('Categories.dao.getUsedCategories', function() {

	lab.test('should work', function(done) {
		dao.getUsedCategories(testingTools.getHotelGroupId(), function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.expectQueryResultsWithFields(res, ['id', 'hotels']);
			done();
		});
	});

});

lab.experiment('Categories.dao.getUsedCategories', function() {
	testingTools.failDbQueryForExperiment(lab, 'hotel');

	lab.test('should give error if invalid DB structure', function(done) {
		dao.getUsedCategories(testingTools.getHotelGroupId(), function(err2) {
			testingTools.code.expect(err2).to.be.object();
			done();
		});
	});
});

lab.experiment('Categories.dao.getUsedCategories', function() {
	testingTools.killDbConnectionForExperiment(lab);

	lab.test('should give error if DB connection fails', function(done) {
		dao.getUsedCategories(testingTools.getHotelGroupId(), function(err2) {
			testingTools.code.expect(err2).to.be.object();
			done();
		});
	});
});
