'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var module = require('../module');
var apiUtils = require('../../utils/apiUtils');

lab.experiment('categories.module', function() {

	var fakeServer = {
		routes: [],
		route: function(def){
			this.routes.push(def);
		}
	};

	lab.test('register should work', function(done) {

		module.register(fakeServer, {}, function(){
			testingTools.code.expect(fakeServer.routes).to.have.length(2);

			testingTools.code.expect(fakeServer.routes[0]).to.be.object();
			testingTools.code.expect(fakeServer.routes[0].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[0].path).to.equal('/categories');

			testingTools.code.expect(fakeServer.routes[1]).to.be.object();
			testingTools.code.expect(fakeServer.routes[1].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[1].path).to.equal('/categories/{categoryId}/subcategories');
			done();
		});
	});

	lab.test('/categories handler should work', {timeout: 3000}, function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/categories?type=regions', {}, {}, 'GET', {})};
		testingTools.getUuid(function(err, uuid){
			testingTools.code.expect(err).to.be.null();
			apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

			fakeServer.routes[0].handler(req, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.expectCollection(res);
				return testingTools.expectCachingHeaders(done, 'default', res);
			});
		});

	});

	lab.test('/categories/{categoryId}/subcategories handler should work', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/categories/reg-eur/subcategories', {}, {categoryId: 'reg-eur'}, 'GET', {})};
		testingTools.getUuid(function(err, uuid) {
			testingTools.code.expect(err).to.be.null();
			apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

			fakeServer.routes[1].handler(req, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.expectCollection(res);
				return testingTools.expectCachingHeaders(done, 'default', res);
			});
		});

	});

});
