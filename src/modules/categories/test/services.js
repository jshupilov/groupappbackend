'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var hgServices = require('../../hotel_groups/services');
var searchServices = require('../../search/services');
var mediaServices = require('../../media/services');
var apiUtils = require('../../utils/apiUtils');

lab.experiment('Categories get categories checking', function() {

	lab.test('should error with empty request', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {});
		apiUtils.populateApiRequest(req, 'v1', 'test', 'test');
		services.getCategories(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.data).to.include(['code']);
			testingTools.code.expect(err.data.code).to.equal('cat-inv-type');
			done();
		});
	});

	lab.test('should error with empty type', function(done) {
		var req = apiUtils.createApiRequest('/?type=', {}, {});
		apiUtils.populateApiRequest(req, 'v1', 'test', 'test');

		services.getCategories(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.data).to.include(['code']);
			testingTools.code.expect(err.data.code).to.equal('cat-inv-type');
			done();
		});
	});

	lab.test('should error with invalid hotelGroupId', function(done) {
		var req = apiUtils.createApiRequest('/?type=regions', {}, {});
		apiUtils.populateApiRequest(req, 'v1', 'test', 'test');

		services.getCategories(req, function(err) {
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});

});

lab.experiment('category services getCategories', function() {
	lab.before(function(done){
		searchServices.init(300, true);
		done();
	});

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm){
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});
	lab.test('should error with poor connection', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/?type=regions', {}, {categoryId: 'reg-eur'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.getCategories(req, function(err) {
			testingTools.expectError(err, 'cat-cnt-query-error');
			done();
		});
	});
});

lab.experiment('category services getCategories 2', function() {


	lab.test('should work and tell there are no subcategories', function(done) {
		var req = apiUtils.createApiRequest('/?type=regions', {}, {categoryId: 'reg-me'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.getCategories(req, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res, JSON.stringify(res)).to.include(['parent', 'items']);
			testingTools.code.expect(res.items[0].hasChildren).to.equal(false);
			done();
		});
	});

	lab.test('should work and return all categories', function(done) {
		var req = apiUtils.createApiRequest('/?type=regions', {}, {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.getCategories(req, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res, JSON.stringify(res)).to.include(['items']);
			testingTools.code.expect(res, JSON.stringify(res)).not.to.include(['parent']);
			testingTools.code.expect(res.items[0].hasChildren).to.equal(true);
			done();
		});
	});
});

lab.experiment('category services getCategories 3', function() {
	testingTools.updateDbRowForExperiment(lab, 'hotel_group',
			function(row){
				row.data.categories.push(
						{
							categoryId: 'some-new-cat',
							type: 'some-type'
						}
				);
			},
			['(data ->> \'hotelGroupId\') = %L', testingTools.getHotelGroupId()]
	);

	lab.test('should return no categories', function(done) {
		var req = apiUtils.createApiRequest('/?type=some-type', {}, {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.getCategories(req, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res, JSON.stringify(res)).to.include(['items']);
			testingTools.code.expect(res, JSON.stringify(res)).not.to.include(['parent']);
			testingTools.code.expect(res.items).to.have.length(0);
			done();
		});
	});
});

lab.experiment('Categories get hotels count in categories checking', function() {

	lab.test('should error with empty hotelGroupId', function(done) {
		services.getHotelsCountInCategories('', function(err) {
			testingTools.expectError(err, 'cat-cnt-query-error');
			done();
		});
	});

	lab.test('should error with undefined hotelGroupId', function(done) {
		services.getHotelsCountInCategories(undefined, function(err) {
			testingTools.expectError(err, 'cat-cnt-query-error');
			done();
		});
	});

	lab.test('should error with null hotelGroupId', function(done) {
		services.getHotelsCountInCategories(null, function(err) {
			testingTools.expectError(err, 'cat-cnt-query-error');
			done();
		});
	});
});

lab.experiment('Categories.getCategoryBg', function() {
	var f, f1, f2;
	lab.before(function(done){
		f = mediaServices.formatImageObject;
		f1 = hgServices.getLocalizedBackgroundImage;
		f2 = hgServices.getMediaConfigurationFromHotelGroup;
		mediaServices.formatImageObject = function(){
			return arguments[0];
		};
		hgServices.getLocalizedBackgroundImage = function(){
			return 'defaultBg';
		};
		hgServices.getMediaConfigurationFromHotelGroup = function(){
			return {};
		};
		done();
	});

	lab.after(function(done){
		mediaServices.formatImageObject = f;
		hgServices.getLocalizedBackgroundImage = f1;
		hgServices.getMediaConfigurationFromHotelGroup = f2;
		done();
	});

	lab.test('should return category own image', function(done) {
		var cat = {
			localized: {
				en_GB: {
					backgroundImage: {bla: 1}
				}
			}
		};
		var bg = services.getCategoryBg(cat, 'en_GB', {});
		testingTools.code.expect(bg).to.equal(cat.localized.en_GB.backgroundImage);
		done();
	});

	lab.test('should return common image if no bg for lang', function(done) {
		var cat = {
			backgroundImage: {bla: 2},
			localized: {
				en_GB: {
					backGroundImage2: {bla: 1}
				}
			}
		};
		var bg = services.getCategoryBg(cat, 'en_GB', {});
		testingTools.code.expect(bg).to.equal(cat.backgroundImage);
		done();
	});

	lab.test('should return parent bg', function(done) {
		var cat = {
			parentCategoryId: 1,
			localized: {
				en_GB: {
					backGroundImage2: {bla: 1}
				}
			}
		};
		var hd = {
			categories: [
				{categoryId: 2},
				{
					categoryId: 1,
					localized: {
						en_GB: {
							backgroundImage: {bla: 3}
						}
					}
				}
			]
		};
		var bg = services.getCategoryBg(cat, 'en_GB', hd);
		testingTools.code.expect(bg).to.equal(hd.categories[1].localized.en_GB.backgroundImage);
		done();
	});

	lab.test('should return category own image', function(done) {
		var cat = {
			localized: {
				en_GB: {
					backgroundImage: {bla: 1}
				}
			}
		};
		var bg = services.getCategoryBg(cat, 'en_GB', {});
		testingTools.code.expect(bg).to.equal(cat.localized.en_GB.backgroundImage);
		done();
	});

	lab.test('should return fallback bg if parent has no bg', function(done) {
		var cat = {
			parentCategoryId: 1,
			localized: {
				en_GB: {
					backGroundImage2: {bla: 1}
				}
			}
		};
		var hd = {
			categories: [
				{categoryId: 2},
				{
					categoryId: 1,
					localized: {
						en_GB: {}
					}
				}
			]
		};
		var bg = services.getCategoryBg(cat, 'en_GB', hd);
		testingTools.code.expect(bg).to.equal('defaultBg');
		done();
	});

	lab.test('should return fallback bg if has no parent', function(done) {
		var cat = {
			localized: {
			}
		};
		var hd = {
			categories: [
				{categoryId: 2},
				{
					categoryId: 1,
					localized: {
						en_GB: {}
					}
				}
			]
		};
		var bg = services.getCategoryBg(cat, 'en_GB', hd);
		testingTools.code.expect(bg).to.equal('defaultBg');
		done();
	});

	lab.test('should return fallback bg from main menu if parent not found', function(done) {
		var cat = {
			parentCategoryId: 31,
			type: 'asdsadasd',
			localized: {
				en_GB: {
					backGroundImage2: {bla: 1}
				}
			}
		};
		var hd = {
			mainMenu: {
				items: [
					{

					},
					{
						navigation: {}
					},
					{
						action: {
							navigation: {
								categoryType: 'asdsadasd',
								backGroundImage: {asdas: 1}
							}
						}
					}

				]
			},
			categories: [
				{categoryId: 2},
				{
					categoryId: 1,
					localized: {
						en_GB: {
							backgroundImage: {bla: 1}
						}
					}
				}
			]
		};
		var bg = services.getCategoryBg(cat, 'en_GB', hd);
		testingTools.code.expect(bg).to.equal('defaultBg');
		done();
	});

	lab.test('should return fallback bg (localized) from main menu if parent not found', function(done) {
		var cat = {
			parentCategoryId: 31,
			type: 'asdsadasd',
			localized: {
				en_GB: {
					backGroundImage2: {bla: 1}
				}
			}
		};
		var hd = {
			mainMenu: {
				items: [
					{

					},
					{
						navigation: {}
					},
					{
						action: {
							navigation: {
								categoryType: 'asdsadasd'

							}
						},
						localized: {
							en_GB: {
								backgroundImage: {asdas: 1}
							}
						}
					}
				]
			},
			categories: [
				{categoryId: 2},
				{
					categoryId: 1,
					localized: {
						en_GB: {
							backgroundImage: {blas: 1}
						}
					}
				}
			]
		};
		var bg = services.getCategoryBg(cat, 'en_GB', hd);
		testingTools.code.expect(bg).to.equal(hd.mainMenu.items[2].localized.en_GB.backgroundImage);
		done();
	});

	lab.test('should return fallback bg (unlocalized) from main menu if parent not found', function(done) {
		var cat = {
			parentCategoryId: 31,
			type: 'asdsadasd',
			localized: {
				en_GB: {
					backGroundImage2: {bla: 1}
				}
			}
		};
		var hd = {
			mainMenu: {
				items: [
					{

					},
					{
						action: {}
					},
					{
						action: {
							navigation: {}
						}
					},
					{
						action: {
							navigation: {
								categoryType: 'asdasd'
							}
						}
					},
					{
						action: {
							navigation: {
								categoryType: 'asdsadasd'

							}
						},
						localized: {},
						backgroundImage: {asdas: 1}
					}
				]
			},
			categories: [
				{categoryId: 2},
				{
					categoryId: 1,
					localized: {
						en_GB: {
							backgroundImage: {blas: 1}
						}
					}
				}
			]
		};
		var bg = services.getCategoryBg(cat, 'en_GB', hd);
		testingTools.code.expect(bg).to.equal(hd.mainMenu.items[4].backgroundImage);
		done();
	});

	lab.test('should return fallback bg if related main menu not found', function(done) {
		var cat = {
			parentCategoryId: 31,
			localized: {
				en_GB: {
					backGroundImage2: {bla: 1}
				}
			}
		};
		var hd = {
			categories: [
				{categoryId: 2},
				{
					categoryId: 1,
					localized: {
						en_GB: {
							backgroundImage: {bla: 1}
						}
					}
				}
			]
		};
		var bg = services.getCategoryBg(cat, 'en_GB', hd);
		testingTools.code.expect(bg).to.equal('defaultBg');
		done();
	});

	lab.test('should return fallback bg from main menu item', function(done) {
		var cat = {
			type: 'asdsadasd',
			localized: {
				en_GB: {
					backGroundImage2: {bla: 1}
				}
			}
		};
		var hd = {
			categories: [
				{categoryId: 2},
				{
					categoryId: 1,
					localized: {
						en_GB: {

						}
					}
				}
			],
			mainMenu: {
				items: [
					{
						action: {
							navigation: {
								categoryType: 'asdsadasd'

							}
						},
						localized: {},
						backgroundImage: {asdas: 1}
					}
				]
			}
		};
		var bg = services.getCategoryBg(cat, 'en_GB', hd);
		testingTools.code.expect(bg).to.equal(hd.mainMenu.items[0].backgroundImage);
		done();
	});




});

lab.experiment('Categories.formatCategory', function() {
	var category = {};
	lab.test('should give 0 as hotels count if not hotels count specified', function(done) {
		var formatted = services.formatCategory(category, 'some-type', {hotelGroupId: testingTools.getHotelGroupId()}, testingTools.getHotelGroupId());
		testingTools.code.expect(formatted).to.include('hotels');
		testingTools.code.expect(formatted.hotels).to.equal(0);
		done();
	});

	lab.test('should give expected result', function(done) {
		category.hotels = 1;
		category.children = 1;
		var formatted = services.formatCategory(category, 'some-type', {hotelGroupId: testingTools.getHotelGroupId()}, testingTools.getHotelGroupId());
		testingTools.code.expect(formatted).to.include('hotels');
		testingTools.code.expect(formatted.hotels).to.equal(1);
		testingTools.code.expect(formatted.hasChildren).to.equal(false);

		category.hotels = 2;
		var formatted2 = services.formatCategory(category, 'some-type', {hotelGroupId: testingTools.getHotelGroupId()}, testingTools.getHotelGroupId());
		testingTools.code.expect(formatted2).to.include('hotels');
		testingTools.code.expect(formatted2.hotels).to.equal(2);
		testingTools.code.expect(formatted2.hasChildren).to.equal(true);


		done();
	});




});
