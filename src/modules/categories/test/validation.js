'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var validation = require('../validation');

lab.experiment('Categories.validation.validateCategory', function() {

	var getValidData = null;
	var hotelGroupId = null;
	lab.before(function(done){
		hotelGroupId = 'asdasdad';
		getValidData = function(){
			var imagePrefix = process.env.CLOUDINARY_BASE_FOLDER;
			var category = {
				categoryId: 'cat1',
				type: 'foo',
				localized: {
					en_GB: {}
				},
				buttonImage: {
					'source': {
						'public_id': imagePrefix + '/group-app/' + hotelGroupId + '/categories/reg-eur/button1429258905000',
						'isCloudinary': true,
						'resource_type': 'image'
					}
				},
				backgroundImage: {
					'source': {
						'public_id': imagePrefix + '/group-app/' + hotelGroupId + '/categories/reg-eur/button1429258905000',
						'isCloudinary': true,
						'resource_type': 'image'
					}
				},
				name: 'Category 1'

			};

			return category;
		};
		done();
	});
	lab.test('should give error if data type is invalid', function(done) {
		var category = 1;
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-inv-type');
			done();
		});

	});

	lab.test('should give error if category type is invalid', function(done) {
		var category = 1;
		validation.validateCategory(category, 'asdasd', function(err){
			testingTools.expectError(err, 'cv-inv-val-type');
			done();
		});

	});

	lab.test('should give error if category is missing basic keys', function(done) {
		var category = {};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-key-miss');
			done();
		});

	});

	lab.test('should give error if categoryId is of invalid type', function(done) {
		var category = {
			categoryId: null,
			type: null,
			localized: null,
			buttonImage: null,
			backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('categoryId');
			done();
		});

	});

	lab.test('should give error if categoryId is empty string', function(done) {
		var category = {
			categoryId: '',
			type: null,
			localized: null,
			buttonImage: null,
			backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('categoryId');
			done();
		});

	});

	lab.test('should give error if type is of invalid type', function(done) {
		var category = {
			categoryId: 'cat1',
			type: null,
			localized: null,
			buttonImage: null,
			backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('type');
			done();
		});

	});

	lab.test('should give error if type is empty string', function(done) {
		var category = {
			categoryId: 'cat1',
			type: '',
			localized: null,
			buttonImage: null,
			backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('type');
			done();
		});

	});

	lab.test('should give error if localized is of invalid type ', function(done) {
		var category = {
			categoryId: 'cat1',
			type: 'foo',
			localized: null,
			buttonImage: null,
			backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized');
			done();
		});

	});

	lab.test('should give error if localized has invalid language key in it', function(done) {
		var category = {
			categoryId: 'cat1',
			type: 'foo',
			localized: {blabla: {}},
			buttonImage: null,
			backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-inv-lang');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized');
			done();
		});

	});

	lab.test('should give error if localized has invalid object in it', function(done) {
		var category = {
			categoryId: 'cat1',
			type: 'foo',
			localized: {et: null},
			buttonImage: null,
			backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized[et]');
			done();
		});

	});

	lab.test('should give error if no buttonImage for every language (or default)', function(done) {
		var category = {
			categoryId: 'cat1',
			type: 'foo',
			localized: {
				en_GB: {}
			},
			//buttonImage: null,
			backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).to.equal('localized[en_GB].buttonImage');
			done();
		});

	});

	lab.test('should not give error if no buttonImage for every language (or default) and not required', function(done) {
		var category = {
			categoryId: 'cat1',
			type: 'foo',
			localized: {
				en_GB: {}
			},
			//buttonImage: null,
			backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).not.to.equal('localized[en_GB].buttonImage');
			done();
		}, {
			requireCategoryBackground: false,
			requireCategoryButtonImage: false
		});

	});

	lab.test('should give error if no backgroundImage for every language (or default)', function(done) {
		var category = {
			categoryId: 'cat1',
			type: 'foo',
			localized: {
				en_GB: {}
			},
			buttonImage: null//,
			//backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized[en_GB].backgroundImage');
			done();
		});

	});

	lab.test('should not give error if no backgroundImage for every language (or default) and not required', function(done) {
		var category = {
			categoryId: 'cat1',
			type: 'foo',
			localized: {
				en_GB: {}
			},
			buttonImage: null
			//backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).not.to.equal('localized[en_GB].backgroundImage');
			done();
		}, {
			requireCategoryBackground: false,
			requireCategoryButtonImage: false
		});

	});

	lab.test('should give error if no name for every language (or default)', function(done) {
		var category = {
			categoryId: 'cat1',
			type: 'foo',
			localized: {
				en_GB: {}
			},
			buttonImage: null,
			backgroundImage: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized[en_GB].name');
			done();
		});

	});

	lab.test('should give error for buttonImage (from media module validation - meaning media validations are used)', function(done) {
		var category = {
			categoryId: 'cat1',
			type: 'foo',
			localized: {
				en_GB: {}
			},
			buttonImage: null,
			backgroundImage: null,
			name: null
		};
		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.code, JSON.stringify(err)).to.include(['mv-']);
			testingTools.code.expect(err.data.debuggingData.key).to.equal('buttonImage');
			done();
		});

	});

	lab.test('should validate localized buttonImage', function(done) {
		var category = getValidData();
		category.localized.en_GB.buttonImage = null;

		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).to.equal('localized[en_GB].buttonImage');
			done();
		}, {hotelGroupId: hotelGroupId});

	});

	lab.test('should give error for backgroundImage (from media module validation - meaning media validations are used) if backgroundImage has invalid type', function(done) {
		var category = getValidData();
		category.backgroundImage = null;

		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.code).to.include(['mv-']);
			testingTools.code.expect(err.data.debuggingData.key).to.equal('backgroundImage');
			done();
		}, {hotelGroupId: hotelGroupId});

	});

	lab.test('should validate localized backgroundImage', function(done) {
		var category = getValidData();
		category.localized.en_GB.backgroundImage = null;

		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized[en_GB].backgroundImage');
			done();
		}, {hotelGroupId: hotelGroupId});

	});

	lab.test('should give error if name has invalid type', function(done) {

		var category = getValidData();
		category.name = null;

		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('name');
			done();
		}, {hotelGroupId: hotelGroupId});

	});

	lab.test('should validate localized.name (invalid type)', function(done) {
		var category = getValidData();
		category.localized.en_GB.name = null;

		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized[en_GB].name');
			done();
		}, {hotelGroupId: hotelGroupId});

	});

	lab.test('should give error if name is an empty string', function(done) {
		var category = getValidData();
		category.name = '';

		validation.validateCategory(category, 'full', function(err){
			testingTools.expectError(err, 'cv-nam-empty');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('name');
			done();
		}, {hotelGroupId: hotelGroupId});

	});

	lab.test('should pass if all OK', function(done) {
		var category = getValidData();

		validation.validateCategory(category, 'full', function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		}, {hotelGroupId: hotelGroupId, defaultLanguage: 'en_GB'});

	});

	lab.test('should pass if all OK - no defaults', function(done) {
		var category = getValidData();
		category.localized.en_GB = {
			name: category.name,
			buttonImage: category.buttonImage,
			backgroundImage: category.backgroundImage
		};
		delete category.name;
		delete category.buttonImage;
		delete category.backgroundImage;

		validation.validateCategory(category, 'full', function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		}, {hotelGroupId: hotelGroupId, defaultLanguage: 'en_GB'});

	});

});
