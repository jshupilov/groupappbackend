'use strict';
var errors = require('../utils/errors');
var langUtils = require('../utils/langUtils');
var mediaVal = require('../media/validation');
var check = require('check-types');


errors.defineError('cv-inv-type', 'System error', 'Category has invalid data type', 'Categories.validation', 400);
errors.defineError('cv-inv-val', 'System error', 'Category has invalid data type', 'Categories.validation', 400);
errors.defineError('cv-inv-val-type', 'System error', 'Invalid validation type given', 'Categories.validation');
errors.defineError('cv-key-miss', 'System error', 'Missing a required key in category', 'Categories.validation', 400);
errors.defineError('cv-inv-lang', 'System error', 'Invalid language provided', 'Categories.validation', 400);
errors.defineError('cv-nam-empty', 'Category name cannot be empty', 'Category name cannot be empty string', 'Categories.validation', 400);
errors.defineError('cv-cat-no-defl', 'Category has no translation in default language', 'Category is missing translations in default language', 'Categories.validation', 400);

function validateRootKeys(category, validationType, callback){
	var rootKeys = ['categoryId', 'type', 'localized'];

	for(var key in rootKeys){
		if(!category.hasOwnProperty(rootKeys[key])){
			return callback(errors.newError('cv-key-miss', {key: rootKeys[key]}));
		}
	}

	return callback(null);
}

function validateCategoryId(category, validationType, callback){
	if( typeof category.categoryId !== 'string' ){
		return callback(errors.newError('cv-inv-type', {key: 'categoryId'}));
	}

	if( category.categoryId.length < 1 ){
		return callback(errors.newError('cv-inv-val', {key: 'categoryId'}));
	}
	return callback(null);
}

function validateType(category, validationType, callback){
	if( typeof category.type !== 'string' ){
		return callback(errors.newError('cv-inv-type', {key: 'type'}));
	}

	if( category.type.length < 1 ){
		return callback(errors.newError('cv-inv-val', {key: 'type'}));
	}
	return callback(null);
}

function validateLocalized(category, validationType, callback, context){
	if( !check.object(category.localized) ){
		return callback(errors.newError('cv-inv-type', {key: 'localized'}));
	}

	var invLan = false;
	var invTyp = false;
	var invBi = false;
	var invBgi = false;
	var invN = false;


	for( var lk in category.localized ){
		var loc = category.localized[lk];
		if( !langUtils.isValidLanguageCode(lk) ){
			invLan = errors.newError('cv-inv-lang', {key: 'localized', givenKey: lk });
		}else if( !check.object(loc) ){
			invTyp = errors.newError('cv-inv-type', {key: 'localized[' + lk + ']'});
		}else if( !loc.hasOwnProperty('buttonImage') && !category.hasOwnProperty('buttonImage') ){
			if(context.requireCategoryButtonImage){
				invBi = errors.newError('cv-key-miss', {key: 'localized[' + lk + '].buttonImage'});
			}
		}else if( !loc.hasOwnProperty('backgroundImage') && !category.hasOwnProperty('backgroundImage') ){
			if(context.requireCategoryBackground){
				invBgi = errors.newError('cv-key-miss', {key: 'localized[' + lk + '].backgroundImage', context: context});
			}
		}else if( !loc.hasOwnProperty('name') && !category.hasOwnProperty('name') ){
			invN = errors.newError('cv-key-miss', {key: 'localized[' + lk + '].name'});
		}
	}

	if(invLan){
		return callback(invLan);
	}
	if(invTyp){
		return callback(invTyp);
	}
	if(invBi){
		return callback(invBi);
	}
	if(invBgi){
		return callback(invBgi);
	}
	if(invN){
		return callback(invN);
	}

	return callback(null);
}

function validateNames(category, validationType, callback){

	var i = 0;
	var names = [];
	function doVal(){
			if(names.length > 0){
				var name = names[i][0];
				if( typeof name !== 'string' ) {
					return callback(errors.newError('cv-inv-type', {key: names[i][1]}));
				}else if( name.length < 1 ){
					return callback(errors.newError('cv-nam-empty', {key: names[i][1]}));
				}else if(names[i+1] !== undefined){
					i++;
					doVal();
				}else{
					return callback(null);
				}
			}else{
				return callback(null);
			}
	}


	//gather all names for validation
	if(category.hasOwnProperty('name')) {
		names.push([category.name, 'name']);
	}

	for(var lk in category.localized) {
		if(category.localized[lk].hasOwnProperty('name')) {
			names.push([category.localized[lk].name, 'localized[' + lk + '].name']);
		}
	}

	doVal();
}

/**
 * Validate collection of images
 * @param imgs
 * @param validationType
 * @param callback
 * @param context
 */
function validateImageCollection(imgs, validationType, callback, context){
	var i = 0;

	//recursive validation
	function doVal(){
		mediaVal.validateImageObject(imgs[i][0], validationType, function(err){
			if(err){
				err.data.debuggingData.key = imgs[i][1];
				return callback(err);
			}else if( imgs[i+1] !== undefined ){
				i++;
				doVal();
			}else{
				return callback(null);
			}
		}, context);
	}
	if(imgs.length > 0){
		doVal();
	}else{
		callback(null);
	}
}

/**
 * Validate all buttonImages
 * @param category
 * @param validationType
 * @param callback
 * @parram context
 */
function validateButtonImages(category, validationType, callback, context) {
	var imgs = [];
	//gather all buttonImages for validation
	if(category.hasOwnProperty('buttonImage')) {
		imgs.push([category.buttonImage, 'buttonImage']);
	}

	for(var lk in category.localized) {
		if(category.localized[lk].hasOwnProperty('buttonImage')) {
			imgs.push([category.localized[lk].buttonImage, 'localized[' + lk + '].buttonImage']);
		}
	}

	validateImageCollection(imgs, 'hotelGroupImage', callback, context);
}

/**
 * Validate all backgroundImages
 * @param category
 * @param validationType
 * @param callback
 */
function validateBackgroundImages(category, validationType, callback, context){
	var imgs = [];
	//gather all backgroundImages for validation
	if(category.hasOwnProperty('backgroundImage')){
		imgs.push([category.backgroundImage, 'backgroundImage']);
	}

	for( var lk in category.localized ){
		if(category.localized[lk].hasOwnProperty('backgroundImage')){
			imgs.push([category.localized[lk].backgroundImage, 'localized[' + lk + '].backgroundImage']);
		}
	}

	validateImageCollection(imgs, 'hotelGroupImage', callback, context);

}

function validateDefaultLanguage(category, validationType, callback, context){
	if( !category.localized.hasOwnProperty(context.defaultLanguage) ){
		return callback(errors.newError('cv-cat-no-defl', {key: 'localized'}));
	}
	callback(null);
}

/**
 * Validate an category
 * @param category
 * @param validationType
 * @param callback
 * @param context
 * @returns {*}
 */
exports.validateCategory = function(category, validationType, callback, context){
	if(!context){
		context = {
			requireCategoryBackground: true,
			requireCategoryButtonImage: true
		};
	}
	var validationTypes = {
		full: [validateRootKeys, validateCategoryId, validateType, validateLocalized, validateButtonImages, validateBackgroundImages, validateNames, validateDefaultLanguage]
	};


	if(validationTypes.hasOwnProperty(validationType)){

		var checkIndex = 0;
		var valCaller = function(){
			//call first check
			validationTypes[validationType][checkIndex](category, validationType, function(err, res){
				if(err){
					callback(err);
					//call next check
				}else if( validationTypes[validationType][checkIndex+1] !== undefined ){
					checkIndex++;
					valCaller();
				}else{
					callback(null, res);
				}
			}, context);
		};

		if( !check.object(category) ){
			return callback(errors.newError('cv-inv-type', {}));
		}

		valCaller();

	}else{
		callback(errors.newError('cv-inv-val-type', {}));
	}
};
