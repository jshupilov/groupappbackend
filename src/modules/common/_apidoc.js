/**
 * This file holds apidoc for previous versions.
 * If an API service interface changes with a new version number,
 * then a copy of old version must be copied to this file
 */

/**
 * @api {get} / 1. Introduction
 * @apiName basicsIntroduction
 * @apiVersion 0.0.1
 * @apiGroup 1_Basics
 * @apiDescription Cardola Group App API provides RESTful services for client applications.
 * In this section we cover some of the basics to get you started.
 *
 * The Cardola Group API will adhere to the following conventions:
 * - Request / Response headers shall indicate the use of JSON and UTF-8 character set in payload.
 * - -  Request will have a header: **Accept: application/json**
 * - - Response will include a header: **Content-Type: application/json; charset=utf-8**
 * - The API has versions, the version of the API is specified in the URL
 * - All references to time shall be expressed in accordance with **ISO8601**
 * - All references to a language shall be expressed in accordance with **ISO3166**
 * - All access to data specific to a user will be made over SSL
 * - All access to data that may be available to anyone downloading the application and browsing the hotels shall be sent in the clear (without SSL)
 * - Mobile applications must not assume that only the JSON fields / collections they require have been returned and should successfully ignore data name value pairs within the JSON it does not require
 * - Preferred languages will be sent in all headers by the mobile apps and take advantage of the weighting system provided.
 * - -  If only a single language has been selected by the user the header will contain in the case of Russian being the selected language:
 * - - - Accept-Language: ru
 * - If German is the users first choice and Estonian second it will be:
 * - - Accept-Language: da; q=0.7, ee; q=0.3
 * - If Russian 1st, English 2nd and German 3rd it will be:
 * - - Accept-Language: ru; q=0.5, en; q=0.3, da; q=0.2
 * - Mobile applications must fail gracefully and report errors to the user
 * - Caching of non-encrypted data will facilitate caching through the use of the cache control tags ETag’s , Cache-Control:public, max-age=<seconds> and expires: <date/time> information
 * - All requests, except "registration" , shall include an header x-cardola-uuid with the UUID value, which was given to the app from "/registration" API call.
 *
 * The Base url for LIVE API is as follows:
 *
 */


/**
 * @api {get} /images/<width>x<height>/sample.jpg 5. Images
 * @apiName basicsImages
 * @apiVersion 0.0.1
 * @apiGroup 1_Basics
 * @apiDescription If a resource must return a reference to a image, it will return an url to the image.
 * This url contains tokens, which must be replaced by the client to get the working url.
 *
 * Tokens in the url are wrapped between `<` and `>` (as seen in the example url)
 *
 * NB! I a request is made to the final url of the image, then it might take couple of seconds to respond,
 * because if the image is requested with a size, that has never been requested before,
 * the server will render the image to that size and return it.
 *
 * @apiParam (Tokens) {Number} width The required width of the image in pixels
 *
 * Example: "100" to get image with width of 100 pixels
 * @apiParam (Tokens) {Number} height The required height of the image in pixels
 *
 * Example: "90" to get image with height of 90 pixels
 *
 *
 */

/**
 * @apiDefine CollectionStructure
 * @apiVersion 0.0.1
 * @apiParam (Collection structure) {String} href A URL, which points to a service,
 * which returns this collection
 * @apiParam (Collection structure) {Number} offset=0 An offset for pagination, if set, then response will skip the
 * first `N` values.
 * @apiParam (Collection structure) {Number} limit=10 A limit for maximum number of items returned in one response
 * @apiParam (Collection structure) {Number} total Total number of items in this collection, without limit and offset
 * @apiParam (Collection structure) {Number} [next] Href to next page in this collection - if next page exists
 * @apiParam (Collection structure) {Number} [prev] Href to previous page in this collection - if previous page exists
 * @apiParam (Collection structure) {Object[]} items An array of items, which are included in this collection.
 * The contents of this array is controlled by `offset` and `limit` query parameters
 */

/**
 * @apiDefine CollectionQueryParams
 * @apiVersion 0.0.1
 * @apiParam (Collection query parameters) {Number} limit=10 A limit for maximum number of items returned in one response
 * @apiParam (Collection query parameters) {Number} offset=0 An offset for pagination, if set, then response will skip the
 * first `N` values.
 *
 */

/**
 * @api {get} - 8. Collections
 * @apiName basicsCollections
 * @apiVersion 0.0.1
 * @apiGroup 1_Basics
 * @apiDescription Some API services need to return multiple items in one response, like list of hotels or categories.
 * A list of same type items is represented as a `Collection` in this API. A `Collection` has a predefined high level
 * structure, which is described above.
 *
 * If interacting with a API endpoint, which returns a collection,
 * then there are also some query parameters available for controlling the output, these parameters are described below.
 *
 *
 * @apiUse CollectionStructure
 * @apiUse CollectionQueryParams
 *

 *
 * @apiParamExample {json} Collection sample:
 {
  "href": "http://.......",
  "offset": 2,
  "limit": 10,
  "total": 21,
  "next": "http:........." ,
  "prev": "http:........." ,
  "items": [
    {....item 1},
    {....item 2}
  ]
 }
 * @apiParamExample {html} Request url sample:
 http://...../categories?limit=10&offset=20
 ==> This will return 10 items starting from item 21
 *
 */

/**
 * @api {get} /app/registration 3. Get the main menu
 * @apiName appMainMenu
 * @apiVersion 0.0.1
 * @apiGroup 2_Initialization
 * @apiDescription Get the structure of the main menu
 *
 *
 * @apiExample {html} Example usage:
 * GET /v1/test-hgid/app/main-menu HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 *
 * @apiSuccess {String}     href The href of current request/resource
 * @apiSuccess {Object[]}   items The items of the main menu.
 * @apiSuccess {Object}     items.localized Localized values of this menu item
 * Elements in this object have a key in the format of ISO 639-1 with region designator.
 * @apiSuccess {Object}     items.localized.LANGUAGE_CODE.name The menu item name
 * @apiSuccess {Object}     items.localized.LANGUAGE_CODE.icon The menu item icon
 * ([About images](#api-1_Basics-basicsImages))
 * @apiSuccess {Object}     mainMenu.action The object defining the item action
 * @apiSuccess {String=navigation}     mainMenu.action.type The type of action this item does `navigation` means in-app navigation
 * @apiSuccess {String}     mainMenu.action.href The href, which gives data for the target view
 * @apiSuccess {String=categoryGrid,hotelsOnMap,hotelList}     mainMenu.action.viewType The view type, which should be rendered if user navigates to this menu item
 * @apiSuccess {Object[]}   [items.children] Children menu items **NOT USED CURRENTLY**
 *
 * @apiSuccessExample {json} Success-Example:
 * {
  "items": [
    {
      "action": {
        "type": "navigation",
        "href": "http://localhost:8000/v1/test-hgid/categories?type=regions",
        "viewType": "categoryGrid"
      },
      "children": [],
      "localized": {
        "ar_SA": {
          "name": "المناطق",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/globe.png"
        },
        "en_GB": {
          "name": "Regions",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/globe.png"
        },
        "et_EE": {
          "name": "Piirkonnad",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/globe.png"
        },
        "fr_FR": {
          "name": "Régions",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/globe.png"
        },
        "ru_RU": {
          "name": "Регионы",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/globe.png"
        }
      }
    },
    {
      "action": {
        "type": "navigation",
        "href": "http://localhost:8000/v1/test-hgid/categories?type=tags",
        "viewType": "categoryGrid"
      },
      "children": [],
      "localized": {
        "ar_SA": {
          "name": "أنواع",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/umbrella.png"
        },
        "en_GB": {
          "name": "Types",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/umbrella.png"
        },
        "et_EE": {
          "name": "Tüübid",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/umbrella.png"
        },
        "fr_FR": {
          "name": "Types",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/umbrella.png"
        },
        "ru_RU": {
          "name": "Виды",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/umbrella.png"
        }
      }
    },
    {
      "action": {
        "type": "navigation",
        "href": "http://localhost:8000/v1/test-hgid/categories?type=brands",
        "viewType": "categoryGrid"
      },
      "children": [],
      "localized": {
        "ar_SA": {
          "name": "العلامات التجارية",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/x.png"
        },
        "en_GB": {
          "name": "Brands",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/x.png"
        },
        "et_EE": {
          "name": "Brändid",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/x.png"
        },
        "fr_FR": {
          "name": "Marques",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/x.png"
        },
        "ru_RU": {
          "name": "Бренды",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/x.png"
        }
      }
    },
    {
      "action": {
        "type": "navigation",
        "href": "http://localhost:8000/v1/test-hgid/hotels?lat=<latitude>&long=<longitude>&radius=<radius>",
        "viewType": "hotelsOnMap"
      },
      "children": [],
      "localized": {
        "ar_SA": {
          "name": "بالقرب مني",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/location.png"
        },
        "en_GB": {
          "name": "Near me",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/location.png"
        },
        "et_EE": {
          "name": "Lähedal",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/location.png"
        },
        "fr_FR": {
          "name": "Near me",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/location.png"
        },
        "ru_RU": {
          "name": "Рядом со мной",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/location.png"
        }
      }
    },
    {
      "action": {
        "type": "navigation",
        "href": "http://localhost:8000/v1/test-hgid/hotels",
        "viewType": "hotelList"
      },
      "children": [],
      "localized": {
        "ar_SA": {
          "name": "عرض كل",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/all.png"
        },
        "en_GB": {
          "name": "Show all",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/all.png"
        },
        "et_EE": {
          "name": "Näita kõiki",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/all.png"
        },
        "fr_FR": {
          "name": "Montrer tous",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/all.png"
        },
        "ru_RU": {
          "name": "Показать все",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/all.png"
        }
      }
    }
  ],
  "href": "http://localhost:8000/v1/test-hgid/app/main-menu"
}
 *
 */

/**
 * @api {get} /app/init 2. Get initial data
 * @apiName appInit
 * @apiVersion 0.4.7
 * @apiGroup 2_Initialization
 * @apiDescription When client application has the UUID, then every time the application is loaded, it must make a call
 * to this service to get the vital information, which it will need to build the UI and provide content to end-user.
 * @apiUse GlobalHeaders
 *
 * @apiSuccess {String}     href  The href of current request/resource
 * @apiSuccess {Language[]}   supportedLanguages The list of language objects, which are supported by this hotel group.
 * Elements in this object have a key in the format of ISO 639-1 with region designator
 * @apiSuccess {Language}   supportedLanguages.LANGUAGE_CODE Supported language object
 * ([About languages](#api-1_Basics-basicsLangs))
 * @apiSuccess {String}     defaultLanguage The default language for the hotel group as ISO 639-1 with region designator
 * @apiSuccess {Object}     translations Object, which contain translations of the UI phrases.
 * Elements in this object have a key in the format of ISO 639-1 with region designator,
 * and items under each key are translations in this language
 * @apiSuccess {String[]}     translations.LANGUAGE_CODE An Object containing translations for a language
 * defined by `LANGUAGE_CODE`
 * @apiSuccess {String}     translations.LANGUAGE_CODE.KEY A translated string with a given `KEY`
 *
 * @apiSuccess {Link}       signIn A Link object, which points to a sing in action
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiSuccess {Link}       signUp A Link object, which points to a sing up in action
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiSuccess {Link}       signOut A Link object, which points to a sing out in action
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiSuccess {Link}       mainMenu A Link object, which points to a service, returning main menu structure
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiSuccess {Object}     hotelGroupInfo A Object containing info about hotel group
 * @apiSuccess {Object}     hotelGroupInfo.localized A Object containing info in certain language
 * @apiSuccess {Object}     hotelGroupInfo.localized.LANGUAGE
 * @apiSuccess {Object}     hotelGroupInfo.localized.LANGUAGE.name Hotel name in certain language
 * @apiSuccess {Object}     hotelGroupInfo.localized.LANGUAGE.logo Hotel logo
 *
 * @apiSuccessExample {json} Success-Response:
 {
  "supportedLanguages": {
    "en_GB": {
      "code": "en_GB",
      "englishName": "English",
      "nativeName": "English",
      "rtl": false
    },
    "fr_FR": {
      "code": "fr_FR",
      "englishName": "French",
      "nativeName": "Français",
      "rtl": false
    },
    "et_EE": {
      "code": "et_EE",
      "englishName": "English",
      "nativeName": "Eesti keel",
      "rtl": false
    },
    "ru_RU": {
      "code": "ru_RU",
      "englishName": "Russian",
      "nativeName": "По русски",
      "rtl": false
    },
    "ar_SA": {
      "code": "ar_SA",
      "englishName": "Arabic",
      "nativeName": "al-ʿArabiyyah",
      "rtl": true
    }
  },
  "defaultLanguage": "en_GB",
  "translations": {
    "ar_SA": {
      "skip": "تخطى",
      "hello": "مرحبا",
      "signIn": "تسجيل الدخول",
      "signUp": "سجل",
      "language": "لغة",
      "enterEmail": "من فضلك ادخل بريدك الالكتروني",
      "enterPassword": "الرجاء إدخال كلمة المرور",
      "enterPasswordConfirmation": "الرجاء إعادة كتابة كلمة المرور الخاصة بك"
    },
    "en_GB": {
      "skip": "Skip",
      "hello": "Hello",
      "signIn": "Sign in",
      "signUp": "Sign up",
      "language": "Language",
      "enterEmail": "Please enter your email",
      "enterPassword": "Please enter your password",
      "enterPasswordConfirmation": "Please retype your password"
    },
    "et_EE": {
      "skip": "Mine edasi",
      "hello": "Tere",
      "signIn": "Logi sisse",
      "signUp": "Registreeri",
      "language": "Keel",
      "enterEmail": "Palun sisesta oma email",
      "enterPassword": "Palun sisesta oma parool",
      "enterPasswordConfirmation": "Palun korda parooli"
    },
    "fr_FR": {
      "skip": "Sauter",
      "hello": "Bonjour",
      "signIn": "Connectez-vous",
      "signUp": "Inscrivez-vous",
      "language": "Langue",
      "enterEmail": "Se il vous plaît entrer votre e-mail",
      "enterPassword": "Se il vous plaît entrer votre mot de passe",
      "enterPasswordConfirmation": "Se il vous plaît retaper votre mot de passe"
    },
    "ru_RU": {
      "skip": "Пропустить",
      "hello": "Привет",
      "signIn": "войти в систему",
      "signUp": "зарегистрироваться",
      "language": "Язык",
      "enterEmail": "Пожалуйста, введите адрес электронной почты",
      "enterPassword": "Пожалуйста, введите пароль",
      "enterPasswordConfirmation": "Пожалуйста, еще раз введите пароль"
    }
  },
  "hotelGroupInfo": {
    "localized": {
      "en_GB": {
        "name": "The Leading Hotels of the World",
        "logo": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo"
        }
      },
      "fr_FR": {
        "name": "The Leading Hotels of the World",
        "logo": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo"
        }
      },
      "et_EE": {
        "name": "The Leading Hotels of the World",
        "logo": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo"
        }
      },
      "ru_RU": {
        "name": "The Leading Hotels of the World",
        "logo": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo"
        }
      },
      "ar_SA": {
        "name": "The Leading Hotels of the World",
        "logo": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,t_q10,w_<width>/v1/group_app/hotel_groups/test-hgid/logo"
        }
      }
    }
  },
  "signIn": {
    "href": "http://localhost:8000/v1/test-hgid/profiles/signin",
    "method": "POST"
  },
  "signUp": {
    "href": "http://localhost:8000/v1/test-hgid/profiles/signup",
    "method": "POST"
  },
  "signOut": {
    "href": "http://localhost:8000/v1/test-hgid/profiles/signout",
    "method": "GET"
  },
  "mainMenu": {"href": "http://localhost:8000/v1/test-hgid/app/main-menu"},
  "href": "http://localhost:8000/v1/test-hgid/app/init"
}
 *
 */

/**
 * @api {get} - j. Errors
 * @apiName basicsErrors
 * @apiVersion 0.4.7
 * @apiGroup 1_Basics
 * @apiSampleRequest off
 * @apiDescription When an error occurs, the API returns a error message with a JSON body
 *
 * @apiParamExample {json} Example error object
 * {
    "module": "App",
    "code": "app-inv-route",
    "message": "Invalid path",
    "developerMessage": "This path does not exist",
    "moreInfo": "https://localhost:8000/doc/#errors-app-inv-route",
    "statusCode": 400,
    "isCustomError": true,
    "debuggingData": {
        "requestedPath": "/v1/test-hgid/app/inits"
    },
    "localized": {
        "en_GB": {
            "userMessage": "Invalid path"
        }
    }
}

 * @apiParam (Error object structure) {Object} error
 * @apiParam (Error object structure) {String} error.module Which module gave the error
 * @apiParam (Error object structure) {String} error.message Error message for the end-user (untranslated)
 * @apiParam (Error object structure) {String} error.developerMessage Error message for the developer
 * @apiParam (Error object structure) {String} error.moreInfo Link to a page with more information about this error
 * @apiParam (Error object structure) {Number} error.statusCode=500 The http status code, which was given
 * @apiParam (Error object structure) {Boolean} error.isCustomError=true Boolean indicating, that this error was handled
 * by the system
 * @apiParam (Error object structure) {Object} error.debuggingData Object, which has some information,
 * which can help debug and fix the cause of this error
 * @apiParam (Error object structure) {Object} error.localized Localized information about this error
 * @apiParam (Error object structure) {Object} error.localized.LANG Information in given `LANG`
 * @apiParam (Error object structure) {Object} error.localized.LANG.userMessage Translated message, to show to the end-user
 *
 @apiError (Possible error codes) {String} app-inv-route <span id="error_app-inv-route"></span> This path does not exist
 @apiError (Possible error codes) {String} com-fields-type <span id="error_com-fields-type"></span> Some fields did not match expected type
 @apiError (Possible error codes) {String} cat-query-error <span id="error_cat-query-error"></span> Error occurred querying categories
 @apiError (Possible error codes) {String} cat-cnt-query-error <span id="error_cat-cnt-query-error"></span> Error occurred querying hotel count in categories
 @apiError (Possible error codes) {String} cat-inv-type <span id="error_cat-inv-type"></span> Invalid category type provided
 @apiError (Possible error codes) {String} ai-not-found <span id="error_ai-not-found"></span> Invalid hotel group id or uuid provided
 @apiError (Possible error codes) {String} hg-not-found <span id="error_hg-not-found"></span> Invalid hotel group id provided
 @apiError (Possible error codes) {String} hg-err-save <span id="error_hg-err-save"></span> Error occurred while saving hotel group to database
 @apiError (Possible error codes) {String} hg-err-del <span id="error_hg-err-del"></span> Error occurred while deleting hotel group from database
 @apiError (Possible error codes) {String} hg-err-format <span id="error_hg-err-format"></span> Error occurred while formatting data
 @apiError (Possible error codes) {String} hg-err-up <span id="error_hg-err-up"></span> Error occurred while updating hotel group
 @apiError (Possible error codes) {String} hs-hotel-nf <span id="error_hs-hotel-nf"></span> Hotel, with given parameters was not found
 @apiError (Possible error codes) {String} hs-err-save <span id="error_hs-err-save"></span> Error occurred while saving hotel to database
 @apiError (Possible error codes) {String} hs-src-err <span id="error_hs-src-err"></span> Error searching hotels
 @apiError (Possible error codes) {String} hs-err-del <span id="error_hs-err-del"></span> Error deleting item(s) from search index
 @apiError (Possible error codes) {String} hs-err-for <span id="error_hs-err-for"></span> Error formatting hotel
 @apiError (Possible error codes) {String} iu-err-up-img <span id="error_iu-err-up-img"></span> Error occurred while uploading the image to CDN
 @apiError (Possible error codes) {String} iu-err-no-img <span id="error_iu-err-no-img"></span> The image was not found in given path
 @apiError (Possible error codes) {String} iu-inv-img-obj <span id="error_iu-inv-img-obj"></span> Invalid image object passed fot parsing
 @apiError (Possible error codes) {String} iu-err-del-imgs <span id="error_iu-err-del-imgs"></span> Error occurred, trying to delete images
 @apiError (Possible error codes) {String} iu-err-inv-mc <span id="error_iu-err-inv-mc"></span> Invalid media configuration given
 @apiError (Possible error codes) {String} iu-err-inv-tr <span id="error_iu-err-inv-tr"></span> Invalid transformation given
 @apiError (Possible error codes) {String} iu-err-inv-tr-tp <span id="error_iu-err-inv-tr-tp"></span> Invalid transformation type given
 @apiError (Possible error codes) {String} ps-account-add <span id="error_ps-account-add"></span> Account, with given parameters can not be added
 @apiError (Possible error codes) {String} ps-account-auth <span id="error_ps-account-auth"></span> Account, with given parameters can not be authenticated
 @apiError (Possible error codes) {String} ps-app-get <span id="error_ps-app-get"></span> Application, with given parameters can not be found
 @apiError (Possible error codes) {String} ps-acc-get <span id="error_ps-acc-get"></span> Account, with given parameters can not be found
 @apiError (Possible error codes) {String} ps-session-add <span id="error_ps-session-add"></span> Application, with given parameters can not be found
 @apiError (Possible error codes) {String} ps-session-out <span id="error_ps-session-out"></span> Application can not update sessionEnd
 @apiError (Possible error codes) {String} ps-session-get <span id="error_ps-session-get"></span> Application can not get last session details for this profile
 @apiError (Possible error codes) {String} ps-tenant-verify <span id="error_ps-tenant-verify"></span> Application can not verify user account
 @apiError (Possible error codes) {String} ps-token-add <span id="error_ps-token-add"></span> Application can not add to user account custom data
 @apiError (Possible error codes) {String} ps-verify-inv <span id="error_ps-verify-inv"></span> Verification key is invalid
 @apiError (Possible error codes) {String} ps-verify-failed <span id="error_ps-verify-failed"></span> This page does not exist
 @apiError (Possible error codes) {String} ps-resend-failed <span id="error_ps-resend-failed"></span> Email verification resending failed.
 @apiError (Possible error codes) {String} ps-resend-inv <span id="error_ps-resend-inv"></span> Email verification resending not valid. Check PayLoad details. Login information is necessary
 @apiError (Possible error codes) {String} sr-err-sav-ix <span id="error_sr-err-sav-ix"></span> Error saving to search index
 @apiError (Possible error codes) {String} sr-err-lis-ix <span id="error_sr-err-lis-ix"></span> Error listing search indexes
 @apiError (Possible error codes) {String} sr-err-src <span id="error_sr-err-src"></span> Error searching
 @apiError (Possible error codes) {String} sr-err-new <span id="error_sr-err-new"></span> Error creating search index
 @apiError (Possible error codes) {String} sr-err-del <span id="error_sr-err-del"></span> Error deleting search index
 @apiError (Possible error codes) {String} sr-itm-err-del <span id="error_sr-itm-err-del"></span> Error deleting item from search index
 @apiError (Possible error codes) {String} au-inv-hgid <span id="error_au-inv-hgid"></span> Invalid HotelGroup Id provided
 @apiError (Possible error codes) {String} au-inv-ver <span id="error_au-inv-ver"></span> Invalid API version given
 @apiError (Possible error codes) {String} au-no-uuid <span id="error_au-no-uuid"></span> No UUID given
 @apiError (Possible error codes) {String} au-inv-uuid <span id="error_au-inv-uuid"></span> Invalid UUID given
 @apiError (Possible error codes) {String} au-inv-path <span id="error_au-inv-path"></span> Invalid api call URL
 @apiError (Possible error codes) {String} au-inv-hrefqp <span id="error_au-inv-hrefqp"></span> Invalid queryParams provided, must be an object
 @apiError (Possible error codes) {String} au-inv-linkmethod <span id="error_au-inv-linkmethod"></span> Invalid method provided for link, must be on of GET/PUT/POST/DELETE
 @apiError (Possible error codes) {String} au-inv-req-method <span id="error_au-inv-req-method"></span> Invalid request method
 @apiError (Possible error codes) {String} au-inv-loc-inp <span id="error_au-inv-loc-inp"></span> Invalid location search input
 @apiError (Possible error codes) {String} au-inv-req-bd <span id="error_au-inv-req-bd"></span> Invalid payload
 @apiError (Possible error codes) {String} au-inv-req-ct <span id="error_au-inv-req-ct"></span> Invalid request content type
 @apiError (Possible error codes) {String} au-https-req <span id="error_au-https-req"></span> Use of https protocol is required
 @apiError (Possible error codes) {String} au-ver-req <span id="error_au-ver-req"></span> Use of x-cardola-verify header is required
 @apiError (Possible error codes) {String} au-ver-inv <span id="error_au-ver-inv"></span> Invalid x-cardola-verify header value
 @apiError (Possible error codes) {String} au-inv-verifykey <span id="error_au-inv-verifykey"></span> Invalid Verify Key provided
 @apiError (Possible error codes) {String} au-len-verifykey <span id="error_au-len-verifykey"></span> Invalid Verify Key length. Length must be more then 10 symbols
 @apiError (Possible error codes) {String} sr-err-ol2pi-hl <span id="error_sr-err-ol2pi-hl"></span> Error converting offset and limit to pages, it took too many cycles
 @apiError (Possible error codes) {String} db-cc-db <span id="error_db-cc-db"></span> No database connection
 @apiError (Possible error codes) {String} db-query-fails <span id="error_db-query-fails"></span> Database query failed
 @apiError (Possible error codes) {String} lu-no-langcode <span id="error_lu-no-langcode"></span> No language code provided
 @apiError (Possible error codes) {String} lu-inv-langcode <span id="error_lu-inv-langcode"></span> Invalid language code provided
 @apiError (Possible error codes) {String} lu-no-countrycode <span id="error_lu-no-countrycode"></span> No country code provided
 @apiError (Possible error codes) {String} lu-inv-countrycode <span id="error_lu-inv-countrycode"></span> Invalid country code provided
 @apiError (Possible error codes) {String} lu-no-trans <span id="error_lu-no-trans"></span> No translation found
 */
