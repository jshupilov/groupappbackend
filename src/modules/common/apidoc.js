/*
This file includes COMMON api documentation parts, which can be inherited later
 */


/**
 * @api {get} - a. Introduction
 * @apiName basicsIntroduction
 * @apiVersion 1.7.4
 * @apiGroup 1_Basics
 * @apiSampleRequest off
 * @apiDescription Cardola Group App API provides RESTful services for client applications.
 * In this section we cover some of the basics to get you started.
 *
 * The Cardola Group API will adhere to the following conventions:
 * - Request / Response headers shall indicate the use of JSON and UTF-8 character set in payload.
 *  -  Request will have a header: **Accept: application/json**
 *  - Response will include a header: **Content-Type: application/json; charset=utf-8**
 * - The API has versions, the version of the API is specified in the URL
 * - All references to time shall be expressed in accordance with **ISO8601**
 * - All references to a language shall be expressed in accordance with **ISO 639-1** and include region designator (Example 'en_GB')
 * - All access to data specific to a user will be made over SSL
 * - Mobile applications must not assume that only the JSON fields / collections they require
 * have been returned and should successfully ignore data name value pairs within the JSON it does not require
 * - Mobile applications must fail gracefully and report errors to the user
 * - Caching of non-encrypted data will facilitate caching through the use of the cache control tags ETag’s ,
 * Cache-Control:public, max-age=<seconds> and expires: <date/time> information
 * - All requests, except "app/registration" , shall include a header **X-Cardola-Uuid** with the UUID value,
 * which was given to the app from "/registration" API call.
 * - All requests, which handle delicate data or access limited services shall include a header **X-Cardola-Verify**,
 * which is a checksum for the request ([More here](#api-1_Basics-basicsRequest)).
 *
 */

/**
 * @api {get} /:path b. Request structure
 * @apiName basicsRequest
 * @apiVersion 1.7.4
 * @apiGroup 1_Basics
 * @apiSampleRequest off
 * @apiDescription The REST api has a very specific URL structure,
 * which specifies which version of the API are you working with and
 * which hotel group data are you working with.
 *
 * Also bear in mind that the **method** of the request (GET/PUT/POST/DELETE)
 * is also a vital component of the RESTful services.
 *
 * @apiParam {String} version The API version to use. Example: "v1".
 *
 * List of possible API versions are listed on top right corner of this page.
 * If you want to use version "1.0.1", then you specify the `:version` parameter in the URL as `v1`
 * @apiParam {String} hgId The hotel group Id. Example: "test-hgid"
 * @apiParam {String} path The path, which determines the REST API service, which is requested.
 * For example "app/init" or "categories"
 *
 * @apiHeader (Header) {String} X-Cardola-Uuid UUID value, which has been assigned to this application instance
 * through /registration call.
 *
 * @apiHeader (Header) {String} X-Cardola-Verify The Hash checksum of the request.
 * The hash is calculated using the API KEY provided by Cardola.
 *
 * **The calculation algorithm uses following inputs:**
 * - **url** is the full url including query parameters
 * - **uuid** is the UUID of the app, which has been retrieved from /app/registration (value used in the X-Cardola-Uuid header)
 *  - **NB!** This value is empty for **app/registration** service, because the app does not know it yet
 * - **apiKey** is an API KEY provided by Cardola
 * - **randomSalt** is a random string with a fixed length of 10
 *
 * **The method of calculating the checksum:**
 * - a = sha256(url + apiKey)
 * - b = sha256(a + randomSalt)
 * - c = sha256(b + uuid)
 * - checksum = toHex(c) + randomSalt (toHex converts each character code from decimal to hexadecimal)
 *
 * **Sample:**
 *
 * Data:
 * - url: `https://localhost:8000/v1/test-hgid/profiles/status`
 * - uuid: `b4cf2e90-b134-11e4-af5d-5d230ac74c0b`
 * - apiKey: `test-api-key`
 * - randomSalt: `5324b42a7f`
 * Resulting hash: `fefa828c04beba4be0100ffae77b4d3837979deef6ab8bf1b17c01f861ff477fdc9ad44edb`
 *
 * Calculation:
 * - a = sha256("https://localhost:8000/v1/test-hgid/profiles/status" + "test-api-key") = `-_ï\u0000åÁñÑÏviÉÅüîµs®¾{èìï¼xu)V\u001fYD`
 * - b = sha256(a + "5324b42a7f") = `Ö\u0006ëÜ\u0006\u000ecÝO5ðÇDµ>nMã\u0016§>ìfã3ÔD`
 * - c = sha256(b + "b4cf2e90-b134-11e4-af5d-5d230ac74c0b") = `ÎøÃV?_¾øÃdçøt>1\u0013±Çü0à\u0003¬rnÜ@Å¢`
 * - checksum = toHex(c) + randomSalt = "cef8c3563f5fbef8c364e7f8743e3113b1c7fc30e00398ac72856edc40c5a2e3" + "5324b42a7f" = `cef8c3563f5fbef8c364e7f8743e3113b1c7fc30e00398ac72856edc40c5a2e35324b42a7f`
 *
 *
 *
 * @apiHeaderExample {html} Header-Example:
 * X-Cardola-Uuid: b4cf2e90-b134-11e4-af5d-5d230ac74c0b
 * X-Cardola-Verify: cef8c3563f5fbef8c364e7f8743e3113b1c7fc30e00398ac72856edc40c5a2e35324b42a7f
 *
 *
 */


/**
 * @api {get} - c. Links
 * @apiName basicsLinks
 * @apiVersion 1.7.4
 * @apiGroup 1_Basics
 * @apiSampleRequest off
 * @apiDescription If a resource needs to reference to another resource (or an actions), it will use a `Link` object.
 *
 * @apiParam (Link object) {Object} link Link object
 * @apiParam (Link object) {String} link.href The URL to REST service, which returns the linked resource
 * or does an action.
 * @apiParam (Link object) {String=GET,PUT,POST,DELETE} [link.method=GET] The request method to use,
 * if this is missing then GET must be used
 *
 *
 */

/**
 * @api {get} - d. Languages
 * @apiName basicsLangs
 * @apiVersion 1.7.4
 * @apiGroup 1_Basics
 * @apiSampleRequest off
 * @apiDescription All resources, which are returned from the REST API are returned in all supported languages.
 * It is up to the client application to display the content in a right language.
 *
 * Every resource object, which is returned from the REST API, includes a `localized` Object, which holds translated
 * information about this resource. Note that not all of the information is localized. The `localized` object has
 * a key for every language, into which this resource is translated into, if a key for some language is missing,
 * it means that the resource is not translated into this language.
 *
 * When language is represented in the API response, it has the following structure:
 *
 * @apiParam (Language object) {Object}     language Language object
 * @apiParam (Language object) {String{5}}  language.code Language code in ISO 639-1 plus region designator (Example `en_GB`)
 * @apiParam (Language object) {String}     language.englishName Language name in english
 * @apiParam (Language object) {String}     language.nativeName Language name in its own language
 * @apiParam (Language object) {Boolean}    language.rtl Boolean indicating if this language is written from Right to Left
 *
 *
 */


/**
 * @api {get} /images/<width>x<height>/sample.jpg e. Images
 * @apiName basicsImages
 * @apiVersion 1.7.4
 * @apiGroup 1_Basics
 * @apiSampleRequest off
 * @apiDescription If a resource must return a reference to a image, it will return an Object,
 * which contains url's, with tokens, to the image
 *
 * Tokens in the url are wrapped between `<` and `>` (as seen in the example url)
 *
 * NB! I a request is made to the final url of the image, then it might take couple of seconds to respond,
 * because if the image is requested with a size, that has never been requested before,
 * the server will render the image to that size and return it.
 *
 * @apiParam (Image object) {Object}     image Image object
 * @apiParam (Image object) {String}     image.highRes URL to high resolution image, with tokens
 * @apiParam (Image object) {String}     image.lowRes URL to low resolution image, with tokens
 *
 * @apiParam (Tokens) {Number} width The required width of the image in pixels
 *
 * Example: "100" to get image with width of 100 pixels
 * @apiParam (Tokens) {Number} height The required height of the image in pixels
 *
 * Example: "90" to get image with height of 90 pixels
 *
 *
 */

/**
 * @api {get} - f. Testing
 * @apiName basicsTesting
 * @apiVersion 1.7.4
 * @apiGroup 1_Basics
 * @apiSampleRequest off
 * @apiDescription To test the the RESTful services you must use our testing server
 * `http://group-app.herokuapp.com` instead of `http://api.cadrola.com`.
 *
 * For testing purposes you DO NOT need to use the `X-Cardola-Verify` header.
 * So if you do not provide this header, it will work.
 * If you do provide this header, then it is validated and must have a correct value,
 * as described in [here](#api-1_Basics-basicsRequest)
 *
 * We have setup a demo Hotel Group called `Cardola Luxury hotels & resorts`,
 * with a Hotel Group Id (`:hgid` part in the URL) value of `test-hgid`.
 *
 * For example, if you want to register your app (first thing you must do! [/app/registration](#api-2_Initialization-appRegistration)),
 * you must use the url: `http://group-app.herokuapp.com/v1/test-hgid/app/registration`
 *
 *
 *
 */


/**
 * @api {get} - g. Resources
 * @apiName basicsResources
 * @apiVersion 1.7.4
 * @apiGroup 1_Basics
 * @apiSampleRequest off
 * @apiDescription As a RESTful service, this API operates with resources. A resource is an object, with a certain type.
 * In this API we operate with resources like `Hotel` and `Category`
 *
 * All resources have their own data structure, but they also share some common fields
 *
 * @apiParam (Common fields on resources) {String} href A URL, which points to a service,
 * which returns only this resource, with detail information.
 *
 *
 */

/**
 * @apiDefine CollectionStructure
 * @apiVersion 1.7.4
 * @apiParam (Collection structure) {String} href A URL, which points to a service,
 * which returns this collection
 * @apiParam (Collection structure) {Number} offset=0 An offset for pagination, if set, then response will skip the
 * first `N` values.
 * @apiParam (Collection structure) {Number} limit=10 A limit for maximum number of items returned in one response
 * @apiParam (Collection structure) {Number} total Total number of items in this collection, without limit and offset
 * @apiParam (Collection structure) {Number} [next] Href to next page in this collection - if next page exists
 * @apiParam (Collection structure) {Number} [prev] Href to previous page in this collection - if previous page exists
 * @apiParam (Collection structure) {Object[]} items An array of items, which are included in this collection.
 * The contents of this array is controlled by `offset` and `limit` query parameters
 * @apiParam (Collection structure) {Object} [parent] An optional parent item for items in the collection
 */

/**
 * @apiDefine CollectionQueryParams
 * @apiVersion 1.7.4
 * @apiParam (Collection query parameters) {Number} limit=10 A limit for maximum number of items returned in one response
 * @apiParam (Collection query parameters) {Number} offset=0 An offset for pagination, if set, then response will skip the
 * first `N` values.
 *
 */

/**
 * @apiDefine GlobalHeaders
 * @apiHeader (Global headers) {string} [x-cardola-uuid] The unique ID of the API, fetched from app/registration response.
 * @apiHeaderExample {json} Header-Example:
 { "x-cardola-uuid": "test-uuid-for-testing" }
 */

/**
 * @api {get} - h. Collections
 * @apiName basicsCollections
 * @apiVersion 1.7.4
 * @apiGroup 1_Basics
 * @apiSampleRequest off
 * @apiDescription Some API services need to return multiple items in one response, like list of hotels or categories.
 * A list of same type items is represented as a `Collection` in this API. A `Collection` has a predefined high level
 * structure, which is described above.
 *
 * If interacting with a API endpoint, which returns a collection,
 * then there are also some query parameters available for controlling the output, these parameters are described below.
 *
 *
 * @apiUse CollectionStructure
 * @apiUse CollectionQueryParams
 *
 *
 * @apiParamExample {json} Collection sample:
 {
  "href": "http://.......",
  "offset": 2,
  "limit": 10,
  "total": 21,
  "next": "http:........." ,
  "prev": "http:........." ,
  "items": [
    {....item 1},
    {....item 2}
  ],
  "parent": {....parent item}
 }
 * @apiParamExample {html} Request url sample:
 http://...../categories?limit=10&offset=20
 ==> This will return 10 items starting from item 21
 *
 */

/**
 * @api {get} - i. Security
 * @apiName basicsSecurity
 * @apiVersion 1.7.4
 * @apiGroup 1_Basics
 * @apiSampleRequest off
 * @apiDescription Some API services are not public and they handle delicate data, which must be secured.
 * Therefore we have defined security levels, which can be required by some of the services.
 * In this documentation the services, which require some security (more than level 0),
 * have a note about the level, they require.
 *
 * These security levels are:
 *
 * - **0** - No extra security required
 * - **1** - Use of `x-cardola-uuid` header is required ([About](#api-1_Basics-basicsRequest))
 * - **2** - Requirements of level **1** + Use of `https` protocol is required
 * - **3** - Requirements of level **2** + Use of `x-cardola-verify` header is required ([About](#api-1_Basics-basicsRequest))
 */


/**
 * @api {get} - j. Errors
 * @apiName basicsErrors
 * @apiVersion 1.7.4
 * @apiGroup 1_Basics
 * @apiSampleRequest off
 * @apiDescription When an error occurs, the API returns a error message with a JSON body
 *
 * @apiParamExample {json} Example error object
 * {
    "module": "App",
    "code": "app-inv-route",
    "message": "Invalid path",
    "developerMessage": "This path does not exist",
    "moreInfo": "https://localhost:8000/doc/#errors-app-inv-route",
    "statusCode": 400,
    "isCustomError": true,
    "isRetriable": true,
    "debuggingData": {
        "requestedPath": "/v1/test-hgid/app/inits"
    },
    "localized": {
        "en_GB": {
            "userMessage": "Invalid path"
        }
    }
}

 * @apiParam (Error object structure) {Object} error
 * @apiParam (Error object structure) {String} error.module Which module gave the error
 * @apiParam (Error object structure) {String} error.message Error message for the end-user (untranslated)
 * @apiParam (Error object structure) {String} error.developerMessage Error message for the developer
 * @apiParam (Error object structure) {String} error.moreInfo Link to a page with more information about this error
 * @apiParam (Error object structure) {Number} error.statusCode=500 The http status code, which was given
 * @apiParam (Error object structure) {Boolean} error.isCustomError=true Boolean indicating, that this error was handled
 * by the system
 * @apiParam (Error object structure) {Boolean} error.isRetriable=false Boolean indicating, that this error is retriable or not
 * @apiParam (Error object structure) {Object} error.debuggingData Object, which has some information,
 * which can help debug and fix the cause of this error
 * @apiParam (Error object structure) {Object} error.localized Localized information about this error
 * @apiParam (Error object structure) {Object} error.localized.LANG Information in given `LANG`
 * @apiParam (Error object structure) {Object} error.localized.LANG.userMessage Translated message, to show to the end-user
 *
 @apiError (Possible error codes) {String} app-inv-route <span id="error_app-inv-route"></span> This path does not exist
 @apiError (Possible error codes) {String} com-fields-type <span id="error_com-fields-type"></span> Some fields did not match expected type
 @apiError (Possible error codes) {String} cat-query-error <span id="error_cat-query-error"></span> Error occurred querying categories
 @apiError (Possible error codes) {String} cat-cnt-query-error <span id="error_cat-cnt-query-error"></span> Error occurred querying hotel count in categories
 @apiError (Possible error codes) {String} cat-inv-type <span id="error_cat-inv-type"></span> Invalid category type provided
 @apiError (Possible error codes) {String} ai-not-found <span id="error_ai-not-found"></span> Invalid hotel group id or uuid provided
 @apiError (Possible error codes) {String} hg-not-found <span id="error_hg-not-found"></span> Invalid hotel group id provided
 @apiError (Possible error codes) {String} hg-err-save <span id="error_hg-err-save"></span> Error occurred while saving hotel group to database
 @apiError (Possible error codes) {String} hg-err-del <span id="error_hg-err-del"></span> Error occurred while deleting hotel group from database
 @apiError (Possible error codes) {String} hg-err-format <span id="error_hg-err-format"></span> Error occurred while formatting data
 @apiError (Possible error codes) {String} hg-err-up <span id="error_hg-err-up"></span> Error occurred while updating hotel group
 @apiError (Possible error codes) {String} hs-hotel-nf <span id="error_hs-hotel-nf"></span> Hotel, with given parameters was not found
 @apiError (Possible error codes) {String} hs-err-save <span id="error_hs-err-save"></span> Error occurred while saving hotel to database
 @apiError (Possible error codes) {String} hs-src-err <span id="error_hs-src-err"></span> Error searching hotels
 @apiError (Possible error codes) {String} hs-inv-loc-inp <span id="error_hs-inv-loc-inp"></span> Invalid location search input
 @apiError (Possible error codes) {String} hs-err-del <span id="error_hs-err-del"></span> Error deleting item(s) from search index
 @apiError (Possible error codes) {String} hs-err-for <span id="error_hs-err-for"></span> Error formatting hotel
 @apiError (Possible error codes) {String} hs-err-ip <span id="error_hs-err-ip"></span> Invalid request ip address
 @apiError (Possible error codes) {String} iu-err-up-img <span id="error_iu-err-up-img"></span> Error occurred while uploading the image to CDN
 @apiError (Possible error codes) {String} iu-err-no-img <span id="error_iu-err-no-img"></span> The image was not found in given path
 @apiError (Possible error codes) {String} iu-inv-img-obj <span id="error_iu-inv-img-obj"></span> Invalid image object passed fot parsing
 @apiError (Possible error codes) {String} iu-err-del-imgs <span id="error_iu-err-del-imgs"></span> Error occurred, trying to delete images
 @apiError (Possible error codes) {String} iu-err-inv-mc <span id="error_iu-err-inv-mc"></span> Invalid media configuration given
 @apiError (Possible error codes) {String} iu-err-inv-tr <span id="error_iu-err-inv-tr"></span> Invalid transformation given
 @apiError (Possible error codes) {String} iu-err-inv-tr-tp <span id="error_iu-err-inv-tr-tp"></span> Invalid transformation type given
 @apiError (Possible error codes) {String} ps-account-add <span id="error_ps-account-add"></span> Account, with given parameters can not be added
 @apiError (Possible error codes) {String} ps-account-auth <span id="error_ps-account-auth"></span> Account, with given parameters can not be authenticated
 @apiError (Possible error codes) {String} ps-app-get <span id="error_ps-app-get"></span> Application, with given parameters can not be found
 @apiError (Possible error codes) {String} ps-acc-get <span id="error_ps-acc-get"></span> Account, with given parameters can not be found
 @apiError (Possible error codes) {String} ps-session-add <span id="error_ps-session-add"></span> Application, with given parameters can not be found
 @apiError (Possible error codes) {String} ps-session-out <span id="error_ps-session-out"></span> Application can not update sessionEnd
 @apiError (Possible error codes) {String} ps-session-get <span id="error_ps-session-get"></span> Application can not get last session details for this profile
 @apiError (Possible error codes) {String} ps-session-inv <span id="error_ps-session-inv"></span> You have invalid session details
 @apiError (Possible error codes) {String} ps-tenant-verify <span id="error_ps-tenant-verify"></span> Application can not verify user account
 @apiError (Possible error codes) {String} ps-token-add <span id="error_ps-token-add"></span> Application can not add to user account custom data
 @apiError (Possible error codes) {String} ps-verify-inv <span id="error_ps-verify-inv"></span> Verification key is invalid
 @apiError (Possible error codes) {String} ps-verify-failed <span id="error_ps-verify-failed"></span> This page does not exist
 @apiError (Possible error codes) {String} ps-resend-failed <span id="error_ps-resend-failed"></span> Email verification resending failed.
 @apiError (Possible error codes) {String} ps-resend-inv <span id="error_ps-resend-inv"></span> Email verification resending not valid. Check PayLoad details. Login information is necessary
 @apiError (Possible error codes) {String} sr-err-sav-ix <span id="error_sr-err-sav-ix"></span> Error saving to search index
 @apiError (Possible error codes) {String} sr-err-lis-ix <span id="error_sr-err-lis-ix"></span> Error listing search indexes
 @apiError (Possible error codes) {String} sr-err-src <span id="error_sr-err-src"></span> Error searching
 @apiError (Possible error codes) {String} sr-err-new <span id="error_sr-err-new"></span> Error creating search index
 @apiError (Possible error codes) {String} sr-err-del <span id="error_sr-err-del"></span> Error deleting search index
 @apiError (Possible error codes) {String} sr-itm-err-del <span id="error_sr-itm-err-del"></span> Error deleting item from search index
 @apiError (Possible error codes) {String} au-inv-hgid <span id="error_au-inv-hgid"></span> Invalid HotelGroup Id provided
 @apiError (Possible error codes) {String} au-inv-ver <span id="error_au-inv-ver"></span> Invalid API version given
 @apiError (Possible error codes) {String} au-no-uuid <span id="error_au-no-uuid"></span> No UUID given
 @apiError (Possible error codes) {String} au-inv-uuid <span id="error_au-inv-uuid"></span> Invalid UUID given
 @apiError (Possible error codes) {String} au-inv-path <span id="error_au-inv-path"></span> Invalid api call URL
 @apiError (Possible error codes) {String} au-inv-hrefqp <span id="error_au-inv-hrefqp"></span> Invalid queryParams provided, must be an object
 @apiError (Possible error codes) {String} au-inv-linkmethod <span id="error_au-inv-linkmethod"></span> Invalid method provided for link, must be on of GET/PUT/POST/DELETE
 @apiError (Possible error codes) {String} au-inv-req-method <span id="error_au-inv-req-method"></span> Invalid request method
 @apiError (Possible error codes) {String} au-inv-loc-inp <span id="error_au-inv-loc-inp"></span> Invalid location search input
 @apiError (Possible error codes) {String} au-inv-req-bd <span id="error_au-inv-req-bd"></span> Invalid payload
 @apiError (Possible error codes) {String} au-inv-req-ct <span id="error_au-inv-req-ct"></span> Invalid request content type
 @apiError (Possible error codes) {String} au-https-req <span id="error_au-https-req"></span> Use of https protocol is required
 @apiError (Possible error codes) {String} au-ver-req <span id="error_au-ver-req"></span> Use of x-cardola-verify header is required
 @apiError (Possible error codes) {String} au-ver-inv <span id="error_au-ver-inv"></span> Invalid x-cardola-verify header value
 @apiError (Possible error codes) {String} au-inv-verifykey <span id="error_au-inv-verifykey"></span> Invalid Verify Key provided
 @apiError (Possible error codes) {String} au-len-verifykey <span id="error_au-len-verifykey"></span> Invalid Verify Key length. Length must be more then 10 symbols
 @apiError (Possible error codes) {String} sr-err-ol2pi-hl <span id="error_sr-err-ol2pi-hl"></span> Error converting offset and limit to pages, it took too many cycles
 @apiError (Possible error codes) {String} db-cc-db <span id="error_db-cc-db"></span> No database connection
 @apiError (Possible error codes) {String} db-query-fails <span id="error_db-query-fails"></span> Database query failed
 @apiError (Possible error codes) {String} lu-no-langcode <span id="error_lu-no-langcode"></span> No language code provided
 @apiError (Possible error codes) {String} lu-inv-langcode <span id="error_lu-inv-langcode"></span> Invalid language code provided
 @apiError (Possible error codes) {String} lu-no-countrycode <span id="error_lu-no-countrycode"></span> No country code provided
 @apiError (Possible error codes) {String} lu-inv-countrycode <span id="error_lu-inv-countrycode"></span> Invalid country code provided
 @apiError (Possible error codes) {String} lu-no-trans <span id="error_lu-no-trans"></span> No translation found
 @apiError (Possible error codes) {String} system-error <span id="error_system-error"></span> There is a problem with the code
 @apiError (Possible error codes) {String} lu-transf-merge-fail <span id="error_lu-transf-merge-fail"></span> Failed to merge translations with defaults
 @apiError (Possible error codes) {String} lu-vdt-inv-val <span id="error_lu-vdt-inv-val"></span> Invalid translation object value
 @apiError (Possible error codes) {String} lu-vdt-key-miss <span id="error_lu-vdt-key-miss"></span> Key missing from translation object
 @apiError (Possible error codes) {String} lu-vdt-inv-lang <span id="error_lu-vdt-inv-lang"></span> Invalid language in translations
 @apiError (Possible error codes) {String} lu-vdt-inv-tpl <span id="error_lu-vdt-inv-tpl"></span> Invalid translation value template
 @apiError (Possible error codes) {String} mv-inv-val-type <span id="error_mv-inv-val-type"></span> Invalid validation type given
 @apiError (Possible error codes) {String} mv-inv-type <span id="error_mv-inv-type"></span> Invalid data type given
 @apiError (Possible error codes) {String} mv-inv-val <span id="error_mv-inv-val"></span> Invalid value was given
 @apiError (Possible error codes) {String} mv-key-miss <span id="error_mv-key-miss"></span> Required key is missing
 @apiError (Possible error codes) {String} mv-src-mis-prefix <span id="error_mv-src-mis-prefix"></span> The image public_id should start with a certain prefix
 @apiError (Possible error codes) {String} cv-inv-type <span id="error_cv-inv-type"></span> Category has invalid data type
 @apiError (Possible error codes) {String} cv-inv-val <span id="error_cv-inv-val"></span> Category has invalid data type
 @apiError (Possible error codes) {String} cv-inv-val-type <span id="error_cv-inv-val-type"></span> Invalid validation type given
 @apiError (Possible error codes) {String} cv-key-miss <span id="error_cv-key-miss"></span> Missing a required key in category
 @apiError (Possible error codes) {String} cv-inv-lang <span id="error_cv-inv-lang"></span> Invalid language provided
 @apiError (Possible error codes) {String} cv-nam-empty <span id="error_cv-nam-empty"></span> Category name cannot be empty string
 @apiError (Possible error codes) {String} hgv-dbl-catid <span id="error_hgv-dbl-catid"></span> Two categories have the same categoryId
 @apiError (Possible error codes) {String} cv-cat-no-defl <span id="error_cv-cat-no-defl"></span> Category is missing translations in default language
 @apiError (Possible error codes) {String} msv-inv-val <span id="error_msv-inv-val"></span> Invalid email template key value
 @apiError (Possible error codes) {String} msv-key-miss <span id="error_msv-key-miss"></span> Required email template key is missing
 @apiError (Possible error codes) {String} msv-inv-tpl <span id="error_msv-inv-tpl"></span> Invalid email template given
 @apiError (Possible error codes) {String} msv-inv-type <span id="error_msv-inv-type"></span> Invalid email template key type
 @apiError (Possible error codes) {String} msv-non-ex-val <span id="error_msv-non-ex-val"></span> This value not exists
 @apiError (Possible error codes) {String} msv-inv-frm-email <span id="error_msv-inv-frm-email"></span> Invalid sender email address given
 @apiError (Possible error codes) {String} msv-inv-to-email <span id="error_msv-inv-to-email"></span> Invalid email address given
 @apiError (Possible error codes) {String} msv-inv-lang-code <span id="error_msv-inv-lang-code"></span> Invalid language code provided
 @apiError (Possible error codes) {String} msv-tpld-inv-val <span id="error_msv-tpld-inv-val"></span> Email template data has invalid information
 @apiError (Possible error codes) {String} msv-tpld-mis-val <span id="error_msv-tpld-mis-val"></span> Email template is missing some inpu data
 @apiError (Possible error codes) {String} hgv-key-miss <span id="error_hgv-key-miss"></span> A key is missing from the data set
 @apiError (Possible error codes) {String} hgv-inv-type <span id="error_hgv-inv-type"></span> A value has a wrong data type
 @apiError (Possible error codes) {String} hgv-inv-val <span id="error_hgv-inv-val"></span> A value has an invalid value
 @apiError (Possible error codes) {String} hgv-inv-val-type <span id="error_hgv-inv-val-type"></span> Invalid validation type provided
 @apiError (Possible error codes) {String} hgv-inv-qty <span id="error_hgv-inv-qty"></span> A key items count is invalid
 @apiError (Possible error codes) {String} hgv-hgId-exists <span id="error_hgv-hgId-exists"></span> Hotel group with this id already exists
 @apiError (Possible error codes) {String} hgv-inv-def-lan <span id="error_hgv-inv-def-lan"></span> Invalid default language
 @apiError (Possible error codes) {String} hgv-inv-trans-lang <span id="error_hgv-inv-trans-lang"></span> Invalid language code for translations given
 @apiError (Possible error codes) {String} hgv-trans-no-def <span id="error_hgv-trans-no-def"></span> No translations given for default language
 @apiError (Possible error codes) {String} hgv-trans-nonstr <span id="error_hgv-trans-nonstr"></span> Translation must be provided as string
 @apiError (Possible error codes) {String} hgv-trans-empty <span id="error_hgv-trans-empty"></span> Translations cannot be empty strings
 @apiError (Possible error codes) {String} hgv-men-nav-inv <span id="error_hgv-men-nav-inv"></span> The menu item action.navigation structure is incorrect
 @apiError (Possible error codes) {String} hgv-men-inv-cat <span id="error_hgv-men-inv-cat"></span> The menu item is pointing to nonexistent category
 @apiError (Possible error codes) {String} hgv-men-too-deep <span id="error_hgv-men-too-deep"></span> The menu item has a children, which points to a parent or there are more than 100 levels of children
 @apiError (Possible error codes) {String} hgv-men-inv-lang <span id="error_hgv-men-inv-lang"></span> The menu item has a localized translation with an invalid language key
 @apiError (Possible error codes) {String} hgv-inf-miss-def-lang <span id="error_hgv-inf-miss-def-lang"></span> Hotel group information id not given in default language
 @apiError (Possible error codes) {String} hgv-inf-inv-lang <span id="error_hgv-inf-inv-lang"></span> Hotel group information is provided with invalid langauge key
 @apiError (Possible error codes) {String} hgv-inv-parcat <span id="error_hgv-inv-parcat"></span> The parent category Id does not exist
 @apiError (Possible error codes) {String} hgv-dbl-men-id <span id="error_hgv-dbl-men-id"></span> Two main menu items have a same menuItemId
 @apiError (Possible error codes) {String} hgv-emt-no-def-lang <span id="error_hgv-emt-no-def-lang"></span> The email template is not provided in default langauge
 @apiError (Possible error codes) {String} mss-rndr-err <span id="error_mss-rndr-err"></span> Error rendering given template
 @apiError (Possible error codes) {String} mss-snd-inv-lang <span id="error_mss-snd-inv-lang"></span> Invalid language code given
 @apiError (Possible error codes) {String} mss-snd-mis-lang <span id="error_mss-snd-mis-lang"></span> Email template does not support this language
 @apiError (Possible error codes) {String} mss-snd-err <span id="error_mss-snd-err"></span> Error sending email
 @apiError (Possible error codes) {String} ps-as-add <span id="error_ps-as-add"></span> Account store, with given parameters can not be added
 @apiError (Possible error codes) {String} ps-dir-get <span id="error_ps-dir-get"></span> Cannot get Stormpath directory
 @apiError (Possible error codes) {String} ps-dir-add <span id="error_ps-dir-add"></span> Directory, with given parameters can not be added
 @apiError (Possible error codes) {String} ps-dir-upd <span id="error_ps-dir-upd"></span> Directory, with given parameters can not be updated
 @apiError (Possible error codes) {String} ps-customdata-sav <span id="error_ps-customdata-sav"></span> Application can not get information of account custom data
 @apiError (Possible error codes) {String} ps-reset-failed <span id="error_ps-reset-failed"></span> Email is invalid or does not exist in db.
 @apiError (Possible error codes) {String} ps-reset-inv <span id="error_ps-reset-inv"></span> Password reset is not valid. Check PayLoad details. Email is required
 @apiError (Possible error codes) {String} ps-no-acc <span id="error_ps-no-acc"></span> User with this email does not exist
 @apiError (Possible error codes) {String} ps-inv-pass <span id="error_ps-inv-pass"></span> Invalid password entered
 @apiError (Possible error codes) {String} ps-inv-body <span id="error_ps-inv-body"></span> Request body has invalid value
 @apiError (Possible error codes) {String} ps-mis-inp <span id="error_ps-mis-inp"></span> Missing input parameter
 @apiError (Possible error codes) {String} ps-inv-inp <span id="error_ps-inv-inp"></span> Invalid input parameter
 @apiError (Possible error codes) {String} ps-weak-pass <span id="error_ps-weak-pass"></span> Too weak password
 @apiError (Possible error codes) {String} ps-inv-email <span id="error_ps-inv-email"></span> Email is invalid
 @apiError (Possible error codes) {String} ps-inv-gn <span id="error_ps-inv-gn"></span> Given name is invalid
 @apiError (Possible error codes) {String} ps-inv-sn <span id="error_ps-inv-sn"></span> Surname is invalid
 @apiError (Possible error codes) {String} ps-ex-email <span id="error_ps-ex-email"></span> Email is not unique!
 @apiError (Possible error codes) {String} ps-set-inv-inp <span id="error_ps-set-inv-inp"></span> Invalid input type
 @apiError (Possible error codes) {String} ps-frmt-res <span id="error_ps-frmt-res"></span> Error occurred while formatting the response
 @apiError (Possible error codes) {String} ps-resettoken-inv <span id="error_ps-resettoken-inv"></span> Error occurred with token validation
 @apiError (Possible error codes) {String} ps-resetpass-inv <span id="error_ps-resetpass-inv"></span> Error occurred with inputs, missing input parameters
 @apiError (Possible error codes) {String} ps-resetpassmatch-inv <span id="error_ps-resetpassmatch-inv"></span> Passwords do not match
 @apiError (Possible error codes) {String} com-fields-miss <span id="error_com-fields-miss"></span> Missing fields in body
 @apiError (Possible error codes) {String} hv-non-str <span id="error_hv-non-str"></span> Field must be provided as string
 @apiError (Possible error codes) {String} hv-non-bool <span id="error_hv-non-bool"></span> Field must be provided as boolean
 @apiError (Possible error codes) {String} hv-key-miss <span id="error_hv-key-miss"></span> A key is missing from the data set
 @apiError (Possible error codes) {String} hv-key-empty <span id="error_hv-key-empty"></span> Key cannot be empty
 @apiError (Possible error codes) {String} hv-inv-type <span id="error_hv-inv-type"></span> A value has a wrong data type
 @apiError (Possible error codes) {String} hv-inv-val <span id="error_hv-inv-val"></span> A value has an invalid value
 @apiError (Possible error codes) {String} hv-inv-val-type <span id="error_hv-inv-val-type"></span> Invalid validation type provided
 @apiError (Possible error codes) {String} hv-inv-email <span id="error_hv-inv-email"></span> Hotel email is invalid.
 @apiError (Possible error codes) {String} hv-inv-url <span id="error_hv-inv-url"></span> Hotel web site url is invalid.
 @apiError (Possible error codes) {String} hv-inv-lang <span id="error_hv-inv-lang"></span> Invalid language code for localization given
 @apiError (Possible error codes) {String} hv-nodef-lang <span id="error_hv-nodef-lang"></span> Hotel localization default language is miss
 @apiError (Possible error codes) {String} hv-hId-exists <span id="error_hv-hId-exists"></span> Hotel with this id already exists
 @apiError (Possible error codes) {String} hv-hgId-exists <span id="error_hv-hgId-exists"></span> Hotel Group with this id already exists
 @apiError (Possible error codes) {String} test-code <span id="error_test-code"></span> Mes2
 @apiError (Possible error codes) {String} je-inv-job-cd <span id="error_je-inv-job-cd"></span> Invalid job code given
 @apiError (Possible error codes) {String} au-inv-cache <span id="error_au-inv-cache"></span> Invalid cache type provided
 @apiError (Possible error codes) {String} au-err-etag <span id="error_au-err-etag"></span> Cannot apply etag, maybe empty reponse body?
 @apiError (Possible error codes) {String} cu-string-inv <span id="error_cu-string-inv"></span> TypeError: Bad input string
 @apiError (Possible error codes) {String} hg-publish-fail <span id="error_hg-publish-fail"></span> Publish hotelGroup failed
 @apiError (Possible error codes) {String} hg-unpublish-fail <span id="error_hg-unpublish-fail"></span> Unpublish hotelGroup failed
 @apiError (Possible error codes) {String} hd-hotel-ne <span id="error_hd-hotel-ne"></span> Hotel, with given parameters was not found
 @apiError (Possible error codes) {String} ver-no-entity <span id="error_ver-no-entity"></span> Entity not specified
 @apiError (Possible error codes) {String} ver-no-status <span id="error_ver-no-status"></span> Status missing in item data
 @apiError (Possible error codes) {String} ver-unk-table <span id="error_ver-unk-table"></span> Unknown table specified for add item
 @apiError (Possible error codes) {String} ver-inv-val-type <span id="error_ver-inv-val-type"></span> Invalid validation type provided
 @apiError (Possible error codes) {String} ver-inv-type <span id="error_ver-inv-type"></span> A value has a wrong data type
 @apiError (Possible error codes) {String} ver-inv-status <span id="error_ver-inv-status"></span> A value has a invalid status
 @apiError (Possible error codes) {String} ver-inv-applied <span id="error_ver-inv-applied"></span> A value can not be applied
 @apiError (Possible error codes) {String} ver-inv-guid <span id="error_ver-inv-guid"></span> A value has a invalid guid
 @apiError (Possible error codes) {String} val-inv-iso-date <span id="error_val-inv-iso-date"></span> Invalid date format
 @apiError (Possible error codes) {String} ver-mis-sched <span id="error_ver-mis-sched"></span> Scheduled at key missing
 @apiError (Possible error codes) {String} ver-no-change <span id="error_ver-no-change"></span> New item is same as old one
 @apiError (Possible error codes) {String} ver-no-prev <span id="error_ver-no-prev"></span> Previous version not found in system
 @apiError (Possible error codes) {String} ss-err-hpost <span id="error_ss-err-hpost"></span> Error posting the hash to baking endpoint
 @apiError (Possible error codes) {String} htl-map-error <span id="error_htl-map-error"></span> Error occurred while mapping hotel data
 @apiError (Possible error codes) {String} htl-imp-err <span id="error_htl-imp-err"></span> Unable to import hotel
 @apiError (Possible error codes) {String} res-cd-save <span id="error_res-cd-save"></span> Can not save reservation key to Stormpath custom data
 @apiError (Possible error codes) {String} res-not-auth <span id="error_res-not-auth"></span> This user not authenticated. Please log in and try again
 @apiError (Possible error codes) {String} res-inv-email <span id="error_res-inv-email"></span> This profile email is invalid
 @apiError (Possible error codes) {String} imp-comm-err <span id="error_imp-comm-err"></span> Unable to make http request
 @apiError (Possible error codes) {String} imp-parse-fail <span id="error_imp-parse-fail"></span> Wrong input content format
 @apiError (Possible error codes) {String} int-img-err <span id="error_int-img-err"></span> Unable to parse image object
 @apiError (Possible error codes) {String} http-inv-res <span id="error_http-inv-res"></span> Invalid response from http(s) request
 @apiError (Possible error codes) {String} u-err-no-img <span id="error_u-err-no-img"></span> Error when comparing images
 */


/**
 * @apiDefine daoErrors
 *
 * @apiError (Possible error codes) {String} db-cc-db Cannot connect to database [Definition](#error_db-cc-db)
 * @apiError (Possible error codes) {String} db-query-fails Error querying information from database [Definition](#error_db-query-fails)
 */

/**
 * @api {get} /app/registration 1. Register the APP
 * @apiName appRegistration
 * @apiVersion 1.7.4
 * @apiGroup 2_Initialization
 * @apiDescription When a client application is setup and launched for the first time, it needs to get
 * a UUID - which it must store locally. UUID is used to identify the application instance,
 * and it must be included in the header of every other REST request as X-Cardola-Uuid.
 *
 * @apiSuccess {String} uuid Unique User Id
 * @apiSuccess {String} href The href of current request/resource
 *
 * @apiSuccessExample {html} Success-Example:
 * HTTP/1.1 200 OK
 * {
 *    "uuid": "1234-5678-1234-5678",
 *    "href": "http://api.cardola.com/v1/test-hgid/app/registration"
 * }
 *
 */


/**
 * @api {post} /app/registration 1.1 POST Register the APP
 * @apiName appRegistration2
 * @apiVersion 1.7.4
 * @apiGroup 2_Initialization
 * @apiDescription When a client application is setup and launched for the first time, it needs to get
 * a UUID - which it must store locally. UUID is used to identify the application instance,
 * and it must be included in the header of every other REST request as X-Cardola-Uuid.
 *
 * @apiSuccess {String} uuid Unique User Id
 * @apiSuccess {String} href The href of current request/resource
 *
 * @apiParam (POST body)  {String}  manufacturer Manufacturer name ex: Apple
 * @apiParam (POST body)  {String}  model Model name of the device ex: iPhone 3s
 * @apiParam (POST body)  {String}  os Operating system ex: iOS 8.0
 * @apiParam (POST body)  {String}  appVersion Application version ex: 1.2
 * @apiParam (POST body)  {Boolean}  isTablet isTablet ex: true
 * @apiParam (POST body)  {Array}  configuredLanguages Language preferences presented as String in Array
 * @apiParam (POST body)  {String}  carrierName carrierName ex: EMT
 * @apiParam (POST body)  {Object}  screenResolution Resolution of the device, should containt width and height
 * @apiParam (POST body)  {Number}  screenResolution.width Width of the device display ex: 1024
 * @apiParam (POST body)  {Number}  screenResolution.height Height of the device display ex: 2048
 * @apiParam (POST body)  {Number}  screenResolution.scale=1 Scale of the screen
 *
 * @apiUse daoErrors
 * @apiError (Possible error codes) {String} com-fields-miss Missing fields in body ([Definition](#error_com-fields-miss))
 * @apiError (Possible error codes) {String} com-fields-type Some fields did not match expected type ([Definition](#error_com-fields-type))

 * @apiSuccessExample {html} Success-Example:
 * HTTP/1.1 200 OK
 * {
 *    "uuid": "1234-5678-1234-5678",
 *    "href": "http://api.cardola.com/v1/test-hgid/app/registration"
 * }
 *
 */



/**
 * @api {get} /app/init 2. Get initial data
 * @apiName appInit
 * @apiVersion 1.7.4
 * @apiGroup 2_Initialization
 * @apiDescription When client application has the UUID, then every time the application is loaded, it must make a call
 * to this service to get the vital information, which it will need to build the UI and provide content to end-user.
 * @apiUse GlobalHeaders
 *
 * @apiSuccess {String}     href  The href of current request/resource
 * @apiSuccess {Language[]}   supportedLanguages List of Language objects, which are supported by the hotel group
 * ([About languages](#api-1_Basics-basicsLangs))
 * @apiSuccess {Language}     defaultLanguage The default language object ([About languages](#api-1_Basics-basicsLangs))
 * @apiSuccess {Object}     translations Object, which contain translations of the UI phrases.
 * @apiSuccess {String}     gaTrackingId Google Analytics Tracking Id, value is string or null
 * @apiSuccess {Object}     translations.localized Object containing localized translations for every language.
 * @apiSuccess {String[]}     translations.localized.LANGUAGE_CODE An Object containing translations for a language
 * @apiSuccess {String[]}     translations.localized.LANGUAGE_CODE An Object containing translations for a language
 * defined by `LANGUAGE_CODE`
 * @apiSuccess {String}     translations.LANGUAGE_CODE.KEY A translated string with a given `KEY`
 *
 * @apiSuccess {Link}       signIn A Link object, which points to a sing in action
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiSuccess {Link}       signUp A Link object, which points to a sing up in action
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiSuccess {Link}       signOut A Link object, which points to a sing out in action
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiSuccess {Link}       mainMenu A Link object, which points to a service, returning main menu structure
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiSuccess {Object}     hotelGroupInfo A Object containing info about hotel group
 * @apiSuccess {Object}     hotelGroupInfo.localized A Object containing info in certain language
 * @apiSuccess {Object}     hotelGroupInfo.localized.LANGUAGE
 * @apiSuccess {Object}     hotelGroupInfo.localized.LANGUAGE.name Hotel name in certain language
 * @apiSuccess {Image}     hotelGroupInfo.localized.LANGUAGE.logo Hotel logo as Image object ([About images](#api-1_Basics-basicsImages))
 * @apiSuccess {Image}     hotelGroupInfo.localized.LANGUAGE.backgroundImage The background image to use for this hotel group.
 * @apiSuccess {Object}     theme Object containing theme information
 * @apiSuccess {Color}     theme.primaryColor Object containing information about one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.secondaryColor Object containing information about one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.primaryTextColor Object containing information about one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.secondaryTextColor Object containing information about one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.backgroundColor Object containing information about primary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.inputColor Object containing information about secondary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.backgroundTextColor Object containing information about secondary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.inputTextColor Object containing information about secondary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.primaryImageTextColor Object containing information about secondary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.secondaryImageTextColor Object containing information about secondary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.backgroundSubtextColor Object containing information about secondary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.heroTextColor Object containing information about secondary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.heroSubtextColor Object containing information about secondary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.placeholderTextColor Object containing information about secondary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.progressIndicatorColor Object containing information about secondary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.primaryIconColor Object containing information about secondary one theme color (See the color object definition)
 * @apiSuccess {Color}     theme.secondaryIconColor Object containing information about secondary one theme color (See the color object definition)
 *
 * @apiSuccess (Color object) {String}      r Value for Red color (in RGB) from 0-255
 * @apiSuccess (Color object) {Integer}     g Value for Green color (in RGB) from 0-255
 * @apiSuccess (Color object) {Integer}     b Value for Blue color (in RGB) from 0-255
 * @apiSuccess (Color object) {Float}       alpha The alpha/opacity value for this color from 0-1
 * @apiUse daoErrors
 * @apiUse getHotelGroupErrors

 * @apiSuccessExample {json} Success-Response:
 {
  "supportedLanguages": [
    {
      "code": "ar_SA",
      "englishName": "Arabic",
      "nativeName": "العربية",
      "rtl": true,
      "countryEnglishName": "Saudi Arabia"
    },
    {
      "code": "de_DE",
      "englishName": "German",
      "nativeName": "Deutsch",
      "rtl": false,
      "countryEnglishName": "Germany"
    }
  ],
  "gaTrackingId": "UA-70491234-1",
  "defaultLanguage": {
    "code": "en_GB",
    "englishName": "English",
    "nativeName": "English",
    "rtl": false,
    "countryEnglishName": "United Kingdom of Great Britain and Northern Ireland"
  },
  "translations": {
    "localized": {
      "fr_FR": {
        "NoneCell": "None",
        "MapButton": "Map",
        "PhoneText": "Phone",
        "WebButton": "WWW",
        "CallButton": "Call",
        "DoneButton": "Done",
        "EmailField": "Email Address",
        "SearchText": "Search",
        "SkipButton": "Skip",
        "ShowAllCell": "Show all",
        "SignInButton": "Sign in",
        "SignUpButton": "Sign up",
        "SurnameField": "Surname",
        "VerifyButton": "Verify details",
        "NoResultsCell": "No results",
        "PasswordField": "Password",
        "SignOutButton": "Sign out",
        "FirstNameField": "First Name",
        "LanguageButton": "Language",
        "ProcessingText": "Processing",
        "SettingsButton": "Settings",
        "ForgottenButton": "Forgotten password",
        "EmailConfirmField": "Confirm Email Address",
        "ExploreHotelButton": "Explore Hotel",
        "InvalidEmailFormat": "Invalid email address format. Please correct and try again.",
        "InvalidEntryFormat": "Invalid entry. Please correct and try again.",
        "InvalidEmailConfirm": "Your emails do not match. Please try again.",
        "PasswordConfirmField": "Confirm Password",
        "CorrectPasswordFormat": "Passwords must be at least 8 characters long and include both upper and lower-case letters and numbers.",
        "CurrentLocationButton": "Current location",
        "InvalidPasswordFormat": "Weak password. See requirements.",
        "CheckConnectionMessage": "Please check your internet connection.",
        "InvalidPasswordConfirm": "Your passwords do not match. Please try again.",
        "TermsAndConditionsHint": "By submitting your application you agree to be bound by the terms and conditions below.",
        "TermsAndConditionsText": "Please read and review the terms and conditions before using this service.\n\nUser acknowledges and agrees that the Services are for personal use and agrees not to use the Services in a manner prohibited by any federal or state law or regulation.\n\nUser acknowledges that there is content on the Internet or otherwise available through the Services which may be offensive, or which may not be in compliance with all local laws, regulations and other rules. We assume no responsibility for and exercises no control over the content contained on the Internet or is otherwise available through the Services. All content accessed or received by the User is used by User at his or her own risk, and we and our employees shall have no liability resulting from the access or use of such content by the User.",
        "NoAvailableRouteMessage": "There is no route available.",
        "PreferredLanguageHeader": "Preferred language",
        "TermsAndConditionsButton": "Terms and conditions",
        "ThirdPreferredLanguageHeader": "Third choice (optional)",
        "SecondPreferredLanguageHeader": "Second choice (optional)",
        "TermsAndConditionsButtonShort": "T&Cs",
        "LocationServiceDisabledMessage": "Please enable location services.",
        "NoHotelsInNearestRadiusMessage": "There are no hotels in your radius."
      },
      "it_IT": {
        "NoneCell": "None",
        "MapButton": "Map",
        "PhoneText": "Phone",
        "WebButton": "WWW",
        "CallButton": "Call",
        "DoneButton": "Done",
        "EmailField": "Email Address",
        "SearchText": "Search",
        "SkipButton": "Skip",
        "ShowAllCell": "Show all",
        "SignInButton": "Sign in",
        "SignUpButton": "Sign up",
        "SurnameField": "Surname",
        "VerifyButton": "Verify details",
        "NoResultsCell": "No results",
        "PasswordField": "Password",
        "SignOutButton": "Sign out",
        "FirstNameField": "First Name",
        "LanguageButton": "Language",
        "ProcessingText": "Processing",
        "SettingsButton": "Settings",
        "ForgottenButton": "Forgotten password",
        "EmailConfirmField": "Confirm Email Address",
        "ExploreHotelButton": "Explore Hotel",
        "InvalidEmailFormat": "Invalid email address format. Please correct and try again.",
        "InvalidEntryFormat": "Invalid entry. Please correct and try again.",
        "InvalidEmailConfirm": "Your emails do not match. Please try again.",
        "PasswordConfirmField": "Confirm Password",
        "CorrectPasswordFormat": "Passwords must be at least 8 characters long and include both upper and lower-case letters and numbers.",
        "CurrentLocationButton": "Current location",
        "InvalidPasswordFormat": "Weak password. See requirements.",
        "CheckConnectionMessage": "Please check your internet connection.",
        "InvalidPasswordConfirm": "Your passwords do not match. Please try again.",
        "TermsAndConditionsHint": "By submitting your application you agree to be bound by the terms and conditions below.",
        "TermsAndConditionsText": "Please read and review the terms and conditions before using this service.\n\nUser acknowledges and agrees that the Services are for personal use and agrees not to use the Services in a manner prohibited by any federal or state law or regulation.\n\nUser acknowledges that there is content on the Internet or otherwise available through the Services which may be offensive, or which may not be in compliance with all local laws, regulations and other rules. We assume no responsibility for and exercises no control over the content contained on the Internet or is otherwise available through the Services. All content accessed or received by the User is used by User at his or her own risk, and we and our employees shall have no liability resulting from the access or use of such content by the User.",
        "NoAvailableRouteMessage": "There is no route available.",
        "PreferredLanguageHeader": "Preferred language",
        "TermsAndConditionsButton": "Terms and conditions",
        "ThirdPreferredLanguageHeader": "Third choice (optional)",
        "SecondPreferredLanguageHeader": "Second choice (optional)",
        "TermsAndConditionsButtonShort": "T&Cs",
        "LocationServiceDisabledMessage": "Please enable location services.",
        "NoHotelsInNearestRadiusMessage": "There are no hotels in your radius."
      },
      "ru_RU": {
        "NoneCell": "Не выбран",
        "MapButton": "Карта",
        "PhoneText": "Телефон",
        "WebButton": "WWW",
        "CallButton": "Позвонить",
        "DoneButton": "Готово",
        "EmailField": "Адрес электронной почты",
        "SearchText": "Поиск",
        "SkipButton": "Пропустить",
        "ShowAllCell": "Показать все",
        "SignInButton": "Войти в систему",
        "SignUpButton": "Зарегистрироваться",
        "SurnameField": "Фамилия",
        "VerifyButton": "Проверьте детали",
        "NoResultsCell": "Нет результатов",
        "PasswordField": "Пароль",
        "SignOutButton": "Выйти из системы",
        "FirstNameField": "Имя",
        "LanguageButton": "Язык",
        "ProcessingText": "Обработка",
        "SettingsButton": "Настройки",
        "ForgottenButton": "Забыли пароль",
        "EmailConfirmField": "Подтвердить адрес электронной почты",
        "ExploreHotelButton": "Посмотреть Отель",
        "InvalidEmailFormat": "Неправильный формат адреса электронной почты. Пожалуйста, исправьте и попробуйте снова.",
        "InvalidEntryFormat": "Неправильный ввод. Подалуйста, исправьте и попробуйье снова.",
        "InvalidEmailConfirm": "Адреса электронной почты не совпадают. Попробуйте снова.",
        "PasswordConfirmField": "Подтвердить пароль",
        "CorrectPasswordFormat": "Пароль должен быть не короче 8 символов, содержать заглавные и строчные буквы и как минимум одну цифру.",
        "CurrentLocationButton": "Текущее местоположение",
        "InvalidPasswordFormat": "Пароль не соответствует требованиям.",
        "CheckConnectionMessage": "Пожалуйста, проверьте подключение к интернету.",
        "InvalidPasswordConfirm": "Ваши пароли не совпадают. Попробуйте снова.",
        "TermsAndConditionsHint": "При регистрации вы соглашаетесь с положениями и условиями, описанными ниже.",
        "TermsAndConditionsText": "Пожалуйста, прочитайте и рассмотрите условия, прежде чем использовать эту услугу.\n\nПользователь признает и соглашается с тем, что Услуги предназначены для личного использования и обязуется не использовать Услуги в манере, запрещенной любым федеральным или государственным закона или нормативным актом.\n\nПользователь признает, что есть материалы, доступные в Интернете или иным образом с помощью данной услуги, которые могут быть оскорбительными, или не соответствовать всем местным законам, правилам и другим нормам. Мы не несем ответственность и не осуществляем никакого контроля над материалом, содержащимся в Интернете или иным способом доступным с использованием Услуги. Все материалы, просмотренные или полученные Пользователем, используются на его или её собственный страх и риск, и мы и наши сотрудники не несём никакой ответственности за последствия использования такого контента пользователем.",
        "NoAvailableRouteMessage": "Путь не найден.",
        "PreferredLanguageHeader": "Основной язык",
        "TermsAndConditionsButton": "Положения и условия",
        "ThirdPreferredLanguageHeader": "Третий язык (по желанию)",
        "SecondPreferredLanguageHeader": "Второй язык (по желанию)",
        "TermsAndConditionsButtonShort": "Правила",
        "LocationServiceDisabledMessage": "Пожалуйста, включите службы геолокации.",
        "NoHotelsInNearestRadiusMessage": "В непосредственной близости от вас отелей не найдено."
      }
    }
  },
  "hotelGroupInfo": {
    "localized": {
      "ar_SA": {
        "name": "The Leading Hotels of the World",
        "logo": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/v1/heroku-dev/group-app/test-hgid/logo1428324117000",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/logo1428324117000"
        },
        "backgroundImage": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg"
        }
      },
      "en_GB": {
        "name": "The Leading Hotels of the World",
        "logo": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/v1/heroku-dev/group-app/test-hgid/logo1428324117000",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/logo1428324117000"
        },
        "backgroundImage": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg"
        }
      },
      "fr_FR": {
        "name": "The Leading Hotels of the World",
        "logo": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/v1/heroku-dev/group-app/test-hgid/logo1428324117000",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/logo1428324117000"
        },
        "backgroundImage": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg"
        }
      },
      "ru_RU": {
        "name": "The Leading Hotels of the World",
        "logo": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/v1/heroku-dev/group-app/test-hgid/logo1428324117000",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/logo1428324117000"
        },
        "backgroundImage": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg"
        }
      },
      "et_EE": {
        "name": "The Leading Hotels of the World",
        "logo": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/v1/heroku-dev/group-app/test-hgid/logo1428324117000",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/logo1428324117000"
        },
        "backgroundImage": {
          "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg",
          "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/heroku-dev/group-app/test-hgid/bg1428324117000.jpg"
        }
      }
    }
  },
  "theme": {
    "inputColor": {
      "normal": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 0.9
      },
      "disabled": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 0.25
      },
      "selected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlighted": {
          "b": 226,
          "g": 226,
          "r": 226,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      }
    },
    "primaryColor": {
      "normal": {
          "b": 254,
          "g": 128,
          "r": 1,
          "alpha": 1
      },
      "disabled": {
          "b": 0,
          "g": 0,
          "r": 0,
          "alpha": 0.25
      },
      "selected": {
          "b": 255,
          "g": 189,
          "r": 126,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 211,
          "r": 170,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 255,
          "g": 189,
          "r": 126,
          "alpha": 1
      }
    },
    "heroTextColor": {
      "normal": {
          "b": 166,
          "g": 221,
          "r": 253,
          "alpha": 1
      },
      "disabled": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 0.5
      },
      "selected": {
          "b": 183,
          "g": 152,
          "r": 20,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 211,
          "r": 170,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 183,
          "g": 152,
          "r": 20,
          "alpha": 1
      }
    },
    "inputTextColor": {
      "normal": {
          "b": 60,
          "g": 60,
          "r": 60,
          "alpha": 1
      },
      "disabled": {
          "b": 0,
          "g": 0,
          "r": 0,
          "alpha": 0.25
      },
      "selected": {
          "b": 60,
          "g": 60,
          "r": 60,
          "alpha": 1
      },
      "highlighted": {
          "b": 60,
          "g": 60,
          "r": 60,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 60,
          "g": 60,
          "r": 60,
          "alpha": 1
      }
    },
    "secondaryColor": {
      "normal": {
          "b": 183,
          "g": 152,
          "r": 20,
          "alpha": 1
      },
      "disabled": {
          "b": 0,
          "g": 0,
          "r": 0,
          "alpha": 0.25
      },
      "selected": {
          "b": 219,
          "g": 203,
          "r": 139,
          "alpha": 1
      },
      "highlighted": {
          "b": 237,
          "g": 229,
          "r": 196,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 219,
          "g": 203,
          "r": 139,
          "alpha": 1
      }
    },
    "backgroundColor": {
      "normal": {
          "b": 0,
          "g": 0,
          "r": 0,
          "alpha": 0.9
      },
      "disabled": {
          "b": 0,
          "g": 0,
          "r": 0,
          "alpha": 0.25
      },
      "selected": {
          "b": 254,
          "g": 128,
          "r": 1,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 189,
          "r": 126,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 254,
          "g": 128,
          "r": 1,
          "alpha": 1
      }
    },
    "heroSubtextColor": {
      "normal": {
          "b": 227,
          "g": 227,
          "r": 227,
          "alpha": 1
      },
      "disabled": {
          "b": 0,
          "g": 0,
          "r": 0,
          "alpha": 0.25
      },
      "selected": {
          "b": 60,
          "g": 60,
          "r": 60,
          "alpha": 1
      },
      "highlighted": {
          "b": 60,
          "g": 60,
          "r": 60,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 60,
          "g": 60,
          "r": 60,
          "alpha": 1
      }
    },
    "primaryIconColor": {
      "normal": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "disabled": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 0.5
      },
      "selected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 211,
          "r": 170,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      }
    },
    "primaryTextColor": {
      "normal": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "disabled": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 0.5
      },
      "selected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      }
    },
    "secondaryIconColor": {
      "normal": {
          "b": 253,
          "g": 127,
          "r": 1,
          "alpha": 1
      },
      "disabled": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 0.5
      },
      "selected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 211,
          "r": 170,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      }
    },
    "secondaryTextColor": {
      "normal": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "disabled": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 0.5
      },
      "selected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      }
    },
    "backgroundTextColor": {
      "normal": {
          "b": 227,
          "g": 227,
          "r": 227,
          "alpha": 1
      },
      "disabled": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 0.5
      },
      "selected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      }
    },
    "placeholderTextColor": {
      "normal": {
          "b": 254,
          "g": 128,
          "r": 1,
          "alpha": 1
      },
      "disabled": {
          "b": 0,
          "g": 0,
          "r": 0,
          "alpha": 0.25
      },
      "selected": {
          "b": 254,
          "g": 128,
          "r": 1,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 254,
          "g": 128,
          "r": 1,
          "alpha": 1
      }
    },
    "primaryImageTextColor": {
      "normal": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "disabled": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 0.5
      },
      "selected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      }
    },
    "backgroundSubtextColor": {
      "normal": {
          "b": 254,
          "g": 128,
          "r": 1,
          "alpha": 1
      },
      "disabled": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 0.5
      },
      "selected": {
          "b": 255,
          "g": 189,
          "r": 126,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 255,
          "g": 189,
          "r": 126,
          "alpha": 1
      }
    },
    "progressIndicatorColor": {
      "normal": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "disabled": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "selected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      }
    },
    "secondaryImageTextColor": {
      "normal": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "disabled": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 0.5
      },
      "selected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlighted": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      },
      "highlightedSelected": {
          "b": 255,
          "g": 255,
          "r": 255,
          "alpha": 1
      }
    },
  },
  "mainMenu": {
    "href": "https://api-dev.cardola.net/v1/test-hgid/app/main-menu"
  },
  "href": "https://api-dev.cardola.net/v1/test-hgid/app/init"
}
 *
 */

/**
 * @api {get} /app/main-menu 3. Get the main menu
 * @apiName appMainMenu
 * @apiVersion 1.7.4
 * @apiGroup 2_Initialization
 * @apiDescription Get the structure of the main menu
 *
 *
 * @apiExample {html} Example usage:
 * GET /v1/test-hgid/app/main-menu HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 *
 * @apiSuccess {String}     href The href of current request/resource
 * @apiSuccess {Object[]}   items The items of the main menu.
 * @apiSuccess {Object}     items.localized Localized values of this menu item
 * Elements in this object have a key in the format of ISO 639-1 with region designator.
 * @apiSuccess {Object}     items.localized.LANGUAGE_CODE.name The menu item name
 * @apiSuccess {Image}     items.localized.LANGUAGE_CODE.buttonImage The menu item image as Image object
 * ([About images](#api-1_Basics-basicsImages))
 * @apiSuccess {Image}     items.localized.LANGUAGE_CODE.backgroundImage The background image for the target page, given as Image object
 * ([About images](#api-1_Basics-basicsImages))
 * @apiSuccess {Image[]}     items.localized.LANGUAGE_CODE.slideshowImages The slideshow images for slideshow page, given as Array of Image objects
 * ([About images](#api-1_Basics-basicsImages))
 * @apiSuccess {Object}     mainMenu.action The object defining the item action
 * @apiSuccess {String=navigation,slideshow}     mainMenu.action.type The type of action this item does `navigation` means in-app navigation, if this item `slideshow`
 * then it is will be navigate to separate page with slideshow
 * @apiSuccess {String}     mainMenu.action.href The href, which gives data for the target view
 * @apiSuccess {String=categoryGridView,nearMeMapView,resultListView,slideshow}     mainMenu.action.viewType The view type, which should be rendered if user navigates to this menu item
 * @apiSuccess {Object[]}   [items.children] Children menu items **NOT USED CURRENTLY**
 * @apiSuccess {Object}   [customItems] Custom items
 * @apiSuccess {Object}   [customItems.search] Search menu item description (Same structure as object in `items`)
 *

 * @apiUse daoErrors
 * @apiUse getHotelGroupErrors

 * @apiSuccessExample {json} Success-Example:
 {
    "items": [
        {
            "action": {
                "type": "navigation",
                "href": "https://localhost:8000/v1/test-hgid/categories?type=regions",
                "viewType": "categoryGridView"
            },
            "children": [],
            "localized": {
                "ar_SA": {
                    "name": "المناطق",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "en_GB": {
                    "name": "Regions",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "et_EE": {
                    "name": "Piirkonnad",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "fr_FR": {
                    "name": "Régions",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "ru_RU": {
                    "name": "Регионы",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-reg/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                }
            }
        },
        {
            "action": {
                "type": "navigation",
                "href": "https://localhost:8000/v1/test-hgid/categories?type=tags",
                "viewType": "categoryGridView"
            },
            "children": [],
            "localized": {
                "ar_SA": {
                    "name": "تجربة",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "en_GB": {
                    "name": "Experience",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "et_EE": {
                    "name": "Elamus",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "fr_FR": {
                    "name": "Expérience",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "ru_RU": {
                    "name": "Oпыт",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-typ/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                }
            }
        },
        {
            "action": {
                "type": "navigation",
                "href": "https://localhost:8000/v1/test-hgid/categories?type=brands",
                "viewType": "categoryGridView"
            },
            "children": [],
            "localized": {
                "ar_SA": {
                    "name": "العلامات التجارية",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "en_GB": {
                    "name": "Brands",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "et_EE": {
                    "name": "Brändid",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "fr_FR": {
                    "name": "Marques",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "ru_RU": {
                    "name": "Бренды",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-br/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                }
            }
        },
        {
            "action": {
                "type": "navigation",
                "href": "https://localhost:8000/v1/test-hgid/hotels?lat=<latitude>&long=<longitude>&limit=100000",
                "viewType": "nearMeMapView"
            },
            "children": [],
            "localized": {
                "ar_SA": {
                    "name": "بالقرب مني",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "en_GB": {
                    "name": "Near me",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "et_EE": {
                    "name": "Lähedal",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "fr_FR": {
                    "name": "Near me",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "ru_RU": {
                    "name": "Рядом со мной",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-nm/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                }
            }
        },
        {
            "action": {
                "type": "navigation",
                "href": "https://localhost:8000/v1/test-hgid/hotels",
                "viewType": "resultListView"
            },
            "children": [],
            "localized": {
                "ar_SA": {
                    "name": "عرض كل",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "en_GB": {
                    "name": "Show all",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "et_EE": {
                    "name": "Näita kõiki",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "fr_FR": {
                    "name": "Montrer tous",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "ru_RU": {
                    "name": "Показать все",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                }
            }
        },
        {
            "menuItemId": "mm-ss",
            "action": {
                "type": "slideshow",
                "navigation": {
                   "viewType": "slideshow"
                }
            },
            "children": [],
            "localized": {
                "en_GB": {
                   "name": "Slideshow",
                   "slideshowImages": [{
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }],
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image"
                    }
                },
                "de_DE": {
                   "name": "Diashow",
                   "slideshowImages": [{
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }],
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image"
                    }
                },
                "ru_RU": {
                   "name": "Cлайд-шоу",
                   "slideshowImages": [{
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }],
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image"
                    }
                },
                "et_EE": {
                   "name": "Slideshow",
                   "slideshowImages": [{
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }],
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image"
                    }
                },
                "ar_SA": {
                   "name": "عرض الشرائحة",
                   "slideshowImages": [{
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }],
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/main_menu/mm-all/image"
                    }
                }
            }
        }
    ],
    "customItems": {
        "search": {
            "action": {
                "type": "search",
                "href": "https://localhost:8000/v1/test-hgid/hotels?lat=<latitude>&long=<longitude>&search=<search>",
                "viewType": "search"
            },
            "children": [],
            "localized": {
                "en_GB": {
                    "name": "Search",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/placeholder",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/placeholder"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                },
                "et_EE": {
                    "name": "Otsing",
                    "buttonImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/placeholder",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/placeholder"
                    },
                    "backgroundImage": {
                        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/v1/indrek-dev/group-app/test-hgid/bg.jpg",
                        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_fill,fl_force_strip,h_<height>,w_<width>/b_rgb:000,e_grayscale,o_50/fl_png8,q_10/v1/indrek-dev/group-app/test-hgid/bg.jpg"
                    }
                }
            }
        }
    },
    "href": "https://localhost:8000/v1/test-hgid/app/main-menu"
}
 *
 */

/**
 * @api {get} /app/test 4. Test service
 * @apiName appTestrequest
 * @apiVersion 1.7.4
 * @apiGroup 2_Initialization
 * @apiDescription Test service, to test the integration. It will validate the `x-cardola-verify` header if it is set.
 * Also, it will return the body of the request, as it understood it.
 *
 *
 * @apiSuccess {Object}     receivedBody The parsed body of the request
 * @apiSuccess {String}     [signatureVerified=true] If x-cardola-verify header was given and it was valid,
 * it is set to true, if invalid, then error is returned.
 */
