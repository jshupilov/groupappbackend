'use strict';
var databaseUtils = require('../utils/databaseUtils');

/**
 * Find a hotel group by ID
 * @param uuid
 * @param hotelGroupId
 * @param headers
 * @param callback
 */
exports.registerAppInstance = function(uuid, hotelGroupId, headers, baseProfileEmail, callback) {

	databaseUtils.getConnection(function(err, client, done) {
		if(err) {
			return callback(err);
		}
		var data = {
			uuid: uuid,
			headers: headers,
			hotelGroupId: hotelGroupId,
			baseProfileEmail: baseProfileEmail,
			timeStamp: new Date().toISOString()
		};
		client.query('INSERT INTO app_instance(data) VALUES($1)', [data], function(err) {
			done();
			callback(err, data);
		});
	});

};

exports.registerAppInstance2 = function(uuid, hotelGroupId, headers, body, baseProfileEmail, callback) {

    databaseUtils.getConnection(function(err, client, done) {
        if(err) {
            return callback(err);
        }

        var data = {
            uuid: uuid,
            headers: headers,
            hotelGroupId: hotelGroupId,
            info: body,
	          timeStamp: new Date().toISOString(),
						baseProfileEmail: baseProfileEmail
        };

        client.query('INSERT INTO app_instance(data) VALUES($1)', [data], function(err) {
            done();
            callback(err, data);
        });
    });

};

/**
 *
 * @param uuid
 * @param hotelGroupId
 * @param callback
 */
exports.getAppInstanceCount = function(uuid, hotelGroupId, callback) {

	databaseUtils.getConnection(function(err, client, done) {
		if(err) {
			return callback(err);
		}
		client.query(
			'SELECT count(*) as count FROM app_instance ' +
			"WHERE data -> 'uuid' = $1 AND data -> 'hotelGroupId' = $2",
			['"' + uuid + '"', '"' + hotelGroupId + '"'],
			function(err, res) {
				done();
				callback(err, res);
			}
		);
	});

};

/**
 *
 * @param uuid
 * @param hotelGroupId
 * @param callback
 */
exports.getAppInstance = function(uuid, hotelGroupId, callback) {

	databaseUtils.getConnection(function(err, client, done) {
		if(err) {
			return callback(err);
		}
		client.query(
			'SELECT * FROM app_instance ' +
			"WHERE data -> 'uuid' = $1 AND data -> 'hotelGroupId' = $2",
			['"' + uuid + '"', '"' + hotelGroupId + '"'],
			function(err, res) {
				done();
				callback(err, res);
			}
		);
	});

};

/**
 * Update
 * @param data
 * @param callback
 */
exports.updateAppInstance = function(data, callback) {

	databaseUtils.getConnection(function(err, client, done) {
		if(err) {
			return callback(err);
		}
		client.query(
			'UPDATE app_instance  SET data = $1 ' +
			"WHERE data -> 'uuid' = $2 AND data -> 'hotelGroupId' = $3",
			[data, '"' + data.uuid + '"', '"' + data.hotelGroupId + '"'],
			function(err, result) {
				done();
				callback(err, result);
			}
		);
	});

};

exports.getActivations = function(hgId, callback){
	databaseUtils.getConnection(function(err, client, done) {
		if(err) {
			return callback(err);
		}
		client.query(
			'SELECT to_char((data->>\'timeStamp\')::timestamp with time zone, \'YYYY-MM-DD\') as time, count(*) as count ' +
			'FROM app_instance ' +
			"WHERE data->>'hotelGroupId' = $1 " +
			"GROUP BY to_char((data->>\'timeStamp\')::timestamp with time zone, \'YYYY-MM-DD\') " +
			"ORDER BY to_char((data->>\'timeStamp\')::timestamp with time zone, \'YYYY-MM-DD\') ",
			[hgId],
			function(err, result) {
				done();
				callback(err, result);
			}
		);
	});
};
