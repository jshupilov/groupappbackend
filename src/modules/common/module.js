'use strict';
var services = require('./services');
var errors = require('../utils/errors');
var apiUtils = require('../utils/apiUtils');
var cryptoUtils = require('../utils/cryptoUtils');
var logging = require('../utils/logging');
var Boom = require('boom');
var commonModule = {
	register: function(server, options, next) {
		// Add the route
		server.route({
			method: 'GET',
			path: '/app/registration',
			handler: function(request, reply) {
				services.registration(request.apiRequest, function(err, res) {
					apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res), 'noCache');
				});
			}
		});

		server.route({
			method: 'POST',
			path: '/app/registration',
			handler: function (request, reply) {
				services.registration2(request.apiRequest, function (err, res) {
					return reply(errors.wrapToBoom(err), res);
				});
			}
		});


		// Add the route
		server.route({
			method: 'GET',
			path: '/app/init',
			handler: function(request, reply) {
				services.init(request.apiRequest, function(err, res) {
					apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res));
				});
			}
		});

		// Add the route
		server.route({
			method: 'GET',
			path: '/app/main-menu',
			handler: function(request, reply) {
				services.mainMenu(request.apiRequest, function(err, res) {
					apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res));
				});
			}
		});

		// Add the route
		server.route({
			method: 'POST',
			path: '/app/test',
			handler: function(request, reply) {
				if(request.apiRequest.headers['x-cardola-verify']){
					apiUtils.ensureSecurity(3, request.apiRequest, function(err){
						reply(errors.wrapToBoom(err), {signatureVerifies: true, receivedBody: request.apiRequest.body});
					});
				}else{
					reply(null, {receivedBody: request.apiRequest.body});
				}
			}
		});

		server.route({
			method: 'GET',
			path: '/app/status',
			handler: function(request, reply) {
				reply(null, {status: 'I am okay'});
			}
		});

		server.route({
			method: 'GET',
			path: '/app/info',
			handler: function(request, reply) {

				var md5 = cryptoUtils.md5(request.apiRequest.hotelGroupId + 'B117649793F4F2C38AD79CF001753AC74412EA5659D2F203FBD419D98E5AA3FD');
				if( request.apiRequest.query.hash !== md5 ){
					logging.info('Requested hash: ' + md5 + ' got: ' + request.apiRequest.query.hash);
					return reply(Boom.forbidden('Access forbidden'));
				}

				if(request.apiRequest.headers.hasOwnProperty('content-type')
					&& request.apiRequest.headers['content-type'].indexOf('application/json') > -1
				){
					services.info(request.apiRequest, function(err, res){
						apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res), 'noCache');
					});

				}else{
					reply.view('common/info', {});
				}
			}
		});

		next();
	}
};

commonModule.register.attributes = {
	name: 'Common',
	version: '1.0.0'
};

exports.register = commonModule.register;
exports.services = services;
