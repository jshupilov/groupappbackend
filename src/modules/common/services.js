'use strict';
var apiUtils = require('../utils/apiUtils');
var profiles = require('../profiles/services');
var resourceUtils = require('../utils/resourceUtils');
var hgServices = require('../hotel_groups/services');
var uuid = require('node-uuid');
var dao = require('./dao');
var errors = require('../utils/errors');

errors.defineError('com-fields-miss', 'System failure', 'Missing fields in body', 'Common', 400);
errors.defineError('com-fields-type', 'System failure', 'Some fields did not match expected type', 'Common', 400);

var createUuid = function() {
	return uuid.v1();
};

/**
 * Register the app instance
 * @param apiRequest
 * @param callback
 */
exports.registration = function(apiRequest, callback) {
	apiRequest.uuid = createUuid();
	profiles.createBaseAccount(apiRequest, function(err, account){
		if(err) {
			return callback(err);
		}
		dao.registerAppInstance(apiRequest.uuid, apiRequest.hotelGroupId, apiRequest.headers, account.email, function(err, res) {
			if(err) {
				return callback(err);
			}
			callback(null, resourceUtils.formatResource({uuid: res.uuid}, apiRequest));
		});
	});

};

/**
 * [POST]
 * @param apiRequest
 * @param callback
 */
exports.registration2 = function (apiRequest, callback) {

	var fields = ['manufacturer', 'model', 'os', 'appVersion', 'isTablet', 'carrierName', 'screenResolution', 'configuredLanguages'];
	var fNum = ['width', 'height', 'scale'];
	var fStr = ['manufacturer', 'model', 'os', 'appVersion', 'carrierName'];
	try {
		fields.forEach(function (v) {
			if (!apiRequest.body.hasOwnProperty(v)) {
				throw errors.newError('com-fields-miss', {missingField: v});
			}

		});
		fStr.forEach(function (v) {
			if (typeof (apiRequest.body[v]) !== 'string') {
				throw  errors.newError('com-fields-type', {invalid: v});
			}
		});
		if (typeof apiRequest.body.screenResolution !== 'object') {
			throw  errors.newError('com-fields-type', {invalid: 'screenResolution'});
		}
		if (!Array.isArray(apiRequest.body.configuredLanguages)) {
			throw  errors.newError('com-fields-type', {invalid: 'configuredLanguages'});
		}
		fNum.forEach(function (v) {
			if (apiRequest.body.screenResolution.hasOwnProperty(v)) {
				if (isNaN(apiRequest.body.screenResolution[v])) {
					throw  errors.newError('com-fields-type', {invalid: 'screenResolution.' + v});
				}
			} else {
				throw errors.newError('com-fields-miss', {missingField: 'screenResolution.' + v});
			}
		});

		apiRequest.body.configuredLanguages.forEach(function (v, i) {
			if (typeof (v) !== 'string') {
				throw  errors.newError('com-fields-type', {invalid: 'configuredLanguages[' + i +']'});
			}
		});
	} catch (err) {
		return callback(err);
	}
	apiRequest.uuid = createUuid();
	profiles.createBaseAccount(apiRequest, function(err, account){
		if(err) {
			return callback(err);
		}
		dao.registerAppInstance2(apiRequest.uuid, apiRequest.hotelGroupId, apiRequest.headers, apiRequest.body, account.email, function (err, res) {
			if (err) {
				return callback(err);
			}
			callback(null, resourceUtils.formatResource({uuid: res.uuid}, apiRequest));
		});
	});
};

/**
 * Get data for initialization
 * @param apiRequest
 * @param callback
 */
exports.init = function(apiRequest, callback) {
	hgServices.getInitialData(apiRequest.hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}

		res.mainMenu = apiUtils.getServiceLinkWithRequest(apiRequest, '/app/main-menu');

		callback(null, resourceUtils.formatResource(res, apiRequest));
	});
};

/**
 * Get main menu
 * @param apiRequest
 * @param callback
 */
exports.mainMenu = function(apiRequest, callback) {
	hgServices.getMainMenu(apiRequest.hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}

		callback(null, resourceUtils.formatResource(res, apiRequest));
	});

};

/**
 * Validate uuid - checks if uuid exist for this hotel group
 * @param uuid
 * @param hotelGroupId
 * @param callback
 */
exports.validateUuid = function(uuid, hotelGroupId, callback) {
	dao.getAppInstanceCount(uuid, hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}
		return callback(null, res.rows.length + parseInt(res.rows[0].count) > 1);
	});
};

/**
 * Get valid Api keys
 * @param hotelGroupId
 * @param callback
 */
exports.getValidApiKeys = function(hotelGroupId, callback) {
	hgServices.getHotelGroup(hotelGroupId, function(err, hotelGroupRow) {
		if(err) {
			return callback(err);
		}
		var apiKeys = hotelGroupRow.data.validApiKeys ? hotelGroupRow.data.validApiKeys : [];
		callback(null, apiKeys);
	});
};

/**
 * Validate api key
 * @param apiKey
 * @param hotelGroupId
 * @param callback
 */
exports.validateApiKey = function(apiKey, hotelGroupId, callback) {
	exports.getValidApiKeys(hotelGroupId, function(err, apiKeys) {
		if(err) {
			return callback(err);
		}
		callback(null, apiKeys.indexOf(apiKey) > -1);
	});
};

/**
 * Get app information and stats
 * @param apiRequest
 * @param callback
 */
exports.info = function(apiRequest, callback){
	dao.getActivations(apiRequest.hotelGroupId, function(err, res){
		if(err) {
			return callback(err);
		}
		var total = 0;
		res.rows.forEach(function(row){
			row.count = parseInt( row.count );
			total += row.count;
		});
		callback(null, {stats: { activations: {total: total, days: res.rows}}});
	});
};
