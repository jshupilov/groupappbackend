'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var dao = require('../dao');
//var databaseUtils = require('../../utils/databaseUtils');

lab.experiment('Common DAO Interactions', function () {
	var _uuid, _hgId, _email;

	lab.before(function (done) {
		_hgId = testingTools.getHotelGroupId();
		_uuid = testingTools.generateUuid();
		_email = _uuid + '@cardola.net';
		done();
	});
	lab.after(function (done) {
		done();
	});

	lab.test('registerAppInstance should work', function (done) {
		dao.registerAppInstance(_uuid, _hgId, {}, _email, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['uuid', 'headers', 'hotelGroupId', 'baseProfileEmail', 'timeStamp']);
			done();
		});
	});
	lab.test('registerAppInstance2 should work', function (done) {
		_uuid = testingTools.generateUuid();
		dao.registerAppInstance2(_uuid, _hgId, {}, {}, _email, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['uuid', 'headers', 'hotelGroupId', 'info', 'baseProfileEmail', 'timeStamp']);
			done();
		});
	});
	lab.test('getAppInstanceCount should work', function (done) {
		dao.getAppInstanceCount(_uuid, _hgId, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include('rows');
			testingTools.code.expect(res.rows.length).to.equal(1);
			testingTools.code.expect(res.rows[0]).to.include('count');
			done();
		});
	});
	lab.test('getAppInstance should work', function (done) {
		dao.getAppInstance(_uuid, _hgId, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.expectQueryResultsWithFields(res, ['id', 'data']);
			testingTools.code.expect(res.rows.length).to.equal(1);
			done();
		});
	});
	lab.test('updateAppInstance should work', function (done) {
		dao.updateAppInstance({	uuid: _uuid, hotelGroupId: _hgId }, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include('rowCount');
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});
});

lab.experiment('Common DAO if DB structure invalid', function () {
	var _uuid, _hgId, _email;



	lab.before({timeout: 5000}, function (done) {
		_hgId = testingTools.getHotelGroupId();
		testingTools.getUuid(function(err, uuid){
			testingTools.code.expect(err).to.be.null();
			_email = uuid + '@cardola.net';
			_uuid = uuid;
			done();
		});
	});

	testingTools.failDbQueryForExperiment(lab, 'app_instance');

	lab.after(function (done) {
		done();
	});

	lab.test('registerAppInstance should give error', function (done) {
		dao.registerAppInstance(_uuid, _hgId, {}, _email, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('registerAppInstance2 should give error', function (done) {
		dao.registerAppInstance2(_uuid, _hgId, {}, {}, _email, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('getAppInstanceCount should give error', function (done) {
		dao.getAppInstanceCount(_uuid, _hgId, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('getAppInstance should give error', function (done) {
		dao.getAppInstance(_uuid, _hgId, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('updateAppInstance should give error', function (done) {
		dao.updateAppInstance({	uuid: _uuid, hotelGroupId: _hgId }, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});

lab.experiment('Common DAO if DB connection', function () {
	var _uuid, _hgId, _email;

	lab.before(function (done) {
		_hgId = testingTools.getHotelGroupId();
		testingTools.getUuid(function(err, uuid){
			testingTools.code.expect(err).to.be.null();
			_email = uuid + '@cardola.net';
			_uuid = uuid;
			done();
		});
	});

	testingTools.killDbConnectionForExperiment(lab);

	lab.after(function (done) {
		done();
	});

	lab.test('registerAppInstance should give error', function (done) {
		dao.registerAppInstance(_uuid, _hgId, {}, _email, function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('registerAppInstance2 should give error', function (done) {
		dao.registerAppInstance2(_uuid, _hgId, {}, {}, _email, function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('getAppInstanceCount should give error', function (done) {
		dao.getAppInstanceCount(_uuid, _hgId, function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('getAppInstance should give error', function (done) {
		dao.getAppInstance(_uuid, _hgId, function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('updateAppInstance should give error', function (done) {
		dao.updateAppInstance({	uuid: _uuid, hotelGroupId: _hgId }, function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});
