'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var module = require('../module');
var apiUtils = require('../../utils/apiUtils');
var profiles = require('../../profiles/services');
var dao = require('../dao');
var hgServices = require('../../hotel_groups/services');

lab.experiment('common.module', function() {

	var _createBaseAccount,
		_registerAppInstance,
		_uuid,
		_getInitialData,
		_getMainMenu;
	var fakeServer = {
		routes: [],
		route: function(def){
			this.routes.push(def);
		}
	};

	lab.before(function (done) {
		_createBaseAccount = profiles.createBaseAccount;
		_registerAppInstance = dao.registerAppInstance;
		_getInitialData = hgServices.getInitialData;
		_getMainMenu = hgServices.getMainMenu;

		testingTools.getUuid(function(err, uuid){
			testingTools.code.expect(err).to.be.null();
			_uuid = uuid;
			done();
		});
	});
	lab.after(function (done) {
		profiles.createBaseAccount = _createBaseAccount;
		dao.registerAppInstance = _registerAppInstance;
		hgServices.getInitialData = _getInitialData;
		hgServices.getMainMenu = _getMainMenu;

		done();
	});

	lab.test('register should work', function(done) {

		module.register(fakeServer, {}, function(){
			testingTools.code.expect(fakeServer.routes).to.have.length(7);

			testingTools.code.expect(fakeServer.routes[0]).to.be.object();
			testingTools.code.expect(fakeServer.routes[0].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[0].path).to.equal('/app/registration');

			testingTools.code.expect(fakeServer.routes[1]).to.be.object();
			testingTools.code.expect(fakeServer.routes[1].method).to.equal('POST');
			testingTools.code.expect(fakeServer.routes[1].path).to.equal('/app/registration');

			testingTools.code.expect(fakeServer.routes[2]).to.be.object();
			testingTools.code.expect(fakeServer.routes[2].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[2].path).to.equal('/app/init');

			testingTools.code.expect(fakeServer.routes[3]).to.be.object();
			testingTools.code.expect(fakeServer.routes[3].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[3].path).to.equal('/app/main-menu');

			testingTools.code.expect(fakeServer.routes[4]).to.be.object();
			testingTools.code.expect(fakeServer.routes[4].method).to.equal('POST');
			testingTools.code.expect(fakeServer.routes[4].path).to.equal('/app/test');

			testingTools.code.expect(fakeServer.routes[5]).to.be.object();
			testingTools.code.expect(fakeServer.routes[5].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[5].path).to.equal('/app/status');

			testingTools.code.expect(fakeServer.routes[6]).to.be.object();
			testingTools.code.expect(fakeServer.routes[6].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[6].path).to.equal('/app/info');
			done();
		});
	});

	lab.test('GET /app/registration should work', function(done) {
		var req = {apiRequest: apiUtils.createApiRequest('some/path', {}, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		profiles.createBaseAccount = function(r, cb){
			cb(null, {
				email: _uuid + '@cardola.net'
			});
		};
		dao.registerAppInstance = function(uuid, hgId, headers, email, cb){
			cb(null, {
				uuid: _uuid
			});
		};
		fakeServer.routes[0].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['href', 'uuid']);
			return testingTools.expectCachingHeaders(done, 'noCache', res);
		});
	});

	lab.test('POST /app/registration should work', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('some/path', {}, {}, 'GET', {
			'manufacturer': 'Apple',
			'model': 'iPhone 3s',
			'os': 'iOS 8.0',
			'appVersion': '13',
			'isTablet': false,
			'configuredLanguages': ['test', 'test'],
			'carrierName': 'EMT',
			'screenResolution': {
				'width': 1024,
				'height': 2048,
				'scale': 2
			}
		})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[1].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['uuid', 'href']);
			done();
		});
	});

	lab.test('GET /app/init should work', function(done) {
		var req = {apiRequest: apiUtils.createApiRequest('some/path', {}, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		hgServices.getInitialData = function(hgId, cb){
			cb(null, {});
		};
		fakeServer.routes[2].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['href', 'mainMenu']);
			return testingTools.expectCachingHeaders(done, 'default', res);
		});
	});

	lab.test('GET /app/main-menu should work', function(done) {
		var req = {apiRequest: apiUtils.createApiRequest('some/path', {}, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		hgServices.getMainMenu = function(hgId, cb){
			cb(null, {
				mainMenuKey: 'mainMenuValue'
			});
		};
		fakeServer.routes[3].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['href', 'mainMenuKey']);
			testingTools.code.expect(res.mainMenuKey).to.equal('mainMenuValue');
			return testingTools.expectCachingHeaders(done, 'default', res);
		});
	});

	lab.test('POST /app/test should work', function(done) {
		var req = {apiRequest: apiUtils.createApiRequest('some/path', {}, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[4].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include('receivedBody');
			done();
		});
	});
	lab.test('POST /app/test should given error', function(done) {
		var req = {apiRequest: apiUtils.createApiRequest('some/path', {'x-cardola-verify': 'test-verify-key'}, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[4].handler(req, function(err, res){
			testingTools.expectError(err, 'au-https-req');
			testingTools.code.expect(res).to.include(['receivedBody', 'signatureVerifies']);
			done();
		});
	});

	lab.test('GET /app/status should work', function(done) {
		var req = {apiRequest: apiUtils.createApiRequest('app/status', {}, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[5].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include('status');
			done();
		});
	});

	lab.test('GET /app/info should work and respond application/json', function(done) {
		var req = {apiRequest: apiUtils.createApiRequest('app/info?hash=91e6dfb059bfaf14a4b6f0a7bc320c67', {'content-type': 'application/json'}, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[6].handler(req, function (err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include('stats');
			return testingTools.expectCachingHeaders(done, 'noCache', res);
		});
	});

	lab.test('GET /app/info should work', function(done) {
		var req = {apiRequest: apiUtils.createApiRequest('app/info?hash=91e6dfb059bfaf14a4b6f0a7bc320c67', {}, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[6].handler(req, {view: function(tpl){
			testingTools.code.expect(tpl).to.include('common/info');
			done();
		}});
	});

});
