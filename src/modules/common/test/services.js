'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var dao = require('../dao');
var apiUtils = require('../../utils/apiUtils');
var langUtils = require('../../utils/langUtils');
var validationUtils = require('../../utils/validationUtils');
var profiles = require('../../profiles/services');
var hgServices = require('../../hotel_groups/services');
var q = require('q');

lab.experiment('Common services registration', function () {
	var _createBaseAccount, _registerAppInstance;

	lab.before(function (done) {
		_createBaseAccount = profiles.createBaseAccount;
		_registerAppInstance = dao.registerAppInstance;
		done();
	});
	lab.after(function (done) {
		profiles.createBaseAccount = _createBaseAccount;
		dao.registerAppInstance = _registerAppInstance;
		done();
	});

	lab.test('should given error with invalid profiles.createBaseAccount response', function(done){
		var req = apiUtils.createApiRequest('some/path', {}, {hotelId: 'stpeterasdasd'}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		profiles.createBaseAccount = function(r, cb){
			cb(new Error('Error registration createBaseAccount'));
		};
		services.registration(req, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error registration createBaseAccount');
			done();
		});
	});
	lab.test('should given error with invalid dao.registerAppInstance response', function(done){
		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		profiles.createBaseAccount = function(r, cb){
			cb(null, {
				email: 'test@cardola.net'
			});
		};
		dao.registerAppInstance = function(uuid, hgId, headers, email, cb){
			cb(new Error('Error registration registerAppInstance'));
		};
		services.registration(req, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error registration registerAppInstance');
			done();
		});
	});
	lab.test('should work', function(done){
		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		profiles.createBaseAccount = function(r, cb){
			cb(null, {
				email: 'test@cardola.net'
			});
		};
		dao.registerAppInstance = function(uuid, hgId, headers, email, cb){
			cb(null, {
				uuid: 'some-uuid'
			});
		};
		services.registration(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['href', 'uuid']);
			done();
		});
	});
});
lab.experiment('Common services registration2', function () {
	var _registerAppInstance2, body, _createBaseAccount;
	lab.before(function (done) {
		_registerAppInstance2 = dao.registerAppInstance2;
		_createBaseAccount = profiles.createBaseAccount;
		body = {
			'manufacturer': 'Apple',
			'model': 'iPhone 3s',
			'os': 'iOS 8.0',
			'appVersion': 13,
			'isTablet': false,
			'configuredLanguages': ['test', 'test'],
			'carrierName': 'EMT',
			'screenResolution': {
				'width': 1024,
				'height': 2048,
				'scale': 2
			}
		};
		done();
	});
	lab.afterEach(function (done) {
		dao.registerAppInstance2 = _registerAppInstance2;
		profiles.createBaseAccount = _createBaseAccount;
		body = {
			'manufacturer': 'Apple',
			'model': 'iPhone 3s',
			'os': 'iOS 8.0',
			'appVersion': '13',
			'isTablet': false,
			'configuredLanguages': ['test', 'test'],
			'carrierName': 'EMT',
			'screenResolution': {
				'width': 1024,
				'height': 2048,
				'scale': 2
			}
		};
		done();
	});
	lab.test('should fail (com-fields-miss appVersion)', function (done) {

		delete body.appVersion;

		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');


		services.registration2(req, function (err) {
			testingTools.expectError(err, 'com-fields-miss');
			done();
		});

	});

	lab.test('should fail (com-fields-type appVersion is not string)', function (done) {

		body.appVersion = 13;

		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.registration2(req, function (err) {
			testingTools.expectError(err, 'com-fields-type');
			done();
		});

	});

	lab.test('should fail (com-fields-type configuredLanguages is invalid)', function (done) {

		body.configuredLanguages = null;

		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.registration2(req, function (err) {
			testingTools.expectError(err, 'com-fields-type');
			testingTools.code.expect(err.data.debuggingData.invalid).to.equal('configuredLanguages');
			done();
		});

	});

	lab.test('should fail (com-fields-type configuredLanguages contains not string)', function (done) {

		body.configuredLanguages[0] = 25;

		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.registration2(req, function (err) {
			testingTools.expectError(err, 'com-fields-type');
			testingTools.code.expect(err.data.debuggingData.invalid).to.equal('configuredLanguages[0]');
			done();
		});

	});

	lab.test('should fail (com-fields-miss screenResolution missing height)', function (done) {

		delete body.screenResolution.height;

		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.registration2(req, function (err) {
			testingTools.expectError(err, 'com-fields-miss');
			done();
		});

	});

	lab.test('should fail (com-fields-type screenResolution is not object)', function (done) {

		body.screenResolution = 'screenResolution';

		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.registration2(req, function (err) {
			testingTools.expectError(err, 'com-fields-type');
			done();
		});

	});

	lab.test('should fail (com-fields-type screenResolution width is not number)', function (done) {

		body.screenResolution.width = 'ddd';

		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.registration2(req, function (err) {
			testingTools.expectError(err, 'com-fields-type');
			done();
		});

	});

	lab.test('should fail (com-fields-type configuredLanguages is not array)', function (done) {

		body.configuredLanguages = 'configuredLanguages';

		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.registration2(req, function (err) {
			testingTools.expectError(err, 'com-fields-type');
			done();
		});

	});

	lab.test('should fail (registerAppInstance2)', {timeout: 5000}, function (done) {

		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		dao.registerAppInstance2 = function () {
			arguments[arguments.length - 1](new Error('Error registration2 registerAppInstance2'));
		};

		services.registration2(req, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error registration2 registerAppInstance2');
			done();
		});
	});

	lab.test('should given error with invalid createBaseAccount response', {timeout: 5000}, function (done) {
		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');
		profiles.createBaseAccount = function(apiReq, cb){
			cb(new Error('Error registration2 createBaseAccount'));
		};
		services.registration2(req, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error registration2 createBaseAccount');
			done();
		});

	});
	lab.test('should return true', {timeout: 5000}, function (done) {
		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.registration2(req, function (err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.include(['uuid', 'href']);
			done();
		});

	});
});

lab.experiment('Common services init', function () {
	var _getInitialData;

	lab.before(function (done) {
		_getInitialData = hgServices.getInitialData;
		done();
	});
	lab.afterEach(function (done) {
		hgServices.getInitialData = _getInitialData;
		done();
	});

	lab.test('should given error with invalid hgServices.getInitialData response', function (done) {
		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		hgServices.getInitialData = function(hgId, cb){
			cb(new Error('Error init'));
		};
		services.init(req, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error init');
			done();
		});
	});

	lab.test('should work', function (done) {
		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		hgServices.getInitialData = function(hgId, cb){
			cb(null, {});
		};
		services.init(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['href', 'mainMenu']);
			done();
		});
	});

	lab.test('should work', function (done) {
		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.init(req, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.include(['href', 'mainMenu', 'translations']);
			testingTools.code.expect(res.translations).to.include('localized');
			testingTools.code.expect(res.translations.localized).to.be.object();
			var promises = [];


			function doVal(item, p){
				validationUtils.validateHasKeys(langUtils.getDefaultTranslationKeys(), item, function(err){
					testingTools.code.expect(err).to.be.null();
					p.resolve();
				});
			}

			for(var lk in res.translations.localized){
				testingTools.code.expect(langUtils.isValidLanguageCode(lk)).to.be.true();
				var p = q.defer();
				promises.push(p.promise);
				doVal(res.translations.localized[lk], p);
			}

			q.all(promises).done(function(){
				done();
			});
		});
	});

});

lab.experiment('Common services mainMenu', function () {
	var _getMainMenu;

	lab.before(function (done) {
		_getMainMenu = hgServices.getMainMenu;
		done();
	});
	lab.after(function (done) {
		hgServices.getMainMenu = _getMainMenu;
		done();
	});

	lab.test('should given error with invalid hgServices.getInitialData response', function (done) {
		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		hgServices.getMainMenu = function(hgId, cb){
			cb(new Error('Error mainMenu'));
		};
		services.mainMenu(req, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error mainMenu');
			done();
		});
	});

	lab.test('should work', function (done) {
		var req = apiUtils.createApiRequest('some/path', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		hgServices.getMainMenu = function(hgId, cb){
			cb(null, {
				mainMenuKey: 'mainMenuValue'
			});
		};
		services.mainMenu(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['href', 'mainMenuKey']);
			testingTools.code.expect(res.mainMenuKey).to.equal('mainMenuValue');
			done();
		});
	});

});

lab.experiment('Common services validateUuid', function () {
	var _getAppInstanceCount;

	lab.before(function (done) {
		_getAppInstanceCount = dao.getAppInstanceCount;
		done();
	});
	lab.after(function (done) {
		dao.getAppInstanceCount = _getAppInstanceCount;
		done();
	});

	lab.test('should given error with invalid hgServices.getInitialData response', function (done) {
		dao.getAppInstanceCount = function(uuid, hgId, cb){
			cb(new Error('Error validateUuid'));
		};
		services.validateUuid('some-uuid', testingTools.getHotelGroupId(), function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error validateUuid');
			done();
		});
	});

	lab.test('should work and return false', function (done) {
		dao.getAppInstanceCount = function(uuid, hgId, cb){
			cb(null, {
				rows: [{
					count: 0
				}]
			});
		};
		services.validateUuid('some-uuid', testingTools.getHotelGroupId(), function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.false();
			done();
		});
	});

	lab.test('should work and return true', function (done) {
		dao.getAppInstanceCount = function(uuid, gId, cb){
			cb(null, {
				rows: [{
					count: 1
				}]
			});
		};
		services.validateUuid('some-uuid', testingTools.getHotelGroupId(), function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.true();
			done();
		});
	});

});

lab.experiment('Common services getValidApiKeys', function () {
	var _getHotelGroup;

	lab.before(function (done) {
		_getHotelGroup = hgServices.getHotelGroup;
		done();
	});
	lab.after(function (done) {
		hgServices.getHotelGroup = _getHotelGroup;
		done();
	});

	lab.test('should given error with invalid hgServices.getInitialData response', function (done) {
		hgServices.getHotelGroup = function(hgId, cb){
			cb(new Error('Error getValidApiKeys'));
		};
		services.getValidApiKeys(testingTools.getHotelGroupId(), function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error getValidApiKeys');
			done();
		});
	});

	lab.test('should work and return array of validApiKeys', function (done) {
		hgServices.getHotelGroup = function(hgId, cb){
			cb(null, {
				data: {
					validApiKeys: ['valid-key']
				}
			});
		};
		services.getValidApiKeys(testingTools.getHotelGroupId(), function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array();
			testingTools.code.expect(res[0]).to.equal('valid-key');

			done();
		});
	});

	lab.test('should work and return empty array of validApiKeys', function (done) {
		hgServices.getHotelGroup = function(hgId, cb){
			cb(null, {
				data: {	}
			});
		};
		services.getValidApiKeys(testingTools.getHotelGroupId(), function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array();
			testingTools.code.expect(res.length).to.equal(0);
			done();
		});
	});

});

lab.experiment('Common services validateApiKey', function () {
	var _getValidApiKeys;

	lab.before(function (done) {
		_getValidApiKeys = services.getValidApiKeys;
		done();
	});
	lab.after(function (done) {
		services.getValidApiKeys = _getValidApiKeys;
		done();
	});

	lab.test('should given error with invalid hgServices.getInitialData response', function (done) {
		services.getValidApiKeys = function(hgId, cb){
			cb(new Error('Error validateApiKey'));
		};
		services.validateApiKey('some-apiKey', testingTools.getHotelGroupId(), function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error validateApiKey');
			done();
		});
	});

	lab.test('should work and return true', function (done) {
		services.getValidApiKeys = function(hgId, cb){
			cb(null, ['valid-key']);
		};
		services.validateApiKey('valid-key', testingTools.getHotelGroupId(), function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.true();
			done();
		});
	});

	lab.test('should work and return false', function (done) {
		services.getValidApiKeys = function(hgId, cb){
			cb(null, ['valid-key']);
		};
		services.validateApiKey('some-apiKey', testingTools.getHotelGroupId(), function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.false();
			done();
		});
	});

});
