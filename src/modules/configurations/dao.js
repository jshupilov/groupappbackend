'use strict';
var databaseUtils = require('../utils/databaseUtils');

/**
 * Get configuration value
 * @param hotelGroupId
 * @param callback
 */
exports.get = function (key, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query('SELECT * ' +
					 'FROM configuration ' +
					 'WHERE (data -> \'key\') = $1 ' +
					 'ORDER BY id DESC ' +
			         'LIMIT 1',
			['"' + key + '"'],
			function (err1, result) {
				done();
				if(err1){
					return callback(err1);
				}
				var configuration = null;
				if(result.rows.length > 0){
					configuration = result.rows[0].data;
				}
				return callback(err1, configuration);
		});
	});

};

/**
 * Get configuration value
 * @param hotelGroupId
 * @param callback
 */
exports.set = function (key, value, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query(
			'UPDATE configuration SET data = $1 ' +
			'WHERE data -> \'key\' = $2',
			[{key: key, value: value}, '"' + key + '"'],
			function(err1, result) {
				done();
				return callback(err1, result);
			}
		);
	});

};



/**
 * Add configuration to database
 * @param data
 * @param callback
 */
exports.add = function (key, value, callback) {
	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query('INSERT INTO configuration(data) VALUES($1)',
			[{key: key, value: value}],
			function (err1, result) {
			done();
			return callback(err1, result);
		});
	});

};
