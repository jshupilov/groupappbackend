'use strict';
var dao = require('./dao');

/**
 * Set configuration value
 * @param key
 * @param value
 * @param callback
 */
exports.set = function(key, value, callback){
	exports.get(key, function(err, data){
		if(err){
			return callback(err);
		}
		if(data){
			dao.set(key, value, function(err1){
				return callback(err1);
			});
		} else {
			dao.add(key, value, function(err1){
				return callback(err1);
			});
		}
	});
};

/**
 * Get configuration value
 * @param key
 * @param callback
 */
exports.get = function(key, callback){
	dao.get(key, function(err, data){
		return callback(err, data);
	});
};
