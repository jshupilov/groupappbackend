'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var dao = require('../dao');
var databaseUtils = require('../../utils/databaseUtils');

lab.experiment('Configurations DAO Interactions', function () {
	var key, value;
	lab.before(function(done){
		key = 'test-key';
		value = 'test-value';
		done();
	});
	lab.afterEach(function (done) {
		databaseUtils.getConnection(function (err, client, done1) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			client.query("DELETE FROM configuration where data -> 'key' = $1",
				['"' + key + '"'],
				function (err1) {
					testingTools.code.expect(err1, JSON.stringify(err1)).to.be.null();
					done1();
					done();
			});
		});
	});
	lab.test('add should work', function (done) {
		dao.add(key, value, function (err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});

	lab.test('get should work', function (done) {
		dao.add(key, value, function (err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			dao.get(key, function (err, res) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(res).to.include(['key', 'value']);
				done();
			});
		});
	});
	lab.test('get should work and return null', function (done) {
		dao.get(key, function (err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.null();
			done();
		});
	});

	lab.test('set should work', function (done) {
		dao.add(key, value, function (err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			dao.set(key, 'new-test-value', function (err, res) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(res.rowCount).to.equal(1);
				done();
			});
		});
	});
});

lab.experiment('Configurations DAO if DB structure invalid', function () {
	testingTools.failDbQueryForExperiment(lab, 'configuration');
	var key, value;
	lab.before(function(done){
		key = 'test-key';
		value = 'test-value';
		done();
	});
	lab.test('add should give error', function (done) {
		dao.add(key, value, function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('get should give error', function (done) {
		dao.get(key, function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});

	lab.test('set should give error', function (done) {
		dao.set(key, 'new-test-value', function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});

lab.experiment('Configurations DAO if DB connection is down', function () {
	testingTools.killDbConnectionForExperiment(lab);
	var key, value;
	lab.before(function(done){
		key = 'test-key';
		value = 'test-value';
		done();
	});
	lab.test('add should give error', function (done) {
		dao.add(key, value, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});

	lab.test('get should give error', function (done) {
		dao.get(key, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});

	lab.test('set should give error', function (done) {
		dao.set(key, 'new-test-value', function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});
