'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var databaseUtils = require('../../utils/databaseUtils');

lab.experiment('Configurations Interactions', function () {
	var key, value;
	lab.before(function(done){
		key = 'test-key';
		value = 'test-value';
		done();
	});
	lab.afterEach(function(done){
		databaseUtils.getConnection(function (err, client, done1) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			client.query("DELETE FROM configuration where data -> 'key' = $1",
				['"' + key + '"'],
				function (err1) {
					testingTools.code.expect(err1, JSON.stringify(err1)).to.be.null();
					done1();
					done();
				});
		});
	});
	lab.test('get should work', function (done) {
		services.get(key, function (err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('set should work and add new configuration object', function (done) {
		services.set(key, value, function (err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('set should work and update existing configuration object', function (done) {
		services.set(key, value, function (err) {
			testingTools.code.expect(err).to.be.null();
			services.set(key, value, function (err1) {
				testingTools.code.expect(err1).to.be.null();
				done();
			});
		});
	});


	lab.test('set to same object should result in one row', function (done) {
		services.set(key, null, function (err) {
			testingTools.code.expect(err).to.be.null();
			services.set(key, 0, function (err1) {
				testingTools.code.expect(err1).to.be.null();
				services.set(key, undefined, function (err2) {
					testingTools.code.expect(err2).to.be.null();
					databaseUtils.getConnection(function (err3, client, done1) {
						testingTools.code.expect(err3, JSON.stringify(err3)).to.be.null();
						client.query("SELECT * FROM configuration where (data -> 'key') = $1",
							['"' + key + '"'],
							function (err4, res) {
								testingTools.code.expect(err4, JSON.stringify(err4)).to.be.null();
								done1();
								testingTools.code.expect(res.rowCount).to.equal(1);
								done();
							});
					});

				});
			});
		});
	});

});

lab.experiment('Configurations Interactions', function () {
	testingTools.failDbQueryForExperiment(lab, 'configuration');
	var key, value;
	lab.before(function(done){
		key = 'test-key';
		value = 'test-value';
		done();
	});
	lab.test('should return error', function (done) {
		services.set(key, value, function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});
