/**
 * @apiDefine getHotelGroupErrors
 *
 * @apiError (Possible error codes) {String} hg-not-found Hotel group not found ([Definition](#error_hg-not-found))
 */
