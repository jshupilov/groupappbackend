'use strict';
var databaseUtils = require('../utils/databaseUtils');
var errors = require('../utils/errors');

errors.defineError('hg-publish-fail', 'Publish hotelGroup failed', 'Publish hotelGroup failed', 'HotelGroups.Dao');
errors.defineError('hg-unpublish-fail', 'Unpublish hotelGroup failed', 'Unpublish hotelGroup failed', 'HotelGroups.Dao');

/**
 * Find a hotel group by ID
 * @param hotelGroupId
 * @param callback
 */
exports.findByHotelGroupId = function (hotelGroupId, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query("SELECT * from hotel_group where data -> 'hotelGroupId' = $1", ['"' + hotelGroupId + '"'], function (err, result) {
			done();
			callback(err, result);
		});
	});

};

/**
 * Check if hotel group exists
 * @param hotelGroupId
 * @param callback
 */
exports.checkIfExists = function (hotelGroupId, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query("SELECT count(*) as c from hotel_group where data -> 'hotelGroupId' = $1", ['"' + hotelGroupId + '"'], function (err, result) {
			done();
			if (err) {
				return callback(err);
			}
			callback(null, result.rows[0].c > 0);
		});
	});

};

/**
 * Get all hotel groups
 * @param callback
 */
exports.getAll = function (callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query('SELECT * from hotel_group', function (err, result) {
			done();
			callback(err, result);
		});
	});

};

/**
 * Check if hotel group with given id is active
 * @param hotelGroupId
 * @param callback
 */
exports.checkIfActive = function (hotelGroupId, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query('SELECT count(*) AS c from hotel_group WHERE (data -> \'hotelGroupId\') = $1', ['"' + hotelGroupId + '"'], function (err, result) {
			done();
			if (err) {
				return callback(err);
			}
			return callback(null, result.rows[0].c > 0);
		});
	});

};

exports.getMediaConfiguration = function (hotelGroupId, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query("SELECT data -> 'mediaConfiguration' as media_configuration from hotel_group where data -> 'hotelGroupId' = $1", ['"' + hotelGroupId + '"'], function (err, result) {
			done();
			callback(err, result);
		});
	});

};

exports.getCategories = function (hotelGroupId, callback) {
	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query("SELECT data -> 'categories' as categories from hotel_group where data -> 'hotelGroupId' = $1", ['"' + hotelGroupId + '"'], function (err, result) {
			done();
			callback(err, result);
		});
	});

};

exports.getProfilesConfiguration = function (hotelGroupId, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query("SELECT data -> 'profilesConfiguration' as profiles_configuration from hotel_group where data -> 'hotelGroupId' = $1", ['"' + hotelGroupId + '"'], function (err, result) {
			done();
			callback(err, result);
		});
	});

};

/**
 * Get first hotel in database
 * @param callback
 */
exports.getFirst = function (callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query('SELECT * from hotel_group LIMIT 1', function (err, result) {
			done();
			callback(err, result);
		});
	});

};

/**
 * Get first hotel group in database
 * @param data
 * @param callback
 */
exports.create = function (data, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query('INSERT INTO hotel_group (data) VALUES($1)', [data], function (err, result) {
			done();
			callback(err, result);
		});
	});

};


/**
 * Publish hotelGroup version item to live table
 * @param item
 * @param callback
 */
exports.publishHotelGroup = function (item, callback) {

	databaseUtils.doInTransaction(function(client, cb) {
		client.query("DELETE FROM hotel_group WHERE (data ->'hotelGroupId') = $1",
			['"' + item.data.hotelGroupId + '"'], function (err) {
				if (err) {
					return cb(err);
				}
				client.query('INSERT INTO hotel_group (data) VALUES($1)', [item.data], function (err) {
					if(err) {
						return cb(err);
					}
					cb(null);
				});
			});
	}, callback);

};
/**
 * Remove hotelGroup item from live table
 * @param item
 * @param callback
 */
exports.unPublishHotelGroup = function (item, callback) {
	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query("DELETE FROM hotel_group WHERE (data -> 'hotelGroupId') = $1",
			['"' + item.data.hotelGroupId + '"'], function (err, res) {
				done();
				if (err) {
					return callback(err);
				}
				callback(err, res);
			});
	});
};
