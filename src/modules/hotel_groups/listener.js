/**
 * Created by jevgenishupilov on 19/01/15.
 */
'use strict';
var eventUtils = require('../utils/eventUtils');
//update cache when data is updated

eventUtils.emitter.on('hotel.published', function(hotel, callback) {
	var data = { hotelGroupId: hotel.hotelGroup.hotelGroupId };
		eventUtils.emitter.parallel('hotelGroup.dataUpdated', data, function(err) {
			//before listeners are done, call the callback
		return callback(err);
		});
});

eventUtils.emitter.on('hotel.unpublished', function(hotelGroupId, hotelId, callback) {
	var data = { hotelGroupId: hotelGroupId };
	eventUtils.emitter.parallel('hotelGroup.dataUpdated', data, function(err) {
		//before listeners are done, call the callback
		return callback(err);
	});

});

eventUtils.emitter.on('hotelGroup.published', function(hotelGroup, callback) {
	var data = { hotelGroupId: hotelGroup.hotelGroupId };
	eventUtils.emitter.parallel('hotelGroup.dataUpdated', data, function(err) {
		//before listeners are done, call the callback
		return callback(err);
	});

});

eventUtils.emitter.on('hotelGroup.unpublished', function(hotelGroupId, callback) {
	var data = { hotelGroupId: hotelGroupId };
	eventUtils.emitter.parallel('hotelGroup.dataUpdated', data, function(err) {
		//before listeners are done, call the callback
		return callback(err);
	});

});
