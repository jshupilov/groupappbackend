'use strict';
var dao = require('./dao');
var validation = require('./validation');
var langUtils = require('../utils/langUtils');
var apiUtils = require('../utils/apiUtils');
var mediaServices = require('../media/services');
var errors = require('../utils/errors');
var eventUtils = require('../utils/eventUtils');
var q = require('q');
var version = require('../versioning/services');

errors.defineError('hg-not-found', 'System failure', 'Invalid hotel group id provided', 'HotelGroup', 400);
errors.defineError('hg-err-save', 'System failure', 'Error occurred while saving hotel group to database', 'HotelGroup');
errors.defineError('hg-err-del', 'System failure', 'Error occurred while deleting hotel group from database', 'HotelGroup');
errors.defineError('hg-err-format', 'System failure', 'Error occurred while formatting data', 'HotelGroup');
errors.defineError('hg-err-up', 'System failure', 'Error occurred while updating hotel group', 'HotelGroup');

/**
 * Check if this hotel group id is valid for a brand new hotel group
 * @param hotelGroupId
 * @param callback
 */
exports.validateHotelGroupId = function(hotelGroupId, callback){
	dao.checkIfActive(hotelGroupId, function(err, isActive){
		callback(err, isActive);
	});
};

/**
 * Check if this hotel group id is valid for a brand new hotel group
 * @param hotelGroupId
 * @param callback
 */
exports.checkIfHotelGroupIdExists = function(hotelGroupId, callback){
	dao.checkIfExists(hotelGroupId, function(err, isActive){
		callback(err, isActive);
	});
};

/**
 * Format initial hotel group data
 * @param hotelGroup
 * @param callback
 */
function formatInitialData(hotelGroup, callback) {

	var promises = [];
	var out = {
		supportedLanguages: [],
		defaultLanguage: langUtils.getLanguageObject(hotelGroup.data.defaultLanguage),
		translations: {},
		hotelGroupInfo: {
			localized: {}
		},
		gaTrackingId: hotelGroup.data.gaTrackingId ? hotelGroup.data.gaTrackingId : null,
		theme: null

	};

	var p1 = q.defer();
	promises.push(p1.promise);

	langUtils.mergeTranslationsWithDefault(hotelGroup.data.translations, function(err, translations){
		if(err){
			return p1.reject(err);
		}else{
			out.translations = translations;
			p1.resolve();
		}
	});


	function format(promise){
		if(hotelGroup.data.slhCustom){
			out.slhCustom = hotelGroup.data.slhCustom;
		}

		try {
			if(hotelGroup.data.mediaConfiguration && hotelGroup.data.mediaConfiguration.theme){
				out.theme = hotelGroup.data.mediaConfiguration.theme;
			}

			hotelGroup.data.supportedLanguages.forEach(function(langCode) {
				out.supportedLanguages.push(langUtils.getLanguageObject(langCode));
				if( hotelGroup.data.info.localized[langCode] ){
					out.hotelGroupInfo.localized[langCode] = langUtils.getLocalizedData(hotelGroup.data.info, langCode, function(item) {
						return {
							name: item.name,
							logo: mediaServices.formatImageObject(item.logo, exports.getMediaConfigurationFromHotelGroup(hotelGroup.data), 'hgLogo'),
							backgroundImage: mediaServices.formatImageObject(item.backgroundImage, exports.getMediaConfigurationFromHotelGroup(hotelGroup.data), 'bg')

						};

					});
				}

			});
			promise.resolve();
		} catch(e) {
			promise.reject(errors.newError('hg-err-format', {originalError: e.toString()}));
		}
	}


	var p2 = q.defer();
	promises.push(p2.promise);

	format(p2);


	q.all(promises).done(function(){
		callback(null, out);
	}, function(err){
		callback(err);
	});



};

/**
 * Returns localized background image
 * @param hotelGroup
 * @param lang
 * @returns {*}
 */
exports.getLocalizedBackgroundImage = function(hotelGroup, lang){
	if( hotelGroup.info.localized[lang] && hotelGroup.info.localized[lang].backgroundImage ){
		return hotelGroup.info.localized[lang].backgroundImage;
	}else if(hotelGroup.info.backgroundImage){
		return hotelGroup.info.backgroundImage;
	}else{
		return null;
	}

};

/**
 * Format main menu item for output
 * @param hotelGroup
 * @param item
 * @returns {*}
 */
function formatMainMenuItem(hotelGroup, item) {

	var btnImageType = 'menuBtn';
	if(item.action.type === 'logo'){
		btnImageType = 'menuBtnLogo';
	}
	var out = {
		action: {
			type: item.action.type
		},
		children: item.children,
		localized: langUtils.getLocalizedDataForAllLanguages(item, function(localItem, lang) {
			var obj = {
				name: localItem.name,
				buttonImage: mediaServices.formatImageObject(localItem.buttonImage, exports.getMediaConfigurationFromHotelGroup(hotelGroup.data), btnImageType),
				backgroundImage: mediaServices.formatImageObject([localItem.backgroundImage, exports.getLocalizedBackgroundImage(hotelGroup.data, lang)], exports.getMediaConfigurationFromHotelGroup(hotelGroup.data), ['menuPageBg', 'bg'])
			};
			if(localItem.slideshowImages){
				obj.slideshowImages = [];
				for(var i in localItem.slideshowImages){
					obj.slideshowImages.push(mediaServices.formatImageObject(localItem.slideshowImages[i], exports.getMediaConfigurationFromHotelGroup(hotelGroup.data), 'slideshowImage'));
				}
			}
			return obj;
		})
	};

	var path = item.action.navigation.path;
	var method = item.action.navigation.method || 'GET';
	var query = item.action.navigation.query || {};
	switch(item.action.type) {
		case 'customUrl':
			out.action.href = path;
			out.action.method = method;
			out.action.viewType = item.action.navigation.viewType;
			break;
		case 'navigation':
		case 'search':
			//if related to category type
			if(item.action.navigation.categoryType){
				path = 'categories';
				query.type = item.action.navigation.categoryType;
			}
			out.action.href = apiUtils.getServiceHref(hotelGroup.data.hotelGroupId, path, query);
			out.action.viewType = item.action.navigation.viewType;
			break;
		case 'slideshow':
			out.action.viewType = item.action.navigation.viewType;
			break;
		default:
			out.action.viewType = item.action.navigation.viewType;
			break;

	}
	return out;
};

/**
 * Format the whole main menu
 * @param hotelGroup
 * @returns {{items: Array}}
 */
function formatMainMenu(hotelGroup) {

	var out = {
		items: [],
		customItems: {}

	};

	for(var li in hotelGroup.data.mainMenu.items) {
		out.items.push(formatMainMenuItem(hotelGroup, hotelGroup.data.mainMenu.items[li]));
	}
	if(hotelGroup.data.mainMenu.customItems){
		for(var ci in hotelGroup.data.mainMenu.customItems) {
			out.customItems[ci] = formatMainMenuItem(hotelGroup, hotelGroup.data.mainMenu.customItems[ci]);
		}
	}
	return out;
};

/**
 * Get initial data for client
 * @param hotelGroupId
 * @param callback
 */
exports.getInitialData = function(hotelGroupId, callback) {

	dao.findByHotelGroupId(hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}
		if(res.rows.length < 1) {
			return callback(errors.newError('hg-not-found', {
				givenHotelGroupId: hotelGroupId,
				providedBy: 'getInitialData'
			}));
		}

		formatInitialData(res.rows[0], function(err, res) {
			callback(err, res);
		});
	});

};

/**
 * Get a hotel group by hotelGroupId
 * @param hotelGroupId
 * @param callback
 */
exports.getHotelGroup = function(hotelGroupId, callback) {
	dao.findByHotelGroupId(hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}
		if(res.rows.length < 1) {
			return callback(errors.newError('hg-not-found', {
				givenHotelGroupId: hotelGroupId,
				providedBy: 'getHotelGroup'
			}));
		}
		callback(null, res.rows[0]);
	});
};


/**
 * Get all hotel groups as raw data
 * @param callback
 */
exports.getAllRaw = function( callback) {
	dao.getAll(function(err, res) {
		if(err) {
			return callback(err);
		}
		callback(null, res.rows);
	});
};

/**
 * Get placeholder image
 * @param hotelGroupId
 * @param callback
 */
exports.getMediaConfiguration = function(hotelGroupId, callback) {
	dao.getMediaConfiguration(hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}
		if(res.rows.length < 1) {
			return callback(errors.newError('hg-not-found', {
				givenHotelGroupId: hotelGroupId,
				providedBy: 'getMediaConfiguration'
			}));
		}
		if(!res.rows[0].media_configuration) {
			return callback(null, {});
		}
		res.rows[0].media_configuration.hotelGroupId = hotelGroupId;
		callback(null, res.rows[0].media_configuration);
	});
};

exports.getMediaConfigurationFromHotelGroup = function(hotelGroupData) {
	hotelGroupData.mediaConfiguration.hotelGroupId = hotelGroupData.hotelGroupId;
	return hotelGroupData.mediaConfiguration;
};

/**
 * Get placeholder image
 * @param hotelGroupId
 * @param callback
 */
exports.getCategories = function(hotelGroupId, callback) {
	dao.getCategories(hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}
		if(res.rows.length < 1) {
			return callback(errors.newError('hg-not-found', {
				givenHotelGroupId: hotelGroupId,
				providedBy: 'getCategories'
			}));
		}
		if(!res.rows[0].categories) {
			return callback(null, []);
		}
		callback(null, res.rows[0].categories);
	});
};

exports.getProfilesConfiguration = function(hotelGroupId, callback) {
	dao.getProfilesConfiguration(hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}
		if(res.rows.length < 1) {
			return callback(errors.newError('hg-not-found', {
				givenHotelGroupId: hotelGroupId,
				providedBy: 'getProfilesConfiguration'
			}));
		}
		if(!res.rows[0].profiles_configuration) {
			return callback(null, {});
		}
		callback(null, res.rows[0].profiles_configuration);
	});
};

exports.getProfilesConfigurationFromHotelGroup = function(hotelGroupData) {
	return hotelGroupData.profilesConfiguration;
};

/**
 * Get main menu for the client
 * @param hotelGroupId
 * @param callback
 */
exports.getMainMenu = function(hotelGroupId, callback) {
	exports.getHotelGroup(hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}
		callback(null, formatMainMenu(res));
	});

};

/**
 * Add new hotelgroup item to version table
 * @param data
 * @param callback
 */
exports.createHotelGroup = function(data, callback) {
	validation.validateHotelGroupData(data, 'insert', function(err){
		if(err){
			return callback(err);
		}
//emit event
		eventUtils.emitter.parallel('hotelGroup.beforeCreate', data, function(err) {
//before listeners are done, call the callback
			if(err){
				return callback(err);
			}
			version.addItem(data, 'hotel_group_v', function(err, res){
				if(err) {
					return callback(err);
				}
				//emit event
				eventUtils.emitter.parallel('hotelGroup.created', data, function(err) {
					//after listeners are done, call the callback
					if(err){
						return callback(err);
					}
					callback(null, res);
				});
			});
		});
	});
};

/**
 * Update existing hotelGroup in version table
 * @param data
 * @param callback
 */
exports.updateHotelGroup = function(data, callback) {
	validation.validateHotelGroupData(data, 'update', function(err) {
		if(err) {
			return callback(err);
		}
		eventUtils.emitter.parallel('hotelGroup.beforeUpdate', data, function(err) {
			//before listeners are done, call the callback
			if(err) {
				return callback(err);
			}
			version.updateItem(data, 'hotel_group_v', function(err, res) {
				if(err) {
					return callback(err);
				}
				eventUtils.emitter.parallel('hotelGroup.updated', data, function(err) {
					//after listeners are done, call the callback
					if(err) {
						return callback(err);
					}
					callback(null, res);
				});
			});
		});
	});
};

/**
 * Publish hotelGroup version item to live table
 * @param item
 * @param callback
 */
exports.publishHotelGroup = function(item, callback){

	dao.publishHotelGroup(item, function(err, res){
		if (err) {
			return callback(err);
		}
		version.updateVersion(item, 'hotel_group_v', function(err){
			if(err){
				return callback(err);
			}
			eventUtils.emitter.parallel('hotelGroup.published', item.data, function(err) {
				if(err){
					return callback(err);
				}

				return callback(null, res);
			});
		});
	});
};

/**
 * Delete hotelgroup from live table
 * @param item
 * @param callback
 */
exports.unPublishItem = function (item, callback) {
	dao.unPublishHotelGroup(item, function(err, res){
		if (err) {
			return callback(err, res);
		}
		version.updateVersion(item, 'hotel_group_v', function(err) {
			if(err){
				return callback(err);
			}
			eventUtils.emitter.parallel('hotelGroup.unpublished', item.data.hotelGroupId, function (err) {
				if(err){
					return callback(err);
				}
				callback(null, res);
			});

		});
	});
};

exports.createTestHotelGroup = function(data, callback) {
	validation.validateHotelGroupData(data, 'testData', function(err){
		if(err){
			//if exists, update
			if(err.data.code === 'hgv-hgId-exists'){
				return exports.updateHotelGroup(data, callback);
			}else{
				return callback(err);
			}
		}
		exports.createHotelGroup(data, callback);
	});
};

exports.updateHotelGroupCatIDs = function (hotelGroupId, newCategories, callback){

	exports.getHotelGroup(hotelGroupId, function (err, data){
		if(err){
			return callback(err);
		}

		var cat = [];
		data.data.categories.forEach(function(category){
			cat.push(category.categoryId);
		});

		for(var i = 0; i < newCategories.length; i++){
			if(cat.indexOf(newCategories[i].categoryId) < 0){
				data.data.categories.push(newCategories[i]);
			}

		}
		callback(null, data);
	});


};
