'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var dao = require('../dao');
var databaseUtils = require('../../utils/databaseUtils');
var version = require('../../versioning/services');
var escape = require('pg-escape');
var data = {
	hotelGroupId: 'bla-hgId'
};
lab.experiment('Hotel Groups DAO Interactions', function () {
	var _hgId;
	lab.before(function (done) {
		_hgId = testingTools.getHotelGroupId();
		done();
	});
	lab.after(function (done) {
		dao.unPublishHotelGroup({data: data}, function (err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('findByHotelGroupId should work', function (done) {
		dao.findByHotelGroupId(_hgId, function (err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.expectQueryResultsWithFields(res, ['id', 'data']);
			done();
		});
	});

	lab.test('checkIfExists should work', function (done) {
		dao.checkIfExists(_hgId, function (err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.equal(true);
			done();
		});
	});

	lab.test('getAll should work', function (done) {
		dao.getAll(function (err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.expectQueryResultsWithFields(res, ['id', 'data']);
			done();
		});
	});

	lab.test('checkIfActive should work', function (done) {
		dao.checkIfActive(_hgId, function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.true();
			done();
		});
	});

	lab.test('getMediaConfiguration should work', function (done) {
		dao.getMediaConfiguration(_hgId, function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.expectQueryResultsWithFields(res, ['media_configuration']);
			done();
		});
	});

	lab.test('getCategories should work', function (done) {
		dao.getCategories(_hgId, function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.expectQueryResultsWithFields(res, ['categories']);
			done();
		});
	});

	lab.test('getProfilesConfiguration should work', function (done) {
		dao.getProfilesConfiguration(_hgId, function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.expectQueryResultsWithFields(res, ['profiles_configuration']);
			done();
		});
	});
	lab.test('getFirst should work', function (done) {
		dao.getFirst(function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.expectQueryResultsWithFields(res, ['id', 'data']);
			done();
		});
	});
	lab.test('create should work', function (done) {
		dao.create(data, function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});

});

lab.experiment('Hotel Groups DAO Interactions checkIfActive', function () {
	var _getConnection;
	lab.before(function (done) {
		_getConnection = databaseUtils.getConnection;
		done();
	});
	lab.after(function (done) {
		databaseUtils.getConnection = _getConnection;
		done();
	});
	lab.test('checkIfActive should return true', function (done) {
		databaseUtils.getConnection = function (cb) {
			cb(null, {
				query: function (q, p, cb2) {
					cb2(null, {
						rows: [{
							c: 1
						}]
					});
				}
			}, function () {
			});
		};
		dao.checkIfActive(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.true();
			done();
		});
	});
	lab.test('checkIfActive should return false', function (done) {
		databaseUtils.getConnection = function (cb) {
			cb(null, {
				query: function (q, p, cb2) {
					cb2(null, {
						rows: [{
							c: 0
						}]
					});
				}
			}, function () {
			});
		};
		dao.checkIfActive(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.false();
			done();
		});
	});
});

lab.experiment('Hotel Groups DAO if DB structure invalid', function () {
	var _hgId;
	testingTools.failDbQueryForExperiment(lab, 'hotel_group');
	lab.before(function (done) {
		_hgId = testingTools.getHotelGroupId();
		done();
	});
	lab.after(function (done) {
		dao.unPublishHotelGroup({data: data}, function (err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('findByHotelGroupId should give error', function (done) {
		dao.findByHotelGroupId(_hgId, function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('getAll should give error', function (done) {
		dao.getAll(function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});

	lab.test('checkIfActive should give error', function (done) {
		dao.checkIfActive(_hgId, function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('getMediaConfiguration should give error', function (done) {
		dao.getMediaConfiguration(_hgId, function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('getCategories should give error', function (done) {
		dao.getCategories(_hgId, function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('getProfilesConfiguration should give error', function (done) {
		dao.getProfilesConfiguration(_hgId, function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('getFirst should give error', function (done) {
		dao.getFirst(function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('create should give error', function (done) {
		dao.create(data, function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});

});

lab.experiment('Hotel Groups DAO if DB connection is down', function () {
	testingTools.killDbConnectionForExperiment(lab);
	var _hgId;
	lab.before(function (done) {
		_hgId = testingTools.getHotelGroupId();
		done();
	});
	lab.after(function (done) {
		dao.unPublishHotelGroup({data: data}, function (err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('findByHotelGroupId should give error', function (done) {
		dao.findByHotelGroupId(_hgId, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});

	lab.test('checkIfExists should give error', function (done) {
		dao.checkIfExists(_hgId, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});

	lab.test('getAll should give error', function (done) {
		dao.getAll(function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});

	lab.test('checkIfActive should give error', function (done) {
		dao.checkIfActive(_hgId, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});

	lab.test('getMediaConfiguration should give error', function (done) {
		dao.getMediaConfiguration(_hgId, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('getCategories should give error', function (done) {
		dao.getCategories(_hgId, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('getProfilesConfiguration should give error', function (done) {
		dao.getProfilesConfiguration(_hgId, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('getFirst should give error', function (done) {
		dao.getFirst(function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('create should give error', function (done) {
		dao.create(data, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});

	lab.test('unPublishHotelGroup should give error', function (done) {
		dao.unPublishHotelGroup({data: data}, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});

lab.experiment('Hotel Groups DAO if DB table is missing', function () {
	testingTools.failDbQueryForExperiment(lab, 'hotel_group');

	lab.test('checkIfExists should return error', function (done) {
		dao.checkIfExists('adas', function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});

lab.experiment('HotelGroups dao publishHotelGroup', function () {
	var hgData =
	{
		'data': {
			'meta': {},
			'hotelGroupId': 'test-hgid-ver-fail'
		}
	};
	lab.test('Publish should go well', function (done) {
		dao.publishHotelGroup(hgData, function (err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
});

lab.experiment('Hotel Grouos DAO versioning failures', function () {
	var getC, client, cd, setLive, dataVer3;
	lab.before(function (done) {
		setLive = version.setItemAlive;
		getC = databaseUtils.getConnection;

		databaseUtils.getConnection(function (err, cl, doneDb) {
			testingTools.code.expect(err).to.be.null();
			client = cl;
			cd = doneDb;

			done();
		});

	});
	lab.beforeEach(function (done) {
		dataVer3 =
		{
			'data': {
				'meta': {},
				'hotelGroupId': 'test-hgid-ver-fail'
			}
		};
		done();
	});

	lab.afterEach(function (done) {
		databaseUtils.getConnection = getC;
		version.setItemAlive = setLive;
		client.query('ROLLBACK', function (err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});

	});
	lab.after(function (done) {
		client.query("DELETE FROM hotel_group WHERE (data -> 'hotelGroupId') = $1",
			['"' + dataVer3.data.hotelGroupId + '"'], function (err) {
				testingTools.code.expect(err).to.be.null();
				testingTools.disableEvents();
				cd();
				done();
			});
	});

	lab.test('Transaction should fail to start', function (done) {
		databaseUtils.getConnection = function (cb) {
			cb(
				null,
				{
					query: function () {
						return arguments[arguments.length - 1](new Error('Transaction start failure'));
					}
				},
				function () {
				}
			);
		};
		dao.publishHotelGroup(dataVer3, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('Transaction start failure');
			done();
		});

	});
	lab.test('Delete item should fail', function (done) {
		var counter = 0;
		databaseUtils.getConnection = function (cb) {
			cb(
				null,
				{
					query: function () {
						counter++;
						if (counter === 2) {
							return arguments[arguments.length - 1](new Error('Delete item failure'));
						}
						client.query.apply(client, arguments);
					}
				},
				function () {
				}
			);
		};

		dao.publishHotelGroup(dataVer3, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('Delete item failure');
			done();
		});


	});

	lab.test('Insert item should fail', function (done) {
		var counter = 0;
		databaseUtils.getConnection = function (cb) {
			cb(
				null,
				{
					query: function () {
						counter++;
						if (counter === 3) {
							return arguments[arguments.length - 1](new Error('Insert failure'));
						}
						client.query.apply(client, arguments);
					}
				},
				function () {
				}
			);
		};
		dao.publishHotelGroup(dataVer3, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('Insert failure');
			done();
		});
	});

	lab.test('Commit should fail', function (done) {
		var counter = 0;
		databaseUtils.getConnection = function (cb) {
			cb(
				null,
				{
					query: function () {
						counter++;
						if (counter === 4) {
							return arguments[arguments.length - 1](new Error('Commit failure'));
						}
						client.query.apply(client, arguments);
					}
				},
				function () {
				}
			);
		};
		dao.publishHotelGroup(dataVer3, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('Commit failure');
			done();
		});
	});
});

lab.experiment('hotelgroups.dao.publishHotelGroup.failures', function () {
	testingTools.killDbConnectionForExperiment(lab);
	lab.test('Database should give failure', function (done) {
		dao.publishHotelGroup({}, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});

lab.experiment('Dao unpublish failures', function () {
	testingTools.killDbConnectionForExperiment(lab);
	lab.test('Database connection should get failure', function (done) {
		dao.unPublishHotelGroup({}, function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});

lab.experiment('Hotel group unpublish', function () {
	var tableName = 'hotel_group';
	var verId, getC;
	var hgData =
	{
		'data': {
			'meta': {},
			'hotelGroupId': 'test-hgid-ver-succ'
		}
	};


	lab.before(function (done) {
		getC = databaseUtils.getConnection;
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', tableName, JSON.stringify(hgData.data));
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rows[0].id).to.be.above(0);
				verId = res.rows[0].id;
				doneDb();
				done();
			});
		});
	});

	lab.afterEach(function (done) {
		databaseUtils.getConnection = getC;
		done();
	});

	lab.after(function (done) {
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM %I WHERE id= %L', tableName, verId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(0);
				doneDb();
				done();
			});
		});
	});
	lab.test('Unpublish should go well', function (done) {
		dao.unPublishHotelGroup(hgData, function (err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('Query should fail', function (done) {
		databaseUtils.getConnection = function (cb) {
			cb(
				null,
				{
					query: function () {
						return arguments[arguments.length - 1](new Error('query failed'));
					}
				},
				function () {
				}
			);
		};
		dao.unPublishHotelGroup(hgData, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('query failed');
			done();
		});
	});
});
