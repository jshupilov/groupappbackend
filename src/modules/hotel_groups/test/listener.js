/**
 * Created by jevgenishupilov on 10/02/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var eventUtils = require('../../utils/eventUtils');



lab.experiment('Hotel Group listener', function(){

	//modify functions
	lab.before(function(done){
		testingTools.enableEvents();
		eventUtils.emitter.removeAllListeners('hotel.unpublished');
		eventUtils.emitter.removeAllListeners('hotel.published');
		eventUtils.emitter.removeAllListeners('hotelGroup.published');
		eventUtils.emitter.removeAllListeners('hotelGroup.unpublished');
		testingTools.requireUncached('../modules/hotel_groups/listener');
		done();
	});

	//set functions back to originals
	lab.after(function(done){
		testingTools.disableEvents();
		done();
	});

	lab.test('should call hotel.published', {timeout: 10000}, function(done){
		eventUtils.emitter.parallel('hotel.published', {hotelGroup: {hotelGroupId: testingTools.getHotelGroupId()}}, function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should call hotel.unpublished', {timeout: 10000}, function(done){
		eventUtils.emitter.parallel('hotel.unpublished', testingTools.getHotelGroupId(), 'test-hotel-id', function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});


	//
	lab.test('should call hotelGroup.published', {timeout: 10000}, function(done){
		eventUtils.emitter.parallel('hotelGroup.published', {hotelGroupId: testingTools.getHotelGroupId()}, function(err) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});
	});

	lab.test('should call hotelGroup.unpublished', {timeout: 10000}, function(done){
		eventUtils.emitter.parallel('hotelGroup.unpublished', testingTools.getHotelGroupId(), function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
});
