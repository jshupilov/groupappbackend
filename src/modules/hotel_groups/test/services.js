'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var dao = require('../dao');
var langUtils = require('../../utils/langUtils');
var mediaServices = require('../../media/services');
var eventUtils = require('../../utils/eventUtils');
var htlGData = require('./hotelGroup.json');
var databaseUtils = require('../../utils/databaseUtils');
var version = require('../../versioning/services');
var escape = require('pg-escape');


lab.experiment('hotels_groups.services.checkIfHotelGroupIdExists', function () {
	var _checkIfExists;
	lab.before(function (done) {
		_checkIfExists = dao.checkIfExists;
		done();
	});

	lab.after(function (done) {
		dao.checkIfExists = _checkIfExists;
		done();
	});
	lab.test('should work', function (done) {
		dao.checkIfExists = function (hgId, cb) {
			cb(null, true);
		};
		services.checkIfHotelGroupIdExists(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.boolean();
			testingTools.code.expect(res).to.be.true();
			done();
		});
	});
});

lab.experiment('hotels_groups.services.validateHotelGroupId', function () {
	var _checkIfActive;
	lab.before(function (done) {
		_checkIfActive = dao.checkIfActive;
		done();
	});

	lab.after(function (done) {
		dao.checkIfActive = _checkIfActive;
		done();
	});
	lab.test('should work', function (done) {
		dao.checkIfActive = function (hgId, cb) {
			cb(null, true);
		};
		services.validateHotelGroupId(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.boolean();
			testingTools.code.expect(res).to.be.true();
			done();
		});
	});
});

lab.experiment('hotels_groups.services.getInitialData', function () {
	var _findByHotelGroupId,
		_getLocalizedData,
		_formatImageObject,
		_mergeTranslationsWithDefault,
		_getMediaConfigurationFromHotelGroup;

	lab.before(function (done) {
		_findByHotelGroupId = dao.findByHotelGroupId;
		_getLocalizedData = langUtils.getLocalizedData;
		_formatImageObject = mediaServices.formatImageObject;
		_getMediaConfigurationFromHotelGroup = services.getMediaConfigurationFromHotelGroup;
		_mergeTranslationsWithDefault = langUtils.mergeTranslationsWithDefault;
		done();
	});

	lab.afterEach(function (done) {
		dao.findByHotelGroupId = _findByHotelGroupId;
		langUtils.getLocalizedData = _getLocalizedData;
		mediaServices.formatImageObject = _formatImageObject;
		services.getMediaConfigurationFromHotelGroup = _getMediaConfigurationFromHotelGroup;
		langUtils.mergeTranslationsWithDefault = _mergeTranslationsWithDefault;

		done();
	});

	lab.test('should work without supportedLanguages and gaTrackingId', function (done) {
		dao.findByHotelGroupId = function (hgId, cb) {
			cb(null, {
				rows: [{
					data: {
						defaultLanguage: 'en_GB',
						supportedLanguages: [],
						hotelGroupId: testingTools.getHotelGroupId(),
						translations: {}
					}
				}]
			});
		};
		services.getInitialData(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['supportedLanguages', 'defaultLanguage', 'translations', 'hotelGroupInfo', 'theme', 'gaTrackingId']);
			testingTools.code.expect(res.gaTrackingId).to.be.null();
			done();
		});
	});

	lab.test('should work without mediaConfiguration.theme and with gaTrackingId', function (done) {
		dao.findByHotelGroupId = function (hgId, cb) {
			cb(null, {
				rows: [{
					data: {
						defaultLanguage: 'en_GB',
						supportedLanguages: [],
						hotelGroupId: testingTools.getHotelGroupId(),
						translations: {},
						mediaConfiguration: {},
						gaTrackingId: 'gaTrackingId-asdasd'
					}
				}]
			});
		};
		services.getInitialData(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['supportedLanguages', 'defaultLanguage', 'translations', 'hotelGroupInfo', 'theme']);
			testingTools.code.expect(res.gaTrackingId).to.equal('gaTrackingId-asdasd');
			done();
		});
	});

	lab.test('should work with mediaConfiguration.theme', function (done) {
		dao.findByHotelGroupId = function (hgId, cb) {
			cb(null, {
				rows: [{
					data: {
						defaultLanguage: 'en_GB',
						supportedLanguages: [],
						hotelGroupId: testingTools.getHotelGroupId(),
						translations: {},
						mediaConfiguration: {
							theme: {blaasdsad: 12123}
						}
					}
				}]
			});
		};
		services.getInitialData(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['supportedLanguages', 'defaultLanguage', 'translations', 'hotelGroupInfo', 'theme']);
			testingTools.code.expect(res.theme).to.include(['blaasdsad']);
			done();
		});
	});

	lab.test('should work with supportedLanguages and localized info', function (done) {

		dao.findByHotelGroupId = function (hgId, cb) {
			cb(null, {
				rows: [{
					data: {
						defaultLanguage: 'en_GB',
						supportedLanguages: ['en_GB'],
						hotelGroupId: testingTools.getHotelGroupId(),
						translations: {},
						info: {
							localized: {
								'en_GB': {}
							}
						}
					}
				}]
			});
		};

		langUtils.getLocalizedData = function (info, lc, cb) {
			cb({
				name: '',
				logo: '',
				backgroundImage: ''
			});
		};
		mediaServices.formatImageObject = function (bgImg, gmcfhtg, prefix) {
			return prefix;
		};
		services.getMediaConfigurationFromHotelGroup = function (data) {
			return data;
		};
		services.getInitialData(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['supportedLanguages', 'defaultLanguage', 'translations', 'hotelGroupInfo', 'theme']);
			done();
		});
	});

	lab.test('should work with supportedLanguages and without localized info', function (done) {
		dao.findByHotelGroupId = function (hgId, cb) {
			cb(null, {
				rows: [{
					data: {
						defaultLanguage: 'en_GB',
						supportedLanguages: ['en_GB'],
						hotelGroupId: testingTools.getHotelGroupId(),
						translations: {},
						info: {
							localized: []
						}
					}
				}]
			});
		};
		services.getInitialData(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['supportedLanguages', 'defaultLanguage', 'translations', 'hotelGroupInfo', 'theme']);
			done();
		});
	});

	lab.test('should given error with miss one of the structure key', function (done) {
		dao.findByHotelGroupId = function (hgId, cb) {
			cb(null, {
				rows: [{
					data: {
						defaultLanguage: 'en_GB',
						hotelGroupId: testingTools.getHotelGroupId(),
						translations: {},
						info: {
							localized: []
						}
					}
				}]
			});
		};
		services.getInitialData(testingTools.getHotelGroupId(), function (err) {
			testingTools.expectError(err, 'hg-err-format');
			done();
		});
	});

	lab.test('should given error with invalid database response', function (done) {
		dao.findByHotelGroupId = function (hgId, cb) {
			cb(new Error('Error'));
		};
		services.getInitialData(testingTools.getHotelGroupId(), function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error');
			done();
		});
	});

	lab.test('should given error with empty database response', function (done) {
		dao.findByHotelGroupId = function (hgId, cb) {
			cb(null, {
				rows: []
			});
		};
		services.getInitialData(testingTools.getHotelGroupId(), function (err) {
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});

	lab.test('should given error if translations merge fails', function (done) {
		langUtils.mergeTranslationsWithDefault = function () {
			arguments[arguments.length - 1](new Error('j98sdfj89sfd'));
		};
		services.getInitialData(testingTools.getHotelGroupId(), function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('j98sdfj89sfd');
			done();
		});
	});

	lab.test('should give SLH flag "slhCustom" if hotelgroup is SLH', function (done) {
		dao.findByHotelGroupId = function (hgId, cb) {
			cb(null, {
				rows: [{
					data: {
						defaultLanguage: 'en_GB',
						supportedLanguages: ['en_GB'],
						hotelGroupId: testingTools.getHotelGroupId(),
						translations: {},
						info: {
							localized: []
						},
						slhCustom: true
					}
				}]
			});
		};
		services.getInitialData(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.include(['slhCustom']);
			done();
		});
	});

});

lab.experiment('hotels_groups.services.getHotelGroup', function () {
	var _findByHotelGroupId;

	lab.before(function (done) {
		_findByHotelGroupId = dao.findByHotelGroupId;
		done();
	});

	lab.after(function (done) {
		dao.findByHotelGroupId = _findByHotelGroupId;
		done();
	});

	lab.test('should work', function (done) {
		dao.findByHotelGroupId = function (hgId, cb) {
			cb(null, {
				rows: [{
					hotelGroupId: testingTools.getHotelGroupId()
				}]
			});
		};
		services.getHotelGroup(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['hotelGroupId']);
			done();
		});
	});

	lab.test('should given error with invalid database response', function (done) {
		dao.findByHotelGroupId = function (hgId, cb) {
			cb(new Error('Error'));
		};
		services.getHotelGroup(testingTools.getHotelGroupId(), function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error');
			done();
		});
	});

	lab.test('should given error with empty database response', function (done) {
		dao.findByHotelGroupId = function (hgId, cb) {
			cb(null, {
				rows: []
			});
		};
		services.getHotelGroup(testingTools.getHotelGroupId(), function (err) {
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});

});

lab.experiment('hotels_groups.services.getAllRaw', function () {
	var _getAll;

	lab.before(function (done) {
		_getAll = dao.getAll;
		done();
	});

	lab.after(function (done) {
		dao.getAll = _getAll;
		done();
	});

	lab.test('should work', function (done) {
		dao.getAll = function (cb) {
			cb(null, {
				rows: [{
					hotelGroupId: testingTools.getHotelGroupId()
				}]
			});
		};
		services.getAllRaw(function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res[0]).to.include(['hotelGroupId']);
			done();
		});
	});

	lab.test('should given error with invalid database response', function (done) {
		dao.getAll = function (cb) {
			cb(new Error('Error'));
		};
		services.getAllRaw(function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error');
			done();
		});
	});

});

lab.experiment('hotels_groups.services.getMediaConfiguration', function () {
	var _getMediaConfiguration;

	lab.before(function (done) {
		_getMediaConfiguration = dao.getMediaConfiguration;
		done();
	});

	lab.after(function (done) {
		dao.getMediaConfiguration = _getMediaConfiguration;
		done();
	});

	lab.test('should work', function (done) {
		dao.getMediaConfiguration = function (hgId, cb) {
			cb(null, {
				rows: [{
					media_configuration: {}
				}]
			});
		};
		services.getMediaConfiguration(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['hotelGroupId']);
			done();
		});
	});

	lab.test('should given error with invalid database response', function (done) {
		dao.getMediaConfiguration = function (hgId, cb) {
			cb(new Error('Error'));
		};
		services.getMediaConfiguration(testingTools.getHotelGroupId(), function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error');
			done();
		});
	});

	lab.test('should given error with empty database response', function (done) {
		dao.getMediaConfiguration = function (hgId, cb) {
			cb(null, {
				rows: []
			});
		};
		services.getMediaConfiguration(testingTools.getHotelGroupId(), function (err) {
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});

	lab.test('should return empty object with miss media_configuration', function (done) {
		dao.getMediaConfiguration = function (hgId, cb) {
			cb(null, {
				rows: [{
					hotelGroupId: testingTools.getHotelGroupId()
				}]
			});
		};
		services.getMediaConfiguration(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(Object.keys(res).length).to.equal(0);

			done();
		});
	});
});

lab.experiment('hotels_groups.services.getCategories', function () {
	var _getCategories;

	lab.before(function (done) {
		_getCategories = dao.getCategories;
		done();
	});

	lab.after(function (done) {
		dao.getCategories = _getCategories;
		done();
	});

	lab.test('should work', function (done) {
		dao.getCategories = function (hgId, cb) {
			cb(null, {
				rows: [{
					categories: []
				}]
			});
		};
		services.getCategories(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array();
			done();
		});
	});

	lab.test('should given error with invalid database response', function (done) {
		dao.getCategories = function (hgId, cb) {
			cb(new Error('Error getCategories dao.getCategories'));
		};
		services.getCategories(testingTools.getHotelGroupId(), function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error getCategories dao.getCategories');
			done();
		});
	});

	lab.test('should given error with empty database response', function (done) {
		dao.getCategories = function (hgId, cb) {
			cb(null, {
				rows: []
			});
		};
		services.getCategories(testingTools.getHotelGroupId(), function (err) {
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});

	lab.test('should return empty object with miss categories', function (done) {
		dao.getCategories = function (hgId, cb) {
			cb(null, {
				rows: [{
					hotelGroupId: testingTools.getHotelGroupId()
				}]
			});
		};
		services.getCategories(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array();
			testingTools.code.expect(res.length).to.equal(0);

			done();
		});
	});
});

lab.experiment('hotels_groups.services.getMediaConfigurationFromHotelGroup', function () {

	lab.test('should work', function (done) {
		var hotelGroupData = {
			hotelGroupId: testingTools.getHotelGroupId(),
			mediaConfiguration: {}
		};
		var res = services.getMediaConfigurationFromHotelGroup(hotelGroupData);
		testingTools.code.expect(res).to.include(['hotelGroupId']);
		done();
	});
});

lab.experiment('hotels_groups.services.getLocalizedBackgroundImage', function () {

	lab.test('should work with empty info->localized and with missing info->backgroundImage', function (done) {
		var hotelGroupData = {
			info: {
				localized: {}
			}
		};
		var res = services.getLocalizedBackgroundImage(hotelGroupData, 'en_GB');
		testingTools.code.expect(res).to.be.null();
		done();
	});

	lab.test('should work with info->localized->lang and missing info->localized->lang->backgroundImage and info->backgroundImage', function (done) {
		var hotelGroupData = {
			info: {
				localized: {
					'en_GB': {}
				}
			}
		};
		var res = services.getLocalizedBackgroundImage(hotelGroupData, 'en_GB');
		testingTools.code.expect(res).to.be.null();
		done();
	});

	lab.test('should work with info->localized->lang->backgroundImage and with missing info->backgroundImage', function (done) {
		var hotelGroupData = {
			info: {
				localized: {
					'en_GB': {
						backgroundImage: {}
					}
				}
			}
		};
		var res = services.getLocalizedBackgroundImage(hotelGroupData, 'en_GB');
		testingTools.code.expect(res).to.be.object();
		done();
	});

	lab.test('should work with empty info->localized and with empty info->backgroundImage', function (done) {
		var hotelGroupData = {
			info: {
				localized: {},
				backgroundImage: {}
			}
		};
		var res = services.getLocalizedBackgroundImage(hotelGroupData, 'en_GB');
		testingTools.code.expect(res).to.be.object();
		done();
	});

});

lab.experiment('hotels_groups.services.getProfilesConfiguration', function () {
	var _getProfilesConfiguration;
	lab.before(function (done) {
		_getProfilesConfiguration = dao.getProfilesConfiguration;
		done();
	});
	lab.after(function (done) {
		dao.getProfilesConfiguration = _getProfilesConfiguration;
		done();
	});
	lab.test('should given error with empty response', function (done) {
		dao.getProfilesConfiguration = function (hgId, cb) {
			cb(null, {
				rows: []
			});
		};
		services.getProfilesConfiguration(testingTools.getHotelGroupId(), function (err) {
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});

	lab.test('should return emty object with missed profiles_configuration field', function (done) {
		dao.getProfilesConfiguration = function (hgId, cb) {
			cb(null, {
				rows: [{}]
			});
		};
		services.getProfilesConfiguration(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(Object.keys(res).length).to.equal(0);
			done();
		});
	});

	lab.test('should given error', function (done) {
		dao.getProfilesConfiguration = function (hgId, cb) {
			cb(new Error('Error hotels_groups.getProfilesConfiguration'));
		};
		services.getProfilesConfiguration(testingTools.getHotelGroupId(), function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error hotels_groups.getProfilesConfiguration');
			done();
		});
	});

	lab.test('should work', function (done) {
		dao.getProfilesConfiguration = function (hgId, cb) {
			cb(null, {
				rows: [{
					profiles_configuration: {
						spDirectoryHref: '',
						spBaseDirectoryHref: ''
					}
				}]
			});
		};
		services.getProfilesConfiguration(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res).to.include(['spDirectoryHref', 'spBaseDirectoryHref']);
			testingTools.code.expect(Object.keys(res).length).to.equal(2);

			done();
		});
	});

});

lab.experiment('hotels_groups.services.getProfilesConfigurationFromHotelGroup', function () {
	lab.test('should work', function (done) {
		var data = {
			profilesConfiguration: {}
		};
		var pc = services.getProfilesConfigurationFromHotelGroup(data);
		testingTools.code.expect(pc).to.be.object();
		testingTools.code.expect(Object.keys(pc).length).to.equal(0);
		done();
	});
});

lab.experiment('hotels_groups.services.getMainMenu', function () {
	var _getHotelGroup;

	lab.before(function (done) {
		_getHotelGroup = services.getHotelGroup;
		done();
	});

	lab.after(function (done) {
		services.getHotelGroup = _getHotelGroup;
		done();
	});

	lab.test('should work with mainMenu->items and mainMenu->customItems and mainMenu->items->localized', function (done) {
		services.getHotelGroup = function (hgId, cb) {
			cb(null, {
				data: {
					mainMenu: {
						items: [{
							action: {
								type: 'logo',
								navigation: {
									path: '',
									query: {}
								}
							},
							children: '',
							localized: {
								'en_GB': {}
							}
						}],
						customItems: {}
					},
					mediaConfiguration: {
						hotelGroupId: testingTools.getHotelGroupId(),
						placeholderImage: {
							'source': {
								public_id: 'devel-i/group-app/test-hgid/placeholder1421413155000',
								isCloudinary: true
							}
						}
					},
					info: {
						localized: {}
					}
				}
			});
		};
		services.getMainMenu(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['items', 'customItems']);
			done();
		});
	});

	lab.test('should work with mainMenu->items and customItems and with missing localized and with exact action->type', function (done) {
		services.getHotelGroup = function (hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					mainMenu: {
						items: [{
							action: {
								type: 'search',
								navigation: {
									path: ''
								}
							},
							children: ''
						}],
						customItems: {}
					}
				}
			});
		};
		services.getMainMenu(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['items', 'customItems']);
			done();
		});
	});

	lab.test('should work with exact action->type and navigation->categoryType and navigation->viewType', function (done) {
		services.getHotelGroup = function (hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					mainMenu: {
						items: [{
							action: {
								type: 'search',
								navigation: {
									path: '',
									categoryType: 'Bla-bla',
									viewType: ''
								}
							},
							children: ''
						}],
						customItems: {}
					}
				}
			});
		};
		services.getMainMenu(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['items', 'customItems']);
			done();
		});
	});

	lab.test('should work with customUrl POST', function (done) {
		services.getHotelGroup = function (hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					mainMenu: {
						items: [{
							action: {
								type: 'customUrl',
								navigation: {
									path: 'http://www.google.com',
									method: 'POST',
									viewType: 'view-smt'
								}
							},
							children: ''
						}],
						customItems: {}
					}
				}
			});
		};
		services.getMainMenu(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['items', 'customItems']);
			testingTools.code.expect(res.items[0].action.href).to.equal('http://www.google.com');
			testingTools.code.expect(res.items[0].action.method).to.equal('POST');
			testingTools.code.expect(res.items[0].action.viewType).to.equal('view-smt');

			done();
		});
	});

	lab.test('should work with customUrl and automatically put GET method', function (done) {
		services.getHotelGroup = function (hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					mainMenu: {
						items: [{
							action: {
								type: 'customUrl',
								navigation: {
									path: 'http://www.google.com',
									viewType: 'view-smt'
								}
							},
							children: ''
						}],
						customItems: {}
					}
				}
			});
		};
		services.getMainMenu(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['items', 'customItems']);
			testingTools.code.expect(res.items[0].action.href).to.equal('http://www.google.com');
			testingTools.code.expect(res.items[0].action.method).to.equal('GET');
			testingTools.code.expect(res.items[0].action.viewType).to.equal('view-smt');

			done();
		});
	});

	lab.test('should work with exact action->type `slideshow` and empty slideshowImages', function (done) {
		services.getHotelGroup = function (hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					mainMenu: {
						items: [{
							action: {
								type: 'slideshow',
								navigation: {
									viewType: ''
								}
							},
							localized: {
								en_GB: {
									slideshowImages: [],
									buttonImage: {
										'source': {
											'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
											'etag': '247b22a529510c0b0a19b4b277f7e7f6',
											'tags': [],
											'type': 'upload',
											'bytes': 25443,
											'width': 860,
											'format': 'png',
											'height': 230,
											'version': 1434352636,
											'public_id': 'devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000',
											'signature': '8bc39849de1189ac0135a7d04b73bad51f1579a0',
											'created_at': '2015-06-15T07:17:16Z',
											'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
											'isCloudinary': true,
											'resource_type': 'image',
											'original_filename': 'image'
										}
									},
									backgroundImage: {
										'source': {
											'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
											'etag': '247b22a529510c0b0a19b4b277f7e7f6',
											'tags': [],
											'type': 'upload',
											'bytes': 25443,
											'width': 860,
											'format': 'png',
											'height': 230,
											'version': 1434352636,
											'public_id': 'devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000',
											'signature': '8bc39849de1189ac0135a7d04b73bad51f1579a0',
											'created_at': '2015-06-15T07:17:16Z',
											'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
											'isCloudinary': true,
											'resource_type': 'image',
											'original_filename': 'image'
										}
									}
								}
							},
							children: ''
						}],
						customItems: {}
					},
					mediaConfiguration: {
						hotelGroupId: testingTools.getHotelGroupId()
					},
					info: {
						localized: {
							en_GB: {
								backgroundImage: {
									'source': {
										'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
										'etag': '247b22a529510c0b0a19b4b277f7e7f6',
										'tags': [],
										'type': 'upload',
										'bytes': 25443,
										'width': 860,
										'format': 'png',
										'height': 230,
										'version': 1434352636,
										'public_id': 'devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000',
										'signature': '8bc39849de1189ac0135a7d04b73bad51f1579a0',
										'created_at': '2015-06-15T07:17:16Z',
										'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
										'isCloudinary': true,
										'resource_type': 'image',
										'original_filename': 'image'
									}
								}
							}
						}
					}
				}
			});
		};
		services.getMainMenu(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['items', 'customItems']);
			testingTools.code.expect(res.items).to.be.an.array();
			testingTools.code.expect(res.items).to.have.length(1);
			testingTools.code.expect(res.items[0]).to.be.an.object();
			testingTools.code.expect(res.items[0]).to.include(['action', 'localized', 'children']);
			testingTools.code.expect(res.items[0].localized).to.be.an.object();
			testingTools.code.expect(res.items[0].localized).to.include('en_GB');
			testingTools.code.expect(res.items[0].localized.en_GB).to.be.an.object();
			testingTools.code.expect(res.items[0].localized.en_GB).to.include(['slideshowImages', 'buttonImage', 'backgroundImage']);
			testingTools.code.expect(res.items[0].localized.en_GB.slideshowImages).to.be.an.array().and.have.length(0);
			testingTools.code.expect(res.items[0].localized.en_GB.buttonImage).to.be.an.object();
			testingTools.code.expect(res.items[0].localized.en_GB.backgroundImage).to.be.an.object();

			done();
		});
	});

	lab.test('should work with exact action->type `slideshow` and non empty slideshowImages', function (done) {
		services.getHotelGroup = function (hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					mainMenu: {
						items: [{
							action: {
								type: 'slideshow',
								navigation: {
									viewType: ''
								}
							},
							localized: {
								en_GB: {
									slideshowImages: [{
										'source': {
											'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
											'etag': '247b22a529510c0b0a19b4b277f7e7f6',
											'tags': [],
											'type': 'upload',
											'bytes': 25443,
											'width': 860,
											'format': 'png',
											'height': 230,
											'version': 1434352636,
											'public_id': 'devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000',
											'signature': '8bc39849de1189ac0135a7d04b73bad51f1579a0',
											'created_at': '2015-06-15T07:17:16Z',
											'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
											'isCloudinary': true,
											'resource_type': 'image',
											'original_filename': 'image'
										}
									}],
									buttonImage: {
										'source': {
											'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
											'etag': '247b22a529510c0b0a19b4b277f7e7f6',
											'tags': [],
											'type': 'upload',
											'bytes': 25443,
											'width': 860,
											'format': 'png',
											'height': 230,
											'version': 1434352636,
											'public_id': 'devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000',
											'signature': '8bc39849de1189ac0135a7d04b73bad51f1579a0',
											'created_at': '2015-06-15T07:17:16Z',
											'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
											'isCloudinary': true,
											'resource_type': 'image',
											'original_filename': 'image'
										}
									},
									backgroundImage: {
										'source': {
											'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
											'etag': '247b22a529510c0b0a19b4b277f7e7f6',
											'tags': [],
											'type': 'upload',
											'bytes': 25443,
											'width': 860,
											'format': 'png',
											'height': 230,
											'version': 1434352636,
											'public_id': 'devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000',
											'signature': '8bc39849de1189ac0135a7d04b73bad51f1579a0',
											'created_at': '2015-06-15T07:17:16Z',
											'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
											'isCloudinary': true,
											'resource_type': 'image',
											'original_filename': 'image'
										}
									}
								}
							},
							children: ''
						}],
						customItems: {}
					},
					mediaConfiguration: {
						hotelGroupId: testingTools.getHotelGroupId()
					},
					info: {
						localized: {
							en_GB: {
								backgroundImage: {
									'source': {
										'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
										'etag': '247b22a529510c0b0a19b4b277f7e7f6',
										'tags': [],
										'type': 'upload',
										'bytes': 25443,
										'width': 860,
										'format': 'png',
										'height': 230,
										'version': 1434352636,
										'public_id': 'devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000',
										'signature': '8bc39849de1189ac0135a7d04b73bad51f1579a0',
										'created_at': '2015-06-15T07:17:16Z',
										'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1434352636/devel-i/group-app/test-hgid/main_menu/mm-ss/image1434090152000.png',
										'isCloudinary': true,
										'resource_type': 'image',
										'original_filename': 'image'
									}
								}
							}
						}
					}
				}
			});
		};
		services.getMainMenu(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['items', 'customItems']);
			testingTools.code.expect(res.items).to.be.an.array();
			testingTools.code.expect(res.items).to.have.length(1);
			testingTools.code.expect(res.items[0]).to.be.an.object();
			testingTools.code.expect(res.items[0]).to.include(['action', 'localized', 'children']);
			testingTools.code.expect(res.items[0].localized).to.be.an.object();
			testingTools.code.expect(res.items[0].localized).to.include('en_GB');
			testingTools.code.expect(res.items[0].localized.en_GB).to.be.an.object();
			testingTools.code.expect(res.items[0].localized.en_GB).to.include(['slideshowImages', 'buttonImage', 'backgroundImage']);
			testingTools.code.expect(res.items[0].localized.en_GB.slideshowImages).to.be.an.array().and.have.length(1);
			testingTools.code.expect(res.items[0].localized.en_GB.slideshowImages[0]).to.be.an.object();
			testingTools.code.expect(res.items[0].localized.en_GB.buttonImage).to.be.an.object();
			testingTools.code.expect(res.items[0].localized.en_GB.backgroundImage).to.be.an.object();

			done();
		});
	});

	lab.test('should work with empty mainMenu->items and with mainMenu->customItems and with missing localized', function (done) {
		services.getHotelGroup = function (hgId, cb) {
			cb(null, {
				data: {
					mainMenu: {
						items: [],
						customItems: {
							search: {
								action: {
									type: '',
									navigation: {
										path: '',
										query: {}
									}
								},
								children: ''
							}
						}
					}
				}
			});
		};
		services.getMainMenu(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['items']);
			done();
		});
	});
	lab.test('should work with missing customItems and localized', function (done) {
		services.getHotelGroup = function (hgId, cb) {
			cb(null, {
				data: {
					mainMenu: {
						items: []
					}
				}
			});
		};
		services.getMainMenu(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['items']);
			done();
		});
	});

	lab.test('should work with empty mainMenu->items and with mainMenu->customItems and with missing localized', function (done) {
		services.getHotelGroup = function (hgId, cb) {
			cb(null, {
				data: {
					meta: {},
					mainMenu: {
						items: [],
						customItems: {
							search: {
								action: {
									type: '',
									navigation: {
										path: '',
										query: {}
									}
								},
								children: ''
							}
						}
					}
				}
			});
		};
		services.getMainMenu(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['items']);
			done();
		});
	});

	lab.test('should given error with invalid getHotelGroup response', function (done) {
		services.getHotelGroup = function (hgId, cb) {
			cb(new Error('Error'));
		};
		services.getMainMenu(testingTools.getHotelGroupId(), function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error');
			done();
		});
	});
});

lab.experiment('hotels_groups.services.createHotelGroup', function () {
	var hotelGroupOrig;
	var hgId = 'test-666-hgid';
	var liveTable = 'hotel_group';


	lab.before(function (done) {
		htlGData.hotelGroupId = 'some-very-random-htlgroup-forevent-test';
		eventUtils.emitter.removeAllListeners('hotelGroup.created');
		eventUtils.emitter.removeAllListeners('hotelGroup.beforeCreate');
		testingTools.enableEvents();
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query1 = escape("SELECT data FROM %I WHERE (data->'hotelGroupId') = '\"test-hgid\"'", liveTable);
			client.query(query1, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				hotelGroupOrig = JSON.stringify(res.rows[0]).replace(/test-hgid/g, hgId);
				hotelGroupOrig = JSON.parse(hotelGroupOrig);
				hotelGroupOrig.data.meta.guid = hgId;
				doneDb();
				done();
			});
		});

	});

	lab.after(function (done) {
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			client.query(
				"DELETE FROM hotel_group_v WHERE (data -> 'hotelGroupId') = $1",
				['"' + hgId + '"'],
				function (err) {
					testingTools.code.expect(err).to.be.null();
					doneDb();
					testingTools.disableEvents();
					done();
				}
			);
		});


	});

	lab.test('should given error with invalid beforeCreate listener response', function (done) {
		var c1 = 0;
		var data = JSON.parse(JSON.stringify(hotelGroupOrig.data));

		eventUtils.emitter.on('hotelGroup.beforeCreate', function (hotelGroupId, cb) {
			c1++;
			if (c1 === 1) {
				cb(new Error('Error listener hotelGroup.beforeCreate'));
			} else {
				cb(false);
			}

		});

		services.createHotelGroup(data, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error listener hotelGroup.beforeCreate');
			testingTools.code.expect(c1).to.equal(1);
			done();
		});
	});
	lab.test('hotelGroup.created event should give error', function (done) {
		var data = JSON.parse(JSON.stringify(hotelGroupOrig.data));
		eventUtils.emitter.on('hotelGroup.created', function (hotelGroupId, cb) {
			cb(new Error('Error listener hotelGroup.created'));
		});
		services.createHotelGroup(data, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error listener hotelGroup.created');
			done();
		});
	});

});

lab.experiment('hotels_groups.services.versioning', function () {

	var table = 'hotel_group_v';
	var liveTable = table.substring(0, table.length - 2);
	var hgId = 'test-service-update78';
	var hotelGroupOrig;
	var hotelGroup;


	lab.before(function (done) {
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query1 = escape("SELECT data FROM %I WHERE (data->'hotelGroupId') = '\"test-hgid\"'", liveTable);
			client.query(query1, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				hotelGroupOrig = JSON.stringify(res.rows[0]).replace(/test-hgid/g, hgId);
				hotelGroupOrig = JSON.parse(hotelGroupOrig);
				hotelGroupOrig.data.meta.guid = hgId;
				doneDb();
				done();
			});
		});


	});

	lab.beforeEach(function (done) {
		hotelGroup = JSON.parse(JSON.stringify(hotelGroupOrig.data));
		done();
	});

	lab.after(function (done) {
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			client.query(
				"DELETE FROM hotel_group_v WHERE (data -> 'hotelGroupId') = $1",
				['"' + hgId + '"'],
				function (err) {
					testingTools.code.expect(err).to.be.null();
					doneDb();
					done();
				}
			);
		});
	});
	lab.test('Add hotelGroup should work', function (done) {
		services.createHotelGroup(hotelGroup, function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.be.equal(1);

			databaseUtils.getConnection(function (err, client, doneDb) {
				testingTools.code.expect(err).to.be.null();
				client.query('SELECT * ' +
					'FROM hotel_group_v ' +
					"WHERE (data -> 'hotelGroupId') = $1",
					['"' + hotelGroup.hotelGroupId + '"'], function (err, res) {
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(res.rows.length).to.be.above(0);
						doneDb();
						done();
					});
			});
		});
	});
	lab.test('With non valid data, should not work', function (done) {
		delete hotelGroup.info;
		services.createHotelGroup(hotelGroup, function (err) {
			testingTools.expectError(err, 'hgv-key-miss');
			done();
		});
	});

});


lab.experiment('hotel_groups.services.versioning.noDb', function () {
	var hgId = 'test-service-add3';
	var _create;

	var table = 'hotel_group_v';
	var liveTable = table.substring(0, table.length - 2);
	var hotelGroupOrig;
	var hotelGroup;

	lab.before(function (done) {
		_create = version.addItem;
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query1 = escape("SELECT data FROM %I WHERE (data->'hotelGroupId') = '\"test-hgid\"'", liveTable);
			client.query(query1, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				hotelGroupOrig = JSON.stringify(res.rows[0]).replace(/test-hgid/g, hgId);
				hotelGroupOrig = JSON.parse(hotelGroupOrig);
				hotelGroupOrig.data.meta.guid = hgId;
					doneDb();
					done();
			});
		});





	});
	lab.after(function (done) {
		version.addItem = _create;
		done();
	});
	lab.test('Should give an error with invalid database response', function (done) {
		hotelGroup = JSON.parse(JSON.stringify(hotelGroupOrig.data));
		version.addItem = function (data, table, cb) {
			cb(new Error('Errorasdasdd'));
		};
		services.createHotelGroup(hotelGroup, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Errorasdasdd');
			done();
		});
	});
});

lab.experiment('hotels_groups.services.createTestHotelGroup', function () {
	var f1, f2, f3;
	lab.before(function (done) {
		f1 = services.createHotelGroup;
		f2 = dao.checkIfExists;
		f3 = services.updateHotelGroup;
		done();
	});

	lab.afterEach(function (done) {
		services.createHotelGroup = f1;
		dao.checkIfExists = f2;
		services.updateHotelGroup = f3;
		done();
	});

	lab.test('should give error with invalid data - means validation is applied', function (done) {
		var data = {};

		services.createTestHotelGroup(data, function (err) {
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('hotelGroupId');
			done();
		});
	});

	lab.test('should not give validation error if data OK', function (done) {

		services.createHotelGroup = function () {
			arguments[arguments.length - 1](null, 'aadsasda');
		};
		dao.checkIfExists = function () {
			arguments[arguments.length - 1](null, false);
		};

		services.getHotelGroup(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			services.createTestHotelGroup(res.data, function (err, res) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(res).to.equal('aadsasda');
				done();
			});
		});

	});

	lab.test('should not complain about existing item and should update instead', function (done) {

		services.updateHotelGroup = function () {
			arguments[arguments.length - 1](null, 'aadsa3333sda');
		};

		services.getHotelGroup(testingTools.getHotelGroupId(), function (err, res) {
			testingTools.code.expect(err).to.be.null();
			services.createTestHotelGroup(res.data, function (err, res) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(res).to.equal('aadsa3333sda');
				done();
			});
		});

	});

});

lab.experiment('hotelGroup.Publish', function () {
	var liveTable = 'hotel_group';
	var daoPublish, f2;
	var now = new Date();
	var yesterday = now.setDate(now.getDate() - 1);
	var hotelGroupId = 'test-service-publish2';
	var hotelGroup = {
		data: {
			meta: {
				'createdAt': now,
				'scheduledAt': new Date(yesterday).toISOString(),
				'appliedAt': null,
				'status': 'published',
				'guid': hotelGroupId
			},
			'hotelGroupId': hotelGroupId
		}
	};
	lab.before(function (done) {
		eventUtils.emitter.removeAllListeners('hotelGroup.published');
		testingTools.enableEvents();
		f2 = version.updateVersion;
		done();
	});
	lab.beforeEach(function (done) {
		daoPublish = dao.publishHotelGroup;
		version.updateVersion = f2;
		done();
	});
	lab.afterEach(function (done) {
		dao.publishHotelGroup = daoPublish;
		done();
	});
	lab.after(function (done) {
		testingTools.disableEvents();
		//Clean up test data in versioning (live table should be cleared by tests)
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape("DELETE FROM %I WHERE (data->'hotelGroupId') = '%I'", liveTable, hotelGroupId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				doneDb();
				done();
			});
		});
	});
	lab.test('Publish should go well', function (done) {
		services.publishHotelGroup(hotelGroup, function (err) {
			testingTools.code.expect(err).to.be.null();
			databaseUtils.getConnection(function (err, client, doneDb) {
				testingTools.code.expect(err).to.be.null();
				var query = escape("SELECT * FROM %I WHERE (data->'hotelGroupId') = '%I'",
					liveTable, hotelGroupId);
				client.query(query, function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.equal(1);
					doneDb();
					done();
				});
			});
		});
	});
	lab.test('Dao publish item should give one tiny sweet failure', function (done) {
		dao.publishHotelGroup = function (item, cb) {
			cb(new Error('daopublishfail'));
		};
		services.publishHotelGroup(hotelGroup, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('daopublishfail');
			done();
		});
	});

	lab.test('version.updateVersion should give error', function (done) {
		version.updateVersion = function (item, tbl, cb) {
			cb(new Error('23423adsf42323'));
		};
		services.publishHotelGroup(hotelGroup, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('23423adsf42323');
			done();
		});
	});
	lab.test('Emitter should hit right between the eyes', function (done) {
		eventUtils.emitter.on('hotelGroup.published', function (hgid, cb) {
			cb(new Error('liveeventfailure'));
		});
		services.publishHotelGroup(hotelGroup, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('liveeventfailure');
			done();
		});
	});
});
lab.experiment('hotelGroup.UnPublish', function () {
	var liveTable = 'hotel_group';
	var daoPublish, f2;
	var now = new Date();
	var yesterday = now.setDate(now.getDate() - 1);
	var hotelGroupId = 'test-service-unpublish2';
	var hotelGroup = {
		data: {
			meta: {
				'createdAt': now,
				'scheduledAt': new Date(yesterday).toISOString(),
				'appliedAt': null,
				'status': 'draft',
				'guid': hotelGroupId
			},
			'hotelGroupId': hotelGroupId
		}
	};
	lab.before(function (done) {
		f2 = version.updateVersion;
		eventUtils.emitter.removeAllListeners('hotelGroup.unpublished');
		testingTools.enableEvents();
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', liveTable, JSON.stringify(hotelGroup.data));
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rows[0].id).to.be.above(0);
				doneDb();
				done();
			});
		});
	});
	lab.beforeEach(function (done) {
		daoPublish = dao.unPublishHotelGroup;
		done();
	});
	lab.afterEach(function (done) {
		dao.unPublishHotelGroup = daoPublish;
		version.updateVersion = f2;
		done();
	});
	lab.after(function (done) {
		testingTools.disableEvents();
		//Clean up test data in versioning (live table should be cleared by tests)
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape("DELETE FROM %I WHERE (data->'hotelGroupId') = '%I'", liveTable, hotelGroupId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(0);
				doneDb();
				done();
			});
		});
	});
	lab.test('UnPublish should go well', function (done) {
		services.unPublishItem(hotelGroup, function (err) {
			testingTools.code.expect(err).to.be.null();
			databaseUtils.getConnection(function (err, client, doneDb) {
				testingTools.code.expect(err).to.be.null();
				var query = escape("SELECT * FROM %I WHERE (data->'hotelGroupId') = '%I'",
					liveTable, hotelGroupId);
				client.query(query, function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.equal(0);
					doneDb();
					done();
				});
			});
		});
	});
	lab.test('Dao unpublish item should give one tiny sweet failure', function (done) {
		dao.unPublishHotelGroup = function (item, cb) {
			cb(new Error('daopublishfail'));
		};
		services.unPublishItem(hotelGroup, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('daopublishfail');
			done();
		});
	});

	lab.test('version.updateVersion should give one tiny sweet failure', function (done) {
		version.updateVersion = function (item, tbl, cb) {
			cb(new Error('version.updateVersionErr'));
		};
		services.unPublishItem(hotelGroup, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('version.updateVersionErr');
			done();
		});
	});
	lab.test('Emitter should hit right between the eyes', function (done) {
		eventUtils.emitter.on('hotelGroup.unpublished', function (hgid, cb) {
			cb(new Error('deleteeventfailure'));
		});
		services.unPublishItem(hotelGroup, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('deleteeventfailure');
			done();
		});
	});
});

lab.experiment('Hotelgroup update', function () {
	var e;
	var table = 'hotel_group_v';
	var liveTable = table.substring(0, table.length - 2);
	var hgId = 'test-service-update';
	var hotelGroupOrig;
	var hotelGroup;


	lab.before(function (done) {
		e = new Error('update event failure');

		eventUtils.emitter.removeAllListeners('hotelGroup.updated');
		eventUtils.emitter.removeAllListeners('hotelGroup.beforeUpdate');
		testingTools.enableEvents();

		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query1 = escape("SELECT data FROM %I WHERE (data->'hotelGroupId') = '\"test-hgid\"'", liveTable);
			client.query(query1, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				hotelGroupOrig = JSON.stringify(res.rows[0]).replace(/test-hgid/g, hgId);
				hotelGroupOrig = JSON.parse(hotelGroupOrig);
				hotelGroupOrig.data.meta.guid = hgId;
				var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', table, JSON.stringify(hotelGroupOrig.data));
				client.query(query, function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rows[0].id).to.be.above(0);
					doneDb();
					done();
				});
			});
		});
	});

	lab.beforeEach(function (done) {
		hotelGroup = JSON.parse(JSON.stringify(hotelGroupOrig));
		done();
	});

	lab.after(function (done) {
		testingTools.disableEvents();
		//Clean up test data in versioning
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape("DELETE FROM %I WHERE (data->'hotelGroupId') = '%I'", table, hgId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(3);
				doneDb();
				done();
			});
		});
	});

	lab.test('Should get no change error', function (done) {
		services.updateHotelGroup(hotelGroup.data, function (err) {
			testingTools.expectError(err, 'ver-no-change');
			done();
		});
	});
	lab.test('Should go well', function (done) {
		hotelGroup.data.meta.status = 'draft';
		services.updateHotelGroup(hotelGroup.data, function (err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should given error with invalid beforeUpdate listener response', function (done) {
		var c1 = 0;

		eventUtils.emitter.on('hotelGroup.beforeUpdate', function (hotelGroupId, cb) {
			c1++;
			if (c1 === 1) {
				cb(new Error('Error listener hotelGroup.beforeUpdate'));
			} else {
				cb(false);
			}
		});

		services.updateHotelGroup(hotelGroup.data, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error listener hotelGroup.beforeUpdate');
			testingTools.code.expect(c1).to.equal(1);
			done();
		});
	});

	lab.test('Some event listener should fail', function (done) {
		hotelGroup.data.meta.status = 'published';
		eventUtils.emitter.once('hotelGroup.updated', function (data, callback) {
			callback(e);
		});
		hotelGroup.data.meta.status = 'published';
		services.updateHotelGroup(hotelGroup.data, function (err) {
			testingTools.code.expect(err, JSON.stringify(err)).to.equal(e);
			done();
		});
	});
	lab.test('Waiting for data validation error', function (done) {
		delete hotelGroup.data.info;
		services.updateHotelGroup(hotelGroup.data, function (err) {
			testingTools.expectError(err, 'hgv-key-miss');
			done();
		});
	});
});
lab.experiment('hotels_groups.updateHotelGroupCatIDs', function () {

	lab.test('should fail with wrong hotelGroupId', function(done){
		services.updateHotelGroupCatIDs('test-hotel-id', {}, function(err){
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});

	lab.test('should not add category that already exists', function(done){
		services.getHotelGroup('test-hgid', function(err, data){

			testingTools.code.expect(err).to.be.null();


			services.updateHotelGroupCatIDs('test-hgid', [data.data.categories[data.data.categories.length - 1]], function(e, json){
				testingTools.code.expect(e).to.be.null();
				testingTools.code.expect(json.data.categories).to.deep.equal(data.data.categories);

				done();
			});
		});

	});

	lab.test('should work and modify hotelgroup categoryids', function(done){
		var newCategory = [{
			'type': 'newType',
			'categoryId': 'newCategory',
			'name': 'newName',
			'localized': {}
		}];

		services.updateHotelGroupCatIDs('test-hgid', newCategory, function (err, json) {

			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(json.data.categories[json.data.categories.length - 1].type).to.be.equal('newType');
			testingTools.code.expect(json.data.categories[json.data.categories.length - 1].categoryId).to.be.equal('newCategory');
			done();
		});
	});
});
