'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var validation = require('../validation');
var langUtils = require('../../utils/langUtils');
var q = require('q');

lab.experiment('HotelGroups.validation.validateHotelGroupData generally', function() {

	lab.test('should not accept invalid data type', function(done) {
		validation.validateHotelGroupData(123, 'full', function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			done();
		});
	});

	lab.test('should not accept invalid validation type', function(done) {
		validation.validateHotelGroupData({}, 'asdasdas', function(err){
			testingTools.expectError(err, 'hgv-inv-val-type');
			done();
		});
	});

});

lab.experiment('HotelGroups.validation.validateHotelGroupData with \'full\' validation', function() {

	var getValidData;


	var validationType = 'full';
	var f1, f2;

	lab.before(function(done){
		f1 = langUtils.getDefaultTranslationKeys;
		f2 = langUtils.mergeTranslationsWithDefault;
		//create function to get valid hotelgroup data sample
		getValidData = function(){
			var imagePrefix = process.env.CLOUDINARY_BASE_FOLDER;
			var hgId = 'asdasd';
			var data = {
				hotelGroupId: hgId,
				viewConfiguration: {
					hotelMenu: [{
						action: 'web',
						removeIfDisabled: true
					}, {
						action: 'phone',
						removeIfDisabled: true
					}, {
						action: 'map',
						removeIfDisabled: true
					}, {
						action: 'share',
						removeIfDisabled: true
					}, {
						action: 'book',
						removeIfDisabled: true
					}],
					'profileMenu': [{
						action: 'settings',
						removeIfDisabled: true
					}, {
						action: 'languages',
						removeIfDisabled: true
					}, {
						action: 'signOut',
						removeIfDisabled: true
					}, {
						action: 'conditions',
						removeIfDisabled: true
					}, {
						action: 'signIn',
						removeIfDisabled: true
					}, {
						action: 'signUp',
						removeIfDisabled: true
					}]
				},
				supportedLanguages: ['en_GB', 'et', 'ru_RU'],
				defaultLanguage: 'en_GB',
				translations: {localized: {en_GB: {}}},
				categories: [
					{
						'type': 'regions',
						'localized': {
							'en_GB': {'name': 'Germany'}
						},
						'categoryId': 'reg-eur-ger',
						'buttonImage': {
							'source': {
								'public_id': imagePrefix + '/group-app/' + hgId + '/main_menu/mm-all/image1429258905000',
								'isCloudinary': true,
								'resource_type': 'image'
							}
						},
						'backgroundImage': {
							'source': {
								'public_id': imagePrefix + '/group-app/' + hgId + '/main_menu/mm-all/image1429258905000',
								'isCloudinary': true,
								'resource_type': 'image'
							}
						}
					},
					{
					'type': 'regions',
					'localized': {
						'en_GB': {'name': 'Cologne'},
						'et_EE': {'name': 'Köln'},
						'ru_RU': {'name': 'Кёльн'}
					},
					'categoryId': 'reg-eur-ger-col',
					'parentCategoryId': 'reg-eur-ger',
					'buttonImage': {
						'source': {
							'public_id': imagePrefix + '/group-app/' + hgId + '/main_menu/mm-all/image1429258905000',
							'isCloudinary': true,
							'resource_type': 'image'
						}
					},
					'backgroundImage': {
						'source': {
							'public_id': imagePrefix + '/group-app/' + hgId + '/main_menu/mm-all/image1429258905000',
							'isCloudinary': true,
							'resource_type': 'image'
						}
					}
				}],
				mainMenu: {
					items: [
						{
							menuItemId: 'menu-1',
							action: {
								type: 'navigation',
								navigation: {
									viewType: 'someViewThatDevicesKnow',
									path: ''
								}
							},
							children: [
								{
									menuItemId: 'menu-11',
									action: {
										type: 'navigation',
										navigation: {
											viewType: 'someViewThatDevicesKnow',
											path: ''
										}
									},
									children: [],
									localized: {
										en_GB: {}
									},
									name: 'blabla',
									'buttonImage': {
										'source': {
											'public_id': imagePrefix + '/group-app/' + hgId + '/main_menu/mm-all/image1429258905000',
											'isCloudinary': true,
											'resource_type': 'image'
										}
									},
									'backgroundImage': {
										'source': {
											'public_id': imagePrefix + '/group-app/' + hgId + '/main_menu/mm-all/image1429258905000',
											'isCloudinary': true,
											'resource_type': 'image'
										}
									}
								}
							],
							localized: {
								en_GB: {}
							},
							name: 'blabla',
							'buttonImage': {
								'source': {
									'public_id': imagePrefix + '/group-app/' + hgId + '/main_menu/mm-all/image1429258905000',
									'isCloudinary': true,
									'resource_type': 'image'
								}
							},
							'backgroundImage': {
								'source': {
									'public_id': imagePrefix + '/group-app/' + hgId + '/main_menu/mm-all/image1429258905000',
									'isCloudinary': true,
									'resource_type': 'image'
								}
							}
						},
						{
							menuItemId: 'menu-ss',
							action: {
								type: 'slideshow',
								navigation: {
									viewType: 'someViewThatDevicesKnow'
								}
							},
							localized: {
								en_GB: {
									slideshowImages: [{
										source: {
											public_id: imagePrefix + '/group-app/' + hgId + '/main_menu/mm-ss/image1429258905000',
											isCloudinary: true,
											resource_type: 'image'
										}
									}, {
										source: {
											public_id: imagePrefix + '/group-app/' + hgId + '/main_menu/mm-ss/image1429258905000',
											isCloudinary: true,
											resource_type: 'image'
										}
									}]
								}
							},
							name: 'blabla',
							buttonImage: {
								source: {
									public_id: imagePrefix + '/group-app/' + hgId + '/main_menu/mm-all/image1429258905000',
									isCloudinary: true,
									resource_type: 'image'
								}
							},
							backgroundImage: {
								'source': {
									public_id: imagePrefix + '/group-app/' + hgId + '/main_menu/mm-all/image1429258905000',
									isCloudinary: true,
									resource_type: 'image'
								}
							},
							children: []
						}
					],
					customItems: {}
				},
				info: {
					'logo': {
						'source': {
							'public_id': imagePrefix + '/group-app/' + hgId + '/logo1423133470000',
							'isCloudinary': true,
							'resource_type': 'image'
						}
					},
					'name': 'The Leading Hotels of the World',
					'localized': {
						'en_GB': {}
					},
					'backgroundImage': {
						'source': {
							'public_id': imagePrefix + '/group-app/' + hgId + '/logo1423133470000',
							'isCloudinary': true,
							'resource_type': 'image'
						}
					}
				},
				validApiKeys: ['test-api-key'],
				mediaConfiguration: {
					'theme': {
						'primaryColor': {
							'normal': { 'r': 1, 'g': 128, 'b': 254, 'alpha': 1 },
							'highlighted': { 'r': 170, 'g': 211, 'b': 255, 'alpha': 1 },
							'selected': { 'r': 126, 'g': 189, 'b': 255, 'alpha': 1 },
							'highlightedSelected': { 'r': 126, 'g': 189, 'b': 255, 'alpha': 1 },
							'disabled': { 'r': 0, 'g': 0, 'b': 0, 'alpha': 0.25 }
						},
						'secondaryColor': {
							'normal': {'r': 20, 'g': 152, 'b': 183, 'alpha': 1},
							'highlighted': {'r': 196, 'g': 229, 'b': 237, 'alpha': 1},
							'selected': {'r': 139, 'g': 203, 'b': 219, 'alpha': 1},
							'highlightedSelected': {'r': 139, 'g': 203, 'b': 219, 'alpha': 1},
							'disabled': {'r': 0, 'g': 0, 'b': 0, 'alpha': 0.25}
						},
						'primaryTextColor': {
							'normal': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlighted': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'selected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlightedSelected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'disabled': {'r': 255, 'g': 255, 'b': 255, 'alpha': 0.5}
						},
						'secondaryTextColor': {
							'normal': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlighted': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'selected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlightedSelected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'disabled': {'r': 255, 'g': 255, 'b': 255, 'alpha': 0.5}
						},
						'backgroundColor': {
							'normal': {'r': 0, 'g': 0, 'b': 0, 'alpha': 0.9},
							'highlighted': {'r': 126, 'g': 189, 'b': 255, 'alpha': 1},
							'selected': {'r': 1, 'g': 128, 'b': 254, 'alpha': 1},
							'highlightedSelected': {'r': 1, 'g': 128, 'b': 254, 'alpha': 1},
							'disabled': {'r': 0, 'g': 0, 'b': 0, 'alpha': 0.25}
						},
						'inputColor': {
							'normal': {'r': 255, 'g': 255, 'b': 255, 'alpha': 0.9},
							'highlighted': {'r': 226, 'g': 226, 'b': 226, 'alpha': 1},
							'selected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlightedSelected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'disabled': {'r': 255, 'g': 255, 'b': 255, 'alpha': 0.25}
						},
						'backgroundTextColor': {
							'normal': {'r': 227, 'g': 227, 'b': 227, 'alpha': 1},
							'highlighted': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'selected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlightedSelected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'disabled': {'r': 255, 'g': 255, 'b': 255, 'alpha': 0.5}
						},
						'inputTextColor': {
							'normal': {'r': 60, 'g': 60, 'b': 60, 'alpha': 1},
							'highlighted': {'r': 60, 'g': 60, 'b': 60, 'alpha': 1},
							'selected': {'r': 60, 'g': 60, 'b': 60, 'alpha': 1},
							'highlightedSelected': {'r': 60, 'g': 60, 'b': 60, 'alpha': 1},
							'disabled': {'r': 0, 'g': 0, 'b': 0, 'alpha': 0.25}
						},
						'primaryImageTextColor': {
							'normal': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlighted': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'selected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlightedSelected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'disabled': {'r': 255, 'g': 255, 'b': 255, 'alpha': 0.5}
						},
						'secondaryImageTextColor': {
							'normal': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlighted': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'selected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlightedSelected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'disabled': {'r': 255, 'g': 255, 'b': 255, 'alpha': 0.5}
						},
						'backgroundSubtextColor': {
							'normal': {'r': 1, 'g': 128, 'b': 254, 'alpha': 1},
							'highlighted': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'selected': {'r': 126, 'g': 189, 'b': 255, 'alpha': 1},
							'highlightedSelected': {'r': 126, 'g': 189, 'b': 255, 'alpha': 1},
							'disabled': {'r': 255, 'g': 255, 'b': 255, 'alpha': 0.5}
						},
						'heroTextColor': {
							'normal': {'r': 253, 'g': 221, 'b': 166, 'alpha': 1},
							'highlighted': {'r': 170, 'g': 211, 'b': 255, 'alpha': 1},
							'selected': {'r': 20, 'g': 152, 'b': 183, 'alpha': 1},
							'highlightedSelected': {'r': 20, 'g': 152, 'b': 183, 'alpha': 1},
							'disabled': {'r': 255, 'g': 255, 'b': 255, 'alpha': 0.5}
						},
						'heroSubtextColor': {
							'normal': {'r': 227, 'g': 227, 'b': 227, 'alpha': 1},
							'highlighted': {'r': 60, 'g': 60, 'b': 60, 'alpha': 1},
							'selected': {'r': 60, 'g': 60, 'b': 60, 'alpha': 1},
							'highlightedSelected': {'r': 60, 'g': 60, 'b': 60, 'alpha': 1},
							'disabled': {'r': 0, 'g': 0, 'b': 0, 'alpha': 0.25}
						},
						'placeholderTextColor': {
							'normal': {'r': 1, 'g': 128, 'b': 254, 'alpha': 1},
							'highlighted': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'selected': {'r': 1, 'g': 128, 'b': 254, 'alpha': 1},
							'highlightedSelected': {'r': 1, 'g': 128, 'b': 254, 'alpha': 1},
							'disabled': {'r': 0, 'g': 0, 'b': 0, 'alpha': 0.25}
						},
						'progressIndicatorColor': {
							'normal': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlighted': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'selected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlightedSelected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'disabled': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1}
						},
						'primaryIconColor': {
							'normal': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlighted': {'r': 170, 'g': 211, 'b': 255, 'alpha': 1},
							'selected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlightedSelected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'disabled': {'r': 255, 'g': 255, 'b': 255, 'alpha': 0.5}
						},
						'secondaryIconColor': {
							'normal': {'r': 1, 'g': 127, 'b': 253, 'alpha': 1},
							'highlighted': {'r': 170, 'g': 211, 'b': 255, 'alpha': 1},
							'selected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'highlightedSelected': {'r': 255, 'g': 255, 'b': 255, 'alpha': 1},
							'disabled': {'r': 255, 'g': 255, 'b': 255, 'alpha': 0.5}
						}
					},
					'defaults': {
						'bg': {
							'crop': 'fill',
							'transformations': ['grayscale']
						},
						'hgLogo': {
							'crop': 'pad',
							'transformations': []
						},
						'menuBtn': {
							'crop': 'fill',
							'transformations': []
						},
						'hotelBtn': {
							'crop': 'fill',
							'transformations': []
						},
						'hotelGal': {
							'crop': 'fill',
							'transformations': []
						},
						'categoryBg': {
							'crop': 'fill',
							'transformations': ['vignette']
						},
						'menuPageBg': {
							'crop': 'fill',
							'transformations': ['vignette']
						},
						'categoryBtn': {
							'crop': 'fill',
							'transformations': []
						}
					},
					'extraAssets': [
						{
							'id': 'extra_assets/bg_overlay',
							'source': {
								'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1429539752/indrek-dev/group-app/test-hgid/extra_assets/bg_overlay1425885582000.png',
								'etag': 'bda1844d0fa29aa164c5bf3ecf960c06',
								'tags': [],
								'type': 'upload',
								'bytes': 227854,
								'width': 2048,
								'format': 'png',
								'height': 1536,
								'version': 1429539752,
								'public_id': imagePrefix + '/group-app/' + hgId + '/extra_assets/bg_overlay1425885582000',
								'signature': '08265a41f8ca66292b1eadf796538c18739004b2',
								'created_at': '2015-04-20T14:22:32Z',
								'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1429539752/indrek-dev/group-app/test-hgid/extra_assets/bg_overlay1425885582000.png',
								'isCloudinary': true,
								'resource_type': 'image'
							}
						}
					],
					'placeholderImage': {
						'source': {
							'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1429539753/indrek-dev/group-app/test-hgid/placeholder1423133470000.png',
							'etag': '8d81fcaac954891fb25e3f41e2b2f565',
							'tags': [],
							'type': 'upload',
							'bytes': 8004,
							'width': 480,
							'format': 'png',
							'height': 360,
							'version': 1429539753,
							'public_id': imagePrefix + '/group-app/' + hgId + '/placeholder1423133470000',
							'signature': '48b6415cd6dff1fc6f7563fea041f4c957db0bfc',
							'created_at': '2015-04-20T14:22:33Z',
							'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1429539753/indrek-dev/group-app/test-hgid/placeholder1423133470000.png',
							'isCloudinary': true,
							'resource_type': 'image'
						}
					},
					'predefinedTransformations': {
						'vignette': [
							{
								'crop': 'scale',
								'flags': 'relative',
								'width': '1.0',
								'effect': 'multiply',
								'height': '1.0',
								'overlay': 'extra_assets/bg_overlay'
							}
						],
						'grayscale': [
							{
								'effect': 'grayscale',
								'opacity': 50,
								'background': '#000'
							}
						]
					}
				},
				profilesConfiguration: null,
				emailTemplates: {
					emailVerification: {
						localized: {
							en_GB: {
								fromEmail: 'from_email@example.com',
								fromName: 'Example Name',
								subject: 'example subject {{var2}}',
								bodyHtml: '<div>example code {{var1}}</div>'
							}
						}
					},
					passwordReset: {
						localized: {
							en_GB: {
								fromEmail: 'from_email@example.com',
								fromName: 'Example Name',
								subject: 'example subject {{var2}}',
								bodyHtml: '<div>example code {{var1}}</div>'
							}
						}
					}
				},
				searchConfiguration: {
					resultOrdering: 'random'
				}
			};

			return JSON.parse(JSON.stringify(data));
		};

		done();
	});

	lab.afterEach(function(done){
		langUtils.getDefaultTranslationKeys = f1;
		langUtils.mergeTranslationsWithDefault = f2;
		done();
	});

	lab.test('Should be ok without result ordering', function(done){
		var data2 = getValidData();
		delete data2.searchConfiguration.resultOrdering;
		validation.validateHotelGroupData(data2, 'full', function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('Should be NOT ok with wrong result ordering', function(done){
		var data2 = getValidData();
		data2.searchConfiguration.resultOrdering='wrongonw';
		validation.validateHotelGroupData(data2, 'full', function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			done();
		});
	});

	lab.test('should give error if some root key is missing', function(done) {
		var hotelGroupData = getValidData();
		var promises = [];

		function doVal(p, data, key, errExp){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.expectError(err, errExp);
				testingTools.code.expect(err.data.debuggingData.key).to.equal(key);
				p.resolve();
			});
		}

		for(var i in hotelGroupData){
			var data = getValidData();
			var p = q.defer();
			promises.push(p.promise);
			delete data[i];
			doVal(p, data, i, 'hgv-key-miss');

		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should give error if hotelGroupId is not string', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.hotelGroupId = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('hotelGroupId');
			done();
		});
	});

	lab.test('should give error if hotelGroupId already exists', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.hotelGroupId = 'test-hgid';

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-hgId-exists');
			done();
		});
	});

	lab.test('should not give error about existing id if hotelGroupId already exists and updating', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.hotelGroupId = 'test-hgid';

		validation.validateHotelGroupData(hotelGroupData, 'update', function(err){
			testingTools.expectError(err, 'mv-src-mis-prefix');
			done();
		});
	});

	lab.test('should give error if hotelGroupId is empty string', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.hotelGroupId = '';

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('hotelGroupId');
			done();
		});
	});

	lab.test('should give error if viewConfiguration is not object', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.viewConfiguration = 33;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('viewConfiguration');
			done();
		});
	});

	lab.test('should give error if viewConfiguration items are missing', function(done) {
		var hotelGroupData = getValidData();
		var viewConfiguration = hotelGroupData.viewConfiguration;
		var promises = [];

		function doVal(p, data, key, errExp){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.expectError(err, errExp);
				testingTools.code.expect(err.data.debuggingData.key).to.equal(key);
				p.resolve();
			});
		}

		for(var i in viewConfiguration){
			var data = getValidData();
			var p = q.defer();
			promises.push(p.promise);
			delete data.viewConfiguration[i];

			doVal(p, data, 'viewConfiguration.' + i, 'hgv-key-miss');

		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should work if viewConfiguration[x] is false', function(done) {
		var hotelGroupData = getValidData();
		var viewConfiguration = hotelGroupData.viewConfiguration;
		var promises = [];

		function doVal(p, data){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.code.expect(err).to.equal(null);
				p.resolve();
			});
		}

		for(var k in viewConfiguration){
			var data = getValidData();
			var p = q.defer();
			promises.push(p.promise);
			data.viewConfiguration[k] = false;
			doVal(p, data);

		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should give error if viewConfiguration[x] is not array', function(done) {
		var hotelGroupData = getValidData();
		var viewConfiguration = hotelGroupData.viewConfiguration;
		var promises = [];

		function doVal(p, data, key, errExp){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.expectError(err, errExp);
				testingTools.code.expect(err.data.debuggingData.key).to.equal(key);
				p.resolve();
			});
		}

		for(var i in viewConfiguration){
			var data = getValidData();
			var p = q.defer();
			promises.push(p.promise);
			data.viewConfiguration[i] = 33;

			doVal(p, data, 'viewConfiguration.' + i, 'hgv-inv-type');

		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should give error if viewConfiguration[x] is empty array', function(done) {
		var hotelGroupData = getValidData();
		var viewConfiguration = hotelGroupData.viewConfiguration;
		var promises = [];

		function doVal(p, data, key, errExp){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.expectError(err, errExp);
				testingTools.code.expect(err.data.debuggingData.key).to.equal(key);
				p.resolve();
			});
		}

		for(var i in viewConfiguration){
			var data = getValidData();
			var p = q.defer();
			promises.push(p.promise);
			data.viewConfiguration[i] = [];
			doVal(p, data, 'viewConfiguration.' + i, 'hgv-inv-qty');

		}

		q.all(promises).done(function(){
			done();
		});

	});

	lab.test('should give error if viewConfiguration[x][y] is not an object', function(done) {
		var hotelGroupData = getValidData();
		var viewConfiguration = hotelGroupData.viewConfiguration;
		var promises = [];

		function doVal(p, data, key, errExp){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.expectError(err, errExp);
				testingTools.code.expect(err.data.debuggingData.key).to.equal(key);
				p.resolve();
			});
		}

		for(var i in viewConfiguration){
			for(var y in viewConfiguration[i]){
				var data = getValidData();
				var p = q.defer();
				promises.push(p.promise);
				data.viewConfiguration[i][y] = 33;

				doVal(p, data, 'viewConfiguration.' + i + '[' + y + ']', 'hgv-inv-type');

			}
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should give error if viewConfiguration[x][y] not contain action and removeIfDisabled keys', function(done) {
		var hotelGroupData = getValidData();
		var viewConfiguration = hotelGroupData.viewConfiguration;
		var promises = [];

		function doVal(p, data, key, errExp){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.expectError(err, errExp);
				testingTools.code.expect(err.data.debuggingData.key).to.equal(key);
				p.resolve();
			});
		}

		for(var i in viewConfiguration){
			for(var y in viewConfiguration[i]){
				for(var k in viewConfiguration[i][y]){
					var data = getValidData();
					var p = q.defer();
					promises.push(p.promise);
					delete data.viewConfiguration[i][y][k];
					doVal(p, data, 'viewConfiguration.' + i + '[' + y + '].' + k, 'hgv-key-miss');
				}
			}
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should give error if viewConfiguration[x][y].action is not string', function(done) {
		var hotelGroupData = getValidData();
		var viewConfiguration = hotelGroupData.viewConfiguration;
		var promises = [];

		function doVal(p, data, key, errExp){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.expectError(err, errExp);
				testingTools.code.expect(err.data.debuggingData.key).to.equal(key);
				p.resolve();
			});
		}

		for(var i in viewConfiguration){
			for(var y in viewConfiguration[i]){
				var data = getValidData();
				var p = q.defer();
				promises.push(p.promise);
				data.viewConfiguration[i][y].action = 33;
				doVal(p, data, 'viewConfiguration.' + i + '[' + y + '].action', 'hgv-inv-type');
			}
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should give error if viewConfiguration[x][y].action is empty string', function(done) {
		var hotelGroupData = getValidData();
		var viewConfiguration = hotelGroupData.viewConfiguration;
		var promises = [];

		function doVal(p, data, key, errExp){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.expectError(err, errExp);
				testingTools.code.expect(err.data.debuggingData.key).to.equal(key);
				p.resolve();
			});
		}

		for(var i in viewConfiguration) {
			for (var y in viewConfiguration[i]) {
				var data = getValidData();
				var p = q.defer();
				promises.push(p.promise);
				data.viewConfiguration[i][y].action = '';
				doVal(p, data, 'viewConfiguration.' + i + '[' + y + '].action', 'hgv-inv-val');
			}
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should give error if viewConfiguration[x][y].removeIfDisabled is not boolean', function(done) {
		var hotelGroupData = getValidData();
		var viewConfiguration = hotelGroupData.viewConfiguration;
		var promises = [];

		function doVal(p, data, key, errExp){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.expectError(err, errExp);
				testingTools.code.expect(err.data.debuggingData.key).to.equal(key);
				p.resolve();
			});
		}

		for(var i in viewConfiguration) {
			for (var y in viewConfiguration[i]) {
				var data = getValidData();
				var p = q.defer();
				promises.push(p.promise);
				data.viewConfiguration[i][y].removeIfDisabled = 33;
				doVal(p, data, 'viewConfiguration.' + i + '[' + y + '].removeIfDisabled', 'hgv-inv-type');
			}
		}

		q.all(promises).done(function(){
			done();
		});
	});

	//validate supportedLanguages
	lab.test('should give error if supportedLanguages is of incorrect type', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.supportedLanguages = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('supportedLanguages');
			done();
		});
	});

	lab.test('should give error if supportedLanguages has incorrect value', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.supportedLanguages = ['aaa'];

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('supportedLanguages[0]');
			done();
		});
	});

	lab.test('should give error if supportedLanguages has value with incorrect type', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.supportedLanguages = [1];

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('supportedLanguages[0]');
			done();
		});
	});

	//validate defaultLanguage
	lab.test('should give error if defaultLanguage is of invalid type', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.defaultLanguage = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('defaultLanguage');
			done();
		});
	});


	lab.test('should give error if defaultLanguage is not in supportedLanguages list', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.supportedLanguages = ['en', 'et', 'ru_RU'];

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-def-lan');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('defaultLanguage');
			done();
		});
	});

	//validate translations
	lab.test('should give error if translations object has incorrect data type', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.translations = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('translations');
			done();
		});
	});

	lab.test('should give error if translations object has incorrect data structure', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.translations = {};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('translations.localized');
			done();
		});
	});

	lab.test('should give error if translations object has incorrect language keys', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.translations = {localized: {estonian: {}}};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-trans-lang');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('translations.localized[estonian]');
			done();
		});
	});

	lab.test('should give error if translations object does not have default language', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.translations = {localized: {et: { bla: 'Bla bla bla' }}};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-trans-no-def');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('translations.localized');
			done();
		});
	});

	lab.test('should give error if translation is an empty string', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.translations = {localized: {en_GB: { car: ''}}};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-trans-empty');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('translations.localized[en_GB][car]');
			done();
		});
	});

	lab.test('should give error if translation is not a string', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.translations = {localized: {en_GB: { car: 1}}};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-trans-nonstr');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('translations.localized[en_GB][car]');
			done();
		});
	});

	lab.test('should give error if translations merge fails', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.translations = {localized: {en_GB: { car: 'adsds'}}};
		langUtils.mergeTranslationsWithDefault = function(){
			arguments[arguments.length-1](new Error('asdad4fe'));
		};
		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('asdad4fe');
			done();
		});
	});

	lab.test('should give error if translation has invalid template', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.translations = {localized: {en_GB: { car: 'adsds {{ad}'}}};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'lu-vdt-inv-tpl');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('translations.localized[en_GB][car]');
			done();
		});
	});

	//validate categories
	lab.test('should give error if categories has invalid type', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.categories = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('categories');
			done();
		});
	});

	lab.test('should give error if a category is of invalid type', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.categories = [
			1
		];

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'cv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('categories[0]');
			done();
		});
	});

	lab.test('should give error if category has an invalid structure', function(done) {
		
		var hotelGroupData = getValidData();
		hotelGroupData.categories = [
			{}
		];

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.code).to.include('cv-');
			testingTools.code.expect(err.data.debuggingData.key).to.include(['categories[0]']);
			done();
		});
	});

	lab.test('should give error if a category has no default language translation', function(done) {
		
		var hotelGroupData = getValidData();
		hotelGroupData.categories = [
			{
				categoryId: 'cat-1',
				type: 'blabla',
				localized: {}
			}
		];

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'cv-cat-no-defl');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('categories[0].localized');
			done();
		});
	});


	lab.test('should give error if a categoryId\'s are not unique', function(done) {
		
		var hotelGroupData = getValidData();
		hotelGroupData.categories[1].categoryId = hotelGroupData.categories[0].categoryId;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-dbl-catid');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('categories[1].categoryId');
			done();
		});
	});

	lab.test('should give error if parentCategoryId does not exist', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.categories[0].parentCategoryId = 'cat-0';

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-parcat');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('categories[0].parentCategoryId');
			done();
		});
	});

	lab.test('should not give error if no categories', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.categories = [];

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	//validate main menu
	lab.test('should give error if a mainMenu is of invalid type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu');
			done();
		});
	});

	lab.test('should give error if a mainMenu is missing items', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items');
			done();
		});
	});

	lab.test('should give error if a mainMenu is missing customItems', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: []
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.customItems');
			done();
		});
	});

	lab.test('should give error if a mainMenu items has invalid type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: null,
			customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items');
			done();
		});
	});

	lab.test('should give error if a mainMenu customItems has invalid type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [],
				customItems: null
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.customItems');
			done();
		});
	});

	lab.test('should give error if a mainMenu item does not have all required fields', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{

				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].menuItemId');
			done();
		});
	});

	lab.test('should give error if a mainMenu.item[x].menuItemId has invalid type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: null,
					action: null,
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].menuItemId');
			done();
		});
	});

	lab.test('should give error if a mainMenu.item[x].menuItemId is empty string', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: '',
					action: null,
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].menuItemId');
			done();
		});
	});

	lab.test('should give error if a mainMenu.item[x].action has invalid data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: null,
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action');
			done();
		});
	});

	lab.test('should give error if a mainMenu.item[x].action does not have required fields', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.type');
			done();
		});
	});

	lab.test('should give error if a mainMenu.item[x].action.type has invalid data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: null,
						navigation: null
					},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.type');
			done();
		});
	});

	lab.test('should give error if a mainMenu.item[x].action.type has invalid value', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'asdasddsa',
						navigation: null
					},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.type');
			done();
		});
	});

	lab.test('should give error if a mainMenu.item[x].action.navigation has invalid data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: null
					},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.navigation');
			done();
		});
	});

	lab.test('should give error if a mainMenu.item[x].action.navigation does not have required fields', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {}
					},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.navigation.viewType');
			done();
		});
	});

	lab.test('should give error if a mainMenu.item[x].action.navigation.viewType has invalid data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: null
						}
					},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.navigation.viewType');
			done();
		});
	});

	lab.test('should give error if a mainMenu.item[x].action.navigation.viewType is empty string', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: ''
						}
					},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.navigation.viewType');
			done();
		});
	});

	lab.test('should give error if a mainMenu.item[x].action.navigation does not have any of(path/categoryType)', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: 'someViewThatDevicesKnow'
						}
					},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-men-nav-inv');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.navigation');
			done();
		});
	});

	lab.test('should give error about mainMenu.item[x].action.navigation.path if it has invalid data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: 'someViewThatDevicesKnow',
							path: null
						}
					},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.navigation.path');
			done();
		});
	});

	lab.test('should give error about mainMenu.item[x].action.navigation.query if it has invalid data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: 'someViewThatDevicesKnow',
							path: '',
							query: null
						}
					},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.navigation.query');
			done();
		});
	});

	lab.test('should not give error about mainMenu.item[x].action.navigation if it is correct', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: 'someViewThatDevicesKnow',
							path: ''
						}
					},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.debuggingData.key).not.to.include(['mainMenu.items[0].action.navigation']);
			done();
		});
	});

	lab.test('should give error about mainMenu.item[x].action.navigation.categoryType if it has incorrect data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: 'someViewThatDevicesKnow',
							categoryType: null
						}
					},
					children: null,
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.navigation.categoryType');
			done();
		});
	});


	lab.test('should not give error about mainMenu.item[x].action.navigation.categoryType if it is correct', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].action.navigation = {
			viewType: 'someViewThatDevicesKnow',
			categoryType: hotelGroupData.categories[0].type
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});
	});




	lab.test('should give error about mainMenu.item[x].localized if has wrong data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: 'someViewThatDevicesKnow',
							path: ''
						}
					},
					children: [],
					localized: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].localized');
			done();
		});
	});

	lab.test('should give error about mainMenu.item[x].localized if it has invalid language keys', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: 'someViewThatDevicesKnow',
							path: ''
						}
					},
					children: [],
					localized: {
						bla: {}
					}
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-men-inv-lang');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].localized[bla]');
			done();
		});
	});


	lab.test('should give error about mainMenu.item[x].localized if it is missing required data', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].localized = {en_GB: {}};
		delete hotelGroupData.mainMenu.items[0].name;


		var p1 = q.defer();
		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].localized[en_GB].name');
			p1.resolve();
		});



		var p2 = q.defer();
		p1.promise.done(function(){
			hotelGroupData = getValidData();
			hotelGroupData.mainMenu.items[0].localized = {en_GB: {}};
			delete hotelGroupData.mainMenu.items[0].buttonImage;
			validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
				testingTools.expectError(err, 'hgv-key-miss');
				testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].localized[en_GB].buttonImage');
				//no error if test data
				validation.validateHotelGroupData(hotelGroupData, 'testData', function(err){
					testingTools.code.expect(err).to.be.null();
					p2.resolve();
				});
			});
		});



		var p3 = q.defer();
		p2.promise.done(function(){
			hotelGroupData = getValidData();
			hotelGroupData.mainMenu.items[0].localized = {en_GB: {}};
			delete hotelGroupData.mainMenu.items[0].backgroundImage;
			validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
				testingTools.expectError(err, 'hgv-key-miss');
				testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].localized[en_GB].backgroundImage');
				//no error if test data
				validation.validateHotelGroupData(hotelGroupData, 'testData', function(err){
					testingTools.code.expect(err).to.be.null();
					p3.resolve();
				});
			});
		});

		var p4 = q.defer();
		p3.promise.done(function(){
			hotelGroupData = getValidData();
			hotelGroupData.mainMenu.items[0].localized = {en_GB: {}};
			delete hotelGroupData.mainMenu.items[0].backgroundImage;
			delete hotelGroupData.mainMenu.items[0].buttonImage;
			//nod images to validate and it should be OK with it
			validation.validateHotelGroupData(hotelGroupData, 'testData', function(err){
				testingTools.code.expect(err).to.be.null();
				p4.resolve();
			});

		});

		p4.promise.done(function(){
			done();
		});
	});

	lab.test('should give error about mainMenu.item[x].(localized[x]).name if it has invalid data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: 'someViewThatDevicesKnow',
							path: ''
						}
					},
					children: [],
					localized: {
						en_GB: {
							name: null,
							buttonImage: null,
							backgroundImage: null
						}
					}
				}
			],
				customItems: {}
		};
		var p1 = q.defer();
		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].localized[en_GB].name');
			p1.resolve();
		});

		p1.promise.done(function(){
			hotelGroupData = getValidData();
			hotelGroupData.mainMenu.items[0].localized.en_GB.name = 'Some name';
			hotelGroupData.mainMenu.items[0].name = null;

				validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
				testingTools.expectError(err, 'hgv-inv-type');
				testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].name');
				done();
			});
		});
	});

	lab.test('should give error about mainMenu.item[x].localized[x].name if it is empty', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: 'someViewThatDevicesKnow',
							path: ''
						}
					},
					children: [],
					localized: {
						en_GB: {
							name: '',
							buttonImage: null,
							backgroundImage: null
						}
					}
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].localized[en_GB].name');
			done();
		});
	});

	lab.test('should give error about mainMenu.item[x].localized[x].buttonImage is incorrect (error from media module)', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: 'someViewThatDevicesKnow',
							path: ''
						}
					},
					children: [],
					localized: {
						en_GB: {
							name: 'asd',
							buttonImage: null,
							backgroundImage: null
						}
					}
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.debuggingData.key).to.include('mainMenu.items[0].localized[en_GB].buttonImage');
			done();
		});
	});

	lab.test('should give error about mainMenu.item[x].localized[x].backgroundImage is incorrect (error from media module)', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].localized.en_GB.backgroundImage = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.debuggingData.key).to.include('mainMenu.items[0].localized[en_GB].backgroundImage');
			done();
		});
	});

	lab.test('should give error about mainMenu.item[x].name is incorrect (means defaults are validated)', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [
				{
					menuItemId: 'menu-1',
					action: {
						type: 'navigation',
						navigation: {
							viewType: 'someViewThatDevicesKnow',
							path: ''
						}
					},
					children: [],
					localized: {
						en_GB: {}
					},
					name: '',
					buttonImage: null,
					backgroundImage: null
				}
			],
				customItems: {}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].name');
			done();
		});
	});

	lab.test('should not give error about mainMenu.item[x].localized if it correct (inherits defaults)', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].localized.en_GB = {};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should give error about mainMenu.item[x] if it has invalid menu navigation structure and action type no equal to slideshow', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[1].action.type = 'navigation';
		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-men-nav-inv');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[1].action.navigation');

			done();
		});
	});

	lab.test('should not give error about mainMenu.item[x].localized if it correct (without defaults)', function(done) {

		var hotelGroupData = getValidData();

		hotelGroupData.mainMenu.items[0].localized.en_GB = {
			name: 'blabla',
			buttonImage: hotelGroupData.mainMenu.items[0].buttonImage,
			backgroundImage: hotelGroupData.mainMenu.items[0].backgroundImage
		};
		delete hotelGroupData.mainMenu.items[0].buttonImage;
		delete hotelGroupData.mainMenu.items[0].backgroundImage;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should give error about mainMenu.items[0].action.navigation.query if it has wrong data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].action.navigation.query = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.navigation.query');
			done();
		});
	});

	lab.test('should give error about mainMenu.items[0].action.navigation.query.a if it has wrong data type', {timeout: 5000}, function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].action.navigation.query = { a: null };

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].action.navigation.query[a]');
			done();
		});
	});

	lab.test('should NOT give error about mainMenu.items[0].action.navigation.query.a if it is correct', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].action.navigation.query = { a: 'b' };

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should give error about mainMenu.items[0].buttonImage if it is incorrect', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].buttonImage = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'mv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].buttonImage');
			done();
		});
	});

	lab.test('should give error about mainMenu.items[0].buttonImage if it is incorrect', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].buttonImage = {};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.debuggingData.key).to.include('mainMenu.items[0].buttonImage');
			testingTools.code.expect(err.data.code).to.include('mv-');
			done();
		});
	});

	lab.test('should give error about mainMenu.customItems.blabla - meaning customItems are also validated like normal items', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu = {
			items: [],
			customItems: {
				blabla: {}
			}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.customItems[blabla].menuItemId');
			done();
		});
	});


	//leave these to bottom - testing of children
	lab.test('should give error about mainMenu.item[x].children if it has wrong data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].children = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			//testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].children');
			done();
		});
	});

	lab.test('should give error about mainMenu.item[x].children[x] - meaning children items are also validated', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].children.push({});

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[0].children[1].menuItemId');
			done();
		});
	});


	lab.test('should give error about infinite nesting of menu children elements in mainMenu.item[x].children[x]', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items[0].children = [hotelGroupData.mainMenu.items[0]];

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-men-too-deep');
			testingTools.code.expect(err.data.debuggingData.key).to.include('mainMenu.items[0].children[0]');
			done();
		});
	});

	lab.test('should give error about double menuItemIds', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items.push(hotelGroupData.mainMenu.items[0]);
		var id = hotelGroupData.mainMenu.items.length-1;
		hotelGroupData.mainMenu.items[id].children = [];

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-dbl-men-id');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mainMenu.items[' + id + '].menuItemId');
			done();
		});
	});

	lab.test('should not give error if no main menu items', function(done) {
		var hotelGroupData = getValidData();
		hotelGroupData.mainMenu.items = [];
		hotelGroupData.mainMenu.customItems = {};
		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});


	lab.test('should give error about info if has invalid data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info = null;


		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('info');
			done();
		});
	});

	lab.test('should give error about info if it is missing required fields', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info = {};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('info.localized');
			done();
		});
	});

	lab.test('should give error about info.localized if it has invalid data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info = {
			localized: null
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('info.localized');
			done();
		});
	});

	lab.test('should give error about info.localized[x] if has invalid language key', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info = {
			localized: {
				bla: {}
			}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inf-inv-lang');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('info.localized[bla]');
			done();
		});
	});

	lab.test('should give error about info.localized[x] if it is missing default language translation', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info = {
			localized: {
			}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inf-miss-def-lang');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('info.localized[en_GB]');
			done();
		});
	});

	lab.test('should give error about info.localized[x] if it misses required fields', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info = {
			localized: {
				en_GB: {}
			}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('info.localized[en_GB].name');
			done();
		});
	});

	lab.test('should give error about info.localized[x].name if it has invalid data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info = {
			localized: {
				en_GB: {
					name: null,
						logo: null,
						backgroundImage: null
				}
			}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('info.localized[en_GB].name');
			done();
		});
	});

	lab.test('should give error about info.localized[x].name if it is empty', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info = {
			localized: {
				en_GB: {
					name: '',
						logo: null,
						backgroundImage: null
				}
			}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('info.localized[en_GB].name');
			done();
		});
	});


	lab.test('should give error about info.localized[x].logo if it has invalid type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info = {
			localized: {
				en_GB: {
					name: 'asas',
						logo: null,
						backgroundImage: null
				}
			}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.debuggingData.key).to.equal('info.localized[en_GB].logo');
			done();
		});
	});

	lab.test('should give error about info.localized[x].logo if it is invalid', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info = {
			localized: {
				en_GB: {
					name: 'asas',
					logo: {},
					backgroundImage: null
				}
			}
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.debuggingData.key).to.include('info.localized[en_GB].logo');
			done();
		});
	});

	lab.test('should give error about info.localized[x].backgroundImage if it is invalid', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info.localized.en_GB.backgroundImage = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err);
			testingTools.code.expect(err.data.debuggingData.key).to.equal('info.localized[en_GB].backgroundImage');
			done();
		});
	});

	lab.test('should NOT give error about info.localized[x] if it is correct (w/0 defaults)', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info.localized.en_GB = {
			name: 'asdas',
			backgroundImage: hotelGroupData.info.backgroundImage
		};
		delete hotelGroupData.info.backgroundImage;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should NOT give error about info.localized[x] if it is correct (with defaults)', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.info.localized.en_GB = {};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	//validate apikeys
	lab.test('should give error about validApiKeys if it has wrong data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.validApiKeys = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('validApiKeys');
			done();
		});
	});

	lab.test('should give error about validApiKeys[x] if it has wrong data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.validApiKeys = [1];

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('validApiKeys[0]');
			done();
		});
	});

	lab.test('should give error about validApiKeys[x] if it is empty', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.validApiKeys = [''];

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('validApiKeys[0]');
			done();
		});
	});

	lab.test('should give error about mediaConfiguration if has wrong data type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mediaConfiguration = null;

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mediaConfiguration');
			done();
		});
	});

	lab.test('should give error about mediaConfiguration if is missing content', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mediaConfiguration = {};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mediaConfiguration.theme');
			done();
		});
	});

	lab.test('should give error about mediaConfiguration.theme if it has invalid type', function(done) {

		var hotelGroupData = getValidData();
		hotelGroupData.mediaConfiguration = {
			theme: null,
			defaults: null,
			extraAssets: null,
			placeholderImage: null,
			predefinedTransformations: null
		};

		validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('mediaConfiguration.theme');
			done();
		});
	});


	lab.test('should give error about mediaConfiguration.theme if it is missing data', function(done) {

		var hotelGroupData = getValidData();
		var correctTheme = JSON.parse(JSON.stringify(hotelGroupData.mediaConfiguration.theme));
		hotelGroupData.mediaConfiguration = {
			theme: {},
			defaults: null,
			extraAssets: null,
			placeholderImage: null,
			predefinedTransformations: null
		};


		//check for every theme key, if it is been validated
		var checks = [];

		for( var k in correctTheme ){
			var theme = {};
			for( var k2 in correctTheme ){
				if(k2 !== k){
					theme[k2] = null;
				}
			}
			checks.push([theme, k]);
		}


		function doVal(p, i){
			hotelGroupData = getValidData();
			hotelGroupData.mediaConfiguration.theme = checks[i][0];
			validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
				testingTools.expectError(err, 'hgv-key-miss');
				testingTools.code.expect(err.data.debuggingData.key).to.equal('mediaConfiguration.theme.' + checks[i][1]);
				p.resolve();
			});
		}

		testingTools.code.expect(Object.keys(checks).length).to.equal(Object.keys(correctTheme).length);
		var promises = [];

		var i = 0;
		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			doVal(p, i);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});

	});

	lab.test('should give error about mediaConfiguration.theme[x] if it has wrong data type', function(done) {

		var hotelGroupData = getValidData();

		var i = 0;
		var keys = Object.keys(hotelGroupData.mediaConfiguration.theme);
		function doVal(){
			hotelGroupData = getValidData();
			hotelGroupData.mediaConfiguration.theme[keys[i]] = null;

			validation.validateHotelGroupData(hotelGroupData, validationType, function(err){
				testingTools.expectError(err, 'hgv-inv-type');
				testingTools.code.expect(err.data.debuggingData.key).to.equal('mediaConfiguration.theme.' + keys[i]);
				if(keys[i+1] !== undefined){
					i++;
					doVal();
				}else{
					done();
				}
			});
		}
		doVal();

	});

	lab.test('should give error about mediaConfiguration.theme[x][x] if it is missing data', {timeout: 10000}, function(done) {

		var hotelGroupData = getValidData();
		var correctColor = JSON.parse(JSON.stringify(hotelGroupData.mediaConfiguration.theme.primaryColor));

		var checks = [];

		//check for every theme[x] key, if it has been validated
		for( var k in hotelGroupData.mediaConfiguration.theme ){
			for( var k2 in correctColor ){
				var themeKey = {};
				for( var k3 in correctColor ){
					if(k3 !== k2){
						themeKey[k3] = null;
					}
				}
				checks.push([themeKey, k, k2]);
			}
		}


		function doVal(p, i){
			hotelGroupData = getValidData();
			hotelGroupData.mediaConfiguration.theme[checks[i][1]] = checks[i][0];
			validation.validateMediaConfiguration(hotelGroupData, validationType, function(err){

				testingTools.expectError(err, 'hgv-key-miss');
				testingTools.code.expect(err.data.debuggingData.key).to.equal('mediaConfiguration.theme.' + checks[i][1] + '.' + checks[i][2]);
				p.resolve();

			});

		}

		testingTools.code.expect(Object.keys(checks).length).to.equal(Object.keys(correctColor).length * Object.keys(hotelGroupData.mediaConfiguration.theme).length);

		var i = 0;
		var promises = [];
		while( checks[i] !== undefined ){
			var p = q.defer();
			promises.push(p.promise);
			doVal(p, i);

			i++;
		}

		q.all(promises).done(function(){
			done();
		});


	});


	lab.test('should give error about mediaConfiguration.theme[x][x] if it has wrong data type', function(done) {

		var hotelGroupData = getValidData();
		var correctColor = JSON.parse(JSON.stringify(hotelGroupData.mediaConfiguration.theme.primaryColor));

		var checks = [];

		//check for every theme[x] key, if it has been validated
		for( var k in hotelGroupData.mediaConfiguration.theme ){
			for( var k2 in correctColor ){
				var themeKey = {};
				for( var k3 in correctColor ){
					if(k3 !== k2){
						themeKey[k3] = {};
					}else{
						themeKey[k3] = null;
					}
				}
				checks.push([themeKey, k, k2]);
			}
		}


		function doVal(p, i){
			hotelGroupData = getValidData();
			hotelGroupData.mediaConfiguration.theme[checks[i][1]] = checks[i][0];
			validation.validateMediaConfiguration(hotelGroupData, validationType, function(err){

				testingTools.expectError(err, 'hgv-inv-type');
				testingTools.code.expect(err.data.debuggingData.key).to.equal('mediaConfiguration.theme.' + checks[i][1] + '.' + checks[i][2]);
				p.resolve();

			});
		}

		testingTools.code.expect(Object.keys(checks).length).to.equal(Object.keys(correctColor).length * Object.keys(hotelGroupData.mediaConfiguration.theme).length);
		var i = 0;
		var promises = [];
		while(checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			doVal(p, i);
			i++;
		}

		q.all(promises).done(function(){
			done();
		});

	});


	lab.test('should give error about mediaConfiguration.theme[x][x][x] if it is missing data', function(done) {

		var hotelGroupData = getValidData();
		var correctColor = JSON.parse(JSON.stringify(hotelGroupData.mediaConfiguration.theme.primaryColor));
		var colorObj = JSON.parse(JSON.stringify(hotelGroupData.mediaConfiguration.theme.primaryColor.normal));

		var checks = [];

		//check for every theme[x] key, if it has been validated
		for( var k in hotelGroupData.mediaConfiguration.theme ){
			for( var k2 in correctColor ){

				for( var k4 in colorObj ){
					var colorKey = {};
					for( var k5 in colorObj ){
						if(k5 !== k4){
							colorKey[k5] = null;
						}
					}
					checks.push([colorKey, k, k2, k4]);
				}
			}
		}


		function doVal(p, i){
			hotelGroupData = getValidData();
			hotelGroupData.mediaConfiguration.theme[checks[i][1]][checks[i][2]] = checks[i][0];
			validation.validateMediaConfiguration(hotelGroupData, validationType, function(err){

				testingTools.expectError(err, 'hgv-key-miss');
				testingTools.code.expect(err.data.debuggingData.key).to.equal('mediaConfiguration.theme.' + checks[i][1] + '.' + checks[i][2] + '.' + checks[i][3]);
				p.resolve();

			});
		}

		testingTools.code.expect(Object.keys(checks).length)
			.to.equal(
				Object.keys(correctColor).length *
				Object.keys(hotelGroupData.mediaConfiguration.theme).length *
				Object.keys(colorObj).length
			);


		var i = 0;
		var promises = [];
		while(checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			doVal(p, i);
			i++;
		}

		q.all(promises).done(function(){
			done();
		});


	});

	lab.test('should give error about mediaConfiguration.theme[COLOR][STATE][FIELD] if it has wrong data type', function(done) {

		var hotelGroupData = getValidData();
		var correctColor = JSON.parse(JSON.stringify(hotelGroupData.mediaConfiguration.theme.primaryColor));
		var colorObj = JSON.parse(JSON.stringify(hotelGroupData.mediaConfiguration.theme.primaryColor.normal));

		var checks = [];

		//check for every theme[x] key, if it has been validated
		for( var k in hotelGroupData.mediaConfiguration.theme ){
			for( var k2 in correctColor ){

				for( var k4 in colorObj ){
					var colorKey = {};
					for( var k5 in colorObj ){
						if(k5 !== k4){
							colorKey[k5] = hotelGroupData.mediaConfiguration.theme[k][k2][k5];
						}else{
							colorKey[k5] = 'a';
						}
					}
					checks.push([colorKey, k, k2, k4]);
				}
			}
		}


		function doVal(p, i){
			hotelGroupData = getValidData();
			hotelGroupData.mediaConfiguration.theme[checks[i][1]][checks[i][2]] = checks[i][0];
			validation.validateMediaConfiguration(hotelGroupData, validationType, function(err){

				testingTools.expectError(err, 'hgv-inv-type');
				testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).to.equal('mediaConfiguration.theme.' + checks[i][1] + '.' + checks[i][2] + '.' + checks[i][3]);
				p.resolve();

			});
		}

		testingTools.code.expect(Object.keys(checks).length)
			.to.equal(
			Object.keys(correctColor).length *
			Object.keys(hotelGroupData.mediaConfiguration.theme).length *
			Object.keys(colorObj).length
		);


		var i = 0;
		var promises = [];
		while(checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			doVal(p, i);
			i++;
		}

		q.all(promises).done(function(){
			done();
		});


	});


	lab.test('should give error about mediaConfiguration.theme[COLOR][STATE][FIELD] if it has too small value', function(done) {

		var hotelGroupData = getValidData();
		var correctColor = JSON.parse(JSON.stringify(hotelGroupData.mediaConfiguration.theme.primaryColor));
		var colorObj = JSON.parse(JSON.stringify(hotelGroupData.mediaConfiguration.theme.primaryColor.normal));

		var checks = [];

		//check for every theme[x] key, if it has been validated
		for( var k in hotelGroupData.mediaConfiguration.theme ){
			for( var k2 in correctColor ){


				for( var k4 in colorObj ){

					//too small values
					var colorKey = {};
					var colorKey2 = {};
					var colorKey3 = {};
					for( var k5 in colorObj ){
						if(k5 !== k4){
							colorKey[k5] = hotelGroupData.mediaConfiguration.theme[k][k2][k5];
						}else{
							colorKey[k5] = -1;
						}

						if(k5 !== k4){
							colorKey2[k5] = hotelGroupData.mediaConfiguration.theme[k][k2][k5];
						}else{
							colorKey2[k5] = k5 === 'alpha' ? 1.1 : 256;
						}

						if(k5 !== k4){
							colorKey3[k5] = hotelGroupData.mediaConfiguration.theme[k][k2][k5];
						}else{
							colorKey3[k5] = 3.3;
						}
					}
					checks.push([colorKey, k, k2, k4]);
					checks.push([colorKey2, k, k2, k4]);
					checks.push([colorKey3, k, k2, k4]);
				}


			}
		}


		function doVal(p, i){
			hotelGroupData = getValidData();
			hotelGroupData.mediaConfiguration.theme[checks[i][1]][checks[i][2]] = checks[i][0];
			validation.validateMediaConfiguration(hotelGroupData, validationType, function(err){

				testingTools.expectError(err, 'hgv-inv-val');
				testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).to.equal('mediaConfiguration.theme.' + checks[i][1] + '.' + checks[i][2] + '.' + checks[i][3]);
				p.resolve();

			});
		}

		testingTools.code.expect(Object.keys(checks).length)
			.to.equal(
			Object.keys(correctColor).length *
			Object.keys(hotelGroupData.mediaConfiguration.theme).length *
			Object.keys(colorObj).length *
			3
		);


		var i = 0;
		var promises = [];
		while(checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			doVal(p, i);
			i++;
		}

		q.all(promises).done(function(){
			done();
		});


	});

	lab.test('should give error if email templates missing', function(done) {
		var data = getValidData();
		delete data.emailTemplates;
		validation.validateHotelGroupData(data, validationType, function(err){
			testingTools.expectError(err, 'hgv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).to.equal('emailTemplates');

			done();
		});

	});

	lab.test('should give error if email templates missing has wrong type', function(done) {
		var data = getValidData();
		data.emailTemplates = null;
		validation.validateHotelGroupData(data, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).to.equal('emailTemplates');

			done();
		});

	});

	lab.test('should give error if emailTemplates[x] is missing', function(done) {
		var data = getValidData();
		var templates = Object.keys(data.emailTemplates);
		var promises = [];

		function doVal(p, data, key){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.expectError(err, 'hgv-key-miss');
				testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).to.equal('emailTemplates[' + key + ']');
				p.resolve();
			});
		}

		for(var i in templates){
			var data1 = getValidData();
			delete data1.emailTemplates[templates[i]];
			var p = q.defer();
			promises.push(p.promise);
			doVal(p, data1, templates[i]);
		}

		q.all(promises).done(function(){
			done();
		});

	});

	lab.test('should give error if emailTemplates[x] is invalid', function(done) {
		var data = getValidData();
		var templates = Object.keys(data.emailTemplates);
		var promises = [];

		function doVal(p, data, key, errExp){
			validation.validateHotelGroupData(data, validationType, function(err){
				testingTools.expectError(err, errExp);
				testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).to.equal(key);
				p.resolve();
			});
		}

		for(var i in templates){
			var data1 = getValidData();
			data1.emailTemplates[templates[i]] = null;
			var p = q.defer();
			promises.push(p.promise);
			doVal(p, data1, 'emailTemplates[' + templates[i] + ']', 'msv-inv-val');

			var data2 = getValidData();
			data2.emailTemplates[templates[i]] = {};
			var p2 = q.defer();
			promises.push(p2.promise);
			doVal(p2, data2, 'emailTemplates[' + templates[i] + '].localized', 'msv-key-miss');

			var data4 = getValidData();
			data4.emailTemplates[templates[i]].localized = {};
			var p4 = q.defer();
			promises.push(p4.promise);
			doVal(p4, data4, 'emailTemplates[' + templates[i] + '].localized', 'hgv-emt-no-def-lang');
		}


		q.all(promises).done(function(){
			done();
		});

	});

	lab.test('should give error if validateGaTrackingId incorrect', function(done) {
		var data = getValidData();
		data.gaTrackingId = 1;
		validation.validateHotelGroupData(data, validationType, function(err){
			testingTools.expectError(err, 'hgv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('gaTrackingId');
			done();
		});

	});

	lab.test('should not give error if all OK', function(done) {
		var data = getValidData();
		validation.validateHotelGroupData(data, validationType, function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});

	});

});
