'use strict';
var errors = require('../utils/errors');
var langUtils = require('../utils/langUtils');
var validationUtils = require('../utils/validationUtils');
var catVal = require('../categories/validation');
var dao = require('./dao');
var check = require('check-types');
var mediaVal = require('../media/validation');
var messagingVal = require('../messaging/validation');
var q = require('q');

errors.defineError('hgv-key-miss', 'System error', 'A key is missing from the data set', 'HotelGroups.Validation', 400);
errors.defineError('hgv-inv-type', 'System error', 'A value has a wrong data type', 'HotelGroups.Validation', 400);
errors.defineError('hgv-inv-val', 'System error', 'A value has an invalid value', 'HotelGroups.Validation', 400);
errors.defineError('hgv-inv-val-type', 'System error', 'Invalid validation type provided', 'HotelGroups.Validation', 500);
errors.defineError('hgv-inv-qty', 'System error', 'A key items count is invalid', 'HotelGroups.Validation', 500);
errors.defineError('hgv-hgId-exists', 'System error', 'Hotel group with this id already exists', 'HotelGroups.Validation', 400);
errors.defineError('hgv-inv-def-lan', 'Default language must be one of supported languages', 'Invalid default language', 'HotelGroups.Validation', 400);
errors.defineError('hgv-inv-trans-lang', 'System error', 'Invalid language code for translations given', 'HotelGroups.Validation', 400);
errors.defineError('hgv-trans-no-def', 'Translations for default language are mandatory', 'No translations given for default language', 'HotelGroups.Validation', 400);
errors.defineError('hgv-trans-nonstr', 'System error', 'Translation must be provided as string', 'HotelGroups.Validation', 400);
errors.defineError('hgv-trans-empty', 'Translation cannot be empty', 'Translations cannot be empty strings', 'HotelGroups.Validation', 400);
errors.defineError('hgv-men-nav-inv', 'System error', 'The menu item action.navigation structure is incorrect', 'HotelGroups.Validation', 400);
errors.defineError('hgv-men-too-deep', 'The menu item cannot link to parent or have over 100 levels', 'The menu item has a children, which points to a parent or there are more than 100 levels of children', 'HotelGroups.Validation', 400);
errors.defineError('hgv-men-inv-lang', 'System error', 'The menu item has a localized translation with an invalid language key', 'HotelGroups.Validation', 400);
errors.defineError('hgv-inf-miss-def-lang', 'Information must be provided in default language', 'Hotel group information id not given in default language', 'HotelGroups.Validation', 400);
errors.defineError('hgv-inf-inv-lang', 'System error', 'Hotel group information is provided with invalid langauge key', 'HotelGroups.Validation', 400);
errors.defineError('hgv-inv-parcat', 'System error', 'The parent category Id does not exist', 'HotelGroups.Validation', 400);
errors.defineError('hgv-dbl-men-id', 'Menu items cannot be identical', 'Two main menu items have a same menuItemId', 'HotelGroups.Validation', 400);
errors.defineError('hgv-emt-no-def-lang', 'Email template must be provided in the default language', 'The email template is not provided in default langauge', 'HotelGroups.Validation', 400);
errors.defineError('hgv-dbl-catid', 'Cannot have two same categories', 'Two categories have the same categoryId', 'Categories.validation', 400);

/**
 * Validate that all root level keys exist
 * @param hotelGroupData
 * @param validationType
 * @param callback
 * @returns {*}
 */
function validateRootKeys(hotelGroupData, validationType, callback){
	var rootKeys = ['hotelGroupId', 'supportedLanguages', 'defaultLanguage', 'translations', 'categories', 'mainMenu', 'info', 'validApiKeys', 'mediaConfiguration', 'profilesConfiguration', 'emailTemplates', 'viewConfiguration'];

	validationUtils.validateHasKeys(rootKeys, hotelGroupData, function(missingKey){
		if(missingKey){
			return callback(errors.newError('hgv-key-miss', {key: missingKey}));
		}

		return callback(null);
	});
}

function validateSearchConfiguration(hotelGroupData, validationType, callback){
	if(hotelGroupData.searchConfiguration){
		if(hotelGroupData.searchConfiguration.resultOrdering){
			var possibleValues = ['random'];
			if(possibleValues.indexOf(hotelGroupData.searchConfiguration.resultOrdering) <0){
				return callback(errors.newError('hgv-key-miss', {error: 'Result ordering wrong value'}));
			}
		}
	}else{
		return callback(errors.newError('hgv-key-miss', {key: 'searchConfiguration'}));
	}
	return callback(null);
}

/**
 * Validate that all root level keys exist
 * @param hotelGroupData
 * @param validationType
 * @param callback
 * @returns {*}
 */
function validateViewConfiguration(hotelGroupData, validationType, callback){
	var viewConfiguration = hotelGroupData.viewConfiguration;
	var errPrefix = 'viewConfiguration';
	if(!check.object(viewConfiguration)){
		return callback(errors.newError('hgv-inv-type', {key: errPrefix, type: typeof viewConfiguration, expectedType: 'object'}));
	}
	validationUtils.validateHasKeys(['hotelMenu', 'profileMenu'], viewConfiguration, function(missingKey) {

		if (missingKey) {
			return callback(errors.newError('hgv-key-miss', {key: errPrefix + '.' + missingKey}));
		}
		validationUtils.skipValidationIfKeysEqualTo(
			viewConfiguration,
			['hotelMenu', 'profileMenu'],
			[false, false],
			function (key, scb) {
				validationUtils.validateWithArrayOfFunctionsWithCallbacks(
					[
						function (vConf, errKey, k, cb) {
							if(!check.array(vConf[k])){
								return cb(errors.newError('hgv-inv-type', {
									key: errKey + '.' + k,
									type: typeof vConf[k],
									expectedType: 'array'
								}));
							} else {
								return cb(null);
							}
						},
						function (vConf, errKey, k, cb) {
							if (check.emptyArray(vConf[key])) {
								return cb(errors.newError('hgv-inv-qty', {
									key: errKey + '.' + k,
									count: vConf.hotelMenu.length
								}));
							}
							return cb(null);
						},
						function (vConf, errKey, k, cb) {
							for (var index in vConf[k]) {
								var item = vConf[k][index];
								var menuItemKey = errKey + '.' + k + '[' + index + ']';
								if (!check.object(item)) {
									return cb(errors.newError('hgv-inv-type', {
										key: menuItemKey,
										type: typeof item,
										expectedType: 'object'
									}));
								}
								var actionKey = errKey + '.' + key + '[' + index + '].action';
								var removeIfDisableKey = errKey + '.' + key + '[' + index + '].removeIfDisabled';

								if (!item.hasOwnProperty('action')) {
									return cb(errors.newError('hgv-key-miss', {key: actionKey}));
								}
								if (!check.string(item.action)) {
									return cb(errors.newError('hgv-inv-type', {key: actionKey}));
								}
								if (!check.unemptyString(item.action)) {
									return cb(errors.newError('hgv-inv-val', {key: actionKey}));
								}
								if (!item.hasOwnProperty('removeIfDisabled')) {
									return cb(errors.newError('hgv-key-miss', {key: removeIfDisableKey}));
								}
								if (!check.boolean(item.removeIfDisabled)) {
									return cb(errors.newError('hgv-inv-type', {key: removeIfDisableKey}));
								}

							}
							return cb(null);
						}
					],
					[viewConfiguration, errPrefix, key],
					function (err) {
						return scb(err);
					}
				);
			},
			function(err){
				return callback(err);
			}
		);
	});

}

/**
 * Validate single main menu item
 * @param item {Object} Main menu item object
 * @param field {String} Path to the field
 * @param ids {String[]} Array of ids - will be modified
 * @param doubleId {boolean|Array} If double IDs are found the result is stored here - will be modified
 * @param hotelGroupData {Object} Hotel group data object
 * @param cb {Function} Callback function
 * @param [level] {Integer} The nesting level, starting from 0 (used internally)
 * @param context
 * @returns {*}
 */
function validateMainMenuItem(item, field, ids, doubleId, hotelGroupData, cb, level, context){
	level = level ? level : 0;
	var reqF = ['menuItemId', 'action', 'children', 'localized'];
	for ( var fi in reqF ){
		if( !item.hasOwnProperty(reqF[fi]) ){
			return cb(errors.newError('hgv-key-miss', {key: field + '.' + reqF[fi]}));
		}
	}

	if( !check.string(item.menuItemId) ){
		return cb(errors.newError('hgv-inv-type', {key: field + '.menuItemId'}));
	}

	if( !check.unemptyString(item.menuItemId) ){
		return cb(errors.newError('hgv-inv-val', {key: field + '.menuItemId'}));
	}
	//validate action
	if( !check.object(item.action) ){
		return cb(errors.newError('hgv-inv-type', {key: field + '.action'}));
	}


	var af = ['type', 'navigation'];
	for(var ai in af){
		if( !item.action.hasOwnProperty(af[ai]) ){
			return cb(errors.newError('hgv-key-miss', {key: field + '.action.' + af[ai]}));
		}
	}

	if( !check.string(item.action.type) ){
		return cb(errors.newError('hgv-inv-type', {key: field + '.action.type'}));
	}


	var validActionTypes = ['navigation', 'search', 'customUrl', 'slideshow', 'logo'];

	if( validActionTypes.indexOf(item.action.type) < 0 ){
		return cb(errors.newError('hgv-inv-val', {key: field + '.action.type', validValues: validActionTypes, givenType: item.action.type}));
	}

	if( !check.object(item.action.navigation) ){
		return cb(errors.newError('hgv-inv-type', {key: field + '.action.navigation'}));
	}

	//validate action.navigation

	var avF = ['viewType'];
	for(var avi in avF){
		if( !item.action.navigation.hasOwnProperty(avF[avi]) ){
			return cb(errors.newError('hgv-key-miss', {key: field + '.action.navigation.' + avF[avi]}));
		}
	}

	if( !check.string(item.action.navigation.viewType) ){
		return cb(errors.newError('hgv-inv-type', {key: field + '.action.navigation.viewType'}));
	}

	if( !check.unemptyString(item.action.navigation.viewType) ){
		return cb(errors.newError('hgv-inv-val', {key: field + '.action.navigation.viewType'}));
	}

	//check if has correct set of fields in it
	if( item.action.navigation.hasOwnProperty('categoryType') ){
		//if category type given
		if( !check.string(item.action.navigation.categoryType) ){
			return cb(errors.newError('hgv-inv-type', {key: field + '.action.navigation.categoryType'}));
		}

	}else if( item.action.navigation.hasOwnProperty('path') ){
		if( !check.string(item.action.navigation.path) ){
			return cb(errors.newError('hgv-inv-type', {key: field + '.action.navigation.path'}));
		}
	}else{
		if( item.action.type !== 'slideshow' ){
			return cb(errors.newError('hgv-men-nav-inv', {key: field + '.action.navigation'}));
		}
	}

	if( item.action.navigation.hasOwnProperty('query') ){
		if( !check.object(item.action.navigation.query) ){
			return cb(errors.newError('hgv-inv-type', {key: field + '.action.navigation.query'}));
		}
		for(var qi in item.action.navigation.query){
			if( !check.string(item.action.navigation.query[qi]) && !check.number(item.action.navigation.query[qi]) ){
				return cb(errors.newError('hgv-inv-type', {key: field + '.action.navigation.query[' + qi + ']'}));
			}
		}
	}

	//validate localized
	if( !check.object(item.localized) ){
		return cb(errors.newError('hgv-inv-type', {key: field + '.localized'}));
	}

	/**
	 * SUBFUNCTION! Validate name field
	 * @param name
	 * @param fieldKey
	 * @param cbVn Callback
	 * @returns {*}
	 */
	function valName(name, fieldKey, cbVn){
		if( !check.string(name) ){
			return cbVn(errors.newError('hgv-inv-type', {key: fieldKey}));
		}

		if( !check.unemptyString(name) ){
			return cbVn(errors.newError('hgv-inv-val', {key: fieldKey}));
		}

		return cbVn(null);
	}

	/**
	 * SUBFUNCTION! Validate image field
	 * @param imgObj
	 * @param field
	 * @param cbIv Callback
	 */
	function valImg(imgObj, field, cbIv){
		mediaVal.validateImageObject(imgObj, 'hotelGroupImage', function(err){
			if(err){
				err.data.debuggingData.key = field + (err.data.debuggingData.key ? '.' + err.data.debuggingData.key : '');
			}
			return cbIv(err);
		}, {hotelGroupId: hotelGroupData.hotelGroupId});
	}

	var validateNames = [];
	var validateImages = [];

	for( var lk in item.localized ){
		if( !langUtils.isValidLanguageCode(lk) ){
			return cb(errors.newError('hgv-men-inv-lang', {key: field + '.localized[' + lk + ']'}));
		}

		var loc = item.localized[lk];

		if( !loc.hasOwnProperty('name') && !item.hasOwnProperty('name') ){
			return cb(errors.newError('hgv-key-miss', {key: field + '.localized[' + lk + '].name'}));
		}

		if( !loc.hasOwnProperty('buttonImage') && !item.hasOwnProperty('buttonImage') ){
			if(context.requireMenuItemButtonImage) {
				return cb(errors.newError('hgv-key-miss', {key: field + '.localized[' + lk + '].buttonImage'}));
			}
		}

		if( !loc.hasOwnProperty('backgroundImage') && !item.hasOwnProperty('backgroundImage') ){
			if(context.requireMenuItemBackground){
				return cb(errors.newError('hgv-key-miss', {key: field + '.localized[' + lk + '].backgroundImage'}));
			}
		}
		//validate fields
		if( loc.hasOwnProperty('name') ){
			validateNames.push([loc.name, field + '.localized[' + lk + '].name']);

		}

		if( loc.hasOwnProperty('buttonImage') ){
			validateImages.push([loc.buttonImage, field + '.localized[' + lk + '].buttonImage']);
		}

		if( loc.hasOwnProperty('backgroundImage') ){
			validateImages.push([loc.backgroundImage, field + '.localized[' + lk + '].backgroundImage']);
		}

	}

	//validate defaults
	if( item.hasOwnProperty('name') ){
		validateNames.push([item.name, field + '.name']);
	}

	if( item.hasOwnProperty('buttonImage') ){
		validateImages.push([item.buttonImage, field + '.buttonImage']);
	}

	if( item.hasOwnProperty('backgroundImage') ){
		validateImages.push([item.backgroundImage, field + '.backgroundImage']);
	}

	//declare functions - will be called at bottom
	var vii = 0;
	function validateImagesFunc(next){
		if(validateImages[vii] === undefined){
			return next();
		}
		valImg(validateImages[vii][0], validateImages[vii][1], function(erI){
			if(erI){
				return cb(erI);
			}else if(validateImages[vii+1] !== undefined){
				vii++;
				validateImagesFunc(next);
			}else{
				next();
			}
		});


	}

	var vni = 0;
	function validateNamesFunc(next){
		var promises = [];

		function val(p, i1, i2){
			valName(i1, i2, function(erN) {
				if(erN){
					p.reject(erN);
				}else{
					p.resolve();
				}
			});
		}

		for(vni=0; validateNames[vni] !== undefined; vni++){
			var p = q.defer();
			promises.push(p.promise);
			val(p, validateNames[vni][0], validateNames[vni][1]);
		}

		q.all(promises).done(function(){
			next(null);
		}, function(err){
			cb(err);
		});

	}

	function validateDoubles(next){
		//store doubles
		if(ids.indexOf(item.menuItemId) < 0){
			ids.push(item.menuItemId);
		}else{
			doubleId.push([item.menuItemId, field + '.menuItemId']);
		}

		next();
	}

	/**
	 * SUBFUNCTION! End the validations
	 * @returns {*}
	 */
	function endValidation(next){
		//validate children
		if( !check.array(item.children) ){
			return cb(errors.newError('hgv-inv-type', {key: field + '.children'}));
		}

		//validate children
		if( item.children.length > 0 ){
			//infinite recursion protection
			if(level > 100){
				return cb(errors.newError('hgv-men-too-deep', {key: field + '.children'}));
			}

			var childI = 0;
			var valChild = function(ccb){
				validateMainMenuItem(item.children[childI], field + '.children[' + childI + ']', ids, doubleId, hotelGroupData, function(err){
					if(err){
						return ccb(err);
					}else if( item.children[childI+1] !== undefined ){
						childI++;
						valChild(ccb);
					}else{
						ccb(null);
					}
				}, level+1, context);
			};

			valChild(function(ce){
				if(ce){
					return cb(ce);
				}else{
					next();
				}
			});

		}else{
			next();
		}
	}

	validateNamesFunc(function(){
		validateImagesFunc(function(){
			endValidation(function(){
				validateDoubles(cb);
			});
		});
	});


}

/**
 * Validate main menu structure
 * @param hotelGroupData
 * @param validationType
 * @param callback
 */
function validateMainMenu(hotelGroupData, validationType, callback, context){
	if( !check.object(hotelGroupData.mainMenu) ){
		return callback(errors.newError('hgv-inv-type', {key: 'mainMenu'}));
	}

	if( !hotelGroupData.mainMenu.hasOwnProperty('items') ){
		return callback(errors.newError('hgv-key-miss', {key: 'mainMenu.items'}));
	}

	if( !hotelGroupData.mainMenu.hasOwnProperty('customItems') ){
		return callback(errors.newError('hgv-key-miss', {key: 'mainMenu.customItems'}));
	}

	if( !Array.isArray(hotelGroupData.mainMenu.items) ){
		return callback(errors.newError('hgv-inv-type', {key: 'mainMenu.items', expectedType: 'array'}));
	}

	if( !check.object(hotelGroupData.mainMenu.customItems) ){
		return callback(errors.newError('hgv-inv-type', {key: 'mainMenu.customItems', expectedType: 'object'}));
	}

	var items = [];
	var i = 0;
	var ids = [];
	var doubleId = [];
	function validateMainMenuItems(cb){
		var item = items[i][0];
		validateMainMenuItem(item, items[i][1], ids, doubleId, hotelGroupData, function(err){
			if(err){
				return cb(err);
			}else if(items[i+1] !== undefined){
				i++;
				return validateMainMenuItems(cb);
			}else{
				cb(null);
			}
		}, 0, context);

	}

	for( var ii in hotelGroupData.mainMenu.items ){
		items.push([hotelGroupData.mainMenu.items[ii], 'mainMenu.items[' + ii + ']']);
	}

	for( var ci in hotelGroupData.mainMenu.customItems ){
		items.push([hotelGroupData.mainMenu.customItems[ci], 'mainMenu.customItems[' + ci + ']']);
	}

	if(items.length > 0){
		validateMainMenuItems(function(err){
			if(err){
				return callback(err);
			}

			if(doubleId.length > 0){
				return callback(errors.newError('hgv-dbl-men-id', {key: doubleId[0][1]}));
			}

			callback(null);

		});
	}else{
		callback(null);
	}

}

/**
 * Validate categories
 * @param hotelGroupData
 * @param validationType
 * @param callback
 */
function validateCategories(hotelGroupData, validationType, callback, context){
	if( !Array.isArray(hotelGroupData.categories) ){
		return callback(errors.newError('hgv-inv-type', {key: 'categories'}));
	}

	//overwrite the callback function to do some extra validation after validating each category
	var _cb = callback;
	var ids = [];
	callback = function(err){
		if(err){
			return _cb(err);
		}
		//validate if categoryIds are unique
		for( var ci1 in hotelGroupData.categories ){
			if( ids.indexOf(hotelGroupData.categories[ci1].categoryId) < 0 ){
				ids.push(hotelGroupData.categories[ci1].categoryId);
			}else{
				return _cb(errors.newError('hgv-dbl-catid', {key: 'categories[' + ci1 + '].categoryId'}));
			}
		}
		//validate parentCategoryIds
		for( var ci2 in hotelGroupData.categories ){
			if( hotelGroupData.categories[ci2].parentCategoryId && ids.indexOf(hotelGroupData.categories[ci2].parentCategoryId) < 0){
				return _cb(errors.newError('hgv-inv-parcat', {key: 'categories[' + ci2 + '].parentCategoryId'}));
			}
		}

		_cb();
	};

	//recursive function for validation of every category
	var ci = 0;
	function doVal(p, item){
		var ctx = {hotelGroupId: hotelGroupData.hotelGroupId, defaultLanguage: hotelGroupData.defaultLanguage};
		for(var cn in context){
			ctx[cn] = context[cn];
		}
		//use other module to validate
		catVal.validateCategory(item, 'full', function(err){
			if(err){
				err.data.debuggingData.key = 'categories[' + ci + ']' + (err.data.debuggingData.key ? '.' + err.data.debuggingData.key : '');
				return p.reject(err);
			}else{
				return p.resolve(null);
			}
		}, ctx);
	}

	//start validation
	if(hotelGroupData.categories.length > 0){
		var promises = [];
		for(ci=0; hotelGroupData.categories[ci] !== undefined; ci++) {
			var p = q.defer();
			promises.push(p.promise);
			doVal(p, hotelGroupData.categories[ci]);
		}
		q.all(promises).done(function(){
			callback(null);
		}, function(err){
			callback(err);
		});
	}else{
		callback(null);
	}


}

/**
 * Validate the hotel group ID
 * @param hotelGroupData
 * @param validationType
 * @param callback
 */
function validateHotelGroupId(hotelGroupData, validationType, callback){
	if( !check.string(hotelGroupData.hotelGroupId)){
		return callback(errors.newError('hgv-inv-type', {key: 'hotelGroupId'}));
	}
	if( !check.unemptyString(hotelGroupData.hotelGroupId) ){
		return callback(errors.newError('hgv-inv-val', {key: 'hotelGroupId'}));
	}
	dao.checkIfExists(hotelGroupData.hotelGroupId, function(err, exists){
		if(exists && validationType !== 'update'){
			return callback(errors.newError('hgv-hgId-exists'));
		}
		callback(err);
	});


}

/**
 * Validate the translations
 * @param hotelGroupData
 * @param validationType
 * @param callback
 */
function validateTranslations(hotelGroupData, validationType, callback){

	if( !check.object(hotelGroupData.translations) ){
		return callback(errors.newError('hgv-inv-type', {key: 'translations'}));
	}

	if( !check.object(hotelGroupData.translations.localized) ){
		return callback(errors.newError('hgv-inv-type', {key: 'translations.localized'}));
	}

	var hasDef = false;
	var emptyVal = false;
	var invVal = false;

	for( var lk in hotelGroupData.translations.localized ){
		//validate language key
		if( !langUtils.isValidLanguageCode(lk) ){
			return callback(errors.newError('hgv-inv-trans-lang', {key: 'translations.localized[' + lk + ']'}));
		}
		if(lk === hotelGroupData.defaultLanguage){
			hasDef = true;
		}

		//validate keys
		for(var key in hotelGroupData.translations.localized[lk]){

			if( !check.string(hotelGroupData.translations.localized[lk][key]) ){
				invVal =[lk, key, typeof hotelGroupData.translations.localized[lk][key]];
			}else if( !check.unemptyString(hotelGroupData.translations.localized[lk][key]) ){
				emptyVal =[lk, key];
			}
		}

	}
	//no default language
	if(!hasDef){
		return callback(errors.newError('hgv-trans-no-def', {key: 'translations.localized'}));
	}

	if(emptyVal){
		return callback(errors.newError('hgv-trans-empty', {key: 'translations.localized[' + emptyVal[0] + '][' + emptyVal[1] + ']'}));
	}

	if(invVal){
		return callback(errors.newError('hgv-trans-nonstr', {key: 'translations.localized[' + invVal[0] + '][' + invVal[1] + ']', givenType: invVal[2]}));
	}

	//merge with defaults and validate
	langUtils.mergeTranslationsWithDefault(hotelGroupData.translations, function(err, translations){
		if(err){
			return callback(err);
		}

		langUtils.validateTranslations(translations, function(err){
			if(err){
				err.data.debuggingData.key = 'translations.' + err.data.debuggingData.key;
				return callback(err);
			}
			callback(null);
		});

	});



}

/**
 * Validate the API keys
 * @param hotelGroupData
 * @param validationType
 * @param callback
 */
function validateApiKeys(hotelGroupData, validationType, callback){
	if( !check.array(hotelGroupData.validApiKeys) ){
		return callback(errors.newError('hgv-inv-type', {key: 'validApiKeys'}));
	}

	for(var ki in hotelGroupData.validApiKeys){
		if( !check.string(hotelGroupData.validApiKeys[ki]) ){
			return callback(errors.newError('hgv-inv-type', {key: 'validApiKeys[' + ki + ']'}));
		}

		if( !check.unemptyString(hotelGroupData.validApiKeys[ki]) ){
			return callback(errors.newError('hgv-inv-val', {key: 'validApiKeys[' + ki + ']'}));
		}
	}
	callback(null);

}

/**
 * Validate the default language
 * @param hotelGroupData
 * @param validationType
 * @param callback
 */
function validateDefaultLanguage(hotelGroupData, validationType, callback){
	if( !check.string(hotelGroupData.defaultLanguage) ){
		return callback(errors.newError('hgv-inv-type', {key: 'defaultLanguage'}));
	}
	if( hotelGroupData.supportedLanguages.indexOf(hotelGroupData.defaultLanguage) < 0 ){
		return callback(errors.newError('hgv-inv-def-lan', {key: 'defaultLanguage'}));
	}
	callback(null);

}

/**
 * Validate the media configuration
 * @param hotelGroupData
 * @param validationType
 * @param callback
 */
function validateMediaConfiguration(hotelGroupData, validationType, callback){
	var fields = ['theme', 'defaults', 'extraAssets', 'placeholderImage', 'predefinedTransformations'];
	var themeFields = ['primaryColor', 'secondaryColor', 'primaryTextColor', 'secondaryTextColor', 'backgroundColor',
		'inputColor', 'backgroundTextColor', 'inputTextColor', 'primaryImageTextColor', 'secondaryImageTextColor',
		'backgroundSubtextColor', 'heroTextColor', 'heroSubtextColor', 'placeholderTextColor', 'progressIndicatorColor',
		'primaryIconColor', 'secondaryIconColor'];
	var colorFields = ['normal', 'disabled', 'selected', 'highlighted', 'highlightedSelected'];
	var stateFields = ['b', 'g', 'r', 'alpha'];

	validationUtils.validateWithArrayOfFunctionsWithCallbacks(
		[
			function(cb){
				if( !check.object(hotelGroupData.mediaConfiguration) ){
					return cb(errors.newError('hgv-inv-type', {key: 'mediaConfiguration'}));
				}
				cb();
			},

			function(cb){
				validationUtils.validateHasKeys(fields, hotelGroupData.mediaConfiguration, function(errKey){
					if(errKey){
						return cb(errors.newError('hgv-key-miss', {key: 'mediaConfiguration.' + errKey}));
					}
					cb();
				});
			},

			function(cb){
				if( !check.object(hotelGroupData.mediaConfiguration.theme) ){
					return cb(errors.newError('hgv-inv-type', {key: 'mediaConfiguration.theme'}));
				}
				cb();
			},

			//validate theme fields existence
			function(cb){
				validationUtils.validateHasKeys(themeFields, hotelGroupData.mediaConfiguration.theme, function(errKey){
					if(errKey){
						return cb(errors.newError('hgv-key-miss', {key: 'mediaConfiguration.theme.' + errKey}));
					}
					cb();
				});
			},

			//validate theme fields type
			function(cb){
				for(var ti in themeFields){
					if( !check.object(hotelGroupData.mediaConfiguration.theme[themeFields[ti]]) ){
						return cb(errors.newError('hgv-inv-type', {key: 'mediaConfiguration.theme.' + themeFields[ti]}));
					}
				}
				cb();
			},


			//validate theme[x] fields existence
			function(cb){
				var ti = 0;
				//validate recursively
				function doVal(){
					validationUtils.validateHasKeys(colorFields, hotelGroupData.mediaConfiguration.theme[themeFields[ti]], function(errKey){
						if(errKey){
							return cb(errors.newError('hgv-key-miss', {key: 'mediaConfiguration.theme.' + themeFields[ti] + '.' + errKey}));
						}else if( themeFields[ti+1] !== undefined ){
							ti++;
							doVal();
						}else{
							cb();
						}
					});
				}

				doVal();

			},

			//validate theme[x] fields type
			function(cb){
				var ti = 0;
				//validate recursively
				function doVal(){
					for( var fi in colorFields ){
						if( !check.object(hotelGroupData.mediaConfiguration.theme[themeFields[ti]][colorFields[fi]]) ){
							return cb(errors.newError('hgv-inv-type', {key: 'mediaConfiguration.theme.' + themeFields[ti] + '.' + colorFields[fi]}));
						}
					}

					if( themeFields[ti+1] !== undefined ){
						ti++;
						doVal();
					}else{
						cb();
					}

				}

				doVal();
			},

			//validate theme[x][x] fields existence
			function(cb){
				var ti = 0;
				var ci = 0;
				//validate recursively
				function doVal(){
					validationUtils.validateHasKeys(stateFields, hotelGroupData.mediaConfiguration.theme[themeFields[ti]][colorFields[ci]], function(errKey){
						if(errKey){
							return cb(errors.newError('hgv-key-miss', {key: 'mediaConfiguration.theme.' + themeFields[ti] + '.' + colorFields[ci] + '.' + errKey}));
						}else if( colorFields[ci+1] !== undefined ){
							ci++;
							doVal();
						}else if( themeFields[ti+1] !== undefined ){
							ci = 0;
							ti++;
							doVal();
						}else{
							cb();
						}
					});
				}

				doVal();

			},

			//validate theme[COLOR][STATE][FIELD] types
			function(cb){
				var ti = 0;
				var ci = 0;
				//validate recursively
				function doVal(){
					for( var fi in stateFields ){
						var val = hotelGroupData.mediaConfiguration.theme[themeFields[ti]][colorFields[ci]][stateFields[fi]];
						if(!check.number( val ) ){
							return cb(errors.newError('hgv-inv-type', {key: 'mediaConfiguration.theme.' + themeFields[ti] + '.' + colorFields[ci] + '.' + stateFields[fi], value: val}));
						}
					}

					if( colorFields[ci+1] !== undefined ){
						ci++;
						doVal();
					}else if( themeFields[ti+1] !== undefined ){
						ci = 0;
						ti++;
						doVal();
					}else{
						cb();
					}
				}

				doVal();

			},

			//validate theme[COLOR][STATE][FIELD] ranges
			function(cb){
				var ti = 0;
				var ci = 0;
				//validate recursively
				function doVal(){
					var colors = ['b', 'g', 'r'];

					for( var fi in colors ){
						var val = hotelGroupData.mediaConfiguration.theme[themeFields[ti]][colorFields[ci]][colors[fi]];
						if( val < 0 || val > 255 || !check.integer(val)){
							return cb(errors.newError('hgv-inv-val', {key: 'mediaConfiguration.theme.' + themeFields[ti] + '.' + colorFields[ci] + '.' + colors[fi]}));
						}
					}

					var alpha = hotelGroupData.mediaConfiguration.theme[themeFields[ti]][colorFields[ci]].alpha;
					if( alpha < 0 || alpha > 1){
						return cb(errors.newError('hgv-inv-val', {key: 'mediaConfiguration.theme.' + themeFields[ti] + '.' + colorFields[ci] + '.alpha'}));
					}
					if( colorFields[ci+1] !== undefined ){
						ci++;
						doVal();
					}else if( themeFields[ti+1] !== undefined ){
						ci = 0;
						ti++;
						doVal();
					}else{
						cb();
					}
				}

				doVal();

			}
		],
		[],
		function(err){
			callback(err);
		}
	);

}

exports.validateMediaConfiguration = validateMediaConfiguration;

/**
 * Validate the hotel group info
 * @param hotelGroupData
 * @param validationType
 * @param callback
 */
function validateInfo(hotelGroupData, validationType, callback){

	//validate with given functions until first fails or al pass
	validationUtils.validateWithArrayOfFunctionsWithCallbacks(
		[
			//if info is valid object
			function(hotelGroupData, cb){
				if( !check.object(hotelGroupData.info) ){
					return cb(errors.newError('hgv-inv-type', {key: 'info'}));
				}
				cb(null);
			},

			//if has all keys
			function(hotelGroupData, cb){
				validationUtils.validateHasKeys(['localized'], hotelGroupData.info, function(errKey){
					if(errKey){
						return cb(errors.newError('hgv-key-miss', {key: 'info.' + errKey}));
					}
					cb(null);
				});
			},

			//if localized is object
			function(hotelGroupData, cb){
				if( !check.object(hotelGroupData.info.localized) ){
					return cb(errors.newError('hgv-inv-type', {key: 'info.localized'}));
				}
				cb(null);
			},



			//if localized language codes are correct
			function(hotelGroupData, cb){
				for(var li in hotelGroupData.info.localized){
					if(!langUtils.isValidLanguageCode(li)){
						return cb(errors.newError('hgv-inf-inv-lang', {key: 'info.localized[' +li+ ']'}));
					}
				}
				cb(null);
			},

			//if localized has default language
			function(hotelGroupData, cb){
				validationUtils.validateHasKeys([hotelGroupData.defaultLanguage], hotelGroupData.info.localized, function(errKey){
					if(errKey){
						return cb(errors.newError('hgv-inf-miss-def-lang', {key: 'info.localized[' + errKey + ']'}));
					}
					cb(null);
				});
			},

			//if localized items have required fields (or defaults)
			function(hotelGroupData, cb){
				var err = null;
				var callB = function(li){
					return function(errKey){
						if(errKey){
							err = errors.newError('hgv-key-miss', {key: 'info.localized[' + li + '].' + errKey});
						}
					};
				};
				for(var li in hotelGroupData.info.localized){
					validationUtils.validateLocalizedOrDefaultKeys(
						['name', 'logo', 'backgroundImage'],
						hotelGroupData.info,
						hotelGroupData.info.localized[li],
						callB(li)
					);
					if(err){
						break;
					}
				}
				cb(err);
			},

			//check names
			function(hotelGroupData, cb){
				var names = [];
				if(hotelGroupData.info.hasOwnProperty('name')){
					names.push([hotelGroupData.info.name, 'name']);
				}

				for(var li in hotelGroupData.info.localized){
					if(hotelGroupData.info.localized[li].hasOwnProperty('name')){
						names.push([hotelGroupData.info.localized[li].name, 'localized[' + li + '].name']);
					}
				}

				for(var ni in names){
					if( !check.string(names[ni][0]) ){
						return cb(errors.newError('hgv-inv-type', {key: 'info.' + names[ni][1]}));
					}
				}

				for(var ni2 in names){
					if( !check.unemptyString(names[ni2][0]) ){
						return cb(errors.newError('hgv-inv-val', {key: 'info.' + names[ni2][1]}));
					}
				}

				cb(null);

			},

			//check images
			function(hotelGroupData, cb){
				var images = [];
				if(hotelGroupData.info.hasOwnProperty('logo')){
					images.push([hotelGroupData.info.logo, 'info.logo']);
				}
				if(hotelGroupData.info.hasOwnProperty('backgroundImage')){
					images.push([hotelGroupData.info.backgroundImage, 'info.backgroundImage']);
				}

				for(var li in hotelGroupData.info.localized){
					if(hotelGroupData.info.localized[li].hasOwnProperty('logo')){
						images.push([hotelGroupData.info.localized[li].logo, 'info.localized[' + li + '].logo']);
					}
					if(hotelGroupData.info.localized[li].hasOwnProperty('backgroundImage')){
						images.push([hotelGroupData.info.localized[li].backgroundImage, 'info.localized[' + li + '].backgroundImage']);
					}
				}

				//validate images
				var ni = 0;
				function imgVal(){
					mediaVal.validateImageObject(images[ni][0], 'hotelGroupImage', function(err){
						if(err){
							err.data.debuggingData.key = images[ni][1] + (err.data.debuggingData.key ? '.' + err.data.debuggingData.key : '');
							return cb(err);
						}else if( images[ni+1] !== undefined ){
							ni++;
							imgVal();
						}else{
							cb(null);
						}
					}, {hotelGroupId: hotelGroupData.hotelGroupId});
				}

				imgVal();

			}

		],
		[
			hotelGroupData
		],
		function(err){
			return callback(err);
		}
	);

}


/**
 * Validate supported languages
 * @param hotelGroupData
 * @param validationType
 * @param callback
 * @returns {*}
 */
function validateSupportedLanguages(hotelGroupData, validationType, callback){
	if( !Array.isArray(hotelGroupData.supportedLanguages) ){
		return callback(errors.newError('hgv-inv-type', {key: 'supportedLanguages', expectedType: 'array'}));
	}
	for( var lkey in hotelGroupData.supportedLanguages){
		if( !check.string(hotelGroupData.supportedLanguages[lkey]) ){
			return callback(errors.newError('hgv-inv-type', {key: 'supportedLanguages[' + lkey + ']', expectedType: 'string'}));
		}

		try{
			langUtils.getLanguageObject(hotelGroupData.supportedLanguages[lkey]);
		}catch(e){
			return callback(errors.newError('hgv-inv-val', {key: 'supportedLanguages[' + lkey + ']', givenValue: hotelGroupData.supportedLanguages[lkey]}));
		}

	}
	return callback(null);

}

/**
 * Validate Google Analytics Tracking Id
 * @param hotelGroupData
 * @param validationType
 * @param callback
 * @returns {*}
 */
function validateGaTrackingId(hotelGroupData, validationType, callback){
	if(hotelGroupData.gaTrackingId && !check.unemptyString(hotelGroupData.gaTrackingId)){
		return callback(errors.newError('hgv-inv-val', {key: 'gaTrackingId', givenValue: hotelGroupData.gaTrackingId}));
	}
	return callback(null);

}

function validateEmailTemplates(hotelGroupData, validationType, callback) {

	validationUtils.validateWithArrayOfFunctionsWithCallbacks(
		[
			//validate if correct object
			function(hotelGroupData, cb){
				if( !check.object(hotelGroupData.emailTemplates) ){
					return cb(errors.newError('hgv-inv-type', {key: 'emailTemplates'}));
				}
				cb(null);
			},

			//validate if has required templates
			function(hotelGroupData, cb){
				validationUtils.validateHasKeys(
					['emailVerification', 'passwordReset'],
					hotelGroupData.emailTemplates,
				function(errKey){
					if(errKey){
						return cb(errors.newError('hgv-key-miss', {key: 'emailTemplates[' + errKey + ']'}));
					}
					cb(null);

				});
			},

			//validate all templates
			function(hotelGroupData, cb){
				var promises = [];

				function doVal(p, data, t){

					messagingVal.validateEmailTemplate(data, function(err){
						if(err){
							err.data.debuggingData.key = 'emailTemplates[' + t + ']' + (err.data.debuggingData.key ? '.' + err.data.debuggingData.key : '');
							return p.reject(err);
						}
						//check for default language
						if( !data.localized.hasOwnProperty(hotelGroupData.defaultLanguage) ){
							return p.reject(errors.newError('hgv-emt-no-def-lang', {key: 'emailTemplates[' + t + '].localized', defaultLanguage: hotelGroupData.defaultLanguage}));
						}
						p.resolve();
					});
				}
				for(var ti in hotelGroupData.emailTemplates){
					var p = q.defer();
					promises.push(p.promise);
					doVal(p, hotelGroupData.emailTemplates[ti], ti);
				}

				q.all(promises).done(function(){
					cb(null);
				}, function(err){
					cb(err);
				});
			}
		],
		[
			hotelGroupData
		],
		function(err){
			callback(err);
		}
	);


}


exports.validateHotelGroupData = function(hotelGroupData, validationType, callback){

	var validationTypes = {
		full: {
			functions: [
				validateRootKeys,
				validateHotelGroupId,
				validateSupportedLanguages,
				validateDefaultLanguage,
				validateTranslations,
				validateCategories,
				validateMainMenu,
				validateInfo,
				validateApiKeys,
				validateMediaConfiguration,
				validateEmailTemplates,
				validateViewConfiguration,
				validateGaTrackingId,
				validateSearchConfiguration,
			],
			context: {
				requireCategoryBackground: true,
				requireCategoryButtonImage: true,
				requireMenuItemBackground: true,
				requireMenuItemButtonImage: true
			}
		}
	};

	validationTypes.testData = {
		functions: validationTypes.full.functions,
		context: {
			requireCategoryBackground: false,
			requireCategoryButtonImage: false,
			requireMenuItemBackground: false,
			requireMenuItemButtonImage: false
		}
	};

	validationTypes.insert = {
		functions: validationTypes.full.functions,
		context: {
			requireCategoryBackground: false,
			requireCategoryButtonImage: false,
			requireMenuItemBackground: false,
			requireMenuItemButtonImage: false
		}
	};

	validationTypes.update = {
		functions: validationTypes.full.functions,
		context: {
			requireCategoryBackground: false,
			requireCategoryButtonImage: false,
			requireMenuItemBackground: false,
			requireMenuItemButtonImage: false
		}
	};


	if(validationTypes.hasOwnProperty(validationType)){

		var checkIndex = 0;
		var valCaller = function(){
			var context = validationTypes[validationType].context;
			//call first check
			validationTypes[validationType].functions[checkIndex](hotelGroupData, validationType, function(err, res){
				if(err){
					callback(err);
					//call next check
				}else if( validationTypes[validationType].functions[checkIndex+1] !== undefined ){
					checkIndex++;
					valCaller();
				}else{
					callback(null, res);
				}
			}, context);
		};

		if( !check.object(hotelGroupData) ){
			return callback(errors.newError('hgv-inv-type', {key: 'rootObject'}));
		}

		valCaller();

	}else{
		callback(errors.newError('hgv-inv-val-type'));
	}

};
