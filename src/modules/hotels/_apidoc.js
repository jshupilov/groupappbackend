/**
 * @apiDefine HotelStructure
 * @apiVersion 0.0.1
 * @apiParam (Basic hotel structure) {String} href An URL, which points to a service,
 * which returns only this resource, with detail information.
 * @apiParam (Basic hotel structure) {Link} brochure Link object, which points to a service,
 * which returns hotels (with same filters as request has specified + paginated) with `brochure` information
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiParam (Basic hotel structure) {String} id Identification code of this hotel
 * @apiParam (Basic hotel structure) {Object} coordinates An object containing the coordinates of this hotel.
 * @apiParam (Basic hotel structure) {Number{-180 - 180}} coordinates.lat Latitude of this hotels location
 * @apiParam (Basic hotel structure) {Number{-180 - 180}} coordinates.long Longitude of this hotels location
 *
 * @apiParam (Basic hotel structure) {Object} localized Object, where each key represents
 * localized information about this hotel in a certain language
 * @apiParam (Basic hotel structure) {Object} localized.LANGUAGE Information about this hotel in language,
 * defined by `LANGUAGE` key.
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.name Name of this hotel
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.icon Icon image URL for this hotel
 * ([About images](#api-1_Basics-basicsImages))
 * @apiParam (Basic hotel structure) {Object} localized.LANGUAGE.location An object containing the location info of this hotel.
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.location.city Name of the city, where this hotel is at
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.location.country Name of the country, where this hotel is at
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.location.fullAddress Full address of the hotel
 *
 *
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Number} localized.LANGUAGE.stars Hotel's rating as number of stars
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE.contacts Contact information for this hotel
 *
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE.contacts.primary Object containing primary contact information to show to clients
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE.contacts.primary.phone Contact phone number
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE.contacts.primary.email Contact e-mail address
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String} localized.LANGUAGE.contacts.primary.website Website URL
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object[]} localized.LANGUAGE.description Array of description blocks as Objects
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String[]/Object[]} localized.LANGUAGE.description.heading Heading of this description block.
 * Represented as list of strings or objects.
 * All strings in this array should be glued together without a separator, Object elements should be ignored for now.
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String[]/Object[]} localized.LANGUAGE.description.text Body text of this description block
 * Represented as list of strings or objects.
 * All strings in this array should be glued together without a separator, Object elements should be ignored for now.
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object[]} localized.LANGUAGE.images Object, containing links to images
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String} localized.LANGUAGE.images.highRes The high-resolution image URL
 * ([About images](#api-1_Basics-basicsImages))
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String} localized.LANGUAGE.images.lowRes The low-resolution image URL
 * ([About images](#api-1_Basics-basicsImages))
 *
 * @apiParamExample {json} Example basic hotel resource
 {
  "href": "http://localhost:8000/v1/test-hgid/hotels/1",
  "id": "1",
  "coordinates": {
    "lat": 59.438591,
    "long": 24.747744
  },
  "localized": {
    "en_GB": {
      "name": "Schlossle",
      "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.png",
      "location": {
        "city": "Tallinn",
        "country": "Estonia",
        "fullAddress": "Pühavaimu 13/15, 10123 Tallinn, Estonia"
      }
    },
    "et_EE": {
      "name": "Schlössle",
      "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.jpg",
      "location": {
        "city": "Tallinn",
        "country": "Eesti",
        "fullAddress": "Pühavaimu 13/15, 10123 Tallinn, Eesti"
      }
    }
  },
  "brochure": {
    "href": "http://localhost:8000/v1/test-hgid/categories/reg-eur-est/hotels?detail=brochure&limit=1&offset=0"
  }
}
 * @apiParamExample {json} Example brochure hotel resource
 {
  "href": "http://localhost:8000/v1/test-hgid/hotels/1",
  "id": "1",
  "coordinates": {
    "lat": 59.438591,
    "long": 24.747744
  },
  "localized": {
    "en_GB": {
      "name": "Schlossle",
      "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.png",
      "location": {
        "city": "Tallinn",
        "country": "Estonia",
        "fullAddress": "Pühavaimu 13/15, 10123 Tallinn, Estonia"
      },
      "stars": 5,
      "contacts": {
        "primary": {
          "email": "sch@schlossle-hotels.com",
 "phone": "+372 699 7700",
 "website": "http://www.schloesslehotel.com"
 }
 },
 "description": [
 {
	"text": [
		{
			"styles": ["predefinedStyle1"],
			"inLineImages": []
		},
		"Tallinn",
		"in the 13th century was a thriving Hanseatic trading centre where merchants and nobles from all across Europe came to do business . This fortified medieval city was one of the most famous ports in the eastern world, home for goods brought from ships that sailed from as far away as Africa, and by horse and carriage from both east and west . Tallinn was and still is a trading city, and many of the old medieval buildings are a continuing tribute to that legacy "
	],
	"heading": ["History"]
},
 {
	"text": [
		{
			"link": "http://www.schloesslehotel.com",
			"styles": ["predefinedStyle2"]
		},
		"The Schlössle Hotel",
		"is a luxurious 5-star hotel in Tallinn’s Old Town. The 23-room boutique hotel has luxurious furnishings and attentive staff to ensure that you will have a memorable stay in this charming hotel . Nestled in the heart of Tallinn ’ s Old Town, the Schlössle is a delightful boutique hotel that will take you back to Estonia ’ s medieval past . "
	],
	"heading": ["Services"]
}
 ],
 "images": [
 {
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle.jpg",
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle.jpg"
},
 {
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle2.jpg",
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle2.jpg"
}
 ]
 },
 "et_EE": {
      "name": "Schlössle",
      "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.jpg",
      "location": {
        "city": "Tallinn",
        "country": "Eesti",
        "fullAddress": "Pühavaimu 13/15, 10123 Tallinn, Eesti"
      },
      "stars": 5,
      "contacts": {
        "primary": {
          "email": "sch@schlossle-hotels.com",
 "phone": "+372 699 7700",
 "website": "http://www.schloesslehotel.com/et/"
 }
 },
 "description": [
 {
	"text": [
		{
			"link": "http://www.schloesslehotel.com/et",
			"styles": ["predefinedStyle2"]
		},
		"Hotel Schlössle",
		" on luksuslik 5-e tärni hotell Tallinna vanalinnas Pühavaimu tänaval. Hotellis on 23 luksuslikult sisustatud hotellituba, hotelli tähelepanelik personal teeb kõik et viibimine hotellis oleks külalistele igati meeldiv."
	],
	"heading": ["Teenused"]
}
 ],
 "images": [
 {
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle.jpg",
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle.jpg"
},
 {
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle2.jpg",
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle2.jpg"
}
 ]
 }
 }
 }
 *
 */

/**
 * @apiDefine HotelQuery
 * @apiVersion 0.0.1
 * @apiParam (Hotel query parameters) {String=full,brochure} [detail] Specifies the amount of detail,
 * which should be returned for each hotel.
 *
 * If this parameter is given, with a value of `full` (**not available yet**), then Hotel object(s) will be returned in full detail.
 *
 * If this parameter is given, with a value of `brochure`, then Hotel object(s) will be returned with details,
 * which are needed for displaying a brochure.
 *
 * If this parameter is omitted then only basic information is returned about hotel.
 */

/**
 * @apiDefine HotelCollectionQuery
 * @apiVersion 0.0.1
 * @apiParam (Hotel collection query parameters) {Number{-180 - 180}} lat Latitude of a location,
 * near which the hotel should be located. **NB! Should be used along with `long` and `radius` query parameters.**
 * @apiParam (Hotel collection query parameters) {Number{-180 - 180}} long Longitude of a location,
 * near which the hotel should be located. **NB! Should be used along with `lat` and `radius` query parameters.**
 * @apiParam (Hotel collection query parameters) {Number{0 - 6371}} radius=10 The radius in KM,
 * inside which the hotels should be located (maximum distance from given coordinate - `lat` & `long`).
 * **NB! Should be used along with `lat` and `long` query parameters.**
 * @apiParam (Hotel collection query parameters) {String} search A search phrase. If given, then
 * a search is performed, and only hotels, which match the `search phrase`, will be returned.
 * The string passed with this parameter must be url encoded and in UTF-8 encoding. (About urlencode)[http://www.w3schools.com/tags/ref_urlencode.asp]
 *
 */

/**
 * @api {get} - 1. Hotels resource
 * @apiName hotelResource
 * @apiVersion 0.0.1
 * @apiGroup 4_Hotels
 * @apiDescription Hotel resource is the main object in this API. It represents a hotel in a hotel group.
 * By default a hotel is returned with basic set of properties and you have to specify the `?detail=`
 * query parameter to get the Hotel object in more detail.
 *
 * @apiUse HotelStructure
 * @apiUse HotelQuery
 *
 *
 */

/**
 * @api {get} /hotels/ 1.1 Get all hotels
 * @apiName getHotels
 * @apiVersion 0.0.1
 * @apiGroup 4_Hotels
 * @apiDescription This method returns a Collection of **all** Hotel objects.
 *
 * [About collections](#api-1_Basics-basicsCollections)
 *
 * @apiUse CollectionQueryParams
 * @apiUse HotelCollectionQuery
 * @apiUse HotelQuery
 *
 * @apiExample {html} Get all hotels request example:
 * GET /v1/test-hgid/en/hotels HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 * @apiExample {html} Get all hotels near a location:
 * GET /v1/test-hgid/en/hotels?lat=59.438575&long=24.747423 HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 * @apiExample {html} Search hotels with Luxury rooms:
 * GET /v1/test-hgid/en/hotels?search=luxury%20rooms HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 *
 * @apiSuccessExample {json} Get all hotels response example:
 {
  "href": "http://localhost:8000/v1/test-hgid/hotels",
  "offset": 0,
  "limit": 10,
  "total": 2,
  "items": [
    {
      "href": "http://localhost:8000/v1/test-hgid/hotels/1",
      "id": "1",
      "coordinates": {
        "lat": 59.438591,
        "long": 24.747744
      },
      "localized": {
        "en_GB": {
          "name": "Schlossle",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.png",
          "location": {
            "city": "Tallinn",
            "country": "Estonia",
            "fullAddress": "Pühavaimu 13/15, 10123 Tallinn, Estonia"
          }
        },
        "et_EE": {
          "name": "Schlössle",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.jpg",
          "location": {
            "city": "Tallinn",
            "country": "Eesti",
            "fullAddress": "Pühavaimu 13/15, 10123 Tallinn, Eesti"
          }
        }
      },
      "brochure": {
        "href": "http://localhost:8000/v1/test-hgid/hotels?detail=brochure&limit=1&offset=0"
      }
    },
    {
      "href": "http://localhost:8000/v1/test-hgid/hotels/2",
      "id": "2",
      "coordinates": {
        "lat": 59.436968,
        "long": 24.742877
      },
      "localized": {
        "en_GB": {
          "name": "St Petersbourg",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/stpeter_logo.png",
          "location": {
            "city": "Tallinn",
            "country": "Estonia",
            "fullAddress": "Rataskaevu 7, 10123 Tallinn, Estonia"
          }
        },
        "et_EE": {
          "name": "St Petersbourg",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/stpeter_logo.png",
          "location": {
            "city": "Tallinn",
            "country": "Eesti",
            "fullAddress": "Rataskaevu 7, 10123 Tallinn, Eesti"
          }
        }
      },
      "brochure": {
        "href": "http://localhost:8000/v1/test-hgid/hotels?detail=brochure&limit=1&offset=1"
      }
    }
  ]
}
 *
 */

/**
 * @api {get} /hotels/:hotelId 1.2 Get a hotel
 * @apiName getHotel
 * @apiVersion 0.0.1
 * @apiGroup 4_Hotels
 * @apiDescription This method returns a single Hotel, which has the given id.
 *
 * @apiExample {html} Get a hotel (with 'brochure' details) request example:
 * GET /v1/test-hgid/en/hotels/33?detail=brochure HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 * @apiUse HotelQuery
 *
 * @apiSuccessExample {json} Get a hotel (with 'brochure' details) response example:
 {
  "href": "http://localhost:8000/v1/test-hgid/hotels/1",
  "id": "1",
  "coordinates": {
    "lat": 59.438591,
    "long": 24.747744
  },
  "localized": {
    "en_GB": {
      "name": "Schlossle",
      "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.png",
      "location": {
        "city": "Tallinn",
        "country": "Estonia",
        "fullAddress": "Pühavaimu 13/15, 10123 Tallinn, Estonia"
      },
      "stars": 5,
      "contacts": {
        "primary": {
          "email": "sch@schlossle-hotels.com",
 "phone": "+372 699 7700",
 "website": "http://www.schloesslehotel.com"
 }
 },
 "description": [
 {
	"text": [
		{
			"styles": ["predefinedStyle1"],
			"inLineImages": []
		},
		"Tallinn",
		"in the 13th century was a thriving Hanseatic trading centre where merchants and nobles from all across Europe came to do business . This fortified medieval city was one of the most famous ports in the eastern world, home for goods brought from ships that sailed from as far away as Africa, and by horse and carriage from both east and west . Tallinn was and still is a trading city, and many of the old medieval buildings are a continuing tribute to that legacy "
	],
	"heading": ["History"]
},
 {
	"text": [
		{
			"link": "http://www.schloesslehotel.com",
			"styles": ["predefinedStyle2"]
		},
		"The Schlössle Hotel",
		"is a luxurious 5-star hotel in Tallinn’s Old Town. The 23-room boutique hotel has luxurious furnishings and attentive staff to ensure that you will have a memorable stay in this charming hotel . Nestled in the heart of Tallinn ’ s Old Town, the Schlössle is a delightful boutique hotel that will take you back to Estonia ’ s medieval past . "
	],
	"heading": ["Services"]
}
 ],
 "images": [
 {
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle.jpg",
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle.jpg"
},
 {
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle2.jpg",
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle2.jpg"
}
 ]
 },
 "et_EE": {
      "name": "Schlössle",
      "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.jpg",
      "location": {
        "city": "Tallinn",
        "country": "Eesti",
        "fullAddress": "Pühavaimu 13/15, 10123 Tallinn, Eesti"
      },
      "stars": 5,
      "contacts": {
        "primary": {
          "email": "sch@schlossle-hotels.com",
 "phone": "+372 699 7700",
 "website": "http://www.schloesslehotel.com/et/"
 }
 },
 "description": [
 {
	"text": [
		{
			"link": "http://www.schloesslehotel.com/et",
			"styles": ["predefinedStyle2"]
		},
		"Hotel Schlössle",
		" on luksuslik 5-e tärni hotell Tallinna vanalinnas Pühavaimu tänaval. Hotellis on 23 luksuslikult sisustatud hotellituba, hotelli tähelepanelik personal teeb kõik et viibimine hotellis oleks külalistele igati meeldiv."
	],
	"heading": ["Teenused"]
}
 ],
 "images": [
 {
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle.jpg",
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle.jpg"
},
 {
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/t_q10,w_<width>,h_<height>,c_scale/schlossle2.jpg",
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle2.jpg"
}
 ]
 }
 }
 }
 *
 */

/**
 * @api {get} /categories/:categoryId/hotels 1.3 Get hotels in a category
 * @apiName getCategoryHotels
 * @apiVersion 0.0.1
 * @apiGroup 4_Hotels
 * @apiDescription This method returns a Collection of Hotel objects, which belong to given category.
 *
 * [About collections](#api-1_Basics-basicsCollections)
 *
 * @apiUse CollectionQueryParams
 * @apiUse HotelCollectionQuery
 * @apiUse HotelQuery
 *
 * @apiExample {html} Get category hotels request example:
 * GET /v1/test-hgid/en/categories/2/hotels HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 *
 * @apiSuccessExample {json} Get category hotels response example:
 {
  "href": "http://localhost:8000/v1/test-hgid/categories/reg-eur/hotels",
  "offset": 0,
  "limit": 10,
  "total": 2,
  "items": [
    {
      "href": "http://localhost:8000/v1/test-hgid/hotels/1",
      "id": "1",
      "coordinates": {
        "lat": 59.438591,
        "long": 24.747744
      },
      "localized": {
        "en_GB": {
          "name": "Schlossle",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.png",
          "location": {
            "city": "Tallinn",
            "country": "Estonia",
            "fullAddress": "Pühavaimu 13/15, 10123 Tallinn, Estonia"
          }
        },
        "et_EE": {
          "name": "Schlössle",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/schlossle_logo.jpg",
          "location": {
            "city": "Tallinn",
            "country": "Eesti",
            "fullAddress": "Pühavaimu 13/15, 10123 Tallinn, Eesti"
          }
        }
      },
      "brochure": {"href": "http://localhost:8000/v1/test-hgid/categories/reg-eur/hotels?detail=brochure&limit=1&offset=0"}
    },
    {
      "href": "http://localhost:8000/v1/test-hgid/hotels/2",
      "id": "2",
      "coordinates": {
        "lat": 59.436968,
        "long": 24.742877
      },
      "localized": {
        "en_GB": {
          "name": "St Petersbourg",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/stpeter_logo.png",
          "location": {
            "city": "Tallinn",
            "country": "Estonia",
            "fullAddress": "Rataskaevu 7, 10123 Tallinn, Estonia"
          }
        },
        "et_EE": {
          "name": "St Petersbourg",
          "icon": "http://res.cloudinary.com/cardola-estonia/image/upload/w_<width>,h_<height>,c_scale/stpeter_logo.png",
          "location": {
            "city": "Tallinn",
            "country": "Eesti",
            "fullAddress": "Rataskaevu 7, 10123 Tallinn, Eesti"
          }
        }
      },
      "brochure": {"href": "http://localhost:8000/v1/test-hgid/categories/reg-eur/hotels?detail=brochure&limit=1&offset=1"}
    }
  ]
}
 *
 */

/**
 * @apiDefine HotelStructure
 * @apiVersion 0.4.7
 * @apiParam (Basic hotel structure) {String} href An URL, which points to a service,
 * which returns only this resource, with detail information.
 * @apiParam (Basic hotel structure) {Link} brochure Link object, which points to a service,
 * which returns hotels (with same filters as request has specified + paginated) with `brochure` information
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiParam (Basic hotel structure) {String} id Identification code of this hotel
 * @apiParam (Basic hotel structure) {Object} coordinates An object containing the coordinates of this hotel.
 * @apiParam (Basic hotel structure) {Number{-180 - 180}} coordinates.lat Latitude of this hotels location
 * @apiParam (Basic hotel structure) {Number{-180 - 180}} coordinates.long Longitude of this hotels location
 *
 *
 * @apiParam (Basic hotel structure) {Object} localized Object, where each key represents
 * localized information about this hotel in a certain language
 * @apiParam (Basic hotel structure) {Object} localized.LANGUAGE Information about this hotel in language,
 * defined by `LANGUAGE` key.
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.name Name of this hotel
 * @apiParam (Basic hotel structure) {Object} localized.LANGUAGE.location An object containing the location info of this hotel.
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.location.city Name of the city, where this hotel is at
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.location.country Name of the country, where this hotel is at
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.location.fullAddress Full address of the hotel
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.location.shortAddress Short address of the hotel (neighborhood, city, country)
 * @apiParam (Basic hotel structure) {Image}  localized.LANGUAGE.image Image object, gives primary image of the hotel, which should be shown in listings
 * ([About images](#api-1_Basics-basicsImages))
 *
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object[]} gallery Image objects collection,
 * each one represents image in gallery
 * ([About images](#api-1_Basics-basicsImages))
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Number} stars Hotel's rating as number of stars
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE.contacts Contact information for this hotel
 *
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE.contacts.primary Object containing primary contact information to show to clients
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE.contacts.primary.phone Contact phone number
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE.contacts.primary.email Contact e-mail address
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String} localized.LANGUAGE.contacts.primary.website Website URL
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object[]} localized.LANGUAGE.description Array of description blocks as Objects
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String[]/Object[]} localized.LANGUAGE.description.heading Heading of this description block.
 * Represented as list of strings or objects.
 * All strings in this array should be glued together without a separator, Object elements should be ignored for now.
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String[]/Object[]} localized.LANGUAGE.description.text Body text of this description block
 * Represented as list of strings or objects.
 * All strings in this array should be glued together without a separator, Object elements should be ignored for now.
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String} vhSiteKey The VirtualHotel APP keyword, if virtual
 * hotel app exists for this hotel, if not, then this value is false.
 *
 *
 * @apiParamExample {json} Example hotel resource
 {
  "href": "http://localhost:8000/v1/test-hgid/hotels/ernst",
  "id": "ernst",
  "coordinates": {
    "lat": 50.941857,
    "long": 6.956522
  },
  "localized": {
    "en_GB": {
      "name": "Excelsior Hotel Ernst",
      "location": {
        "city": "Cologne",
        "country": "Germany",
        "fullAddress": "Domplatz/Trankgasse 1-5, 50667 Cologne, Germany",
        "shortAddress": "Alstadt-Nord, Cologne, Germany"
      },
      "buttonImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image"
      },
      "contacts": {
        "primary": {
          "email": " info@excelsior-hotel-ernst.de",
 "phone": "+49 (0)221 270 1",
 "website": "http://www.excelsiorhotelernst.com/en"
 }
 },
 "description": [
 {
	"text": [
		{
			"style": null,
			"value": "The Excelsior Hotel Ernst is Cologne’s Grand Hotel next to the Cathedral. We provide individualized services to respond to the uniqueness of every guest and employee. We maintain the tradition of hospitality and remain modern by applying innovation. As Grand Hotel in the heart of Cologne, we actively participate in urban life and we are committed to art and culture. Your individuality is our Excellence."
		}
	],
	"heading": [
		{
			"style": null,
			"value": "About us"
		}
	]
},
 {
	"text": [
		{
			"style": null,
			"value": "Carl Ernst, Royal Restorer of the central station, was builder and owner of the Hotel Ernst in the city center. The opening ceremony took place on May 16th 1863. In 1871, after as little as eight years, he sold the Hotel Ernst to Friedrich Kracht. Friedrich Kracht moved from Belgium to Cologne to manage the hotel but he died four years later. His wife and his son Carl took over the management of the house.  In those days, the Grand Hotel was already the first choice of prominent guests."
		}
	],
	"heading": [
		{
			"style": null,
			"value": "History"
		}
	]
}
 ],
 "gallery": [
 {
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1",
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1"
},
 {
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0",
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0"
}
 ]
 }
 },
 "stars": 5,
 "vhSiteKey": "devtallinn"
 }
 *
 */
