/**
 * @apiDefine HotelStructure
 * @apiVersion 1.7.4
 * @apiParam (Basic hotel structure) {String} href An URL, which points to a service,
 * which returns only this resource, with detail information.
 * @apiParam (Basic hotel structure) {Link} brochure Link object, which points to a service,
 * which returns hotels (with same filters as request has specified + paginated) with `brochure` information
 * ([About links](#api-1_Basics-basicsLinks))
 * @apiParam (Basic hotel structure) {String} id Identification code of this hotel
 * @apiParam (Basic hotel structure) {Object} coordinates An object containing the coordinates of this hotel.
 * @apiParam (Basic hotel structure) {Number{-90 - 90}} coordinates.lat Latitude of this hotels location
 * @apiParam (Basic hotel structure) {Number{-180 - 180}} coordinates.long Longitude of this hotels location
 *
 *
 * @apiParam (Basic hotel structure) {String} contacts Url to get dynamic contact details by client ip address.
 * @apiParam (Basic hotel structure) {Object} localized Object, where each key represents
 * localized information about this hotel in a certain language
 * @apiParam (Basic hotel structure) {Object} localized.LANGUAGE Information about this hotel in language,
 * defined by `LANGUAGE` key.
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.name Name of this hotel
 * @apiParam (Basic hotel structure) {Object} localized.LANGUAGE.location An object containing the location info of this hotel.
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.location.city Name of the city, where this hotel is at
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.location.country Name of the country, where this hotel is at
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.location.fullAddress Full address of the hotel
 * @apiParam (Basic hotel structure) {String} localized.LANGUAGE.location.shortAddress Short address of the hotel (neighborhood, city, country)
 * @apiParam (Basic hotel structure) {Image}  localized.LANGUAGE.image Image object, gives primary image of the hotel, which should be shown in listings
 * ([About images](#api-1_Basics-basicsImages))
 *
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object[]} gallery Image objects collection,
 * each one represents image in gallery
 * ([About images](#api-1_Basics-basicsImages))
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE.contacts Contact information for this hotel
 *
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE.contacts.primary Object containing primary contact information to show to clients
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String} localized.LANGUAGE.contacts.primary.phone Contact phone number
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String} localized.LANGUAGE.contacts.primary.email Contact e-mail address
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String} localized.LANGUAGE.contacts.primary.website Website URL
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String/NULL} localized.LANGUAGE.bookingUrl Url to the direct booking form of this hotel, if missing, then it equals `NULL`.
 * This url includes following tokens, which should be replaced and user should be directed to the final url:
 *
 * `<StartDate>` Date, when the booking starts, it is date represented as `mm/dd/yyyy` and uriEncoded, meaning the `/` is converted to `%2f2` and the output format will be like `11%2f25%2f2015`
 *
 * `<StartDateIso>` Date, when the booking starts in ISO format, it is date represented as `yyyy-mm-dd` for example `2015-09-21`
 *
 * *NB! StartDate and StartDateIso are never together int the URL, it is either one or another*
 *
 * `<NoOfNights>` Integer, stating how many nights is the booking requested for
 *
 * `<NoOfAdults>` Integer, stating for how many adults the booking is requested for
 *
 * Example value would be like:
 *
 * `https://booking.slh.com/en-GB/Room/Availability/?HotelCode=HUTLLTE&StartDate=<StartDate>&NoOfNights=<NoOfNights>&NoOfAdults=<NoOfAdults>`
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object[]} localized.LANGUAGE.description Array of description blocks as Objects
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String[]/Object[]} localized.LANGUAGE.description.heading Heading of this description block.
 * Represented as list of strings or objects.
 * All strings in this array should be glued together without a separator, Object elements should be ignored for now.
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String[]/Object[]} localized.LANGUAGE.description.text Body text of this description block
 * Represented as list of strings or objects.
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object[]} localized.LANGUAGE.menu Hotel actions menu as array of Objects
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String=web,phone,map,share,book} localized.LANGUAGE.menu.action Identifier of the action
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Boolean=true,false} localized.LANGUAGE.menu.isEnabled Controls if the button is usable (active) or in disabled state (inactive)
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String} localized.LANGUAGE.menu.name Display name for this action
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {Object} localized.LANGUAGE.menu.data Contain properties of this action. Every action has custom properties.
 *
 * `web` has `url` - url to hotel website might be string
 *
 * `phone` has `phone` - phone number for calling to hotel
 *
 * `map` has `lat` and `long` - hotel coordinate values. Latitude and Longitude
 *
 * `share` has `name` and `gallery` - Name of hotel and hotel gallery for sharing in social networks
 *
 * `book` has `url` and `type` - Url for booking in hotel and Type to know, which booking method is used. If type equal to `url`, then should find `url` property for
 * booking
 *
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String} vhSiteKey The VirtualHotel APP keyword, if virtual
 * hotel app exists for this hotel, if not, then this value is `false`.
 * @apiParam ('Brochure' hotel structure (Extra for basic)) {String} gaTrackingId The Google Analytics tracking code (UA-....), if missing then this value is `false`
 *
 *
 * @apiParamExample {json} Example hotel resource
 {
  "href": "http://localhost:8000/v1/test-hgid/hotels/ernst",
  "id": "ernst",
  "coordinates": {
    "lat": 50.941857,
    "long": 6.956522
  },
  "localized": {
    "en_GB": {
      "name": "Excelsior Hotel Ernst",
      "location": {
        "city": "Cologne",
        "country": "Germany",
        "fullAddress": "Domplatz/Trankgasse 1-5, 50667 Cologne, Germany",
        "shortAddress": "Alstadt-Nord, Cologne, Germany"
      },
      "buttonImage": {
        "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image",
        "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image"
      },
      "contacts": {
        "primary": {
          "email": " info@excelsior-hotel-ernst.de",
 "phone": "+49 (0)221 270 1",
 "website": "http://www.excelsiorhotelernst.com/en"
 }
 },
 "bookingUrl": "https://booking.slh.com/en-GB/Room/Availability/?HotelCode=HUBKIGI&StartDate=<StartDate>&NoOfNights=<NoOfNights>&NoOfAdults=<NoOfAdults>",
 "description": [
 {
	"text": [
		{
			"style": null,
			"value": "The Excelsior Hotel Ernst is Cologne’s Grand Hotel next to the Cathedral. We provide individualized services to respond to the uniqueness of every guest and employee. We maintain the tradition of hospitality and remain modern by applying innovation. As Grand Hotel in the heart of Cologne, we actively participate in urban life and we are committed to art and culture. Your individuality is our Excellence."
		}
	],
	"heading": [
		{
			"style": null,
			"value": "About us"
		}
	]
},
 {
	"text": [
		{
			"style": null,
			"value": "Carl Ernst, Royal Restorer of the central station, was builder and owner of the Hotel Ernst in the city center. The opening ceremony took place on May 16th 1863. In 1871, after as little as eight years, he sold the Hotel Ernst to Friedrich Kracht. Friedrich Kracht moved from Belgium to Cologne to manage the hotel but he died four years later. His wife and his son Carl took over the management of the house.  In those days, the Grand Hotel was already the first choice of prominent guests."
		}
	],
	"heading": [
		{
			"style": null,
			"value": "History"
		}
	]
}
 ],
 "gallery": [
 {
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1",
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1"
},
 {
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0",
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0"
}
 ]
 }
 },
 "menu": [
 {
    "action": "web",
    "isEnabled": true,
    "data": {
        "url": "http://www.corinthia.com/hotels/london/"
    }
},
 {
    "action": "phone",
    "isEnabled": true,
    "data": {
        "phone": "+372 628 6500"
    }
},
 {
    "action": "map",
    "isEnabled": true,
    "data": {
        "lat": 51.506551,
        "long": -0.124039
    }
},
 {
    "action": "share",
    "isEnabled": true,
    "data": {
        "name": "Corinthia Hotel London",
        "gallery": [
           {
                "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1",
                "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1"
            },
            {
                "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0",
                "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0"
            }
        ]
    }
},
 {
    "action": "book",
    "isEnabled": true,
    "data": {
        "url": "https://booking.slh.com/en-GB/Room/Availability/?HotelCode=HUTLLTE&StartDate=<StartDate>&NoOfNights=<NoOfNights>&NoOfAdults=<NoOfAdults>",
        "type": "url"
    }
}
 ],
 "vhSiteKey": "devtallinn",
 "gaTrackingId": "UA-XXXXX-XX"
 }
 *
 */

/**
 * @apiDefine HotelQuery
 * @apiVersion 1.7.4
 * @apiParam (Hotel query parameters) {String=full,brochure} [detail] Specifies the amount of detail,
 * which should be returned for each hotel.
 *
 * If this parameter is given, with a value of `full` (**not available yet**), then Hotel object(s) will be returned in full detail.
 *
 * If this parameter is given, with a value of `brochure`, then Hotel object(s) will be returned with details,
 * which are needed for displaying a brochure.
 *
 * If this parameter is omitted then only basic information is returned about hotel.
 */

/**
 * @apiDefine HotelCollectionQuery
 * @apiVersion 1.7.4
 * @apiParam (Hotel collection query parameters) {Number{-180 - 180}} lat Latitude of a location,
 * near which the hotel should be located. **NB! Should be used along with `long` and `radius` query parameters.**
 * @apiParam (Hotel collection query parameters) {Number{-180 - 180}} long Longitude of a location,
 * near which the hotel should be located. **NB! Should be used along with `lat` and `radius` query parameters.**
 * @apiParam (Hotel collection query parameters) {Number{120 - 637100}} radius=10000 The radius in meters,
 * inside which the hotels should be located (maximum distance from given coordinate - `lat` & `long`).
 * 120 is the least effective radius, which works, values under it match with nothing
 * **NB! Should be used along with `lat` and `long` query parameters.**
 * @apiParam (Hotel collection query parameters) {String} search A search phrase. If given, then
 * a search is performed, and only hotels, which match the `search phrase`, will be returned.
 * The string passed with this parameter must be url encoded and in UTF-8 encoding. (About urlencode)[http://www.w3schools.com/tags/ref_urlencode.asp]
 *
 */

/**
 * @api {get} - 1. Hotels resource
 * @apiName hotelResource
 * @apiVersion 1.7.4
 * @apiGroup 4_Hotels
 * @apiDescription Hotel resource is the main object in this API. It represents a hotel in a hotel group.
 * By default a hotel is returned with basic set of properties and you have to specify the `?detail=`
 * query parameter to get the Hotel object in more detail.
 *
 * @apiUse HotelStructure
 * @apiUse HotelQuery
 * @apiUse daoErrors
 * @apiUse getHotelGroupErrors
 * @apiError (Possible error codes) {String} hs-hotel-nf Hotel, with given parameters was not found ([Definition](#error_hs-hotel-nf))
 * @apiError (Possible error codes) {String} hs-err-save Error occurred while saving hotel to database ([Definition](#error_hs-err-save))
 * @apiError (Possible error codes) {String} hs-src-err Error searching hotels ([Definition](#error_hs-src-err))
 * @apiError (Possible error codes) {String} hs-err-del Error deleting item(s) from search index ([Definition](#error_hs-err-del))
 * @apiError (Possible error codes) {String} hs-err-for Error formatting hotel ([Definition](#error_hs-err-for))
 * @apiError (Possible error codes) {String} sr-err-src Error searching ([Definition](#error_sr-err-src))

 */

/**
 * @api {get} /hotels/ 1.1 Get all hotels
 * @apiName getHotels
 * @apiVersion 1.7.4
 * @apiGroup 4_Hotels
 * @apiDescription This method returns a Collection of **all** Hotel objects.
 *
 * [About collections](#api-1_Basics-basicsCollections)
 *
 * @apiUse CollectionQueryParams
 * @apiUse HotelCollectionQuery
 * @apiUse HotelQuery
 *
 * @apiExample {html} Get all hotels request example:
 * GET /v1/test-hgid/en/hotels HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 * @apiExample {html} Get all hotels near a location:
 * GET /v1/test-hgid/en/hotels?lat=59.438575&long=24.747423 HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 * @apiExample {html} Search hotels with Luxury rooms:
 * GET /v1/test-hgid/en/hotels?search=luxury%20rooms HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 * @apiUse daoErrors
 * @apiUse getHotelGroupErrors
 * @apiError (Possible error codes) {String} hs-src-err Error searching hotels ([Definition](#error_hs-src-err))
 * @apiError (Possible error codes) {String} hs-err-for Error formatting hotel ([Definition](#error_hs-err-for))
 * @apiError (Possible error codes) {String} sr-err-src Error searching ([Definition](#error_sr-err-src))

 * @apiSuccessExample {json} Get all hotels response example:
 {
  "href": "http://localhost:8000/v1/test-hgid/hotels",
  "offset": 0,
  "limit": 10,
  "total": 2,
  "items": [
    {
	    "href": "http://localhost:8000/v1/test-hgid/hotels/ernst",
	    "id": "ernst",
	    "coordinates": {
	        "lat": 50.941857,
	        "long": 6.956522
	    },
	    "localized": {
	        "en_GB": {
	            "name": "Excelsior Hotel Ernst",
	            "location": {
	                "city": "Cologne",
	                "country": "Germany",
	                "fullAddress": "Domplatz/Trankgasse 1-5, 50667 Cologne, Germany",
	                "shortAddress": "Alstadt-Nord, Cologne, Germany"
	            },
	            "buttonImage": {
	                "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image",
	                "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image"
	            }
	        }
	    }
		}
  ]
}
 *
 */

/**
 * @api {get} /hotels/:hotelId 1.2 Get a hotel
 * @apiName getHotel
 * @apiVersion 1.7.4
 * @apiGroup 4_Hotels
 * @apiDescription This method returns a single Hotel, which has the given id.
 *
 * @apiExample {html} Get a hotel (with 'brochure' details) request example:
 * GET /v1/test-hgid/en/hotels/33?detail=brochure HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 * @apiUse HotelQuery
 *
 * @apiUse daoErrors
 * @apiUse getHotelGroupErrors
 * @apiError (Possible error codes) {String} hs-hotel-nf Hotel, with given parameters was not found ([Definition](#error_hs-hotel-nf))

 * @apiSuccessExample {json} Get a hotel (with 'brochure' details) response example:
{
"href": "http://localhost:8000/v1/test-hgid/hotels/ernst",
"id": "ernst",
"coordinates": {
	"lat": 50.941857,
	"long": 6.956522
  },
  "localized": {
	"en_GB": {
	  "name": "Excelsior Hotel Ernst",
	  "location": {
		"city": "Cologne",
		"country": "Germany",
		"fullAddress": "Domplatz/Trankgasse 1-5, 50667 Cologne, Germany",
		"shortAddress": "Alstadt-Nord, Cologne, Germany"
	  },
	  "buttonImage": {
		"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image",
		"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image"
	  },
	  "contacts": {
		"primary": {
		  "email": " info@excelsior-hotel-ernst.de",
 "phone": "+49 (0)221 270 1",
 "website": "http://www.excelsiorhotelernst.com/en"
 }
 },
 "bookingUrl": null,
 "description": [
 {
	"text": [
		{
			"style": null,
			"value": "The Excelsior Hotel Ernst is Cologne’s Grand Hotel next to the Cathedral. We provide individualized services to respond to the uniqueness of every guest and employee. We maintain the tradition of hospitality and remain modern by applying innovation. As Grand Hotel in the heart of Cologne, we actively participate in urban life and we are committed to art and culture. Your individuality is our Excellence."
		}
	],
	"heading": [
		{
			"style": null,
			"value": "About us"
		}
	]
},
 {
	"text": [
		{
			"style": null,
			"value": "Carl Ernst, Royal Restorer of the central station, was builder and owner of the Hotel Ernst in the city center. The opening ceremony took place on May 16th 1863. In 1871, after as little as eight years, he sold the Hotel Ernst to Friedrich Kracht. Friedrich Kracht moved from Belgium to Cologne to manage the hotel but he died four years later. His wife and his son Carl took over the management of the house.  In those days, the Grand Hotel was already the first choice of prominent guests."
		}
	],
	"heading": [
		{
			"style": null,
			"value": "History"
		}
	]
}
 ],
 "gallery": [
 {
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1",
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1"
},
 {
	"highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0",
	"lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0"
}
 ]
 }
 },
 "menu": [
 {
    "action": "web",
    "isEnabled": true,
    "data": {
        "url": "http://www.corinthia.com/hotels/london/"
    }
},
 {
    "action": "phone",
    "isEnabled": true,
    "data": {
        "phone": "+372 628 6500"
    }
},
 {
    "action": "map",
    "isEnabled": true,
    "data": {
        "lat": 51.506551,
        "long": -0.124039
    }
},
 {
    "action": "share",
    "isEnabled": true,
    "data": {
        "name": "Corinthia Hotel London",
        "gallery": [
            {
                "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1",
                "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/1"
            },
            {
                "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0",
                "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/gallery/0"
            }
        ]
    }
},
 {
    "action": "book",
    "isEnabled": true,
    "data": {
        "url": "https://booking.slh.com/en-GB/Room/Availability/?HotelCode=HUTLLTE&StartDate=<StartDate>&NoOfNights=<NoOfNights>&NoOfAdults=<NoOfAdults>",
        "type": "url"
    }
}
 ],
 "vhSiteKey": "devtallinn",
 "gaTrackingId": "UA-XXXXX-XX"
 }
 *
 */

/**
 * @api {get} /hotels/:hotelId/contacts 1.2 Get hotel contact details
 * @apiName getHotelContacts
 * @apiVersion 1.7.4
 * @apiGroup 4_Hotels
 * @apiDescription Returns hotel contact information, which is customized, 
 * based on the location of the client (detected from IP address)
 *
 * @apiExample {html} Get hotel contacts details request example:
 * GET /v1/test-hgid/en/hotel/berna/contacts HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 *
 * @apiUse daoErrors
 * @apiUse getHotelGroupErrors
 * @apiError (Possible error codes) {String} hs-err-ip Invalid request ip address ([Definition](#error_hs-err-ip))

 * @apiSuccessExample {json} Get a hotel contact details response example:
{
    "localized": {
        "en_GB": {
            "primary": {
                "email": " info@excelsior-hotel-ernst.de",
                "phone": "+49 (0)221 270 1",
                "website": "http://www.excelsiorhotelernst.com/en"
            }
        }
    },
    "callCenterNumbers": {
      "defaultNumber": "+33 33 444",
	    "continents": {
	      "Asia": "+13 44 22",
	      "Europe": "+23 44 33"
	    },
	    "countries": {
	      "Australia": "1 800 333 4444",
	      "United Kingdom": "0800 3333 222",
	      "United States": "1-877-666-3332"
	    },
	    "cities": {
	      "Moscow": "8 6621111"
	    }
    }
  }
}
 *
 */

/**
 * @api {get} /categories/:categoryId/hotels 1.3 Get hotels in a category
 * @apiName getCategoryHotels
 * @apiVersion 1.7.4
 * @apiGroup 4_Hotels
 * @apiDescription This method returns a Collection of Hotel objects, which belong to given category.
 *
 * [About collections](#api-1_Basics-basicsCollections)
 *
 * @apiUse CollectionQueryParams
 * @apiUse HotelCollectionQuery
 * @apiUse HotelQuery
 *
 * @apiExample {html} Get category hotels request example:
 * GET /v1/test-hgid/en/categories/2/hotels HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 * @apiUse daoErrors
 * @apiUse getHotelGroupErrors
 * @apiError (Possible error codes) {String} hs-src-err Error searching hotels ([Definition](#error_hs-src-err))
 * @apiError (Possible error codes) {String} hs-err-for Error formatting hotel ([Definition](#error_hs-err-for))
 * @apiError (Possible error codes) {String} sr-err-src Error searching ([Definition](#error_sr-err-src))

 * @apiSuccessExample {json} Get category hotels response example:
 {
  "href": "http://localhost:8000/v1/test-hgid/categories/reg-eur/hotels",
  "offset": 0,
  "limit": 10,
  "total": 1,
  "items": [
    {
	    "href": "http://localhost:8000/v1/test-hgid/hotels/ernst",
	    "id": "ernst",
	    "coordinates": {
	        "lat": 50.941857,
	        "long": 6.956522
	    },
	    "localized": {
	        "en_GB": {
	            "name": "Excelsior Hotel Ernst",
	            "location": {
	                "city": "Cologne",
	                "country": "Germany",
	                "fullAddress": "Domplatz/Trankgasse 1-5, 50667 Cologne, Germany",
	                "shortAddress": "Alstadt-Nord, Cologne, Germany"
	            },
	            "buttonImage": {
	                "highRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image",
	                "lowRes": "http://res.cloudinary.com/cardola-estonia/image/upload/c_pad,h_<height>,w_<width>/q_10/v1/indrek-dev/group-app/test-hgid/hotels/ernst/image"
	            }
	        }
	    }
		}
  ]
}
 *
 */
