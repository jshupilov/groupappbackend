'use strict';
var databaseUtils = require('../utils/databaseUtils');
var errors = require('../utils/errors');

errors.defineError('hd-hotel-ne', 'Hotel was not found', 'Hotel, with given parameters was not found', 'hotels', 400);

/**
 * Find a hotel by ID
 * @param id
 * @param hotelGroupId
 * @param callback
 */
exports.findById = function (id, hotelGroupId, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query(
			'SELECT * ' +
			'FROM hotel ' +
			"WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 AND (data -> 'hotelId') = $2",
			['"' + hotelGroupId + '"', '"' + id + '"'],
			function (err, result) {
				done();
				callback(err, result);
			});
	});

};

/**
 * Check if hotel exist
 * @param id
 * @param callback
 */
exports.checkIfExists = function (id, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query(
			'SELECT count(*) AS c  ' +
			'FROM hotel_v ' +
			"WHERE (data -> \'hotelId\') = $1",
			['"' + id + '"'],
			function (err, result) {
				done();
				if (err) {
					return callback(err);
				}
				return callback(null, result.rows[0].c > 0);
			});
	});

};

/**
 * Find hotels by ids
 * @param ids
 * @param hotelGroupId
 * @param callback
 */
exports.findByIds = function (ids, hotelGroupId, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query(
			'SELECT * ' +
			'FROM hotel ' +
			"WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 AND (data -> 'hotelId') ?| $2::text[]",
			['"' + hotelGroupId + '"', ids],
			function (err, result) {
				done();
				callback(err, result);
			});
	});

};

/**
 * Get first hotel in database
 * @param callback
 */
exports.getFirst = function (callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query('SELECT * from hotel LIMIT 1', function (err, result) {
			done();
			callback(err, result);
		});
	});

};

/**
 * Get all hotels
 * @param hotelGroupId
 * @param callback
 */
exports.getAll = function (hotelGroupId, callback) {
	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query(
			'SELECT * from hotel  ' +
			"WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 ",
			['"' + hotelGroupId + '"'],
			function (err, result) {
				done();
				callback(err, result);
			});
	});

};

/**
 * Create hotel
 * @param data
 * @param callback
 * @deprecated - insert data is moved to versioning
 */
exports.create = function (data, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query('INSERT INTO hotel (data) VALUES($1)', [data], function (err, result) {
			done();
			callback(err, result);
		});
	});

};

/**
 * Delete hotel group hotels
 * @param hotelGroupId
 * @param callback
 */
exports.deleteHotelGroupHotels = function (hotelGroupId, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query(
			"DELETE FROM hotel WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 ",
			['"' + hotelGroupId + '"'],
			function (err, result) {
				done();
				callback(err, result);
			}
		);
	});

};

/**
 * Delete hotel
 * @param hotelGroupId
 * @param hotelId
 * @param callback
 */
exports.deleteHotel = function (hotelGroupId, hotelId, callback) {

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query(
			"DELETE FROM hotel WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 AND (data -> 'hotelId') = $2 ",
			['"' + hotelGroupId + '"', '"' + hotelId + '"'],
			function (err, result) {
				done();
				callback(err, result);
			}
		);
	});

};



/**
 * Publish item to live table
 * @param item
 * @param callback
 */
exports.publishHotel = function (item, callback) {

	databaseUtils.doInTransaction(function(client, cb){

		client.query("DELETE FROM hotel WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 AND (data -> 'hotelId') = $2 ",
			['"' + item.data.hotelGroup.hotelGroupId + '"', '"' + item.data.hotelId + '"'], function (err) {
				if (err) {
					return cb(err);
				}
				client.query('INSERT INTO hotel (data) VALUES($1)', [item.data], function (err) {
					if(err) {
						return cb(err);
					}
					cb(null);
				});
			});
	}, callback);

};

/**
 * Unpublish (delete) item from live table
 * @param item
 * @param callback
 */
exports.unPublishHotel = function (item, callback) {
	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query("DELETE FROM hotel WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 AND (data -> 'hotelId') = $2 ",
			['"' + item.data.hotelGroup.hotelGroupId + '"', '"' + item.data.hotelId + '"'], function (err, res) {
				done();
				callback(err, res);
			});
	});
};

/**
 * get publish hotels by ids
 * @param ids
 * @param callback
 */
exports.getPublishedHotelsByIds = function (hgId, ids, callback) {
	var keys = [];
	var values = ['"' + hgId + '"'];

	for(var i = 0; i < ids.length; i++){
		keys.push('$' + (i + 2));
		values.push('"' + ids[i] + '"');
	}
	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query(
			'SELECT data ' +
			'FROM hotel ' +
			'WHERE (data -> \'hotelGroup\' -> \'hotelGroupId\') = $1 AND (data -> \'hotelId\') IN (' + keys.join(',') +') ' +
			'ORDER BY id ASC ',
			values,
			function (err1, result) {
				done();
				if(err1){
					return callback(err1);
				}
				return callback(null, result.rows);
			}
		);
	});
};

/**
 * get publish hotels by ids
 * @param hgId
 * @param callback
 */
exports.getPublishedHotelIds = function (hgId, callback) {
	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query(
			'SELECT (data ->> \'hotelId\') as hotel_id ' +
			'FROM hotel ' +
			'WHERE (data -> \'hotelGroup\' ->> \'hotelGroupId\') = $1' +
			'ORDER BY id ASC ',
				[hgId],
			function (err1, result) {
				done();
				if(err1){
					return callback(err1);
				}
				return callback(err1, result.rows.map(function(row){
					return row.hotel_id;
				}));
			}
		);
	});
};
