/**
 * Created by jevgenishupilov on 19/01/15.
 */
'use strict';
var eventUtils = require('../utils/eventUtils');
var logging = require('../utils/logging');
var services = require('./services');

eventUtils.emitter.on('hotelGroup.unpublished', function(hotelGroupId, callback) {
	logging.info('You deleted a hotelGroup... let me delete hotels ', hotelGroupId);

	services.deleteHotelGroupHotels(hotelGroupId, function(err) {
		if(err) {
			return callback(err);
		}
		callback(null);

	});

});
