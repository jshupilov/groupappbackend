'use strict';
var services = require('./services');
var errors = require('../utils/errors');
var apiUtils = require('../utils/apiUtils');

var regionModule = {
	register: function(server, options, next) {
		// Add the route
		server.route({
			method: 'GET',
			path: '/hotels',
			handler: function(request, reply) {
				services.getHotels(request.apiRequest, function(err, res) {
					apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res));
				});
			}
		});
		server.route({
			method: 'GET',
			path: '/hotels/{hotelId}',
			handler: function(request, reply) {
				services.getHotel(request.apiRequest, function(err, res) {
					apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res));
				});
			}
		});
		server.route({
			method: 'GET',
			path: '/hotels/{hotelId}/contacts',
			handler: function(request, reply) {
				services.getHotelContacts(request.apiRequest, function(err, res) {
					apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res), 'noCache');
				});
			}
		});
		server.route({
			method: 'GET',
			path: '/categories/{categoryId}/hotels',
			handler: function(request, reply) {
				services.getHotels(request.apiRequest, function(err, res) {
					apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res));
				});
			}
		});
		next();
	}
};

regionModule.register.attributes = {
	name: 'Hotels',
	version: '1.0.0'
};

exports.register = regionModule.register;
exports.services = services;
