'use strict';
var apiUtils = require('../utils/apiUtils');
var validation = require('./validation');
var collectionUtils = require('../utils/collectionUtils');
var errors = require('../utils/errors');
var geoUtils = require('../utils/geoUtils');
var dao = require('./dao');
var q = require('q');
var hgServices = require('../hotel_groups/services');
var searchServices = require('../search/services');
var mediaServices = require('../media/services');
var eventUtils = require('../utils/eventUtils');
var langUtils = require('../utils/langUtils');
var version = require('../versioning/services');
var assert = require('assert');
var logger = require('../utils/logging');
var geo = require('geoip2ws')(process.env.GEOIP_USER_ID, process.env.GEOIP_API_KEY);

errors.defineError('hs-hotel-nf', 'Hotel was not found', 'Hotel, with given parameters was not found', 'hotels', 400);
errors.defineError('hs-err-save', 'Saving failed', 'Error occurred while saving hotel to database', 'hotels');
errors.defineError('hs-src-err', 'Search failed', 'Error searching hotels', 'hotels');
errors.defineError('hs-err-del', 'Cannot fully delete', 'Error deleting item(s) from search index', 'hotels');
errors.defineError('hs-err-for', 'Cannot get hotel', 'Error formatting hotel', 'hotels');
errors.defineError('hs-err-ip', 'Cannot get IP', 'Invalid request ip address', 'hotels');
errors.defineError('hs-err-detail', 'Cannot get hotel', 'Not allowed details', 'hotels');


/**
 * get publish hotels by ids
 * @param ids
 * @param callback
 */
exports.getPublishedHotelsByIds = function (hgId, ids, callback) {
	if(!ids.length){
		return callback(null, []);
	}
	dao.getPublishedHotelsByIds(hgId, ids, function(err, res){
		if(err) {
			return callback(err);
		}
		return callback(null, res);
	});
};
/**
 * Format an hotelMenu API structure
 * @param hotelGroup
 * @param hotel
 * @param localized
 * @param image
 * @returns {Array}
 */

function formatHotelMenu(hotelGroup, hotel, localized, image){
	var menu = [];
	if(hotelGroup.viewConfiguration.hotelMenu){
		hotelGroup.viewConfiguration.hotelMenu.forEach(function(menuItem){

			switch(menuItem.action){
				case 'web':
					apiUtils.populateMenu(
						menuItem,
						localized.contacts.primary.website,
						['url', 'hotelName'],
						[localized.contacts.primary.website, localized.name],
						menu,
						langUtils.getTranslation(hotelGroup.translations, 'WebButton', localized.language)
					);
					break;
				case 'phone':
					apiUtils.populateMenu(
						menuItem,
						localized.contacts.primary.phone,
						['phone', 'hotelName'],
						[localized.contacts.primary.phone, localized.name],
						menu,
						langUtils.getTranslation(hotelGroup.translations, 'CallButton', localized.language)
					);
					break;
				case 'map':
					apiUtils.populateMenu(
						menuItem,
						hotel.data.coordinates.lat && hotel.data.coordinates.long,
						['lat', 'long', 'hotelName'],
						[hotel.data.coordinates.lat, hotel.data.coordinates.long, localized.name],
						menu,
						langUtils.getTranslation(hotelGroup.translations, 'MapButton', localized.language)
					);
					break;
				case 'share':
					apiUtils.populateMenu(
						menuItem,
						localized.name,
						['image', 'hotelName'],
						[ image.highRes, localized.name],
						menu,
						langUtils.getTranslation(hotelGroup.translations, 'ShareButton', localized.language)
					);
					break;
				case 'book':
					apiUtils.populateMenu(
						menuItem,
						localized.bookingUrl,
						['url', 'type', 'hotelName'],
						[localized.bookingUrl, 'url', localized.name],
						menu,
						langUtils.getTranslation(hotelGroup.translations, 'BookNowButton', localized.language)
					);
					break;
			}
		});
	}

	return menu;
}

/**
 * Format an hotel from database to API structure
 * @param apiRequest
 * @param hotel
 * @param detail
 * @param hotelGroupData
 * @returns {{href: String, brochure: Object, id: *, coordinates: *, localized: {}}}
 */
function formatHotel(apiRequest, hotel, detail, hotelGroupData) {



	function formatLocalized(localized) {
		var localData = {
			name: localized.name,
			location: localized.location,
			buttonImage: mediaServices.formatImageObject(localized.gallery[0], hotelGroupData.mediaConfiguration, 'hotelBtn')
		};

		if(['brochure', 'full'].indexOf(detail) > -1) {
			localData.contacts = {
				primary: localized.contacts.primary
			};
			localData.description = localized.description;
			localData.bookingUrl = localized.bookingUrl ? localized.bookingUrl : null;
			localData.gallery = [];
			localized.gallery.forEach(function(galleryImageObj) {
				localData.gallery.push(mediaServices.formatImageObject(galleryImageObj, hotelGroupData.mediaConfiguration, 'hotelGal'));
			});
			if(localData.gallery.length === 0){
				localData.gallery.push(mediaServices.formatImageObject(hotelGroupData.mediaConfiguration.placeholderImage, hotelGroupData.mediaConfiguration, 'hotelGal'));
			}
			localData.menu = formatHotelMenu(hotelGroupData, hotel, localized, mediaServices.formatImageObject(localized.gallery[0], hotelGroupData.mediaConfiguration, 'hotelGal', false, false));


		}
		return localData;

	}

	var formatted = {
		href: exports.getHotelHref(hotel),
		id: hotel.data.hotelId,
		coordinates: hotel.data.coordinates,
		localized: {}
	};

	if(apiRequest.geoLocation && hotel.data.coordinates){
		formatted.distanceKm = Math.round( geoUtils.getDistanceFromLatLonInKm(apiRequest.geoLocation.latitude, apiRequest.geoLocation.longitude, hotel.data.coordinates.lat, hotel.data.coordinates.long)*1000 )/1000;
	}

	if(['brochure', 'full'].indexOf(detail) > -1) {
		formatted.vhSiteKey = (hotel.data.vhSiteKey) ? hotel.data.vhSiteKey : false;
		formatted.gaTrackingId = (hotel.data.gaTrackingId) ? hotel.data.gaTrackingId : false;

	}

	for(var li in hotel.data.localized) {
		var lang = hotel.data.localized[li].language;

		formatted.localized[lang] = langUtils.getLocalizedData(
			hotel.data,
			lang,
			formatLocalized,
			'language'
		);

	}

    formatted.contacts = apiUtils.getServiceLink(apiRequest.hotelGroupId, 'hotels/' + hotel.data.hotelId + '/contacts');

	return formatted;

}

exports.formatHotel = formatHotel;

/**
 * Format hotel in collection
 *
 * @param apiRequest
 * @param hotel
 * @param hotelOffset
 * @param detail
 * @param mediaConfiguration
 * @returns {{href: String, brochure: Object, id: *, coordinates: *, localized: {}}}
 */
function formatHotelInCollection(apiRequest, hotel, hotelOffset, detail, hotelGroupData) {
	var formatted = formatHotel(apiRequest, hotel, detail, hotelGroupData);
	formatted.brochure = exports.getHotelBrochureLink(apiRequest, hotelOffset);
	return formatted;
}

/**
 * Format hotels in a collection
 * @param hotels
 * @param apiRequest
 * @param detail
 * @param mediaConfiguration
 * @returns {Array}
 */
function formatHotelsInCollection(hotels, apiRequest, detail, hotelGroupData) {
	var out = [];
	var hotelOffset = apiRequest.collectionQuery.offset;
	hotels.forEach(function(hotel) {
		out.push(formatHotelInCollection(apiRequest, hotel, hotelOffset++, detail, hotelGroupData));
		hotel = null;
	});
	hotels = null;
	return out;
}

/**
 * Get href to hotel resource
 * @param hotel
 * @returns {String}
 */
exports.getHotelHref = function(hotel) {
	return apiUtils.getServiceHref(hotel.data.hotelGroup.hotelGroupId, 'hotels/' + hotel.data.hotelId);
};

/**
 * Get Link object pointing to brochure view
 * @param apiRequest
 * @param hotelOffset
 * @returns {Object} Link object
 */
exports.getHotelBrochureLink = function(apiRequest, hotelOffset) {
	var query = {detail: 'brochure'};
	query.limit = 1;
	if(hotelOffset !== undefined) {
		query.offset = hotelOffset;
	}
	return apiUtils.getModifiedCurrentLink(apiRequest, query);
};

/**
 * Get list of hotels
 * @param apiRequest
 * @param callback
 */
exports.getHotels = function(apiRequest, callback) {
	var detail = null;
	var allowed = [];

	if(apiRequest.query.detail && ['brochure', 'full'].indexOf(apiRequest.query.detail) > -1) {
		detail = apiRequest.query.detail;
	}
	if(detail && allowed.indexOf(detail) < 0){
		return callback(errors.newError('hs-err-detail', {detail: detail}));
	}
	var searchFilters = {
		collectionQuery: apiRequest.collectionQuery
	};

	if(apiRequest.query.hasOwnProperty('search')) {
		searchFilters.keyword = apiRequest.query.search;
	}

	//if coordinates given
	if(apiRequest.geoLocation) {

		searchFilters.geoLocation = {
			latitude: apiRequest.geoLocation.latitude,
			longitude: apiRequest.geoLocation.longitude,
			radius: apiRequest.geoLocation.radius
		};
	}


	if(apiRequest.params.categoryId) {
		searchFilters.categoryIds = [apiRequest.params.categoryId];
	}

	var placeHolderImagePromise = q.defer();
	var searchHotelsPromise = q.defer();
	var queryHotelsPromise = q.defer();

	//when all done do these:
	q.all([placeHolderImagePromise.promise, searchHotelsPromise.promise, queryHotelsPromise.promise]).spread(function(hotelGroupData, searchRes, hotelsRes) {
		try{
			callback(null, collectionUtils.formatCollectionWithRequest(formatHotelsInCollection(hotelsRes.rows, apiRequest, detail, hotelGroupData.data), apiRequest, searchRes.nbHits));
			hotelGroupData = null;
			searchRes = null;
			hotelsRes = null;
			placeHolderImagePromise = null;
			searchHotelsPromise = null;
			queryHotelsPromise = null;
		}catch(e){
			return callback(errors.newError('hs-err-for', {originalError: e}));
		}
	}, function(reason) {
		return callback(errors.newError('hs-src-err', {reason: reason}));
	}).done();

	//get placeholder image
	hgServices.getHotelGroup(apiRequest.hotelGroupId, function(piErr, phI) {
		if(piErr) {
			placeHolderImagePromise.reject({causedBy: 'getHotelGroup', error: piErr});
		} else {
			placeHolderImagePromise.resolve(phI);
		}
	});

	//search for hotels
	searchServices.searchHotels(searchFilters, apiRequest.hotelGroupId, function(err, res) {
		if(err) {
			return searchHotelsPromise.reject({causedBy: 'searchHotels', error: err});
		}
		res.ids = [];
		res.hits.forEach(function(hit) {
			res.ids.push(hit.objectID);
		});
		searchHotelsPromise.resolve(res);

	});

	//load hotels from db
	searchHotelsPromise.promise.then(function(res) {
		//load data for hotels
		dao.findByIds(res.ids, apiRequest.hotelGroupId, function(errF, resF) {
			if(errF) {
				queryHotelsPromise.reject({causedBy: 'findByIds', error: errF});
			} else {
				resF.rows = collectionUtils.orderByArrayAndSubField(resF.rows, res.ids, 'data', 'hotelId');
				queryHotelsPromise.resolve(resF);
			}
		});
	});

};

/**
 * Format hotel contact details
 * @param hotel
 * @param [phone]
 * @param [callCenterNumbers]
 */
function formatHotelContacts(hotel, phone, callCenterNumbers) {
	var contacts = {localized: {}};
	for(var lang in hotel.localized) {
		hotel.localized[lang].contacts.primary.phone = phone || hotel.localized[lang].contacts.primary.phone;
		contacts.localized[lang] = hotel.localized[lang].contacts;
	}
	if(callCenterNumbers) {
		contacts.callCenterNumbers = callCenterNumbers;
	}
	return contacts;
}

/**
 * Get call center phone number by ip
 * @param apiRequest
 * @param callback
 */
exports.getHotelContacts = function(apiRequest, callback) {
	apiRequest.urlObj.query.detail = 'full';
	exports.getHotel(apiRequest, function(err, hotel) {
		if(err) {
			return callback(err);
		}
		hgServices.getHotelGroup(apiRequest.hotelGroupId, function(err, hotelGroup) {
			if(err) {
				return callback(err);
			}
			var hotelGroupData = hotelGroup.data;

			if(hotelGroupData.callCenterNumbers) {
				geo(apiRequest.remoteAddress, function(err, geoData) {
					if(err) {
						logger.err('Error response from geoIp2Views: ' + err);
						return callback(null, formatHotelContacts(hotel, hotelGroupData.callCenterNumbers.defaultNumber, hotelGroupData.callCenterNumbers));
					}
					var phone;
					try {
						assert.notStrictEqual(hotelGroupData.callCenterNumbers.cities[geoData.city.names.en], undefined);
						phone = hotelGroupData.callCenterNumbers.cities[geoData.city.names.en];
					} catch(e) {
						try {
							assert.notStrictEqual(hotelGroupData.callCenterNumbers.countries[geoData.country.names.en], undefined);
							phone = hotelGroupData.callCenterNumbers.countries[geoData.country.names.en];

						} catch(e) {
							try {
								assert.notStrictEqual(hotelGroupData.callCenterNumbers.continents[geoData.continent.names.en], undefined);
								phone = hotelGroupData.callCenterNumbers.continents[geoData.continent.names.en];
							} catch(e) {
								phone = hotelGroupData.callCenterNumbers.defaultNumber;
							}
						}
					}
					return callback(null, formatHotelContacts(hotel, phone, hotelGroupData.callCenterNumbers));
				});
			} else {
				return callback(null, formatHotelContacts(hotel));
			}
		});
	});
};

/**
 * Get a single hotel
 * @param apiRequest
 * @param callback
 */
exports.getHotel = function(apiRequest, callback) {

	//get placeholder image
	hgServices.getHotelGroup(apiRequest.hotelGroupId, function(piErr, hotelGroupData) {
		if(piErr) {
			return callback(piErr);
		} else {
			dao.findById(apiRequest.params.hotelId, apiRequest.hotelGroupId, function(err, res) {
				if(err) {
					return callback(err);
				}
				if(res.rows.length < 1) {
					return callback(errors.newError('hs-hotel-nf', {
						givenId: 'none',
						providedBy: 'getHotel'
					}));
				}
				callback(null, formatHotel(apiRequest, res.rows[0], apiRequest.urlObj.query.detail, hotelGroupData.data));
			});
		}
	});

};

/**
 * Get one hotel from database without formatting
 * @param hotelGroupId
 * @param hotelId
 * @param callback
 */
exports.getHotelRaw = function(hotelGroupId, hotelId, callback) {
	dao.findById(hotelId, hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}
		if(res.rows.length < 1) {
			return callback(errors.newError('hs-hotel-nf', {
				givenId: 'none',
				providedBy: 'getHotelRaw'
			}));
		}
		callback(null, res.rows[0]);
	});
};

/**
 * Get all hotels of a hotel group from database without formatting
 * @param hotelGroupId
 * @param callback
 */
exports.getHotelsRaw = function(hotelGroupId, callback) {
	dao.getAll(hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}
		callback(null, res.rows);
	});
};

/**
 * Create or update a hotel
 * @param data
 * @param hotelGroupData
 * @param callback
 */
exports.createOrUpdateHotel = function(data, hotelGroupData, callback) {
	validation.validateHotelData(data, 'full', function(err){
		if(err){
			if(err.data.code === 'hv-hId-exists'){
				return exports.updateHotel(data, hotelGroupData, callback);
			}else{
				return callback(err);
			}
		}
		exports.createHotel(data, hotelGroupData, callback);
	}, hotelGroupData);
};


/**
 * Create hotel
 * @param data
 * @param hotelGroupData
 * @param callback
 */
exports.createHotel = function(data, hotelGroupData, callback) {
  validation.validateHotelData(data, 'full', function(err){
    if(err){
      return callback(err);
    }
    version.addItem(data, 'hotel_v', function(err, res){
      if(err) {
        return callback(err);
      }
      //emit event
      eventUtils.emitter.parallel('hotel.created', data, function() {
        callback(null, res);
      });
    });
  }, hotelGroupData);
};

/**
 * Update hotel
 * @param data
 * @param hotelGroupData
 * @param callback
 */
exports.updateHotel = function(data, hotelGroupData, callback) {
	validation.validateHotelData(data, 'update', function(err){
		if(err){
			return callback(err);
		}
		version.updateItem(data, 'hotel_v', function(err, res){
			if(err) {
				return callback(err);
			}

			//emit event
			eventUtils.emitter.parallel('hotel.updated', data, function(err) {
				//after listeners are done, call the callback
				if(err){
					return callback(err);
				}
				callback(null, res);
			});

		});
	}, hotelGroupData);
};



/**
 * Publish version item to live.
 * Delete old entry (if exists) and insert new
 * @param item
 * @param callback
 */
exports.publishHotel = function(item, callback){
	dao.publishHotel(item, function(err, res){
		if(err){
			return callback(err);
		}
		version.updateVersion(item, 'hotel_v', function(err) {
			if(err) {
				return callback(err);
			}

			eventUtils.emitter.parallel('hotel.published', item.data, function(err){
				if(err){
					return callback(err);
				}
				callback(null, res);
			});
		});
	});
};


/**
 * Delete hotel item from live table
 * @param item
 * @param callback
 */
exports.unPublishHotel = function(item, callback){
	dao.unPublishHotel(item, function(err, res){
		if(err){
			return callback(err);
		}
		version.updateVersion(item, 'hotel_v', function(err) {
			if(err) {
				return callback(err);
			}

			eventUtils.emitter.parallel('hotel.unpublished', item.data.hotelGroup.hotelGroupId, item.data.hotelId, function(err){
				if(err){
					return callback(err);
				}
				callback(null, res);
			});
		});
	});
};

/**
 * Delete all hotel group hotels
 * @param hotelGroupId
 * @param callback
 * @deprecated
 */
exports.deleteHotelGroupHotels = function(hotelGroupId, callback) {
	dao.deleteHotelGroupHotels(hotelGroupId, function(err, res) {
		if(err) {
			return callback(errors.newError('hs-err-del', {
				providedBy: 'deleteHotelGroupHotels',
				originalError: err
			}));
		}

		//emit event
		eventUtils.emitter.parallel('hotel.deletedAllInHotelGroup', hotelGroupId, function(err) {
			//after listeners are done, call the callback
			if(err){
				return callback(err);
			}
			callback(null, res);
		});

	});
};

/**
 * Delete one hotel
 * @param hotelGroupId
 * @param hotelId
 * @param callback
 * @deprecated
 */
exports.deleteHotel = function(hotelGroupId, hotelId, callback) {
	dao.deleteHotel(hotelGroupId, hotelId, function(err, res) {
		if(err) {
			return callback(errors.newError('hs-err-del',  {
				providedBy: 'deleteHotel',
				originalError: err
			}));
		}

		//emit event
		eventUtils.emitter.parallel('hotel.deleted', hotelGroupId, hotelId, function(err) {
			//after listeners are done, call the callback
			if(err){
				return callback(err);
			}
			callback(null, res);
		});

	});
};

/**
 * Get all published hotels by Ids
 * @param hgId
 * @param callback
 */
exports.getPublishedHotelIds = function (hgId, callback) {
	dao.getPublishedHotelIds(hgId, function(err, res){
		return callback(err, res);
	});
};
