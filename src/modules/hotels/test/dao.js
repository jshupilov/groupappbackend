'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var dao = require('../dao');
var databaseUtils = require('../../utils/databaseUtils');
var data = {
  hotelId: 'bla-hId',
  hotelGroup: {
    hotelGroupId: 'bla-hgId'
  }
};
lab.experiment('Hotels DAO Interactions', function() {
	var _hgId;
	lab.before(function(done){
      _hgId = testingTools.getHotelGroupId();
      done();
	});
	lab.after(function(done){
		dao.deleteHotel(data.hotelGroup.hotelGroupId, data.hotelId, function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('fyndById should work', function(done) {
		dao.findById('stpeter', _hgId, function(err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.expectQueryResultsWithFields(res, ['id', 'data']);
			done();
		});
	});
	lab.test('findByIds should work', function(done) {
		dao.findByIds(['stpeter'], _hgId, function(err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.expectQueryResultsWithFields(res, ['id', 'data']);
			done();
		});
	});


	lab.test('checkIfExists should error', function(done) {
		dao.checkIfExists('stpeter-test', function(err) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			//testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});

	lab.test('checkIfExists should return FALSE', function(done) {
		dao.checkIfExists('som-test-hotel-id', function(err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.false();
			done();
		});
	});


	lab.test('getFirst should work', function(done) {
		dao.getFirst(function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.expectQueryResultsWithFields(res, ['id', 'data']);
			done();
		});
	});

	lab.test('getAll should work', function (done) {
		dao.getAll(_hgId, function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.expectQueryResultsWithFields(res, ['id', 'data']);
			done();
		});
	});
	lab.test('create should work', function (done) {
		dao.create(data, function (err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('getPublishedHotelsByIds should work', function (done) {
		dao.getPublishedHotelsByIds(_hgId, [data.hotelId], function (err, res) {
			testingTools.code.expect(err).to.be.null();
            testingTools.code.expect(res).to.be.array();
			done();
		});
	});

});

lab.experiment('Hotels DAO Interactions Deletes', function () {

  lab.beforeEach(function (done) {
    dao.create(data, function (err) {
      testingTools.code.expect(err).to.be.null();
      done();
    });
  });

  lab.test('deleteHotel should work', function (done) {
    dao.deleteHotel(data.hotelGroup.hotelGroupId, data.hotelId, function (err, res) {
      testingTools.code.expect(err).to.be.null();
      testingTools.code.expect(res.rowCount).to.equal(1);
      done();
    });
  });

  lab.test('deleteHotelGroupHotels should work', function (done) {
    dao.deleteHotelGroupHotels(data.hotelGroup.hotelGroupId, function (err, res) {
      testingTools.code.expect(err).to.be.null();
      testingTools.code.expect(res.rowCount).to.equal(1);
      done();
    });
  });

});

lab.experiment('Hotel version DAO if DB structure invalid', function(){
  testingTools.failDbQueryForExperiment(lab, 'hotel_v');
  lab.test('checkIfExists should give error', function(done) {
    dao.checkIfExists('stpeter', function(err) {
      testingTools.expectError(err, 'db-query-fails');
      done();
    });
  });

});

lab.experiment('Hotels DAO if DB structure invalid', function () {
  var _hgId;
  testingTools.failDbQueryForExperiment(lab, 'hotel');
  lab.before(function (done) {
    _hgId = testingTools.getHotelGroupId();
    done();
  });
  lab.after(function (done) {
    dao.deleteHotel(data.hotelGroup.hotelGroupId, data.hotelId, function (err) {
      testingTools.code.expect(err).to.be.null();
      done();
    });
  });
  lab.test('fyndById should give error', function (done) {
    dao.findById('stpeter', _hgId, function (err) {
      testingTools.expectError(err, 'db-query-fails');
      done();
    });
  });

  lab.test('findByIds should give error', function (done) {
    dao.findByIds(['stpeter'], _hgId, function (err) {
      testingTools.expectError(err, 'db-query-fails');
      done();
    });
  });



	lab.test('getFirst should give error', function(done) {
		dao.getFirst(function(err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});

  lab.test('getAll should give error', function (done) {
    dao.getAll(_hgId, function (err) {
      testingTools.expectError(err, 'db-query-fails');
      done();
    });
  });

  lab.test('create should give error', function (done) {
    dao.create(data, function (err) {
      testingTools.expectError(err, 'db-query-fails');
      done();
    });
  });

	lab.test('getPublishedHotelsByIds should give error', function (done) {
		dao.getPublishedHotelsByIds(_hgId, [data.hotelId], function (err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});

lab.experiment('Hotels DAO if DB structure invalid Deletes', function () {

  testingTools.failDbQueryForExperiment(lab, 'hotel');

  lab.beforeEach(function (done) {
    dao.create(data, function (err) {
      testingTools.expectError(err, 'db-query-fails');
      done();
    });
  });

  lab.test('deleteHotel should give error', function (done) {
    dao.deleteHotel(data.hotelGroup.hotelGroupId, data.hotelId, function (err) {
      testingTools.expectError(err, 'db-query-fails');
      done();
    });
  });

  lab.test('deleteHotelGroupHotels should give error', function (done) {
    dao.deleteHotelGroupHotels(data.hotelGroup.hotelGroupId, function (err) {
      testingTools.expectError(err, 'db-query-fails');
      done();
    });
  });

});

lab.experiment('Hotels DAO if DB connection', function () {
  testingTools.killDbConnectionForExperiment(lab);
  var _hgId;
  lab.before(function (done) {
    _hgId = testingTools.getHotelGroupId();
    done();
  });
  lab.after(function (done) {
    dao.deleteHotel(data.hotelGroup.hotelGroupId, data.hotelId, function (err) {
      testingTools.code.expect(err).to.be.null();
      done();
    });
  });
  lab.test('fyndById should give error', function (done) {
    dao.findById('stpeter', _hgId, function (err) {
      testingTools.expectError(err, 'db-cc-db');
      done();
    });
  });

	lab.test('findByIds should give error', function(done) {
		dao.findByIds(['stpeter'], _hgId, function(err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});

	lab.test('checkIfExists should give error', function(done) {
		dao.checkIfExists('stpeter', function(err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});

  lab.test('getFirst should give error', function (done) {
    dao.getFirst(function (err) {
      testingTools.expectError(err, 'db-cc-db');
      done();
    });
  });

  lab.test('getAll should give error', function (done) {
    dao.getAll(_hgId, function (err) {
      testingTools.expectError(err, 'db-cc-db');
      done();
    });
  });

	lab.test('getPublishedHotelsByIds should give error', function (done) {
		dao.getPublishedHotelsByIds(_hgId, [data.hotelId], function (err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});

lab.experiment('Hotels DAO if DB connection Deletes', function () {

  testingTools.killDbConnectionForExperiment(lab);

  lab.beforeEach(function (done) {
    dao.create(data, function (err) {
      testingTools.expectError(err, 'db-cc-db');
      done();
    });
  });

  lab.test('deleteHotel should give error', function (done) {
    dao.deleteHotel(data.hotelGroup.hotelGroupId, data.hotelId, function (err) {
      testingTools.expectError(err, 'db-cc-db');
      done();
    });
  });

  lab.test('deleteHotelGroupHotels should give error', function (done) {
    dao.deleteHotelGroupHotels(data.hotelGroup.hotelGroupId, function (err) {
      testingTools.expectError(err, 'db-cc-db');
      done();
    });
  });
});

lab.experiment('hotels.dao.versioning', function () {
  testingTools.buildTestHotelsForVersioning(lab, 'hotel_v', 100, false);
  lab.test('checkIfExists should return TRUE', function(done) {
    dao.checkIfExists('known', function(err, res) {
      testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
      testingTools.code.expect(res).to.be.true();
      done();
    });
  });
});

lab.experiment('hotels.publish.woDb', function(){
	testingTools.killDbConnectionForExperiment(lab);
	lab.test('Database connection should give failure', function(done){
	  var item = {};
		dao.publishHotel(item, function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('Unpublish should give failure', function(done){
		var item = {};
		dao.unPublishHotel(item, function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});

lab.experiment('hotels.publish.query.failures', function(){
	var getC, client, cd;
	var delData = {
		'data': {
			'hotelId': 'testHotel',
			'hotelGroup': {
				'hotelGroupId': 'test-hgid7'
			}
		}
	};
	lab.before(function(done){
		getC = databaseUtils.getConnection;
		databaseUtils.getConnection(function(err, cl, doneDb){
			testingTools.code.expect(err).to.be.null();
			client = cl;
			cd = doneDb;
			done();
		});
	});
	lab.afterEach(function(done){
		databaseUtils.getConnection = getC;
		done();
	});
	lab.after(function(done){
		cd();
		done();
	});
	lab.test('Transaction should fail to start', function (done) {
		databaseUtils.getConnection = function (cb) {
			cb(
				null,
				{
					query: function () {
						return arguments[arguments.length - 1](new Error('Transaction start failure'));
					}
				},
				function () {
				}
			);
		};
		dao.publishHotel({}, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Transaction start failure');
			done();
		});
	});
	lab.test('Delete item should fail', function (done) {
		var counter = 0;
		databaseUtils.getConnection = function (cb) {
			cb(
				null,
				{
					query: function () {
						counter++;
						if (counter === 2) {
							return arguments[arguments.length - 1](new Error('Delete item failure'));
						}
						client.query.apply(client, arguments);
					}
				},
				function () {
				}
			);
		};
		dao.publishHotel(delData, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Delete item failure');
			done();
		});
	});
	lab.test('Insert item should fail', function (done) {
		var counter = 0;
		databaseUtils.getConnection = function (cb) {
			cb(
				null,
				{
					query: function () {
						counter++;
						if (counter === 3) {
							return arguments[arguments.length - 1](new Error('Insert failure'));
						}
						client.query.apply(client, arguments);
					}
				},
				function () {
				}
			);
		};
		dao.publishHotel(delData, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Insert failure');
			done();
		});
	});
	lab.test('Commit should fail', function (done) {
		var counter = 0;
		databaseUtils.getConnection = function (cb) {
			cb(
				null,
				{
					query: function () {
						counter++;
						if (counter === 4) {
							return arguments[arguments.length - 1](new Error('Commit failure'));
						}
						client.query.apply(client, arguments);
					}
				},
				function () {
				}
			);
		};
		dao.publishHotel(delData, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Commit failure');
			done();
		});
	});
});

lab.experiment('Hotels.dao.getPublishedHotelIds', function() {
	lab.test('should work', function(done) {
		dao.getPublishedHotelIds(testingTools.getHotelGroupId(), function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array().and.have.length(20);
			testingTools.code.expect(res[0]).to.be.string();
			done();
		});
	});
});

lab.experiment('Hotels.dao.getPublishedHotelIds 2', function() {
	testingTools.failDbQueryForExperiment(lab);
	lab.test('should give query error', function(done) {
		dao.getPublishedHotelIds(testingTools.getHotelGroupId(), function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});

lab.experiment('Hotels.dao.getPublishedHotelIds 3', function() {
	testingTools.killDbConnectionForExperiment(lab);
	lab.test('should give query error', function(done) {
		dao.getPublishedHotelIds(testingTools.getHotelGroupId(), function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});
