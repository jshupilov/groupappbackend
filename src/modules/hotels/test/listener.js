/**
 * Created by jevgenishupilov on 19/01/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var eventUtils = require('../../utils/eventUtils');
var services = require('../services');

lab.experiment('Hotels Listener', function() {
	var _deleteHotelGroupHotels;
	lab.before(function (done) {
		_deleteHotelGroupHotels = services.deleteHotelGroupHotels;

		eventUtils.emitter.removeAllListeners('hotelGroup.unpublished');
		testingTools.requireUncached('../modules/hotels/listener');
		testingTools.enableEvents();
		done();
	});
	lab.after(function (done) {
		services.deleteHotelGroupHotels = _deleteHotelGroupHotels;
		testingTools.disableEvents();
		done();
	});
	lab.test('should call services.addHotelToIndex on hotelGroup.unpublished', {timeout: 5000}, function(done){
		var c1 = 0;
		services.deleteHotelGroupHotels = function(hgId, cb){
			c1++;
			cb(null);
		};
		eventUtils.emitter.parallel('hotelGroup.unpublished', testingTools.getHotelGroupId(), function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(c1).to.equal(1);
			done();
		});
	});
	lab.test('should give error services.addHotelToIndex on hotelGroup.unpublished', {timeout: 5000}, function(done){
		var c1 = 0;
		services.deleteHotelGroupHotels = function(hgId, cb){
			c1++;
			cb(new Error('Error'));
		};
		eventUtils.emitter.parallel('hotelGroup.unpublished', testingTools.getHotelGroupId(), function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error');
			testingTools.code.expect(c1).to.equal(1);
			done();
		});
	});
});
