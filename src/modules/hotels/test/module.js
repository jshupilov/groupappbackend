'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var module = require('../module');
var apiUtils = require('../../utils/apiUtils');

lab.experiment('hotels.module', function() {

	var fakeServer = {
		routes: [],
		route: function(def){
			this.routes.push(def);
		}
	};

	lab.test('register should work', function(done) {

		module.register(fakeServer, {}, function(){
			testingTools.code.expect(fakeServer.routes).to.have.length(4);

			testingTools.code.expect(fakeServer.routes[0]).to.be.object();
			testingTools.code.expect(fakeServer.routes[0].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[0].path).to.equal('/hotels');

			testingTools.code.expect(fakeServer.routes[1]).to.be.object();
			testingTools.code.expect(fakeServer.routes[1].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[1].path).to.equal('/hotels/{hotelId}');

			testingTools.code.expect(fakeServer.routes[2]).to.be.object();
			testingTools.code.expect(fakeServer.routes[2].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[2].path).to.equal('/hotels/{hotelId}/contacts');

			testingTools.code.expect(fakeServer.routes[3]).to.be.object();
			testingTools.code.expect(fakeServer.routes[3].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[3].path).to.equal('/categories/{categoryId}/hotels');
			done();
		});
	});

	lab.test('/hotels handler should work', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('some/href?lat=-20.33&long=180&radius=100000000', {}, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		fakeServer.routes[0].handler(req, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.expectNonEmptyCollection(res);
			return testingTools.expectCachingHeaders(done, 'default', res);
		});

	});

	lab.test('/hotels/{hotelId} handler should work', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('hotels/stpeter', {}, {hotelId: 'stpeter'}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		fakeServer.routes[1].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res).to.include(['id', 'href', 'coordinates', 'localized']);
			return testingTools.expectCachingHeaders(done, 'default', res);
		});

	});

	lab.test('/hotels/{hotelId}/contacts handler should work', function(done) {
		var req = {
				apiRequest: apiUtils.createApiRequest('/hotels/cef/contacts',
					{},
					{hotelId: 'jacob'},
					'GET',
					{},
					{remoteAddress: '178.140.172.164'}
				)
		};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');
		fakeServer.routes[2].handler(req, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.object().and.include(['localized']);
			for (var lang in res.localized) {
				testingTools.code.expect(res.localized[lang]).to.include(['primary']);
				testingTools.code.expect(res.localized[lang].primary).to.include(['email', 'phone', 'website']);
			}
			return testingTools.expectCachingHeaders(done, 'noCache', res);

		});

	});

	lab.test('/categories/{categoryId}/hotels handler should work', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('categories/reg-eur/hotels?detail=brochure&search=hotel', {}, {categoryId: 'reg-eur'}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		fakeServer.routes[3].handler(req, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.expectNonEmptyCollection(res);
			for( var l in res.items[0].localized){
				testingTools.code.expect(res.items[0].localized[l]).to.include(['gallery', 'description', 'contacts']);
			}

			return testingTools.expectCachingHeaders(done, 'default', res);
		});

	});
});
