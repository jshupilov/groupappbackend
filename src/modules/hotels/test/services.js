'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var dao = require('../dao');
var searchServices = require('../../search/services');
var hgServices = require('../../hotel_groups/services');
var apiUtils = require('../../utils/apiUtils');
var eventUtils = require('../../utils/eventUtils');
var databaseUtils = require('../../utils/databaseUtils');
var versionDao = require('../../versioning/dao');
var escape = require('pg-escape');
var mediaServices = require('../../media/services');

function getMockHotel(){
	return {
		id: 10000,
		data: {
			hotelId: 'test-hotel',
			hotelGroup: {
				hotelGroupId: testingTools.getHotelGroupId()
			}
		}
	};
}

lab.experiment('hotels.services.getPublishedHotelsByIds', function() {
	testingTools.killDbConnectionForExperiment(lab);
	lab.test('should give error if connection to DB fail', function(done) {
		services.getPublishedHotelsByIds('invalid-hotel-group-id', ['test-hotel-id'], function(err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});

lab.experiment('hotels.services.getPublishedHotelsByIds', function() {
	lab.test('should work as expected', function(done) {
		services.getPublishedHotelsByIds(testingTools.getHotelGroupId(), ['test-hotel-id'], function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array().and.empty();
			done();
		});
	});

	lab.test('should return nothing', function(done) {
		services.getPublishedHotelsByIds(testingTools.getHotelGroupId(), [], function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array().and.empty();
			done();
		});
	});
});

lab.experiment('hotels.services.getHotelHref', function() {
	lab.test('should work as expected', function(done) {
		var hotel = getMockHotel();
		var href = services.getHotelHref(hotel);
		testingTools.code.expect(href).to.be.string();
		testingTools.code.expect(href).to.include([hotel.data.hotelId, hotel.data.hotelGroup.hotelGroupId]);
		done();
	});
});

lab.experiment('hotels.services.getHotelBrochureLink', function() {
	function getReq(){
		var req = apiUtils.createApiRequest('some/href', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');
		return req;
	}
	lab.test('should work with hotel offset', function(done) {
		var req = getReq();
		var link = services.getHotelBrochureLink(req, 1);
		testingTools.code.expect(link).to.be.object();
		testingTools.code.expect(link.href, link.href).to.include(
			[ 'some/href', 'offset=1', 'limit=1', 'detail=brochure']
		);
		done();
	});

	lab.test('should work without hotel offset', function(done) {
		var req = getReq();
		var link = services.getHotelBrochureLink(req);
		testingTools.code.expect(link).to.be.object();
		testingTools.code.expect(link.href, link.href).to.include(
			[ 'some/href', 'limit=1', 'detail=brochure']
		);
		testingTools.code.expect(link.href, link.href).not.to.include(['offset=']);
		done();
	});
});

lab.experiment('hotels.services.getHotels', function() {

	lab.test('should work with brochure details, search and catIds', function(done) {
		var req = apiUtils.createApiRequest('categories/reg-eur/hotels?detail=brochure&search=hotel', {}, {categoryId: 'reg-eur'}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');
		services.getHotels(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.expectNonEmptyCollection(res);
			for( var l in res.items[0].localized){
				testingTools.code.expect(res.items[0].localized[l]).to.include(['gallery', 'description', 'contacts']);
			}

			done();
		});

	});

	lab.test('should work without details, search and catIds', function(done) {
		var req = apiUtils.createApiRequest('some/href', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');
		services.getHotels(req, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.expectNonEmptyCollection(res);
			for( var l in res.items[0].localized){
				testingTools.code.expect(res.items[0].localized[l]).not.to.include(['gallery', 'description', 'contacts']);
			}

			done();
		});

	});
});

lab.experiment('hotels.services.getHotels with geo input', function() {
	lab.test('should work with lat,lang and radius', function(done) {
		var req = apiUtils.createApiRequest('some/href?lat=-20.33&long=180&radius=1', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');
		services.getHotels(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.expectCollection(res);
			testingTools.code.expect(res.items).to.be.empty();
			done();
		});

	});
	lab.test('should work and find smt with lat,lang and radius', function(done) {
		var req = apiUtils.createApiRequest('some/href?lat=-20.33&long=180&radius=100000000', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');
		services.getHotels(req, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.expectNonEmptyCollection(res);
			done();
		});

	});
	lab.test('should failed with empty lat,lang and radius', function(done) {
		var req = apiUtils.createApiRequest('some/href?lat=-20.33&long=180&radius=100000000', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');
		services.getHotels(req, function(err){
			testingTools.expectError(err, 'hs-err-for');
			done();
		});

	});


});

lab.experiment('hotels.services.getHotels with no connection to search engine', function() {
	lab.before(function(done){
		searchServices.init(3000, true);
		done();
	});

	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 503;
		res.end('{"error": "Callback is not given, this is random response", {"message": ""}}', 'utf8');
	});

	lab.after(function(done){
		searchServices.init();
		done();
	});

	lab.test('should fail if connection to search engine fails', function(done) {
		var req = apiUtils.createApiRequest('some/href?lat=-20.33&long=180&radius=1', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');
		services.getHotels(req, function(err){
			testingTools.expectError(err, 'hs-src-err');
			testingTools.code.expect(err.data.debuggingData.reason.causedBy).to.equal('searchHotels');
			done();
		});
	});

});

lab.experiment('hotels.services.getHotels error from DB to getMediaConfiguration', function() {
	var _getHotelGroup;
	lab.before(function(done){
		_getHotelGroup = hgServices.getHotelGroup;
		hgServices.getHotelGroup = function(hgId, cb){
			cb(new Error('Error' + hgId));
		};
		done();
	});
	lab.after(function(done){
		hgServices.getHotelGroup = _getHotelGroup;
		done();
	});
	lab.test('should fail if getHotelGroup fails', function(done) {
		var req = apiUtils.createApiRequest('some/href?lat=-20.33&long=180&radius=1', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');
		services.getHotels(req, function(err){
			testingTools.expectError(err, 'hs-src-err');
			testingTools.code.expect(err.data.debuggingData.reason.causedBy, JSON.stringify(err)).to.equal('getHotelGroup');
			done();
		});
	});

});

lab.experiment('hotels.services.getHotels with error from DB to findByIds', function() {
	var fo = dao.findByIds;
	lab.before(function(done){
		dao.findByIds = function(ids, hgId, cb){
			cb(new Error('Test error'));
		};
		done();
	});

	lab.after(function(done){
		dao.findByIds = fo;
		done();
	});

	lab.test('should fail', function(done) {
		var req = apiUtils.createApiRequest('some/href?lat=-20.33&long=180&radius=1', {}, {}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');
		services.getHotels(req, function(err){
			testingTools.expectError(err, 'hs-src-err');
			testingTools.code.expect(err.data.debuggingData.reason.causedBy, JSON.stringify(err)).to.equal('findByIds');
			done();
		});
	});

});
lab.experiment('hotels.services.getHotelContacts', function () {
    var uuid, req;

    lab.before({timeout: 10000}, function (done) {
        testingTools.getUuid(function (err, _uuid) {
            testingTools.code.expect(err).to.be.null();
            uuid = _uuid;
            done();
        });
    });

    lab.beforeEach(function (done) {
        req = apiUtils.createApiRequest('hotels/jacob/contacts', {}, {hotelId: 'jacob'}, 'GET', {});
        apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
        done();
    });

    testingTools.mockResponsesForExperiment(lab, function (req, res) {
        res.setHeader('content-type', 'application/json');
        res.statusCode = 200;
        res.end(JSON.stringify({}), 'utf8');
    });

    lab.test('should work with default phone number if city, country and continent miss', function (done) {
        req.remoteAddress = '1.2.3.4';
        hgServices.getHotelGroup(req.hotelGroupId, function(err, hg){
            testingTools.code.expect(err).to.be.null();
            services.getHotelContacts(req, function (err1, res) {
                testingTools.code.expect(err1).to.be.null();
                testingTools.code.expect(res).to.be.object().and.include(['localized']);
                for (var lang in res.localized) {
                    testingTools.code.expect(res.localized[lang]).to.include(['primary']);
                    testingTools.code.expect(res.localized[lang].primary).to.include(['email', 'phone', 'website']);
                    testingTools.code.expect(res.localized[lang].primary.phone).to.be.equal(hg.data.callCenterNumbers.defaultNumber);
                }
                done();
            });
        });
    });

});
lab.experiment('hotels.services.getHotelContacts', function () {
    var uuid, req, _getConnection, cd, client;

    lab.before({timeout: 10000}, function (done) {
        _getConnection = databaseUtils.getConnection;
        testingTools.getUuid(function (err, _uuid) {
            testingTools.code.expect(err).to.be.null();
            uuid = _uuid;
            databaseUtils.getConnection(function(err, cl, doneDb){
                testingTools.code.expect(err).to.be.null();
                client = cl;
                cd = doneDb;
                done();
            });
        });
    });

    lab.beforeEach(function (done) {
        req = apiUtils.createApiRequest('hotels/cef/contacts', {}, {hotelId: 'jacob'}, 'GET', {}, {remoteAdress: '178.140.172.164'});
        apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
        done();
    });

    lab.afterEach(function (done) {
        databaseUtils.getConnection = _getConnection;
        done();

    });
    lab.after(function (done) {
        cd();
        done();
    });
    lab.test('should fail if hotel not exists', function (done) {
        req.params.hotelId = 'test-invalid-hotel-id';
        services.getHotelContacts(req, function (err) {
            testingTools.expectError(err, 'hs-hotel-nf');
            done();
        });
    });

    lab.test('should fail if hotel group not exists', function (done) {
        var counter = 0;
        databaseUtils.getConnection = function (cb) {
            cb(
                null,
                {
                    query: function () {
                        counter++;
                        if (counter === 3) {
                            return arguments[arguments.length - 1](new Error('getHotelContacts getHotelGroup Commit failure'));
                        }
                        client.query.apply(client, arguments);
                    }
                },
                function () {
                }
            );
        };
        services.getHotelContacts(req, function (err) {
            testingTools.code.expect(err).to.be.instanceof(Error);
            testingTools.code.expect(err.message).to.equal('getHotelContacts getHotelGroup Commit failure');
            done();
        });
    });



    lab.test('should return default contact phone if invalid ip adress', function (done) {
        req.remoteAddress = '0.0.0.1';
		hgServices.getHotelGroup(req.hotelGroupId, function (err, hotelGroup) {
			testingTools.code.expect(err).to.be.null();
			services.getHotelContacts(req, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res).to.be.object().and.include(['localized']);
				for (var lang in res.localized) {
					testingTools.code.expect(res.localized[lang]).to.be.object().and.include(['primary']);
					testingTools.code.expect(res.localized[lang].primary).to.be.object().and.include(['email', 'phone', 'website']);
					testingTools.code.expect(res.localized[lang].primary.phone).to.equal(hotelGroup.data.callCenterNumbers.defaultNumber);
					testingTools.code.expect(res.callCenterNumbers).to.deep.equal(hotelGroup.data.callCenterNumbers);
				}
				done();
			});
		});
    });

    lab.test('should work with city phone number', function (done) {
        req.remoteAddress = '178.140.172.164';
		hgServices.getHotelGroup(req.hotelGroupId, function (err, hotelGroup) {
			testingTools.code.expect(err).to.be.null();
			services.getHotelContacts(req, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res).to.be.object().and.include(['localized']);
				for (var lang in res.localized) {
					testingTools.code.expect(res.localized[lang]).to.be.object().and.include(['primary']);
					testingTools.code.expect(res.localized[lang].primary).to.be.object().and.include(['email', 'phone', 'website']);
					testingTools.code.expect(res.localized[lang].primary.phone).to.be.equal(hotelGroup.data.callCenterNumbers.cities.Moscow);
				}
				done();
			});
		});
    });
    lab.test('should work with country phone number', function (done) {
        req.remoteAddress = '1.1.1.10';
		hgServices.getHotelGroup(req.hotelGroupId, function (err, hotelGroup) {
			testingTools.code.expect(err).to.be.null();
			services.getHotelContacts(req, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res).to.be.object().and.include(['localized']);
				for (var lang in res.localized) {
					testingTools.code.expect(res.localized[lang]).to.be.object().and.include(['primary']);
					testingTools.code.expect(res.localized[lang].primary).to.be.object().and.include(['email', 'phone', 'website']);
					testingTools.code.expect(res.localized[lang].primary.phone).to.be.equal(hotelGroup.data.callCenterNumbers.countries.Australia);
				}
				done();
			});
		});
    });

    lab.test('should work with continent phone number', function (done) {
        req.remoteAddress = '80.235.85.10';
		hgServices.getHotelGroup(req.hotelGroupId, function (err, hotelGroup) {
			testingTools.code.expect(err).to.be.null();
			services.getHotelContacts(req, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res).to.be.object().and.include(['localized']);
				for (var lang in res.localized) {
					testingTools.code.expect(res.localized[lang]).to.be.object().and.include(['primary']);
					testingTools.code.expect(res.localized[lang].primary).to.be.object().and.include(['email', 'phone', 'website']);
					testingTools.code.expect(res.localized[lang].primary.phone).to.be.equal(hotelGroup.data.callCenterNumbers.continents.Europe);
				}
				done();
			});
		});
    });
});

lab.experiment('hotels.services.getHotelContacts without callcenter cnumbers', function() {

	var uuid;

	lab.before({timeout: 10000}, function (done) {
		testingTools.getUuid(function (err, _uuid) {
			testingTools.code.expect(err).to.be.null();
			uuid = _uuid;
			done();
		});
	});

	testingTools.updateDbRowForExperiment(lab, 'hotel_group',
			function(row){
				delete row.data.callCenterNumbers;
			},
			['(data ->> \'hotelGroupId\') = %L', testingTools.getHotelGroupId()]
	);
	lab.test('should return hotel contact phone if call center info miss in hotel group', function (done) {
		var req = apiUtils.createApiRequest('hotels/jacob/contacts', {}, {hotelId: 'jacob'}, 'GET', {}, {remoteAdress: '178.140.172.164'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
		req.hotelGroupId = testingTools.getHotelGroupId();
		req.params.hotelId = 'jacob';
		req.remoteAddress = '178.140.172.164';

		databaseUtils.getConnection(function (err1, client, done1) {
			testingTools.code.expect(err1).to.be.null();
			client.query(
					'SELECT * ' +
					'FROM hotel ' +
					'WHERE (data -> \'hotelGroup\' -> \'hotelGroupId\') = $1 ' +
					'AND (data -> \'hotelId\') = $2',
					['"' + req.hotelGroupId + '"', '"' + req.params.hotelId + '"'],
					function (err2, hotel) {
						done1();
						testingTools.code.expect(err2).to.be.null();
						hgServices.getHotelGroup(req.hotelGroupId, function (err3, hotelGroup) {
							testingTools.code.expect(err3).to.be.null();
							var frmtHotel = services.formatHotel({}, hotel.rows[0], 'full', hotelGroup.data);
							services.getHotelContacts(req, function (err, res) {
								testingTools.code.expect(err).to.be.null();
								testingTools.code.expect(res).to.be.object().and.include(['localized']);
								testingTools.code.expect(res).not.to.include(['callCenterNumbers']);
								for (var lang in res.localized) {
									testingTools.code.expect(res.localized[lang]).to.deep.equal(frmtHotel.localized[lang].contacts);
								}
								done();
							});
						});
					}
			);
		});
	});
});

lab.experiment('hotels.services.getHotel', function() {
	lab.test('should work with low detail', function(done) {
		var req = apiUtils.createApiRequest('hotels/stpeter', {}, {hotelId: 'stpeter'}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.getHotel(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res).to.include(['id', 'href', 'coordinates', 'localized']);
			done();
		});
	});

	lab.test('should work with brochure detail', function(done) {
		var req = apiUtils.createApiRequest('hotels/stpeter?detail=brochure', {}, {hotelId: 'stpeter'}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.getHotel(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res).to.include(['id', 'href', 'coordinates', 'localized']);
			done();
		});

	});
});

lab.experiment('hotels.services.getHotel without DB', function() {
	testingTools.killDbConnectionForExperiment(lab);
	lab.test('should give error', function(done) {
		var req = apiUtils.createApiRequest('hotels/stpeter?detail=brochure', {}, {hotelId: 'stpeter'}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.getHotel(req, function(err){
			testingTools.expectError(err);
			done();
		});

	});
});

lab.experiment('hotels.services.getHotel with invalid id', function() {

	lab.test('should give error if not found', function(done) {
		var req = apiUtils.createApiRequest('hotels/stpeterasdsa?detail=brochure', {}, {hotelId: 'stpeterasdasd'}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.getHotel(req, function(err){
			testingTools.expectError(err, 'hs-hotel-nf');
			done();
		});

	});
});

lab.experiment('hotels.services.getHotel with error on dao.findById', function() {
	var o;

	lab.before(function(done){
		o = dao.findById;
		dao.findById = function(id, hgId, cb){
			cb(new Error('some error'));
		};
		done();
	});

	lab.after(function(done){
		dao.findById = o;
		done();
	});

	lab.test('should give error if not found', function(done) {
		var req = apiUtils.createApiRequest('hotels/stpeterasdsa?detail=brochure', {}, {hotelId: 'stpeterasdasd'}, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'some-uuid');

		services.getHotel(req, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});

	});
});

lab.experiment('hotels.services.getHotelRaw', function() {
	lab.test('should work', function(done) {
		services.getHotelRaw(testingTools.getHotelGroupId(), 'stpeter', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res).to.include(['id', 'data']);
			done();
		});
	});
});

lab.experiment('hotels.services.getHotelRaw without DB', function() {
	testingTools.killDbConnectionForExperiment(lab);

	lab.test('should give error', function(done) {
		services.getHotelRaw(testingTools.getHotelGroupId(), 'stpeter', function(err){
			testingTools.expectError(err);
			done();
		});
	});
});

lab.experiment('hotels.services.getHotelRaw with invalid id', function() {

	lab.test('should give error', function(done) {
		services.getHotelRaw(testingTools.getHotelGroupId(), 'stpeterasdasd', function(err){
			testingTools.expectError(err, 'hs-hotel-nf');
			done();
		});
	});
});

lab.experiment('hotels.services.createHotel', function() {
	var hotel, hotelData, hotelGroupId, hotelIdToBe;

	lab.before(function(done){
		hotelIdToBe = 'hotel-plain-create';
		services.getHotelRaw(testingTools.getHotelGroupId(), 'stpeter', function(err, res){
			testingTools.code.expect(err).to.be.null();
			hotel = JSON.stringify(res.data).replace(/stpeter/g, hotelIdToBe);
			hotel = JSON.parse(hotel);
			hotelGroupId = hotel.hotelGroup.hotelGroupId;
			hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, resHG){
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				hotelData = resHG.data;

				done();
			});
		});
	});

	lab.after(function(done){
		//Clean up test data
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			client.query(
				"DELETE FROM hotel_v WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 AND (data -> 'hotelId') = $2 ",
				['"' + hotel.hotelGroup.hotelGroupId + '"', '"' + hotel.hotelId + '"'],
				function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.equal(1);
					doneDb();
					done();
				}
			);
		});
	});

	lab.test('should given error with invalid validation', function(done) {
		hotel.hotelGroup.hotelGroupId = 'some-test-totel-group-id';
		services.createHotel(hotel, hotelData, function(err){
			testingTools.expectError(err, 'hv-hgId-exists');
			done();
		});
	});

	lab.test('should work', function(done) {
		hotel.hotelGroup.hotelGroupId = hotelGroupId;
		services.createHotel(hotel, hotelData, function(err){
			testingTools.code.expect(err).to.be.null();
			databaseUtils.getConnection(function(err1, client, doneDb){
				testingTools.code.expect(err1).to.be.null();
				client.query(
					'SELECT * ' +
					'FROM hotel_v ' +
					"WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 AND (data -> 'hotelId') = $2",
					['"' + hotelGroupId + '"', '"' + hotelIdToBe + '"'],
					function(err2, result) {
						testingTools.code.expect(err2).to.be.null();
						testingTools.code.expect(result.rowCount).to.be.equal(1);
						doneDb();
						done();
					}
				);
			});
		});
	});
});

lab.experiment('hotels.services.createHotel with DB error', function() {
	var o, hotel, hotelData;

	lab.before(function (done) {
		o = versionDao.create;
		versionDao.create = function (daa, tb, cb) {
			cb(new Error('Error db save'));
		};
		services.getHotelRaw(testingTools.getHotelGroupId(), 'stpeter', function (err, res) {
			testingTools.code.expect(err).to.be.null();
			hotel = JSON.stringify(res.data).replace(/stpeter/g, 'hotel-blabla-db-error-test');
			hotel = JSON.parse(hotel);

			hgServices.getHotelGroup(testingTools.getHotelGroupId(), function (err, resHG) {
				testingTools.code.expect(err).to.be.null();
				hotelData = resHG.data;
				done();
			});
		});
	});

	lab.after(function(done){
		versionDao.create = o;
		done();
	});

	lab.test('should fail if database insert gives error', function(done) {
		services.createHotel(hotel, hotelData, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error db save');
			done();

		});
	});
});


lab.experiment('hotels.services.getHotelsRaw', function() {
	var _getAll;
	lab.before(function (done) {
		_getAll = dao.getAll;
		done();
	});
	lab.after(function (done) {
		dao.getAll = _getAll;
		done();
	});
	lab.test('should fail', function(done) {
		dao.getAll = function(hgId, cb){
			cb(new Error('Error'));
		};
		services.getHotelsRaw(testingTools.getHotelGroupId(), function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error');
			done();
		});
	});
	lab.test('should work', function(done) {
		dao.getAll = function(hgId, cb){
			cb(null, {
				rows: [{
					id: 'test-id',
					data: {}
				}]
			});
		};
		services.getHotelsRaw(testingTools.getHotelGroupId(), function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array();
			testingTools.code.expect(res[0]).to.include(['id', 'data']);
			done();
		});
	});
});

lab.experiment('hotels.services.deleteHotelGroupHotels', function() {
	var o, c, hgId;
	lab.before(function(done){
		hgId = testingTools.getHotelGroupId() + '_nonex';
		o = dao.deleteHotelGroupHotels;
		dao.deleteHotelGroupHotels = function(hgId, cb){
			if(!c){
				c = true;
				cb(new Error('Error'));
			}else{
				cb(null);
			}
		};
		done();
	});

	lab.after(function(done){
		dao.deleteHotelGroupHotels = o;
		done();
		testingTools.disableEvents();
	});

	lab.test('should fail if DB query fails', function(done) {
		services.deleteHotelGroupHotels(hgId, function(err){
			testingTools.expectError(err, 'hs-err-del');
			done();
		});
	});

	lab.test('should succeed', function(done) {
		services.deleteHotelGroupHotels(hgId, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});



	lab.test('should fail if some listener fails', function(done) {
		testingTools.enableEvents();
		eventUtils.emitter.removeAllListeners('hotel.deletedAllInHotelGroup');
		eventUtils.emitter.once('hotel.deletedAllInHotelGroup', function(data, callback){
			callback( new Error('Error'));
		});


		services.deleteHotelGroupHotels(hgId, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error');
			done();
		});
	});
});

lab.experiment('hotels.services.deleteHotel', function() {
	var b, c;

	lab.before(function(done){
		testingTools.enableEvents();
		b = dao.deleteHotel;
		dao.deleteHotel = function(hgId, hotelId, cb){
			if(!c){
				c = true;
				cb(new Error('Error'));
			}else{
				cb(null);
			}
		};
		done();
	});

	lab.after(function(done){
		dao.deleteHotel = b;
		testingTools.disableEvents();
		done();
	});

	lab.test('should fail if DB connection fails', function(done) {
		services.deleteHotel('asdsadasd', 'adsasd', function(err){
			testingTools.expectError(err, 'hs-err-del');
			done();
		});
	});

	lab.test('should succeed', {timeout: 5000}, function(done) {
		services.deleteHotel('asdsadasd', 'adsasd', function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});
	});

	lab.test('should fail if some listener fails', function(done) {
		eventUtils.emitter.removeAllListeners('hotel.unpublished');
		eventUtils.emitter.once('hotel.deleted', function(hotelGroupId, hotelId, callback){
			callback( new Error('Error'));
		});

		services.deleteHotel('asdsadasd', 'adsasd', function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error');
			done();
		});
	});
});

lab.experiment('hotels.services.formatHotel', function() {
	var hotelData;
	var hotelGroupData;
	var getHotelData;
	var getHotelGroupData;
	var hgId;
	var validateMenuItem;

	lab.before(function(done){
		hgId = testingTools.getHotelGroupId();
		services.getHotelRaw(hgId, 'stpeter', function(err, hotel){
			testingTools.code.expect(err).to.be.null();
			hotelData = hotel;
			getHotelData = function(){
				return JSON.parse(JSON.stringify(hotelData));
			};
			hgServices.getHotelGroup(hgId, function(err, hotelGroup){
				testingTools.code.expect(err).to.be.null();
				hotelGroupData = hotelGroup.data;
				getHotelGroupData = function(){
					return JSON.parse(JSON.stringify(hotelGroupData));
				};
				validateMenuItem = function(item){
					testingTools.code.expect(item).to.include(['action', 'isEnabled', 'name', 'data']);
					testingTools.code.expect(item.action).to.be.a.string().and.to.not.be.empty();
					testingTools.code.expect(item.name).to.be.a.string().and.to.not.be.empty();
					testingTools.code.expect(item.isEnabled).to.be.a.boolean();
					testingTools.code.expect(item.data).to.be.an.object();
				};
				done();
			});
		});
	});

	lab.test('should return booking URL if it is set', function(done) {

		var bookingUrl = 'https://some.url.ee';
		var hotel = getHotelData();
		var hotelGroup = getHotelGroupData();

		for(var i in hotel.data.localized) {
			var hd = getHotelData();
			var lang = hd.data.localized[i].language;

			hd.data.localized[i].bookingUrl = bookingUrl;

			var formatted = services.formatHotel({}, hd, 'brochure', hotelGroup);
			testingTools.code.expect(formatted.localized[lang].bookingUrl).to.equal(bookingUrl);
		}

		done();
	});

	lab.test('should return placeholder image if all hotel galleries are empty', function(done) {

		var hotel = getHotelData();
		var hotelGroup = getHotelGroupData();
		var hd = getHotelData();

		for(var i in hotel.data.localized) {
			hd.data.localized[i].gallery = [];
		}

		var formatted = services.formatHotel({}, hd, 'brochure', hotelGroup);
		var placeholderImage = mediaServices.formatImageObject(hotelGroup.mediaConfiguration.placeholderImage, hotelGroup.mediaConfiguration, 'hotelGal');

		for(var y in hotel.data.localized) {
			var lang = hd.data.localized[y].language;
			testingTools.code.expect(formatted.localized[lang].gallery[0]).to.deep.equal(placeholderImage);
		}


		done();
	});

	lab.test('should return localized[x].menu as empty array if all depended keys are null if every menu item has removedIfDisable key equal to true', function(done) {
		var hotel = getHotelData();
		var hotelGroup = getHotelGroupData();

		for(var i in hotel.data.localized){
			var hd = getHotelData();
			var lang = hd.data.localized[i].language;

			hd.data.coordinates.lat = null;
			hd.data.coordinates.long = null;
			hd.data.localized[i].contacts.primary.website = null;
			hd.data.localized[i].contacts.primary.phone = null;
			hd.data.localized[i].name = null;
			hd.data.localized[i].bookingUrl = null;

			var formatted = services.formatHotel({}, hd, 'brochure', hotelGroup);
			testingTools.code.expect(formatted.localized[lang].menu).to.be.array().and.have.length(0);
		}

		done();
	});

	lab.test('should return localized[x].menu as empty array if all depended keys are missing if every menu item has removedIfDisable key equal to true', function(done) {
		var hotel = getHotelData();
		var hotelGroup = getHotelGroupData();


		for(var i in hotel.data.localized){
			var hd = getHotelData();
			var lang = hd.data.localized[i].language;

			delete hd.data.coordinates.lat;
			delete hd.data.coordinates.long;
			delete hd.data.localized[i].contacts.primary.website;
			delete hd.data.localized[i].contacts.primary.phone;
			delete hd.data.localized[i].name;
			delete hd.data.localized[i].bookingUrl;

			var formatted = services.formatHotel({}, hd, 'brochure', hotelGroup);
			testingTools.code.expect(formatted.localized[lang].menu).to.have.length(0);
		}

		done();
	});

	lab.test('should return localized[x].menu with items, which contain `action`, `isEnabled`, `name` and `data` keys', function(done) {
		var hotel = getHotelData();
		var hotelGroup = getHotelGroupData();

		for(var i in hotel.data.localized){
			var hd = getHotelData();
			var lang = hd.data.localized[i].language;

			var formatted = services.formatHotel({}, hd, 'brochure', hotelGroup);
			testingTools.code.expect(formatted.localized[lang].menu.length).to.be.above(0);
			for(var m in formatted.localized[lang].menu ){
				validateMenuItem(formatted.localized[lang].menu[m]);
			}
		}

		done();
	});
	lab.test('should return localized[x].menu[x].isEnabled equal to false if removeIfDisabled equal to false and items are missing', function(done) {
		var hotel = getHotelData();
		var hotelGroup = getHotelGroupData();
		for(var y in hotelGroup.viewConfiguration.hotelMenu) {
			hotelGroup.viewConfiguration.hotelMenu[y].removeIfDisabled = false;
		}
		for (var i in hotel.data.localized) {
			var hd = getHotelData();
			var lang = hd.data.localized[i].language;

			delete hd.data.coordinates.lat;
			delete hd.data.coordinates.long;
			delete hd.data.localized[i].contacts.primary.website;
			delete hd.data.localized[i].contacts.primary.phone;
			delete hd.data.localized[i].name;
			delete hd.data.localized[i].bookingUrl;

			var formatted = services.formatHotel({}, hd, 'brochure', hotelGroup);

			testingTools.code.expect(formatted.localized[lang].menu.length).to.be.above(0);
			for (var m in formatted.localized[lang].menu) {
				validateMenuItem(formatted.localized[lang].menu[m]);
				testingTools.code.expect(formatted.localized[lang].menu[m].isEnabled).to.equal(false);
			}
		}

		done();
	});
	lab.test('should return localized[x].menu if one of action in hotelGroup.viewConfiguration.hotelMenu is missing', function(done) {
		var hotel = getHotelData();
		var hotelGroup = getHotelGroupData();
		for(var y in hotelGroup.viewConfiguration.hotelMenu) {
			var hgd = getHotelGroupData();
			delete hgd.viewConfiguration.hotelMenu[y];

			for (var i in hotel.data.localized) {
				var hd = getHotelData();
				var lang = hd.data.localized[i].language;

				var formatted = services.formatHotel({}, hd, 'brochure', hgd);

				testingTools.code.expect(formatted.localized[lang].menu.length).to.be.above(0);

				for (var m in formatted.localized[lang].menu) {
					validateMenuItem(formatted.localized[lang].menu[m]);
				}
			}
		}

		done();
	});
	lab.test('should return localized[x].menu if one of action in hotelGroup.viewConfiguration.hotelMenu has not conditions', function(done) {
		var hotel = getHotelData();
		var hotelGroup = getHotelGroupData();
		hotelGroup.viewConfiguration.hotelMenu.push({
			action: 'test-action'
		});
		for (var i in hotel.data.localized) {
			var hd = getHotelData();
			var lang = hd.data.localized[i].language;

			var formatted = services.formatHotel({}, hd, 'brochure', hotelGroup);

			testingTools.code.expect(formatted.localized[lang].menu.length).to.be.above(0);

			for (var m in formatted.localized[lang].menu) {
				validateMenuItem(formatted.localized[lang].menu[m]);
			}
		}
		done();
	});

	lab.test('should return localized[x].menu as empty array if viewConfiguration.hotelMenu equal to FALSE', function(done) {
		var hotel = getHotelData();
		var hotelGroup = getHotelGroupData();
		hotelGroup.viewConfiguration.hotelMenu = false;
		for (var i in hotel.data.localized) {
			var hd = getHotelData();
			var lang = hd.data.localized[i].language;

			var formatted = services.formatHotel({}, hd, 'brochure', hotelGroup);

			testingTools.code.expect(formatted.localized[lang].menu).have.length(0);

		}
		done();
	});
});

lab.experiment('hotels.publish', function(){
	var daoPublish, daoUpdate;
	var tableName = 'hotel';
	var hotelId = 'test-publish-success';
	var hotelGroupId = 'test-hgid';
	var now = new Date();
	var yesterday = now.setDate(now.getDate() - 1);
	var htlData = {
		'data': {
			'meta': {
				'createdAt': new Date(yesterday).toISOString(),
				'scheduledAt': new Date(yesterday).toISOString(),
				'appliedAt': null,
				'status': 'draft',
				'guid': hotelId + '_' + hotelGroupId
			},
			'hotelId': hotelId,
			'hotelGroup': {
				'hotelGroupId': hotelGroupId
			},
			'coordinates': {
				'lat': 50.941857,
				'long': 6.956522
			}
		}
	};
	lab.before(function(done){
		eventUtils.emitter.removeAllListeners('hotel.published');
		testingTools.enableEvents();
		daoUpdate = versionDao.update;
		done();
	});
	lab.beforeEach(function(done){
		daoPublish = dao.publishHotel;
		done();
	});
	lab.afterEach(function(done){
		dao.publishHotel = daoPublish;
		versionDao.update = daoUpdate;
		done();
	});
	lab.after(function(done){
		testingTools.disableEvents();
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape("DELETE FROM %I WHERE (data->'hotelGroup'->'hotelGroupId') = '%I' AND (data->'hotelId')= '%I'",
				tableName, hotelGroupId, hotelId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.above(0);
				doneDb();
				done();
			});
		});
	});
	lab.test('Publish should go well', function(done){
		services.publishHotel(htlData, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('Dao should give error', function(done){
		dao.publishHotel = function(item, cb){
			cb(new Error('daofailure'));
		};
		services.publishHotel(htlData, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('daofailure');
			done();
		});
	});

	lab.test('Update should give error', function(done){
		dao.publishHotel = function(item, cb){
			cb(null);
		};
		versionDao.update = function(i, t, cb){
			cb(new Error('versionDao.update.err'));
		};
		services.publishHotel(htlData, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('versionDao.update.err');
			done();
		});
	});


	lab.test('Event should give failure', function(done){
		eventUtils.emitter.on('hotel.published', function(htl, cb){
			cb(new Error('daopublisherror'));
		});
		services.publishHotel(htlData, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('daopublisherror');
			done();
		});
	});
});

lab.experiment('hotels.unpublish', function(){
	var daoUnPublish, daoUpdate;
	var tableName = 'hotel';
	var hotelId = 'test-unpublish-success666';
	var hotelGroupId = 'test-hgid';
	var now = new Date();
	var yesterday = now.setDate(now.getDate() - 1);
	var htlData = {
		'data': {
			'meta': {
				'createdAt': new Date(yesterday).toISOString(),
				'scheduledAt': new Date(yesterday).toISOString(),
				'appliedAt': null,
				'status': 'draft',
				'guid': hotelId + '_' + hotelGroupId
			},
			'hotelId': hotelId,
			'hotelGroup': {
				'hotelGroupId': hotelGroupId
			},
			'coordinates': {
				'lat': 50.941857,
				'long': 6.956522
			}
		}
	};
	lab.before(function(done){
		daoUpdate = versionDao.update;
		eventUtils.emitter.removeAllListeners('hotel.unpublished');
		testingTools.enableEvents();
		done();
	});
	lab.beforeEach(function(done){
		daoUnPublish = dao.unPublishHotel;
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', tableName, JSON.stringify(htlData.data));
			client.query(query, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				doneDb();
				done();
			});
		});
	});
	lab.afterEach(function(done){
		dao.unPublishHotel = daoUnPublish;
		versionDao.update = daoUpdate;
		done();
	});
	lab.after(function(done){
		testingTools.disableEvents();
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape("DELETE FROM %I WHERE (data->'hotelGroup'->'hotelGroupId') = '%I' AND (data->'hotelId')= '%I'",
				tableName, hotelGroupId, hotelId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(0);
				doneDb();
				done();
			});
		});
	});
	lab.test('Publish should go well', function(done){
		services.unPublishHotel(htlData, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('Dao should give error', function(done){
		dao.unPublishHotel = function(item, cb){
			cb(new Error('daofailure'));
		};
		services.unPublishHotel(htlData, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('daofailure');
			done();
		});
	});

	lab.test('Update should give error', function(done){
		dao.unPublishHotel = function(item, cb){
			cb(null);
		};

		versionDao.update = function(t, i, cb){
			cb(new Error('versionDao.update.errr'));
		};
		services.unPublishHotel(htlData, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('versionDao.update.errr');
			done();
		});
	});
	lab.test('Event should give failure', function(done){
		eventUtils.emitter.on('hotel.unpublished', function(htl, hg, cb){
			cb(new Error('daopublisherror'));
		});
		services.unPublishHotel(htlData, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('daopublisherror');
			done();
		});
	});
});

lab.experiment('Hotels.Update', function(){
	var table = 'hotel_v';
	var hotel, e;
	var hotelData;
	var hotelGroupId;
	var hotelIdToBe = 'hotel-plain-create';
	lab.before(function(done){
		e = new Error('some test error 23ds');

		//fail the event

		testingTools.enableEvents();
		services.getHotelRaw(testingTools.getHotelGroupId(), 'stpeter', function(err, res){
			testingTools.code.expect(err).to.be.null();
			hotel = JSON.stringify(res.data).replace(/stpeter/g, hotelIdToBe);
			hotel = JSON.parse(hotel);
			databaseUtils.getConnection(function(err, client, doneDb){
				testingTools.code.expect(err).to.be.null();
				var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', table, JSON.stringify(hotel));
				client.query(query, function(err){
					testingTools.code.expect(err).to.be.null();
					hotelGroupId = hotel.hotelGroup.hotelGroupId;
					hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, resHG){
						testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
						hotelData = resHG.data;
						doneDb();
						done();
					});
				});
			});
		});
	});

	lab.after(function(done){
		testingTools.disableEvents();
		//Clean up test data
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			client.query(
				"DELETE FROM hotel_v WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 AND (data -> 'hotelId') = $2 ",
				['"' + hotel.hotelGroup.hotelGroupId + '"', '"' + hotel.hotelId + '"'],
				function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.equal(3);
					doneDb();
					done();
				}
			);
		});
	});

	lab.test('should given error with invalid validation', function(done) {
		hotel.hotelGroup.hotelGroupId = 'some-test-totel-group-id';
		services.updateHotel(hotel, hotelData, function(err){
			testingTools.expectError(err, 'hv-hgId-exists');
			done();
		});
	});

	lab.test('should work', function(done) {
		hotel.hotelGroup.hotelGroupId = hotelGroupId;
		hotel.meta.status = 'draft';
		services.updateHotel(hotel, hotelData, function(err){
			testingTools.code.expect(err).to.be.null();
			databaseUtils.getConnection(function(err, client, doneDb){
				testingTools.code.expect(err).to.be.null();
				client.query(
					'SELECT * ' +
					'FROM hotel_v ' +
					"WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 AND (data -> 'hotelId') = $2",
					['"' + hotelGroupId + '"', '"' + hotelIdToBe + '"'],
					function(err, result) {
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(result.rowCount).to.be.equal(2);
						doneDb();
						done();

					});
			});
		});
	});
	lab.test('Should give error', function(done) {
		hotel.hotelGroup.hotelGroupId = hotelGroupId;
		hotel.meta.status = 'unknown_status';
		services.updateHotel(hotel, hotelData, function(err){
			testingTools.expectError(err, 'ver-inv-status');
			done();
		});
	});

	lab.test('should fail if some listener gives error', function(done) {
		eventUtils.emitter.removeAllListeners('hotel.updated');
		eventUtils.emitter.once('hotel.updated', function(data, callback){
			callback(e);
		});
		hotel.meta.status = 'published';
		services.updateHotel(hotel, hotelData, function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.equal(e);
			done();
		});
	});
});

lab.experiment('Hotels.services.getPublishedHotelIds', function() {
	lab.test('should work', function(done) {
		services.getPublishedHotelIds(testingTools.getHotelGroupId(), function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array();
			done();
		});
	});
});

lab.experiment('Hotels.services.createOrUpdateHotel', function() {
	var f1, f2, hotelGroup;

	lab.before(function(done){
		f1 = services.updateHotel;
		f2 = services.createHotel;
		hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, hg){
			testingTools.code.expect(err).to.be.null();
			hotelGroup = hg;
			done();
		});
	});

	lab.afterEach(function(done){
		services.updateHotel = f1;
		services.createHotel = f2;
		done();
	});

	lab.test('should pass on validation errors', function(done) {
		services.createOrUpdateHotel({}, {}, function(err){
			testingTools.expectError(err, 'hv-key-miss');
			done();
		});
	});

	lab.test('should update if existing', function(done) {

		services.updateHotel = function(d1, d2, cb){
			cb(null, 'doneUpdate');
		};

		services.getHotelRaw(testingTools.getHotelGroupId(), 'jacob', function(err, hotel){
			testingTools.code.expect(err).to.be.null();
			services.createOrUpdateHotel(hotel.data, hotelGroup, function(err, res){
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(res).to.equal('doneUpdate');
				done();
			});
		});

	});

	lab.test('should crate if not existing', function(done) {

		services.createHotel = function(d1, d2, cb){
			cb(null, 'doneCreate');
		};
		services.getHotelRaw(testingTools.getHotelGroupId(), 'jacob', function(err, hotel){
			testingTools.code.expect(err).to.be.null();
			var hotelNew = JSON.parse(JSON.stringify(hotel).replace(/jacob/g, '98adf3iu4ntu3h'));
			services.createOrUpdateHotel(hotelNew.data, hotelGroup.data, function(err, res){
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(res).to.equal('doneCreate');
				done();
			});
		});

	});

});
