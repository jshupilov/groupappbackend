'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var validation = require('../validation');
var dao = require('../dao');
var q = require('q');

lab.experiment('HotelGroups.validation.validateHotelData generally', function() {

	lab.test('should not accept invalid data type', function(done) {
		validation.validateHotelData(123, 'full', function(err){
			testingTools.expectError(err, 'hv-inv-type');
			done();
		});
	});

	lab.test('should not accept invalid validation type', function(done) {
		validation.validateHotelData({}, 'asdasdas', function(err){
			testingTools.expectError(err, 'hv-inv-val-type');
			done();
		});
	});

});

lab.experiment('Hotel.validation.validateHotelData with \'full\' validation', {timeout: 5000}, function() {

	var getValidHotelData;
	var validateSuccess;
	var getValidContextData;
	var getKeys;
	var getNewObject;
	var removeKeys;
	var hotelData;
	var validateHotelData;
	var validationType = 'full';
	var context;
	var _checkIfExists;
	
	lab.before(function(done){
		_checkIfExists = dao.checkIfExists;
		getNewObject = function(data){
			return JSON.parse(JSON.stringify(data));
		};
		validateSuccess = function(data, p, context){
			validation.validateHotelData(data, validationType, function(err){
				p.resolve();
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();

			}, context);
		};
		validateHotelData = function(data, context, p, errKey, key){
			validation.validateHotelData(data, validationType, function(err){
				p.resolve();
				testingTools.expectError(err, errKey);
				if (key) {
					testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).to.equal(key);
				}
			}, context);
		};

		getKeys = function(data, key, value){
			var checks = [];
			//check for every theme[x] key, if it has been validated
			for( var k in data ){
				var object = getNewObject(data[k]);
				object[key] = value;
				checks.push(object);
			}
			return checks;
		};
		removeKeys = function(data, key){
			var checks = [];
			for(var k in data) {
				var object = getNewObject(data[k]);

				delete object[key];
				checks.push(object);
			}
			return checks;
		};
		//create function to get valid hotel data sample
		getValidHotelData = function(){
			var imagePrefix = process.env.CLOUDINARY_BASE_FOLDER;
			var hId = 'test-hotel-id';
			var hgId = 'test-hotel-group-id';

			var data = {
				'meta': {'createdAt': '2014-11-01T00:00:00+00:00'},
				'gallery': [{
					'source': {
						'public_id': imagePrefix + '/group-app/' + hgId + '/hotels/' + hId + '/gallery/01429282823000',
						'isCloudinary': true,
						'resource_type': 'image'
					}
				}, {
					'source': {
						'public_id': imagePrefix + '/group-app/' + hgId + '/hotels/' + hId + '/gallery/01429282823000',
						'isCloudinary': true,
						'resource_type': 'image'
					}
				}, {
					'source': {
						'public_id': imagePrefix + '/group-app/' + hgId + '/hotels/' + hId + '/gallery/01429282823000',
						'isCloudinary': true,
						'resource_type': 'image'
					}
				}],
				'hotelId': hId,
				'version': {'number': 1},
				'vhSiteKey': 'test-site-key',
				'gaTrackingId': 'UA-42721275-2',
				'localized': [{
					'name': 'Hotel Berna',
					'gallery': [{
						'source': {
							'public_id': imagePrefix + '/group-app/' + hgId + '/hotels/' + hId + '/gallery/01429282823000',
							'isCloudinary': true,
							'resource_type': 'image'
						}
					}, {
						'source': {
							'public_id': imagePrefix + '/group-app/' + hgId + '/hotels/' + hId + '/gallery/01429282823000',
							'isCloudinary': true,
							'resource_type': 'image'
						}
					}],
					'contacts': {
						'primary': {
							'email': 'info@hotelberna.com',
							'phone': '+390294755705',
							'website': 'http://website.com'
						}
					},
					'language': 'en_GB',
					'location': {
						'city': 'Milano',
						'country': 'Italy',
						'fullAddress': 'Via Napo Torriani, 18, 20124 Milano, Italy',
						'shortAddress': 'Via Napo Torriani, Milano, Italy'
					},
					'description': [{
						'text': [{
							'style': null,
							'value': 'Hotel Berna has 116 rooms. In the main building there are 100 rooms of all types, the Reception Desk, the Breakfast Room and the American Bar. The Berna Tower is located opposite; this is a quiet and secluded annex with 16 rooms, with carpeting, double beds and the utmost privacy.'
						}, {
							'style': null,
							'value': 'Hotel Berna has 116 rooms. In the main building there are 100 rooms of all types, the Reception Desk, the Breakfast Room and the American Bar. The Berna Tower is located opposite; this is a quiet and secluded annex with 16 rooms, with carpeting, double beds and the utmost privacy.'
						}],
						'heading': [{
							'style': null,
							'value': 'Our hotel'
						}]
					}, {
						'text': [{
							'style': null,
							'value': 'First of all, our welcoming smile. However, the smiles after will be many: when we welcome you and during your stay. And there will also be your smiles when you discover our qualities, the cuddles that we have reserved for you and the surprises prepared just for you. From the breakfast to the cleaning, from the choice of the room temperature to the silence. In the heart of Milano you will discover the atmosphere of one of your second homes.'
						}],
						'heading': [{
							'style': null,
							'value': 'Hospitality'
						}]
					}],
					'bookingUrl': 'https://booking.slh.com/en-GB/Room/Availability/?HotelCode=HUTLLTE&StartDate=<StartDate>&NoOfNights=<NoOfNights>&NoOfAdults=<NoOfAdults>'
				}, {
					'name': 'Hotel Testing',
					'gallery': [{
						'source': {
							'public_id': imagePrefix + '/group-app/' + hgId + '/hotels/' + hId + '/gallery/01429282823000',
							'isCloudinary': true,
							'resource_type': 'image'
						}
					}, {
						'source': {
							'public_id': imagePrefix + '/group-app/' + hgId + '/hotels/' + hId + '/gallery/01429282823000',
							'isCloudinary': true,
							'resource_type': 'image'
						}
					}],
					'contacts': {
						'primary': {
							'email': 'info@hotelberna.com',
							'phone': '+390294755705',
							'website': 'http://website.com'
						}
					},
					'language': 'ru_RU',
					'location': {
						'city': 'Milano',
						'country': 'Italy',
						'fullAddress': 'Via Napo Torriani, 18, 20124 Milano, Italy',
						'shortAddress': 'Via Napo Torriani, Milano, Italy'
					},
					'description': [{
						'text': [{
							'style': null,
							'value': 'Hotel Berna has 116 rooms. In the main building there are 100 rooms of all types, the Reception Desk, the Breakfast Room and the American Bar. The Berna Tower is located opposite; this is a quiet and secluded annex with 16 rooms, with carpeting, double beds and the utmost privacy.'
						}, {
							'style': null,
							'value': 'gHotel Berna has 116 rooms. In the main building there are 100 rooms of all types, the Reception Desk, the Breakfast Room and the American Bar. The Berna Tower is located opposite; this is a quiet and secluded annex with 16 rooms, with carpeting, double beds and the utmost privacy.'
						}],
						'heading': [{
							'style': null,
							'value': 'Our hotel'
						}, {
							'style': null,
							'value': 'gOur hotel'
						}]
					}, {
						'text': [{
							'style': null,
							'value': 'First of all, our welcoming smile. However, the smiles after will be many: when we welcome you and during your stay. And there will also be your smiles when you discover our qualities, the cuddles that we have reserved for you and the surprises prepared just for you. From the breakfast to the cleaning, from the choice of the room temperature to the silence. In the heart of Milano you will discover the atmosphere of one of your second homes.'
						}],
						'heading': [{
							'style': null,
							'value': 'Hospitality'
						}]
					}],
					'bookingUrl': 'https://booking.slh.com/en-GB/Room/Availability/?HotelCode=HUTLLTE&StartDate=<StartDate>&NoOfNights=<NoOfNights>&NoOfAdults=<NoOfAdults>'
				}],
				'hotelGroup': {'hotelGroupId': hgId},
				'publishing': {
					'endDate': '2015-11-06T00:00:00+00:00',
					'published': true,
					'startDate': '2014-11-01T00:00:00+00:00'
				},
				'categoryIds': ['reg-eur', 'reg-eur-est'],
				'coordinates': {'lat': 90, 'long': -179.9999999}
			};

			return getNewObject(data);
		};
		//create function to get valid hotel group data sample
		getValidContextData = function(){
			var hgId = 'test-hotel-group-id';

			var data = {
				hotelGroupId: hgId,
				categories: [{
					categoryId: 'reg-eur',
					name: 'Europe',
					localized: {
						'en_GB': {'name': 'Europe'}
					}
				}, {
					categoryId: 'reg-eur-est',
					name: 'Estonia',
					localized: {
						'en_GB': {'name': 'Estonia'}
					}
				}],
				defaultLanguage: 'en_GB',

			};

			return getNewObject(data);
		};
		hotelData = getValidHotelData();
		context = getValidContextData();
		done();
	});

	lab.afterEach(function(done){
		dao.checkIfExists = _checkIfExists;
		hotelData = getValidHotelData();
		context = getValidContextData();
		done();
	});

	lab.test('should given error if some root key is missing', function(done) {
		var promises = [];
		var rKeys = ['meta', 'hotelId', 'version', 'localized', 'hotelGroup', 'publishing', 'categoryIds', 'coordinates'];
		for(var i in rKeys){
			var rKey = rKeys[i];

			var data = getValidHotelData();
			var p = q.defer();

			promises.push(p.promise);
			delete data[rKey];

			validateHotelData(
				data,
				context,
				p,
				'hv-key-miss',
				rKey
			);
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if some key type is invalid', function(done) {
		var promises = [];
		var rKeys = ['meta', 'hotelId', 'version', 'localized', 'hotelGroup', 'publishing', 'categoryIds', 'coordinates'];
		for(var i in rKeys){
			var rKey = rKeys[i];

			var data = getValidHotelData();
			var p = q.defer();

			promises.push(p.promise);
			data[rKey] = 125;

			validateHotelData(
				data,
				context,
				p,
				'hv-inv-type',
				rKey
			);
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if hotelId has empty string', function(done) {
		hotelData.hotelId = '';
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-inv-val');
			done();
		});
	});

	lab.test('should given error if hotelId not exists in database', function(done) {
		hotelData.hotelId = '';
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-inv-val');
			done();
		});
	});

	lab.test('should given error if hotelId exists in database', function(done) {
		dao.checkIfExists = function(hgId, cb){
			cb(null, true);
		};
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-hId-exists');
			done();
		});
	});

	lab.test('should given error if version.number not exists', function(done) {
		delete hotelData.version.number;
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-key-miss');
			done();
		});
	});

	lab.test('should given error if version.number is not number', function(done) {
		hotelData.version.number = {};
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-inv-type');
			done();
		});
	});

	lab.test('should given error if hotelGroup.hotelGroupId not exists', function(done) {
		delete hotelData.hotelGroup.hotelGroupId;
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-key-miss');
			done();
		});
	});

	lab.test('should given error if hotelGroup.hotelGroupId has empty string', function(done) {
		hotelData.hotelGroup.hotelGroupId = '';
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-inv-val-type');
			done();
		});
	});

	lab.test('should given error if hotelGroup.hotelGroupId not inherit hotelGroupId', function(done) {
		context.hotelGroupId = 'test-hotelGroup-id';
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-hgId-exists');
			done();
		}, context);
	});

	lab.test('should given error if coordinates.lat not exists', function(done) {
		delete hotelData.coordinates.lat;
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-key-miss');
			done();
		}, context);
	});

	lab.test('should given error if coordinates.lat is not number', function(done) {
		hotelData.coordinates.lat = '';
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-inv-type');
			done();
		}, context);
	});

	lab.test('should given error if coordinates.lat has value more then range', function(done) {
		hotelData.coordinates.lat = 100;
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-inv-val');
			done();
		}, context);
	});

	lab.test('should given error if coordinates.lat has value less then range', function(done) {
		hotelData.coordinates.lat = -100;
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-inv-val');
			done();
		}, context);
	});
	lab.test('should given error if coordinates.long is not number', function(done) {
		hotelData.coordinates.long = {};
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-inv-type');
			done();
		}, context);
	});

	lab.test('should given error if coordinates.long has less value range', function(done) {
		hotelData.coordinates.long = -200;
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-inv-val');
			done();
		}, context);
	});

	lab.test('should given error if coordinates.long has large value range', function(done) {
		hotelData.coordinates.long = 200;
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-inv-val');
			done();
		}, context);
	});


	lab.test('should given error if categoryIds has invalid values', function(done) {
		var promises = [];
		for(var i in hotelData.categoryIds){
			var data = getValidHotelData();
			var p = q.defer();

			promises.push(p.promise);
			data.categoryIds[i]= ['test-ex-id'];

			validateHotelData(
				data,
				context,
				p,
				'hv-inv-val',
				'categoryIds[' + i + ']'
			);
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x] not contain language', function(done) {
		var checks = removeKeys(hotelData.localized, 'language');
		var promises = [];
		var i = 0;

		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			var data = getValidHotelData();
			data.localized[i] = checks[i];
			validateHotelData(
				data,
				context,
				p,
				'hv-key-miss',
				'localized[' + i + '].language'
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].language is empty string', function(done) {
		var checks = getKeys(hotelData.localized, 'language', '');
		var promises = [];
		var i = 0;

		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			var data = getValidHotelData();
			data.localized[i] = checks[i];
			validateHotelData(
				data,
				context,
				p,
				'hv-key-empty',
				'localized[' + i + '].language'
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].language has invalid value', function(done) {
		var checks = getKeys(hotelData.localized, 'language', 'test-lang');
		var promises = [];
		var i = 0;

		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			var data = getValidHotelData();
			data.localized[i] = checks[i];
			validateHotelData(
				data,
				context,
				p,
				'hv-inv-lang',
				'localized[' + i + '].language'
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].language not contain default language', function(done) {
		hotelData.localized = getKeys(hotelData.localized, 'language', 'tt_TT');

		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-nodef-lang');
			testingTools.code.expect(err.data.debuggingData.key, JSON.stringify(err)).to.equal('hasDefaultLanguage');

			done();
		}, context);

	});

	lab.test('should given error if optional key has empty string', function(done) {
		var promises = [];
		var oKeys = ['vhSiteKey', 'gaTrackingId'];
		for(var i in oKeys){
			var oKey = oKeys[i];
			var data = getValidHotelData();
			var p = q.defer();

			promises.push(p.promise);
			data[oKey] = '';

			validateHotelData(
				data,
				context,
				p,
				'hv-key-empty',
				oKey
			);
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should work if optional key is missing', function(done) {
		var promises = [];
		var oKeys = ['vhSiteKey', 'gaTrackingId'];
		for(var i in oKeys){
			var oKey = oKeys[i];
			var data = getValidHotelData();
			var p = q.defer();

			promises.push(p.promise);
			delete data[oKey];

			validateSuccess(
				data,
				p,
				context
			);
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should work if localization[x].bookingUrl is missing', function(done) {

		var checks = removeKeys(hotelData.localized, 'bookingUrl');
		var promises = [];
		var i = 0;

		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			var data = getValidHotelData();
			data.localized[i] = checks[i];
			validateSuccess(
				data,
				p,
				context
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localization[x].bookingUrl is not url', function(done) {

		var checks = getKeys(hotelData.localized, 'bookingUrl', 44);
		var promises = [];
		var i = 0;

		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			var data = getValidHotelData();
			data.localized[i] = checks[i];
			validateHotelData(
				data,
				context,
				p,
				'hv-inv-url',
				'localized[' + i + '].bookingUrl'
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localization[x].bookingUrl is not contain booking tags', function(done) {
		var promises = [];
		for(var i in hotelData.localized){
			var p = q.defer();
			promises.push(p.promise);
			var tags = ['<StartDate>', '<NoOfNights>', '<NoOfAdults>'];
			for(var t in tags) {
				var tag = tags[t];
				var data = getValidHotelData();
				var bUrl = data.localized[i].bookingUrl;
				bUrl = bUrl.replace(tag, '<Test>');
				data.localized[i].bookingUrl = bUrl;
				validateHotelData(
					data,
					context,
					p,
					'hv-inv-val',
					'localized[' + i + '].bookingUrl'
				);
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if default and localized galleries not exists', function(done) {

		var checks = removeKeys(hotelData.localized, 'gallery');
		var promises = [];
		var i = 0;

		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			var data = getValidHotelData();
			delete data.gallery;
			data.localized[i] = checks[i];
			validateHotelData(
				data,
				context,
				p,
				'hv-key-miss'
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should work if localized gallery not exists', function(done) {

		var checks = removeKeys(getValidHotelData().localized, 'gallery');
		var promises = [];
		var i = 0;

		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			var data = getValidHotelData();
			data.localized[i] = checks[i];
			validateSuccess(
				data,
				p,
				context
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should work if default gallery not exists', function(done) {
		delete hotelData.gallery;
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		}, context);
	});

	lab.test('should given error if default gallery exists and is not array', function(done) {
		hotelData.gallery = 22;
		validation.validateHotelData(hotelData, validationType, function(err){
			testingTools.expectError(err, 'hv-inv-type');
			done();
		}, context);
	});

	lab.test('should given error if localized gallery exists and is not array', function(done) {

		var checks = getKeys(hotelData.localized, 'gallery', {});
		var promises = [];
		var i = 0;

		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			hotelData = getValidHotelData();
			hotelData.localized[i] = checks[i];
			validateHotelData(
				hotelData,
				context,
				p,
				'hv-inv-type'
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if image array in localized gallery are empty', function(done) {

		var promises = [];
		for(var k in hotelData.localized) {
			var checks = getKeys(hotelData.localized[k].gallery, 'source', {});
			var i = 0;

			while (checks[i] !== undefined) {
				var p = q.defer();
				promises.push(p.promise);
				hotelData = getValidHotelData();
				hotelData.localized[k].gallery[i] = checks[i];
				validateHotelData(
					hotelData,
					context,
					p,
					'mv-key-miss'
				);
				i++;
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if image array in root gallery are empty', function(done) {

		var promises = [];
		var checks = getKeys(hotelData.gallery, 'source', {});
		var i = 0;

		while (checks[i] !== undefined) {
			var p = q.defer();
			promises.push(p.promise);
			hotelData = getValidHotelData();
			hotelData.gallery[i] = checks[i];
			validateHotelData(
				hotelData,
				context,
				p,
				'mv-key-miss'
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized key name is missing', function(done) {
		var checks = removeKeys(hotelData.localized, 'name');
		var promises = [];
		var i = 0;

		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			hotelData = getValidHotelData();
			hotelData.localized[i] = checks[i];
			validateHotelData(
				hotelData,
				context,
				p,
				'hv-key-miss'
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized key name has invalid type', function(done) {
		var checks = getKeys(hotelData.localized, 'name', {});
		var promises = [];
		var i = 0;

		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			hotelData = getValidHotelData();
			hotelData.localized[i] = checks[i];
			validateHotelData(
				hotelData,
				context,
				p,
				'hv-inv-type'
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].contacts.primary not exists', function(done) {
		var checks = getKeys(hotelData.localized, 'contacts', {});
		var promises = [];
		var i = 0;

		while( checks[i] !== undefined){
			var p = q.defer();
			promises.push(p.promise);
			var data = getValidHotelData();
			data.localized[i] = checks[i];
			validateHotelData(
				data,
				context,
				p,
				'hv-key-miss',
				'localized[' + i + '].contacts.primary'
			);
			i++;
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].contacts.primary is not object', function(done) {

		var promises = [];
		for(var k in hotelData.localized) {
			var checks = getKeys(hotelData.localized[k].contacts, 'primary', 67);

			var i = 0;

			while (checks[i] !== undefined) {
				var p = q.defer();
				promises.push(p.promise);
				var data = getValidHotelData();
				data.localized[k].contacts = checks[i];
				validateHotelData(
					data,
					context,
					p,
					'hv-inv-type',
					'localized[' + k + '].contacts.primary'
				);
				i++;
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].contacts.primary[x] not exists', function(done) {
		var promises = [];
		for(var k in hotelData.localized) {
			for(var ck in hotelData.localized[k].contacts.primary){
				var p = q.defer();
				promises.push(p.promise);
				var data = getValidHotelData();
				delete data.localized[k].contacts.primary[ck];
				validateHotelData(
					data,
					context,
					p,
					'hv-key-miss',
					'localized[' + k + '].contacts.primary.' + ck
				);
			}

		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should work if localized[x].contacts.primary[x] is null', function(done) {
		var promises = [];
		for(var k in hotelData.localized) {
			for(var ck in hotelData.localized[k].contacts.primary){
				var p = q.defer();
				promises.push(p.promise);
				hotelData = getValidHotelData();
				hotelData.localized[k].contacts.primary[ck] = null;
				validateSuccess(hotelData, p, context);
			}

		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].contacts.primary.email has invalid type', function(done) {
		var promises = [];
		for(var k in hotelData.localized) {

			var p = q.defer();
			promises.push(p.promise);
			hotelData = getValidHotelData();
			hotelData.localized[k].contacts.primary.email = {};
			validateHotelData(
				hotelData,
				context,
				p,
				'hv-non-str',
				'localized[' + k + '].contacts.primary.email'
			);
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].contacts.primary.email is empty string', function(done) {
		var promises = [];
		for(var k in hotelData.localized) {

			var p = q.defer();
			promises.push(p.promise);
			hotelData = getValidHotelData();
			hotelData.localized[k].contacts.primary.email = '';
			validateHotelData(
				hotelData,
				context,
				p,
				'hv-key-empty',
				'localized[' + k + '].contacts.primary.email'
			);
		}
		q.all(promises).done(function(){
			done();
		});

	});

	lab.test('should given error if localized[x].contacts.primary.email is not contain `@`', function(done) {
		var promises = [];
		for(var k in hotelData.localized) {

			var p = q.defer();
			promises.push(p.promise);
			hotelData = getValidHotelData();
			hotelData.localized[k].contacts.primary.email = 'test-email-value';
			validateHotelData(
				hotelData,
				context,
				p,
				'hv-inv-email',
				'localized[' + k + '].contacts.primary.email'
			);
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].contacts.primary.website is not equal to url', function(done) {
		var promises = [];
		for(var k in hotelData.localized) {

			var p = q.defer();
			promises.push(p.promise);
			hotelData = getValidHotelData();
			hotelData.localized[k].contacts.primary.website = 'test-url-value';
			validateHotelData(
				hotelData,
				context,
				p,
				'hv-inv-url',
				'localized[' + k + '].contacts.primary.website'
			);
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should work if localized[x].description[x] is empty array', function(done) {
		var promises = [];

		var checks = getKeys(hotelData.localized, 'description', []);
		var i = 0;

		while (checks[i] !== undefined) {
			var p = q.defer();
			promises.push(p.promise);
			hotelData = getValidHotelData();
			hotelData.localized[i] = checks[i];
			validateSuccess(hotelData, p, context);
			i++;
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].description[x] is not equal to object', function(done) {
		var promises = [];

		for(var lk in hotelData.localized) {
			for(var dk in hotelData.localized[lk].description) {
				var p = q.defer();
				promises.push(p.promise);
				var data = getValidHotelData();
				data.localized[lk].description[dk] = 22;
				validateHotelData(
					data,
					context,
					p,
					'hv-inv-type',
					'localized[' + lk + '].description[' + dk+ ']'
				);

			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].description[x][heading|text] is not exists', function(done) {
		var promises = [];

		for(var lk in hotelData.localized) {
			for(var dk in hotelData.localized[lk].description) {
				for(var dkn in hotelData.localized[lk].description[dk]) {
					var p = q.defer();
					promises.push(p.promise);
					var data = getValidHotelData();
					delete data.localized[lk].description[dk][dkn];
					validateHotelData(
						data,
						context,
						p,
						'hv-key-miss',
						'localized[' + lk + '].description[' + dk + '].' + dkn
					);
				}
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].description[x][heading|text] has invalid type', function(done) {
		var promises = [];
		for(var lk in hotelData.localized) {
			for(var dk in hotelData.localized[lk].description) {
				for(var dkn in hotelData.localized[lk].description[dk]) {
					var p = q.defer();
					promises.push(p.promise);
					var data = getValidHotelData();
					data.localized[lk].description[dk][dkn] = 105;
					validateHotelData(
						data,
						context,
						p,
						'hv-inv-type',
						'localized[' + lk + '].description[' + dk + '].' + dkn
					);
				}
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].description[x][heading|text][x] has invalid type', function(done) {
		var promises = [];
		for(var i in hotelData.localized) {
			for(var x in hotelData.localized[i].description) {
				for(var y in hotelData.localized[i].description[x]) {
					for(var z in hotelData.localized[i].description[x][y]) {
						var p = q.defer();
						promises.push(p.promise);
						hotelData = getValidHotelData();
						hotelData.localized[i].description[x][y][z] = 'test-value';
						validateHotelData(
							hotelData,
							context,
							p,
							'hv-inv-type',
							'localized[' + i + '].description[' + x + '].' + y + '[' + z + ']'
						);
					}
				}
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].description[x].[heading|text][x].value is not exists', function(done) {
		var promises = [];
		for(var i in hotelData.localized) {
			for(var x in hotelData.localized[i].description) {
				for(var y in hotelData.localized[i].description[x]) {
					for(var z in hotelData.localized[i].description[x][y]) {
						for(var w in hotelData.localized[i].description[x][y][z]) {
							var p = q.defer();
							promises.push(p.promise);
							hotelData = getValidHotelData();
							delete hotelData.localized[i].description[x][y][z][w];
							validateHotelData(
								hotelData,
								context,
								p,
								'hv-key-miss',
								'localized[' + i + '].description[' + x + '].' + y + '[' + z +'].' + w
							);
						}
					}
				}
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].description[x].[heading|text][x].value has invalid type', function(done) {
		var promises = [];
		for(var lk in hotelData.localized) {
			for(var dk in hotelData.localized[lk].description) {
				for(var dkn in hotelData.localized[lk].description[dk]) {
					var checks = getKeys(hotelData.localized[lk].description[dk][dkn], 'value', 33);
					var i = 0;
					while (checks[i] !== undefined) {
						var p = q.defer();
						promises.push(p.promise);
						hotelData = getValidHotelData();
						hotelData.localized[lk].description[dk][dkn][i] = checks[i];
						validateHotelData(
							hotelData,
							context,
							p,
							'hv-inv-type',
							'localized[' + lk + '].description[' + dk + '].' + dkn + '[' + i +'].value'
						);
						i++;
					}
				}
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if localized[x].description[x].[heading|text][x].style has invalid type', function(done) {
		var promises = [];
		for(var lk in hotelData.localized) {
			for(var dk in hotelData.localized[lk].description) {
				for(var dkn in hotelData.localized[lk].description[dk]) {
					var checks = getKeys(hotelData.localized[lk].description[dk][dkn], 'style', {});
					var i = 0;
					while (checks[i] !== undefined) {
						var p = q.defer();
						promises.push(p.promise);
						hotelData = getValidHotelData();
						hotelData.localized[lk].description[dk][dkn][i] = checks[i];
						validateHotelData(
							hotelData,
							context,
							p,
							'hv-inv-type',
							'localized[' + lk + '].description[' + dk + '].' + dkn + '[' + i +'].style'
						);
						i++;
					}
				}
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should work if location is empty object', function(done) {
		var promises = [];

		var checks = getKeys(hotelData.localized, 'location', {});
		var i = 0;
		while (checks[i] !== undefined) {
			var p = q.defer();
			promises.push(p.promise);
			hotelData = getValidHotelData();
			hotelData.localized[i] = checks[i];
			validateSuccess(hotelData, p, context);
			i++;
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if location key is not string', function(done) {
		var promises = [];
		for(var lk in hotelData.localized) {
			for(var lkl in hotelData.localized[lk].location){
				var p = q.defer();
				promises.push(p.promise);
				hotelData = getValidHotelData();
				hotelData.localized[lk].location[lkl] = 33;
				validateHotelData(
					hotelData,
					context,
					p,
					'hv-inv-type',
					'localized[' + lk + '].location.' + lkl
				);
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if location key is empty string', function(done) {
		var promises = [];
		for(var lk in hotelData.localized) {
			for(var lkl in hotelData.localized[lk].location){
				var p = q.defer();
				promises.push(p.promise);
				hotelData = getValidHotelData();
				hotelData.localized[lk].location[lkl] = '';
				validateHotelData(
					hotelData,
					context,
					p,
					'hv-key-empty',
					'localized[' + lk + '].location.' + lkl
				);
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should given error if name key is empty string', function(done) {
		var promises = [];

		var checks = getKeys(hotelData.localized, 'name', '');
		var i = 0;
		while (checks[i] !== undefined) {
			var p = q.defer();
			promises.push(p.promise);
			hotelData = getValidHotelData();
			hotelData.localized[i] = checks[i];
			validateHotelData(
				hotelData,
				context,
				p,
				'hv-key-empty',
				'localized[' + i + '].name'
			);
			i++;
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should work', function(done) {
		validation.validateHotelData(hotelData, validationType, function (err) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		}, context);
	});














});
