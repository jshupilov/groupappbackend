'use strict';
var errors = require('../utils/errors');
var validator = require('validator');
var langUtils = require('../utils/langUtils');
var validationUtils = require('../utils/validationUtils');
var dao = require('./dao');
var check = require('check-types');
var mediaVal = require('../media/validation');

errors.defineError('hv-non-str', 'System error', 'Field must be provided as string', 'Hotels.Validation', 400);
errors.defineError('hv-non-bool', 'System error', 'Field must be provided as boolean', 'Hotels.Validation', 400);
errors.defineError('hv-key-miss', 'System error', 'A key is missing from the data set', 'Hotels.Validation', 400);
errors.defineError('hv-key-empty', 'System error', 'Key cannot be empty', 'Hotels.Validation', 400);
errors.defineError('hv-inv-type', 'System error', 'A value has a wrong data type', 'Hotels.Validation', 400);
errors.defineError('hv-inv-val', 'System error', 'A value has an invalid value', 'Hotels.Validation', 400);
errors.defineError('hv-inv-val-type', 'System error', 'Invalid validation type provided', 'Hotels.Validation', 500);
errors.defineError('hv-inv-email', 'System error', 'Hotel email is invalid.', 'Hotels.Validation', 400);
errors.defineError('hv-inv-url', 'System error', 'Hotel web site url is invalid.', 'Hotels.Validation', 400);
errors.defineError('hv-inv-lang', 'System error', 'Invalid language code for localization given', 'Hotels.Validation', 400);
errors.defineError('hv-nodef-lang', 'System error', 'Hotel localization default language is miss', 'Hotels.Validation', 400);
errors.defineError('hv-hId-exists', 'System error', 'Hotel with this id already exists', 'Hotels.Validation', 400);
errors.defineError('hv-hgId-exists', 'System error', 'Hotel Group with this id already exists', 'Hotels.Validation', 400);

/**
 * Validate that all root level keys exist
 * @param data
 * @param validationType
 * @param callback
 * @returns {*}
 */
function validateRootKeys(data, validationType, callback){

	var rootKeys = ['meta',	'hotelId', 'version', 'localized', 'hotelGroup', 'publishing', 'categoryIds', 'coordinates'];

	validationUtils.validateHasKeys(rootKeys, data, function(missingKey){
		if(missingKey){
			return callback(errors.newError('hv-key-miss', {key: missingKey, providedBy: 'validateRootKeys'}));
		}
		//must be the same order as in rootKeys
		var types = ['object', 'string', 'object',	'array', 'object', 'object', 'array', 'object'];
		validationUtils.validateTypeOfKeys(rootKeys, types, data, '', 'hv-inv-type', function(err){
			if (err) {
				return callback(errors.newError(err.code, err.details));
			}
			return callback(null);
		});
	});

}
/**
 * Validate that all optional keys
 * @param data
 * @param validationType
 * @param callback
 * @returns {*}
 */
function validateOptionalKeys(data, validationType, callback){

	var optionalKeys = ['vhSiteKey', 'gaTrackingId'];
	validationUtils.validateArray(
		optionalKeys,
		function(key, i, cb1){
			if (data.hasOwnProperty(key)) {
				if (!check.unemptyString(data[key])) {
					return cb1(errors.newError('hv-key-empty', {key: key, value: data[key] }));
				}	else{
					return cb1(null);
				}
			}	else{
				return cb1(null);
			}
		},
		function(err){
			return callback(err);
		}
	);
}
/**
 * Validate bookingUrl key
 * @param data
 * @param validationType
 * @param callback
 * @returns {*}
 */
function validateBookingUrl(data, validationType, callback){
	validationUtils.validateArray(data.localized, function(item, lockIndex, cb1){
		//might be the same order as array with keys
		if(item.hasOwnProperty('bookingUrl')){
			var bUrl = item.bookingUrl;
			if (validator.isURL(bUrl)){

				var tags = [/<StartDate(Iso)?>/, /<NoOfNights>/, /<NoOfAdults>/];
				validationUtils.validateArray(
					tags,
					function(tag, i, cb2){
						if( !bUrl.match(tag) ){
							return cb2(errors.newError('hv-inv-val', {
								key: 'localized[' + lockIndex +'].bookingUrl',
								givenUrl: bUrl
							}));
						} else {
							return cb2(null);
						}
					},
					function(err){
						return cb1(err);
					}
				);
			} else {
				return cb1(errors.newError('hv-inv-url', {
					key: 'localized[' + lockIndex +'].bookingUrl',
					givenUrl: bUrl
				}));
			}
		} else {
			return cb1(null);
		}
	}, function(err){
		callback(err);
	});
}

/**
 * Validate that all root level keys exist
 * @param data
 * @param validationType
 * @param callback
 * @returns {*}
 */
function validateDefaultLanguage (data, validationType, callback, context){
	var hasDefaultLanguage = false;
	for(var lk in data.localized){
		var localizedItem = data.localized[lk];
		if (!localizedItem.hasOwnProperty('language')) {
			return callback(errors.newError('hv-key-miss', {key: 'localized[' + lk + '].language', providedBy: 'validateDefaultLanguage'}));
		} else {
			var lang = localizedItem.language;
			//validate language key
			if (!check.unemptyString(lang)) {
				return callback(errors.newError('hv-key-empty', {key: 'localized[' + lk + '].language'}));
			} else if (!langUtils.isValidLanguageCode(lang)) {
				return callback(errors.newError('hv-inv-lang', {key: 'localized[' + lk + '].language'}));
			} else if (lang === context.hotelGroup.defaultLanguage) {
				hasDefaultLanguage = true;
			}
		}
	}

	if (!hasDefaultLanguage) {
		return callback(errors.newError('hv-nodef-lang', {
			key: 'hasDefaultLanguage',
			value: hasDefaultLanguage,
			defaultLang: context.hotelGroup.defaultLanguage
		}));
	}
	return callback(null);
}
/**
 * Validate categoryIds
 * @param data
 * @param validationType
 * @param callback
 */
function validateCategoryIds(data, validationType, callback, context){

	var categoryIds = context.hotelGroup.categories.map(function(x){
		return x.categoryId;
	});
	for (var i = 0; i < data.categoryIds.length; i++) {
		var id = data.categoryIds[i];
		var categoryIndex = categoryIds.indexOf(id);
		if (categoryIndex < 0) {
			return callback(errors.newError('hv-inv-val', {
				key: 'categoryIds[' + i + ']',
				value: id
			}));
		}
	}
	return callback(null);
}


/**
 * Validate gallery
 * @param data
 * @param validationType
 * @param callback
 */
function validateGallery(data, validationType, callback){
	var images = [];
	if(data.hasOwnProperty('gallery')){
		if(!Array.isArray(data.gallery)){

			return callback(errors.newError('hv-inv-type', {
				key: 'gallery',
				type: typeof data.gallery,
				expectedType: 'array'
			}));
		}
		for(var gi in data.gallery ){
			images.push([data.gallery[gi], 'gallery[' + gi + ']']);
		}
	}
	validationUtils.validateArray(
		data.localized,
		function(item, lockIndex, cb1) {

			validationUtils.validateWithArrayOfFunctionsWithCallbacks(
				[
					function(hotelData, locData, images, cb) {
						validationUtils.validateLocalizedOrDefaultKeys(['gallery'], data, item, function (errKey) {
							if (errKey) {
								return cb(errors.newError('hv-key-miss', {key: errKey, providedBy: 'validateGallery'}));
							}
							cb(null);
						});
					},
					function(hotelData, locData, images, cb) {
						if(locData.hasOwnProperty('gallery')){

							if(!Array.isArray(locData.gallery)){
								return cb(errors.newError('hv-inv-type', {
									key: 'localized[' + lockIndex + '].gallery',
									type: typeof locData.gallery,
									expectedType: 'array'
								}));
							}
							for(var lgi in locData.gallery ){
								images.push([locData.gallery[lgi], 'localized[' + lockIndex + '].gallery[' + lgi + ']']);
							}
						}

						var ni = 0;
						function imgVal() {
							if( images[ni] === undefined ) {
								return cb(null);
							}
							mediaVal.validateImageObject(images[ni][0], 'hotelImage', function (err) {
								if (err) {

									err.data.debuggingData.key = images[1] + '.' + err.data.debuggingData.key;
									return cb(err);
								}else{
									ni++;
									imgVal();
								}


							}, {
								hotelGroupId: hotelData.hotelGroup.hotelGroupId,
								hotelId: hotelData.hotelId
							});
						}
						imgVal();
					}
				],
				[data, item, images],
				function(err){
					cb1(err);
				}
			);

		},
		function (err){
			return callback(err);
		}
	);
}



/**
 * Validate localized
 * @param data
 * @param validationType
 * @param callback
 */
function validateLocalized(data, validationType, callback){
	function validateDescriptionItems(descriptionItemsData, localizedItemErrKey, cbDescriptionItems) {
		validationUtils.validateArray(descriptionItemsData, function(descriptionItems, descriptionItemKey, cb1){
			if (!Array.isArray(descriptionItems)) {
				return cb1(errors.newError('hv-inv-type', {
					key: localizedItemErrKey + descriptionItemKey,
					type: typeof descriptionItems,
					expectedType: 'array'
				}));
			}

			validationUtils.validateArray(descriptionItems, function(descItemKeys, descItemKeyIndex, cb2){
				var descriptionItemErrKey = localizedItemErrKey + descriptionItemKey + '[' + descItemKeyIndex + ']';


				if (!check.object(descItemKeys)) {
					return cb2(errors.newError('hv-inv-type', {
						key: descriptionItemErrKey,
						type: typeof descItemKeys,
						expectedType: 'array'
					}));
				}
				validationUtils.validateHasKeys(['style', 'value'], descItemKeys, function (missingKey) {
					descriptionItemErrKey = descriptionItemErrKey + '.';
					if (missingKey) {
						return cb2(errors.newError('hv-key-miss', {
							key: descriptionItemErrKey + missingKey,
							providedBy: 'validateLocalized'
						}));
					}
					if (!check.null(descItemKeys.style)) {
						return cb2(errors.newError('hv-inv-type', {
							key: descriptionItemErrKey + 'style',
							expectedType: 'null'
						}));
					}
					if (!check.unemptyString(descItemKeys.value)) {
						return cb2(errors.newError('hv-inv-type', {
							key: descriptionItemErrKey + 'value',
							expectedType: 'string'
						}));
					} else {
						return cb2(null);
					}
				});
			}, function(err){
				cb1(err);
			});

		}, function(err){
			cbDescriptionItems(err);
		});
	};

	function validateDescription(descriptionLocalizedItem, localizedErrKey, cbDescription){

		validationUtils.validateArray(descriptionLocalizedItem, function(dItem, dk, cb1){
			var localizedItemErrKey = localizedErrKey + '[' + dk + ']';

			if (!check.object(dItem)) {

				return cb1(errors.newError('hv-inv-type', {
					key: localizedItemErrKey,
					type: typeof dItem,
					expectedType: 'array'
				}));
			}
			validationUtils.validateHasKeys(['heading', 'text'], dItem, function (errKey) {
				localizedItemErrKey = localizedItemErrKey + '.';
				if (errKey) {
					return cb1(errors.newError('hv-key-miss', {
						key: localizedItemErrKey + errKey,
						providedBy: 'validateDescription'
					}));
				}
				validateDescriptionItems(dItem, localizedItemErrKey, function (err) {
					return cb1(err);
				});
			});
		}, function(err){
			return cbDescription(err);
		});
	};

	var validateLocation = function(locationData, localizedErrKey, cbLocation){
		for(var locKey in locationData){
			var locItem = locationData[locKey];
			var localizedItemErrKey = localizedErrKey + '.';
			if (!check.string(locItem)) {
				return cbLocation(errors.newError('hv-inv-type', {
					key: localizedItemErrKey + locKey,
					givenType: typeof locItem
				}));
			} else if (!check.unemptyString(locItem)) {
				return cbLocation(errors.newError('hv-key-empty', {
					key: localizedItemErrKey + locKey,
					givenType: typeof locItem
				}));
			}
		}
		return cbLocation(null);
	};
	var validateContacts = function(localizedItem, localizedErrKey, cbContacts){
		if (localizedItem.primary) {
			if (check.object(localizedItem.primary)) {
				validationUtils.validateHasKeys(['phone', 'email', 'website'], localizedItem.primary, function (errKey) {
					var localizedItemErrKey = localizedErrKey + '.primary.';
					if (errKey) {
						return cbContacts(errors.newError('hv-key-miss', {
							key: localizedItemErrKey + errKey,
							providedBy: 'validateContacts'
						}));
					} else {
						for (var primaryItem in localizedItem.primary) {
							var locationPrimaryItem = localizedItem.primary[primaryItem];

							errKey = localizedItemErrKey + primaryItem;

							if (!check.string(locationPrimaryItem)) {
								if (!check.null(locationPrimaryItem)) {
									return cbContacts(errors.newError('hv-non-str', {
										key: errKey,
										expectedType: 'string OR null',
										type: typeof localizedItem
									}));
								}
							} else if (!check.unemptyString(locationPrimaryItem)) {
								return cbContacts(errors.newError('hv-key-empty', {
									key: errKey
								}));
							} else {
								if (primaryItem === 'email') {
									if (locationPrimaryItem.indexOf('@') < 1) {
										return cbContacts(errors.newError('hv-inv-email', {
											key: errKey,
											givenEmail: locationPrimaryItem
										}));
									}
								} else if (primaryItem === 'website') {
									if (!validator.isURL(locationPrimaryItem)) {
										return cbContacts(errors.newError('hv-inv-url', {
											key: errKey,
											givenUrl: locationPrimaryItem
										}));
									}
								}
							}
						}
						return cbContacts(null);
					}

				});

			} else {
				return cbContacts(errors.newError('hv-inv-type', {
					key: localizedErrKey + '.primary',
					value: localizedItem,
					type: typeof localizedItem
				}));
			}
		} else {
			return cbContacts(errors.newError('hv-key-miss', {
				key: localizedErrKey + '.primary',
				value: localizedItem,
				providedBy: 'validateContacts'
			}));
		}
	};
	var validateLocalizedItems = function(locData, keys, hotelData, lockIndex, cbLocalizedItems){
		validationUtils.validateArray(keys, function(key, ai, cb1){
			var localizedItem = locData[key];
			var localizedErrKey = 'localized[' + lockIndex + '].' + key;
			if (key === 'contacts') {
				validateContacts(localizedItem, localizedErrKey, function(err){
					return cb1(err);
				});
			}
			else if (key === 'description') {
				if (localizedItem.length > 0) {
					validateDescription(localizedItem, localizedErrKey, function(err){
						return cb1(err);
					});
				} else {
					return cb1(null);
				}
			} else if (key === 'location') {
				validateLocation(localizedItem, localizedErrKey, function(err){
					return cb1(err);
				});
			} else {
				if (!check.unemptyString(localizedItem)) {
					return cb1(errors.newError('hv-key-empty', {key: localizedErrKey}));
				} else {
					return cb1(null);
				}
			}

		}, function(err){
			return cbLocalizedItems(err);
		});

	};
	var validateLocalizedArray = function(hotelData, keys, types, cbLocalizedArray){
		validationUtils.validateArray(hotelData.localized, function(item, lockIndex, cb1){
			//might be the same order as array with keys
			validationUtils.validateHasKeys(keys, item, function(missingKey) {
				if (missingKey) {
					return cb1(errors.newError('hv-key-miss', {
						key: missingKey,
						providedBy: 'validateLocalizedArray'
					}));
				}
				validationUtils.validateTypeOfKeys(keys, types, item, 'localized[' + lockIndex + '].', 'hv-inv-type', function(err){
					if (err) {
						return cb1(errors.newError(err.code, err.details));
					}
					validateLocalizedItems(item, keys, hotelData, lockIndex, function(err){
						return cb1(err);
					});
				});
			});
		}, function(err){

			cbLocalizedArray(err);
		});

	};
	var localizedKeys = ['name', 'contacts', 'location', 'description'];
	var localizedKeysTypes = ['string', 'object', 'object', 'array'];
	validateLocalizedArray(data, localizedKeys, localizedKeysTypes, function(err) {
			return callback(err);
		}
	);


}

exports.validateLocalized = validateLocalized;

/**
 * Validate coordinates
 * @param data
 * @param validationType
 * @param callback
 */
function validateCoordinates(data, validationType, callback){

	validationUtils.validateHasKeys(['lat', 'long'], data.coordinates, function(errKey){
		if(errKey){
			return callback(errors.newError('hv-key-miss', {key: 'coordinates.' + errKey, providedBy: 'validateCoordinates'}));
		} else if(!check.number(data.coordinates.lat)) {
			return callback(errors.newError('hv-inv-type', {key: 'coordinates.lat', type: typeof data.coordinates.lat}));
		} else if(data.coordinates.lat > 90 || data.coordinates.lat < -90) {
			return callback(errors.newError('hv-inv-val', {key: 'coordinates.lat', value: data.coordinates.lat}));
		} else if(!check.number(data.coordinates.long)){
			return callback(errors.newError('hv-inv-type', {key: 'coordinates.long', type: typeof data.coordinates.long}));
		} else if(data.coordinates.long > 180 || data.coordinates.long < -180){
			return callback(errors.newError('hv-inv-val', {key: 'coordinates.long', value: data.coordinates.long}));
		}
		callback(null);
	});
}

/**
 * Validate the hotel group ID
 * @param data
 * @param validationType
 * @param callback
 */
function validateHotelGroup(data, validationType, callback, context){
	//validate with given functions until first fails or al pass
	validationUtils.validateWithArrayOfFunctionsWithCallbacks(
		[

			//if has all keys
			function(cb){
				validationUtils.validateHasKeys(['hotelGroupId'], data.hotelGroup, function(errKey){
					if(errKey){
						return cb(errors.newError('hv-key-miss', {key: 'hotelGroup.' + errKey, providedBy: 'validateHotelGroup'}));
					}
					cb(null);
				});
			},

			//validate hotelGroup hotelGroupId type and filling
			function(cb){
				if(!check.unemptyString(data.hotelGroup.hotelGroupId)){
					return cb(errors.newError('hv-inv-val-type', {key: 'hotelGroup.hotelGroupId'}));
				}
				cb(null);

			},

			//validate if hotelGroupId exists
			function(cb){
				if(data.hotelGroup.hotelGroupId !== context.hotelGroup.hotelGroupId){
					return cb(errors.newError('hv-hgId-exists', {
						key: 'hotelGroup.hotelGroupId',
						value: context.hotelGroup.hotelGroupId,
						givenValue: data.hotelGroup.hotelGroupId
					}));
				}
				cb(null);
			}
		],
		[],
		function(err){
			callback(err);
		}
	);
}

/**
 * Validate the hotel group ID
 * @param data
 * @param validationType
 * @param callback
 */
function validateVersion(data, validationType, callback){
	//validate with given functions until first fails or al pass
	validationUtils.validateWithArrayOfFunctionsWithCallbacks(
		[

			//if has all keys
			function(cb){
				validationUtils.validateHasKeys(['number'], data.version, function(errKey){
					if(errKey){
						return cb(errors.newError('hv-key-miss', {key: 'version.' + errKey, providedBy: 'validateVersion'}));
					}
					cb(null);
				});
			},

			//validate version number type
			function(cb){
				if(!check.number(data.version.number)){
					return cb(errors.newError('hv-inv-type', {key: 'version.number'}));
				}
				cb(null);

			}
		],
		[],
		function(err){
			callback(err);
		}
	);
}
/**
 * Validate the hotel group ID
 * @param data
 * @param validationType
 * @param callback
 */
function validateHotelId(data, validationType, callback){
	if( !check.unemptyString(data.hotelId) ){
		return callback(errors.newError('hv-inv-val', {key: 'hotelId'}));
	}
	dao.checkIfExists(data.hotelId, function(err, exists){
		if(exists){
			return callback(errors.newError('hv-hId-exists'));
		}
		callback(err);
	});


}



exports.validateHotelData = function(data, validationType, callback, hotelGroupData){

	var validationTypes = {
		full: {
			functions: [
				validateRootKeys,
				validateHotelId,
				validateVersion,
				validateHotelGroup,
				validateCoordinates,
				validateCategoryIds,
				validateDefaultLanguage,
				validateOptionalKeys,
				validateBookingUrl,
				validateGallery,
				validateLocalized
			],
			context: {
				hotelGroup: hotelGroupData
			}
		},
		update: {
			functions: [
				validateRootKeys,
				validateVersion,
				validateHotelGroup,
				validateCoordinates,
				validateCategoryIds,
				validateDefaultLanguage,
				validateOptionalKeys,
				validateBookingUrl,
				validateGallery,
				validateLocalized
			],
			context: {
				hotelGroup: hotelGroupData
			}
		}
	};

	validationTypes.insert = {
		functions: validationTypes.full.functions,
		context: {
			hotelGroup: hotelGroupData
		}
	};


	if(validationTypes.hasOwnProperty(validationType)){

		var checkIndex = 0;
		var valCaller = function(){
			var context = validationTypes[validationType].context;
			//call first check
			validationTypes[validationType].functions[checkIndex](data, validationType, function(err, res){

				if(err){
					return callback(err);
					//call next check
				}else if( validationTypes[validationType].functions[checkIndex+1] !== undefined ){
					checkIndex++;
					valCaller();
				}else{
					return callback(null, res);
				}
			}, context);
		};

		if( !check.object(data) ){
			return callback(errors.newError('hv-inv-type', {key: 'rootObject'}));
		}

		valCaller();

	}else{
		callback(errors.newError('hv-inv-val-type'));
	}

};
