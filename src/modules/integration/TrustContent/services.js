'use strict';
var errors = require('../../utils/errors');
var logging = require('../../utils/logging');
var hgServices = require('../../hotel_groups/services');
var hServices = require('../../hotels/services');
var fs = require('fs');
var xml2json = require('xml2json');
var media = require('../../media/services');
var q = require('q');
var versionDao = require('../../versioning/dao');
var htlValidation = require('../../hotels/validation');
var verValidation = require('../../versioning/validation');
var htlSrv = require('../../hotels/services');
var jobHandler = require('../../worker/jobHandler');
var scheduler = require('../../worker/scheduler');
var ftpClient = require('ftp');
var path = require('path');
var aws = require('aws-sdk');
var check = require('check-types');
var jq = require('jQuery');
var http = require('http');

/**
 * jQuery improvements
 */

jq.expr[':'].Contains = function(a, i, m) {
	return jq(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};

errors.defineError('int-tc-parse-error', 'Parsing failed', 'Error occurred while parsing XML file with hotel data', 'Integration');
errors.defineError('int-tc-map-error', 'Mapping failed', 'Error occurred while mapping hotel data', 'Integration');
errors.defineError('int-tc-imp-err', 'System error', 'Unable to import hotel', 'Integration');
errors.defineError('int-tc-ftp-err', 'System error', 'Error occurred communicating with FTP server', 'Integration');
errors.defineError('int-tc-ftp-get', 'System error', 'Error occurred fetching file from FTP server', 'Integration');
errors.defineError('int-s3-list-err', 'System error', 'Error occurred fetching file list from AWS S3', 'Integration');
errors.defineError('int-s3-upl-err', 'System error', 'Error occurred uploading file to AWS S3', 'Integration');
errors.defineError('int-s3-get-err', 'System error', 'Error occurred getting file from AWS S3', 'Integration');
errors.defineError('int-tc-blckl-err', 'Import failed', 'Import failed, Hotel is black listed', 'Integration');
errors.defineError('int-comm-err', 'Communication failure', 'Unable to make http request', 'Integration');
errors.defineError('int-img-err', 'Image object error', 'Unable to parse image object', 'Integration');
errors.defineError('int-inv-hg', 'Importing failure', 'Import process for this hotel group is not defined', 'Integration');
errors.defineError('int-tc-blckl-cnt-grt', 'Validate failed', 'Validate blacklist failed, Count of hotels greater than possible', 'Integration');

errors.defineError('int-web-js-pars', 'Import failed', 'Cannot parse json', 'Integration');
errors.defineError('int-web-get-f', 'Import failed', 'Cannot get webpage file', 'Integration');
errors.defineError('int-web-map-err', 'Import failed', 'Error occurred mapping html page to hotel dataset', 'Integration');

errors.defineError('bl-req-fail', 'HttpRequest Error', 'Failed to connet host', 'Integration');
errors.defineError('bl-update-fail', 'BlackList update failed', 'Missing list of livehotels in hotelGroup', 'Integration');
errors.defineError('bl-update-cnd', 'BlackList update cancelled', 'BlackList limit reached', 'Integration');
errors.defineError('bl-req-pars-err', 'BlackList update failed', 'Cannot parse JSON', 'Integration');

var p1 = q.defer();
var requestQueue = [p1.promise];
p1.resolve();

/**
 * Make http request
 * @param options {host, path, method, timeout}
 * @param callback
 */
exports.makeHttpRequest = function(options, callback) {

	var p = q.defer();
	requestQueue.push(p.promise);

	requestQueue[requestQueue.length - 2].finally(function() {
		var req = http.request(options, function(response) {
			var str = '';
			response.on('data', function(chunk) {
				str += chunk;
			});
			response.on('end', function() {
				p.resolve();
				return callback(null, str);
			});
		});
		req.on('error', function(e) {
			p.resolve();
			return callback(errors.newError('int-comm-err', {originalError: e, path: options.path}));
		});
		req.on('timeout', function() {
			p.resolve();
			req.abort();
		});
		req.setTimeout(options.timeout);
		req.end();
	});

};

exports.makeGetRequest = function(path, callback) {
	exports.makeHttpRequest({
		host: 'www.slh.com',
		path: path,
		method: 'GET',
		timeout: 80000
	}, callback);
};

function getImageUrls(hotelid, callback) {
	var g1 = hotelid.substr(hotelid.length - 1, hotelid.length);
	var g3 = hotelid.substr(hotelid.length - 3, hotelid.length);
	var g5 = hotelid.substr(hotelid.length - 5, hotelid.length);

	var url = 'http://images.slh.com/galleries/clients/140751/data/' + g1 + '/' + g3 + '/' + g5 + '/0000000000000' + hotelid + '/en_data.json?v=01.17&callback=getJSON&_1446470793537=';

	var options = {
		host: 'images.slh.com',
		path: url,
		method: 'GET',
		timeout: 50000
	};

	exports.makeHttpRequest(options, function(err, res) {
		if(err) {
			return callback(err);
		}
		callback(null, res);
	});
}

/**
 * Unpublish blacklisted hotels
 * @param hotelGroupId {String} Hotel group Id, into which the hotels should be unpublished
 * @param callback
 */
exports.unpublishBlacklistedHotels = function(hotelGroupId, callback) {
	hgServices.getHotelGroup(hotelGroupId, function(err, hotelGroup) {
		if(err) {
			return callback(err);
		}
		var hotelGroupData = hotelGroup.data;
		htlSrv.getPublishedHotelsByIds(hotelGroupId, hotelGroupData.blacklistedHotels, function(err, hotels) {
			if(err) {
				return callback(err);
			}
			if(hotels.length > 10) {
				return callback(errors.newError('int-tc-blckl-cnt-grt', {hotelsCount: hotels.length}));
			}
			var promises = [];

			function editHotel(p, hotel, hotelGroupData) {
				htlSrv.updateHotel(hotel, hotelGroupData, function(err, res) {
					if(err) {
						return p.reject(err);
					}
					p.resolve(res);
				});
			}

			for(var index in hotels) {
				var hotelData = hotels[index].data;
				var p = q.defer();
				promises.push(p.promise);
				hotelData.meta.status = 'draft';
				hotelData.meta.scheduledAt = new Date().toISOString();
				logging.info('Unpublishing blacklisted hotel', hotelData.hotelId);
				editHotel(p, hotelData, hotelGroupData);
			}

			q.all(promises).done(function() {
				var hotelIds = hotels.map(function(hotel) {
					return hotel.data.hotelId;
				});
				return callback(null, hotelIds);
			}, function(err) {
				callback(err);
			});

		});

	});
};

/**
 * List files in FTP server
 * @param ftpClientInstance
 * @param callback
 */
function listFtpFiles(ftpClientInstance, callback) {
	ftpClientInstance.list(process.env.TRUST_CONTENT_FTP_FOLDER, function(err, list) {

		if(err) {
			return callback(errors.newError('int-tc-ftp-err', {originalError: err.message, causedBy: 'list'}));
		}

		callback(null, list);
	});
}

/**
 * Create a ftp connection
 * @param callback
 */
function createFtpConnection(callback) {
	var ftpClientInstance = new ftpClient();
	ftpClientInstance.on('error', function(err) {
		return callback(errors.newError('int-tc-ftp-err', {originalError: err, causedBy: 'connection'}));
	});

	ftpClientInstance.on('ready', function() {
		callback(null, ftpClientInstance);
	});

	// connect to localhost:21 as anonymous
	ftpClientInstance.connect({
		host: process.env.TRUST_CONTENT_FTP_HOST,
		port: process.env.TRUST_CONTENT_FTP_PORT,
		user: process.env.TRUST_CONTENT_FTP_USER,
		password: process.env.TRUST_CONTENT_FTP_PASS
	});

}

/**
 * Start a coordinated import process from the FTP
 * This will get the file listing from the FTP, and divide the import process into smaller jobs,
 * so it will get done faster and in parallel.
 * @param hotelGroupId
 * @param callback
 * @param [limit]
 * @param [s3Folder] Optional folder path in AWS S3
 */
exports.startCoordinatedImportFromFtp = function(hotelGroupId, callback, limit, s3Folder) {
	var date = new Date();
	s3Folder = s3Folder || path.join('downloaded', date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear());
	var output = [];
	exports.copyFtpFilesToS3(s3Folder, function(err, results) {
		//if something failed in download process, return error (it will get a retry)
		if(err) {
			err.data.retry = true;
			return callback(err);
		}

		//executes a single job, which processes a set of files
		function schedule(p, s3Files) {
			jobHandler.createJob(
				'TrustContent XML importer', 'trustContentImportPart',
				{
					hotelGroupId: hotelGroupId,
					files: s3Files
				},
				1,
				function(err, job) {

					scheduler.executeJobAndWaitForCompletion(job, function(err, subResult) {
						output.push({files: s3Files, job: job, error: err, result: subResult});
						p.resolve();
					});
				}
			);

		}

		var promises = [];
		var processed = 0;
		if(limit) {
			results.splice(limit, 10000);
		}
		while(results.length) {

			var p = q.defer();
			promises.push(p.promise);
			var items = results.splice(0, 50);

			schedule(p, items.map(function(item) {
				return item.s3Key;
			}));
			processed += items.length;

		}

		q.all(promises).done(function() {
			logging.info("DONE importing from Trust Content, reporting failures:");
			output.forEach(function(subResult) {
				if(!subResult.error) {
					subResult.result.response.importResults.forEach(function(fileResult) {
						if(fileResult.error) {
							logging.info(fileResult.file + ' - ERROR - ' + fileResult.error.message + ' - ' + JSON.stringify(fileResult.error.data));
						}
						if(check.array(fileResult.mappingErrors)) {
							fileResult.mappingErrors.forEach(function(mapError) {
								logging.info(fileResult.file + '- MISSING DATA - ' + mapError.message + ' - ' + JSON.stringify(mapError.data));
							});
						}
					});
				} else {
					logging.info(JSON.stringify(subResult.files) + ' - ERROR - ' + subResult.error.message + ' - ' + JSON.stringify(subResult.error.data));
				}

			});
			var dateEnd = new Date();
			logging.info('Trust Content import done in ' + (dateEnd.getTime() - date.getTime()) / 1000 + ' seconds');
			callback(null, output);
		});

	}, limit);

};

/**
 * Start a coordinated import process from the website
 * This will get the file listing from the FTP, and divide the import process into smaller jobs,
 * so it will get done faster and in parallel.
 * @param hotelGroupId
 * @param callback
 * @param [limit]
 * @param [s3Folder] Optional folder path in AWS S3
 * @param [onlyNewHotels] Boolean flag inidicating if only new hotels should be imported
 * @param [onlyListedHotels] Optionally array of hotel ID's, which should only be handeled
 */
exports.startCoordinatedImportFromWebsite = function(hotelGroupId, callback, limit, s3Folder, onlyNewHotels, onlyListedHotels) {
	var date = new Date();
	s3Folder = s3Folder || path.join('downloaded', date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear());
	var output = [];
	if(hotelGroupId !== 'slh') {
		return callback(errors.newError('int-inv-hg', {givenHotelGroupId: hotelGroupId}));
	}

	exports.copySlhWebsiteHtmlToS3(s3Folder, function(err, results) {
		//if something failed in download process, return error (it will get a retry)
		if(err) {
			logging.error('copySlhWebsiteHtmlToS3 failed, rescheduling...', err.message);
			err.data.retry = true;
			return callback(err);
		}

		//executes a single job, which processes a set of files
		function schedule(p, hotels) {
			jobHandler.createJob(
				'Website importer', 'websiteImportPart',
				{
					hotelGroupId: hotelGroupId,
					hotels: hotels
				},
				1,
				function(err, job) {
					if(err) {
						output.push({hotels: hotels, job: job, error: err, result: null});
						return p.resolve();
					}
					scheduler.executeJobAndWaitForCompletion(job, function(err, subResult) {
						output.push({hotels: hotels, job: job, error: err, result: subResult});
						p.resolve();
					}, 1800);
				}
			);

		}

		function doScheduling() {
			var promises = [];
			var processed = 0;

			var hotels = [];
			for(var hotelId in results.hotelResults) {
				if(check.array(onlyListedHotels) && onlyListedHotels.indexOf(hotelId) < 0){
					continue;
				}
				if(check.integer(limit) && limit <= hotels.length) {
					break;
				}
				hotels.push(results.hotelResults[hotelId].files);
			}

			while(hotels.length) {

				var p = q.defer();
				promises.push(p.promise);
				var items = hotels.splice(0, 50);

				schedule(p, items);
				processed += items.length;

			}

			q.all(promises).done(function() {
				logging.info("DONE importing from Website, reporting failures:");
				output.forEach(function(subResult) {
					if(!subResult.error) {
						subResult.result.response.importResults.forEach(function(fileResult) {
							if(fileResult.error) {
								logging.error(' - WEB-IMPORT-ERROR-ITEM - ', fileResult.error.message, JSON.stringify(fileResult));
							}
						});
					} else {
						logging.error(' - WEB-IMPORT-ERROR-BATCH - ', subResult.error.message, JSON.stringify(subResult));
					}

				});
				var dateEnd = new Date();
				logging.info('Website importer done in ' + (dateEnd.getTime() - date.getTime()) / 1000 + ' seconds');
				callback(null, output);
			});
		}

		if(onlyNewHotels) {
			htlSrv.getPublishedHotelIds(hotelGroupId, function(err, exIds) {
				if(err) {
					return callback(err);
				}
				//remove exisitng hotels
				exIds.forEach(function(exId) {
					delete results.hotelResults[exId];
				});
				doScheduling();
			})
		} else {
			doScheduling();
		}
	}, limit);

};

/**
 * Import hotels from XML files, which are located in AWS S3 bucket
 *
 * @param hotelGroupId
 * @param callback
 * @param offset
 * @param limit
 * @param files
 */
exports.importFromS3 = function(hotelGroupId, callback, offset, limit, files) {
	var s3 = new aws.S3({params: {Bucket: process.env.AWS_S3_BUCKET}});

	var promises = [];
	var importResults = [];

	function doImport(p, file, i) {

		promises[i].done(function() {
			//download file
			s3.getObject({Key: file}, function(err, data) {

				if(err) {
					importResults.push(
						{
							file: file,
							importRes: null,
							error: errors.newError('int-s3-get-err', {originalError: err, key: file})
						}
					);
					return p.resolve();
				}

				exports.importTrustContentXml(data.Body, hotelGroupId, function(err, importRes, mappingErrors) {
					importResults.push(
						{
							file: file,
							importRes: importRes,
							error: err,
							mappingErrors: mappingErrors
						}
					);
					return p.resolve();

				});
			});

		});

	}

	var p1 = q.defer();
	promises.push(p1.promise);
	var inProcess = 0;
	files.forEach(function(file, index) {
		//skip ones set by offset
		if(index < offset
			|| inProcess >= limit
		) {
			return;
		}
		var p = q.defer();
		promises.push(p.promise);
		doImport(p, file, inProcess++);
	});

	p1.resolve();

	q.allSettled(promises).done(function() {
		callback(null, {importResults: importResults});
	});
};

/**
 * Import hotels from HTML and JSON files, which are located in AWS S3 bucket
 *
 * @param hotelGroupId
 * @param callback
 * @param hotels
 */
exports.importWebFromS3 = function(hotelGroupId, callback, hotels) {
	var s3 = new aws.S3({params: {Bucket: process.env.AWS_S3_BUCKET}});

	var promises = [];
	var importResults = [];
	var downloadedFiles = {};

	function download(p, file, fileId, hotelId) {

		s3.getObject({Key: file}, function(err, data) {
			if(err) {
				return p.reject(errors.newError('int-s3-get-err', {originalError: err, key: file}));
			} else {
				downloadedFiles[hotelId][fileId] = data.Body.toString();
				p.resolve(data);
			}
		});
	}

	function doImport(p, files, i, hotelId) {
		downloadedFiles[hotelId] = {};
		//download files first
		var dlPromises = [promises[i]];
		for(var fileId in files) {
			var pf = q.defer();
			dlPromises.push(pf.promise);
			download(pf, files[fileId].s3Key, fileId, hotelId);
		}

		//files downloaded and previous import done
		q.all(dlPromises).done(function() {
			exports.importFromWebsite(downloadedFiles[hotelId], hotelGroupId, function(err, importRes) {
				importResults.push(
					{
						importRes: importRes,
						files: files,
						error: err
					}
				);
				return p.resolve();

			});
			//delete reference from large object
			delete downloadedFiles[hotelId];
		}, function(err) {
			importResults.push(
				{
					importRes: null,
					files: files,
					error: err
				}
			);
			delete downloadedFiles[hotelId];
			return p.resolve();
		});

	}

	var p1 = q.defer();
	promises.push(p1.promise);
	var inProcess = 0;

	for(var hotelId in hotels) {
		var filesArray = hotels[hotelId];
		//skip ones set by offset
		var p = q.defer();
		promises.push(p.promise);
		doImport(p, filesArray, inProcess++, hotelId);
	}

	p1.resolve();

	q.allSettled(promises).done(function() {
		callback(null, {importResults: importResults});
	});
};

/**
 * Import a given XML file to the system
 * @param sourceFiles {Object} Object of source files, containing json and html keys
 * @param hotelGroupId {String} Hotel group Id, into which the hotels should be imported
 * @param callback
 */
exports.importFromWebsite = function(sourceFiles, hotelGroupId, callback) {

	hgServices.getHotelGroup(hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}

		try {
			sourceFiles.json = JSON.parse(sourceFiles.json);
		} catch(e) {
			return callback(errors.newError('int-web-js-pars', {originalError: e.message}));
		}
		exports.mapHotelHtml(sourceFiles.html, sourceFiles.json, res.data, function(err, mappedHotelData) {

			if(err) {
				return callback(err);
			}

			exports.importHotel(mappedHotelData, res.data, function(err, importRes) {
				if(err) {
					return callback(err);
				}
				callback(null, importRes);
			});

		});
		sourceFiles = undefined;
	});

};

/**
 * Import a given XML file to the system
 * @param xml {String} XML file
 * @param hotelGroupId {String} Hotel group Id, into which the hotels should be imported
 * @param callback
 */
exports.importTrustContentXml = function(xml, hotelGroupId, callback) {

	try {
		var hotelJson = JSON.parse(xml2json.toJson(xml, {arrayNotation: true}));
	} catch(e) {
		return callback(errors.newError('int-tc-parse-error', {originalError: e.message}));
	}

	hgServices.getHotelGroup(hotelGroupId, function(err, res) {
		if(err) {
			return callback(err);
		}

		if(res.data.blacklistedHotels
			&& res.data.blacklistedHotels.indexOf(hotelJson.TrustContent[0].PropertyInformation[0].HotelCode) > -1) {
			return callback(errors.newError('int-tc-blckl-err'));
		}

		exports.mapHotelData(hotelJson, res.data, false, function(err, mappedHotelData, mappingErrors) {
			if(err) {
				return callback(err);
			}

			exports.importHotel(mappedHotelData, res.data, function(err, importRes) {
				if(err) {
					return callback(err);
				}
				callback(null, importRes, mappingErrors);
			});

		});
	});

};

function validate(hotel, hotelGroup, method, target, p) {
	switch(target) {
		case 'meta':
			verValidation.validateMetaData(hotel.meta, 'hotel_v', method, function(err) {
				if(err) {
					return p.reject(err);
				}
				p.resolve();
			});
			break;
		case 'data':
			htlValidation.validateHotelData(hotel, method, function(err) {
				if(err) {
					return p.reject(err);
				}
				p.resolve();
			}, hotelGroup);
			break;
	}

}

function categories(hotelJson, hotelGroupData, callback) {

	var categoryIds = [];
	var errors = [];

	var hotelId = hotelJson.TrustContent[0].PropertyInformation[0].HotelCode;

	if(hotelGroupData.categoryMap) {
		for(var key in hotelGroupData.categoryMap) {
			var hotelIds = hotelGroupData.categoryMap[key];

			if(hotelIds.indexOf(hotelId) > -1) {
				categoryIds.push(key);
			}
		}
	}
	return callback(null, categoryIds, errors);

}

/**
 * Map needed data from xml2json object to hotel data object
 * @param hotelJson {Object} json data generated from xml
 * @param hotelGroupData {String} Hotel group Id, where the hotel belongs to
 * @param [hotel] {Object} existing hotel object from database (optional)
 * @param callback
 */
exports.mapHotelData = function(hotelJson, hotelGroupData, hotel, callback) {

	try {
		var propInfo = hotelJson.TrustContent[0].PropertyInformation[0];
		var now = new Date().toISOString();
		var hotelId = propInfo.HotelCode;
		var mapErrors = [];
		if(!hotel) {

			hotel = {
				meta: {
					guid: hotelId + '_' + hotelGroupData.hotelGroupId,
					status: 'published',
					createdAt: now,
					appliedAt: null,
					scheduledAt: new Date(0).toISOString() //in past, does not really matter, we need it to be same across versions
				},
				version: {
					number: 1
				},
				publishing: {},
				hotelId: hotelId,
				hotelGroup: {
					hotelGroupId: hotelGroupData.hotelGroupId
				},
				localized: [{
					gallery: [],
					contacts: {
						primary: {}
					},
					location: {},
					description: [],
					bookingUrl: 'https://www.yourreservation.net/tb3/mobile?bf=MSLHChainApp&arrivalDate=<StartDateIso>&nights=<NoOfNights>&adults=<NoOfAdults>&hotelcode=' + hotelId

				}],
				coordinates: {}
			};
		}
		hotel.gallery = [];
		hotel.localized[0].name = propInfo.GeneralInformation[0].PropertyAddress[0].HotelName;
		hotel.localized[0].contacts.primary.email = propInfo.GeneralInformation[0].PropertyAddress[0].EmailAddress;

		try {
			hotel.localized[0].contacts.primary.phone = '+' + propInfo.GeneralInformation[0].PhoneNumber[0].InternationalDialingCode
				+ '-' + propInfo.GeneralInformation[0].PhoneNumber[0].AreaCode
				+ '-' + propInfo.GeneralInformation[0].PhoneNumber[0].Number;
		} catch(e) {
			e.data = {key: 'TrustContent[0].PropertyInformation[0].GeneralInformation[0].PhoneNumber[0]'};

			mapErrors.push(e);
		}
		try {
			hotel.localized[0].contacts.primary.website = propInfo.GeneralInformation[0].PropertyAddress[0].Website.trim();
		} catch(e) {
			e.data = {key: 'TrustContent[0].PropertyInformation[0].GeneralInformation[0].PropertyAddress[0].Website'};

			mapErrors.push(e);
		}
		hotel.localized[0].language = 'en_GB';
		hotel.localized[0].location.city = propInfo.GeneralInformation[0].PropertyAddress[0].City;
		hotel.localized[0].location.country = propInfo.GeneralInformation[0].PropertyAddress[0].Country;

		hotel.localized[0].location.fullAddress = propInfo.GeneralInformation[0].PropertyAddress[0].Street
			+ ', ' + propInfo.GeneralInformation[0].PropertyAddress[0].City
			+ ', ' + propInfo.GeneralInformation[0].PropertyAddress[0].Zipcode
			+ ', ' + propInfo.GeneralInformation[0].PropertyAddress[0].Country;
		hotel.localized[0].location.shortAddress = propInfo.GeneralInformation[0].PropertyAddress[0].Street
			+ ', ' + propInfo.GeneralInformation[0].PropertyAddress[0].Country;

		var regex = new RegExp('\\\\n', 'g');

		try {
			hotel.localized[0].description[0] = {
				"heading": [
					{
						"style": null,
						"value": "Overview"
					}
				],
				"text": [
					{
						"style": null,
						"value": propInfo.GeneralInformation[0].SellingPoint[0].Text.replace(regex, '\n')
					}
				]
			};
		} catch(e) {
			e.data = {key: 'TrustContent[0].PropertyInformation[0].GeneralInformation[0].SellingPoint[0].Text'};

			mapErrors.push(e);
		}

		try {
			var services = [];
			propInfo.PropertyFeatures[0].Services[0].Service.forEach(function(prop) {
				services.push(hotelGroupData.tcCodeMap.ServiceCode[prop.Code]);
			});
			hotel.localized[0].description[1] = {
				"heading": [
					{
						"style": null,
						"value": "Services"
					}
				],
				"text": [
					{
						"style": null,
						"value": services.join(', ').trim()
					}
				]
			};
		} catch(e) {
			e.data = {key: 'TrustContent[0].PropertyInformation[0].PropertyFeatures[0].Services[0].Service'};

			mapErrors.push(e);
		}

		try {
			var facilities = [];
			propInfo.PropertyFeatures[0].Facilities[0].Facility.forEach(function(prop) {
				facilities.push(hotelGroupData.tcCodeMap.FacilityCode[prop.Code]);
			});

			hotel.localized[0].description[1] = {
				"heading": [
					{
						"style": null,
						"value": "Facilities"
					}
				],
				"text": [
					{
						"style": null,
						"value": facilities.join(', ').trim()
					}
				]
			};
		} catch(e) {
			e.data = {key: 'TrustContent[0].PropertyInformation[0].PropertyFeatures[0].Facilities[0].Facility'};

			mapErrors.push(e);
		}
		try {
			var recreation = [];
			propInfo.Recreation[0].Activity.forEach(function(prop) {
				recreation.push(hotelGroupData.tcCodeMap.RecreationCode[prop.Code]);
			});

			hotel.localized[0].description[1] = {
				"heading": [
					{
						"style": null,
						"value": "Recreation"
					}
				],
				"text": [
					{
						"style": null,
						"value": recreation.join(', ').trim()
					}
				]
			};
		} catch(e) {
			e.data = {key: 'TrustContent[0].PropertyInformation[0].Recreation[0].Activity'};

			mapErrors.push(e);
		}

		try {
			hotel.coordinates.lat = parseFloat(propInfo.Location[0].Latitude);

			hotel.coordinates.long = parseFloat(propInfo.Location[0].Longitude);
		} catch(e) {
			return callback(errors.newError('int-tc-map-error', {
				key: 'TrustContent[0].PropertyInformation[0].Location[0]',
				originalError: e.message
			}));
		}

		var newGal = [];
		try {
			var oldGal = hotelJson.TrustContent[0].PropertyInformation[0].GeneralInformation[0].PictureLinks[0].PictureLink;

			for(var i = 0; i < oldGal.length; i++) {
				var item = {
					source: {
						origUrl: oldGal[i].Link,
						origDesc: oldGal[i].Description,
						origCode: oldGal[i].PictureCode
					}
				};
				newGal.push(item);
			}
		} catch(e) {
			e.data = {key: 'TrustContent[0].PropertyInformation[0].GeneralInformation[0].PictureLinks[0].PictureLink'};

			mapErrors.push(e);
		}

	} catch(e) {
		return callback(errors.newError('int-tc-map-error', {originalError: e.stack}))
	}

	exports.uploadPictures(newGal, hotelGroupData.hotelGroupId, hotel.hotelId, function(err, res) {
		if(err) {
			return callback(err);
		}

		hotel.gallery = res;
		hotel.localized[0].gallery = res;
		categories(hotelJson, hotelGroupData, function(err, categoryIds, catErrors) {

			hotel.categoryIds = categoryIds;

			callback(err, hotel, mapErrors.concat(catErrors));
		}); //TODO: implement it according to the hotelGroup mapping

	});

};

exports.uploadPictures = function(gallery, hotelGroupId, hotelId, callback) {

	var newGal = [];
	var doUpload = function(p, index, item, oldPic) {
		var config = {public_id: 'gallery/' + index};
		media.uploadHotelImage(hotelGroupId, hotelId, item.source.origUrl, config, function(err, res) {
			if(err) {
				return p.reject(err);
			}
			var img = {
				origDesc: item.source.origDesc,
				origCode: item.source.origCode
			};
			media.setImageObjectSource(img, res);
			newGal[index] = img;
			p.resolve();
		}, oldPic);
	};

	hServices.getHotelRaw(hotelGroupId, hotelId, function(err, res) {
		var existGal = [];
		if(err) {
			if(err.data.code !== 'hs-hotel-nf') {
				return callback(err);
			}
		} else {
			existGal = (res.data.localized[0].gallery) ? res.data.localized[0].gallery : [];
		}
		var promises = [];
		for(var i = 0; i < gallery.length; i++) {
			var oldPic = existGal[i] ? existGal[i].source : null;
			var item = gallery[i];
			var p = q.defer();
			promises.push(p.promise);
			doUpload(p, i, item, oldPic);
		}
		q.all(promises).done(function() {
			callback(null, newGal);
		}, function(err) {
			callback(err);
		});
	});

};

exports.importHotel = function(hotel, hotelGroup, callback) {
	var guid = hotel.meta.guid;
	versionDao.checkIfGuidExists('hotel_v', guid, function(err, res) {
		if(err) {
			return callback(err);
		}

		var d, m, prom;
		d = q.defer();
		m = q.defer();
		prom = [d.promise, m.promise];

		switch(res) {
			case true:
				//hotel exists, lets update
				validate(hotel, hotelGroup, 'update', 'meta', m);
				validate(hotel, hotelGroup, 'update', 'data', d);
				q.all(prom).done(function() {
					htlSrv.updateHotel(hotel, hotelGroup, function(err) {
						//if no changes, do not report it as an error
						if(err
							&& err.data.code === 'ver-no-change'
						) {
							return callback(null, {noChanges: true});
						}

						callback(err, res);
					});
				}, function(err) {
					callback(err);
				});

				break;
			case false:
				//hotel does not exist yet, need to add
				validate(hotel, hotelGroup, 'add', 'meta', m);
				validate(hotel, hotelGroup, 'full', 'data', d);
				q.all(prom).done(function() {
					htlSrv.createHotel(hotel, hotelGroup, function(err, res) {
						callback(err, res);
					});
				}, function(err) {
					callback(err);
				});
				break;
		}
	});
};

/**
 * Copy files from foreign FTP server to S3 bucket
 * @param targetFolder {String} The target S3 folder
 * @param callback
 * @param [limit] {Integer} Limit of how many files can be imported
 */
exports.copyFtpFilesToS3 = function(targetFolder, callback, limit) {

	createFtpConnection(function(err, ftpConnection) {
		if(err) {
			return callback(err);
		}

		var s3 = new aws.S3({params: {Bucket: process.env.AWS_S3_BUCKET}});

		//get list of already downloaded files from AWS S3
		s3.listObjects({
			Prefix: path.join(process.env.AWS_S3_FOLDER, targetFolder)
		}, function(err, data) {
			if(err) {
				return callback(errors.newError('int-s3-list-err', {originalError: err.stack}));
			}
			var results = [];

			var existingFiles = data.Contents.map(function(item) {
				//add existing files to the completed items array
				var name = item.Key.split('/').pop();
				results.push({uploadRes: true, file: {name: name}, s3Key: item.Key});
				return name;
			});

			//Get list of files in FTP server
			listFtpFiles(ftpConnection, function(err, list) {
				if(err) {
					return callback(err);
				}

				//filter out files, which need to be downloaded
				var missingFiles = [];
				list.forEach(function(file) {
					if(existingFiles.indexOf(file.name) < 0) {
						missingFiles.push(file);
					}
				});

				var promises = [];

				function copy(p, ftpFile) {
					//wait for previous file to finish
					promises[promises.length - 2].finally(function() {
						//get file from FTP

						ftpConnection.get(
							path.join(process.env.TRUST_CONTENT_FTP_FOLDER, ftpFile.name),
							function(err, stream) {
								if(err) {
									results.push({error: err, file: ftpFile});
									return p.reject(errors.newError('int-tc-ftp-get', {originalError: err, causedBy: 'get'}));
								}

								stream.setEncoding('utf8');
								var xml = '';
								stream.on('data', function(chunk) {
									xml += chunk;
								});

								stream.on('end', function() {
									var params = {Key: path.join(process.env.AWS_S3_FOLDER, targetFolder, ftpFile.name), Body: xml};
									//upload file to S3
									s3.upload(params, function(err, data) {

										if(err) {
											results.push({error: err, file: ftpFile});
											logging.info("Download from FTP to S3", ftpFile.name, err.message);
											return p.reject(errors.newError('int-s3-upl-err', {originalError: err}));
										}
										results.push({uploadRes: data, file: ftpFile, s3Key: params.Key});
										p.resolve();
									});
								});

							}
						);
					});

				}

				var p1 = q.defer();
				promises.push(p1.promise);
				//remove other items if limit given
				if(check.integer(limit)) {
					missingFiles.splice(limit, 10000);
				}
				missingFiles.forEach(function(file) {
					var p = q.defer();
					promises.push(p.promise);
					copy(p, file);
				});

				logging.info("Starting FTP to S3 download", missingFiles.length);

				//resolve first dummy promise
				p1.resolve();

				//if all done
				q.allSettled(promises).done(function(snapshots) {
					ftpConnection.end();
					//if error occurred, return first error object
					for(var si in snapshots) {
						var snapshot = snapshots[si];
						if(snapshot.state === 'rejected') {
							return callback(snapshot.reason, results);
						}
					}

					callback(null, results);
				});

			});

		});

	});

};

/**
 * Copy SLH hotel html files from slh.com to S3 bucket
 * @param targetFolder {String} The target S3 folder
 * @param callback
 * @param [limit] {Integer} Limit of how many files can be imported
 */
exports.copySlhWebsiteHtmlToS3 = function(targetFolder, callback, limit) {
	var timeStart = new Date();
	var s3 = new aws.S3({params: {Bucket: process.env.AWS_S3_BUCKET}});
	var resultsByHotelId = {};
	var baseFolder = path.join(process.env.AWS_S3_FOLDER, targetFolder, 'slh');

	function reportFile(hotelId, fileId, s3Key, error, uploadResult) {
		resultsByHotelId[hotelId].files[fileId] = {
			error: error,
			s3Key: s3Key,
			uploadResult: error ? false : uploadResult
		}
	}

	function upload(params, hotelId, fileId, p) {
		s3.upload(params, function(err, data) {
			if(err) {
				reportFile(hotelId, fileId, params.Key, errors.newError('int-s3-upl-err', {originalError: err}));
				return p.reject(errors.newError('int-s3-upl-err', {originalError: err}));
			}
			reportFile(hotelId, fileId, params.Key, null, {Location: data.Location});
			p.resolve();
		});
	}

	function uploadJSON(p, hotel, fileName) {
		var params = {Key: path.join(baseFolder, fileName), Body: JSON.stringify(hotel)};
		//upload file to S3
		upload(params, hotel.hotelcode, 'json', p);
	}

	function getHtml(p, ph, hotel, fileName) {
		var params = {Key: path.join(baseFolder, fileName)};
		p.finally(function() {
			var delay = Math.random() * 10000;

			setTimeout(function() {
				exports.makeGetRequest(hotel.hotelUrl, function(err, res) {

					if(err) {
						reportFile(hotel.hotelcode, 'html', params.Key, errors.newError('int-web-get-f', {originalError: err}));
						return ph.reject(errors.newError('int-web-get-f', {originalError: err}));
					}
					params.Body = res;
					upload(params, hotel.hotelcode, 'html', ph);

				});
			}, delay);

		});

	}

	//download list of hotels
	exports.makeGetRequest('/search/keyword/?q=&p=1&tags=&pageSize=1000&sortBy=a-z&_=1444293178317', function(err, res) {

		if(err) {
			return callback(err);
		}
		try {
			var json = JSON.parse(res);
			if(res.indexOf('�') > -1){
				throw new Error('Invalid character in JSON: ' + res.substring(res.indexOf('�') - 10, res.indexOf('�') + 10));
			}
		} catch(e) {
			return callback(errors.newError('int-web-js-pars', {originalError: e.stack}));
		}

		//get list of already downloaded files from AWS S3
		s3.listObjects({
			Prefix: baseFolder
		}, function(err, data) {
			if(err) {
				return callback(errors.newError('int-s3-list-err', {originalError: err.stack}));
			}

			var existingFiles = data.Contents.map(function(item) {
				//add existing files to the completed items array
				var name = item.Key.split('/').pop();
				return name;
			});

			var promises = [];
			var processing = 0;
			logging.info("Starting WEB to S3 download", json.results.hotels.length - existingFiles.length);
			for(var hi in json.results.hotels) {
				var hotel = json.results.hotels[hi];
				var jsonName = hotel.hotelcode + '.json';
				var htmlName = hotel.hotelcode + '.html';
				var exIndex = existingFiles.indexOf(jsonName);
				var exIndex2 = existingFiles.indexOf(htmlName);

				resultsByHotelId[hotel.hotelcode] = {
					files: {}
				};

				var p = q.defer();
				var p2 = q.defer();
				promises.push(p.promise);
				promises.push(p2.promise);
				var add = 0;
				if(exIndex < 0) {
					uploadJSON(p, hotel, jsonName);
					add = 1;
				} else {
					reportFile(hotel.hotelcode, 'json', data.Contents[exIndex].Key, null, true);
					p.resolve();
				}
				if(exIndex2 < 0) {
					getHtml(p.promise, p2, hotel, htmlName);
					add = 1;
				} else {
					reportFile(hotel.hotelcode, 'html', data.Contents[exIndex2].Key, null, true);
					p2.resolve();
				}

				processing += add;

				if(check.integer(limit) && limit <= processing) {
					logging.info("Reached limit", processing, limit);
					break;
				}
			}

			logging.info("total", processing);

			//if all done
			q.allSettled(promises).done(function(snapshots) {
				//if error occurred, return first error object
				var timeEnd = new Date();
				logging.info("Done", (timeEnd.getTime() - timeStart.getTime()), promises.length / 2, existingFiles.length);

				for(var si in snapshots) {
					var snapshot = snapshots[si];
					if(snapshot.state === 'rejected') {
						return callback(snapshot.reason, {hotelResults: resultsByHotelId});
					}
				}

				callback(null, {hotelResults: resultsByHotelId});
			});

		});
	});

};

exports.mapHotelHtml = function(html, json, hotelGroup, callback) {
	var hotelId = json.hotelcode;
	var jqHtml = jq(html);

	function getEmail() {
		return jqHtml.find('.container-footer').find('a[href^="mailto:"]').text().trim();
	}

	function getNormalizedText(elem) {
		var html = elem.html();
		if(!html) {
			return '';
		}
		return html
			.replace(/<\/?\s*br[^>]?>/g, '\n') //replace <br> with new lines
			.replace(/\r\n/g, '\n') //replace windows style new lines with unix style
			.replace(/<p>/g, '\n') //add new line before paragraph
			.replace(/<h[1-5][^>]?>/g, '\n\n') //add double new line before headers
			.replace(/<\/h[1-5][^>]?>/g, '\n') //add new line after header
			.replace(/(<([^>]+)>)/g, '') //remove all other tags
			.replace(/\u2022\s*/g, '\n') //replace bullets with new lines
			.replace(/[ \t]{2,100}/g, ' ') //remove duplicate spaces and tabs
			.replace(/^[ \t]*/g, '') //remove spaces and tabs from line beginnings
			.replace(/(\s?\n\s?){3,100}/g, '\n\n') //replace
			.replace(/&amp;/g, '&') //replace ampersand
			.replace(/&nbsp;/g, ' ') //replace no braking space
			.replace(/\n /g, '\n') //remove space from beginning of line
			.trim();
	}

	function getPhone() {
		return jqHtml.find('.container-footer').find('a[href^="tel:"]').text().trim();
	}

	function getDividers(divider) {
		var el = jqHtml.find('.divider:Contains("' + divider + '")').next();
		//include following list
		el.append(el.next('ul'));

		return getNormalizedText(el);

	}

	function getRooms(divider) {
		var str = [];
		var slideText = jqHtml.find('.divider:Contains("' + divider + '")').next().find('.landing-slide').find('p').text();
		if(slideText && slideText.trim().length > 0) {
			str.push(slideText.trim());
		}


		return str.join('\n');
	}

	function getDividerTab(divider, tab) {
		var block = jqHtml.find('.divider:Contains("' + divider + '")').next();
		var text = '';
		block.find('.tabs-control').find('li').each(function(key, val) {
			var indexEl = jq(val).find('a:Contains("' + tab + '")');
			if(indexEl.length) {
				text = getNormalizedText(block.find('.slide').eq(key).find('.detail-container').first())
			}
		});
		return text;
	}

	function getGoldenText() {
		var straptext = '';
		jqHtml.find('.strapline').each(function(key, value) {
			if(jq(value).children().eq(1).text().length < 1) {
				straptext = jq(value).children().eq(0).text().trim();
				return false;
			}
		});

		return straptext;
	}

	function getCheckInOut(divider) {
		var str = '';
		str += jqHtml.find('h5:Contains("' + divider + '")').next().children().first().text();
		return str;
	}

	function getFacilities(divider) {
		var str = [];
		jqHtml.find('h5:Contains("' + divider + '")').next('ul').children().each(function(key, val) {
			str.push(jq(val).text().trim());
		});
		return str.join('\n');
	}

	function getRecreation(divider) {
		var str = [];
		jqHtml.find('h5:Contains("' + divider + '")').next('ul').children().each(function(key, val) {
			var substr = '';
			jq(val).children().each(function(key, span) {
				substr += jq(span).text() + ' ';
			});
			str.push(substr.trim());
		});
		return str.join('\n');
	}

	function categories(hotelGroupData) {
		var categoryIds = [];

		if(hotelGroupData.categoryMap) {
			for(var key in hotelGroupData.categoryMap) {
				var hotelIds = hotelGroupData.categoryMap[key];

				if(hotelIds.indexOf(hotelId) > -1) {

					categoryIds.push(key);
				}

			}
		}
		return categoryIds;
	}

	function getFullAddress() {
		var str = jqHtml.find('h4:Contains("Hotel Address")').next().children().find('p').html();
		str = str.split(/<br\s?\/?>/);

		var out = [];
		for(var i = 1; i < str.length; i++) {
			var val = str[i]
				.trim()
				.replace(/[\., -]+$/, '')
				.replace(/ {2,100}/g, ' ')
				.trim();
			if(val.length > 0) {
				out.push(val);
			}
		}
		return out.join(', ');
	}

	function removeEmptyDescription() {
		var newDesc = [];
		for(var i = 0; i < hotel.localized[0].description.length; i++) {
			if(hotel.localized[0].description[i].text[0].value.length > 0) {
				newDesc.push(hotel.localized[0].description[i]);
			}
		}
		hotel.localized[0].description = newDesc;
	}

	try {
		var now = new Date().toISOString();
		var hotel = {
			version: {
				number: 1
			},
			hotelGroup: {
				hotelGroupId: hotelGroup.hotelGroupId
			},
			meta: {
				guid: hotelId + '_' + hotelGroup.hotelGroupId,
				status: 'published',
				createdAt: now,
				appliedAt: null,
				scheduledAt: new Date().toISOString()
			},
			"publishing": {
				"startDate": now,
				"endDate": "2999-11-06T00:00:00+00:00",
				"published": true
			},
			categoryIds: categories(hotelGroup),
			localized: [
				{
					language: 'en_GB',
					gallery: [],
					name: json.name.trim(),
					location: {
						city: json.city,
						country: json.country,
						fullAddress: getFullAddress(),
						shortAddress: json.city + ', ' + json.country
					},
					contacts: {
						primary: {
							phone: getPhone(),
							email: getEmail(),
							website: 'http://www.slh.com' + json.hotelUrl
						}
					},
					description: [
						{
							'heading': [{'style': null, 'value': 'Unique qualities'}],
							'text': [{'style': null, 'value': getGoldenText() + '\n\n' + getDividers('Unique Qualities')}]
						},
						{
							"heading": [{"style": null, "value": "The hotel in detail"}],
							"text": [{"style": null, "value": getDividers('The hotel in detail')}]
						},
						{
							"heading": [{"style": null, "value": 'The rooms'}],
							"text": [{"style": null, "value": getRooms('The rooms')}]
						},
						{
							"heading": [{"style": null, "value": "Food & drink"}],
							"text": [{"style": null, "value": getDividerTab('Things to enjoy', 'Food & drink')}]
						},
						{
							"heading": [{"style": null, "value": "Spa"}],
							"text": [{"style": null, "value": getDividerTab('Things to enjoy', 'Spa')}]
						},
						{
							"heading": [{"style": null, "value": "Check in"}],
							"text": [{"style": null, "value": getCheckInOut('Check in')}]
						},
						{
							"heading": [{"style": null, "value": "Check out"}],
							"text": [{"style": null, "value": getCheckInOut('Check out')}]
						},
						{
							"heading": [{"style": null, "value": "Hotel facilities"}],
							"text": [{"style": null, "value": getFacilities('Hotel Facilities')}]
						},
						{
							"heading": [{"style": null, "value": "Room facilities"}],
							"text": [{"style": null, "value": getFacilities('Room Facilities')}]
						},
						{
							"heading": [{"style": null, "value": "Services"}],
							"text": [{"style": null, "value": getFacilities('Services')}]
						},
						{
							"heading": [{"style": null, "value": "Recreation"}],
							"text": [{"style": null, "value": getRecreation('Recreation')}]
						}
					],
					bookingUrl: 'https://www.yourreservation.net/tb3/mobile?bf=MSLHChainApp&arrivalDate=<StartDateIso>&nights=<NoOfNights>&adults=<NoOfAdults>&hotelcode=' + json.hotelcode

				}
			]
		};
		hotel.hotelId = hotelId;
		hotel.coordinates = {
			lat: json.latitude,
			long: json.longitude
		};

		removeEmptyDescription(hotel);
	} catch(e) {
		return callback(errors.newError('int-web-map-err', {originalError: e.stack}));
	}

	getImageUrls(hotelId, function(err, res) {
		if(err) {
			jqHtml = undefined;
			return callback(err);
		}
		res = res.substr(9, res.length - 10);
		try {
			var obj = JSON.parse(res);
		} catch(e) {
			jqHtml = undefined;
			return callback(errors.newError('int-img-err'));
		}
		var links = [];
		var imagesOrder = obj.tabegoryData.tabCategories[0].rcos;
		for(var imageIndex in imagesOrder) {
			var img = imagesOrder[imageIndex];
			var imgObj = obj.rcoData[img];

			//do not import videos
			if(imgObj.rcoType.match(/video/)){
				logging.warning('This image will be skipped', img, imgObj.caption, 'hotelId:', hotelId);
				continue;
			}
			//try{
			var list = imgObj.encodings;
			var biggest = {
				id: null,
				size: null,
				width: null,
				height: null
			};
			//Find biggest item
			for(var i = 0; i < list.length; i++) {
				var item = list[i];
				item.height = parseInt(item.height);
				item.width = parseInt(item.width);
				var size = item.height * item.width;
				if(size > biggest.size) {
					biggest.id = i;
					biggest.size = size;
					biggest.width = item.width;
					biggest.height = item.height;
				}
			}

			if(biggest.id &&
				biggest.width > biggest.height
			) {
				links.push({source: {origUrl: list[biggest.id].url}});
			}
			//} catch(e){
			//	logging.error('Error while importing hotel image', hotelId, img, e.message);
			//	//Strange item, nothing we can do here
			//}
		}

		exports.uploadPictures(links, hotel.hotelGroup.hotelGroupId, hotelId, function(err, res) {
			if(err) {
				jqHtml = undefined;
				return callback(err);
			}
			delete hotel.gallery;
			hotel.localized[0].gallery = res;
			jqHtml = undefined;
			callback(null, hotel);
		});
	});
};

/**
 * Update hotels blacklist
 * @param hotelGroupId
 * @param callback
 */
exports.updateHotelBlackList = function(hotelGroupId, callback) {
	//get white list of hotels from website
	exports.makeGetRequest('/search/keyword/?q=&p=1&tags=&pageSize=1000&sortBy=a-z&_=1444293178317', function(err, response) {

		if(err) {
			return callback(errors.newError('bl-req-fail', {originalError: err}));
		}

		//try to parse json response
		try {
			var hotelsList = JSON.parse(response).results.hotels;
		} catch(e) {
			return callback(errors.newError('bl-req-pars-err', {originalError: e}));
		}

		var whiteList = hotelsList.map(function(hotel) {
			return hotel.hotelcode;
		});

		hgServices.getHotelGroup(hotelGroupId, function(err, hotelGroup) {
			if(err) {
				return callback(err);
			}

			hotelGroup.data.blacklistedHotels = hotelGroup.data.blacklistedHotels || [];
			var blackList = hotelGroup.data.blacklistedHotels;

			//get list of hotels, we have in live
			htlSrv.getHotelsRaw(hotelGroupId, function(err, hotels) {
				if(err) {
					return callback(err);
				}

				var changed = false;
				var added = 0;
				//see if hotel is in white list or not
				hotels.forEach(function(hotel) {

					//if NOT in white list
					if(whiteList.indexOf(hotel.data.hotelId) < 0) {
						//add to black list if not there yet
						if(blackList.indexOf(hotel.data.hotelId) < 0) {
							blackList.push(hotel.data.hotelId);
							changed = true;
							added++;

						}

						//if IS in white list	and was in blacklist
					} else if(blackList.indexOf(hotel.data.hotelId) > -1) {
						//remove from blacklist
						blackList.splice(blackList.indexOf(hotel.data.hotelId), 1);
						changed = true;
					}

				});
				if(changed) {

					if(added > 10) {
						return callback(errors.newError('bl-update-cnd'));
					}

					//update hotel group
					hgServices.updateHotelGroup(hotelGroup.data, function(err) {
						callback(err, blackList);
					});
				} else {
					callback(null, blackList);
				}

			});
		});

	});

};