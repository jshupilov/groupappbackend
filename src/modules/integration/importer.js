'use strict';
var http = require('http');
var path = require('path');
var jq = require('jQuery');
var errors = require('../utils/errors');
var q = require('q');
var assert = require('assert');
var hgservice = require('../hotel_groups/services');
var versioning = require('../versioning/services');
var media = require('../media/services');

errors.defineError('imp-comm-err', 'Communication error', 'Unable to make http request', 'Integration', 500);
errors.defineError('imp-parse-fail', 'Parsing failed', 'Wrong input content format', 'Integration', 500);

exports.makeGetRequest = function(path, callback){
	exports.makeHttpRequest({
		host: 'www.slh.com',
		path: path,
		method: 'GET',
		timeout: 50000
	}, callback);
};

var prefix = 'slh';

exports.startImport = function(hgid, callback){
	hgservice.getHotelGroup(hgid, function(err, res){
		if(err){
			return callback(err);
		}
		exports.importWrapper(res.data, function(err, res){
			if(err){
				return callback(err);
			}
			versioning.updateItem(res, 'hotel_group_v', function(err){
				if(err){
					if(err.data.code === 'ver-no-change'){
						return callback(null, 'Nothing to import');
					} else{
						return callback(err);
					}

				}
				callback(null, 'Category import done');
			});

		});
	});
};

exports.importWrapper = function(hgJson, callback){
	var experiences;
	var destinations;

	function doTheTask(type, p, cb){
		exports.getCategoryTags(type, function(err, res){
			if(err){
				return p.reject(err);
			}
			exports.getHotels(res, type, hgJson, function(err, res){
				if(err){
					return p.reject(err);
				}
				cb(res);
				p.resolve();
			});
		});
	}

	var prom = [];
	var pd = q.defer();
	var pe = q.defer();
	prom.push(pd.promise);
	prom.push(pe.promise);
	doTheTask('experiences', pe, function(res){
		experiences = res;
	});
	doTheTask('destinations', pd, function(res){
		destinations = res;
	});

	q.all(prom).done(function(){
		exports.mergeData(hgJson, experiences, function(err, res){
			assert.strictEqual(err, null, 'Error in merge');
			exports.mergeData(res, destinations, function(err, res){
				assert.strictEqual(err, null, 'Error in merge');
				callback(null, res);
			});
		});

	}, function(err){
		callback(err);
	});
};

exports.mergeData = function(hgJson, newData, callback){

	function findCatItem(term, list){
		for(var i = 0; i < list.length; i++){
			if(list[i].categoryId === term){
				return i;
			}
		}
		return false;
	}

	function findMapItem(term, list){
		for(var item in list){
			if(item === term){
				return true;
			}
		}
		return false;
	}

	function mergeCat(){
		var origCat = (hgJson.categories) ? hgJson.categories : [];
		var newCat = newData.catList;
		for(var i = 0; i < newCat.length; i++){
			var newItem = newCat[i];
			var existingPosition = findCatItem(newItem.categoryId, origCat);
			if(existingPosition === false){
				origCat.push(newItem);
			} else{
				origCat[existingPosition] = newItem;
			}
		}
		return origCat;
	}

	function mergeCatMap(){
		//Merge catIds
		var origMap = (hgJson.categoryMap) ? hgJson.categoryMap : {};
		var newMap = newData.catMap;
		for(var item in newMap){
			if(!findMapItem(item, origMap)){
				origMap[item] = [];
			}
		}
		//Merge hotels
		for(var item2 in newMap){
			var oldList = origMap[item2];
			var newList = newMap[item2];
			for(var i = 0; i < newList.length; i++){
				var hotel = newList[i];
				if(oldList.indexOf(hotel) < 0){
					oldList.push(hotel);
				}
			}
			origMap[item2] = oldList;
		}
		return origMap;
	}

	hgJson.categories = mergeCat();
	hgJson.categoryMap = mergeCatMap();

	callback(null, hgJson);

};

var p1 = q.defer();
var requestQueue = [p1.promise];
p1.resolve();

/**
 * Make http request
 * @param options {host, path, method, timeout}
 * @param callback
 * @param [noWait] If should not wait for other requests
 */
exports.makeHttpRequest = function(options, callback, noWait){

	var p = q.defer();
	requestQueue.push(p.promise);

	function doReq(){
		console.log('making REQ', options.path);
		var req = http.request(options, function(response){
			var str = '';
			response.on('data', function(chunk){
				str += chunk;
			});
			response.on('end', function(){
				console.log('done', options.path);
				p.resolve();
				return callback(null, str);
			});
		});
		req.on('error', function(e){
			p.resolve();
			return callback(errors.newError('imp-comm-err', {originalError: e, path: options.path}));
		});
		req.on('timeout', function(){
			p.resolve();
			req.abort();
		});
		req.setTimeout(options.timeout);
		req.end();
	}
	if(noWait){
		doReq();
	}else{
		requestQueue[requestQueue.length-2].finally(doReq);
	}

};

/**
 * Get category tags from slh.com
 * @param type
 * @param callback
 */
exports.getCategoryTags = function(type, callback){
	var list = {};
	switch(type){
		case 'destinations':
			exports.makeGetRequest('/header/destinations/', function(err, res){
				if(err){
					return callback(err);
				} else if(res.indexOf('destinations-block') < 0){
					return callback(errors.newError('imp-parse-fail'));
				}
				try{

					jq(res).find('.destinations-block').each(function(key, val){
						var title = jq(val).find('h3').text();
						var item = jq(val).find('ul').html();
						list[title] = [];
						jq(item).find('a').each(function(key, item2){
							var href = jq(item2).attr('href');
							var urlparts = href.split('/');
							var tag = urlparts[urlparts.length - 2];
							list[title].push(tag);
						});
					});
				} catch(e){
					//Strange item, nothing we can do here
				}
				callback(null, list);
			});
			break;
		case 'experiences':

			exports.makeGetRequest('/search/?q=&tags=', function(err, res){
				if(err){
					return callback(err);
				} else if(res.indexOf('id="hotel-type"') < 0){
					return callback(errors.newError('imp-parse-fail'));
				}
				jq(res).find('#hotel-type').find('ul').children().each(function(key, val){
						var link = jq(val).find('a');
						var title = jq(link).text().split('(')[0].trim();

						var map = {
							'Lake or Waterfront': 'Waterfront',
							'Beach': 'Beach resort',
							'Golf': 'Golf resort',
							'Spa': 'Spa resort'
						};

						title = map.hasOwnProperty(title) ? map[title] : title;
						var pathurl = jq(link).attr('href');
						if(pathurl.indexOf('tags') !== -1){
							var rex = new RegExp('tags=([^%]*){1}', 'gi');
							var tag = rex.exec(pathurl)[1];
							list[title] = [tag];
						}
				});
				callback(null, list);
			});
			break;
	}
};

/**
 * Get hotels listed in category.
 * @param list List of tags in category
 * @param type
 * @param hgJson Hotel group json
 * @param callback
 */
exports.getHotels = function(list, type, hgJson, callback){
	var maxDepth = 0;
	switch(type){
		case 'destinations':
			maxDepth = 3;
			break;
		case 'experiences':
			maxDepth = 1;
			break;

	}
	var catList = [];
	var catMap = {};

	//Prepare helper functions

	/* Generates decent category id	*/
	function makeTag(item, type, parent){
		var itemParts = item.split(' ');
		var tag = '';

		for(var i = 0; i < itemParts.length; i++){
			if(itemParts.length > 1){
				tag += itemParts[i].toLowerCase().substring(0, parent ? 5 : 2);
			} else{
				tag += itemParts[i].toLowerCase().substring(0, parent ? 5 : 3);
			}
		}

		var id;
		if(!parent){
			id = prefix + '-' + type.substring(0, 3) + '-' + tag;
		} else{
			id = parent + '-' + tag;
		}
		id = id.replace(/[^a-zA-Z0-9-]/gi, 'x');
		return id;
	}



	/* Converts String To Title Case */
	function toTitleCase(str){
		return str.replace(/\w\S*/g, function(txt){
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		});
	}

	/**
	 * Convert Strings to title case
	 * @param str
	 * @returns {string}
	 */
	function toSentenceCase(str){
		return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
	}

	/**
	 * change casing
	 * @param str
	 * @returns {*}
	 */
	function formatCasing(str){
		//exceptions
		if(['unesco site', 'usa']. indexOf(str.toLocaleLowerCase()) > -1){
			return str.toLocaleUpperCase().replace('SITE', 'site');
		}
		if(type === 'destinations'){
			return toTitleCase(str.replace(/-/g, ' '));
		}else{
			return toSentenceCase(str);
		}
	}

	/* Finds category from category list by category Id */
	function findCategory(term, list){
		for(var i = 0; i < list.length; i++){
			var item = list[i];
			if(item.categoryId === term){
				return item;
			}
		}
		return false;
	}

	/* Maps parent categories in category map */
	function mapParents(){
		for(var item in catMap){
			var catItem = findCategory(item, catList);
			var parent = findCategory(catItem.parentCategoryId, catList);
			if(!catMap[parent.categoryId]){
				catMap[parent.categoryId] = [];
			}
			for(var c = 0; c < catMap[item].length; c++){
				catMap[parent.categoryId].push(catMap[item][c]);
			}
			var master = findCategory(parent.parentCategoryId, catList);
			if(!catMap[master.categoryId]){
				catMap[master.categoryId] = [];
			}
			for(var j = 0; j < catMap[item].length; j++){
				catMap[master.categoryId].push(catMap[item][j]);
			}
		}
	}

	function addNewCategory(cat, promiseList){
		catList.push(cat);
		if(promiseList){
			var pi = q.defer();
			promiseList.push(pi.promise);
			var filePath = path.join(__dirname, '../../../', 'data', 'hotel_groups', hgJson.hotelGroupId, 'categories', cat.categoryId, 'button.png');
			media.uploadHotelGroupImage(hgJson.hotelGroupId, filePath, {public_id: 'categories/' + cat.categoryId + '/button'}, function(err, res){

				if(!err){
					for(var lang in cat.localized){
						cat.localized[lang].buttonImage = {};
						media.setImageObjectSource(cat.localized[lang].buttonImage, res);
					}
					delete cat.buttonImage;
				}

				pi.resolve();
			});
		}

	}

	/* Add category to catList */
	function pushCategory(cat, promiseList){
		if(!findCategory(cat.categoryId, catList)){
			addNewCategory(cat, promiseList);
		}
	}

	/* Fetch data and add to catList and catMap */
	function doIt(list, p){
		/* Do the fetching */
		function getData(path, item, parent, maxDepth, p1){

			exports.makeGetRequest(path, function(err, res){
				if(err){
					return p1.reject(err);
				}

				if(maxDepth > 1){
					var subcat = {
						'categoryId': makeTag(item, type, parent),
						'parentCategoryId': parent,
						'type': type,
						'localized': {
							'en_GB': {
								'name': formatCasing(item)
							}
						}
					};
					parent = subcat.categoryId;
					pushCategory(subcat);
				}

				var obj = JSON.parse(res);

				var hotels = obj.results.hotels;
				var origParent = parent;
				for(var h = 0; h < hotels.length; h++){
					parent = origParent;
					var hotel = hotels[h];
					var area = hotel.city;

					if(maxDepth > 2){
						var subsubcat = {
							'categoryId': makeTag(area, type, parent),
							'parentCategoryId': parent,
							'type': type,
							'localized': {
								'en_GB': {
									'name': formatCasing(area)
								}
							}
						};
						pushCategory(subsubcat);
						parent = subsubcat.categoryId;
					}

					if(!catMap[parent]){
						catMap[parent] = [];
					}
					catMap[parent].push(hotel.hotelcode);
				}
				p1.resolve();

			});
		}//End of getData

		var prom2 = [];
		for(var target in list){
			var dest = list[target];
			var cat = {
				'categoryId': makeTag(target, type, null),
				'type': type,
				'localized': {
					'en_GB': {
						'name': formatCasing(target)
					}
				}
			};
			var parent = cat.categoryId;
			pushCategory(cat, prom2);
			for(var i = 0; i < dest.length; i++){
				var item = dest[i];

				var desturl = '/search/keyword/?tags=' + item.replace(/-/g, '') + '&pageSize=1000';
				var pF = q.defer();
				prom2.push(pF.promise);
				getData(desturl, item, parent, maxDepth, pF);
			}
		}
		q.all(prom2).done(function(){
			p.resolve();
		}, function(err){
			return p.reject(err);
		});

	}

	//Party starts here
	var p = q.defer();
	var promises = [];
	promises.push(p.promise);
	doIt(list, p);

	//When everything is done so far
	q.all(promises).done(function(){
		//Map parents in category map
		if(type === 'destinations'){
			mapParents();
		}

		callback(null, {catList: catList, catMap: catMap});
	}, function(err){
		return callback(err);
	});
};
