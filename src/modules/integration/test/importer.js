'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var importer = require('../importer');
var fs = require('fs');
var path = require('path');
var databaseUtils = require('../../utils/databaseUtils');
var escape = require('pg-escape');
var verDao = require('../../versioning/dao');
var media = require('../../media/services');


lab.experiment('Importer.Http request', function(){

	var options = {
		host: 'www.cardola.com',
		path: '/',
		method: 'GET',
		timeout: 50000
	};
	lab.afterEach(function(done){
		options.timeout = 50000;
		done();
	});

	lab.test('Should work', {timeout: 10000}, function(done){
		importer.makeHttpRequest(options, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('Http error', function(done){
		var options2 = JSON.parse(JSON.stringify(options));
		options2.host = 'someunknownhost';
		importer.makeHttpRequest(options2, function(err){
			testingTools.expectError(err, 'imp-comm-err');
			done();
		});
	});

	lab.test('Http timeout', function(done){
		options.timeout = 1;
		importer.makeHttpRequest(options, function(err){
			testingTools.expectError(err, 'imp-comm-err');
			done();
		});
	});
});

lab.experiment('Importer.Get Categories tags', function(){

	var dest = fs.readFileSync(path.join(__dirname, 'destinations.html')).toString();
	var exp = fs.readFileSync(path.join(__dirname, 'experiences2.html')).toString();
	testingTools.mockResponsesForExperiment(lab, function(){
	}, function(mitm){
		mitm.on('request', function(req, res){
			if(req.url.indexOf('/header/destinations/') !== -1){
				res.end(dest);
			}
			if(req.url.indexOf('/search/?q=&tags=') !== -1){
				res.end(exp);
			}

		});
	});

	lab.test('Should fetch destinations list', function(done){
		importer.getCategoryTags('destinations', function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			for(var item in res){
				testingTools.code.expect(item.length).to.be.above(0);
			}
			done();
		});
	});

	lab.test('Should fetch experiences list', {timeout: 60000}, function(done){
		importer.getCategoryTags('experiences', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res['Adult only'][0]).to.be.equal('adu');
			testingTools.code.expect(res['All inclusive'][0]).to.be.equal('all');
			testingTools.code.expect(res.Waterfront[0]).to.be.equal('lakeandwaterfronthotels');
			done();
		});
	});

});

lab.experiment('Importer.Get Categories error 1', function(){
	testingTools.mockResponsesForExperiment(lab, undefined, function(){
	});

	lab.test('Should get parse error for experiences', function(done){
		importer.getCategoryTags('experiences', function(err){
			testingTools.expectError(err, 'imp-parse-fail');
			done();
		});
	});

	lab.test('Should get parse error for destinations', function(done){
		importer.getCategoryTags('destinations', function(err){
			testingTools.expectError(err, 'imp-parse-fail');
			done();
		});
	});
});

lab.experiment('Importer.Get Categories error 3', function(){
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm){
		mitm.on('connection', function(socket){
			socket.destroy();
		});
	});
	lab.test('Should get comm error', {timeout: 20000}, function(done){
		importer.getCategoryTags('experiences', function(err){
			testingTools.expectError(err, 'imp-comm-err');
			done();
		});
	});
});

lab.experiment('Importer.GetHotels by list', function(){
	var destinations = require('./destinations.json');
	var experineces = require('./experiences.json');
	var estonia = require('./estonian_hotels.json');
	var austria = require('./austria_hotels.json');
	var cambodia = require('./cambodia_hotels.json');
	var adult = require('./adult_hotels.json');
	var allinc = require('./allinc_hotels.json');
	var hgData = {hotelGroupId: 'slh'};
	var f1;

	lab.before(function(done){
		f1 = media.uploadHotelGroupImage;
		done();
	});

	lab.afterEach(function(done){
		media.uploadHotelGroupImage = f1;
		done();
	});

	testingTools.mockResponsesForExperiment(lab, function(){
	}, function(mitm){

		mitm.on('request', function(req, res){
			if(req.url.indexOf('estonia') !== -1){
				res.end(JSON.stringify(estonia));
			}
			if(req.url.indexOf('austria') !== -1){
				res.end(JSON.stringify(austria));
			}
			if(req.url.indexOf('cambodia') !== -1){
				res.end(JSON.stringify(cambodia));
			}
			if(req.url.indexOf('adu') !== -1){
				res.end(JSON.stringify(adult));
			}
			if(req.url.indexOf('all') !== -1){
				res.end(JSON.stringify(allinc));
			}
			if(req.url.indexOf('angeles') !== -1){
				res.end(JSON.stringify(allinc));
			}
		});
	});

	lab.test('getHotels for destinations should go well', {timeout: 20000}, function(done){

		media.uploadHotelGroupImage = function(hotelGroupId, filePath, config, callback){
			if(filePath.indexOf('/slh-des-eur/') > -1){
				return callback(null, {public_id: 'blabla'});
			}else{
				return callback(new Error('cannot upload image!'));
			}
		};

		importer.getHotels(destinations, 'destinations', hgData, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.catList.length).to.be.above(0);
			testingTools.code.expect(res.catList[0].categoryId).to.be.equal('slh-des-eur');
			testingTools.code.expect(res.catMap['slh-des-asi'].length).to.be.equal(1);
			res.catList.forEach(function(cat){
				if(cat.categoryId === 'slh-des-eur'){
					testingTools.code.expect(cat.localized.en_GB, JSON.stringify(cat)).to.include('buttonImage');
				}else{
					testingTools.code.expect(cat.localized.en_GB, JSON.stringify(cat)).not.to.include('buttonImage');
				}
			});
			done();
		});
	});
	lab.test('getHotels for destinations should go well 2 (for line coverage)', {timeout: 20000}, function(done){

		importer.getHotels({'USA': ['angeles']}, 'destinations', hgData, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.catList.length).to.be.above(0);
			testingTools.code.expect(res.catList[0].localized.en_GB.name).to.equal('USA');
			testingTools.code.expect(res.catList[1].localized.en_GB.name).to.equal('Angeles');
			testingTools.code.expect(res.catMap['slh-des-usa-angel'].length).to.be.above(0);
			done();
		});
	});
	lab.test('getHotels for experiences should go well', {timeout: 20000}, function(done){
		importer.getHotels(experineces, 'experiences', hgData, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.catList.length).to.be.above(0);
			testingTools.code.expect(res.catList[0].categoryId).to.be.equal('slh-exp-adonho');
			testingTools.code.expect(res.catMap['slh-exp-adonho'].length).to.be.equal(10);
			done();
		});
	});

});

lab.experiment('Import.Wrapper', function(){
	var hgData = require('./data.json');

	var estonia = require('./estonian_hotels.json');
	var cambodia = require('./cambodia_hotels.json');
	var adultJ = require('./adult_hotels.json');
	var allincJ = require('./allinc_hotels.json');
	var lakeandwater = require('./lakeandwater.json');
	var dest = fs.readFileSync(path.join(__dirname, 'destinations.html')).toString();
	var exp2 = fs.readFileSync(path.join(__dirname, 'experiences2.html')).toString();

	testingTools.mockResponsesForExperiment(lab, function(){
	}, function(mitm){

		mitm.on('connect', function(socket, opts){
			if(opts.host.indexOf('cloudinary') > -1){
				return socket.bypass();
			}

		});

		mitm.on('request', function(req, res){
			if(req.url.indexOf('estonia') !== -1){
				res.end(JSON.stringify(estonia));
			}
			if(req.url.indexOf('cambodia') !== -1){
				res.end(JSON.stringify(cambodia));
			}

			if(req.url.indexOf('tags=adu') !== -1){
				res.end(JSON.stringify(adultJ));
			}
			if(req.url.indexOf('tags=all') !== -1){
				res.end(JSON.stringify(allincJ));
			}
			if(req.url.indexOf('destinations') !== -1){
				res.end(dest);
			}
			if(req.url.indexOf('/search/?q=&tags=') !== -1){
				res.end(exp2);
			}
			if(req.url.indexOf('tags=lakeandwaterfronthotels') !== -1){
				res.end(JSON.stringify(lakeandwater));
			}
		});
	});

	lab.test('Should work', {timeout: 60000}, function(done){
		importer.importWrapper(hgData, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

});

lab.experiment('Import.Wrapper getTags failure', function(){
	var _makeHttpRequest;
	var hgData = require('./data.json');

	lab.before(function(done){
		_makeHttpRequest = importer.makeHttpRequest;
		done();
	});

	lab.afterEach(function(done){
		importer.makeHttpRequest = _makeHttpRequest;
		done();
	});

	lab.test('Should work give failure on getTags', function(done){
		importer.makeHttpRequest = function(options, cb){
			if(options.path === '/header/destinations/'){
				return cb(new Error('httpfailure1'));
			} else if(options.path.indexOf('search/?q') > -1){
				return cb(null, fs.readFileSync( path.join(__dirname, 'allinc_hotels.html'), 'utf8') );
			} else{
				_makeHttpRequest(options, cb);
			}
		};
		importer.importWrapper(hgData, function(err){
			testingTools.code.expect(err.message).to.be.equal('httpfailure1');
			done();
		});
	});

	lab.test('Sould give failure on getHotels', {timeout: 20000}, function(done){
		importer.makeHttpRequest = function(options, cb){
			if(options.path.match(/keyword/g)){
				return cb(new Error('httpfailure2'));
			}
			_makeHttpRequest(options, cb, true);
		};

		importer.importWrapper(hgData, function(err){
			testingTools.code.expect(err.message).to.be.equal('httpfailure2');
			done();
		});

	});
});

lab.experiment('Import.MergeContent', function(){
	var original = require('./data.json');
	var originalWoCat = require('./data2.json');

	lab.test('Merge should do fine for destinations', function(done){
		var newData = require('./dest_catlist.json');

		importer.mergeData(original, newData, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('Merge should do fine for destinations wo cat and map', function(done){
		var newData = require('./dest_catlist.json');

		importer.mergeData(originalWoCat, newData, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('Merge should overwrite categories, if they have been updated', function(done){
		var newData = require('./dest_catlist.json');
		newData.catList[0].testing = 123;

		importer.mergeData(original, newData, function(err, hgJson){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hgJson.categories[5].testing).to.equal(123);
			done();
		});
	});


	lab.test('Merge should do fine for experiences', function(done){
		var newData = require('./exp_catlist.json');

		importer.mergeData(original, newData, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
});

lab.experiment('Importer.StartImport', function(){
	var hgOrig, _versionCreate, f1;
	var guid = 'slh';
	var table = 'hotel_group_v';



	var estonia = require('./estonian_hotels.json');
	var cambodia = require('./cambodia_hotels.json');
	var adultJ = require('./adult_hotels.json');
	var allincJ = require('./allinc_hotels.json');
	var dest = fs.readFileSync(path.join(__dirname, 'destinations.html')).toString();
	var exp2 = fs.readFileSync(path.join(__dirname, 'experiences2.html')).toString();
	var lakeandwater = require('./lakeandwater.json');

	testingTools.mockResponsesForExperiment(lab, function(){
	}, function(mitm){

		mitm.on('connect', function(socket, opts){
			if(opts.host.indexOf('cloudinary') > -1){
				return socket.bypass();
			}

		});

		mitm.on('request', function(req, res){
			if(req.url.indexOf('estonia') !== -1){
				res.end(JSON.stringify(estonia));
			}
			if(req.url.indexOf('cambodia') !== -1){
				res.end(JSON.stringify(cambodia));
			}

			if(req.url.indexOf('tags=adu') !== -1){
				res.end(JSON.stringify(adultJ));
			}
			if(req.url.indexOf('tags=all') !== -1){
				res.end(JSON.stringify(allincJ));
			}
			if(req.url.indexOf('destinations') !== -1){
				res.end(dest);
			}

			if(req.url.indexOf('/search/?q=&tags=') !== -1){
				res.end(exp2);
			}
			if(req.url.indexOf('tags=lakeandwaterfronthotels') !== -1){
				res.end(JSON.stringify(lakeandwater));
			}
		});
	});

	lab.before(function(done){
		f1 = exports.importWrapper;
		_versionCreate = verDao.create;
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape("SELECT * FROM %I WHERE  (data ->'meta'->'guid') = %L ORDER BY id DESC LIMIT 1", table, '"' + guid + '"');
			client.query(query, function(err, res){
				doneDb();
				testingTools.code.expect(err).to.be.null();
				hgOrig = JSON.parse(JSON.stringify(res.rows[0]));
				res.rows[0].data.categories = [];
				verDao.update(res.rows[0], 'hotel_group_v', function(err){
					testingTools.code.expect(err).to.be.null();
					done();
				});
			});
		});

	});

	lab.afterEach(function(done){
		verDao.create = _versionCreate;
		exports.importWrapper = f1;
		done();
	});

	lab.after(function(done){
		verDao.update(hgOrig, 'hotel_group_v', function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('Should give other error on versioning', {timeout: 10000}, function(done){
		verDao.create = function(data, tableName, cb){
			cb({data: {code: 'other'}, message: 'createvererr'});
		};

		importer.startImport('slh', function(err){
			testingTools.code.expect(err.message).to.be.equal('createvererr');
			done();
		});
	});

	lab.test('Start importer should work', {timeout: 10000}, function(done){

		importer.startImport('slh', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.equal('Category import done');
			done();
		});
	});

	lab.test('Should not import because of no differences', {timeout: 10000}, function(done){

		importer.startImport('slh', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.equal('Nothing to import');
			done();
		});
	});
});

lab.experiment('Importer.Start Import failures', function(){
	lab.test('Should get hg not found error', {timeout: 120000}, function(done){
		importer.startImport('someotherhgid', function(err){
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});
});


lab.experiment('Importer.Start Import failures', function(){

	var f1;

	lab.before(function(done){
		f1 = importer.makeHttpRequest;
		done();
	});
	lab.after(function(done){
		importer.makeHttpRequest = f1;
		done();
	});


	lab.test('Should get importer error', {timeout: 10000}, function(done){
		importer.makeHttpRequest = function(a, cb){
			cb(new Error('223f2f23d2d2'));
		};
		importer.startImport('slh', function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('223f2f23d2d2');
			done();
		});
	});
});
