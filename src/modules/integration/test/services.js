'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../TrustContent/services.js');
var hgServices = require('../../hotel_groups/services.js');
var htlServices = require('../../hotels/services.js');
var xml2json = require('xml2json');
var aws = require('aws-sdk');

var databaseUtils = require('../../utils/databaseUtils');
var eventUtils = require('../../utils/eventUtils');
var logging = require('../../utils/logging');
var scheduler = require('../../worker/scheduler');
var escape = require('pg-escape');
var verDao = require('../../versioning/dao');
var verVal = require('../../versioning/validation');
var verSer = require('../../versioning/services');
var htlVal = require('../../hotels/validation');
var fs = require('fs');
var path = require('path');
var hotelGroupId = 'test-hgid';
var ftpClient = require('ftp');
var htlXml, htlJson;
var s3 = new aws.S3({params: {Bucket: process.env.AWS_S3_BUCKET}});
var media = require('../../media/services');
var jobHandler = require('../../worker/jobHandler');

lab.test('read xml from file should succeed', function(done){
	htlXml = fs.readFileSync(path.join(__dirname, 'HUABZMP.xml'));
	done();
});

lab.test('xml to json should succeed', function(done){
	htlJson = JSON.parse(xml2json.toJson(htlXml, {arrayNotation: true}));
	done();
});
lab.experiment('trustContent.hotel.validateBlacklist', function(){

	testingTools.failDbQueryForExperiment(lab, 'hotel');

	lab.test('should give error if invalid DB query', function(done){
		services.unpublishBlacklistedHotels('slh', function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});

});
lab.experiment('trustContent.hotel.validateBlacklist', {timeout: 15000}, function(){
	var hg, newObject, updateHotelGroup, publishNewHotels, unpublishNewHotels;
	var getC, client, cd;

	lab.before(function(done){
		getC = databaseUtils.getConnection;
		databaseUtils.getConnection(function(err, cl, doneDb){
			testingTools.code.expect(err).to.be.null();
			client = cl;
			cd = doneDb;
			done();
		});
	});

	lab.before(function(done){
		newObject = function(data){
			return JSON.parse(JSON.stringify(data));
		};
		updateHotelGroup = function(data, id, callback){
			databaseUtils.getConnection(function(err, client, doneDb){
				testingTools.code.expect(err).to.be.null();
				client.query(
					'UPDATE hotel_group ' +
					'SET data = $1 ' +
					'WHERE id = $2',
					[data, id],
					function(err, res){
						doneDb();
						return callback(err, res);
					}
				);
			});
		};
		publishNewHotels = function(count, callback){
			htlServices.getHotelRaw(hotelGroupId, 'jacob', function(err, hotel){
				testingTools.code.expect(err).to.be.null();
				var queryLive = '';
				var queryVersioning = '';
				var hotelsIds = [];
				for(var i = 0; i < count; i++){
					var hotelId = 'test-hotel-' + i;
					hotelsIds.push(hotelId);
					var data = JSON.parse(JSON.stringify(hotel.data).replace(/jacob/g, hotelId));
					queryLive += escape('INSERT INTO hotel (data) VALUES(%L);', JSON.stringify(data));
					queryVersioning += escape('INSERT INTO hotel_v (data) VALUES(%L);', JSON.stringify(data));
				}

				client.query(queryLive + queryVersioning, function(err){
					return callback(err, hotelsIds);
				});

			});
		};
		unpublishNewHotels = function(callback){
			client.query(
				'DELETE ' +
				'FROM hotel ' +
				'WHERE (data -> \'hotelGroup\' -> \'hotelGroupId\') = $1 AND (data->>\'hotelId\')  LIKE \'test-hotel-%\'',
				['"' + hotelGroupId + '"'],
				function(err){
					testingTools.code.expect(err).to.be.null();
					client.query(
						'DELETE ' +
						'FROM hotel_v ' +
						'WHERE (data -> \'hotelGroup\' -> \'hotelGroupId\') = $1 AND (data->>\'hotelId\') LIKE \'test-hotel-%\' ',
						['"' + hotelGroupId + '"'],
						function(err, result){
							return callback(err, result);
						}
					);
				}
			);
		};
		hgServices.getHotelGroup(hotelGroupId, function(err, hotelGroup){
			testingTools.code.expect(err).to.be.null();
			hg = hotelGroup;
			done();
		});
	});

	lab.afterEach({timeout: 5000}, function(done){
		databaseUtils.getConnection = getC;
		unpublishNewHotels(function(err){
			testingTools.code.expect(err).to.be.null();
			updateHotelGroup(hg.data, hg.id, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				done();
			});
		});
	});

	lab.after(function(done){
		cd();
		done();
	});

	lab.test('should give error if invalid hotelGroupId', function(done){
		services.unpublishBlacklistedHotels('invalid-hotelGroupId', function(err){
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});
	lab.test('should give error if count blacklisted hotels greater then 10', function(done){
		var hotelsCount = 11;
		publishNewHotels(hotelsCount, function(err, hotelsIds){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hotelsIds).to.be.array().and.have.length(hotelsCount);

			var hotelGroupData = newObject(hg.data);

			testingTools.code.expect(hotelGroupData).to.be.object().and.include(['blacklistedHotels']);
			testingTools.code.expect(hotelGroupData.blacklistedHotels).to.be.array();

			hotelGroupData.blacklistedHotels = hotelGroupData.blacklistedHotels.concat(hotelsIds);

			updateHotelGroup(hotelGroupData, hg.id, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				services.unpublishBlacklistedHotels(hotelGroupId, function(err){
					testingTools.expectError(err, 'int-tc-blckl-cnt-grt');
					done();
				});
			});
		});
	});

	lab.test('should work if count blacklisted hotels less then 10', function(done){
		var hotelsCount = 5;
		publishNewHotels(hotelsCount, function(err, hotelsIds){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hotelsIds).to.be.array().and.have.length(hotelsCount);
			var hotelGroupData = newObject(hg.data);

			testingTools.code.expect(hotelGroupData).to.be.object().and.include(['blacklistedHotels']);
			testingTools.code.expect(hotelGroupData.blacklistedHotels).to.be.array();

			hotelGroupData.blacklistedHotels = hotelGroupData.blacklistedHotels.concat(hotelsIds);

			updateHotelGroup(hotelGroupData, hg.id, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				services.unpublishBlacklistedHotels(hotelGroupId, function(err, res){
					testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
					testingTools.code.expect(res).to.be.deep.equal(hotelsIds);
					verSer.handleGoLive('hotel_v', 'hotel', function(err, res){
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(res[1]).to.be.equal(hotelsIds.length);
						htlServices.getPublishedHotelsByIds(hotelGroupId, hotelsIds, function(err, res){
							testingTools.code.expect(err).to.be.null();
							testingTools.code.expect(res).to.be.array().and.empty();
							done();
						});
					});
				});
			});
		});
	});

	lab.test('should give error if can not unpublish hotel', function(done){
		var hotelsCount = 5;
		var counter = 0;
		databaseUtils.getConnection = function(cb){
			cb(
				null,
				{
					query: function(){
						counter++;
						if(counter === 10){
							return arguments[arguments.length - 1](new Error('Hotel unpublich failure'));
						}
						client.query.apply(client, arguments);
					}
				},
				function(){
				}
			);
		};
		publishNewHotels(hotelsCount, function(err, hotelsIds){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hotelsIds).to.be.array().and.have.length(hotelsCount);
			var hotelGroupData = newObject(hg.data);

			testingTools.code.expect(hotelGroupData).to.be.object().and.include(['blacklistedHotels']);
			testingTools.code.expect(hotelGroupData.blacklistedHotels).to.be.array();

			hotelGroupData.blacklistedHotels = hotelGroupData.blacklistedHotels.concat(hotelsIds);

			updateHotelGroup(hotelGroupData, hg.id, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				services.unpublishBlacklistedHotels(hotelGroupId, function(err){
					testingTools.code.expect(err).to.be.instanceof(Error);
					testingTools.code.expect(err.message).to.equal('Hotel unpublich failure');
					done();
				});
			});
		});
	});

	lab.test('should work if blacklist does not contain any common hotel id ', function(done){
		services.unpublishBlacklistedHotels(hotelGroupId, function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});
	});
});
lab.experiment('trustContent.hotel.mapping', {timeout: 50000}, function(){
	var oldHotel, hotelGroupData;
	lab.before(function(done){
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape("SELECT data FROM hotel WHERE (data ->'meta'->'guid') = '\"%s\"'", 'sch_mchotels');
			client.query(query, function(err, res){
				testingTools.code.expect(err).to.be.null();
				oldHotel = res.rows[0].data;
				doneDb();
				done();
			});
		});
	});
	lab.before(function(done){
		hgServices.getHotelGroup(hotelGroupId, function(err, hotelGroup){
			testingTools.code.expect(err).to.be.null();
			hotelGroupData = hotelGroup.data;
			done();
		});
	});
	lab.test('Mapping should go well', function(done){
		services.mapHotelData(htlJson, hotelGroupData, null, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.object();
			done();
		});
	});
	lab.test('Should work with missing ids in catmap', function(done){
		var hg2 = JSON.parse(JSON.stringify(hotelGroupData));
		hg2.categoryMap.testKey=['HUABZMP-TEST'];
		hg2.categoryMap.testKey2=['OTHERID'];
		services.mapHotelData(htlJson, hg2, null, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.object();
			done();
		});
	});


	lab.test('Mapping should return error with invalid json', function(done){
		services.mapHotelData({}, hotelGroupData, null, function(err){
			testingTools.expectError(err, 'int-tc-map-error');
			done();
		});
	});
	lab.test('Mapping should give upload error', function(done){
		var htlJson2 = JSON.parse(JSON.stringify(htlJson));
		htlJson2.TrustContent[0].PropertyInformation[0].GeneralInformation[0].PictureLinks[0].PictureLink[0].Link = 'thisisnotavalidlink';
		services.mapHotelData(htlJson2, hotelGroupData, null, function(err){
			testingTools.expectError(err, 'u-err-no-img');
			done();
		});
	});

	lab.test('should give error if coordinates are missing', {timeout: 20000}, function(done){
		var htlJson2 = JSON.parse(JSON.stringify(htlJson));
		delete htlJson2.TrustContent[0].PropertyInformation[0].Location;
		services.mapHotelData(htlJson2, hotelGroupId, null, function(err){
			testingTools.expectError(err, 'int-tc-map-error');
			testingTools.code.expect(err.data.debuggingData.key).to.include('TrustContent[0].PropertyInformation[0].Location');
			done();
		});

	});

	lab.test('Mapping should not fail if Recreation data missing', function(done){
		var htlJson2 = JSON.parse(JSON.stringify(htlJson));
		delete htlJson2.TrustContent[0].PropertyInformation[0].Recreation[0].Activity;
		services.mapHotelData(htlJson2, hotelGroupData, null, function(err, hotel, mapErrors){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hotel).to.be.object();
			testingTools.code.expect(mapErrors[0].data.key).to.equal('TrustContent[0].PropertyInformation[0].Recreation[0].Activity');

			done();
		});
	});

	lab.test('Mapping should not fail if PropertyInformation[0].GeneralInformation[0].PhoneNumber data missing', function(done){
		var htlJson2 = JSON.parse(JSON.stringify(htlJson));
		delete htlJson2.TrustContent[0].PropertyInformation[0].GeneralInformation[0].PhoneNumber;
		services.mapHotelData(htlJson2, hotelGroupData, null, function(err, hotel, mapErrors){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hotel).to.be.object();
			testingTools.code.expect(mapErrors[0].data.key).to.include('TrustContent[0].PropertyInformation[0].GeneralInformation[0].PhoneNumber[0]');

			done();
		});
	});

	lab.test('Mapping should not fail if PropertyInformation[0].GeneralInformation[0].PropertyAddress[0].Website data missing', function(done){
		var htlJson2 = JSON.parse(JSON.stringify(htlJson));
		delete htlJson2.TrustContent[0].PropertyInformation[0].GeneralInformation[0].PropertyAddress[0].Website;
		services.mapHotelData(htlJson2, hotelGroupData, null, function(err, hotel, mapErrors){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hotel).to.be.object();
			testingTools.code.expect(mapErrors[0].data.key).to.include('TrustContent[0].PropertyInformation[0].GeneralInformation[0].PropertyAddress[0].Website');

			done();
		});
	});

	lab.test('Mapping should not fail if GeneralInformation[0].SellingPoint[0].Text data missing', function(done){
		var htlJson2 = JSON.parse(JSON.stringify(htlJson));
		delete htlJson2.TrustContent[0].PropertyInformation[0].GeneralInformation[0].SellingPoint[0];
		services.mapHotelData(htlJson2, hotelGroupData, null, function(err, hotel, mapErrors){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hotel).to.be.object();
			testingTools.code.expect(mapErrors[0].data.key).to.include('TrustContent[0].PropertyInformation[0].GeneralInformation[0].SellingPoint[0].Text');

			done();
		});
	});

	lab.test('Mapping should not fail if GeneralInformation[0].PictureLinks[0].PictureLink data missing', function(done){
		var htlJson2 = JSON.parse(JSON.stringify(htlJson));
		delete htlJson2.TrustContent[0].PropertyInformation[0].GeneralInformation[0].PictureLinks[0].PictureLink;
		services.mapHotelData(htlJson2, hotelGroupData, null, function(err, hotel, mapErrors){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(hotel).to.be.object();
			testingTools.code.expect(mapErrors[0].data.key).to.equal('TrustContent[0].PropertyInformation[0].GeneralInformation[0].PictureLinks[0].PictureLink');

			done();
		});
	});

	lab.test('Mapping should go well if categoryMap not exists in hotel group', function(done){
		var htlJson2 = JSON.parse(JSON.stringify(htlJson));
		var hgData = JSON.parse(JSON.stringify(hotelGroupData));
		htlJson2.TrustContent[0].PropertyInformation[0].HotelCode = 'lhuk';
		hgData.categoryMap = null;

		services.mapHotelData(htlJson2, hgData, null, function(err, hotel){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(hotel).to.be.object();
			testingTools.code.expect(hotel.categoryIds).to.be.an.array().and.be.empty();
			done();
		});
	});

	lab.test('Mapping should go well if HotelCode exists in categoryMap', function(done){
		var htlJson2 = JSON.parse(JSON.stringify(htlJson));
		htlJson2.TrustContent[0].PropertyInformation[0].HotelCode = 'jacob';
		var hgData = JSON.parse(JSON.stringify(hotelGroupData));
		hgData.categoryMap.blabla = ['jacob'];
		services.mapHotelData(htlJson2, hgData, null, function(err, hotel){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(hotel).to.be.object();
			testingTools.code.expect(hotel.categoryIds).to.be.an.array().and.not.be.empty();
			testingTools.code.expect(hotel.categoryIds).to.include('blabla');
			done();
		});
	});

	lab.test('Mapping should go well if HotelCode not exists in categoryMap', function(done){
		var htlJson2 = JSON.parse(JSON.stringify(htlJson));
		htlJson2.TrustContent[0].PropertyInformation[0].HotelCode = 'test-hotel-code';
		services.mapHotelData(htlJson2, hotelGroupData, null, function(err, hotel){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(hotel).not.to.be.empty();

			done();
		});
	});

	lab.test('Mapping with existing hotel object', function(done){
		services.mapHotelData(htlJson, hotelGroupData, oldHotel, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.object();
			done();
		});

	});
});

lab.experiment('trustContent.upload.image', function(){
	var testGal = [
		{
			'source': {
				'origUrl': 'http://www.cfmedia.vfmleonardo.com/imageRepo/3/0/43/2/147/slh.com_P.jpg',
				'origDesc': 'Exterior View',
				'origCode': 'EXTV'
			}
		},
		{
			'source': {
				'origUrl': 'http://www.cfmedia.vfmleonardo.com/imageRepo/3/0/43/2/111/slh.com_(3)_P.jpg',
				'origDesc': 'Reception/Lobby',
				'origCode': 'RECP'
			}
		},
		{
			'source': {
				'origUrl': 'http://www.cfmedia.vfmleonardo.com/imageRepo/3/0/43/3/256/3_Reception_P.jpg',
				'origDesc': 'Reception/Lobby',
				'origCode': 'RECP'
			}
		}
	];
	var hotelId = 'test-upload-htlid';
	var hotelGroupId2 = 'test-upload-hgid';
	lab.test('Upload images should go well', {timeout: 10000}, function(done){
		services.uploadPictures(testGal, hotelGroupId2, hotelId, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.length).to.be.equal(3);
			done();
		});
	});
	lab.test('Upload images should give error', function(done){
		var brokenGal = [
			{
				'source': {
					'origUrl': 'thisisnotarealurl',
					'origDesc': 'Broken',
					'origCode': 'ERR'
				}
			}
		];
		services.uploadPictures(brokenGal, hotelGroupId2, hotelId, function(err){
			testingTools.expectError(err, 'u-err-no-img');
			done();
		});
	});
});

lab.experiment('TrustContent.UploadImage.CheckDiff', function(){
	var htlItemId;
	var htlJson2 = require(path.join(__dirname, 'existingimage/hotel.json'));
	var imageLinks = require(path.join(__dirname, 'existingimage/imagelinks.json'));
	lab.before(function(done){
		//Insert test hotel
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO hotel (data) VALUES(%L) RETURNING id', JSON.stringify(htlJson2));
			client.query(query, function(err, res){
				doneDb();
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				htlItemId = res.rows[0].id;
				done();
			});
		});

	});

	lab.after(function(done){
		//Delete test hotel
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM hotel WHERE id = %L', htlItemId);
			client.query(query, function(err, res){
				doneDb();
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				done();
			});
		});
	});

	lab.test('Upload should go well', {timeout: 20000}, function(done){
		services.uploadPictures(imageLinks, htlJson2.hotelGroup.hotelGroupId, htlJson2.hotelId, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res[0].source.md5.length).to.be.above(0);
			done();
		});
	});

	lab.test('Should upload again, id notel not found', {timeout: 20000}, function(done){
		services.uploadPictures(imageLinks, 'non-exists-hg', 'exists-htl', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res[0].source.md5.length).to.be.above(0);
			done();
		});
	});
});


lab.experiment('TrustContent.UploadImage.CheckDiff wo gallery', function() {
	var htlItemId;
	var htlJson2 = require(path.join(__dirname, 'existingimage/hotel.json'));
	var imageLinks = require(path.join(__dirname, 'existingimage/imagelinks.json'));
	lab.before(function(done) {
		//Insert test hotel
		delete htlJson2.localized[0].gallery;
		databaseUtils.getConnection(function(err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO hotel (data) VALUES(%L) RETURNING id', JSON.stringify(htlJson2));
			client.query(query, function(err, res) {
				doneDb();
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				htlItemId = res.rows[0].id;
				done();
			});
		});

	});

	lab.after(function(done) {
		//Delete test hotel
		databaseUtils.getConnection(function(err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM hotel WHERE id = %L', htlItemId);
			client.query(query, function(err, res) {
				doneDb();
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				done();
			});
		});
	});

	lab.test('Upload should go well even without existing gallery', {timeout: 20000}, function(done) {
		services.uploadPictures(imageLinks, htlJson2.hotelGroup.hotelGroupId, htlJson2.hotelId, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res[0].source.md5.length).to.be.above(0);
			done();
		});
	});
});

lab.experiment('TrustContent.UploadImage.CheckDiff.woDB', function(){
	testingTools.killDbConnectionForExperiment(lab);
	lab.test('Should give db error', function(done){
		services.uploadPictures([], '123', '123', function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});

lab.experiment('TrustContent.UploadImage.CheckDiff.woGallery', function(){
	var htlItemId;
	var htlJson2 = require(path.join(__dirname, 'existingimage/hotel.json'));
	delete htlJson2.gallery;
	var imageLinks = require(path.join(__dirname, 'existingimage/imagelinks.json'));
	lab.before(function(done){
		//Insert test hotel
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO hotel (data) VALUES(%L) RETURNING id', JSON.stringify(htlJson2));
			client.query(query, function(err, res){
				doneDb();
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				htlItemId = res.rows[0].id;
				done();
			});
		});
	});

	lab.after(function(done){
		//Delete test hotel
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM hotel WHERE id = %L', htlItemId);
			client.query(query, function(err, res){
				doneDb();
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				done();
			});
		});
	});
	lab.test('Upload should go well', {timeout: 20000}, function(done){
		services.uploadPictures(imageLinks, htlJson2.hotelGroup.hotelGroupId, htlJson2.hotelId, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res[0].source.md5.length).to.be.above(0);
			done();
		});
	});


});

lab.experiment('Import hotel', function(){
	var hotelData;
	var hotelGroup;
	var hotelId = 'testimporthotel';
	var hotelGroupId3 = 'testimportgroup';
	var chkGuid, valMeta, valHtl;

	lab.beforeEach(function(done){
		chkGuid = verDao.checkIfGuidExists;
		valMeta = verVal.validateMetaData;
		valHtl = htlVal.validateHotelData;

		done();
	});

	lab.afterEach(function(done){
		testingTools.disableEvents();
		verDao.checkIfGuidExists = chkGuid;
		verVal.validateMetaData = valMeta;
		htlVal.validateHotelData = valHtl;

		done();
	});

	lab.before(function(done){
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			//load hotel
			var query = escape("SELECT data FROM hotel WHERE (data ->'meta'->'guid') = '\"%s\"'", 'berna_test-hgid');
			client.query(query, function(err, res){
				testingTools.code.expect(err).to.be.null();
				hotelData = JSON.stringify(res.rows[0].data).replace(/berna/g, hotelId);
				hotelData = hotelData.replace(/test-hgid/g, hotelGroupId3);
				hotelData = JSON.parse(hotelData);
				hotelData.hotelGroup.hotelGroupId3 = hotelGroupId3;
				hotelData.meta.guid = hotelId + '_' + hotelGroupId3;
				//Loda hotel group
				query = escape("SELECT data FROM hotel_group WHERE (data ->'meta'->'guid') = '\"%s\"'", 'test-hgid');
				client.query(query, function(err, res){
					testingTools.code.expect(err).to.be.null();
					hotelGroup = JSON.stringify(res.rows[0].data).replace(/test-hgid/g, hotelGroupId3);
					hotelGroup = JSON.parse(hotelGroup);
					doneDb();
					done();
				});
			});
		});
	});

	lab.after(function(done){
		//delete test data
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape("DELETE FROM hotel_v WHERE (data ->'meta'->'guid') = '\"%s\"'", hotelId + '_' + hotelGroupId3);
			client.query(query, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(3);
				doneDb();
				done();
			});
		});
	});

	lab.test('New hotel should get an error', function(done){
		var hotel3 = JSON.parse(JSON.stringify(hotelData));
		hotel3.meta.status = 'unknownstatus';
		services.importHotel(hotel3, hotelGroup, function(err){
			testingTools.expectError(err, 'ver-inv-status');
			done();
		});
	});

	lab.test('Import hotel should go well', function(done){
		services.importHotel(hotelData, hotelGroup, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('Update existing hotel should go well without changes', function(done){
		services.importHotel(hotelData, hotelGroup, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.noChanges).to.equal(true);
			done();
		});
	});

	lab.test('Update existing hotel should go well with changes', function(done){
		hotelData.gallery = [];
		services.importHotel(hotelData, hotelGroup, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('Update hotel should return errors from listeners', function(done){
		testingTools.enableEvents();
		eventUtils.emitter.on('hotel.updated', function(data, callback){
			var e = new Error('354egfw');
			e.data = {};
			callback(e);
		});
		hotelData.categoryIds = [];
		services.importHotel(hotelData, hotelGroup, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('354egfw');
			done();
		});
	});

	lab.test('Check guid should fail', function(done){
		verDao.checkIfGuidExists = function(table, guid, cb){
			cb(new Error('guidfail'));
		};
		services.importHotel(hotelData, hotelGroup, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('guidfail');
			done();
		});
	});

	lab.test('Validate meta should fail', function(done){
		verVal.validateMetaData = function(meta, table, method, cb){
			cb(new Error('metafail'));
		};
		services.importHotel(hotelData, hotelGroup, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('metafail');
			done();
		});
	});

	lab.test('Validate data should fail', function(done){
		htlVal.validateHotelData = function(hotel, method, cb){
			cb(new Error('datafail'));
		};
		services.importHotel(hotelData, hotelGroup, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('datafail');
			done();
		});
	});

});

lab.experiment('Integration.TrustContent.Services.importTrustContentXml', function(){
	var hgId = 'slh';
	lab.test('should give error if parsing XML fails', function(done){
		services.importTrustContentXml('tere!!!', hgId, function(err){
			testingTools.expectError(err, 'int-tc-parse-error');
			done();
		});
	});

	lab.test('should give error if nonexisting hotelgroup id given', function(done){

		services.importTrustContentXml(htlXml, 'u89ads89', function(err){
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});

	lab.test('sould give error if hotel is black listed', {timeout: 5000}, function(done){

		hgServices.getHotelGroup(hotelGroupId, function(err, res){
			testingTools.code.expect(err).to.be.null();
			var failBlackListCheckXml = '<TrustContent><PropertyInformation ChainCode="LX" HotelCode="' + res.data.blacklistedHotels[0] + '"></PropertyInformation></TrustContent>';

			services.importTrustContentXml(failBlackListCheckXml, hotelGroupId, function(err){

				testingTools.expectError(err, 'int-tc-blckl-err');
				done();
			});

		});
	});
	lab.test('sould pass if no blacklist in hotelgroup and fail on mapping', {timeout: 5000}, function(done){
		var passBlackListCheckXml = '<TrustContent><PropertyInformation ChainCode="LX" HotelCode="NOTBLACKLISTED"></PropertyInformation></TrustContent>';

		services.importTrustContentXml(passBlackListCheckXml, 'test-hgid', function(err){
			testingTools.expectError(err, 'int-tc-map-error');
			done();
		});

	});
});

lab.experiment('Integration.Servces.ImportTrustContentXml', function(){
	testingTools.updateDbRowForExperiment(lab, 'hotel_group', function(row){
		delete row.data.blacklistedHotels;
	}, ['(data ->> \'hotelGroupId\') = %L', testingTools.getHotelGroupId()]);

	lab.test('sould pass if no blacklist at all in hotelgroup and fail on mapping', {timeout: 5000}, function(done){
		var passBlackListCheckXml = '<TrustContent><PropertyInformation ChainCode="LX" HotelCode="NOTBLACKLISTED"></PropertyInformation></TrustContent>';

		services.importTrustContentXml(passBlackListCheckXml, 'test-hgid', function(err){
			testingTools.expectError(err, 'int-tc-map-error');
			done();
		});
	});
});

lab.experiment('Integration.TrustContent.Services.importTrustContentXml with no DB connection', function(){
	var hgId = 'slh';
	testingTools.killDbConnectionForExperiment(lab);

	lab.test('should give error', function(done){

		services.importTrustContentXml(htlXml, hgId, function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});

lab.experiment('Integration.TrustContent.Services.importTrustContentXml with broken DB', {timeout: 10000}, function(){
	var hgId = 'slh';
	testingTools.failDbQueryForExperiment(lab, 'hotel_v');

	lab.test('should give error if importHotel fails (query fails)', function(done){

		services.importTrustContentXml(htlXml, hgId, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});

lab.experiment('Integration.TrustContent.Services.importTrustContentXml 2', {timeout: 20000}, function(){
	var hgId = 'slh';
	var importedHotelId = 'HUABZMP-TEST';
	lab.after(function(done){
		databaseUtils.getConnection(function(err, client, dbDone){
			testingTools.code.expect(err).to.be.null();
			client.query(
				"DELETE FROM hotel WHERE (data->'hotelId') = $1",
				['"' + importedHotelId + '"'],
				function(err){
					testingTools.code.expect(err).to.be.null();
					client.query(
						"DELETE FROM hotel_v WHERE (data->'hotelId') = $1",
						['"' + importedHotelId + '"'],
						function(err){
							dbDone();
							testingTools.code.expect(err).to.be.null();
							done();
						}
					);
				}
			);

		});

	});
	lab.test('should give error if mapping fails (xml invalid)', function(done){

		var passBlackListCheckXml = '<TrustContent><PropertyInformation ChainCode="LX" HotelCode="NOTBLACKLISTED"></PropertyInformation></TrustContent>';
		services.importTrustContentXml(passBlackListCheckXml, hgId, function(err){
			testingTools.expectError(err, 'int-tc-map-error');
			done();
		});
	});

	lab.test('should import the hotel without problems', function(done){

		services.importTrustContentXml(htlXml, hgId, function(err){
			testingTools.code.expect(err).to.be.null();
			databaseUtils.getConnection(function(err, client, dbDone){
				testingTools.code.expect(err).to.be.null();
				client.query(
					"SELECT * FROM hotel_v WHERE (data->'hotelId') = $1",
					['"' + importedHotelId + '"'],
					function(err, res){
						dbDone();
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(res.rows).to.have.length(1);
						done();
					}
				);
			});

		});
	});
});

lab.experiment('Integration.TrustContent.Services.copyFtpFilesToS3 with ftp connection problems', {timeout: 20000}, function(){
	var e1, e2, e3, e4, e5, f1, f2, f3;
	var folder = 'test-copyFtpFilesToS3';
	testingTools.clearS3FolderAfterExperiment(lab, folder);
	var ftpOpts = testingTools.createFtpServerForExperiment(lab);

	lab.before({timeout: 5000}, function(done){

		//delete one item
		var params = {Key: path.join(process.env.AWS_S3_FOLDER, folder, 'HUZTHPZ.xml')};
		s3.deleteObject(params, function(err, data){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(data).not.to.be.null();
			done();

		});
	});

	function pointToTestFtp(){
		process.env.TRUST_CONTENT_FTP_HOST = ftpOpts.host;
		process.env.TRUST_CONTENT_FTP_PORT = ftpOpts.port;
		process.env.TRUST_CONTENT_FTP_USER = ftpOpts.user;
		process.env.TRUST_CONTENT_FTP_PASS = ftpOpts.password;
		process.env.TRUST_CONTENT_FTP_FOLDER = '';
	}

	lab.before(function(done){
		e1 = process.env.TRUST_CONTENT_FTP_HOST;
		e2 = process.env.TRUST_CONTENT_FTP_PORT;
		e3 = process.env.TRUST_CONTENT_FTP_USER;
		e4 = process.env.TRUST_CONTENT_FTP_PASS;
		e5 = process.env.TRUST_CONTENT_FTP_FOLDER;
		f1 = services.importTrustContentXml;
		f2 = ftpClient.prototype.list;
		f3 = ftpClient.prototype.get;
		done();
	});

	lab.afterEach(function(done){
		process.env.TRUST_CONTENT_FTP_HOST = e1;
		process.env.TRUST_CONTENT_FTP_PORT = e2;
		process.env.TRUST_CONTENT_FTP_USER = e3;
		process.env.TRUST_CONTENT_FTP_PASS = e4;
		process.env.TRUST_CONTENT_FTP_FOLDER = e5;
		services.importTrustContentXml = f1;
		ftpClient.prototype.list = f2;
		ftpClient.prototype.get = f3;
		done();
	});

	lab.test('should give error about creating ftp connection', function(done){
		process.env.TRUST_CONTENT_FTP_HOST = 'kmlaslkmndss';
		services.copyFtpFilesToS3('test/bla', function(err){
			testingTools.expectError(err, 'int-tc-ftp-err');
			testingTools.code.expect(err.data.debuggingData.causedBy).to.equal('connection');
			done();
		});
	});

	lab.test('should give error about ftp listing', function(done){
		ftpClient.prototype.list = function(path, cb){
			cb(new Error('asdds234few'));
		};
		services.copyFtpFilesToS3(folder, function(err){
			testingTools.expectError(err, 'int-tc-ftp-err');
			testingTools.code.expect(err.data.debuggingData.causedBy).to.equal('list');
			testingTools.code.expect(err.data.debuggingData.originalError).to.equal('asdds234few');

			done();
		}, 1);
	});

	lab.test('should give error about ftp get', function(done){

		pointToTestFtp();

		ftpClient.prototype.get = function(path, cb){
			cb(new Error('asdasdq223aef'));
		};
		services.copyFtpFilesToS3(folder, function(err, results){
			testingTools.expectError(err, 'int-tc-ftp-get');
			testingTools.code.expect(err.data.debuggingData.causedBy).to.equal('get');
			testingTools.code.expect(err.data.debuggingData.originalError.message).to.equal('asdasdq223aef');

			testingTools.code.expect(results, JSON.stringify(results)).to.be.array();
			testingTools.code.expect(results[results.length - 1].error).to.equal(err.data.debuggingData.originalError);

			done();
		});
	});
});

lab.experiment('Integration.TrustContent.Services.copyFtpFilesToS3 with no network', {timeout: 60000}, function(){
	testingTools.mockResponsesForExperiment(lab);

	lab.test('should give error about listing s3 files', function(done){
		services.copyFtpFilesToS3('test/bla', function(err){
			testingTools.expectError(err, 'int-s3-list-err');
			done();
		}, 1);
	});
});

lab.experiment('Integration.TrustContent.Services.copyFtpFilesToS3 with bad network', {timeout: 60000}, function(){
	var e1, e2, e3, e4, e5;
	var ftpOpts = testingTools.createFtpServerForExperiment(lab);

	function pointToTestFtp(){
		process.env.TRUST_CONTENT_FTP_HOST = ftpOpts.host;
		process.env.TRUST_CONTENT_FTP_PORT = ftpOpts.port;
		process.env.TRUST_CONTENT_FTP_USER = ftpOpts.user;
		process.env.TRUST_CONTENT_FTP_PASS = ftpOpts.password;
		process.env.TRUST_CONTENT_FTP_FOLDER = '';
	}

	lab.before(function(done){
		e1 = process.env.TRUST_CONTENT_FTP_HOST;
		e2 = process.env.TRUST_CONTENT_FTP_PORT;
		e3 = process.env.TRUST_CONTENT_FTP_USER;
		e4 = process.env.TRUST_CONTENT_FTP_PASS;
		e5 = process.env.TRUST_CONTENT_FTP_FOLDER;
		done();
	});

	lab.afterEach(function(done){
		process.env.TRUST_CONTENT_FTP_HOST = e1;
		process.env.TRUST_CONTENT_FTP_PORT = e2;
		process.env.TRUST_CONTENT_FTP_USER = e3;
		process.env.TRUST_CONTENT_FTP_PASS = e4;
		process.env.TRUST_CONTENT_FTP_FOLDER = e5;
		done();
	});

	var call = 0;
	testingTools.mockResponsesForExperiment(lab, null, function(mitm){

		mitm.on('connect', function(socket){
			if(call++ === 1){
				socket.bypass();
			}
		});

		mitm.on('connection', function(socket){
			socket.destroy();
		});
	});

	lab.test('should give error about upload to s3', function(done){
		pointToTestFtp();
		services.copyFtpFilesToS3('downloaded/15-10-2015/', function(err){
			testingTools.expectError(err, 'int-s3-upl-err');
			done();
		}, 1);
	});
});

lab.experiment('Integration.TrustContent.Services.copyFtpFilesToS3', {timeout: 60000}, function(){
	var folder = 'downloaded/15-10-2015/';

	testingTools.clearS3FolderAfterExperiment(lab, folder);
	//lab.before({timeout: 5000}, function(done){
	//
	//	//delete one item
	//	var params = {Key: path.join(process.env.AWS_S3_FOLDER, folder, 'HUZTHPZ.xml')};
	//	s3.deleteObject(params, function(err, data){
	//		testingTools.code.expect(err).to.be.null();
	//		testingTools.code.expect(data).not.to.be.null();
	//		done();
	//
	//	});
	//});

	lab.test('should upload one missing file', function(done){

		services.copyFtpFilesToS3(folder, function(err, results){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(results, JSON.stringify(results)).to.be.array();
			testingTools.code.expect(results[results.length - 1].file.name).to.include('.xml');
			done();
		}, 1);
	});


	//lab.test('should find existing file', function(done){
	//
	//	services.copyFtpFilesToS3(folder, function(err, results){
	//		testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
	//		testingTools.code.expect(results, JSON.stringify(results)).to.be.array();
	//		testingTools.code.expect(results[results.length - 1].file.name).to.include('.xml');
	//		done();
	//	}, 1);
	//});

	lab.test('should upload nothing', function(done){

		services.copyFtpFilesToS3(folder, function(err, results){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(results, JSON.stringify(results)).to.be.array();
			testingTools.code.expect(results[results.length - 1].uploadRes, JSON.stringify(results)).to.equal(true);
			done();
		}, 0);
	});
});

lab.experiment('Integration.TrustContent.Services.startCoordinatedImportFromFtp', {timeout: 60000}, function(){
	var e1, e2, e3, e4, e5, f1, f2, f3, f4;
	var folder = 'downloaded/15-10-2015/';

	lab.before({timeout: 5000}, function(done){

		//delete one item
		var params = {Key: path.join(process.env.AWS_S3_FOLDER, folder, 'HUZTHPZ.xml')};
		s3.deleteObject(params, function(err, data){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(data).not.to.be.null();
			done();

		});
	});

	lab.before(function(done){
		e1 = process.env.TRUST_CONTENT_FTP_HOST;
		e2 = process.env.TRUST_CONTENT_FTP_PORT;
		e3 = process.env.TRUST_CONTENT_FTP_USER;
		e4 = process.env.TRUST_CONTENT_FTP_PASS;
		e5 = process.env.TRUST_CONTENT_FTP_FOLDER;
		f1 = services.importTrustContentXml;
		f2 = scheduler.executeJobAndWaitForCompletion;
		f3 = services.importFromS3;
		f4 = services.copyFtpFilesToS3;
		done();
	});

	lab.afterEach(function(done){
		process.env.TRUST_CONTENT_FTP_HOST = e1;
		process.env.TRUST_CONTENT_FTP_PORT = e2;
		process.env.TRUST_CONTENT_FTP_USER = e3;
		process.env.TRUST_CONTENT_FTP_PASS = e4;
		process.env.TRUST_CONTENT_FTP_FOLDER = e5;
		services.importTrustContentXml = f1;
		scheduler.executeJobAndWaitForCompletion = f2;
		services.importFromS3 = f3;
		services.copyFtpFilesToS3 = f4;
		done();
	});

	lab.test('should make the s3 folder automatically if not give', function(done){
		services.copyFtpFilesToS3 = function(folder, cb){
			var date = new Date();
			testingTools.code.expect(folder).to.include(path.join('downloaded', date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear()));
			var e = new Error('09rfnjkweopu90ehiog');
			e.data = {};
			cb(e);
		};

		services.startCoordinatedImportFromFtp('slh', function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('09rfnjkweopu90ehiog');
			done();

		});

	});

	lab.test('should give error if ftp copy fails', function(done){
		process.env.TRUST_CONTENT_FTP_HOST = 'localhoistblabla';
		services.startCoordinatedImportFromFtp('slh', function(err){
			testingTools.expectError(err, 'int-tc-ftp-err');
			testingTools.code.expect(err.data.retry).to.equal(true);
			done();

		}, null, folder);
	});

	lab.test('should not limit items, if limit not set', function(done){
		services.copyFtpFilesToS3 = function(folder, cb){
			cb(null, [{s3Key: folder + 'HUZTHPZ.xml'}, {s3Key: folder + 'HUZTHPZ.xml'}]);
		};
		var called = 0;

		scheduler.executeJobAndWaitForCompletion = function(job, cb){
			called++;
			cb(new Error('09iasdnjkas234234dkmle3'));
		};

		services.startCoordinatedImportFromFtp('slh', function(err, results){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(results).to.have.length(1);
			testingTools.code.expect(results[0].files).to.have.length(2);
			testingTools.code.expect(results[0].files).to.have.length(2);
			testingTools.code.expect(called).to.equal(1);
			done();

		});

	});

	lab.test('should give error if scheduling trustContentImportPart job fails', function(done){

		scheduler.executeJobAndWaitForCompletion = function(job, cb){
			cb(new Error('09iasdnjkasdkmle3'));
		};

		services.startCoordinatedImportFromFtp('slh', function(err, result){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(result[0].error.message).to.equal('09iasdnjkasdkmle3');
			done();

		}, 1, folder);
	});

	lab.test('should forward errors from importFromS3', function(done){

		scheduler.executeJobAndWaitForCompletion = function(job, cb){
			cb(null, {response: {importResults: [{error: new Error('89adsfdfnajknjr4908')}]}});
		};

		services.startCoordinatedImportFromFtp('slh', function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.array();
			testingTools.code.expect(res[0]).to.be.object();
			testingTools.code.expect(res[0].result.response.importResults[0].error.message).to.equal('89adsfdfnajknjr4908');
			done();
		}, 1, folder);

	});

	lab.test('should forward mapping errors from importFromS3', function(done){

		scheduler.executeJobAndWaitForCompletion = function(job, cb){
			cb(null, {response: {importResults: [{mappingErrors: [new Error('78yr23kjnds')]}]}});
		};

		services.startCoordinatedImportFromFtp('slh', function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.array();
			testingTools.code.expect(res[0]).to.be.object();
			testingTools.code.expect(res[0].result.response.importResults[0].mappingErrors[0].message).to.equal('78yr23kjnds');
			done();
		}, 1, folder);

	});

	lab.test('should import 2 hotels', function(done){
		services.startCoordinatedImportFromFtp('slh', function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.array();
			testingTools.code.expect(res[0]).to.be.object();
			testingTools.code.expect(res[0].files).to.be.array();
			testingTools.code.expect(res[0].files).to.have.length(2);
			done();
		}, 2, folder);

	});
});

lab.experiment('Integration.TrustContent.Services.importFromS3', function(){
	lab.test('should work without any files', function(done){
		services.importFromS3(
			'slh',
			function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.importResults).to.be.empty();
				done();
			},
			0, 1000, []
		);

	});

	lab.test('should give error for invalid file', function(done){
		services.importFromS3(
			'slh',
			function(err, result){
				testingTools.code.expect(err).to.be.null();
				testingTools.expectError(result.importResults[0].error, 'int-s3-get-err');
				done();
			},
			0, 1000, ['90uadsu90ads9u0das']
		);

	});

	lab.test('should give error if import fails', function(done){
		services.importFromS3(
			'j98asd89sd89udsa',
			function(err, result){
				testingTools.code.expect(err).to.be.null();
				testingTools.expectError(result.importResults[0].error, 'hg-not-found');
				done();
			},
			0, 1, [path.join(process.env.AWS_S3_FOLDER, 'downloaded/15-10-2015/HUABZMP.xml')]
		);

	});

	lab.test('should give error for second file and not import third valid file', {timeout: 20000}, function(done){
		var fname = path.join(process.env.AWS_S3_FOLDER, 'downloaded/15-10-2015/HUABZMP.xml');
		services.importFromS3(
			'slh',
			function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.importResults).to.have.length(1);
				testingTools.expectError(res.importResults[0].error, 'int-s3-get-err');
				testingTools.code.expect(res.importResults[0].error.data.debuggingData.key).to.equal('klasdkadsjasd');
				done();
			},
			1, 12, [fname, 'klasdkadsjasd']
		);

	});

	lab.test('should work and import 1 file', {timeout: 20000}, function(done){
		var fname = path.join(process.env.AWS_S3_FOLDER, 'downloaded/15-10-2015/HUABZMP.xml');
		services.importFromS3(
			'slh',
			function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.importResults).to.have.length(1);
				testingTools.code.expect(res.importResults[0].file).to.equal(fname);
				done();
			},
			0, 1, [fname]
		);

	});
});

lab.experiment('Integration.Services.MapHotelHtml', function(){
	var _uploadPicture, _makeHttpRequest, lw;
	var inputJson = require('./htmlimport/HUTLLTE.json');
	//var picJson = require('./htmlimport/pictures.json');
	var picText = fs.readFileSync(path.join(__dirname, 'htmlimport/pictures.txt')).toString();
	var inputHtml = fs.readFileSync(path.join(__dirname, 'htmlimport/HUTLLTE.html')).toString();
	var inputHtml2 = fs.readFileSync(path.join(__dirname, 'htmlimport/HUTLLTE_2.html')).toString();
	var hotelGroup = require('./htmlimport/hg.json');
	lab.before(function(done){
		lw = logging.warning;
		_uploadPicture = media.uploadHotelImage;
		_makeHttpRequest = services.makeHttpRequest;
		done();
	});
	lab.afterEach(function(done){
		logging.warning = lw;
		media.uploadHotelImage = _uploadPicture;
		services.makeHttpRequest = _makeHttpRequest;
		done();
	});
	lab.test('Html mapping should go well', {timeout: 30000}, function(done){
		services.makeHttpRequest = function(options, cb){
			cb(null, picText);
		};
		services.mapHotelHtml(inputHtml, inputJson, hotelGroup, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.localized[0].description.length).to.be.equals(11);
			htlVal.validateHotelData(res, 'update', function(err){
				testingTools.code.expect(err).to.be.null();
				done();
			}, hotelGroup);

		});
	});

	lab.test('Html mapping should go well with empty uniquer qualities', {timeout: 30000}, function(done){
		services.makeHttpRequest = function(options, cb){
			cb(null, picText);
		};
		services.mapHotelHtml(inputHtml2, inputJson, hotelGroup, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.localized[0].description.length).to.be.equals(11);
			htlVal.validateHotelData(res, 'update', function(err){
				testingTools.code.expect(err).to.be.null();
				done();
			}, hotelGroup);

		});
	});

	lab.test('Html mapping should go well without rooms empty uniquer qualities', {timeout: 30000}, function(done){
		services.makeHttpRequest = function(options, cb){
			cb(null, picText);
		};
		services.mapHotelHtml(inputHtml2.replace('The rooms', 'vblavlal'), inputJson, hotelGroup, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.localized[0].description)
				.not.deep.include({heading: [{style: null, value: 'The rooms'}]});
			done();

		});
	});

	lab.test('Upload picture failure', {timeout: 30000}, function(done){
		services.makeHttpRequest = function(options, cb){
			cb(null, picText);
		};
		media.uploadHotelImage = function(gallery, hotelGroupId, hotelId, config, cb){
			cb(new Error('uploadpicturefailure'));
		};
		services.mapHotelHtml(inputHtml, inputJson, hotelGroup, function(err){
			testingTools.code.expect(err.message).to.be.equal('uploadpicturefailure');
			done();
		});
	});

	lab.test('Upload picture warning', {timeout: 30000}, function(done){
		services.makeHttpRequest = function(options, cb){

			cb(null, picText.replace('"rcoId":"28013451","rcoType":"photo"', '"rcoId":"28013451","rcoType":"allvideo"'));
		};
		media.uploadHotelImage = function(gallery, hotelGroupId, hotelId, config, cb){
			cb(null, {fake: true});
		};
		var warnings = 0;
		logging.warning = function(a1){
			testingTools.code.expect(a1).to.equal('This image will be skipped');
			warnings++;
		};
		services.mapHotelHtml(inputHtml, inputJson, hotelGroup, function(err){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(warnings).to.equal(1);
			done();
		});
	});

	lab.test('Parse imageobject failure', function(done){
		services.makeHttpRequest = function(option, cb){
			cb(null, '');
		};
		services.mapHotelHtml(inputHtml, inputJson, hotelGroup, function(err){
			testingTools.expectError(err, 'int-img-err');
			done();
		});
	});

	lab.test('Html mapping wo category map', {timeout: 30000}, function(done){
		services.makeHttpRequest = function(options, cb){
			cb(null, picText);
		};
		var hotelGroup2 = JSON.parse(JSON.stringify(hotelGroup));
		delete hotelGroup2.categoryMap;
		services.mapHotelHtml(inputHtml2, inputJson, hotelGroup2, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.localized[0].description.length).to.be.equals(11);
			htlVal.validateHotelData(res, 'update', function(err){
				testingTools.code.expect(err).to.be.null();
				done();
			}, hotelGroup);

		});
	});

	lab.test('Html mapping with invalid html', {timeout: 30000}, function(done){
		var hotelGroup2 = JSON.parse(JSON.stringify(hotelGroup));
		delete hotelGroup2.categoryMap;
		services.mapHotelHtml('Page not found!', inputJson, hotelGroup2, function(err){
			testingTools.expectError(err, 'int-web-map-err');
			done();
		});
	});
});

lab.experiment('Importer.MapHtmlHttpError', function(){
	var _httpRequest;
	var inputJson = require('./htmlimport/HUTLLTE.json');
	var inputHtml = fs.readFileSync(path.join(__dirname, 'htmlimport/HUTLLTE.html')).toString();
	var hotelGroup = require('./htmlimport/hg.json');

	lab.before(function(done){
		_httpRequest = services.makeHttpRequest;
		done();
	});
	lab.afterEach(function(done){
		services.makeHttpRequest = _httpRequest;
		done();
	});

	lab.test('Shold give http failure', {timeout: 100000}, function(done){
		services.makeHttpRequest = function(options, cb){
			cb(new Error('httprequestfailure'));
		};
		services.mapHotelHtml(inputHtml, inputJson, hotelGroup, function(err){
			testingTools.code.expect(err.message).to.be.equal('httprequestfailure');
			done();
		});
	});
});

lab.experiment('Importer.Http request', function(){

	var options = {
		host: 'www.slh.com',
		path: '/header/destinations/',
		method: 'GET',
		timeout: 50000
	};
	lab.afterEach(function(done){
		options.timeout = 50000;
		done();
	});

	lab.test('Should work', {timeout: 10000}, function(done){
		services.makeHttpRequest(options, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		}, true);
	});

	lab.test('Http error', function(done){
		var options2 = JSON.parse(JSON.stringify(options));
		options2.host = 'someunknownhost';
		services.makeHttpRequest(options2, function(err){
			testingTools.expectError(err, 'int-comm-err');
			done();
		});
	});

	lab.test('Http timeout', function(done){
		options.timeout = 1;
		services.makeHttpRequest(options, function(err){
			testingTools.expectError(err, 'int-comm-err');
			done();
		}, true);
	});
});

lab.experiment('Integration.TrustContent.Services.copySlhWebsiteHtmlToS3', {timeout: 600000}, function(){
	var called = 0;
	var s3Folder = 'test-' + new Date().toISOString();
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 503;
		res.end(fs.readFileSync(path.join(__dirname, '/allSlhHotels.json')), 'utf8');
	}, function(mitm){
		mitm.on('connect', function(socket){
			if(called++){
				socket.bypass();
			}
		});
	});

	testingTools.clearS3FolderAfterExperiment(lab, s3Folder);

	lab.test('should copy one item', function(done){
		services.copySlhWebsiteHtmlToS3(s3Folder, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.hotelResults).to.be.object();
			testingTools.code.expect(Object.keys(res.hotelResults)).to.have.length(1);
			done();
		}, 1);

	});
});

lab.experiment('Integration.TrustContent.Services.copySlhWebsiteHtmlToS3 with no network', {timeout: 600000}, function(){
	var s3Folder = 'test-' + new Date().toISOString();
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm){
		mitm.on('connection', function(socket){
			socket.destroy();
		});
	});

	lab.test('should give error', function(done){
		services.copySlhWebsiteHtmlToS3(s3Folder, function(err){
			testingTools.expectError(err, 'int-comm-err');
			done();
		}, 1);

	});
});

lab.experiment('Integration.TrustContent.Services.copySlhWebsiteHtmlToS3 with broken slh.com', {timeout: 600000}, function(){
	var s3Folder = 'test-' + new Date().toISOString();
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 200;
		res.end('Is this where I parked my car?', 'utf8');
	});

	lab.test('should give error', function(done){
		services.copySlhWebsiteHtmlToS3(s3Folder, function(err){
			testingTools.expectError(err, 'int-web-js-pars');
			done();
		}, 1);

	});
});

lab.experiment('Integration.TrustContent.Services.copySlhWebsiteHtmlToS3 with crazy slh.com', {timeout: 600000}, function(){
	var s3Folder = 'test-' + new Date().toISOString();
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 200;
		res.end(fs.readFileSync(path.join(__dirname, '/allSlhHotels.json'), 'utf8').replace('Hôtel Juana', 'H��tel Juana'));
	});

	lab.test('should give error', function(done){
		services.copySlhWebsiteHtmlToS3(s3Folder, function(err){
			testingTools.expectError(err, 'int-web-js-pars');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('Invalid character in JSON');
			done();
		}, 1);

	});
});

lab.experiment('Integration.TrustContent.Services.copySlhWebsiteHtmlToS3 with no connection to s3', {timeout: 100000}, function(){
	var called = 0;
	var s3Folder = 'test-' + new Date().toISOString();
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 503;
		res.end(fs.readFileSync(path.join(__dirname, '/allSlhHotels.json')), 'utf8');
	}, function(mitm){
		mitm.on('connection', function(socket){
			if(called++){
				socket.destroy();
			}
		});
	});

	lab.test('should give error', function(done){
		services.copySlhWebsiteHtmlToS3(s3Folder, function(err){
			testingTools.expectError(err, 'int-s3-list-err');
			done();
		}, 1);

	});
});

lab.experiment('Integration.TrustContent.Services.copySlhWebsiteHtmlToS3 with error uploading to s3', {timeout: 100000}, function(){
	var called = 0;
	var s3Folder = 'test-' + new Date().toISOString();
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 503;
		res.end(fs.readFileSync(path.join(__dirname, '/allSlhHotels.json')), 'utf8');
	}, function(mitm){
		mitm.on('connect', function(socket){
			if(called++ === 1){
				socket.bypass();
			}
		});
		mitm.on('connection', function(socket, opts){

			if(opts.host.indexOf('.s3.') > -1){
				socket.destroy();
			}
		});
	});

	lab.test('should give error', function(done){
		services.copySlhWebsiteHtmlToS3(s3Folder, function(err){
			testingTools.expectError(err, 'int-s3-upl-err');
			done();
		}, 1);

	});
});

lab.experiment('Integration.TrustContent.Services.copySlhWebsiteHtmlToS3 with error communicating with slh.com', {timeout: 100000}, function(){
	var called = 0;
	var s3Folder = 'test-' + new Date().toISOString();
	var testFile = fs.readFileSync(path.join(__dirname, '/allSlhHotels.json'), 'utf8');
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 503;
		res.end(testFile);
	}, function(mitm){
		mitm.on('connect', function(socket, opts){
			if(opts.host.indexOf('slh.com') < 0){
				socket.bypass();
			}
		});
		mitm.on('connection', function(socket, opts){

			if(called++ > 0 && opts.host.indexOf('slh.com') > -1){
				socket.destroy();
			}
		});
	});

	lab.test('should give error', function(done){
		services.copySlhWebsiteHtmlToS3(s3Folder, function(err){
			testingTools.expectError(err, 'int-web-get-f');
			done();
		});

	});
});

lab.experiment('Integration.TrustContent.Services.startCoordinatedImportFromWebsite', {timeout: 10000}, function(){
	var called = 0, f1;

	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 503;
		res.end(fs.readFileSync(path.join(__dirname, '/allSlhHotels.json')), 'utf8');
	}, function(mitm){
		mitm.on('connect', function(socket){
			if(called++){
				socket.bypass();
			}
		});
	});

	lab.before(function(done){
		f1 = htlServices.getPublishedHotelIds;
		done();
	});

	lab.afterEach(function(done){
		htlServices.getPublishedHotelIds = f1;
		done();
	});

	lab.test('should not work with other hotel groups', function(done){
		services.startCoordinatedImportFromWebsite('asdsad', function(err){
			testingTools.expectError(err, 'int-inv-hg');
			done();
		}, 0);
	});

	lab.test('Only listed hotels hocus-pocus', {timeout: 30000}, function(done){
		services.startCoordinatedImportFromWebsite('slh', function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		}, 2, 'test1', false, ['HUCNXPH']);
	});


	lab.test('should work and import 0 new hotels', {timeout: 30000}, function(done){
		called = 0;
		services.startCoordinatedImportFromWebsite('slh', function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		}, 0, null, true);

	});

	lab.test('should give error trying to import 0 new hotels', {timeout: 30000}, function(done){
		called = 0;
		htlServices.getPublishedHotelIds = function(h, cb){
			cb(new Error('getPublishedHotelIdsErr'));
		};
		services.startCoordinatedImportFromWebsite('slh', function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('getPublishedHotelIdsErr');
			done();
		}, 0, null, true);
	});

	lab.test('should work and import 1 hotel', {timeout: 60000}, function(done){
		called = 0;
		services.startCoordinatedImportFromWebsite('slh', function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		}, 1, 'test1');

	});
});

lab.experiment('Integration.TrustContent.Services.startCoordinatedImportFromWebsite with failing parts', {timeout: 10000}, function(){
	var f1, f2, f3, f4, f5;

	var called = 0;

	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 503;
		res.end(fs.readFileSync(path.join(__dirname, '/allSlhHotels.json')), 'utf8');
	}, function(mitm){
		mitm.on('connect', function(socket){
			if(called++){
				socket.bypass();
			}
		});
	});

	lab.before(function(done){
		f1 = services.copySlhWebsiteHtmlToS3;
		f2 = jobHandler.createJob;
		f3 = scheduler.executeJobAndWaitForCompletion;
		f4 = services.mapHotelHtml;
		f5 = services.importHotel;
		done();
	});

	lab.afterEach(function(done){
		services.copySlhWebsiteHtmlToS3 = f1;
		jobHandler.createJob = f2;
		scheduler.executeJobAndWaitForCompletion = f3;
		services.mapHotelHtml = f4;
		services.importHotel = f5;

		done();
	});

	lab.test('if web to s3 copy fails it should give retry error', function(done){
		called = 0;
		services.copySlhWebsiteHtmlToS3 = function(f, cb){
			var e = new Error('34sdg3gs4wrsdf');
			e.data = {};
			cb(e);
		};

		services.startCoordinatedImportFromWebsite('slh', function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('34sdg3gs4wrsdf');
			testingTools.code.expect(err.data.retry).to.equal(true);
			done();
		}, 2, 'test1');
	});

	lab.test('if createJob fails it should report it', function(done){
		called = 0;
		services.copySlhWebsiteHtmlToS3 = function(f, cb){
			cb(null, {hotelResults: {testHotel: {
				json: {
					error: null,
					s3Key: 'some/s3/key',
					uploadResult: null
				},
				html: {
					error: null,
					s3Key: 'some/s3/key2',
					uploadResult: null
				}
			}}});
		};
		jobHandler.createJob = function(){
			arguments[arguments.length - 1](new Error('34sdg3gs4sdfdfswrsdf'));
		};

		services.startCoordinatedImportFromWebsite('slh', function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res[0].error.message).to.equal('34sdg3gs4sdfdfswrsdf');
			done();
		}, null, 'test1');
	});

	lab.test('it should report if job exec fails', function(done){
		called = 0;
		services.copySlhWebsiteHtmlToS3 = function(f, cb){
			cb(null, {hotelResults: {testHotel: {
				json: {
					error: null,
					s3Key: 'some/s3/key',
					uploadResult: null
				},
				html: {
					error: null,
					s3Key: 'some/s3/key2',
					uploadResult: null
				}
			}}});
		};
		scheduler.executeJobAndWaitForCompletion = function(job, cb){
			cb(new Error('90sfdnj340i9sdofmg'));
		};

		services.startCoordinatedImportFromWebsite('slh', function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res[0].error.message).to.equal('90sfdnj340i9sdofmg');
			done();
		}, null, 'test1');
	});

	lab.test('it should report data mapping errors, like expected', {timeout: 20000}, function(done){
		called = 0;

		services.mapHotelHtml = function(html, json, hg, cb){
			cb(new Error('98usdfnjsdfn98hrwf'));
		};

		scheduler.executeJobAndWaitForCompletion = function(job, cb){
			services.importWebFromS3(job.input.hotelGroupId, function(err, res){
				cb(err, {response: res});
			}, job.input.hotels);
		};

		services.startCoordinatedImportFromWebsite('slh', function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();

			testingTools.code.expect(res[0].result.response.importResults[0].error.message).to.equal('98usdfnjsdfn98hrwf');
			done();
		}, 1, 'test-' + new Date().getTime());
	});

	lab.test('it should work like expected', {timeout: 30000}, function(done){
		called = 0;

		services.importHotel = function(h, hg, cb){
			cb(null);
		};

		scheduler.executeJobAndWaitForCompletion = function(job, cb){
			services.importWebFromS3(job.input.hotelGroupId, function(err, res){
				cb(err, {response: res});
			}, job.input.hotels);
		};

		services.startCoordinatedImportFromWebsite('slh', function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			res[0].result.response.importResults.forEach(function(ir){
				testingTools.code.expect(ir.error, JSON.stringify(ir.error)).to.be.null();
			});
			done();

		}, 1, 'test1');
	});

});

lab.experiment('Integration.TrustContent.Services.importWebFromS3', function(){
	lab.test('should return download errors', function(done){
		services.importWebFromS3('slh', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.expectError(res.importResults[0].error, 'int-s3-get-err');
			testingTools.code.expect(res.importResults[0].error.data.debuggingData.key).to.equal('blabla');
			done();
		}, {slh: {json: {s3Key: 'blabla'}}});

	});

	lab.test('should return download errors', function(done){
		services.importWebFromS3('slh', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.expectError(res.importResults[0].error, 'int-s3-get-err');
			testingTools.code.expect(res.importResults[0].error.data.debuggingData.key).to.equal('blabla');
			done();
		}, {slh: {json: {s3Key: 'blabla'}}});

	});
});

lab.experiment('Integration.TrustContent.Services.importFromWebsite', function(){

	var f2;

	lab.before(function(done){
		f2 = services.mapHotelHtml;
		done();
	});

	lab.afterEach(function(done){
		services.mapHotelHtml = f2;
		done();
	});

	lab.test('should fail with invalid hotel group', function(done){
		services.importFromWebsite({}, 'shdfdsf', function(err){
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});

	lab.test('should fail with invalid json', function(done){
		services.importFromWebsite({json: 'asdasd'}, 'slh', function(err){
			testingTools.expectError(err, 'int-web-js-pars');
			done();
		});
	});

	lab.test('should fail if mapping fails', function(done){

		services.mapHotelHtml = function(h, j, d, cb){
			cb(new Error('89sdfnijsdfh8934g'));
		};

		services.importFromWebsite({json: '{}'}, 'slh', function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('89sdfnijsdfh8934g');
			done();
		});
	});

	lab.test('should fail if validation/import fails', function(done){

		services.mapHotelHtml = function(h, j, d, cb){
			cb(null, {meta: {}});
		};

		services.importFromWebsite({json: '{}'}, 'slh', function(err){
			testingTools.expectError(err, 'hv-key-miss');
			done();
		});
	});

});

lab.experiment('Integration.TrustContent.Services.updateHotelBlackList with filled blacklist', function(){

	var f1;

	lab.before(function(done){
		f1 = hgServices.updateHotelGroup;
		hgServices.updateHotelGroup = function(d, cb){
			cb(null);
		};
		done();
	});

	lab.after(function(done){
		hgServices.updateHotelGroup = f1;
		done();
	});

	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.end(fs.readFileSync(path.join(__dirname, 'allSlhHotels.json')).toString().replace('HULOPSS', 'berna'));
	});

	testingTools.updateDbRowForExperiment(lab, 'hotel_group', function(row){
		row.data.blacklistedHotels = [new Date().toISOString(), 'berna', 'corinthia', 'ernst', 'fairmont', 'greenwich', 'jacob',
			'kempinski', 'langham', 'lowell', 'midas', 'pierre'];
	}, ['(data -> \'hotelGroupId\') = %L', '"test-hgid"']);

	lab.test('should work and put hotels to blacklist for test-hgid group', function(done){
		services.updateHotelBlackList('test-hgid', function(err, blacklist){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(blacklist).to.be.array();
			testingTools.code.expect(blacklist).to.have.length(20);
			testingTools.code.expect(blacklist).not.to.include('berna');
			done();
		});
	});

	lab.test('should work and add 0 hotels', function(done){
		services.updateHotelBlackList('test-hgid', function(err, blacklist){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(blacklist).to.be.array();
			testingTools.code.expect(blacklist).to.have.length(20);
			done();
		});
	});

	lab.test('should give error for invalid hgid', function(done){
		services.updateHotelBlackList('test-hgidasdadsd', function(err){
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});

});

lab.experiment('Integration.TrustContent.Services.updateHotelBlackList with no changes', function(){

	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.end(JSON.stringify(
			{

				'query': '',
				'currentPage': 1,
				'totalpages': 1,
				'totalResults': 259,
				'sortCode': 'a-z',
				'results': {
					'hotels': [
						{
							'hotelcode': 'plaza'
						}, {
							'hotelcode': 'raffles'
						}, {
							'hotelcode': 'schlossle'
						}, {
							'hotelcode': 'sofitel'
						}, {
							'hotelcode': 'stpeter'
						}, {
							'hotelcode': 'sukhothai'
						}, {
							'hotelcode': 'th-mch'
						}, {
							'hotelcode': 'wangfujing'
						}, {
							'hotelcode': 'wesenbergh'
						}
					]
				}
			}
		));
	});

	testingTools.updateDbRowForExperiment(lab, 'hotel_group', function(row){
		row.data.blacklistedHotels = [new Date().toISOString(), 'berna', 'corinthia', 'ernst', 'fairmont', 'greenwich', 'jacob',
			'kempinski', 'langham', 'lowell', 'midas', 'pierre'];
	}, ['(data -> \'hotelGroupId\') = %L', '"test-hgid"']);

	lab.test('should work and add 0 hotels', function(done){
		services.updateHotelBlackList('test-hgid', function(err, blacklist){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(blacklist).to.be.array();
			testingTools.code.expect(blacklist).to.have.length(12);
			done();
		});
	});

});

lab.experiment('Integration.TrustContent.Services.updateHotelBlackList with empty blacklist', function(){

	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.end(fs.readFileSync(path.join(__dirname, 'allSlhHotels.json')).toString().replace('HUCNXPH', 'berna'));
	});

	testingTools.updateDbRowForExperiment(lab, 'hotel_group', function(row){
		delete row.data.blacklistedHotels;
	}, ['(data -> \'hotelGroupId\') = %L', '"test-hgid"']);

	lab.test('should give error about adding too much', function(done){
		services.updateHotelBlackList('test-hgid', function(err){
			testingTools.expectError(err, 'bl-update-cnd');
			done();
		});
	});

});

lab.experiment('Integration.TrustContent.Services.updateHotelBlackList with network problems', function(){

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm){
		mitm.on('connection', function(socket){
			socket.destroy();
		});
	});

	lab.test('should report error', function(done){
		services.updateHotelBlackList('test-hgid', function(err){
			testingTools.expectError(err, 'bl-req-fail');
			done();
		});
	});

});

lab.experiment('Integration.TrustContent.Services.updateHotelBlackList with broken slh.com', function(){

	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.end('ksndfiofsfdfs');
	});

	lab.test('should work and put all hotels to blacklist for test-hgid group', function(done){
		services.updateHotelBlackList('test-hgid', function(err){
			testingTools.expectError(err, 'bl-req-pars-err');
			done();
		});
	});

});

lab.experiment('Integration.TrustContent.Services.updateHotelBlackList with broken db', function(){

	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.end(fs.readFileSync(path.join(__dirname, 'allSlhHotels.json')).toString());
	});

	testingTools.failDbQueryForExperiment(lab, 'hotel');

	lab.test('should give error on save', function(done){
		services.updateHotelBlackList('test-hgid', function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});

});

lab.experiment('Integration.TrustContent.Services.updateHotelBlackList with broken db', function(){

	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.end(fs.readFileSync(path.join(__dirname, 'allSlhHotels.json')).toString());
	});

	testingTools.updateDbRowForExperiment(lab, 'hotel_group', function(row){
		row.data.blacklistedHotels = [new Date().toISOString(), 'berna', 'corinthia', 'ernst', 'fairmont', 'greenwich', 'jacob',
			'kempinski', 'langham', 'lowell', 'midas', 'pierre'];
	}, ['(data -> \'hotelGroupId\') = %L', '"test-hgid"']);

	testingTools.failDbQueryForExperiment(lab, 'hotel_group_v');

	lab.test('should give error on save', function(done){
		services.updateHotelBlackList('test-hgid', function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});

});
