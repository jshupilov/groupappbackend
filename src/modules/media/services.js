'use strict';
var cloudinary = require('cloudinary');
var path = require('path');
var errors = require('./../utils/errors');
var crypto = require('./../utils/cryptoUtils');
var fs = require('fs');
var httpUtils = require('../utils/httpUtils');

errors.defineError('iu-err-up-img', 'Error uploading image', 'Error occurred while uploading the image to CDN', 'Media');
errors.defineError('iu-err-no-img', 'Error uploading image', 'The image was not found in given path', 'Media');
errors.defineError('iu-inv-img-obj', 'Error getting image', 'Invalid image object passed fot parsing', 'Media');
errors.defineError('iu-err-del-imgs', 'Error deleting images', 'Error occurred, trying to delete images', 'Media');
errors.defineError('iu-err-inv-mc', 'Error getting images', 'Invalid media configuration given', 'Media');
errors.defineError('iu-err-inv-tr', 'Error getting images', 'Invalid transformation given', 'Media');
errors.defineError('iu-err-inv-tr-tp', 'Error getting images', 'Invalid transformation type given', 'Media');
errors.defineError('u-err-no-img', 'Error comparing images', 'Error when comparing images', 'Media');

/**
 * Get base folder name in cloudinary
 * @returns {string}
 */
function getBaseFolderName() {
	return path.join(process.env.CLOUDINARY_BASE_FOLDER, 'group-app');
}

/**
 * Get hotel Group base folder name
 * @param hotelGroupId
 * @returns {*|string}
 */
function getHotelGroupBaseFolderName(hotelGroupId) {
	return path.join(getBaseFolderName(), hotelGroupId);
};

exports.getHotelGroupBaseFolderName = getHotelGroupBaseFolderName;

/**
 * Get hotel base folder name
 * @param hotelGroupId
 * @param hotelId
 * @returns {*|string}
 */
function getHotelBaseFolderName(hotelGroupId, hotelId) {
	return path.join(getHotelGroupBaseFolderName(hotelGroupId), 'hotels', hotelId);
};

exports.getHotelBaseFolderName = getHotelBaseFolderName;

function parseTransformation(transformation, mediaConfiguration) {
	var transf = {};
	for(var i in transformation) {
		transf[i] = transformation[i];
	}
	if(
		transf.overlay
		&& typeof transf.overlay === 'string'
		&& transf.overlay.indexOf('extra_assets') === 0
	) {
		if(!mediaConfiguration.hotelGroupId) {
			throw errors.newError('iu-err-inv-mc', {missingParam: 'hotelGroupId'});
		}

		var overlayObj = null;
		mediaConfiguration.extraAssets.forEach(function(asset) {
			if(asset.id === transf.overlay) {
				overlayObj = asset;
			}
		});

		transf.overlay = path.join('upload', overlayObj.source.public_id);
		transf.overlay = transf.overlay.replace(/\//g, ':');
	}else{
		delete transf.overlay;
	}
	return transf;
}

function parseTransformations(transformations, resultList, mediaConfiguration) {
	transformations.forEach(function(transformation) {
		if(typeof  transformation === 'string') {
			//predefined transformations
			if(mediaConfiguration.predefinedTransformations && mediaConfiguration.predefinedTransformations[transformation]) {

				mediaConfiguration.predefinedTransformations[transformation].forEach(function(transf) {
					resultList.push(parseTransformation(transf, mediaConfiguration));
				});

			} else {
				throw errors.newError('iu-err-inv-tr', {givenTransformation: transformation, message: 'No definition found!'});
			}
		} else if(typeof transformation === 'object') {
			resultList.push(parseTransformation(transformation, mediaConfiguration));
		} else {
			throw errors.newError('iu-err-inv-tr-tp', {givenTransformation: transformation});
		}
	});
}

/**
 * Parse image object to image URL
 * @param imageObject
 * @param conf
 * @returns {*}
 * @throws Error if fails
 */
exports.parseImageObjectToUrl = function(imageObject, conf) {
	conf.version = imageObject.source.version;
	if(imageObject.source.isCloudinary) {
		return cloudinary.url(imageObject.source.public_id, conf);
	}
	throw errors.newError('iu-inv-img-obj', {
		providedBy: 'parseImageObjectToUrl',
		givenImageObject: imageObject
	});
};

/**
 * Return a formatted image object for API output
 * @param imageObject {Object|Array} Object or array of objects  - if array then first non null is taken
 * @param [mediaConfiguration] {Object} Media configuration object
 * @param [imageCategory] {String|Array} Image category as string or array of strings - if imageObject also is array,
 * then item with same index, as first non-null item in imageObject Array, is taken
 * @param [width=<width>] {Integer|boolean|undefined} The requested width of the image, if false, then not set, if undefined, then default
 * @param [height=<height>] {Integer|boolean|undefined} The requested height of the image, if false, then not set, if undefined, then default
 * @returns {{highRes: *, lowRes: *}}
 */
exports.formatImageObject = function(imageObject, mediaConfiguration, imageCategory, width, height) {

	mediaConfiguration = mediaConfiguration ? mediaConfiguration : {};
	width = width !== undefined ? width : '<width>';
	height = height !== undefined ? height : '<height>';

	if(Array.isArray(imageObject)) {
		for(var i in imageObject) {
			if(imageObject[i] && typeof imageObject[i] === 'object') {
				imageObject = imageObject[i];
				//take category from this index also
				if(Array.isArray(imageCategory) && imageCategory[i]) {
					imageCategory = imageCategory[i];
				}
				break;
			}
		}
	} else {
		//take category from this index also
		if(Array.isArray(imageCategory) && imageCategory[0]) {
			imageCategory = imageCategory[0];
		}
	}
	//take fallback if needed
	if(imageObject === undefined || !imageObject || !imageObject.source) {
		imageObject = mediaConfiguration.placeholderImage;
	}
	//if no image and fallback
	if(
		imageObject === undefined
		|| !imageObject
		|| !imageObject.source
	) {
		throw errors.newError('iu-inv-img-obj', {
			providedBy: 'formatImageObject',
			givenImageObject: imageObject,
			givenMediaConfiguration: mediaConfiguration
		});
	}

	var defaults = {};
	if(imageCategory
		&& mediaConfiguration.defaults
		&& mediaConfiguration.defaults[imageCategory]
	) {
		defaults = mediaConfiguration.defaults[imageCategory];
	}

	//default transformation for size
	var t1 = {
		crop: defaults.crop ? defaults.crop : 'pad',
		flags: 'force_strip'
	};

	if(width) {
		t1.width = width;
	}

	if(height) {
		t1.height = height;
	}

	var configFinal = {
		transformation: [t1]
	};

	//add transformations from image object
	if(Array.isArray(imageObject.transformations)) {
		parseTransformations(imageObject.transformations, configFinal.transformation, mediaConfiguration);
	}

	//if image object given - do not apply for fallbacks
	if(imageCategory && defaults.transformations) {
		parseTransformations(mediaConfiguration.defaults[imageCategory].transformations, configFinal.transformation, mediaConfiguration);
	}

	//set up configs for different versions
	var configHigh = {
		transformation: []
	};
	var configLow = {
		transformation: []
	};

	//convert some images to jpg to have smaller size
	if(['categoryBg', 'bg', 'menuPageBg', 'hotelGal'].indexOf(imageCategory) > -1) {
		configHigh.format = 'jpg';
		configLow.format = 'jpg';
	}

	for(var c in configFinal.transformation) {
		configHigh.transformation.push(configFinal.transformation[c]);
		configLow.transformation.push(configFinal.transformation[c]);
	}

	var lowTrans = {quality: 10, flags: 'png8'};

	configLow.transformation.push(lowTrans);

	return {
		highRes: exports.parseImageObjectToUrl(imageObject, configHigh),
		lowRes: exports.parseImageObjectToUrl(imageObject, configLow)
	};
};

/**
 * Upload images to Cloudinary
 *
 * @param filePath
 * @param config
 * @param existingImage
 * @param callback
 */
exports.uploadImage = function uploadImage(filePath, config, callback, existingImage) {
	var fileName;
	var defaultConfig = {
		public_id: config.public_id,
		tags: config.tags ? config.tags : [],
		overwrite: true
	};

	function handleUploadResult(result, md5) {
		if(result.error) {
			if(result.error.code) {
				return callback(errors.newError('iu-err-up-img', {
					originalError: result.error,
					info: 'Network Error',
					providedBy: 'uploadImage'
				}));
			}
			return callback(errors.newError('iu-err-up-img', {originalError: result.error, providedBy: 'uploadImage'}));
		}
		result.isCloudinary = true;
		result.md5 = md5;
		result.origName = fileName;
		callback(null, result);
	}

	exports.detectIfImageChanged(filePath, existingImage, function(err, res) {
		if(err) {
			return callback(err);
		} else if(res.changed) {
			console.log('Image changed', defaultConfig.public_id);
			var md5 = res.md5;
			if(filePath instanceof Buffer) {
				var stream = cloudinary.uploader.upload_stream(function(result) {
					handleUploadResult(result, md5);
				}, defaultConfig);
				stream.end(filePath.toString());
			} else {
				fileName = filePath.split('/');
				fileName = fileName[fileName.length - 1];
				cloudinary.uploader.upload(
					filePath,
					function(result) {
						handleUploadResult(result, md5);
					},
					defaultConfig
				);
			}

		} else {
			callback(null, existingImage);
		}

	});

};

/**
 * Upload hotel group image
 * @param hotelGroupId
 * @param filePath
 * @param config
 * @param callback
 * @param existingImage
 */
exports.uploadHotelGroupImage = function(hotelGroupId, filePath, config, callback, existingImage) {
	config.public_id = path.join(getHotelGroupBaseFolderName(hotelGroupId), config.public_id);
	exports.uploadImage(filePath, config, callback, existingImage);
};

/**
 * Upload hotel image
 * @param hotelGroupId
 * @param hotelId
 * @param filePath
 * @param config
 * @param callback
 * @param existingImage
 */
exports.uploadHotelImage = function(hotelGroupId, hotelId, filePath, config, callback, existingImage) {
	config.public_id = path.join(getHotelBaseFolderName(hotelGroupId, hotelId), config.public_id);
	exports.uploadImage(filePath, config, callback, existingImage);
};

/**
 * Detects if image has been changed or not
 * @param filePath
 * @param existingImage
 * @param callback
 */
exports.detectIfImageChanged = function(filePath, existingImage, callback) {
	function check(newImage) {
		var newMD5 = crypto.md5(newImage);

		if(!existingImage) {
			return callback(null, {changed: true, md5: newMD5});
		}

		var existingMd5 = existingImage.md5;

		var changed = newMD5 !== existingMd5;
		callback(null, {changed: changed, md5: newMD5});

	}

	if(filePath instanceof Buffer) {
		check(filePath);
	} else if(filePath.indexOf('http') === 0) {
		httpUtils.makeRequestToUrl(filePath, {}, function(err, newImage) {
			if(err) {
				return callback(errors.newError('u-err-no-img', {originalError: err}));
			}
			check(newImage.body);
		});
	} else {
		fs.readFile(filePath, {encoding: 'utf8'}, function(err, newImage) {
			if(err) {
				return callback(errors.newError('u-err-no-img', {originalError: err}));
			}
			check(newImage);
		});
	}
};

/**
 * Delete all hotel group images
 * @param hotelGroupId
 * @param callback
 */
exports.deleteHotelGroupImages = function(hotelGroupId, callback) {
	cloudinary.api.delete_resources_by_prefix(
		getHotelGroupBaseFolderName(hotelGroupId),
		function(result) {
			if(result.error) {
				if(result.error.code) {
					return callback(errors.newError('iu-err-del-imgs', {
						originalError: result.error,
						info: 'Network Error',
						providedBy: 'uploadImage'
					}));
				}
				return callback(errors.newError('iu-err-del-imgs', {originalError: result.error}));
			}
			callback(null, result);
		}, {}
	);
};

/**
 * Set image object source image
 * @param imageObject
 * @param source
 */
exports.setImageObjectSource = function(imageObject, source) {
	imageObject.source = source;
};
