/**
 * Created by jevgenishupilov on 16/01/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var mediaServices = require('../services');
var cryptoUtils = require('../../utils/cryptoUtils');
var path = require('path');
var cloudinary = require('cloudinary');
var hgId = testingTools.getHotelGroupId();
var testImage = path.join(__dirname, 'test.png');


lab.experiment('Media services', function() {
	var testConfig = {
		public_id: 'test/test_upload',
		version: '66699666'
	};

	var f, c;
	lab.before(function(done){
		f = cloudinary.uploader.upload;
		c = 0;
		cloudinary.uploader.upload = function(fp, cb, conf){
			c++;
			cb({public_id: conf.public_id, tags: conf.tags});
		};
		done();
	});

	lab.after(function(done){
		cloudinary.uploader.upload = f;
		done();
	});

	lab.test('uploadHotelGroupImage should work', function(done) {
		mediaServices.uploadHotelGroupImage(hgId, testImage, testConfig, function(err, res) {
			testingTools.code.expect(err).to.equal(null);
			testingTools.code.expect(c).to.equal(1);
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res, JSON.stringify(res)).to.include(['isCloudinary', 'public_id']);
			testingTools.code.expect(res.isCloudinary).to.equal(true);
			testingTools.code.expect(res.public_id).to.include([testConfig.public_id]);
			done();
		});
	});

	lab.test('uploadHotelGroupImage should work with tags', function(done) {
		testConfig.tags = ['test image'];
		mediaServices.uploadHotelGroupImage(hgId, testImage, testConfig, function(err, res) {
			testingTools.code.expect(err).to.equal(null);
			testingTools.code.expect(c).to.equal(2);
			testingTools.code.expect(res.tags).to.include(testConfig.tags);
			done();
		});
	});

	lab.test('formatImageObject should work', function(done) {
		var img = mediaServices.formatImageObject(
			{

				source: {
					public_id: testConfig.public_id,
					isCloudinary: true,
					version: testConfig.version
				}

			},
			{}
		);
		testingTools.code.expect(img).to.include(['lowRes', 'highRes']);
		testingTools.code.expect(img.lowRes).to.include([testConfig.public_id]);
		testingTools.code.expect(img.highRes).to.include([testConfig.public_id]);

		//Ensure that version number is included in url
		testingTools.code.expect(img.highRes.indexOf('v'+testConfig.version)).to.be.above(-1);
		testingTools.code.expect(img.lowRes.indexOf('v'+testConfig.version)).to.be.above(-1);

		testingTools.code.expect(img.lowRes).to.include(['<width>', '<height>']);
		testingTools.code.expect(img.highRes).to.include(['<width>', '<height>']);
		done();
	});

	lab.test('formatImageObject should work with undefined or null fallback', function(done) {
		var img = mediaServices.formatImageObject(
			undefined,
			{
				placeholderImage:
				{
					source: {
						public_id: testConfig.public_id,
						isCloudinary: true
					}

				}
			}
		);
		testingTools.code.expect(img).to.include(['lowRes', 'highRes']);
		testingTools.code.expect(img.lowRes).to.include([testConfig.public_id]);
		testingTools.code.expect(img.highRes).to.include([testConfig.public_id]);

		testingTools.code.expect(img.lowRes).to.include(['<width>', '<height>']);
		testingTools.code.expect(img.highRes).to.include(['<width>', '<height>']);

		testingTools.code.expect(mediaServices.formatImageObject(
			null,
			{
				placeholderImage: {
					source: {
						public_id: testConfig.public_id,
						isCloudinary: true
					}
				}
			}
		)).to.deep.equal(img);
		done();
	});

	lab.test('formatImageObject throw with no image and no placeholder image', function(done) {
		var t = function(){
			mediaServices.formatImageObject(
					undefined,
					undefined
			);
		};
		testingTools.expectError(t, 'iu-inv-img-obj');

		var t2 = function(){
			mediaServices.formatImageObject(
				undefined,
				{
					placeholderImage: null
				}
			);
		};
		testingTools.expectError(t2, 'iu-inv-img-obj');

		done();
	});

	lab.test('formatImageObject throw without isCloudinary flag', function(done) {
		var t = function(){
			mediaServices.formatImageObject(
					testConfig,
					{}
			);
		};
		testingTools.expectError(t, 'iu-inv-img-obj');
		done();
	});


	lab.test('formatImageObject uses given transformations from imgObj and defaults', function(done) {
		var t = {crop: 'custom_crop'};
		var t2 = {overlay: 'extra_assets/some/overlay'};
		var cnf = {
			source: {
				public_id: 'some-file',
				isCloudinary: true
			},
			transformations: [t, 't1']
		};

		var mediaConfiguration = {
			defaults: {
				typex: {
					transformations: [t2],
					crop: 'pad_this'
				}
			},
			predefinedTransformations: {
				t1: [{
					crop: 'def_crop'
				}]
			},
			hotelGroupId: testingTools.getHotelGroupId(),
			extraAssets: [
				{id: 'smt_else'},
				{
					id: 'extra_assets/some/overlay',
					source: {
						public_id: 'extra_assets/some/overlay'
					}
				}
			]
		};

		var res = mediaServices.formatImageObject(
			cnf,
			mediaConfiguration,
			'typex'
		);
		testingTools.code.expect(res).to.be.object();
		testingTools.code.expect(res.highRes).to.include(['c_' + t.crop]);
		testingTools.code.expect(res.highRes).to.include(['c_def_crop']);
		testingTools.code.expect(res.highRes).to.include(['c_pad_this']);
		testingTools.code.expect(res.highRes).to.include(['l_', t2.overlay.replace(/\//g, ':')]);
		done();
	});

	lab.test('formatImageObject should fail for overlay transformation if no hotelGroupId', function(done) {
		var t2 = {overlay: 'extra_assets/some/overlay'};
		var cnf = {
			source: {
				public_id: 'some-file',
				isCloudinary: true
			},
			transformations: [t2]
		};

		var mediaConfiguration = {
		};

		var res = function(){
			mediaServices.formatImageObject(
				cnf,
				mediaConfiguration,
				'typex'
			);
		};
		testingTools.expectError(res, 'iu-err-inv-mc');
		done();
	});

	lab.test('formatImageObject should not modify overlay path if it is not in extra assets', function(done) {
		var t2 = {overlay: 'some_overlay'};
		var cnf = {
			source: {
				public_id: 'some-file',
				isCloudinary: true
			},
			transformations: [t2]
		};

		var mediaConfiguration = {
		};

		var res = function(){
			return mediaServices.formatImageObject(
				cnf,
				mediaConfiguration,
				'typex'
			).highRes;
		};
		testingTools.code.expect(res).to.not.throw();
		testingTools.code.expect(res()).to.be.string();
		testingTools.code.expect(res()).to.not.include('upload:');
		done();
	});

	lab.test('formatImageObject should not modify overlay path if it is not string', function(done) {
		var t2 = {overlay: {}};
		var cnf = {
			source: {
				public_id: 'some-file',
				isCloudinary: true
			},
			transformations: [t2]
		};

		var mediaConfiguration = {
		};

		var res = function(){
			return mediaServices.formatImageObject(
				cnf,
				mediaConfiguration,
				'typex'
			).highRes;
		};
		testingTools.code.expect(res).to.not.throw();
		testingTools.code.expect(res()).to.be.string();
		testingTools.code.expect(res()).to.not.include('upload:');
		done();
	});


	lab.test('formatImageObject should fail for overlay transformation if no default transformations', function(done) {
		var cnf = {
			source: {
				public_id: 'some-file',
				isCloudinary: true
			},
			transformations: ['t1']
		};

		var mediaConfiguration = {
			hotelGroupId: testingTools.getHotelGroupId()
		};

		var res = function(){
			mediaServices.formatImageObject(
				cnf,
				mediaConfiguration,
				'typex'
			);
		};
		testingTools.expectError(res, 'iu-err-inv-tr');
		done();
	});

	lab.test('formatImageObject should fail if invalid transformation ref', function(done) {
		var cnf = {
			source: {
				public_id: 'some-file',
				isCloudinary: true
			},
			transformations: [123]
		};

		var mediaConfiguration = {
			hotelGroupId: testingTools.getHotelGroupId()
		};

		var res = function(){
			mediaServices.formatImageObject(
				cnf,
				mediaConfiguration
			);
		};
		testingTools.expectError(res, 'iu-err-inv-tr-tp');
		done();
	});

	lab.test('formatImageObject should work without media conf', function(done) {
		var cnf = {
			source: {
				public_id: 'some-file',
				isCloudinary: true
			},
			transformations: []
		};

		var mediaConfiguration = {
			hotelGroupId: testingTools.getHotelGroupId()
		};

		var res = function(){
			return mediaServices.formatImageObject(
				cnf,
				mediaConfiguration
			);
		};
		testingTools.code.expect(res).not.to.throw();
		testingTools.code.expect(res().highRes).to.include(['c_pad']);



		res = function(){
			mediaConfiguration.defaults = {};
			return mediaServices.formatImageObject(
				cnf,
				mediaConfiguration
			);
		};
		testingTools.code.expect(res).not.to.throw();
		testingTools.code.expect(res().highRes).to.include(['c_pad']);

		done();
	});

	lab.test('formatImageObject should give error with invalid image source', function(done) {
		var cnf = {
			source: {
				public_id: 'some-file'
			},
			transformations: []
		};

		var res = function(){
			return mediaServices.formatImageObject(
				cnf
			);
		};
		testingTools.expectError(res, 'iu-inv-img-obj');

		done();
	});


	lab.test('formatImageObject should accept array as first and last param', function(done) {
		var cnf = {
			source: {
				isCloudinary: true,
				public_id: 'some-file'
			},
			transformations: []
		};

		var res = function(){
			return mediaServices.formatImageObject(
				[null, cnf],
				{},
				['cat1', 'cat2']
			);
		};
		testingTools.code.expect(res).not.to.throw();

		done();
	});

	lab.test('formatImageObject should accept array as first param', function(done) {
		var cnf = {
			source: {
				isCloudinary: true,
				public_id: 'some-file'
			},
			transformations: []
		};

		var res = function(){
			return mediaServices.formatImageObject(
				[null, cnf],
				{},
			'cat2'
			);
		};
		testingTools.code.expect(res).not.to.throw();

		done();
	});

	lab.test('formatImageObject should accept array as last param', function(done) {
		var cnf = {
			source: {
				isCloudinary: true,
				public_id: 'some-file'
			},
			transformations: []
		};

		var res = function(){
			return mediaServices.formatImageObject(
				cnf,
				{},
				['cat2']
			);
		};
		testingTools.code.expect(res).not.to.throw();

		done();
	});



});

lab.experiment('Media services', function() {

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm){
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});


	lab.test('uploadHotelGroupImage should give network error', function (done) {
		mediaServices.uploadHotelGroupImage(hgId, testImage, {public_id: 'blabla'}, function (err) {
			testingTools.expectError(err, 'iu-err-up-img');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('code');
			testingTools.code.expect(err.data.debuggingData.originalError.code).to.equal('ECONNRESET');
			done();
		});
	});
});

lab.experiment('media services uploadHotelGroupImage', function() {
	testingTools.mockResponsesForExperiment(lab);
	lab.test('uploadHotelGroupImage should give error if connection fails or smt', function(done) {
		mediaServices.uploadHotelGroupImage(hgId, testImage, {public_id: 'blabla'}, function(err) {
			testingTools.expectError(err, 'iu-err-up-img');
			done();
		});
	});
});

lab.experiment('media services deleteHotelGroupImages', function() {
	var origF, called;
	lab.before(function(done){
		origF = cloudinary.api.delete_resources_by_prefix;
		cloudinary.api.delete_resources_by_prefix = function(prefix, callback){
			if(!called){
				called = true;
				return callback({args: arguments});
			}
			return callback({error: { message: 'Test error'}});
		};
		done();
	});

	lab.after(function(done){
		cloudinary.api.delete_resources_by_prefix = origF;
		done();
	});

	lab.test('should call cloudinary.api.delete_resources_by_prefix and return success', function(done) {
		mediaServices.deleteHotelGroupImages(testingTools.getHotelGroupId(), function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(called).to.be.true();
			testingTools.code.expect(res.args[0]).to.include([testingTools.getHotelGroupId()]);
			done();
		});

	});

	lab.test('should call cloudinary.api.delete_resources_by_prefix and return error', function(done) {
		mediaServices.deleteHotelGroupImages(testingTools.getHotelGroupId(), function(err){
			testingTools.expectError(err, 'iu-err-del-imgs');
			done();
		});

	});
});

lab.experiment('media services deleteHotelGroupImages', function() {

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm){
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});


	lab.test('should call cloudinary.api.delete_resources_by_prefix and return network error', function(done) {
		mediaServices.deleteHotelGroupImages(testingTools.getHotelGroupId(), function(err){
			testingTools.expectError(err, 'iu-err-del-imgs');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('code');
			testingTools.code.expect(err.data.debuggingData.originalError.code).to.equal('ECONNRESET');
			done();
		});

	});
});

lab.experiment('media services uploadHotelImage', function() {
	var f, c;
	lab.before(function(done){
		f = cloudinary.uploader.upload;
		c = 0;
		cloudinary.uploader.upload = function(fp, cb, conf){
			c++;
			cb({public_id: conf.public_id, tags: conf.tags});
		};
		done();
	});

	lab.after(function(done){
		cloudinary.uploader.upload = f;
		done();
	});

	var testImage2 = path.join(__dirname, 'test.png');
	var testConfig = {
		public_id: 'test/test_upload'
	};
	lab.test('should upload an image', function(done) {
		mediaServices.uploadHotelImage(
			testingTools.getHotelGroupId(),
			'test-hotel-id',
			testImage2,
			testConfig,
			function(err, res){
				testingTools.code.expect(err).to.equal(null);
				testingTools.code.expect(c).to.equal(1);
				testingTools.code.expect(res).to.be.object();
				testingTools.code.expect(res, JSON.stringify(res)).to.include(['isCloudinary', 'public_id']);
				testingTools.code.expect(res.isCloudinary).to.equal(true);
				testingTools.code.expect(res.public_id).to.include([testConfig.public_id]);
				done();
			}
		);

	});
});

lab.experiment('Media.Services.UplaodHotelError', function(){
	testingTools.mockResponsesForExperiment(lab);
	lab.test('Should get http error', function(done){
		mediaServices.uploadHotelImage('testhg', 'testhtl', 'http://fakeurl.com', {public_id: '123'}, function(err){
			testingTools.expectError(err, 'u-err-no-img');
			done();
		});
	});

});

lab.test('media services.setImageObjectSource should work', function(done) {
	var img = {};
	var s = {bla: 'bla'};
	mediaServices.setImageObjectSource(img, s);
	testingTools.code.expect(img).to.deep.equal({source: s});
	done();
});

lab.experiment('media services.uploadImage', function() {
	lab.test('should work with Buffer', {timeout: 5000}, function(done) {
		var svgBuffer = require('fs').readFileSync(path.join(__dirname, 'test.svg'));
		mediaServices.uploadImage(svgBuffer, {public_id: 'test-buffer-svg'}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res.format).to.equal('svg');
			done();
		});
	});
	lab.test('should work with normal image', function(done) {
		mediaServices.uploadImage(path.join(__dirname, 'test.png'), {public_id: 'test-ong'}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res.format).to.equal('png');
			done();
		});
	});
});

lab.experiment('Media.Services.ImageChange', function(){
	var url = 'http://fake.testurl.com';
	var hgid = testingTools.getHotelGroupId();
	var teststring = 'teststring';
	var existimg = {
		md5: cryptoUtils.md5(teststring)
	};
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.end(teststring);
	});

	lab.test('Should not detect change', function(done){
		mediaServices.detectIfImageChanged(url, existimg, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.changed).to.be.false();
			done();
		});
	});
	lab.test('Should detect change', function(done){
		mediaServices.detectIfImageChanged(url, 'otherstring', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.changed).to.be.true();
			done();
		});
	});
	lab.test('Should get file  failure', function(done){
		mediaServices.detectIfImageChanged('localpath', 'otherstring', function(err){
			testingTools.expectError(err, 'u-err-no-img');
			done();
		});
	});
	lab.test('Should detect change via upload', function(done){
		mediaServices.uploadHotelImage(hgid, 'jacob', url, {public_id: '123'}, function(err){
			testingTools.expectError(err, 'iu-err-up-img');
			done();
		}, {md5: '123'});
	});
	lab.test('Should detect NO change via upload', function(done){
		mediaServices.uploadHotelImage(hgid, 'jacob', url, {public_id: '123'}, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		}, {md5: existimg.md5});
	});
});
