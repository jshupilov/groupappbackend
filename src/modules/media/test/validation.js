'use strict';
var testingTools = require('../../../test/testingTools');
var mediaServices = require('../../media/services');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var validation = require('../validation');

lab.experiment('Media.validation.validateImageObject', function() {

	lab.test('should give error if invalid data type', function(done) {
		var imageObj = null;

		validation.validateImageObject(imageObj, 'full', function(err){
			testingTools.expectError(err, 'mv-inv-type');
			testingTools.code.expect(err.data.debuggingData).not.to.include(['key']);
			done();
		});
	});

	lab.test('should give error if missing the structure', function(done) {
		var imageObj = {};

		validation.validateImageObject(imageObj, 'full', function(err){
			testingTools.expectError(err, 'mv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('source');
			done();
		});
	});

	lab.test('should give error if source has invalid type', function(done) {
		var imageObj = {source: null};

		validation.validateImageObject(imageObj, 'full', function(err){
			testingTools.expectError(err, 'mv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('source');
			done();
		});
	});

	lab.test('should give error if source is missing required fields (isCloudinary)', function(done) {
		var imageObj = {source: {}};

		validation.validateImageObject(imageObj, 'full', function(err){
			testingTools.expectError(err, 'mv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('source.isCloudinary');
			done();
		});
	});

	lab.test('should give error if source is missing required fields (public_id)', function(done) {
		var imageObj = {
				source: {
					isCloudinary: null
				}
			};

		validation.validateImageObject(imageObj, 'full', function(err){
			testingTools.expectError(err, 'mv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('source.public_id');
			done();
		});
	});

	lab.test('should give error if source.isCloudinary is not true', function(done) {
		var imageObj = {
				source: {
					isCloudinary: null,
					public_id: null
				}
			};

		validation.validateImageObject(imageObj, 'full', function(err){
			testingTools.expectError(err, 'mv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('source.isCloudinary');
			done();
		});
	});

	lab.test('should give error if source.public_id is not a string', function(done) {
		var imageObj = {
				source: {
					isCloudinary: true,
					public_id: null
				}
			};

		validation.validateImageObject(imageObj, 'full', function(err){
			testingTools.expectError(err, 'mv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('source.public_id');
			done();
		});
	});

	lab.test('should give error if source.public_id is an empty string', function(done) {
		var imageObj = {
				source: {
					isCloudinary: true,
					public_id: ''
				}
			};

		validation.validateImageObject(imageObj, 'full', function(err){
			testingTools.expectError(err, 'mv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('source.public_id');
			done();
		});
	});

	lab.test('should give error if source.public_id does not contain the required prefixes for hotelGroupImage', function(done) {
		var imageObj = {
				source: {
					isCloudinary: true,
					public_id: 'asdasdsa'
				}
			};

		validation.validateImageObject(imageObj, 'hotelGroupImage', function(err){
			testingTools.expectError(err, 'mv-src-mis-prefix');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('source.public_id');
			done();
		}, {hotelGroupId: testingTools.getHotelGroupId()});
	});

	lab.test('should not give error if is correct for "hotelGroupImage" validationType', function(done) {
		var imageObj = {
			source: {
				isCloudinary: true,
				public_id: mediaServices.getHotelGroupBaseFolderName(testingTools.getHotelGroupId()) + '/some/folder/blabla'
			}
		};

		validation.validateImageObject(imageObj, 'hotelGroupImage', function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		}, {hotelGroupId: testingTools.getHotelGroupId()});
	});

	lab.test('should not give error if is correct for "hotelImage" validationType', function(done) {
		var imageObj = {
			source: {
				isCloudinary: true,
				public_id: mediaServices.getHotelBaseFolderName(testingTools.getHotelGroupId(), 'some-hotel') + '/some/folder/blabla'
			}
		};

		validation.validateImageObject(imageObj, 'hotelImage', function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		}, {hotelGroupId: testingTools.getHotelGroupId(), hotelId: 'some-hotel'});
	});

	lab.test('should not give error if is correct for "full" validationType', function(done) {
		var imageObj = {
			source: {
				isCloudinary: true,
				public_id: '/some/folder/blabla'
			}
		};

		validation.validateImageObject(imageObj, 'full', function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		}, {hotelGroupId: testingTools.getHotelGroupId()});
	});

	lab.test('should give error if invalid validationType', function(done) {
		var imageObj = {
			source: {
				isCloudinary: true,
				public_id: '/some/folder/blabla'
			}
		};

		validation.validateImageObject(imageObj, 'dfdsfsfd', function(err){
			testingTools.expectError(err, 'mv-inv-val-type');
			done();
		}, {hotelGroupId: testingTools.getHotelGroupId()});
	});

});
