'use strict';
var errors = require('../utils/errors');
var mediaServices = require('../media/services');
var check = require('check-types');

errors.defineError('mv-inv-val-type', 'System error', 'Invalid validation type given', 'Media.validation', 400);
errors.defineError('mv-inv-type', 'System error', 'Invalid data type given', 'Media.validation', 400);
errors.defineError('mv-inv-val', 'System error', 'Invalid value was given', 'Media.validation', 400);
errors.defineError('mv-key-miss', 'System error', 'Required key is missing', 'Media.validation', 400);
errors.defineError('mv-src-mis-prefix', 'System error', 'The image public_id should start with a certain prefix', 'Media.validation', 400);

/**
 * Validate root keys of Image object
 * @param imageObj
 * @param validationType
 * @param callback
 * @returns {*}
 */
function validateRootKeys(imageObj, validationType, callback){
	var rootKeys = ['source'];

	for(var key in rootKeys){
		if(!imageObj.hasOwnProperty(rootKeys[key])){
			return callback(errors.newError('mv-key-miss', {key: rootKeys[key]}));
		}
	}

	return callback(null);
}

/**
 * Validate image object source element
 * @param imageObj
 * @param validationType
 * @param callback
 * @param [context]
 * @returns {*}
 */
function validateSource(imageObj, validationType, callback){
	if( !check.object(imageObj.source) ){
		return callback(errors.newError('mv-inv-type', {key: 'source'}));
	}

	if( !imageObj.source.hasOwnProperty('isCloudinary') ){
		return callback(errors.newError('mv-key-miss', {key: 'source.isCloudinary'}));
	}

	if( !imageObj.source.hasOwnProperty('public_id') ){
		return callback(errors.newError('mv-key-miss', {key: 'source.public_id'}));
	}

	if( imageObj.source.isCloudinary !== true ){
		return callback(errors.newError('mv-inv-val', {key: 'source.isCloudinary'}));
	}

	if( typeof imageObj.source.public_id !== 'string' ){
		return callback(errors.newError('mv-inv-type', {key: 'source.public_id'}));
	}

	if( imageObj.source.public_id.length < 1 ){
		return callback(errors.newError('mv-inv-val', {key: 'source.public_id'}));
	}

	return callback(null);
}

/**
 * Validate public_id prefix
 * @param imageObj
 * @param validationType
 * @param callback
 * @param context
 * @returns {*}
 */
function validatePrefix(imageObj, validationType, callback, context){
	var pref = '';
	if(validationType === 'hotelGroupImage'){
		pref = mediaServices.getHotelGroupBaseFolderName(context.hotelGroupId);
	}else if(validationType === 'hotelImage'){
		pref = mediaServices.getHotelBaseFolderName(context.hotelGroupId, context.hotelId);
	}
	if( imageObj.source.public_id.indexOf( pref ) !== 0 ){
		return callback(errors.newError('mv-src-mis-prefix', {
			key: 'source.public_id',
			requiredPrefix: pref,
			givenPublicId: imageObj.source.public_id
		}));
	}
	return callback(null);
}

/**
 * Validate image object
 * @param imageObj
 * @param validationType
 * @param callback
 * @param [context] Context object
 * @returns {*}
 */
exports.validateImageObject = function(imageObj, validationType, callback, context){


	var validationTypes = {
		full: [validateRootKeys, validateSource, validatePrefix]
	};

	validationTypes.hotelGroupImage = [].concat(validationTypes.full, []);
	validationTypes.hotelImage = [].concat(validationTypes.full, []);


	if(validationTypes.hasOwnProperty(validationType)){

		var checkIndex = 0;
		var valCaller = function(){
			//call first check
			validationTypes[validationType][checkIndex](imageObj, validationType, function(err, res){
				if(err){
					callback(err);
					//call next check
				}else if( validationTypes[validationType][checkIndex+1] !== undefined ){
					checkIndex++;
					valCaller();
				}else{
					callback(null, res);
				}
			}, context);
		};

		if( !check.object(imageObj) ){
			return callback( errors.newError('mv-inv-type', {givenType: typeof imageObj}));
		}

		valCaller();

	}else{
		callback(errors.newError('mv-inv-val-type', {}));
	}

};
