'use strict';
var validation = require('./validation');
var errors = require('../utils/errors');
var langUtils = require('../utils/langUtils');
var handlebars = require('handlebars');
var mandrill = require('mandrill-api/mandrill');
var mandrillClient = new mandrill.Mandrill(process.env.MANDRILL_API_KEY);

errors.defineError('mss-rndr-err', 'System error', 'Error rendering given template', 'Messaging.Services');
errors.defineError('mss-snd-inv-lang', 'System error', 'Invalid language code given', 'Messaging.Services');
errors.defineError('mss-snd-mis-lang', 'System error', 'Email template does not support this language', 'Messaging.Services');
errors.defineError('mss-snd-err', 'System error', 'Error sending email', 'Messaging.Services');

/**
 * Render the given email template (will modify the input template object)
 * @param emailTemplate {Object} Email temaplate
 * @param templateData {Object} Data for templates
 * @param callback
 * @returns {*}
 */
exports.renderEmailTemplate = function(emailTemplate, templateData, callback){

	//validate if input data for template is correct
	validation.validateTemplateData(emailTemplate, templateData, function(err) {
		if(err) {
			return callback(err);
		}
		var options = {strict: true};
		for(var li in emailTemplate.localized){

			emailTemplate.localized[li].fromName = emailTemplate.localized[li].fromName || emailTemplate.fromName;
			emailTemplate.localized[li].fromEmail = emailTemplate.localized[li].fromEmail || emailTemplate.fromEmail;
			var subject = emailTemplate.localized[li].subject || emailTemplate.subject;
			var bodyHtml = emailTemplate.localized[li].bodyHtml || emailTemplate.bodyHtml;
			var bodyText = emailTemplate.localized[li].bodyText || emailTemplate.bodyText;

			try{
				emailTemplate.localized[li].subject = handlebars.compile(subject, options)(templateData);
				if(bodyHtml){
					emailTemplate.localized[li].bodyHtml = handlebars.compile(bodyHtml, options)(templateData);
				}
				if(bodyText){
					emailTemplate.localized[li].bodyText = handlebars.compile(bodyText, options)(templateData);
				}
			}catch(e){
				return callback(errors.newError('mss-rndr-err', { originalError: e}));
			}
		}
		callback(null, emailTemplate);
	});

};

/**
 * Send raw email message via Mandrill
 * @param message {Object}
 * @param callback
 * @returns {*}
 */
exports.sendRawEmail = function(message, callback){
	mandrillClient.messages.send(
		{message: message, async: true},
		function(result){
			//all fine
			callback(null, result);
		},
		function(err){
			//error
			callback(errors.newError('mss-snd-err', {originalError: err}));
		}
	);
};

/**
 * Send email to listed receiptens
 * @param emailTemplate
 * @param templateData
 * @param languageCode
 * @param recipients
 * @param callback
 */
exports.sendEmail = function(emailTemplate, templateData, languageCode, recipients, callback){

	//validate if input data for template is correct
	validation.validateTemplateData(emailTemplate, templateData, function(err){
		if(err){
			return callback(err);
		}

		//validate language
		if( !langUtils.isValidLanguageCode(languageCode) ){
			return callback(errors.newError('mss-snd-inv-lang', {givenLanguage: languageCode}));
		}

		if( !emailTemplate.localized.hasOwnProperty(languageCode) ){
			return callback(errors.newError('mss-snd-mis-lang', {givenLanguage: languageCode}));
		}

		var localizedEmailTemplate = emailTemplate.localized[languageCode];

		//validate recipients
		validation.validateRecipients(recipients, function(err){
			if(err){
				err.data.debuggingData.key = 'recipients' + (err.data.debuggingData.key ? err.data.debuggingData.key : '');
				return callback(err);
			}

			//render the template
			exports.renderEmailTemplate(emailTemplate, templateData, function(err){
				if(err){
					return callback(err);
				}

				//send the email
				var message = {
					subject: localizedEmailTemplate.subject,
					html: localizedEmailTemplate.bodyHtml,
					text: localizedEmailTemplate.bodyText,
					from_email: localizedEmailTemplate.fromEmail,
					from_name: localizedEmailTemplate.fromName,
					to: recipients,
					auto_text: true

				};

				exports.sendRawEmail(message, callback);

			});

		});

	});

};
