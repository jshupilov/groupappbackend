'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var validation = require('../validation');

lab.experiment('Messaging.service should meet overall requirements', function() {

	lab.test('- should provide methods to deal with email templates', function(done) {
		var methods = [ 'renderEmailTemplate' ];

		for(var mi in methods){
			testingTools.code.expect(services).to.include(methods[mi]);
			testingTools.code.expect(services[ methods[mi] ]).to.be.function();
		}

		done();
	});

	lab.test('- should provide methods to deal with email sending', function(done) {
		var methods = [ 'sendEmail' ];

		for(var mi in methods){
			testingTools.code.expect(services).to.include(methods[mi]);
			testingTools.code.expect(services[ methods[mi] ]).to.be.function();
		}

		done();
	});

});

lab.experiment('Messaging.Service.renderEmailTemplate', function() {
	var t1, t2, t3;

	lab.before(function(done){
		//create and save test templates
		t1 = function(){
			return {
				fromEmail: 'fromEmail@example.com',
				fromName: 'Example Name',
				subject: 'example subject',
				bodyHtml: '<div>example code {{var1}}</div>',
				localized: {
					en_GB: {}
				}
			};
		};

		t2 = function(){
			return {
				fromEmail: 'fromEmail@example.com',
				fromName: 'Example Name',
				subject: 'example subject {{var2}}',
				bodyHtml: '<div>example code {{var1}}</div>',
				localized: {
					en_GB: {}
				}
			};
		};
		t3 = validation.validateTemplateData;
		done();
	});

	lab.after(function(done){
		validation.validateTemplateData = t3;
		done();
	});


	lab.test('should call validateTemplateData', function(done) {
		var c1 = 0;

		validation.validateTemplateData = function(){
			c1++;
			t3.apply(this, arguments);
		};
		services.renderEmailTemplate(1, {var1: 'var1-content'},  function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(c1).to.equal(1);
			done();
		});
	});

	lab.test('should give error if not all required parameters given', function(done) {
		services.renderEmailTemplate(t1(), {},  function(err){
			testingTools.expectError(err, 'msv-tpld-mis-val');
			testingTools.code.expect(err.data.debuggingData.originalError.message).to.include('var1');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('data');
			done();
		});
	});


	lab.test('should return html if all OK', function(done) {
		services.renderEmailTemplate(t2(), {var1: 'var1-content', var2: 'var2-content'}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.localized.en_GB.bodyHtml).to.include('var1-content');
			testingTools.code.expect(res.localized.en_GB.subject).to.include('var2-content');
			done();
		});
	});

	lab.test('should return localized html if all OK', function(done) {
		var t = t2();
		t.localized.en_GB.bodyHtml = 'localized body {{var1}}';
		services.renderEmailTemplate(t, {var1: 'var1-content', var2: 'var2-content'}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.localized.en_GB.bodyHtml).to.include('var1-content');
			testingTools.code.expect(res.localized.en_GB.bodyHtml).to.include('localized body');
			done();
		});
	});

	lab.test('should return text if all OK', function(done) {
		var t = t2();
		t.bodyText = 'asdasad {{var1}}';
		delete t.bodyHtml;
		services.renderEmailTemplate(t, {var1: 'var1-content', var2: 'var2-content'}, function(err){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(t.localized.en_GB).not.to.include('bodyHtml');
			testingTools.code.expect(t.localized.en_GB.bodyText).to.include('var1-content');
			done();
		});
	});

	lab.test('should return text if all OK', function(done) {
		var t = t2();
		t.localized.en_GB.bodyText = 'localized body text{{var1}}';
		delete t.bodyHtml;
		services.renderEmailTemplate(t, {var1: 'var1-content', var2: 'var2-content'}, function(err){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(t.localized.en_GB).not.to.include('bodyHtml');
			testingTools.code.expect(t.localized.en_GB.bodyText).to.include('var1-content');
			testingTools.code.expect(t.localized.en_GB.bodyText).to.include('localized body text');
			done();
		});
	});

	lab.test('should return error if some template is invalid', function(done) {
		validation.validateTemplateData = function(){
			arguments[arguments.length-1](null);
		};
		var t = t2();
		t.localized.en_GB.bodyText = 'localized body text{{var1';
		delete t.bodyHtml;
		services.renderEmailTemplate(t, {var1: 'var1-content', var2: 'var2-content'}, function(err){
			testingTools.expectError(err, 'mss-rndr-err');
			done();
		});
	});

	lab.test('should return error if some variable is missing', function(done) {
		validation.validateTemplateData = function(){
			arguments[arguments.length-1](null);
		};
		var t = t2();
		services.renderEmailTemplate(t, {var1: 'var1-content'}, function(err){
			testingTools.expectError(err, 'mss-rndr-err');
			done();
		});
	});


});

lab.experiment('Messaging.Services.sendEmail with MITM', function() {
	var f1, f2, f3, f4;

	var t1, apiCalls = 0;
	testingTools.mockResponsesForExperiment(lab, null, function(mitm){
		mitm.on('connect', function(socket, opts) {
			testingTools.code.expect(opts.host).to.include('mandrill');
			apiCalls++;
		});
	});

	lab.before(function(done){
		t1 = {
			fromEmail: 'fromEmail@example.com',
			fromName: 'Example Name',
			subject: 'example subject',
			bodyHtml: '<div>example code {{var1}}</div>',
			localized: {
				en_GB: {}
			}
		};
		f1 = validation.validateEmailTemplate;
		f2 = validation.validateTemplateData;
		f3 = services.renderEmailTemplate;
		f4 = validation.validateRecipients;
		done();
	});

	lab.afterEach(function(done){
		validation.validateEmailTemplate = f1;
		validation.validateTemplateData = f2;
		services.renderEmailTemplate = f3;
		validation.validateRecipients = f4;
		done();
	});

	lab.test('should validate template', function(done) {
		var called = 0;
		validation.validateEmailTemplate = function(){
			called++;
			f1.apply(this, arguments);
		};
		services.sendEmail(null, {}, 'en_GB', [], function(){
			testingTools.code.expect(called).to.be.above(0);
			done();
		});
	});

	lab.test('should validate template data', function(done) {
		var called = 0;
		validation.validateTemplateData = function(){
			called++;
			f2.apply(this, arguments);
		};
		services.sendEmail(null, {}, 'en_GB', [], function(){
			testingTools.code.expect(called).to.be.above(0);
			done();
		});

	});

	lab.test('should validate recipients object', function(done) {
		var called = 0;
		validation.validateRecipients = function(){
			called++;
			f4.apply(this, arguments);
		};
		services.sendEmail(t1, {var1: 'var1-content'}, 'en_GB', [], function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(called).to.be.above(0);
			testingTools.code.expect(err.data.debuggingData.key).to.include('recipients');

			done();
		});

	});

	lab.test('should validate recipients - every one', function(done) {
		var called = 0;
		validation.validateRecipients = function(){
			called++;
			f4.apply(this, arguments);
		};
		services.sendEmail(t1, {var1: 'var1-content'}, 'en_GB', [{}], function(err){
			testingTools.expectError(err, 'msv-key-miss');
			testingTools.code.expect(called).to.be.above(0);
			testingTools.code.expect(err.data.debuggingData.key).to.equal('recipients[0].email');
			done();
		});

	});

	lab.test('should give error if invalid language', function(done) {
		services.sendEmail(t1, {var1: 'var1-content'}, null, [], function(err){
			testingTools.expectError(err, 'mss-snd-inv-lang');
			done();
		});
	});

	lab.test('should give error if no template for given language', function(done) {
		services.sendEmail(t1, {var1: 'var1-content'}, 'et_EE', [], function(err){
			testingTools.expectError(err, 'mss-snd-mis-lang');
			done();
		});
	});


	lab.test('should render the template', function(done) {
		var called = 0;
		services.renderEmailTemplate = function(){
			called++;
			f3.apply(this, arguments);
		};
		services.sendEmail(t1, {var1: 'var1-content'}, 'en_GB', [{email: 'u8hhh80hhasd@cardola.net'}], function(err){
			//sending should fail because we are mocking responses
			testingTools.expectError(err, 'mss-snd-err');
			testingTools.code.expect(called).to.equal(1);
			done();
		});
	});

	lab.test('should pass on errors from rendering', function(done) {
		services.renderEmailTemplate = function(){
			arguments[arguments.length-1](new Error('98h9h90h90'));
		};
		services.sendEmail(t1, {var1: 'var1-content'}, 'en_GB', [{email: 'u8hhh80hhasd@cardola.net'}], function(err){
			//sending should fail because we are mocking responses
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('98h9h90h90');
			done();
		});
	});

	lab.test('should send the email trough mandrill API', function(done) {
		var calls = apiCalls;
		services.sendEmail(
			t1,
			{var1: 'var1-content'}, 'en_GB', [{email: 'u8hhh80hhasd@cardola.net'}],
			function(err){
				//sending should fail because we are mocking responses
				testingTools.expectError(err, 'mss-snd-err');
				testingTools.code.expect(apiCalls).to.be.above(calls);
				done();
			}
		);
	});
});

lab.experiment('Messaging.Services.sendEmail without MITM', function() {
	var t1 = {
		fromEmail: 'no-reply@cardola.net',
		fromName: 'Example Name',
		subject: '[GA TEST] Test email({{var1}})',
		bodyHtml: '<div>example code {{var1}}</div>',
		localized: {
			en_GB: {}
		}
	};

	lab.test('should send the email trough mandrill API if all OK', {timeout: 20000}, function(done) {
		services.sendEmail(
			t1,
			{var1: 'var1-content'}, 'en_GB', [{email: '34ff3f@cardola.net'}],
			function(err, res){
				//sending should work
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res[0].status).to.be.equal('sent');
				done();
			}
		);
	});
});
