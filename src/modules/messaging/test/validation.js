'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var validation = require('../validation');
var q = require('q');

lab.experiment('Messaging.validation should meet overall requirements', function() {

	lab.test('- should provide methods to deal with email templates', function(done) {
		var methods = [ 'validateEmailTemplate'];

		for(var mi in methods){
			testingTools.code.expect(validation).to.include(methods[mi]);
			testingTools.code.expect(validation[ methods[mi] ]).to.be.function();
		}

		done();
	});

});

lab.experiment('Messaging.validation.validateEmailTemplate', function() {
	var getValidTemplate;

	lab.before(function(done){
		getValidTemplate = function(){
			return {
				fromEmail: 'fromEmail@example.com',
				fromName: 'Example Name',
				subject: 'example subject {{var2}}',
				bodyHtml: '<div>example code {{var1}}</div>',
				localized: {
					en_GB: {}
				}
			};
		};

		done();
	});

	lab.test('should not complain about correct template', function(done) {
		var emailTemplate = getValidTemplate();
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});
	});

	lab.test('should give error if invalid data type', function(done) {
		var emailTemplate = null;
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-val');
			done();
		});
	});

	lab.test('should give error if missing some keys', function(done) {
		var promises = [];

		function doVal(p, emailTemplate, key){
			validation.validateEmailTemplate(emailTemplate, function(err){
				testingTools.expectError(err, 'msv-key-miss');
				testingTools.code.expect(err.data.debuggingData.key).to.equal(key);
				p.resolve();
			});
		}
		var requiredKeys = ['localized'];
		for(var i in requiredKeys){
			var tmpl = getValidTemplate();
			delete tmpl[requiredKeys[i]];
			var p = q.defer();
			promises.push(p.promise);
			doVal(p, tmpl, requiredKeys[i]);
		}

		q.all(promises).finally(function(){
			done();
		});


	});

	lab.test('should give error if invalid localized object', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.localized = null;
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized');
			done();
		});
	});

	lab.test('should give error if invalid localized language key', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.localized = {asdasdad: {}};
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-lang-code');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized[asdasdad]');
			done();
		});
	});

	lab.test('should give error if localized[x] has invalid type', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.localized = {en_GB: null};
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized[en_GB]');
			done();
		});
	});

	lab.test('should give error if localized[x] does not have required data', function(done) {
		var emailTemplate = {
			localized: {
				en_GB: {}
			}
		};
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.include('localized[en_GB].');
			done();
		});
	});

	lab.test('should give error if non string "fromEmail"', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.fromEmail = null;
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('fromEmail');
			done();
		});
	});

	lab.test('should give error if invalid "fromEmail"', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.fromEmail = 'aasd';
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-frm-email');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('fromEmail');
			done();
		});
	});

	lab.test('should give error if invalid "fromName"', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.fromName = '';
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('fromName');
			done();
		});
	});

	lab.test('should NOt give error if null "fromName"', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.fromName = null;
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should give error if invalid "subject"', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.subject = null;
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('subject');
			done();
		});
	});

	lab.test('should give error if invalid "subject" template', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.subject = 'adssad {{asda';
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-tpl');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('subject');
			done();
		});
	});

	lab.test('should give error if invalid "bodyHtml"', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.bodyHtml = 1;
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('bodyHtml');
			done();
		});
	});

	lab.test('should give error if invalid "bodyText"', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.bodyText = 1;
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('bodyText');
			done();
		});
	});

	lab.test('should NOT give error if null "bodyHtml", but has "bodyText"', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.bodyHtml = null;
		emailTemplate.bodyText = 'Body text';
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should NOT give error if null "bodyText", but has "bodyHtml"', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.bodyText = null;
		emailTemplate.bodyHtml = 'Body text';
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should give error if invalid "bodyHtml" template', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.bodyHtml = 'asdsda {{adsa}';
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-tpl');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('bodyHtml');
			done();
		});
	});

	lab.test('should give error if invalid "bodyText" template', function(done) {
		var emailTemplate = getValidTemplate();
		emailTemplate.bodyText = 'asdsda {{adsa}';
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-inv-tpl');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('bodyText');
			done();
		});
	});

	lab.test('should give error if missing "bodyHtml" or "bodyText"', function(done) {
		var emailTemplate = getValidTemplate();
		delete emailTemplate.bodyHtml;
		delete emailTemplate.bodyText;
		validation.validateEmailTemplate(emailTemplate, function(err){
			testingTools.expectError(err, 'msv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized[en_GB].bodyHtml,bodyText');
			done();
		});
	});


});

lab.experiment('Messaging.Validation.validateTemplateData', function() {
	var getValidTemplate, getValidData;

	lab.before(function(done){
		getValidTemplate = function(){
			return {
				fromEmail: 'fromEmail@example.com',
				fromName: 'Example Name',
				subject: 'example subject {{var2}}',
				bodyHtml: '<div>example code {{var1}}</div>',
				localized: {
					en_GB: {
						bodyText: 'example code {{var3}}'
					}
				}
			};
		};
		getValidData = function(){
			return {
				var1: 'var1-content',
				var2: 'var2-content',
				var3: 'var3-content'
			};
		};

		done();
	});

	lab.test('should validate template', function(done) {
		validation.validateTemplateData(null, null, function(err){
			testingTools.expectError(err, 'msv-inv-val');
			done();
		});
	});

	lab.test('should give error if data has incorrect type', function(done) {
		validation.validateTemplateData(getValidTemplate(), null, function(err){
			testingTools.expectError(err, 'msv-tpld-inv-val');
			done();
		});
	});

	lab.test('should give error if data is missing some required variables', function(done) {
		validation.validateTemplateData(getValidTemplate(), {}, function(err){
			testingTools.expectError(err, 'msv-tpld-mis-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('data');
			done();
		});
	});

	lab.test('should NOT give error if data is correct', function(done) {
		validation.validateTemplateData(getValidTemplate(), getValidData(), function(err, res){

			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res).to.include('subject');
			testingTools.code.expect(res).to.include('bodyHtml');
			testingTools.code.expect(res).to.include('localized[en_GB].bodyText');
			testingTools.code.expect(res.subject).to.equal('example subject var2-content');
			testingTools.code.expect(res.bodyHtml).to.equal('<div>example code var1-content</div>');
			testingTools.code.expect(res['localized[en_GB].bodyText']).to.equal('example code var3-content');
			done();
		});
	});

});

lab.experiment('Messages.validation.validateRecipients', function() {
	lab.test('should give error if invalid recipients', function(done) {
		validation.validateRecipients(null, function(err){
			testingTools.expectError(err, 'msv-inv-type');
			done();
		});
	});

	lab.test('should give error if empty recipients', function(done) {
		validation.validateRecipients([], function(err){
			testingTools.expectError(err, 'msv-inv-val');
			done();
		});
	});

	lab.test('should give error if invalid recipient', function(done) {
		validation.validateRecipients([1], function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0]');
			done();
		});
	});


	lab.test('should give error if recipient item has no email', function(done) {
		validation.validateRecipients([{}], function(err){
			testingTools.expectError(err, 'msv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].email');
			done();
		});
	});

	lab.test('should give error if recipient item has invalid email', function(done) {
		validation.validateRecipients([{email: 'blabla'}], function(err){
			testingTools.expectError(err, 'msv-inv-to-email');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].email');
			done();
		});
	});

	lab.test('should give error if recipient item has invalid email type', function(done) {
		validation.validateRecipients([{email: 1}], function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].email');
			done();
		});
	});

	lab.test('should give error if recipient item has invalid name', function(done) {
		validation.validateRecipients([{email: 'testcardola@asdasdads34eda.com', name: 1}], function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].name');
			done();
		});
	});

	lab.test('should give error if recipient item has invalid name', function(done) {
			validation.validateRecipients([{email: 'testcardola@asdasdads34eda.com', name: ''}], function(err){
				testingTools.expectError(err, 'msv-inv-val');
				testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].name');
				done();
			});
	});

	lab.test('should give error if recipient item has invalid type', function(done) {
			validation.validateRecipients([{email: 'testcardola@asdasdads34eda.com', name: 'Receiver', type: ''}], function(err){
				testingTools.expectError(err, 'msv-inv-val');
				testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].type');
				done();
			});
	});

	lab.test('should give error if recipient item has invalid type - no oneof(to, cc, bcc)', function(done) {
			validation.validateRecipients([{email: 'testcardola@asdasdads34eda.com', name: 'Receiver', type: 'tocc'}], function(err){
				testingTools.expectError(err, 'msv-non-ex-val');
				testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].type');
				done();
			});
	});

	lab.test('should not give error if correct and many recipients', function(done) {
		validation.validateRecipients(
			[
				{email: 'testcardola@asdasdads34eda.com', type: 'to'},
				{email: 'testcardola2@asdasdads34eda.com', name: 'Receiver'},
				{email: 'testcardola3@asdasdads34eda.com', name: 'Receiver', type: 'bcc'},
				{email: 'testcardola4@asdasdads34eda.com'}
			],
			function(err){
				testingTools.code.expect(err).to.be.null();
				done();
			}
		);
	});


});

lab.experiment('Messages.validation.validateAttachments', function() {
	lab.test('should go well if attachments is null or undefined', function(done) {
		validation.validateAttachments(null, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should give error if attachments not array', function(done) {
		validation.validateAttachments({}, function(err){
			testingTools.expectError(err, 'msv-inv-type');
			testingTools.code.expect(err.data.debuggingData.requiredType).to.equal('array');
			done();
		});
	});
	lab.test('should go well if attachments is empty array', function(done) {
		validation.validateAttachments([], function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should give error if invalid attachment', function(done) {
		validation.validateAttachments([1], function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0]');
			done();
		});
	});


	lab.test('should give error if attachment item has no type', function(done) {
		validation.validateAttachments([{}], function(err){
			testingTools.expectError(err, 'msv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].type');
			done();
		});
	});

	lab.test('should give error if attachment item has invalid type type', function(done) {
		validation.validateAttachments([{type: 1}], function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].type');
			done();
		});
	});

	lab.test('should give error if attachment item has no name', function(done) {
		validation.validateAttachments([{type: 'text/csv'}], function(err){
			testingTools.expectError(err, 'msv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].name');
			done();
		});
	});

	lab.test('should give error if attachment item name has invalid type', function(done) {
		validation.validateAttachments([{type: 'text/csv', name: 1}], function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].name');
			done();
		});
	});

	lab.test('should give error if attachment item has no content', function(done) {
		validation.validateAttachments([{type: 'text/csv', name: 'test-name'}], function(err){
			testingTools.expectError(err, 'msv-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].content');
			done();
		});
	});

	lab.test('should give error if attachment item has invalid content', function(done) {
		validation.validateAttachments([{type: 'text/csv', name: 'test-name', content: 1}], function(err){
			testingTools.expectError(err, 'msv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('[0].content');
			done();
		});
	});
	lab.test('should go well', function(done) {
		validation.validateAttachments([{type: 'text/csv', name: 'test-name', content: 'Unempty-string'}], function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

});
