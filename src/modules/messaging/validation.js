'use strict';
var check = require('check-types');
var errors = require('../utils/errors');
var validationUtils = require('../utils/validationUtils');
var langUtils = require('../utils/langUtils');
var handlebars = require('handlebars');
var q = require('q');
errors.defineError('msv-inv-val', 'System error', 'Invalid email template key value', 'Messaging.Validation');
errors.defineError('msv-non-ex-val', 'System error', 'This value not exists', 'Messaging.Validation');
errors.defineError('msv-inv-type', 'System error', 'Invalid email template key type', 'Messaging.Validation');
errors.defineError('msv-key-miss', 'System error', 'Required email template key is missing', 'Messaging.Validation');
errors.defineError('msv-inv-tpl', 'System error', 'Invalid email template given', 'Messaging.Validation');
errors.defineError('msv-inv-frm-email', 'Invalid email format', 'Invalid sender email address given', 'Messaging.Validation');
errors.defineError('msv-inv-to-email', 'Invalid email format', 'Invalid email address given', 'Messaging.Validation');
errors.defineError('msv-inv-lang-code', 'System error', 'Invalid language code provided', 'Messaging.Validation');
errors.defineError('msv-tpld-inv-val', 'System error', 'Email template data has invalid information', 'Messaging.Validation');
errors.defineError('msv-tpld-mis-val', 'System error', 'Email template is missing some inpu data', 'Messaging.Validation');

exports.validateEmailTemplate = function(emailTemplate, callback){

	validationUtils.validateWithArrayOfFunctionsWithCallbacks(
		[
			//validate type
			function(emailTemplate, cb){
				if( !check.object(emailTemplate) ){
					return cb(errors.newError('msv-inv-val', {}));
				}
				cb(null);
			},

			//validate req keys
			function(emailTemplate, cb){
				validationUtils.validateHasKeys(['localized'], emailTemplate, function(errKey){
					if(errKey){
						return cb(errors.newError('msv-key-miss', {key: errKey}));
					}
					cb(null);
				});
			},

			//validate localized type
			function(emailTemplate, cb){
				if( !check.object(emailTemplate.localized) ){
					return cb(errors.newError('msv-inv-val', {key: 'localized'}));
				}
				cb(null);
			},

			//validate localized language keys
			function(emailTemplate, cb){
				for(var li in emailTemplate.localized){
					if( !langUtils.isValidLanguageCode(li) ){
						return cb(errors.newError('msv-inv-lang-code', {key: 'localized[' + li + ']'}));
					}
				}
				cb(null);
			},

			//validate localized object types
			function(emailTemplate, cb){
				for(var li in emailTemplate.localized){
					if( !check.object(emailTemplate.localized[li]) ){
						return cb(errors.newError('msv-inv-val', {key: 'localized[' + li + ']'}));
					}
				}
				cb(null);
			},

			//validate localized or default keys exist
			function(emailTemplate, cb){

				function doVal(p, localized, li){
					validationUtils.validateLocalizedOrDefaultKeys(
						[ 'fromEmail', 'fromName', ['bodyHtml', 'bodyText'], 'subject'],
						emailTemplate,
						localized,
						function(errKey){
							if(errKey){
								return p.reject(errors.newError('msv-key-miss', {key: 'localized[' + li + '].' + errKey}));
							}
							p.resolve();
						}
					);
				}

				var promises = [];

				for( var li in emailTemplate.localized){
					var p = q.defer();
					promises.push(p.promise);
					doVal(p, emailTemplate.localized[li], li);
				}

				q.all(promises).done(function(){
					cb();
				}, function(err){
					cb(err);
				});

			},

			//validate localized/default keys
			function(emailTemplate, cb){

				//collect elements
				var collectedItems = validationUtils.collectLocalizedOrDefaultKeys(
					[ 'fromEmail', 'fromName', 'bodyHtml', 'bodyText', 'subject'],
					emailTemplate
				);


				//validate fromName
				for(var i in collectedItems.fromName){
					if( collectedItems.fromName[i][0] !== null
						&& !check.unemptyString(collectedItems.fromName[i][0])
					){
						return cb(errors.newError('msv-inv-val', {key: collectedItems.fromName[i][1]}));
					}
				}

				//validate fromEmail
				for(var i1 in collectedItems.fromEmail){
					if( !check.unemptyString(collectedItems.fromEmail[i1][0]) ){
						return cb(errors.newError('msv-inv-val', {key: collectedItems.fromEmail[i1][1]}));
					}

					if( collectedItems.fromEmail[i1][0].indexOf('@') < 1 ){
						return cb(errors.newError('msv-inv-frm-email', {key: collectedItems.fromEmail[i1][1]}));
					}
				}

				//validate bodyHtml
				for(var i2 in collectedItems.bodyHtml){
					if(collectedItems.bodyHtml[i2][0] === null){
						continue;
					}
					if( !check.unemptyString(collectedItems.bodyHtml[i2][0]) ){
						return cb(errors.newError('msv-inv-val', {key: collectedItems.bodyHtml[i2][1]}));
					}

					try{
						var template = handlebars.compile(collectedItems.bodyHtml[i2][0]);
						template({});
					}catch(e){
						return cb(errors.newError('msv-inv-tpl', {key: collectedItems.bodyHtml[i2][1], originalError: e}));
					}
				}
				
				//validate bodyHtml
				for(var i22 in collectedItems.bodyText){
					if(collectedItems.bodyText[i22][0] === null){
						continue;
					}
					if( !check.unemptyString(collectedItems.bodyText[i22][0]) ){
						return cb(errors.newError('msv-inv-val', {key: collectedItems.bodyText[i22][1]}));
					}

					try{
						var templateT = handlebars.compile(collectedItems.bodyText[i22][0]);
						templateT({});
					}catch(e){
						return cb(errors.newError('msv-inv-tpl', {key: collectedItems.bodyText[i22][1], originalError: e}));
					}
				}

				//validate subject
				for(var i3 in collectedItems.subject){
					if( !check.unemptyString(collectedItems.subject[i3][0]) ){
						return cb(errors.newError('msv-inv-val', {key: collectedItems.subject[i3][1]}));
					}

					try{
						var templateS = handlebars.compile(collectedItems.subject[i3][0]);
						templateS({});
					}catch(e){
						return cb(errors.newError('msv-inv-tpl', {key: collectedItems.subject[i3][1]}));
					}
				}
				cb(null);

			}
		],
		[emailTemplate],
		function(err){
			callback(err);
		}
	);

};

/**
 * Validate template with data
 * @param emailTemplate
 * @param data
 * @param callback
 */
exports.validateTemplateData = function(emailTemplate, data, callback){
	exports.validateEmailTemplate(emailTemplate, function(err){
		if(err){
			return callback(err);
		}

		if( !check.object(data) ){
			return callback(errors.newError('msv-tpld-inv-val', {key: 'data'}));
		}

		var templateKeys = validationUtils.collectLocalizedOrDefaultKeys(['subject', 'bodyHtml', 'bodyText'], emailTemplate);
		var out = {};
		for( var tk in  templateKeys){
			for(var ti in templateKeys[tk]){
				var options = {strict: true};
				//do not compile nulls
				if( templateKeys[tk][ti][0] === null ){
					continue;
				}
				try{
					var template = handlebars.compile(templateKeys[tk][ti][0], options);
					out[templateKeys[tk][ti][1]] = template(data);
				}catch(e){
					return callback(errors.newError('msv-tpld-mis-val', {key: 'data', originalError: e, template: emailTemplate, templateKeys: templateKeys[tk][ti][0]}));
				}
			}
		}

		callback(null, out);

	});

};

/**
 * Validate recipients of one email
 * @param recipients {Object[]} Array of objects like {email: {*String}, name: {String}, type: {to|cc|bcc} }
 * @param callback
 * @returns {*}
 */
exports.validateRecipients = function(recipients, callback){
	//validate recipients
	if( !check.array(recipients) ){
		return callback(errors.newError('msv-inv-type', {type: typeof recipients, requiredType: 'array'}));
	}

	if( check.emptyArray(recipients) ){
		return callback(errors.newError('msv-inv-val', {providedBy: 'validateRecipients'}));
	}

	for(var ti in recipients){
		if( !check.object(recipients[ti]) ){
			return callback(errors.newError('msv-inv-val', {key: '[' + ti + ']', providedBy: 'validateRecipients'}));
		}
		if( !recipients[ti].hasOwnProperty('email') ){
			return callback(errors.newError('msv-key-miss', {key: '[' + ti + '].email', providedBy: 'validateRecipients'}));
		}

		if( !check.unemptyString(recipients[ti].email) ){
			return callback(errors.newError('msv-inv-val', {key: '[' + ti + '].email', providedBy: 'validateRecipients'}));
		}

		if( recipients[ti].email.indexOf('@') < 1 ){
			return callback(errors.newError('msv-inv-to-email', {key: '[' + ti + '].email', providedBy: 'validateRecipients'}));
		}

		if( recipients[ti].hasOwnProperty('name')
			&& !check.unemptyString(recipients[ti].name) ){
			return callback(errors.newError('msv-inv-val', {key: '[' + ti + '].name', providedBy: 'validateRecipients'}));
		}
		var allowedTypes = ['to', 'cc', 'bcc'];
		if( recipients[ti].hasOwnProperty('type') ){
			if(!check.unemptyString(recipients[ti].type)){
				return callback(errors.newError('msv-inv-val', {key: '[' + ti + '].type', providedBy: 'validateRecipients'}));
			}else if( allowedTypes.indexOf(recipients[ti].type) < 0 ){
				return callback(errors.newError('msv-non-ex-val', {key: '[' + ti + '].type', providedBy: 'validateRecipients'}));
			}
		}
	}

	callback(null);
};

/**
 * Validate attachments of one email
 * @param attachments {Object[]} Array of objects like {type: 'text/csv', name: 'report.csv', content: 'base64')
 * @param callback
 * @returns {*}
 */
exports.validateAttachments = function(attachments, callback){
	//validate recipients
	if(!attachments){
		return callback(null);
	}
	if( !check.array(attachments) ){
		return callback(errors.newError('msv-inv-type', {type: typeof attachments, requiredType: 'array', providedBy: 'validateAttachments'}));
	}

	if( check.emptyArray(attachments) ){
		return callback(null);
	}

	for(var ti in attachments){
		if( !check.object(attachments[ti]) ){
			return callback(errors.newError('msv-inv-val', {key: '[' + ti + ']', providedBy: 'validateAttachments'}));
		}
		if( !attachments[ti].hasOwnProperty('type') ){
			return callback(errors.newError('msv-key-miss', {key: '[' + ti + '].type', providedBy: 'validateAttachments'}));
		}

		if( !check.unemptyString(attachments[ti].type) ){
			return callback(errors.newError('msv-inv-val', {key: '[' + ti + '].type', providedBy: 'validateAttachments'}));
		}
		if( !attachments[ti].hasOwnProperty('name')){
			return callback(errors.newError('msv-key-miss', {key: '[' + ti + '].name', providedBy: 'validateAttachments'}));
		}

		if( !check.unemptyString(attachments[ti].name) ){
			return callback(errors.newError('msv-inv-val', {key: '[' + ti + '].name', providedBy: 'validateAttachments'}));
		}

		if( !attachments[ti].hasOwnProperty('content') ){
			return callback(errors.newError('msv-key-miss', {key: '[' + ti + '].content', providedBy: 'validateAttachments'}));
		}

		if( !check.unemptyString(attachments[ti].content) ){
			return callback(errors.newError('msv-inv-val', {key: '[' + ti + '].content', providedBy: 'validateAttachments'}));
		}
	}

	callback(null);
};
