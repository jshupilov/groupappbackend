/**
 * @apiDefine ProfileStructure
 * @apiVersion 1.7.4
 *
 * @apiParam (Profile structure) {String=signedOut,signedIn} status=signedOut Indicates if the user is signed in or not.
 * @apiParam (Profile structure) {Object} account=null Holds the information about the user account
 * @apiParam (Profile structure) {String} account.email The e-mail of the user
 * @apiParam (Profile structure) {String} account.givenName The first name of the user
 * @apiParam (Profile structure) {String} account.surname The surname of the user
 * @apiParam (Profile structure) {Object} account.fullName The full name of the user
 * @apiParam (Profile structure) {Boolean=true,false} account.isEmailVerified Boolean indicating,
 * if the account email has been verified or not
 * @apiParam (Profile structure) {Object} session=null Session information
 * @apiParam (Profile structure) {String} session.sessionStart The timestamp of the session beginning
 * in ISO-8601 standard (YYYY-MM-DDTHH:mm:ss.sssZ)
 * @apiParam (Profile structure) {String} session.maxSessionEnd=null The timestamp of the maximum session end
 * in ISO-8601 standard (YYYY-MM-DDTHH:mm:ss.sssZ)
 * @apiParam (Profile structure) {Object} settings={} Settings object
 * @apiParam (Profile structure) {Language[]} [settings.selectedLanguages] List of selected languages as objects
 * ([About languages](#api-1_Basics-basicsLangs))
 * @apiParam (Profile structure) {Object} view={} Object, holding assets for view
 * @apiParam (Profile structure) {Object} view.localized Object, where each key represents localized information about view
 * @apiParam (Profile structure) {Object} view.localized.LANGUAGE Information about view in language,
 * defined by `LANGUAGE` key.
 * @apiParam (Profile structure) {Object[]} view.localized.LANGUAGE.profileMenu Profile menu as list of menu items, different versions,
 *  one for signedIn and second for signedOut user
 * @apiParam (Profile structure) {String=settings,languages,signOut,conditions,signIn,signUp} view.localized.LANGUAGE.profileMenu.action Action identifier
 * @apiParam (Profile structure) {String} localized.LANGUAGE.menu.name Display name for this action
 * @apiParam (Profile structure) {Boolean=true,false} localized.LANGUAGE.menu.isEnabled Controls if the button is usable (active) or in disabled state (inactive)
 * @apiParam (Profile structure) {Object} view.localized.LANGUAGE.profileMenu.data Contain properties of this action. Every action can have
 * custom properties.
 *
 * `settings` has `link` - link object contain two keys `href` equal to url and `method` can be equal to POST, GET, PUT ...
 *
 * `languages` has `link` -link object contain two keys `href` equal to url and `method` can be equal to POST, GET, PUT ...
 *
 * `signOut` has `link` - link object contain two keys `href` equal to url and `method` can be equal to POST, GET, PUT ...
 *
 * `conditions` has `TermsAndConditionsText` - contain terms and conditions text
 *
 * `signIn` has `name` and `link` - link object contain two keys `href` equal to url and `method` can be equal to POST, GET, PUT ...
 *
 * `signUp` has `url` and `link` - link object contain two keys `href` equal to url and `method` can be equal to POST, GET, PUT ...
 *
 */

/**
 * @api {get} - 1. Profile resource
 * @apiName profileResource
 * @apiVersion 1.7.4
 * @apiGroup 5_Profiles
 * @apiDescription Profile resource represents the state and the settings of the user, whether he/she is logged in or not,
 * with some metadata and settings.
 *
 * @apiUse ProfileStructure
 *
 * @apiParamExample {json} Example structure for logged out user
 {
	"status": "signedOut",
	"account": null,
	"session": null,
	"settings": {
		"selectedLanguages": [
			{
				"code": "en_GB",
				"englishName": "English",
				"nativeName": "English",
				"rtl": false,
				"countryEnglishName": "United Kingdom of Great Britain and Northern Ireland"
			}
		]
	},
	"view": {
        "localized": {
            "en_GB": {
                "profileMenu": [
                    {
                        "action": "settings",
                        "isEnabled": true,
                        "name": "Settings",
                        "data": {
                            "link": {
                                "href": "https://localhost:8000/v1/test-hgid/profiles/60f244eb15c4eb344a0eaaca3b25f1b2f06f7c/settings",
                                "method": "POST"
                            }
                        }
                    },
                    {
                        "action": "languages",
                        "isEnabled": true,
                        "name": "Language",
                        "data": {
                            "link": {
                                "href": "https://localhost:8000/v1/test-hgid/profiles/60f244eb15c4eb344a0eaaca3b25f1b2f06f7c/settings",
                                "method": "POST"
                            }
                        }
                    },
                    {
                        "action": "signOut",
                        "isEnabled": true,
                        "name": "Sign out",
                        "data": {
                            "link": {
                                "href": "https://localhost:8000/v1/test-hgid/profiles/signout",
                                "method": "GET"
                            }
                        }
                    },
                    {
                        "action": "conditions",
                        "isEnabled": true,
                        "name": "T&Cs",
                        "data": {
                            "TermsAndConditionsText": "Please read and review the terms and conditions before using this service.\n\nUser acknowledges and agrees that the Services are for personal use and agrees not to use the Services in a manner prohibited by any federal or state law or regulation.\n\nUser acknowledges that there is content on the Internet or otherwise available through the Services which may be offensive, or which may not be in compliance with all local laws, regulations and other rules. We assume no responsibility for and exercises no control over the content contained on the Internet or is otherwise available through the Services. All content accessed or received by the User is used by User at his or her own risk, and we and our employees shall have no liability resulting from the access or use of such content by the User."
                        }
                    },
                    {
                        "action": "signIn",
                        "isEnabled": true,
                        "name": "Sign in",
                        "data": {
                            "link": {
                                "href": "https://localhost:8000/v1/test-hgid/profiles/signin",
                                "method": "POST"
                            }
                        }
                    },
                    {
                        "action": "signUp",
                        "isEnabled": true,
                        "name": "Sign up",
                        "data": {
                            "link": {
                                "href": "https://localhost:8000/v1/test-hgid/profiles/signup",
                                "method": "POST"
                            }
                        }
                    }
                ]
            }
		}
	},
	"settingsHref": {
		"href": "https://localhost:8000/v1/test-hgid/profiles/6fa40ae916cfe144040cbd9d6364a1adf6343cade2bc40fa411978220cf81054b66b182950267cd99203f06aee40766a/settings",
		"method": "POST"
	},
	"signIn": {
		"href": "https://localhost:8000/v1/test-hgid/profiles/signin",
		"method": "POST"
	},
	"signUp": {
		"href": "https://localhost:8000/v1/test-hgid/profiles/signup",
		"method": "POST"
	},
	"signOut": {
		"href": "https://localhost:8000/v1/test-hgid/profiles/signout",
		"method": "GET"
	}
}
 *
 * @apiParamExample {json} Example structure for logged in user
{
	"status": "signedIn",
	"account": {
		"email": "Yos33emit2e@yopmail.com",
		"givenName": "dfg",
		"surname": "xcvxc",
		"fullName": "dfg xcvxc",
		"isEmailVerified": false
	},
	"session": {
		"sessionStart": "2015-03-20T12:49:17.827Z",
		"maxSessionEnd": null
	},
	"settings": {
		"selectedLanguages": [
			{
				"code": "en_GB",
				"englishName": "English",
				"nativeName": "English",
				"rtl": false,
				"countryEnglishName": "United Kingdom of Great Britain and Northern Ireland"
			}
		]
	},
	"settingsHref": {
		"href": "https://localhost:8000/v1/test-hgid/profiles/53f841bf43cfef1d5d5dbdee2d26e0f1f2697de2b6e31a/settings",
		"method": "POST"
	},
	"signIn": {
		"href": "https://localhost:8000/v1/test-hgid/profiles/signin",
		"method": "POST"
	},
	"signUp": {
		"href": "https://localhost:8000/v1/test-hgid/profiles/signup",
		"method": "POST"
	},
	"signOut": {
		"href": "https://localhost:8000/v1/test-hgid/profiles/signout",
		"method": "GET"
	}
}
 *
 */

/**
 * @api {post} /profiles/signup 2. Sign up
 * @apiName profileSignup
 * @apiVersion 1.7.4
 * @apiGroup 5_Profiles
 * @apiDescription Sign up for an account. If sign up is successful then automatically
 * the sign-in process is also executed - meaning a session for the account is created.
 * Returns a Profile resource.
 *
 * **NB!** Security level `3` is required ([About security](#api-1_Basics-basicsSecurity))
 *
 * @apiParam (POST body)  {String}  email The working and unique email for the user account
 * @apiParam (POST body)  {String}  password The password for the user account
 * @apiParam (POST body)  {String}  givenName Given name of the person
 * @apiParam (POST body)  {String}  surname Surname of the person
 *
 * @apiError (Possible error codes) {String} system-error System error (error in implementation) ([Definition](#error_system-error))
 * @apiError (Possible error codes) {String} ps-inv-inp Invalid type of input given ([Definition](#error_ps-inv-inp))
 * @apiError (Possible error codes) {String} ps-mis-inp Missing some required input ([Definition](#error_ps-mis-inp))
 * @apiError (Possible error codes) {String} ps-session-add Couldn't start a session for the account ([Definition](#error_ps-session-add))
 * @apiError (Possible error codes) {String} ps-weak-pass The password is too weak ([Definition](#error_ps-weak-pass))
 * @apiError (Possible error codes) {String} ps-inv-email The email is invalid ([Definition](#error_ps-inv-email))
 * @apiError (Possible error codes) {String} ps-ex-email The email is not unique ([Definition](#error_ps-ex-email))
 * @apiError (Possible error codes) {String} ps-inv-gn The given name is invalid ([Definition](#error_ps-inv-gn))
 * @apiError (Possible error codes) {String} ps-inv-sn The surname is invalid ([Definition](#error_ps-inv-sn))
 * @apiError (Possible error codes) {String} ps-account-add There was a problem creating the account ([Definition](#error_ps-account-add))
 *
 * @apiExample {curl} Example request
 * POST /profiles/signup HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
{
	"givenName": "Peter",
	"surname": "Griffin",
	"email": "peter.griffin@yopmail.com",
	"password": "0.aAasdasdd"
}
 *
 * @apiSuccess (Response) {Profile} Profile object ([About](#api-5_Profiles-profileResource))
 *
 */

/**
 * @api {post} /profiles/signin 3. Sign in
 * @apiName profileSignin
 * @apiVersion 1.7.4
 * @apiGroup 5_Profiles
 * @apiDescription Authenticates an account and creates a session for this account.
 * Returns a Profile resource.
 *
 * **NB!** Security level `3` is required ([About security](#api-1_Basics-basicsSecurity))
 *
 *
 * @apiParam (POST body)  {String}  email The email of the user account
 * @apiParam (POST body)  {String}  password The password of the user account
 *
 * @apiExample {curl} Example request
 * POST /profiles/signin HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 * {
 *   "email": "peter.griffin@yopmail.com",
 *   "password": "0.aAasdasdd"
 * }
 *
 * @apiError (Possible error codes) {String} ps-mis-inp Missing some input data ([Definition](#error_ps-mis-inp))
 * @apiError (Possible error codes) {String} ps-acc-get Cannot get/find account from Stormpath ([Definition](#error_ps-acc-get))
 * @apiError (Possible error codes) {String} ps-tenant-verify Cannot auto-verify account (This is not verification of email!) ([Definition](#error_ps-tenant-verify))
 * @apiError (Possible error codes) {String} ps-inv-pass Invalid password ([Definition](#error_ps-inv-pass))
 * @apiError (Possible error codes) {String} ps-no-acc Invalid email - no account with this email ([Definition](#error_ps-no-acc))
 * @apiError (Possible error codes) {String} ps-account-auth Some system error preventing user to log in ([Definition](#error_ps-account-auth))
 * @apiError (Possible error codes) {String} ps-session-add Cannot start a session for the user ([Definition](#error_ps-session-add))
 *
 * @apiSuccess (Response) {Profile} Profile object ([About](#api-5_Profiles-profileResource))
 *
 */

/**
 * @api {get} /profiles/status 4. Status
 * @apiName profileStatus
 * @apiVersion 1.7.4
 * @apiGroup 5_Profiles
 * @apiDescription Get the profile and session status. This method should be called to get the status of the session
 * and the settings, which apply. Call it once in a while to ensure that the session still exists
 * (the session might expire or the administration might end the session)
 * Returns a Profile resource.
 *
 * **NB!** Security level `3` is required ([About security](#api-1_Basics-basicsSecurity))
 *
 * @apiExample {curl} Example request
 * GET /profiles/status HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f

 *
 * @apiSuccess (Response) {Profile} Profile object ([About](#api-5_Profiles-profileResource))
 *
 * @apiError (Possible error codes) {String} ps-session-get Error getting session information ([Definition](#error_ps-session-get))
 * @apiError (Possible error codes) {String} ps-acc-get Error getting account information ([Definition](#error_ps-acc-get))

 */

/**
 * @api {get} /profiles/signout 5. Sign out
 * @apiName profileSignout
 * @apiVersion 1.7.4
 * @apiGroup 5_Profiles
 * @apiDescription Terminates the current session (user is logged out).
 * Returns a Profile resource.
 *
 * **NB!** Security level `3` is required ([About security](#api-1_Basics-basicsSecurity))
 *
 * @apiExample {curl} Example request
 * GET /profiles/signout HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 *
 * @apiSuccess (Response) {Profile} Profile object ([About](#api-5_Profiles-profileResource))
 *
 * @apiError (Possible error codes) {String} ps-session-out Error getting account information ([Definition](#error_ps-session-out))
 * @apiError (Possible error codes) {String} ps-acc-get Cannot get/find account from Stormpath ([Definition](#error_ps-acc-get))
 */

/**
 * @api {post} /profiles/resendverificationemail 6. Resend verification email
 * @apiName profileResendverificationemail
 * @apiVersion 1.7.4
 * @apiGroup 5_Profiles
 * @apiDescription Resends the verification email.
 *
 * **NB!** Security level `3` is required ([About security](#api-1_Basics-basicsSecurity))
 *
 * @apiExample {curl} Example request
 * POST /profiles/resendverificationemail HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 * {
 *  "email": "peter.griffin@yopmail.com"
 * }
 *
 * @apiParam (POST body)  {String}  email The email of the user account
 *
 * @apiSuccess (Response) {Boolean} success Boolean indicating if any settings were saved or not
 *
 * @apiError (Possible error codes) {String} ps-resend-failed Resending failed ([Definition](#error_ps-resend-failed))
 * @apiError (Possible error codes) {String} ps-resend-inv Invalid input given ([Definition](#error_ps-resend-inv))

 * @apiSuccessExample {json} Example response
 {
		"success": true
 }
 *
 */

/**
 * @api {post} /profiles/:profileId/settings 7. Update settings
 * @apiName profileSettings
 * @apiVersion 1.7.4
 * @apiGroup 5_Profiles
 * @apiDescription Changes settings of the profile.
 *
 * **NB!** Security level `3` is required ([About security](#api-1_Basics-basicsSecurity))
 *
 * @apiParam (POST body)  {String[]}  selectedLanguages An array of selected language codes
 *
 * @apiExample {curl} Example request
 * POST /profiles/signin HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
{
	"selectedLanguages": ["fr_FR"]
}
 * @apiSuccessExample {json} Example response
{
	"success": true
}
 *
 * @apiError (Possible error codes) {String} ps-acc-get Cannot get/find account from Stormpath ([Definition](#error_ps-acc-get))
 * @apiError (Possible error codes) {String} hg-not-found Error fetching hotel group information ([Definition](#error_hg-not-found))
 * @apiError (Possible error codes) {String} ps-set-inv-inp Selected languages are invalid ([Definition](#error_ps-set-inv-inp))
 * @apiError (Possible error codes) {String} ps-customdata-sav Error occurred while updating account settings ([Definition](#error_ps-customdata-sav))
 * @apiError (Possible error codes) {String} ps-set-inv-inp Invalid request body given ([Definition](#error_ps-set-inv-inp))

 */

/**
 * @api {post} /profiles/sendresetpassword 8. Reset password
 * @apiName profileResetpassword
 * @apiVersion 1.7.4
 * @apiGroup 5_Profiles
 * @apiDescription Send the reset password email to the user. User receives an email, where he/she has a link to a webpage,
 * where he/she enters the new password
 *
 * **NB!** Security level `3` is required ([About security](#api-1_Basics-basicsSecurity))
 *
 * @apiParam (Post body)  {String}  email The email, which accounts password needs to be reset
 *
 * @apiExample {curl} Example request
 * POST /profiles/sendresetpassword HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 * {
 *  "email": "peter.griffin@yopmail.com"
 * }
 *
 * @apiExample {curl} Example request
 * GET /profiles/sendresetpassword?email=asdasd@asdads.ee HTTP/1.1
 * Host: api.cardola.com
 * Accept: application/json
 * X-Cardola-Uuid: 1234-5678-8765-4321
 * X-Cardola-Verify: ojsdf8934nfger89gbwdnifh7804f
 * @apiSuccessExample {json} Example response
 {
	"success": true
}
 *
 * @apiSuccess {String} success=true  Returned if password reset email was sent
 *
 * @apiError (Possible error codes) {String} ps-reset-failed Invalid email given ([Definition](#error_ps-reset-failed))
 * @apiError (Possible error codes) {String} ps-reset-inv No email given ([Definition](#error_ps-reset-inv))
 * @apiError (Possible error codes) {String} au-https-req Tls layer validation failed ([Definition](#error_au-https-req))
 * @apiError (Possible error codes) {String} au-inv-uuid Invalid uuid given ([Definition](#error_au-inv-uuid))
 * @apiError (Possible error codes) {String} au-ver-req Invalid API version ([Definition](#error_au-ver-req))
 * @apiError (Possible error codes) {String} au-inv-verifykey Invalid checksum value ([Definition](#error_au-inv-verifykey))

 */
