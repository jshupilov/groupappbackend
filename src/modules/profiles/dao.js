'use strict';
var databaseUtils = require('../utils/databaseUtils');

/**
 * Find a hotel by ID
 * @param id
 * @param hotelGroupId
 * @param callback
 */
exports.startProfileSession = function(data, callback) {

	databaseUtils.getConnection(function(err, client, done) {
		if(err) {
			callback(err);
		} else {
			client.query(
				'INSERT INTO profile_session (data) values ($1)',
				[data],
				function (err) {
					done();
					if(err){
						callback(err);
					} else {
						callback(null);
					}

				}
			);
		}
	});

};

exports.getLastSessionByUuid = function (uuid, callback){

	databaseUtils.getConnection(function(err, client, done) {
		if (err) {
			callback(err);
		} else {
			client.query(
				'SELECT * ' +
				'FROM profile_session ' +
				'WHERE (data -> \'uuid\' ) = $1 ' +
				'ORDER BY id DESC ' +
				'LIMIT 1 ',
				['"' + uuid + '"'],
				function (err, result) {
					done();
					if (err) {
						callback(err);
					} else {
						var session = null;
						if (result.rows.length > 0) {
							session = result.rows[0];
						}
						callback(err, session);
					}

				}
			);
		}
	});

};

exports.endProfileSession = function(uuid, callback) {
	databaseUtils.getConnection(function(err, client, done) {
		if(err) {
			callback(err);
		} else {
			exports.getLastSessionByUuid(uuid, function (err, result) {
				if (err) {
					done();
					return callback(err);
				} else if( result
					&& !result.data.sessionEnd
				){
					result.data.sessionEnd = new Date().toISOString();
					client.query(
						'UPDATE profile_session ' +
						'SET data = $1 ' +
						'WHERE id = $2',
						[result.data, result.id],
						function (err, result) {
							done();
							if (err) {
								callback(err);
							} else {
								callback(null, result);
							}

						}
					);
				}else{
					done();
					callback(null, result);
				}
			});
		}
	});
};

exports.deleteProfileSession = function(uuid, callback) {
	databaseUtils.getConnection(function(err, client, done) {
		if(err) {
			return callback(err);
		} else {
			client.query(
				'DELETE FROM profile_session ' +
				'WHERE (data -> \'uuid\' ) = $1',
				['"' + uuid + '"'],
				function (err) {
					done();
					if (err) {
						return callback(err);
					} else {
						callback(null);
					}

				}
			);
		}

	});
};
