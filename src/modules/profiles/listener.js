/**
 * Created by jevgenishupilov on 19/01/15.
 */
'use strict';
var eventUtils = require('../utils/eventUtils');
var services = require('./services');

function createDirs(data, callback){
	services.createDirectories(data.info.name, data.hotelGroupId, function(err, profilesConfiguration){
		if(err){
			callback(err);
		} else {
			data.profilesConfiguration = profilesConfiguration;
			callback(null);
		}
	});
}

eventUtils.emitter.on('hotelGroup.beforeUpdate', createDirs);

eventUtils.emitter.on('hotelGroup.beforeCreate', createDirs);
