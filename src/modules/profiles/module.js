'use strict';
var services = require('./services');
var errors = require('../utils/errors');
var apiUtils = require('../utils/apiUtils');

var profileModule = {
	register: function(server, options, next) {
		// Add the route
		server.route({
			method: 'GET',
			path: '/profiles/status',
			handler: function(request, reply) {
				apiUtils.ensureSecurity(3, request.apiRequest, function(securityError){
					if(securityError){
						return reply(errors.wrapToBoom(securityError));
					}
					services.getProfileStatus(request.apiRequest, function(err, res) {
						apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res), 'noCache');
					});
				});

			}
		});
		server.route({
			method: 'POST',
			path: '/profiles/signup',
			handler: function(request, reply) {
				apiUtils.ensureSecurity(4, request.apiRequest, function(securityError) {
					if(securityError) {
						return reply(errors.wrapToBoom(securityError));
					}
					services.signUp(request.apiRequest, function(err, res) {
						reply(errors.wrapToBoom(err), res);
					});
				});
			}
		});
		server.route({
			method: 'POST',
			path: '/profiles/signin',
			handler: function(request, reply) {
				apiUtils.ensureSecurity(4, request.apiRequest, function(securityError) {
					if(securityError) {
						return reply(errors.wrapToBoom(securityError));
					}
					services.signIn(request.apiRequest, function(err, res) {
						reply(errors.wrapToBoom(err), res);
					});
				});
			}
		});
		server.route({
			method: 'GET',
			path: '/profiles/signout',
			handler: function(request, reply) {
				apiUtils.ensureSecurity(3, request.apiRequest, function(securityError) {
					if(securityError) {
						return reply(errors.wrapToBoom(securityError));
					}
					services.signOut(request.apiRequest, function(err, res) {
						apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res), 'noCache');
					});
				});
			}
		});
		server.route({
			method: 'GET',
			path: '/profiles/verify',
			handler: function(request, reply) {
				services.verify(request.apiRequest, function(err, res) {
					return reply.view('profiles/verified', {error: err, res: res});
				});
			}
		});

		server.route({
			method: 'POST',
			path: '/profiles/resendverificationemail',
			handler: function(request, reply) {
				apiUtils.ensureSecurity(3, request.apiRequest, function(securityError) {
					if(securityError) {
						return reply(errors.wrapToBoom(securityError));
					}
					services.resendVerificationEmail(request.apiRequest, function(err, res) {
						reply(errors.wrapToBoom(err), res);
					});
				});
			}
		});
		server.route({
			method: 'POST',
			path: '/profiles/{profileId}/settings',
			handler: function(request, reply) {
				apiUtils.ensureSecurity(3, request.apiRequest, function(securityError) {
					if(securityError) {
						return reply(errors.wrapToBoom(securityError));
					}
					services.settings(request.apiRequest, function(err, res) {
						reply(errors.wrapToBoom(err), res);
					});
				});
			}
		});

		server.route({
			method: 'POST',
			path: '/profiles/sendresetpassword',
			handler: function(request, reply) {
				apiUtils.ensureSecurity(3, request.apiRequest, function(securityError) {
					if(securityError) {
						return reply(errors.wrapToBoom(securityError));
					}
					services.sendResetPass(request.apiRequest, function(err, res) {
						reply(errors.wrapToBoom(err), res);
					});
				});
			}
		});
		server.route({
			method: ['GET', 'POST'],
			path: '/profiles/resetpassword',
			handler: function(request, reply) {

				apiUtils.ensureSecurity('htmlFormSecure', request.apiRequest, function(securityError) {
					if(securityError) {
						return reply.view('profiles/resetpassword', {error: securityError, hide: true});
					}
					services.verifyNewPassToken(request.apiRequest, function(err, res){
						var hide = false;
						if(err && err.data.code === 'ps-resettoken-inv' ){
							hide = true;
						}else if(res && res.success){
							hide = true;
						}
						return reply.view('profiles/resetpassword', {error: err, res: res, hide: hide});
					});
				});

			}
		});
		next();
	}
};

profileModule.register.attributes = {
	name: 'Profiles',
	version: '1.0.0'
};

exports.register = profileModule.register;
exports.services = services;
