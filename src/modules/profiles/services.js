'use strict';
var apiUtils = require('../utils/apiUtils');
var langUtils = require('../utils/langUtils');
var cryptoUtils = require('../utils/cryptoUtils');
var dao = require('./dao');
var hgServices = require('../hotel_groups/services');
var msgServices = require('../messaging/services');
var sp = require('stormpath');
var check = require('check-types');
var uuid = require('node-uuid');
var errors = require('../utils/errors');
var emailCryptoPass = '^YpV!rJmtX4_hs^';
var stormPathClient, stormPathApplication;

errors.defineError('ps-as-add', 'Account store was not added', 'Account store, with given parameters can not be added', 'profiles', 400);
errors.defineError('ps-account-add', 'Account was not added', 'Account, with given parameters can not be added', 'profiles', 400);
errors.defineError('ps-account-auth', 'Account was not authenticated', 'Account, with given parameters can not be authenticated', 'profiles', 400);
errors.defineError('ps-inv-body', 'System error', 'Request body has invalid value', 'profiles');
errors.defineError('ps-app-get', 'Applications was not got', 'Application, with given parameters can not be found', 'profiles');
errors.defineError('ps-acc-get', 'Account was not found', 'Account, with given parameters can not be found', 'profiles', 400);
errors.defineError('ps-session-add', 'Applications was not got', 'Application, with given parameters can not be found', 'profiles');
errors.defineError('ps-session-out', 'Application can not update sessionEnd', 'Application can not update sessionEnd', 'profiles');
errors.defineError('ps-session-get', 'Application can not get last session', 'Application can not get last session details for this profile', 'profiles');
errors.defineError('ps-session-inv', 'System error', 'You have invalid session details', 'profiles');
errors.defineError('ps-dir-get', 'System error', 'Cannot get Stormpath directory', 'profiles');
errors.defineError('ps-dir-add', 'System error', 'Directory, with given parameters can not be added', 'profiles');
errors.defineError('ps-dir-upd', 'System error', 'Directory, with given parameters can not be updated', 'profiles');
errors.defineError('ps-tenant-verify', 'Application can not verify user', 'Application can not verify user account', 'profiles');
errors.defineError('ps-token-add', 'Application can not add token', 'Application can not add to user account custom data', 'profiles');
errors.defineError('ps-customdata-sav', 'Application can not save custom data of account', 'Application can not get information of account custom data', 'profiles');
errors.defineError('ps-verify-inv', 'Verification is invalid', 'Verification key is invalid', 'profiles', 400);
errors.defineError('ps-verify-failed', 'Verification failed', 'This page does not exist', 'profiles', 404);
errors.defineError('ps-resend-failed', 'Email verification resending failed', 'Email verification resending failed.', 'profiles');
errors.defineError('ps-reset-failed', 'Email is invalid', 'Email is invalid or does not exist in db.', 'profiles');
errors.defineError('ps-reset-inv', 'Email is required', 'Password reset is not valid. Check PayLoad details. Email is required', 'profiles', 400);
errors.defineError('ps-resend-inv', 'Email verification resending not valid', 'Email verification resending not valid. Check PayLoad details. Login information is necessary', 'profiles', 400);
errors.defineError('ps-no-acc', 'Invalid credentials', 'User with this email does not exist', 'profiles', 400);
errors.defineError('ps-inv-pass', 'Invalid credentials', 'Invalid password entered', 'profiles', 400);
errors.defineError('ps-mis-inp', 'Invalid credentials', 'Missing input parameter', 'profiles', 400);
errors.defineError('ps-inv-inp', 'Invalid credentials', 'Invalid input parameter', 'profiles', 400);
errors.defineError('ps-weak-pass', 'Password must be at least 8 characters long and contain numbers and uppercase letters', 'Too weak password', 'profiles', 400);
errors.defineError('ps-inv-email', 'Invalid email', 'Email is invalid', 'profiles', 400);
errors.defineError('ps-inv-gn', 'Invalid given name', 'Given name is invalid', 'profiles', 400);
errors.defineError('ps-inv-sn', 'Invalid surname', 'Surname is invalid', 'profiles', 400);
errors.defineError('ps-ex-email', 'Account with this email already exists', 'Email is not unique!', 'profiles', 400);
errors.defineError('ps-set-inv-inp', 'System error', 'Invalid input type', 'profiles', 400);
errors.defineError('ps-frmt-res', 'System error', 'Error occurred while formatting the response', 'profiles');
errors.defineError('ps-resettoken-inv', 'This link is invalid or has expired!', 'Error occurred with token validation', 'profiles');
errors.defineError('ps-resetpass-inv', 'Please check your input, something is not right!', 'Error occurred with inputs, missing input parameters', 'profiles');
errors.defineError('ps-resetpassmatch-inv', 'Passwords do not match', 'Passwords do not match', 'profiles');

/**
 * Initialize the Stormpath API client
 * @param options
 */
exports.initStormpathClient = function(options){
	var spApiKey = new sp.ApiKey(process.env.STORMPATH_API_KEY_ID, process.env.STORMPATH_API_KEY_SECRET);
	options = options || {};
	options.apiKey = spApiKey;
	stormPathClient = new sp.Client(options);
	return stormPathClient;
};

/**
 * Get the Stormpath API client
 * @returns {*}
 */
exports.getStormpathClient = function(){
	if(stormPathClient){
		return stormPathClient;
	}
	return exports.initStormpathClient();
};

/**
 * Get the Stormpath Application instance
 * @param callback
 * @returns {*}
 */
exports.getStormpathApplication = function(callback){
	if(stormPathApplication){
		return callback(null, stormPathApplication);
	}
	exports.getStormpathClient().getApplication(process.env.STORMPATH_URL, function(err, application) {
		if(err) {
			if(err.inner){
				return callback(errors.newError('ps-app-get', {originalError: err, info: 'Network Error', providedBy: 'init'}) );
			}
			return callback(errors.newError('ps-app-get', {originalError: err}));
		}
		stormPathApplication = application;
		return callback(null, stormPathApplication);

	});
};

/**
 * Get new profile session with default properties
 * @param uuid
 * @param email
 */
function getNewProfileSession(uuid, email){
	return {
		'sessionStart': new Date().toISOString(),
		'sessionEnd': null,
		'uuid': uuid,
		'email': email,
		'maxSessionEnd': null
	};

}

/**
 * Get base profile email
 * @param uuid
 * @return {String} email
 */
function getBaseProfileEmail(uuid){
	return uuid + '@cardola.net';

}

/**
 * Configure StormPath directory workflow settings
 * @param href {String} Directory href
 * @param property {String} Policy name
 * @param config {Object} Policy configuration object
 * @param template {String} Email template name
 * @param callback
 */
function configureDirectory(config, callback){

	exports.getStormpathClient()._dataStore.saveResource(config, function (err) {
		if (err) {
			if(err.inner){
				return callback(errors.newError('ps-dir-upd', {originalError: err, info: 'Network Error', providedBy: 'configureDirectory'}) );
			}
			return callback(errors.newError('ps-dir-upd', {originalError: err, providedBy: 'configureDirectory'}));
		} else {
			callback(null);
		}
	});
}

/**
 * Create directory in StormPath
 * @param hotelGroupId
 * @param dirName
 * @param hotelGroupInfo
 * @param isBase
 * @param callback
 */
function createDirectory(hotelGroupId, dirName, hotelGroupInfo, isBase, callback){
	exports.getStormpathClient().getDirectories({name: dirName}, function(err, directories) {
		if (err) {
			if(err.inner){
				return callback(errors.newError('ps-dir-get', {originalError: err, info: 'Network Error', providedBy: 'createDirectory'}) );
			}
			return callback(errors.newError('ps-dir-get', {originalError: err, providedBy: 'createDirectory'}));
		} else {
			if (directories.items.length > 0) {
				callback(null, directories.items[0]);
			} else {
				exports.getStormpathClient().createDirectory({name: dirName, description: hotelGroupInfo}, function (err1, dir) {
					if (err1) {
						if(err1.inner){
							return callback(errors.newError('ps-dir-add', {originalError: err1, info: 'Network Error', providedBy: 'createAccount'}), dir );
						}
						return callback(errors.newError('ps-dir-add', {originalError: err1}), dir);
					} else {
						var accountPolicy = {
							href: dir.accountCreationPolicy.href,
							verificationEmailStatus: 'DISABLED',
							verificationSuccessEmailStatus: 'DISABLED',
							welcomeEmailStatus: 'DISABLED'
						};
						var passwordPolicy = {
							href: dir.passwordPolicy.href,
							resetEmailStatus: 'DISABLED',
							resetSuccessEmailStatus: 'DISABLED'
						};

						if (isBase) {
							accountPolicy.verificationEmailStatus = 'DISABLED';
							passwordPolicy.resetEmailStatus = 'DISABLED';
						}

						configureDirectory(accountPolicy, function(err2){
							if (err2) {
								callback(err2, dir);
							} else {
								configureDirectory(passwordPolicy, function(err3){
									if (err3) {
										callback(err3, dir);
									} else {
										stormPathApplication.addAccountStore(dir, function(err4) {
											if (err4) {
												if(err4.inner){
													return callback(errors.newError('ps-as-add', {originalError: err4, info: 'Network Error', providedBy: 'createAccount'}), dir );
												}
												return callback(errors.newError('ps-as-add', {originalError: err4}), dir);
											}
											callback(null, dir);
										});
									}
								});
							}
						});
					}
				});
			}
		}
	});
}



/**
 * Get user account by options in StormPath
 * @param directoryUrl
 * @param email
 * @param callback
 */
exports.getAccount = function (directoryUrl, email, callback){
	if(!check.unemptyString(email)){
		return callback(errors.newError('ps-acc-get', {reason: 'invalidEmailGiven'}));
	}
	exports.getStormpathClient().getDirectory(directoryUrl, {expand: 'accounts'}, function(err, directory) {
		if (err) {
			if(err.inner){
				return callback(errors.newError('ps-dir-get', {originalError: err, info: 'Network Error', providedBy: 'getAccount'}) );
			}
			return callback(errors.newError('ps-dir-get', {originalError: err, providedBy: 'getAccount'}) );
		}
		directory.getAccounts({email: email, expand: 'customData'}, function (err, accounts) {
			if (err) {
				if(err.inner){
					return callback(errors.newError('ps-acc-get', {originalError: err, info: 'Network Error', providedBy: 'getAccount'}) );
				}
				return callback(errors.newError('ps-acc-get', {originalError: err}), null);
			}
			if (accounts.items.length === 0) {
				return callback(errors.newError('ps-acc-get', {reason: 'no accounts in stormpath', givenEmail: email}));
			}

			return callback(null, accounts.items[0]);
		});
	});
};

/**
 * Get user account by hotelGroupId
 * @param hgid
 * @param email
 * @param callback
 */
exports.getAccountByHgid = function(hgid, email, callback) {
	hgServices.getHotelGroup(hgid, function(err, res) {
		if(err) {
			return callback(err);
		}
		var dirHref = res.data.profilesConfiguration.spDirectoryHref;
		exports.getAccount(dirHref, email, function(err, res) {
			callback(err, res);
		});
	});
};

/**
 * Format session before send response
 * @param session
 */
function formatSession(session){
	var out = null;
	if(session) {
		out = {
			sessionStart: session.sessionStart,
			maxSessionEnd: session.maxSessionEnd
		};
	}
	return out;
}

/**
 * Format account before send response
 * @param account
 */
function formatAccount(account){
	var out = null;
	if(account) {
		out = {
			email: account.email,
			givenName: account.givenName,
			surname: account.surname,
			fullName: account.fullName,
			isEmailVerified: account.customData.isEmailVerified ? true : false

		};
	}
	return out;
}

/**
 * Format account for stormapt
 * @param email
 * @param password
 * @param [givenName]
 * @param [surname]
 * @returns {{email: *, password: *, givenName: *, surname: *}}
 */
function formatAccountForStormpath(hotelGroupId, email, password, givenName, surname){
	var out = {
		email: email,
		password: password,
		givenName: givenName,
		surname: surname,
		customData: {
			settings: {}
		}
	};
	out.customData.settings[hotelGroupId] = {};

	return out;
}

function formatProfileMenu(hotelGroup, online, settings, signOut, signIn, signUp){
	var view = {
		localized: {}
	};
	hotelGroup.supportedLanguages.forEach(function(lang){
		view.localized[lang] = {
			profileMenu: []
		};
		if( hotelGroup.viewConfiguration.profileMenu ){
			hotelGroup.viewConfiguration.profileMenu.forEach(function(menuItem){
				switch(menuItem.action){
					case 'settings':
						apiUtils.populateMenu(
							menuItem,
							online || !online,
							['link'],
							[settings],
							view.localized[lang].profileMenu,
							langUtils.getTranslation(hotelGroup.translations, 'SettingsButton', lang)
						);
						break;
					case 'languages':
						apiUtils.populateMenu(
							menuItem,
							online || !online,
							['link'],
							[settings],
							view.localized[lang].profileMenu,
							langUtils.getTranslation(hotelGroup.translations, 'LanguageButton', lang)
						);
						break;
					case 'signOut':
						apiUtils.populateMenu(
							menuItem,
							online,
							['link'],
							[signOut],
							view.localized[lang].profileMenu,
							langUtils.getTranslation(hotelGroup.translations, 'SignOutButton', lang)
						);
						break;
					case 'conditions':
						apiUtils.populateMenu(
							menuItem,
							true,
							['TermsAndConditionsText', 'link'],
							[langUtils.getTranslation(hotelGroup.translations, 'TermsAndConditionsText', lang), menuItem.link],
							view.localized[lang].profileMenu,
							langUtils.getTranslation(hotelGroup.translations, 'TermsAndConditionsButtonShort', lang)
						);
						break;
					case 'signIn':
						apiUtils.populateMenu(
							menuItem,
							!online,
							['link'],
							[signIn],
							view.localized[lang].profileMenu,
							langUtils.getTranslation(hotelGroup.translations, 'SignInButton', lang)
						);
						break;
					case 'signUp':
						apiUtils.populateMenu(
							menuItem,
							!online,
							['link'],
							[signUp],
							view.localized[lang].profileMenu,
							langUtils.getTranslation(hotelGroup.translations, 'SignUpButton', lang)
						);
						break;
					case 'privacy':
						apiUtils.populateMenu(
							menuItem,
							true,
							['PrivacyText', 'link'],
							[langUtils.getTranslation(hotelGroup.translations, 'PrivacyText', lang), menuItem.link],
							view.localized[lang].profileMenu,
							langUtils.getTranslation(hotelGroup.translations, 'PrivacyButtonShort', lang)
						);
						break;
					case 'about':
						apiUtils.populateMenu(
							menuItem,
							true,
							['AboutText'],
							[langUtils.getTranslation(hotelGroup.translations, 'AboutText', lang)],
							view.localized[lang].profileMenu,
							langUtils.getTranslation(hotelGroup.translations, 'AboutButtonShort', lang)
						);
						break;
					case 'help':
						apiUtils.populateMenu(
							menuItem,
							true,
							['HelpText'],
							[langUtils.getTranslation(hotelGroup.translations, 'HelpText', lang)],
							view.localized[lang].profileMenu,
							langUtils.getTranslation(hotelGroup.translations, 'HelpButtonShort', lang)
						);
						break;
				}
			});
		}

	});
	return view;
}

/**
 * Get profile response structure
 * @param status
 * @param account
 * @param session
 * @param settings
 * @throws Error if error occurs
 */
function getProfileResponse(email, status, account, session, settings, hotelGroup){

	var hotelGroupId = hotelGroup.hotelGroupId;
	if(settings
		&& settings.selectedLanguages
	){
		var langs = [];
		settings.selectedLanguages.forEach(function(langCode){
			langs.push(langUtils.getLanguageObject(langCode));
		});
		settings.selectedLanguages = langs;
	}
	var href = null, enEmaill = '';

	if(email){
		try{
			enEmaill = exports.encryptEmail(email);
		} catch (e) {
			throw errors.newError('cu-string-inv');
		}
		href = apiUtils.getServiceLink(hotelGroupId, '/profiles/' + enEmaill + '/settings', {}, 'POST');
	}

	return {
		status: status,
		account: formatAccount(account),
		session: formatSession(session),
		settings: settings,
		view: formatProfileMenu(
			hotelGroup,
			status === 'signedIn' ? true : false,
			href,
			apiUtils.getServiceLink(hotelGroupId, '/profiles/signout', {}, 'GET'),
			apiUtils.getServiceLink(hotelGroupId, '/profiles/signin', {}, 'POST'),
			apiUtils.getServiceLink(hotelGroupId, '/profiles/signup', {}, 'POST')
		),
		settingsHref: href,
		signIn: apiUtils.getServiceLink(hotelGroupId, '/profiles/signin', {}, 'POST'),
		signUp: apiUtils.getServiceLink(hotelGroupId, '/profiles/signup', {}, 'POST'),
		signOut: apiUtils.getServiceLink(hotelGroupId, '/profiles/signout', {}, 'GET'),
		resendVerificationEmail: apiUtils.getServiceLink(hotelGroupId, '/profiles/resendverificationemail', {}, 'POST'),
		resetPassword: apiUtils.getServiceLink(hotelGroupId, '/profiles/sendresetpassword', {}, 'POST')
	};
}


/**
 * Create user account in Stormpath and return its contents
 * @param hotelGroupId
 * @param validatedData {Object} Validated data
 * @param directoryUrl {String} Stormpath directory URL
 * @param callback
 */
function createAccount(hotelGroupId, validatedData, directoryUrl, callback){
	var formattedData = formatAccountForStormpath(
		hotelGroupId,
		validatedData.email,
		validatedData.password,
		validatedData.givenName,
		validatedData.surname
	);
	exports.getStormpathClient().getDirectory(directoryUrl, {expand: 'accounts'}, function(err, directory) {
		if(err) {
			if(err.inner){
				return callback(errors.newError('ps-dir-get', {originalError: err, info: 'Network Error', providedBy: 'createAccount'}) );
			}
			return callback(errors.newError('ps-dir-get', {originalError: err, providedBy: 'createAccount'}));
		}
		directory.createAccount(formattedData, {expand: 'customData'}, function (errCA, account) {
			if (errCA) {
				if(errCA.inner){
					return callback(errors.newError('ps-account-add', {originalError: errCA, info: 'Network Error', providedBy: 'createAccount'}) );
				}
				var um = errCA.userMessage.toLocaleLowerCase();
				if(um.indexOf('password') > -1){
					return callback(errors.newError('ps-weak-pass', {originalError: errCA, providedBy: 'createAccount'}));
				} else if (um.indexOf('email') > -1) {
					if (errCA.code === 2001){
						return callback(errors.newError('ps-ex-email', {originalError: errCA}));
					}
					return callback(errors.newError('ps-inv-email', {originalError: errCA}));
				} else if(um.indexOf('givenname') > -1) {
					return callback(errors.newError('ps-inv-gn', {originalError: errCA}));
				} else if(um.indexOf('surname') > -1) {
					return callback(errors.newError('ps-inv-sn', {originalError: errCA}));
				}

				return callback(errors.newError('ps-account-add', {originalError: errCA, givenData: validatedData}));
			}

			callback(null, account);
		});
	});

}

/**
 * Inherit settings etc from base account
 * @param uuid
 * @param hotelGroupId
 * @param account
 * @param profilesConfiguration
 * @param callback
 */
function inheritFromBaseAccount(uuid, hotelGroupId, account, profilesConfiguration, callback){
	exports.getAccount(profilesConfiguration.spBaseDirectoryHref, getBaseProfileEmail(uuid), function(err, baseAccount){
		if (err) {
			return callback(err);
		}

		account.customData.baseAccountEmail = baseAccount.email;
		//inherit all settings
		account.customData.settings[hotelGroupId] = baseAccount.customData.settings[hotelGroupId];

		callback(null);
	});
}

/**
 * Start a new profile session
 * @param uuid
 * @param email
 * @param callback
 */
function startProfileSession(uuid, email, callback){
	dao.endProfileSession(uuid, function(err){
		if(err){
			return callback(err);
		}

		var session = getNewProfileSession(uuid, email);
		dao.startProfileSession(session, function (err) {
			if(err){
				return callback(err);
			}
			callback(null, session);
		});

	});
}

/**
 * Generate a new email verification token and add it to the account customData
 * @param account
 */
function generateEmailVerificationToken(account){
	//save email verification token
	//token
	if(!account.customData.hasOwnProperty('emailVerificationToken')){
		account.customData.emailVerificationToken = [];
		//convert old format to array
	}else if( !Array.isArray(account.customData.emailVerificationToken) ){
		account.customData.emailVerificationToken = [account.customData.emailVerificationToken];
	}
	var token = cryptoUtils.sha256(uuid.v4() + (new Date().getTime()));
	account.customData.emailVerificationToken.push(token);
	return token;
}


/**
 * Generate a new password reset token and add it to the account customData
 * @param account
 */
function generatePasswordResetToken(account){
	var validDays = 1;
	var token = cryptoUtils.sha256(uuid.v4() + (new Date().getTime()));
	var createdAt = new Date();
	var validUntil = new Date(createdAt.getTime());
	validUntil.setDate( createdAt.getDate() + validDays );
	account.customData.passwordResetToken = {
		token: token,
		createdAt: createdAt.toISOString(),
		validUntil: validUntil.toISOString()
	};

	return {
		token: token,
		validDays: validDays,
		validUntil: validUntil
	};
}

/**
 * Verify password reset token
 * @param account
 * @param token
 * @returns {boolean}
 */
function verifyPasswordResetToken(account, token){
	if( check.object(account.customData.passwordResetToken)){
		if(account.customData.passwordResetToken.token === token){
			var currentDate = new Date().getTime();
			var validUntilDate = new Date(account.customData.passwordResetToken.validUntil).getTime();
			if(validUntilDate >= currentDate){
				return true;
			}
		}
	}
	return false;
}

/**
 * Check if given email verification token is valid
 * @param account
 * @param token
 * @returns {boolean}
 */
function verifyEmailVerificationToken(account, token){
	return account.customData.emailVerificationToken.indexOf(token) > -1;
}

/**
 * Validate account data
 * @param data
 * @param action
 * @param callback
 * @returns {*}
 */
exports.validateAccountData = function(data, action, callback){
	var reqFields = [];
	switch(action){
		case 'signup':
			reqFields.push('givenName');
			reqFields.push('surname');
		case 'signin':
			reqFields.push('email');
			reqFields.push('password');
			break;
		default:
			return callback(errors.newError('system-error', {givenAction: action}));
	}
	if(typeof data !== 'object'){
		return callback(errors.newError('ps-inv-inp', {error: 'input must be an object', inputTypeReceived: typeof data}));
	}
	//check required fields
	for( var i in reqFields){
		var f = reqFields[i];
		if(!data.hasOwnProperty(f)){
			return callback(errors.newError('ps-mis-inp', {missingField: f, providedBy: 'validateAccountData'}));
		}
		if(typeof data[f] !== 'string'){
			return callback(errors.newError('ps-inv-inp', {invalidField: f}));
		}
	}

	callback(null, data);
};

/**
 * Validate session
 * @param apiRequest
 * @param session
 * @param callback
 * @returns {*}
 */
exports.validateSession = function(apiRequest, session, callback){
	if(apiRequest.uuid === session.uuid){
		if(apiRequest.headers['x-cardola-access-token']){
			if(session.account){
				var token = apiRequest.headers['x-cardola-access-token'].split('_');
				try {
					var account = JSON.parse(cryptoUtils.strongDecryption(token[0], token[1], session.account));
				} catch (e){
					return callback(errors.newError('ps-session-inv', {info: 'Invalid profile decryption', originalError: e}));
				}
				return callback(null, { account: account });
			} else {
				return callback(errors.newError('ps-session-inv', {info: 'Profile details are missing in session'}));
			}
		} else {
			if(session.account){
				return callback(errors.newError('ps-session-inv', {info: 'Access token is missing in header'}));
			} else {
				return callback(null, { account: null });
			}
		}
	} else {
		return callback(errors.newError('ps-session-inv', {info: 'Header uuid not equal to session uuid'}));
	}
};

/**
 * Init connection to StormPath application
 * @param callback
 */
exports.init = function (callback){
	exports.getStormpathApplication(callback);
};

/**
 * Create base profile to StormPath
 * @param apiRequest
 * @param callback
 */
exports.createBaseAccount = function(apiRequest, callback) {
	if(!apiRequest.uuid){
		return callback(errors.newError('system-error', {message: 'No UUID given for base account creation'}));
	}
	var data = {
		email: getBaseProfileEmail(apiRequest.uuid),
		password: '34rtergItDsdfsfdoessfsdfNotMatter123',
		givenName: 'Base',
		surname: 'User'
	};

	hgServices.getProfilesConfiguration(apiRequest.hotelGroupId, function(err, profilesConfiguration){
		if (err) {
			return callback(err);
		}
		createAccount(apiRequest.hotelGroupId, data, profilesConfiguration.spBaseDirectoryHref, function (err, account) {
			if (err) {
				return callback(err);
			}
			callback(null, account);
		});
	});
};

/**
 * Signup profile to StormPath
 * @param apiRequest
 * @param callback
 */
exports.signUp = function(apiRequest, callback) {
	exports.validateAccountData(apiRequest.body, 'signup', function(err, validatedData){
		//validation failed
		if(err){
			return callback(err);
		}

		hgServices.getHotelGroup(apiRequest.hotelGroupId, function(err, hotelGroup){
			if(err){
				return callback(err);
			}
			var hotelGroupData = hotelGroup.data;

			//create account to stormpath
			createAccount(apiRequest.hotelGroupId, validatedData, hotelGroupData.profilesConfiguration.spDirectoryHref, function(err, account) {
				if(err) {
					return callback(err);
				}
				//inherit stuff from base account
				inheritFromBaseAccount(apiRequest.uuid, apiRequest.hotelGroupId, account, hotelGroupData.profilesConfiguration, function(err) {
					if(err) {
						return callback(err);
					}

					//send verification email and save custom data
					exports.sendVerificationEmail(account, hotelGroupData, function(err){
						if (err) {
							return callback(err);
						}
						//start the session
						startProfileSession(apiRequest.uuid, account.email, function(err, session){
							if (err) {
								return callback(err);
							}
							try {
								var res = getProfileResponse(
									account.email,
									'signedIn',
									account,
									session,
									account.customData.settings[apiRequest.hotelGroupId],
									hotelGroupData
								);
							} catch (e) {
								return callback(errors.newError('ps-frmt-res', {originalError: e.toString(), providedBy: 'signUp'}));
							}
							callback(null, res);
						});
					});

				});

			});

		});


	});

};
/**
 * Signin profile to StormPath
 * @param apiRequest
 * @param callback
 */
exports.signIn = function(apiRequest, callback) {

	if(apiRequest.body) {
		var reqf = ['email', 'password'];
		//using email and password for authentication
		for( var i in reqf){
			var f = reqf[i];
			if(!apiRequest.body.hasOwnProperty(f)){
				return callback(errors.newError('ps-mis-inp', {missingField: f, providedBy: 'signIn'}));
			}
		};
		var data = {
			username: apiRequest.body.email,
			password: apiRequest.body.password
		};

		stormPathApplication.authenticateAccount(data, function (err, result) {
			if (err) {
				if(err.inner){
					return callback(errors.newError('ps-account-auth', {originalError: err, info: 'Network error', providedBy: 'authenticateAccount'}));
				}
				if (err.code === 7100){
					callback(errors.newError('ps-inv-pass', {originalError: err}));
				} else if (err.code === 7104){
					callback(errors.newError('ps-no-acc', {originalError: err}));
				} else {
					callback(errors.newError('ps-account-auth', {originalError: err, providedBy: 'authenticateAccount'}));
				}
			} else {
				//start the session
				startProfileSession(apiRequest.uuid, data.username, function(errSPS, session){
					if(errSPS) {
						return callback( errors.newError('ps-session-add', {originalError: errSPS}));
					}
					result.getAccount({expand: 'customData'}, function(errGA, account){
						if(errGA) {
							if(errGA.inner){
								return callback(errors.newError('ps-acc-get', {originalError: errGA, info: 'Network Error', providedBy: 'signIn'}) );
							}
							return callback(errGA);
						}
						hgServices.getHotelGroup(apiRequest.hotelGroupId, function(errGHG, hgData) {
							if (errGHG) {
								return callback(errGHG);
							}
							try{

								var hotelGroup = hgData.data;
								var res = getProfileResponse(
									account.email,
									'signedIn',
									account,
									session,
									account.customData.settings[apiRequest.hotelGroupId],
									hotelGroup
								);

							}catch (e){
								return callback(errors.newError('ps-frmt-res', {originalError: e.toString(), providedBy: 'signIn'}));
							}
							callback(null, res);
						});
					});
				});

			}

		});
	} else {
		callback(errors.newError('ps-inv-body', {RequestBodyError: apiRequest.body}));
	}
};

/**
 * Create new session, store to DB and return to customer
 * @param apiRequest
 * @param hgid
 * @param email
 * @param callback
 */
exports.createSession = function(apiRequest, hgid, email, callback) {
	exports.getAccountByHgid(hgid, email, function(err, account){
		if(err){
			return callback(err);
		}
		var key = cryptoUtils.getRandomValue(1);
		var iv = cryptoUtils.createSymCryptIv();
		var token = key+'_'+iv;
		var cryptAcc = cryptoUtils.strongEncryption(key, iv, JSON.stringify(account));
		var session = {
			'uuid': apiRequest.uuid,
			'email': email,
			'sessionStart': new Date().toISOString(),
			'sessionEnd': null,
			'maxSessionEnd': null,
			'account': cryptAcc
		};
		dao.startProfileSession(session, function(err){
			callback(err,
				{
					'session': session,
					'account': account,
					'accessToken': token
				});

		});


	});
};



/**
 * Signout profile to StormPath
 * @param apiRequest
 * @param callback
 */
exports.signOut = function(apiRequest, callback) {

	dao.endProfileSession(apiRequest.uuid, function(err){
		if(err) {
			callback(errors.newError('ps-session-out', {originalError: err}));
		} else {
			var email = getBaseProfileEmail(apiRequest.uuid);
			hgServices.getHotelGroup(apiRequest.hotelGroupId, function(err, hgData) {
				if (err) {
					return callback(err);
				}
				var hotelGroup = hgData.data;
				exports.getAccount(hotelGroup.profilesConfiguration.spBaseDirectoryHref, email, function (err, account) {
					if (err) {
						callback(err);
					} else {
						try {
							var res = getProfileResponse(
								email,
								'signedOut',
								null,
								null,
								account.customData.settings[apiRequest.hotelGroupId],
								hotelGroup
							);
						} catch (e) {
							return callback(errors.newError('ps-frmt-res', {originalError: e.toString(), providedBy: 'signOut'}));
						}
						callback(null, res);
					}
				});
			});
		}
	});
};
/**
 * Get profile to StormPath
 * @param apiRequest
 * @param callback
 */
exports.getProfileStatus = function(apiRequest, callback) {
	dao.getLastSessionByUuid(apiRequest.uuid, function (err, session) {
		if (err) {
			callback(errors.newError('ps-session-get', {originalError: err}));
		} else {
			var isOnline = session && !session.data.sessionEnd;

			hgServices.getHotelGroup(apiRequest.hotelGroupId, function(err, hgData) {
				if (err) {
					return callback(err);
				}
				var hotelGroup = hgData.data;
				var profilesConfiguration = hotelGroup.profilesConfiguration;
				var dirUrl = isOnline ? profilesConfiguration.spDirectoryHref: profilesConfiguration.spBaseDirectoryHref;
				var userId = isOnline ? session.data.email : getBaseProfileEmail(apiRequest.uuid);
				exports.getAccount(dirUrl, userId, function (err, account) {
					if (err) {
						callback(err);
					} else {
						try {

							var res = getProfileResponse(
								account.email,
								isOnline ? 'signedIn' : 'signedOut',
								isOnline ? account : null,
								isOnline ? session.data : null,
								account.customData && account.customData.settings ? account.customData.settings[apiRequest.hotelGroupId] : {},
								hotelGroup
							);
						} catch (e) {
							return callback(errors.newError('ps-frmt-res', {originalError: e.toString(), originalStack: e.stack, providedBy: 'getProfileStatus'}));
						}
						callback(null, res);
					}
				});
			});
		}
	});
};
/**
 * Verify profile for GroupApp
 * @param apiRequest
 * @param callback
 */
exports.verify = function(apiRequest, callback) {
	if(apiRequest.query.email && apiRequest.query.sptoken) {
		hgServices.getProfilesConfiguration(apiRequest.hotelGroupId, function(err, profilesConfiguration){
			if(err){
				return callback(err);
			} else {
				exports.getAccount(profilesConfiguration.spDirectoryHref, apiRequest.query.email, function (err, account) {
					if (err) {
						return callback(err);
					}
					if(account.customData.emailVerificationToken) {
						if ( verifyEmailVerificationToken(account, apiRequest.query.sptoken) ) {
							account.customData.isEmailVerified = true;
							account.customData.save(function(err){
								if(err) {
									callback(errors.newError('ps-token-add', {originalError: err}));
								} else {
								    callback(null, {success: true});
								}
							});
						} else {
						    callback(errors.newError('ps-verify-inv', {givenToken: apiRequest.query.sptoken}));
						}
					} else {
					    callback(null);
					}
				});
			}
		});
	} else {
		callback(errors.newError('ps-verify-failed', {givenQuery: apiRequest.query}));
	}
};
/**
 * Reset password: request reset link from StormPath
 * @param apiRequest
 * @param callback
*/

exports.sendResetPass = function(apiRequest, callback) {
	if(apiRequest.body.email) {
		hgServices.getHotelGroup(apiRequest.hotelGroupId, function(err, hotelGroup){
			if (err) {
				return callback(errors.newError('ps-reset-failed', {originalError: err, info: 'invalidHotelGroup', providedNy: 'sendResetPass'}));
			}
			exports.sendPasswordResetEmail(apiRequest.body.email, hotelGroup.data, function (err) {
				if (err) {
					return callback(errors.newError('ps-reset-failed', {originalError: err, info: 'Invalid sendPasswordResetEmail', providedNy: 'sendResetPass'}));
				}
				return callback(null, {success: true});
			});
		});

	} else {
		return callback(errors.newError('ps-reset-inv'));
	}
};
/**
 * Reset password POST to StormPath
 * @param apiRequest
 * @param callback
 */
exports.verifyNewPassToken = function(apiRequest, callback) {
	if(apiRequest.query.email
		&& apiRequest.query.sptoken
	) {
		hgServices.getProfilesConfiguration(apiRequest.hotelGroupId, function(err, profilesConfiguration) {
			if (err) {
				return callback(errors.newError('ps-resettoken-inv', {originalError: err, info: 'getProfilesConfiguration', providedBy: 'verifyNewPassToken getProfilesConfiguration'}));
			}

			exports.getAccount(profilesConfiguration.spDirectoryHref, apiRequest.query.email, function(err, account){
				if(err){
					return callback(errors.newError('ps-resettoken-inv', {originalError: err, info: 'getAccount', providedBy: 'verifyNewPassToken' }));
				}

				//verify if token is valid at all
				//this will cause all existing tokens (old style) to be invalid
				if( !verifyPasswordResetToken(account, apiRequest.query.sptoken) ){
					return callback(errors.newError('ps-resettoken-inv', {info: 'invalidToken'}));
				}
				//user has submitted a form
				if(apiRequest.method === 'POST') {
					var reqf = ['passwordI', 'passwordII'];

					for (var i in reqf) {
						var f = reqf[i];
						if (!apiRequest.body.hasOwnProperty(f)) {
							return callback(errors.newError('ps-resetpass-inv', {info: 'missingInput'}));
						}
					}
					if (apiRequest.body.passwordI !== apiRequest.body.passwordII) {
						return callback(errors.newError('ps-resetpassmatch-inv'));
					}
					//set new password, delete reset token, and save it


					account.password = apiRequest.body.passwordI;
					account.save(function(err){
						if (err) {
							if(err.inner){
								return callback(errors.newError('ps-resetpass-inv', {originalError: err, info: 'Network Error', providedBy: 'verifyNewPassToken account.save' }) );
							}
							var um = err.userMessage.toLocaleLowerCase();
							if(um.indexOf('password') > -1) {
								return callback(errors.newError('ps-weak-pass', {originalError: err, providedBy: 'verifyNewPassToken account.save'}));
							}else{
								return callback(errors.newError('ps-resetpass-inv', {originalError: err, info: 'accountSaveFailed', providedBy: 'verifyNewPassToken account.save'}));
							}
						} else {

							account.customData.remove('passwordResetToken');
							account.customData.save(function(err){
								if(err){
									return callback(errors.newError('ps-resetpass-inv', {originalError: err, info: 'customDataSaveFailed', providedBy: 'verifyNewPassToken account.customData.save'}));
								}
								callback(null, {success: true});
							});

						}
					});
				} else {
					callback(null);
				}

			});
		});
	}else{
		callback(errors.newError('ps-resettoken-inv', {info: 'Invalid query params', providedBy: 'verifyNewPassToken'}));
	}
};

/**
 * Resend profile verification email
 * @param apiRequest
 * @param callback
 */
exports.resendVerificationEmail = function(apiRequest, callback) {

	if(apiRequest.body.email) {
		//get configuration
		hgServices.getHotelGroup(apiRequest.hotelGroupId, function(err, hotelGroup) {
			if (err) {
				return callback(err);
			}
			//send the email
			exports.sendVerificationEmail(apiRequest.body.email, hotelGroup.data, function(err){
				if (err) {
					return callback(err);
				}
				callback(null, {success: true});
			});

		});
	} else {
		callback(errors.newError('ps-resend-inv', {resendVerificationError: apiRequest.body}));
	}
};

/**
 * Encrypt profile email
 * @param email
 */
exports.encryptEmail = function(email){
	return cryptoUtils.encrypt(email, emailCryptoPass);
};
/**
 * Decrypt profile email
 * @param email
 */
exports.decryptEmail = function(email){
	return cryptoUtils.decrypt(email, emailCryptoPass);
};
/**
 * Post profile settings
 * @param apiRequest
 * @param callback
 */
exports.settings = function(apiRequest, callback) {
	var body = apiRequest.body;

	if(body && typeof body === 'object') {
		var encEmail = apiRequest.params.profileId;
		var decEmail, dirUrl;
		try {
			decEmail = exports.decryptEmail(encEmail);
		} catch(e) {
			return callback(errors.newError('cu-string-inv'));
		}


		hgServices.getProfilesConfiguration(apiRequest.hotelGroupId, function(err, profilesConfiguration) {
			if (err) {
				callback(err);
			} else {
				dao.getLastSessionByUuid(apiRequest.uuid, function (err, session) {
					if (err) {
						return callback(err);
					}
						//IF active session
						if( session && !session.data.sessionEnd ){
							//IF correct user
							if(session.data.email === decEmail){
								//all OK - normal user
								dirUrl = profilesConfiguration.spDirectoryHref;
							}else{
								//invalid email given
								return callback(errors.newError('ps-inv-email', {}));
							}
							//IF no active session and correct base user
						}else if(decEmail === getBaseProfileEmail(apiRequest.uuid)){
							//all OK - base user
							dirUrl = profilesConfiguration.spBaseDirectoryHref;
						}else{
							//invalid request
							return callback(errors.newError('ps-tenant-verify', {}));
						}
					exports.getAccount(dirUrl, decEmail, function (err, account) {
							if (err) {
								return callback(err);
							}
								hgServices.getHotelGroup(apiRequest.hotelGroupId, function (err, hotelGroup) {
									if (err) {
										return callback(err);
									}
									if (body.selectedLanguages) {

										if (!langUtils.validateSelectedLanguages(body.selectedLanguages, hotelGroup.data.supportedLanguages)) {
											return callback(errors.newError('ps-set-inv-inp', {invalidValuesIn: 'selectedLanguages'}));
										}
										if (!account.customData.settings[apiRequest.hotelGroupId]) {
											account.customData.settings[apiRequest.hotelGroupId] = {};
										}
										account.customData.settings[apiRequest.hotelGroupId].selectedLanguages = body.selectedLanguages;
										account.customData.save(function (err) {
											if (err) {
												return callback(errors.newError('ps-customdata-sav', {originalError: err, info: 'Local Error', providedBy: 'settings'}));
											}
											callback(null, {success: true});
										});
									} else {
										callback(null, {success: false});
									}
								});
						});

				});
			}
		});
	} else {
		callback(errors.newError('ps-set-inv-inp', {resendVerificationError: apiRequest.body}));
	}
};

/**
 * Create directory for new hotel group
 * @param hotelGroupInfo
 * @param hotelGroupId
 * @param callback
 */
exports.createDirectories = function(hotelGroupInfo, hotelGroupId, callback) {
	var dirName = process.env.STORMPATH_DIR_PREFIX + ' ' + hotelGroupId;
	var profilesConfiguration = {};
	var isBase = false;

	createDirectory(hotelGroupId, dirName, hotelGroupInfo, isBase, function(err, dir){
		if (err) {
			return callback(err, dir);
		}
		profilesConfiguration.spDirectoryHref = dir.href;
		isBase = true;
		createDirectory(hotelGroupId, dirName + ' BASE', hotelGroupInfo + ' BASE', isBase, function(err, dir){
			if (err) {
				return callback(err, dir);
			}
			profilesConfiguration.spBaseDirectoryHref = dir.href;
			callback(null, profilesConfiguration);
		});
	});
};

/**
 * Send email verification email
 * @param emailOrAccount {String|Object} Email as string or Stormpath Account instance
 * @param hotelGroupData
 * @param callback
 */
exports.sendVerificationEmail = function(emailOrAccount, hotelGroupData, callback){

	function doSending(account){
		//generate token
		var sptoken;
		try{
			sptoken = generateEmailVerificationToken(account);
		} catch (e) {
			return callback(errors.newError('cu-string-inv'));
		}
		//save custom data with new token
		account.customData.save(function (err) {
			if (err) {
				return callback(errors.newError('ps-resend-failed', {
					originalError: err,
					info: 'errSaveCustomData'
				}));
			}

			var template = JSON.parse(JSON.stringify(hotelGroupData.emailTemplates.emailVerification));

			var templateData = {
				emailVerificationUrl: apiUtils.getServiceHref(hotelGroupData.hotelGroupId, '/profiles/verify', {sptoken: sptoken, email: account.email})
			};

			var languageCode = langUtils.getPossibleLanguageOfObject(account.customData.settings[hotelGroupData.hotelGroupId].selectedLanguages, hotelGroupData.defaultLanguage, template);
			var localizedInfo = langUtils.getLocalizedData(hotelGroupData.info, languageCode);
			template.localized[languageCode].fromName = localizedInfo.name;
			var recipients = [
				{
					email: account.email,
					name: account.fullName,
					type: 'to'
				}
			];
			msgServices.sendEmail(template, templateData, languageCode, recipients, function(err, res){
				callback(err, res);
			});

		});
	}
	if(check.object(emailOrAccount)){
		doSending(emailOrAccount);
	}else{
		//get account
		exports.getAccount(hotelGroupData.profilesConfiguration.spDirectoryHref, emailOrAccount, function(err, account){
			if(err){
				return callback(err);
			}
			doSending(account);
		});
	}




};

/**
 * Send password reset email
 * @param email
 * @param hotelGroupData
 * @param callback
 */
exports.sendPasswordResetEmail = function(email, hotelGroupData, callback){
	exports.getAccount(hotelGroupData.profilesConfiguration.spDirectoryHref, email, function(err, account){
		if(err){
			return callback(err);
		}
		var tokenObj;
		try{
			tokenObj = generatePasswordResetToken(account);
		} catch (e) {
			return callback(errors.newError('cu-string-inv'));
		}

		//save custom data with new token
		account.customData.save(function (err) {
			if (err) {
				return callback(errors.newError('ps-reset-failed', {
					originalError: err,
					info: 'errSaveCustomData'
				}));
			}

			var template = JSON.parse(JSON.stringify(hotelGroupData.emailTemplates.passwordReset));

			var templateData = {
				passwordResetUrl: apiUtils.getServiceHref(hotelGroupData.hotelGroupId, '/profiles/resetpassword', {sptoken: tokenObj.token, email: account.email}),
				validDays: tokenObj.validDays
			};

			var languageCode = langUtils.getPossibleLanguageOfObject(account.customData.settings[hotelGroupData.hotelGroupId].selectedLanguages, hotelGroupData.defaultLanguage, template);
			var localizedInfo = langUtils.getLocalizedData(hotelGroupData.info, languageCode);
			template.localized[languageCode].fromName = localizedInfo.name;
			var recipients = [
				{
					email: account.email,
					name: account.fullName,
					type: 'to'
				}
			];
			msgServices.sendEmail(template, templateData, languageCode, recipients, function(err, res){
				callback(err, res);
			});
		});
	});
};
