'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var dao = require('../dao');

lab.experiment('Profiles DAO Interactions', {timeout: 5000}, function() {
	var _uuid;
	lab.before({timeout: 5000}, function(done){
		testingTools.getUuid(function(err, uuid){
			testingTools.code.expect(err).to.be.null();
			_uuid = uuid;
			done();
		});
	});
	lab.test('startProfileSession should work', function(done) {
		var data = { 'uuid': _uuid };

		dao.startProfileSession(data, function(err) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});
	});

	lab.test('getLastSessionByUuid should work', function(done) {
		dao.getLastSessionByUuid(_uuid, function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('endProfileSession should work', function(done) {
		dao.endProfileSession(_uuid, function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('endProfileSession should work if no sessions in history', function(done) {
		dao.endProfileSession('adsad' + Math.random(), function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('deleteProfileSession should work', function(done) {
		dao.deleteProfileSession(_uuid, function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('getLastSessionByUuid should work with empty result', function(done) {
		dao.getLastSessionByUuid(_uuid, function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

});


lab.experiment('Profiles DAO if DB structure invalid', function() {
	var _uuid, _getLastSessionByUuid;
	testingTools.failDbQueryForExperiment(lab, 'profile_session');
	lab.before({timeout: 5000}, function(done){
		_getLastSessionByUuid = dao.getLastSessionByUuid;
		testingTools.getUuid(function(err, uuid){
			testingTools.code.expect(err).to.be.null();
			_uuid = uuid;
			done();
		});
	});
	lab.after(function(done){
		dao.getLastSessionByUuid = _getLastSessionByUuid;
		done();
	});
	lab.test('startProfileSession should give error', function(done) {
		var data = { 'uuid': _uuid };

		dao.startProfileSession(data, function(err) {
			testingTools.code.expect(err).to.be.object();
			done();
		});
	});

	lab.test('getLastSessionByUuid should give error', function(done) {
		dao.getLastSessionByUuid(_uuid, function(err) {
			testingTools.code.expect(err).to.be.object();
			done();
		});
	});

	lab.test('endProfileSession should give error with wrong getLastSessionByUuid query', function(done) {
		dao.endProfileSession(_uuid, function(err) {
			testingTools.code.expect(err).to.be.object();
			done();
		});
	});

	lab.test('endProfileSession should give error with wrong query', function(done) {
		dao.getLastSessionByUuid = function(uuid, cb){
			cb(null, { data: { sessionEnd: '' } });
		};

		dao.endProfileSession(_uuid, function(err) {
			testingTools.code.expect(err).to.be.object();
			done();
		});
	});

	lab.test('deleteProfileSession should give error', function(done) {
		dao.deleteProfileSession(_uuid, function(err) {
			testingTools.code.expect(err).to.be.object();
			done();
		});
	});

});


lab.experiment('Profiles DAO if DB connection', function() {
	var _uuid ;

	lab.before(function(done){
		testingTools.getUuid(function(err, uuid){
			testingTools.code.expect(err).to.be.null();
			_uuid = uuid;
			done();
		});
	});

	testingTools.killDbConnectionForExperiment(lab);

	lab.test('startProfileSession should give error', function(done) {
		var data = { 'uuid': _uuid };

		dao.startProfileSession(data, function(err) {
			testingTools.code.expect(err).to.be.object();
			done();
		});
	});

	lab.test('getLastSessionByUuid should give error', function(done) {
		dao.getLastSessionByUuid(_uuid, function(err) {
			testingTools.code.expect(err).to.be.object();
			done();
		});
	});

	lab.test('endProfileSession should give error', function(done) {
		dao.endProfileSession(_uuid, function(err) {
			testingTools.code.expect(err).to.be.object();
			done();
		});
	});

	lab.test('deleteProfileSession should give error', function(done) {
		dao.deleteProfileSession(_uuid, function(err) {
			testingTools.code.expect(err).to.be.object();
			done();
		});
	});
});
