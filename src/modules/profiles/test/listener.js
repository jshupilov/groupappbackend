/**
 * Created by jevgenishupilov on 19/01/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var eventUtils = require('../../utils/eventUtils');
var services = require('../services');

lab.experiment('Profiles Listener', function() {
	var _createDirectories,
		data = {
			hotelGroupId: testingTools.getHotelGroupId(),
			info: {
				name: 'test-name'
			}
		};
	lab.before(function (done) {
		_createDirectories = services.createDirectories;

		eventUtils.emitter.removeAllListeners('hotelGroup.beforeCreate');
		testingTools.requireUncached('../modules/profiles/listener');
		testingTools.enableEvents();
		done();
	});
	lab.after(function (done) {
		services.createDirectories = _createDirectories;
		testingTools.disableEvents();
		done();
	});
	lab.test('should call services.createDirectories on hotelGroup.beforeCreate', {timeout: 5000}, function(done){
		var c1 = 0;
		services.createDirectories = function(name, hgId, cb){
			c1++;
			cb(null);
		};
		eventUtils.emitter.parallel('hotelGroup.beforeCreate', data, function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(c1).to.equal(1);
			done();
		});
	});
	lab.test('should give error services.createDirectories on hotelGroup.beforeCreate', {timeout: 5000}, function(done){
		var c1 = 0;
		services.createDirectories = function(name, hgId, cb){
			c1++;
			cb(new Error('Error listener services.createDirectories'));
		};
		eventUtils.emitter.parallel('hotelGroup.beforeCreate', data, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error listener services.createDirectories');
			testingTools.code.expect(c1).to.equal(1);
			done();
		});
	});
});
