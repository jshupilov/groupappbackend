'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var module = require('../module');
var apiUtils = require('../../utils/apiUtils');
var services = require('../services');
var spApp = require('../../../../node_modules/stormpath/lib/resource/Application');
var dao = require('../dao');

lab.experiment('profiles.module', function() {
	var _uuid, _createAccount, _startProfileSession, _authenticateAccount,
		_getAccounts, _resendVerificationEmail, _ensureSecurity, _signin, _signup, _getProfileStatus,
		_signout, _verify, _resendVerificationEmails, _settings, _sendResetPass, _verifyNewPassToken;
	var fakeServer = {
		routes: [],
		route: function(def){
			this.routes.push(def);
		}
	};
	lab.before(function(done){
		_createAccount = spApp.prototype.createAccount;
		_getAccounts = spApp.prototype.getAccounts;
		_authenticateAccount = spApp.prototype.authenticateAccount;
		_resendVerificationEmail = spApp.prototype.resendVerificationEmail;
		_startProfileSession = dao.startProfileSession;
		_ensureSecurity = apiUtils.ensureSecurity;
		_signin = services.signIn;
		_signup = services.signUp;
		_getProfileStatus = services.getProfileStatus;
		_signout = services.signOut;
		_verify = services.verify;
		_resendVerificationEmails = services.resendVerificationEmail;
		_settings = services.settings;
		_sendResetPass = services.sendResetPass;
		_verifyNewPassToken = services.verifyNewPassToken;

		services.init(function (err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.getUuid(function(err, uuid){
				testingTools.code.expect(err).to.be.null();
				_uuid = uuid;
				done();
			});
		});


	});
	lab.after(function(done){
		spApp.prototype.createAccount = _createAccount;
		spApp.prototype.getAccounts = _getAccounts;
		spApp.prototype.authenticateAccount = _authenticateAccount;
		spApp.prototype.resendVerificationEmail = _resendVerificationEmail;
		dao.startProfileSession = _startProfileSession;
		done();
	});

	lab.afterEach(function(done){
		apiUtils.ensureSecurity = _ensureSecurity;
		services.signIn = _signin;
		services.signUp = _signup;
		services.getProfileStatus = _getProfileStatus;
		services.signOut = _signout;
		services.verify = _verify;
		services.resendVerificationEmail = _resendVerificationEmails;
		services.settings = _settings;
		services.sendResetPass = _sendResetPass;
		services.verifyNewPassToken = _verifyNewPassToken;
		done();
	});
	lab.test('register should work', function(done) {

		module.register(fakeServer, {}, function(){
			testingTools.code.expect(fakeServer.routes).to.have.length(9);

			testingTools.code.expect(fakeServer.routes[0]).to.be.object();
			testingTools.code.expect(fakeServer.routes[0].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[0].path).to.equal('/profiles/status');

			testingTools.code.expect(fakeServer.routes[1]).to.be.object();
			testingTools.code.expect(fakeServer.routes[1].method).to.equal('POST');
			testingTools.code.expect(fakeServer.routes[1].path).to.equal('/profiles/signup');

			testingTools.code.expect(fakeServer.routes[2]).to.be.object();
			testingTools.code.expect(fakeServer.routes[2].method).to.equal('POST');
			testingTools.code.expect(fakeServer.routes[2].path).to.equal('/profiles/signin');

			testingTools.code.expect(fakeServer.routes[3]).to.be.object();
			testingTools.code.expect(fakeServer.routes[3].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[3].path).to.equal('/profiles/signout');

			testingTools.code.expect(fakeServer.routes[4]).to.be.object();
			testingTools.code.expect(fakeServer.routes[4].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[4].path).to.equal('/profiles/verify');

			testingTools.code.expect(fakeServer.routes[5]).to.be.object();
			testingTools.code.expect(fakeServer.routes[5].method).to.equal('POST');
			testingTools.code.expect(fakeServer.routes[5].path).to.equal('/profiles/resendverificationemail');

			testingTools.code.expect(fakeServer.routes[6]).to.be.object();
			testingTools.code.expect(fakeServer.routes[6].method).to.equal('POST');
			testingTools.code.expect(fakeServer.routes[6].path).to.equal('/profiles/{profileId}/settings');

			testingTools.code.expect(fakeServer.routes[7]).to.be.object();
			testingTools.code.expect(fakeServer.routes[7].method).to.equal('POST');
			testingTools.code.expect(fakeServer.routes[7].path).to.equal('/profiles/sendresetpassword');

			testingTools.code.expect(fakeServer.routes[8]).to.be.object();
			testingTools.code.expect(fakeServer.routes[8].method).to.include(['GET', 'POST']);
			testingTools.code.expect(fakeServer.routes[8].path).to.equal('/profiles/resetpassword');
			done();
		});
	});

	lab.test('/profiles/status handler should call ensureSecurity and getProfileStatus', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/status', {}, {}, 'GET', {})};
		var c1 = 0, c2 = 0;
		apiUtils.ensureSecurity = function(l, r, cb){
			c1++;
			cb(null, r, l);
		};
		services.getProfileStatus = function(r, cb){
			c2++;
			cb(null, r);
		};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[0].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal(req.apiRequest);
			testingTools.code.expect(c1).to.equal(1);
			testingTools.code.expect(c2).to.equal(1);
			return testingTools.expectCachingHeaders(done, 'noCache', res);
		});

	});

	lab.test('/profiles/status handler should call ensureSecurity and fail', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/status', {}, {}, 'GET', {})};

		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[0].handler(req, function(err){
			testingTools.expectError(err);
			done();
		});

	});

	lab.test('/profiles/signup handler should call ensureSecurity and signUp', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/signup', {}, {categoryId: 'reg-eur'}, 'POST', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);
		var c1 = 0, c2 = 0;
		apiUtils.ensureSecurity = function(l, r, cb){
			c1++;
			cb(null, r, l);
		};
		services.signUp = function(r, cb){
			c2++;
			cb(null, r);
		};

		fakeServer.routes[1].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal(req.apiRequest);
			testingTools.code.expect(c1).to.equal(1);
			testingTools.code.expect(c2).to.equal(1);
			done();
		});
	});

	lab.test('/profiles/signup handler should call ensureSecurity and fail', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/signup', {}, {categoryId: 'reg-eur'}, 'POST', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);


		fakeServer.routes[1].handler(req, function(err){
			testingTools.expectError(err);
			done();
		});
	});

	lab.test('/profiles/signin handler should call ensureSecurity and signIn', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/signin', {}, {}, 'POST', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		var c1 = 0, c2 = 0;
		apiUtils.ensureSecurity = function(l, r, cb){
			c1++;
			cb(null, r, l);
		};
		services.signIn = function(r, cb){
			c2++;
			cb(null, r);
		};


		fakeServer.routes[2].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal(req.apiRequest);
			testingTools.code.expect(c1).to.equal(1);
			testingTools.code.expect(c2).to.equal(1);
			done();
		});
	});

	lab.test('/profiles/signin handler should call ensureSecurity and fail', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/signin', {}, {}, 'POST', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[2].handler(req, function(err){
			testingTools.expectError(err);
			done();
		});
	});

	lab.test('/profiles/signout handler should call ensureSecurity and signOut', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/signout', {}, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		var c1 = 0, c2 = 0;
		apiUtils.ensureSecurity = function(l, r, cb){
			c1++;
			cb(null, r, l);
		};
		services.signOut = function(r, cb){
			c2++;
			cb(null, r);
		};

		fakeServer.routes[3].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal(req.apiRequest);
			testingTools.code.expect(c1).to.equal(1);
			testingTools.code.expect(c2).to.equal(1);
			return testingTools.expectCachingHeaders(done, 'noCache', res);
		});
	});

	lab.test('/profiles/signout handler should call ensureSecurity and fail', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/signout', {}, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[3].handler(req, function(err){
			testingTools.expectError(err);
			done();
		});
	});

	lab.test('/profiles/resendverificationemail handler should call ensureSecurity and resendVerificationEmail', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/resendverificationemail', {}, {}, 'POST', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		var c1 = 0, c2 = 0;
		apiUtils.ensureSecurity = function(l, r, cb){
			c1++;
			cb(null, r, l);
		};

		services.resendVerificationEmail = function(r, cb){
			c2++;
			cb(null, r);
		};

		fakeServer.routes[5].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal(req.apiRequest);
			testingTools.code.expect(c1).to.equal(1);
			testingTools.code.expect(c2).to.equal(1);
			done();
		});
	});

	lab.test('/profiles/resendverificationemail handler should call ensureSecurity and fail', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/resendverificationemail', {}, {}, 'POST', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[5].handler(req, function(err){
			testingTools.expectError(err);
			done();
		});
	});


	lab.test('/profiles/settings handler should call ensureSecurity and services.settings', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/blabla/settings', {}, {}, 'POST', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		var c1 = 0, c2 = 0;
		apiUtils.ensureSecurity = function(l, r, cb){
			c1++;
			cb(null, r, l);
		};

		services.settings = function(r, cb){
			c2++;
			cb(null, r);
		};

		fakeServer.routes[6].handler(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal(req.apiRequest);
			testingTools.code.expect(c1).to.equal(1);
			testingTools.code.expect(c2).to.equal(1);
			done();
		});
	});

	lab.test('/profiles/settings handler should call ensureSecurity and fail', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/asdad/settings', {}, {}, 'POST', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		fakeServer.routes[6].handler(req, function(err){
			testingTools.expectError(err);
			done();
		});
	});


	lab.test('/profiles/verify handler should services.verify and return smt', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/blabla', {}, {}, 'GET', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		services.verify = function(){
			arguments[arguments.length-1]('asdadasd');
		};

		fakeServer.routes[4].handler(req, {
			view: function(v, err){
				testingTools.code.expect(v).to.equal('profiles/verified');
				testingTools.code.expect(err.error).to.equal('asdadasd');
				done();
			}
		});
	});

	lab.test('/profiles/resendverificationemail should call ensureSecurity and fail', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/resendverificationemail', {}, {}, 'POST', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		apiUtils.ensureSecurity = function(l, r, cb){
			var e = new Error('asdasddas');
			e.data = {};
			cb(e);
		};


		fakeServer.routes[5].handler(req, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('asdasddas');
			done();
		});
	});

	lab.test('/profiles/resendverificationemail should call resendVerificationEmail', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/resendverificationemail', {}, {}, 'POST', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		apiUtils.ensureSecurity = function(l, r, cb){
			cb(null);
		};
		var c = 0;
		services.resendVerificationEmail = function(){
			c++;
			arguments[arguments.length-1](null);
		};


		fakeServer.routes[5].handler(req, function(err){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(c).to.equal(1);
			done();
		});
	});

	lab.test('/profiles/sendresetpassword should call ensureSecurity and fail', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/sendresetpassword', {}, {}, 'POST', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		apiUtils.ensureSecurity = function(l, r, cb){
			var e = new Error('asdas33ddas');
			e.data = {};
			cb(e);
		};


		fakeServer.routes[7].handler(req, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('asdas33ddas');
			done();
		});
	});

	lab.test('/profiles/sendresetpassword should call sendResetPass', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/sendresetpassword', {}, {}, 'POST', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		apiUtils.ensureSecurity = function(l, r, cb){
			cb(null);
		};
		var c = 0;
		services.sendResetPass = function(){
			c++;
			arguments[arguments.length-1](null);
		};


		fakeServer.routes[7].handler(req, function(err){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(c).to.equal(1);
			done();
		});
	});

	lab.test('/profiles/resetpassword should call ensureSecurity and fail with HTML page', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/resetpassword', {}, {}, 'GET', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		apiUtils.ensureSecurity = function(l, r, cb){
			var e = new Error('asdas33ddas');
			e.data = {};
			cb(e);
		};


		fakeServer.routes[8].handler(req, {
			view: function(tmpl, data){
				testingTools.code.expect(data.error).to.be.instanceof(Error);
				testingTools.code.expect(data.error.message).to.equal('asdas33ddas');
				done();
			}
		});
	});

	lab.test('/profiles/resetpassword should call verifyNewPassToken and fail with HTML page if error given', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/resetpassword', {}, {}, 'GET', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		apiUtils.ensureSecurity = function(l, r, cb){
			cb(null);
		};

		var c = 0;
		services.verifyNewPassToken = function(){
			c++;
			var e = new Error('asdas33ddas');
			e.data = {code: 'ps-resettoken-inv', isCustomError: true};
			arguments[arguments.length-1](e);
		};


		fakeServer.routes[8].handler(req, {
			view: function(tmpl, data){
				testingTools.code.expect(c).to.equal(1);
				testingTools.code.expect(data.hide).to.equal(true);
				testingTools.expectError(data.error, 'ps-resettoken-inv');
				done();
			}
		});
	});

	lab.test('/profiles/resetpassword should call verifyNewPassToken with HTML page if all ok', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/resetpassword', {}, {}, 'GET', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		apiUtils.ensureSecurity = function(l, r, cb){
			cb(null);
		};

		var c = 0;
		services.verifyNewPassToken = function(){
			c++;
			arguments[arguments.length-1](null, {success: true});
		};


		fakeServer.routes[8].handler(req, {
			view: function(tmpl, data){
				testingTools.code.expect(c).to.equal(1);
				testingTools.code.expect(data.hide).to.equal(true);
				testingTools.code.expect(data.error).to.be.null();
				done();
			}
		});
	});

	lab.test('/profiles/resetpassword should call verifyNewPassToken with HTML page if no success', function(done) {
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/resetpassword', {}, {}, 'GET', {email: 'test@test.com'})};
		apiUtils.populateApiRequest(req.apiRequest, testingTools.getApiVersion(), testingTools.getHotelGroupId(), _uuid);

		apiUtils.ensureSecurity = function(l, r, cb){
			cb(null);
		};

		var c = 0;
		services.verifyNewPassToken = function(){
			c++;
			arguments[arguments.length-1](null);
		};


		fakeServer.routes[8].handler(req, {
			view: function(tmpl, data){
				testingTools.code.expect(c).to.equal(1);
				testingTools.code.expect(data.hide).to.equal(false);
				testingTools.code.expect(data.error).to.be.null();
				done();
			}
		});
	});


});
