'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = testingTools.requireUncached('../modules/profiles/services');
var dao = require('../dao');
var cryptoUtils = require('../../utils/cryptoUtils');
var apiUtils = require('../../utils/apiUtils');
var langUtils = require('../../utils/langUtils');
var hgServices = require('../../hotel_groups/services');
var databaseUtils = require('../../utils/databaseUtils');
var messagingServices = require('../../messaging/services');
var spApp = require('../../../../node_modules/stormpath/lib/resource/Application');
var spAcc = require('../../../../node_modules/stormpath/lib/resource/Account');
var spCustomData = require('../../../../node_modules/stormpath/lib/resource/CustomData');
var client = require('../../../../node_modules/stormpath/lib/Client');
var ds = require('../../../../node_modules/stormpath/lib/ds/DataStore');
var spTenant = require('../../../../node_modules/stormpath/lib/resource/Tenant');
var nuuid = require('node-uuid');
var escape = require('pg-escape');



lab.experiment('Profile stormpath init', function() {
	var client0;
	lab.test('.initStormpathClient should init client without cache', function(done) {
		client0 = services.initStormpathClient({cacheOptions: {ttl: -1, tti: -1}});
		testingTools.code.expect(client0).to.be.object();
		done();
	});

	lab.test('.getStormpathClient should return same client every time after init', function(done) {
		var client1 = services.getStormpathClient();
		var client2 = services.getStormpathClient();
		testingTools.code.expect(client1).to.equal(client0);
		testingTools.code.expect(client1).to.equal(client2);
		done();
	});

});

lab.experiment('Profile init', function() {

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});
	lab.test('should give network error', {timeout: 10000}, function(done) {
		services.init(function(err) {
			testingTools.expectError(err, 'ps-app-get');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
});
lab.experiment('Profile init', function() {
	var _STORMPATH_URL;
	lab.before(function(done) {
		_STORMPATH_URL = process.env.STORMPATH_URL;
		done();
	});
	lab.test('should give error', {timeout: 5000}, function(done) {
		process.env.STORMPATH_URL = 'some_test_value';
		services.init(function(err) {
			testingTools.expectError(err, 'ps-app-get');
			done();
		});
	});
	lab.test('should get success', {timeout: 5000}, function(done) {
		services.init(function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.afterEach(function(done) {
		process.env.STORMPATH_URL = _STORMPATH_URL;
		done();
	});
});
lab.experiment('Profiles getProfileStatus ProfileMenu', function() {
	var newId, uuid;
	lab.before({timeout: 5000}, function(done) {
		newId = 'new-test-hgid';
		hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, hg) {
			testingTools.code.expect(err).to.be.null();
			hg.data.viewConfiguration.profileMenu = false;
			hg.data.hotelGroupId = newId;
			databaseUtils.getConnection(function(err1, client, done1) {
				testingTools.code.expect(err1).to.be.null();
				client.query('INSERT INTO hotel_group (data) VALUES($1)', [hg.data], function(err2, res) {
					testingTools.code.expect(err2).to.be.null();
					testingTools.code.expect(res.rowCount).to.equal(1);
					testingTools.getUuid(function(err3, _uuid) {
						done1();
						testingTools.code.expect(err3).to.be.null();
						uuid = _uuid;
						done();
					});
				});
			});
		});
	});
	lab.after(function(done) {
		databaseUtils.getConnection(function(err, client, done1) {
			testingTools.code.expect(err).to.be.null();
			client.query("DELETE FROM hotel_group WHERE (data -> 'hotelGroupId') = $1",
				['"' + newId + '"'],
				function(err1, res) {
					done1();
					testingTools.code.expect(err1).to.be.null();
					testingTools.code.expect(res.rowCount).to.equal(1);
					done();
				});
		});
	});
	lab.test('should work if profileMenu equal to false', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), newId, uuid);
		services.getProfileStatus(req, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['session', 'status', 'account', 'view']);
			testingTools.code.expect(res.view).to.include(['localized']);
			testingTools.code.expect(res.view.localized).to.include(['en_GB']);
			testingTools.code.expect(res.view.localized.en_GB).to.include(['profileMenu']);
			testingTools.code.expect(res.view.localized.en_GB.profileMenu).to.be.an.array().and.have.length(0);
			done();
		});
	});

});
lab.experiment('Profiles getProfileStatus Session', function() {

	var _getLastSessionByUuid, _getHotelGroup, _getDirectory;

	lab.before(function(done) {
		_getLastSessionByUuid = dao.getLastSessionByUuid;
		_getDirectory = client.prototype.getDirectory;
		_getHotelGroup = hgServices.getHotelGroup;
		done();
	});

	lab.afterEach(function(done) {
		dao.getLastSessionByUuid = _getLastSessionByUuid;
		client.prototype.getDirectory = _getDirectory;
		hgServices.getHotelGroup = _getHotelGroup;
		done();
	});

	lab.test('should fail email encryption', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		client.prototype.getDirectory = function(dUrl, email, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						'items': [{
							'email': true,
							'givenName': 'test-name',
							'surname': 'test-surname',
							'fillName': 'test-surname',
							'customData': {}
						}]
					});
				}
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {
				data: {
					sessionEnd: null,
					sessionStart: null,
					maxSessionEnd: null,
					email: 'asdsadasd'
				}
			});
		};
		hgServices.getHotelGroup = function(hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					supportedLanguages: [],
					profilesConfiguration: {
						spDirectoryHref: 'test.url'
					}
				}
			});
		};
		services.getProfileStatus(req, function(err) {
			testingTools.expectError(err, 'ps-frmt-res');
			testingTools.code.expect(err.data.statusCode).to.equal(500);

			done();
		});
	});
	lab.test('should work with session and nulled sessionEnd', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');
		client.prototype.getDirectory = function(dUrl, email, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						'items': [{
							'email': 'test@cardola.net',
							'givenName': 'test-name',
							'surname': 'test-surname',
							'fillName': 'test-surname',
							'customData': {}
						}]
					});
				}
			});
		};
		hgServices.getHotelGroup = function(hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					supportedLanguages: [],
					profilesConfiguration: {
						spDirectoryHref: 'test.url'
					}
				}
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {
				data: {
					sessionEnd: null,
					sessionStart: null,
					maxSessionEnd: null,
					email: 'asdsadasd'
				}
			});
		};

		services.getProfileStatus(req, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['session', 'status', 'account', 'view']);
			done();
		});
	});

	lab.test('should work with nulled session', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');
		client.prototype.getDirectory = function(dUrl, email, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						'items': [{
							'email': 'test@cardola.net',
							'givenName': 'test-name',
							'surname': 'test-surname',
							'fillName': 'test-surname',
							'customData': {}
						}]
					});
				}
			});
		};
		hgServices.getHotelGroup = function(hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					supportedLanguages: [],
					profilesConfiguration: {
						spDirectoryHref: 'test.url'
					}
				}
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, null);
		};

		services.getProfileStatus(req, function(err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.include(['session', 'status', 'account', 'view']);
			done();
		});
	});
	lab.test('should work with filled settings in customData', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), 'test-hgid', 'test-uuid');
		client.prototype.getDirectory = function(dUrl, email, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						'items': [{
							'email': 'test@cardola.net',
							'givenName': 'test-name',
							'surname': 'test-surname',
							'fillName': 'test-surname',
							'customData': {
								'settings': {
									'test-hgid': {}
								}
							}
						}]
					});
				}
			});
		};

		hgServices.getHotelGroup = function(hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					supportedLanguages: [],
					profilesConfiguration: {
						spDirectoryHref: 'test.url'
					}
				}
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {
				data: {
					sessionEnd: null,
					sessionStart: null,
					maxSessionEnd: null,
					email: 'asdsadasd'
				}
			});
		};

		services.getProfileStatus(req, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['session', 'status', 'account']);
			done();
		});
	});

	lab.test('should work with isOnline equal FALSE', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');
		client.prototype.getDirectory = function(dUrl, email, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						'items': [{
							'email': 'test@cardola.net',
							'givenName': 'test-name',
							'surname': 'test-surname',
							'fillName': 'test-surname',
							'customData': {
								isEmailVerified: true
							}
						}]
					});
				}
			});
		};

		hgServices.getHotelGroup = function(hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					supportedLanguages: [],
					profilesConfiguration: {
						spDirectoryHref: 'test.url'
					}
				}
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {
				data: {
					sessionEnd: '01.01.0001',
					sessionStart: null,
					maxSessionEnd: null,
					email: 'asdsadasd'
				}
			});
		};

		services.getProfileStatus(req, function(err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.include(['session', 'status', 'account']);
			done();
		});
	});

	lab.test('should give error with invalid dao.getLastSession', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(new Error('Error'));
		};

		services.getProfileStatus(req, function(err) {
			testingTools.expectError(err, 'ps-session-get');
			done();
		});
	});

	lab.test('should give error with invalid hgServices.getProfilesConfiguration', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {data: {sessionEnd: null}});
		};

		hgServices.getHotelGroup = function(hgId, cb) {
			cb(new Error('Error Profiles hgServices.getHotelGroup'));
		};

		services.getProfileStatus(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error Profiles hgServices.getHotelGroup');
			done();
		});
	});

	lab.test('should give error with invalid getAccount client.getDirectory', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {data: {sessionEnd: null, email: 'some-email@cardola.net'}});
		};

		hgServices.getHotelGroup = function(hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					supportedLanguages: [],
					profilesConfiguration: {
						spDirectoryHref: 'test.url'
					}
				}
			});
		};

		client.prototype.getDirectory = function(dUrl, email, cb) {
			cb(new Error('Error Profiles getAccount client.getDirectory'));
		};

		services.getProfileStatus(req, function(err) {
			testingTools.expectError(err, 'ps-dir-get');
			done();
		});
	});

	lab.test('should give error with invalid getAccount directory.getAccounts', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {data: {sessionEnd: null, email: 'some-email@cardola.net'}});
		};

		hgServices.getHotelGroup = function(hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					supportedLanguages: [],
					profilesConfiguration: {
						spDirectoryHref: 'test.url'
					}
				}
			});
		};

		client.prototype.getDirectory = function(dUrl, email, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(new Error('Error Profiles getAccount directory.getAccounts'));
				}
			});
		};

		services.getProfileStatus(req, function(err) {
			testingTools.expectError(err, 'ps-acc-get');
			done();
		});
	});

	lab.test('should give error with empty account, which returned from getAccount directory.getAccounts', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {data: {sessionEnd: null, email: 'some-email@cardola.net'}});
		};

		hgServices.getHotelGroup = function(hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					supportedLanguages: [],
					profilesConfiguration: {
						spDirectoryHref: 'test.url'
					}
				}
			});
		};

		client.prototype.getDirectory = function(dUrl, email, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {items: []});
				}
			});
		};

		services.getProfileStatus(req, function(err) {
			testingTools.expectError(err, 'ps-acc-get');
			done();
		});
	});

	lab.test('should return empty settings value if account.customData is missing', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {data: {sessionEnd: null, email: 'some-email@cardola.net'}});
		};

		hgServices.getHotelGroup = function(hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					supportedLanguages: [],
					profilesConfiguration: {
						spDirectoryHref: 'test.url'
					}
				}
			});
		};

		client.prototype.getDirectory = function(dUrl, email, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {items: []});
				}
			});
		};

		services.getProfileStatus(req, function(err) {
			testingTools.expectError(err, 'ps-acc-get');
			done();
		});
	});

	lab.test('should give error with wrong formatting', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {data: {sessionEnd: null, email: 'some-email@cardola.net'}});
		};

		hgServices.getHotelGroup = function(hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					supportedLanguages: [],
					profilesConfiguration: {
						spDirectoryHref: 'test.url'
					}
				}
			});
		};

		client.prototype.getDirectory = function(dUrl, email, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {items: [{}]});
				}
			});
		};

		services.getProfileStatus(req, function(err) {
			testingTools.expectError(err, 'ps-frmt-res');
			done();
		});
	});
});

lab.experiment('Profile Signup 1', function() {
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});

	lab.test('should give network error with createAccount getDirectory', {timeout: 10000}, function(done) {
		var body = {
			email: 'test@cardola.net',
			password: 'SecurePassword123',
			givenName: 'ads',
			surname: 'asss',
			customData: {
				settings: {}
			}
		};
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-dir-get');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
});

lab.experiment('Profile Signup 2', function() {
	var body = {
		email: 'test@cardola.net',
		password: 'SecurePassword123',
		givenName: 'ads',
		surname: 'asss',
		customData: {
			settings: {}
		}
	};
	var called;
	var uuid;
	lab.before(function(done) {
		called = 0;
		testingTools.getUuid(function(err, u) {
			testingTools.code.expect(err).to.be.null();
			uuid = u;
			done();
		});
	});
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connect', function(socket, opt) {
			if(called !== 1) {
				socket.bypass();
			}
			called += Math.pow(opt.host.indexOf('api.stormpath.com'), 0);
		});
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});
	lab.test('should give network error with createAccount directory.createAccount', {timeout: 10000}, function(done) {

		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-account-add');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
	testingTools.deleteAccountAfterExperiment(lab, body.email, false);

});

lab.experiment('Profile Signup 3', function() {
	var body = {
		email: 'test@cardola.net',
		password: 'SecurePassword123',
		givenName: 'ads',
		surname: 'asss',
		customData: {
			settings: {}
		}
	};
	var called, uuid;
	lab.before(function(done) {
		called = 0;
		testingTools.getUuid(function(err, u) {
			testingTools.code.expect(err).to.be.null();
			uuid = u;
			done();
		});
	});
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connect', function(socket, opt) {
			if(called !== 2) {
				socket.bypass();
			}
			called += Math.pow(opt.host.indexOf('api.stormpath.com'), 0);
		});
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});
	lab.test('should give network error with createAccount inheritFromBaseAccount getAccount getDirectory', {timeout: 10000}, function(done) {

		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-dir-get');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
	testingTools.deleteAccountAfterExperiment(lab, body.email, false);
});

lab.experiment('Profile Signup 4', function() {
	var body = {
		email: 'test@cardola.net',
		password: 'SecurePassword123',
		givenName: 'ads',
		surname: 'asss',
		customData: {
			settings: {}
		}
	};
	var called, uuid;
	lab.before(function(done) {
		called = 0;
		testingTools.getUuid(function(err, u) {
			testingTools.code.expect(err).to.be.null();
			uuid = u;
			done();
		});
	});
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connect', function(socket) {
			if(called !== 3) {
				socket.bypass();
			}
			called++;
		});
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});
	lab.test('should give network error with createAccount inheritFromBaseAccount getAccount getDirectory getAccounts', {timeout: 10000}, function(done) {

		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-acc-get');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
	testingTools.deleteAccountAfterExperiment(lab, body.email, false);
});

lab.experiment('Profile Signup 5', function() {
	var _startProfileSession, _endProfileSession, _getDirectory, _verifyAccountEmail, _getTenant, _getProfilesConfiguration;
	var body, _getHotelGroup, _sendVerificationEmail, uuid;

	//remove any created accounts after every test
	testingTools.deleteAccountAfterEveryTestInExperiment(lab, 'test@cardola.net', false);

	lab.before({timeout: 5000}, function(done) {
		_startProfileSession = dao.startProfileSession;
		_endProfileSession = dao.endProfileSession;
		_getDirectory = client.prototype.getDirectory;
		_verifyAccountEmail = spTenant.prototype.verifyAccountEmail;
		_getTenant = spApp.prototype.getTenant;
		_getProfilesConfiguration = hgServices.getProfilesConfiguration;
		_getHotelGroup = hgServices.getHotelGroup;
		_sendVerificationEmail = services.sendVerificationEmail;
		body = {
			email: 'test@cardola.net',
			password: 'SecurePassword123',
			givenName: 'ads',
			surname: 'asss',
			customData: {
				settings: {}
			}
		};
		testingTools.getUuid(function(err, uuid2) {
			testingTools.code.expect(err).to.be.null();
			uuid = uuid2;
			done();
		});

	});

	lab.afterEach(function(done) {
		dao.startProfileSession = _startProfileSession;
		dao.endProfileSession = _endProfileSession;
		client.prototype.getDirectory = _getDirectory;
		spTenant.prototype.verifyAccountEmail = _verifyAccountEmail;
		spApp.prototype.getTenant = _getTenant;
		hgServices.getProfilesConfiguration = _getProfilesConfiguration;
		hgServices.getHotelGroup = _getHotelGroup;
		services.sendVerificationEmail = _sendVerificationEmail;

		done();

	});

	lab.test('should give error with invalid body', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {body: 'testBody'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-mis-inp');
			done();
		});
	});

	lab.test('should give error with invalid hgServices.getHotelGroup', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		hgServices.getHotelGroup = function(hgId, cb) {
			cb(new Error('Error Profiles Signup hgServices.getHotelGroup'));
		};
		services.signUp(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error Profiles Signup hgServices.getHotelGroup');
			done();
		});
	});

	lab.test('should give error with invalid createAccount client.getDirectory', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		client.prototype.getDirectory = function(href, op, cb) {
			cb(new Error('Error Profiles Signup createAccount client.getDirectory'));
		};
		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-dir-get');
			done();
		});
	});

	lab.test('should give error with weak password in createAccount directory.createAccount ', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				createAccount: function(data, op, cb) {
					cb({
						userMessage: 'Error Profiles Signup createAccount directory.createAccount password'
					});
				}
			});
		};
		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-weak-pass');
			done();
		});
	});

	lab.test('should give error with existing email in createAccount directory.createAccount ', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				createAccount: function(data, op, cb) {
					cb({
						userMessage: 'Error Profiles Signup createAccount directory.createAccount email',
						code: 2001
					});
				}
			});
		};
		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-ex-email');
			done();
		});
	});

	lab.test('should give error with invalid email in createAccount directory.createAccount ', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				createAccount: function(data, op, cb) {
					cb({
						userMessage: 'Error Profiles Signup createAccount directory.createAccount email',
						code: 0
					});
				}
			});
		};
		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-inv-email');
			done();
		});
	});

	lab.test('should give error with invalid givenName in createAccount directory.createAccount ', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				createAccount: function(data, op, cb) {
					cb({
						userMessage: 'Error Profiles Signup createAccount directory.createAccount givenname'
					});
				}
			});
		};
		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-inv-gn');
			done();
		});
	});

	lab.test('should give error with invalid surname in createAccount directory.createAccount ', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				createAccount: function(data, op, cb) {
					cb({
						userMessage: 'Error Profiles Signup createAccount directory.createAccount surname'
					});
				}
			});
		};
		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-inv-sn');
			done();
		});
	});

	lab.test('should give error with account adding in createAccount directory.createAccount ', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				createAccount: function(data, op, cb) {
					cb({
						userMessage: 'Error Profiles Signup createAccount directory.createAccount'
					});
				}
			});
		};
		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-account-add');
			done();
		});
	});

	lab.test('should give error with invalid inheritFromBaseAccount ', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		var c = 1;

		client.prototype.getDirectory = function(href, op, cb) {
			if(c === 1) {
				c++;
				cb(null, {
					createAccount: function(data, op, cb) {
						cb(null, {});
					}
				});
			} else {
				cb(new Error('Error Profiles Signup inheritFromBaseAccount directory.createAccount'));
			}
		};
		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-dir-get');
			done();
		});
	});

	lab.test('should give error if cannot send verification email', {timeout: 10000}, function(done) {

		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		services.sendVerificationEmail = function() {
			arguments[arguments.length - 1](new Error('9ij-ji'));
		};

		services.signUp(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message, JSON.stringify(err)).to.equal('9ij-ji');
			done();
		});
	});

	lab.test('should give error if cannot start session because killing old session fails', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		services.sendVerificationEmail = function() {
			arguments[arguments.length - 1](null);
		};

		dao.endProfileSession = function() {
			arguments[arguments.length - 1](new Error('89fsd8j9sdf'));
		};

		services.signUp(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('89fsd8j9sdf');
			done();
		});
	});

	lab.test('should give error if cannot start session because starting new session fails', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		services.sendVerificationEmail = function() {
			arguments[arguments.length - 1](null);
		};

		dao.startProfileSession = function() {
			arguments[arguments.length - 1](new Error('89fsdsad8j9sdf'));
		};

		services.signUp(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('89fsdsad8j9sdf');
			done();
		});
	});

	lab.test('should give error if error formatting output', {timeout: 60000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		services.sendVerificationEmail = function(account, hotelGroupData, cb) {
			account.customData.settings[testingTools.getHotelGroupId()].selectedLanguages = 1;
			_sendVerificationEmail.call(this, account, hotelGroupData, cb);
		};

		services.signUp(req, function(err) {
			testingTools.expectError(err, 'ps-frmt-res');
			done();
		});
	});

});
lab.experiment('Profile Signin 1', function() {

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});

	lab.test('should give network error spApp.authenticateAccount', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': '', 'password': ''});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.signIn(req, function(err) {
			testingTools.expectError(err, 'ps-account-auth');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
});

lab.experiment('Profile Signin 2', function() {
	var called, uuid;
	var account = testingTools.createAccountForExperiment(lab);

	lab.before(function(done) {
		called = 0;
		testingTools.getUuid(function(err, uuid1) {
			testingTools.code.expect(err).to.be.null();
			uuid = uuid1;
			done();
		});
	});

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connect', function(socket) {
			if(called !== 1) {
				socket.bypass();
			}
			called++;
		});
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});

	lab.test('should give network error result.getAccount', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': account.email, 'password': account.password});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		services.signIn(req, function(err) {
			testingTools.expectError(err, 'ps-acc-get');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
});

lab.experiment('Profile Signin 3', function() {

	var _authenticateAccount, _tenant, _getDirectory, _startProfileSession, _getHotelGroup;
	var email = testingTools.createAccountForExperiment(lab).email;
	lab.before(function(done) {
		_authenticateAccount = spApp.prototype.authenticateAccount;
		_getDirectory = client.prototype.getDirectory;
		_tenant = spApp.prototype.getTenant;
		_getHotelGroup = hgServices.getHotelGroup;
		_startProfileSession = dao.startProfileSession;
		done();
	});

	lab.afterEach(function(done) {
		spApp.prototype.authenticateAccount = _authenticateAccount;
		spApp.prototype.getTenant = _tenant;
		client.prototype.getDirectory = _getDirectory;
		dao.startProfileSession = _startProfileSession;
		hgServices.getHotelGroup = _getHotelGroup;
		done();
	});

	lab.test('should give error with empty request', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');
		services.signIn(req, function(err) {
			testingTools.expectError(err, 'ps-inv-body');
			done();
		});
	});

	lab.test('should error with empty body', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');
		services.signIn(req, function(err) {
			testingTools.expectError(err, 'ps-mis-inp');
			done();
		});
	});

	lab.test('should give error with wrong login details', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', {
			'email': 'te7sdf78ydfsy78dfsst@test.com',
			'password': 'TestPass1'
		});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');
		services.signIn(req, function(err) {
			testingTools.expectError(err, 'ps-no-acc');
			done();
		});
	});
	lab.test('should give error with empty login details', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': '', 'password': ''});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');
		services.signIn(req, function(err) {
			testingTools.expectError(err, 'ps-account-auth');
			done();
		});
	});

	lab.test('should give error with invalid spApp.authenticateAccount', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': '', 'password': ''});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');
		spApp.prototype.authenticateAccount = function(op, cb) {
			cb(new Error('Error'));
		};
		services.signIn(req, function(err) {
			testingTools.expectError(err, 'ps-account-auth');
			done();
		});
	});

	lab.test('should give error message, if no user with this email', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': 'asdasdd', 'password': 'sdf'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.signIn(req, function(err) {
			testingTools.expectError(err, 'ps-no-acc');
			done();
		});
	});

	lab.test('should give error message, if invalid pass', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': email, 'password': 'asddsad'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.signIn(req, function(err) {
			testingTools.expectError(err, 'ps-inv-pass');
			done();
		});
	});

	lab.test('should give error message, if cannot start session', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': email, 'password': 'Password123'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		dao.startProfileSession = function() {
			arguments[arguments.length - 1](new Error('lmku834nj'));
		};

		services.signIn(req, function(err) {
			testingTools.expectError(err, 'ps-session-add');
			done();
		});
	});

	lab.test('should give error message, if cannot get base account', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': 'asdasdd', 'password': ''});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		spApp.prototype.authenticateAccount = function(op, cb) {
			cb(null, {
				account: {},
				getAccount: function() {
					arguments[arguments.length - 1](new Error('err2add'));
				}
			});
		};

		services.signIn(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('err2add');
			done();
		});
	});

	lab.test('should give error message, if cannot get hotelGroup', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': 'asdasdd', 'password': ''});
		apiUtils.populateApiRequest(req, 'v1', testingTools.getHotelGroupId(), 'test');
		spApp.prototype.authenticateAccount = function(op, cb) {
			cb(null, {
				account: {},
				getAccount: function() {
					var r = {
						email: 'blabla',
						customData: {
							settings: {}
						}
					};
					r.customData.settings[testingTools.getHotelGroupId()] = {
						selectedLanguages: ['en_GB']
					};
					arguments[arguments.length - 1](null, r);
				}
			});
		};

		hgServices.getHotelGroup = function(hgId, cb) {
			cb(new Error('Error signIn getHotelGroup'));
		};
		services.signIn(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error signIn getHotelGroup');
			done();
		});
	});

	lab.test('should work and return selected languages', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': 'asdasdd', 'password': ''});
		apiUtils.populateApiRequest(req, 'v1', testingTools.getHotelGroupId(), 'test');
		spApp.prototype.authenticateAccount = function(op, cb) {
			cb(null, {
				account: {},
				getAccount: function() {
					var r = {
						email: 'blabla',
						customData: {
							settings: {}
						}
					};
					r.customData.settings[testingTools.getHotelGroupId()] = {
						selectedLanguages: ['en_GB']
					};
					arguments[arguments.length - 1](null, r);
				}
			});
		};

		hgServices.getHotelGroup = function(hgId, cb) {
			cb(null, {
				data: {
					hotelGroupId: testingTools.getHotelGroupId(),
					supportedLanguages: [],
					profilesConfiguration: {
						spDirectoryHref: 'test.url'
					}
				}
			});
		};

		services.signIn(req, function(err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.settings.selectedLanguages[0].code).to.equal('en_GB');
			done();
		});
	});

	lab.test('should work and return isEmailVerified true in account details', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': 'asdasdd', 'password': ''});
		apiUtils.populateApiRequest(req, 'v1', testingTools.getHotelGroupId(), 'test');
		langUtils.init();
		spApp.prototype.authenticateAccount = function(op, cb) {
			cb(null, {
				account: {},
				getAccount: function() {
					var r = {
						email: 'blabla',
						customData: {
							settings: {},
							isEmailVerified: true
						}
					};
					r.customData.settings[testingTools.getHotelGroupId()] = {
						selectedLanguages: ['en_GB']
					};
					arguments[arguments.length - 1](null, r);
				}
			});
		};

		services.signIn(req, function(err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.settings.selectedLanguages[0].code).to.equal('en_GB');
			testingTools.code.expect(res.account.isEmailVerified).to.be.true();
			done();
		});
	});

	lab.test('should give error if formatting fails', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', {'email': 'asdasdd', 'password': ''});
		apiUtils.populateApiRequest(req, 'v1', testingTools.getHotelGroupId(), 'test');
		spApp.prototype.authenticateAccount = function(op, cb) {
			cb(null, {
				account: {},
				getAccount: function() {
					var r = {
						email: 'blabla',
						customData: {
							settings: {}
						}
					};
					r.customData.settings[testingTools.getHotelGroupId()] = {
						selectedLanguages: 23
					};
					arguments[arguments.length - 1](null, r);
				}
			});
		};

		services.signIn(req, function(err) {
			testingTools.expectError(err, 'ps-frmt-res');
			done();
		});
	});

});

lab.experiment('Profile SignOut 1', function() {
	var uuid;
	lab.before(function(done) {
		testingTools.getUuid(function(err, uuid1) {
			testingTools.code.expect(err).to.be.null();
			uuid = uuid1;
			done();
		});
	});
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connection', function(socket) {
			socket.destroy();
		});

	});

	lab.test('should error with invalid getAccount', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		services.signOut(req, function(err) {
			testingTools.expectError(err, 'ps-dir-get');
			done();
		});
	});
});

lab.experiment('Profile SignOut 2', function() {
	var _endProfileSession, _getDirectory, _getHotelGroup, uuid;
	lab.before(function(done) {
		_endProfileSession = dao.endProfileSession;
		_getDirectory = client.prototype.getDirectory;
		_getHotelGroup = hgServices.getHotelGroup;
		testingTools.getUuid(function(err, _uuid) {
			testingTools.code.expect(err).to.be.null();
			uuid = _uuid;
			done();

		});
	});

	lab.afterEach(function(done) {
		dao.endProfileSession = _endProfileSession;
		client.prototype.getDirectory = _getDirectory;
		hgServices.getHotelGroup = _getHotelGroup;
		done();
	});

	lab.test('should error with crashed connection', function(done) {

		var req = apiUtils.createApiRequest('/', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
		dao.endProfileSession = function(uuid, cb) {
			cb(new Error('Error'));
		};
		services.signOut(req, function(err) {
			testingTools.expectError(err, 'ps-session-out');
			done();
		});
	});

	lab.test('should give error with invalid hgServices.getProfilesConfiguration', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		dao.endProfileSession = function(uuid, cb) {
			cb(null);
		};
		hgServices.getHotelGroup = function(hgId, cb) {
			cb(new Error('Error Profile Signout hgServices.getHotelGroup'));
		};

		services.signOut(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error Profile Signout hgServices.getHotelGroup');
			done();
		});
	});
	lab.test('should work', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		dao.endProfileSession = function(uuid, cb) {
			cb(null);
		};
		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [{
							customData: {
								emailVerificationToken: 'test',
								settings: {}
							}
						}]
					});
				}
			});
		};

		services.signOut(req, function(err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.include(['status', 'account', 'session']);
			done();
		});
	});

	lab.test('should give error if formatting fails', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), 'test-hgid', uuid);

		dao.endProfileSession = function(uuid, cb) {
			cb(null);
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [{
							customData: {
								emailVerificationToken: 'test',
								settings: {
									'test-hgid': {
										selectedLanguages: 34
									}
								}
							}
						}]
					});
				}
			});
		};

		services.signOut(req, function(err) {
			testingTools.expectError(err, 'ps-frmt-res');
			done();
		});
	});
});

lab.experiment('Profile Verify', function() {
	var _getDirectory, _getProfilesConfiguration;
	lab.before(function(done) {
		_getDirectory = client.prototype.getDirectory;
		_getProfilesConfiguration = hgServices.getProfilesConfiguration;
		done();
	});
	lab.afterEach(function(done) {
		client.prototype.getDirectory = _getDirectory;
		hgServices.getProfilesConfiguration = _getProfilesConfiguration;
		done();
	});
	lab.test('should error with empty request', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');
		services.verify(req, function(err) {
			testingTools.expectError(err, 'ps-verify-failed');
			done();
		});
	});

	lab.test('should error with wrong account', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/?email=wrong@test.com&sptoken=testToken', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.verify(req, function(err) {
			testingTools.expectError(err, 'ps-acc-get');
			done();
		});
	});

	lab.test('should error with invalid hgServices.getProfilesConfiguration', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/?email=test@test.com&sptoken=testToken', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(new Error('Error Profile Signout hgServices.getProfilesConfiguration'));
		};

		services.verify(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error Profile Signout hgServices.getProfilesConfiguration');
			done();
		});
	});

	lab.test('should not give error if already verified', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/?email=test@test.com&sptoken=testToken', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [{
							customData: {}
						}]
					});
				}
			});
		};

		services.verify(req, function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should give error with not equal emailVerificationToken', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/?email=test@test.com&sptoken=testToken', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [{
							customData: {
								emailVerificationToken: 'NotEquelTestToken'
							}
						}]
					});
				}
			});
		};

		services.verify(req, function(err) {
			testingTools.expectError(err, 'ps-verify-inv');
			done();
		});
	});

	lab.test('should give error with saving customData', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/?email=test@test.com&sptoken=testToken', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [{
							customData: {
								emailVerificationToken: 'testToken',
								save: function(cb) {
									cb(new Error('Error Profile Verify customData save'));
								}
							}
						}]
					});
				}
			});
		};

		services.verify(req, function(err) {
			testingTools.expectError(err, 'ps-token-add');
			done();
		});
	});
	lab.test('should fail with invalidEmailGiven LOOK THIS', function(done) {
		var req = apiUtils.createApiRequest('/?email=test@test.com&sptoken=testToken', {}, {}, '', '');
		req.query.email = true;
		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spBaseDirectoryHref: 'test-directory-url'
			});
		};
		services.verify(req, function(err) {
			testingTools.expectError(err, 'ps-acc-get');
			testingTools.code.expect(err.data.debuggingData.reason).to.equal('invalidEmailGiven');
			done();
		});
	});

	lab.test('should work', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/?email=test@test.com&sptoken=testToken', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [{
							customData: {
								emailVerificationToken: 'testToken',
								save: function(cb) {
									cb(null);
								}
							}
						}]
					});
				}
			});
		};

		services.verify(req, function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

});

lab.experiment('Profiles ResendVerificationEmail', function() {
	var _resendVerificationEmail, _getProfilesConfiguration, _getDirectory, _ga;

	var email = testingTools.createAccountForExperiment(lab).email;
	lab.before(function(done) {
		_getDirectory = client.prototype.getDirectory;
		_getProfilesConfiguration = hgServices.getProfilesConfiguration;
		_resendVerificationEmail = spApp.prototype.resendVerificationEmail;
		_ga = spApp.prototype.getAccounts;
		done();
	});
	lab.afterEach(function(done) {
		client.prototype.getDirectory = _getDirectory;
		hgServices.getProfilesConfiguration = _getProfilesConfiguration;
		spApp.prototype.resendVerificationEmail = _resendVerificationEmail;
		spApp.prototype.getAccounts = _ga;
		done();
	});

	lab.test('should error with empty request', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.resendVerificationEmail(req, function(err) {
			testingTools.expectError(err, 'ps-resend-inv');
			done();
		});
	});

	lab.test('should error with invalid hotel group id', function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', {email: 'asdasd'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), 'asd43tdwf', 'test-uuid');

		services.resendVerificationEmail(req, function(err) {
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});

	lab.test('should error with invalid email', {timeout: 5000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', {email: 'asdasd'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.resendVerificationEmail(req, function(err) {
			testingTools.expectError(err, 'ps-acc-get');
			done();
		});
	});

	lab.test('should succeed', {timeout: 30000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, {}, '', {email: email});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.resendVerificationEmail(req, function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

});

lab.experiment('Profiles ValidateAccountData', function() {
	lab.test('should work for signin', function(done) {
		services.validateAccountData({email: 'ad', password: 'pass'}, 'signin', function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should work for signup', function(done) {
		services.validateAccountData({
			email: 'ad',
			password: 'pass',
			givenName: 'Peter',
			surname: 'Meter'
		}, 'signup', function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('should not work with invalid action', function(done) {
		services.validateAccountData({}, 'asdsda', function(err) {
			testingTools.expectError(err, 'system-error');
			done();
		});
	});
	lab.test('should not work with invalid input', function(done) {
		services.validateAccountData('{bla: 1}', 'signin', function(err) {
			testingTools.expectError(err, 'ps-inv-inp');
			done();
		});
	});

	lab.test('should give error if field missing', function(done) {
		services.validateAccountData({email: 'ad', password2: 'pass'}, 'signin', function(err) {
			testingTools.expectError(err, 'ps-mis-inp');
			done();
		});
	});

	lab.test('should give error if field invalid', function(done) {
		services.validateAccountData({email: 'ad', password: []}, 'signin', function(err) {
			testingTools.expectError(err, 'ps-inv-inp');
			done();
		});
	});
});

lab.experiment('Profiles CreateBaseAccount', function() {
	var _getDirectory, _getProfilesConfiguration;
	lab.before(function(done) {
		_getDirectory = client.prototype.getDirectory;
		_getProfilesConfiguration = hgServices.getProfilesConfiguration;
		done();
	});

	lab.afterEach(function(done) {
		client.prototype.getDirectory = _getDirectory;
		hgServices.getProfilesConfiguration = _getProfilesConfiguration;
		done();
	});
	lab.test('should give error if no uuid', function(done) {
		services.createBaseAccount({uuid2: 'asdasd'}, function(err) {
			testingTools.expectError(err, 'system-error');
			done();
		});
	});

	lab.test('should give error with invalid hgServices.getProfilesConfiguration', function(done) {
		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(new Error('Error Profile CreateBaseAccount hgServices.getProfilesConfiguration'));
		};
		services.createBaseAccount({uuid: 'test-uuid'}, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error Profile CreateBaseAccount hgServices.getProfilesConfiguration');
			done();
		});

	});

	lab.test('should give error with invalid directory.createAccount', function(done) {

		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				createAccount: function(data, op, cb2) {
					cb2({userMessage: 'Smt wrong!'});
				}
			});
		};

		services.createBaseAccount({uuid: 'test-uuid'}, function(err) {
			testingTools.expectError(err, 'ps-account-add');
			done();
		});

	});

	lab.test('should work', function(done) {
		var ta = {accountName: 'test'};
		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				createAccount: function(data, op, cb2) {
					cb2(null, ta);
				}
			});
		};
		services.createBaseAccount({uuid: 'asdasd'}, function(err, acc) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(acc).to.equal(ta);
			done();
		});

	});

});

lab.experiment('Profiles Settings', function() {

	var _getHotelGroup, _getProfilesConfiguration, _getDirectory, _getLastSessionByUuid, req;

	lab.before(function(done) {
		_getLastSessionByUuid = dao.getLastSessionByUuid;
		_getHotelGroup = hgServices.getHotelGroup;
		_getProfilesConfiguration = hgServices.getProfilesConfiguration;
		_getDirectory = client.prototype.getDirectory;
		req = {body: {bla: 1}, params: {profileId: 'asdads'}, uuid: 'test-uuid', hotelGroupId: 'test-hgId'};
		done();
	});

	lab.afterEach(function(done) {
		dao.getLastSessionByUuid = _getLastSessionByUuid;
		hgServices.getHotelGroup = _getHotelGroup;
		hgServices.getProfilesConfiguration = _getProfilesConfiguration;
		client.prototype.getDirectory = _getDirectory;
		req = {body: {bla: 1}, params: {profileId: 'asdads'}, uuid: 'test-uuid', hotelGroupId: 'test-hgId'};
		done();
	});

	lab.test('should not work with invalid profileId', function(done) {
		services.settings({body: {}, params: {profileId: 'lsdkjfl94'}}, function(err) {
			testingTools.expectError(err, 'cu-string-inv');
			done();
		});
	});
	lab.test('should not work without correct body', function(done) {
		services.settings({body: 'asdasd'}, function(err) {
			testingTools.expectError(err, 'ps-set-inv-inp');
			done();
		});
	});
	lab.test('should give error with missing body', function(done) {
		services.settings({}, function(err) {
			testingTools.expectError(err, 'ps-set-inv-inp');
			done();
		});
	});

	lab.test('should give error with invalid hgServices.getProfilesConfiguration', function(done) {
		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(new Error('Error Profile Settings hgServices.getProfilesConfiguration'));
		};
		services.settings(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error Profile Settings hgServices.getProfilesConfiguration');
			done();
		});
	});

	lab.test('should give error with invalid directory.getAccounts and with REGULAR profile', function(done) {
		req.params.profileId = cryptoUtils.encrypt('something-else@cardola.net', '^YpV!rJmtX4_hs^');
		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {data: {email: 'something-else@cardola.net', sessionEnd: ''}});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(new Error('Error Profile Settings getAccount client.getDirectory REGULAR'));
		};

		services.settings(req, function(err) {
			testingTools.expectError(err, 'ps-dir-get');
			done();
		});
	});

	lab.test('should give error with empty email - cant verify with empty email', {timeout: 5000}, function(done) {
		req.params.profileId = '';
		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};
		services.settings(req, function(err) {
			testingTools.expectError(err, 'ps-inv-email');
			testingTools.code.expect(err.data.message).to.equal('Invalid email');
			done();
		});
	});

	lab.test('should give error with invalid directory.getAccounts and with BASE profile', function(done) {
		req.params.profileId = cryptoUtils.encrypt('test-uuid@cardola.net', '^YpV!rJmtX4_hs^');

		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spBaseDirectoryHref: 'test-directory-url'
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {data: {email: 'test-uuid@cardola.net'}});
		};
		client.prototype.getDirectory = function(href, op, cb) {
			cb(new Error('Error Profile Settings getAccount client.getDirectory BASE'));
		};

		services.settings(req, function(err) {
			testingTools.expectError(err, 'ps-dir-get');
			done();
		});
	});

	lab.test('should give error with session email null and wrong encr email', function(done) {
		req.params.profileId = cryptoUtils.encrypt('something@cardola.net', '^YpV!rJmtX4_hs^');

		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, null);
		};

		services.settings(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.data.code).to.equal('ps-tenant-verify');
			done();
		});
	});
	lab.test('should give error with invalid session uuid', function(done) {
		req = apiUtils.createApiRequest('/', {}, {}, 'POST', {
			'email': 'test-uuid@cardola.net',
			'password': '^YpV!rJmtX4_hs^'
		});
		req.params.profileId = cryptoUtils.encrypt('test-uuid@cardola.net', '^YpV!rJmtX4_hs^');
		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(new Error('Error Profile Settings getLastSessionByUuid'));
		};

		services.settings(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error Profile Settings getLastSessionByUuid');
			done();
		});
	});
	lab.test('should give error with invalid hgServices.getHotelGroup', function(done) {
		req = apiUtils.createApiRequest('/', {}, {}, 'POST', {
			'email': 'test-uuid@cardola.net',
			'password': '^YpV!rJmtX4_hs^'
		});
		req.params.profileId = cryptoUtils.encrypt('test-uuid@cardola.net', '^YpV!rJmtX4_hs^');
		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spBaseDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [{
							bla: 1
						}]
					});
				}
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, {data: {email: 'test-uuid@cardola.net'}});
		};

		hgServices.getHotelGroup = function(hgid, cb) {
			cb(new Error('Error Profile Settings hgServices.getHotelGroup'));
		};

		services.settings(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error Profile Settings hgServices.getHotelGroup');
			done();
		});
	});

	lab.test('should work and not update anything', function(done) {
		req.params.profileId = cryptoUtils.encrypt('test-uuid@cardola.net', '^YpV!rJmtX4_hs^');
		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [{
							bla: 1
						}]
					});
				}
			});
		};

		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, null);
		};

		hgServices.getHotelGroup = function(hgid, cb) {
			cb(null, {});
		};

		services.settings(req, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['success']);
			testingTools.code.expect(res.success).to.equal(false);
			done();
		});
	});

	lab.test('should give error with invalid selectedLanguages', function(done) {
		req.params.profileId = cryptoUtils.encrypt('test-uuid@cardola.net', '^YpV!rJmtX4_hs^');
		req.body = {selectedLanguages: 1};
		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [{
							bla: 1
						}]
					});
				}
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, null);
		};
		hgServices.getHotelGroup = function(hgid, cb) {
			cb(null, {data: {supportedLanguages: []}});
		};
		services.settings(req, function(err) {
			testingTools.expectError(err, 'ps-set-inv-inp');
			done();
		});
	});

	lab.test('should work with empty customData settings and create settings object for hotel group', function(done) {
		req.params.profileId = cryptoUtils.encrypt('test-uuid@cardola.net', '^YpV!rJmtX4_hs^');
		var acc = {
			customData: {
				settings: {},
				save: function(cb) {
					cb(null, 'asdasda');
				}
			}
		};
		req.body = {selectedLanguages: ['en_GB']};

		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [acc]
					});
				}
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, null);
		};

		hgServices.getHotelGroup = function(hgid, cb) {
			cb(null, {data: {supportedLanguages: ['en_GB']}});
		};
		services.settings(req, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.deep.equal({success: true});
			testingTools.code.expect(acc.customData.settings).to.deep.equal({'test-hgId': {selectedLanguages: ['en_GB']}});
			done();
		});
	});
	lab.test('should work and update setting object for hotel group', function(done) {
		req.params.profileId = cryptoUtils.encrypt('test-uuid@cardola.net', '^YpV!rJmtX4_hs^');
		var acc = {
			customData: {
				settings: {
					'test-hgId': {
						selectedLanguages: ['fr_FR']
					}
				},
				save: function(cb) {
					cb(null);
				}
			}
		};
		req.body = {selectedLanguages: ['en_GB']};

		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [acc]
					});
				}
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, null);
		};
		hgServices.getHotelGroup = function(hgid, cb) {
			cb(null, {data: {supportedLanguages: ['en_GB']}});
		};

		services.settings(req, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.deep.equal({success: true});
			testingTools.code.expect(acc.customData.settings).to.deep.equal({'test-hgId': {selectedLanguages: ['en_GB']}});
			done();
		});
	});

	lab.test('should give error with invalid customData.save', function(done) {
		req.params.profileId = cryptoUtils.encrypt('test-uuid@cardola.net', '^YpV!rJmtX4_hs^');
		var acc = {
			customData: {
				settings: {
					hgId: {
						selectedLanguages: ['fr_FR']
					}
				},
				save: function(cb) {
					cb(new Error('Error Profile Settings account.customData.save'));
				}
			}
		};
		req.body = {selectedLanguages: ['en_GB']};

		hgServices.getProfilesConfiguration = function(hgId, cb) {
			cb(null, {
				spDirectoryHref: 'test-directory-url'
			});
		};

		client.prototype.getDirectory = function(href, op, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						items: [acc]
					});
				}
			});
		};
		dao.getLastSessionByUuid = function(uuid, cb) {
			cb(null, null);
		};
		hgServices.getHotelGroup = function(hgid, cb) {
			cb(null, {data: {supportedLanguages: ['en_GB']}});
		};
		services.settings(req, function(err) {
			testingTools.expectError(err, 'ps-customdata-sav');
			done();
		});
	});
});

lab.experiment('Profiles SendResetPass', function() {

	var f, f1;
	var email = testingTools.createAccountForExperiment(lab).email;

	lab.before(function(done) {
		f = spApp.prototype.sendPasswordResetEmail;
		f1 = services.sendPasswordResetEmail;
		done();
	});

	lab.afterEach(function(done) {
		spApp.prototype.sendPasswordResetEmail = f;
		services.sendPasswordResetEmail = f1;
		done();
	});

	lab.test('should give error if email missing', function(done) {
		var req = apiUtils.createApiRequest('/bla/bla', {}, {}, '', '');
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');
		services.sendResetPass(req, function(err) {
			testingTools.expectError(err, 'ps-reset-inv');
			done();
		});
	});

	lab.test('should give error if invalid email', {timeout: 5000}, function(done) {

		var req = apiUtils.createApiRequest('/bla/bla', {}, {}, '', {email: 'asdasd'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		services.sendResetPass(req, function(err) {
			testingTools.expectError(err, 'ps-reset-failed');
			done();
		});
	});

	lab.test('should give error if invalid hotel group id', {timeout: 5000}, function(done) {

		var req = apiUtils.createApiRequest('/bla/bla', {}, {}, '', {email: 'asdasd'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), 3, 'test-uuid');

		services.sendResetPass(req, function(err) {
			testingTools.expectError(err, 'ps-reset-failed');
			testingTools.code.expect(err.data.debuggingData.info).to.equal('invalidHotelGroup');
			done();
		});
	});

	lab.test('should work and trigger the email sending', {timeout: 5000}, function(done) {

		var req = apiUtils.createApiRequest('/bla/bla', {}, {}, '', {email: email});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'test-uuid');

		var c = 0;
		services.sendPasswordResetEmail = function(em, hgData, cb) {
			c++;
			testingTools.code.expect(em).to.equal(email);
			testingTools.code.expect(hgData.hotelGroupId).to.equal(testingTools.getHotelGroupId());
			cb(null);
		};

		services.sendResetPass(req, function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(c).to.equal(1);
			done();
		});
	});

});
lab.experiment('Profiles VerifyNewPassToken', function() {
	var f, f1, f2, f3;
	var email = testingTools.createAccountForExperiment(lab).email;

	lab.before(function(done) {
		f = spApp.prototype.verifyPasswordResetToken;
		f1 = hgServices.getProfilesConfiguration;
		f2 = spAcc.prototype.save;
		f3 = spCustomData.prototype.save;
		done();
	});

	lab.afterEach(function(done) {
		spApp.prototype.verifyPasswordResetToken = f;
		hgServices.getProfilesConfiguration = f1;
		spAcc.prototype.save = f2;
		spCustomData.prototype.save = f3;
		done();
	});

	lab.test('should give error if no email', {timeout: 5000}, function(done) {
		var data = {
			query: {
				sptoken: 'ads'
			}
		};
		services.verifyNewPassToken(data, function(err) {

			testingTools.expectError(err, 'ps-resettoken-inv');
			done();
		});
	});

	lab.test('should give error if no token', {timeout: 5000}, function(done) {

		var data = {
			query: {
				email: 'ads@as.ee'
			}
		};
		services.verifyNewPassToken(data, function(err) {

			testingTools.expectError(err, 'ps-resettoken-inv');
			done();
		});
	});

	lab.test('should give error if cannot get conf', {timeout: 5000}, function(done) {

		var data = {
			query: {
				email: 'adasd234fwefs@as.ee',
				sptoken: 'adssda'
			}
		};
		hgServices.getProfilesConfiguration = function() {
			arguments[arguments.length - 1](new Error('asdsaf34few'));
		};

		services.verifyNewPassToken(data, function(err) {
			testingTools.expectError(err, 'ps-resettoken-inv');
			testingTools.code.expect(err.data.debuggingData.info).to.equal('getProfilesConfiguration');
			done();
		});
	});

	lab.test('should give error if account does not exist', {timeout: 8000}, function(done) {

		var data = {
			query: {
				email: 'adasd234fwefs@as.ee',
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId()
		};
		services.verifyNewPassToken(data, function(err) {
			testingTools.expectError(err, 'ps-resettoken-inv');
			testingTools.code.expect(err.data.debuggingData.providedBy).to.equal('verifyNewPassToken');
			done();
		});
	});

	lab.test('should give error if no token', {timeout: 10000}, function(done) {

		var data = {
			query: {
				email: email,
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId()
		};
		services.verifyNewPassToken(data, function(err) {
			testingTools.expectError(err, 'ps-resettoken-inv');
			testingTools.code.expect(err.data.debuggingData.info).to.equal('invalidToken');
			done();
		});
	});

	lab.test('should give error if invalid format token', {timeout: 10000}, function(done) {

		var data = {
			query: {
				email: email,
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId()
		};

		services.getStormpathApplication(function(err, application) {
			testingTools.code.expect(err).to.be.null();
			application.getAccounts({email: email, expand: 'customData'}, function(err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.items.length, data.email).to.equal(1);

				res.items[0].customData.passwordResetToken = 'u89fwu8wefu';

				res.items[0].customData.save(function(err) {
					testingTools.code.expect(err).to.be.null();
					services.verifyNewPassToken(data, function(err) {
						testingTools.expectError(err, 'ps-resettoken-inv');
						testingTools.code.expect(err.data.debuggingData.info).to.equal('invalidToken');
						done();
					});
				});
			});

		});

	});

	lab.test('should give error if invalid token', {timeout: 5000}, function(done) {

		var data = {
			query: {
				email: email,
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId()
		};

		services.getStormpathApplication(function(err, application) {
			testingTools.code.expect(err).to.be.null();
			application.getAccounts({email: email, expand: 'customData'}, function(err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.items.length).to.equal(1);

				var d = new Date();
				d.setHours(25);
				res.items[0].customData.passwordResetToken = {
					token: 'j90sdfj0efw',
					validUntil: d.toISOString()
				};

				res.items[0].customData.save(function(err) {
					testingTools.code.expect(err).to.be.null();
					services.verifyNewPassToken(data, function(err) {
						testingTools.expectError(err, 'ps-resettoken-inv');
						testingTools.code.expect(err.data.debuggingData.info).to.equal('invalidToken');
						done();
					});
				});
			});

		});

	});

	lab.test('should give error if correct token but expired', {timeout: 8000}, function(done) {

		var data = {
			query: {
				email: email,
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId()
		};

		services.getStormpathApplication(function(err, application) {
			testingTools.code.expect(err).to.be.null();
			application.getAccounts({email: email, expand: 'customData'}, function(err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.items.length).to.equal(1);

				var d = new Date();
				d.setHours(-1);
				res.items[0].customData.passwordResetToken = {
					token: 'adssda',
					validUntil: d.toISOString()
				};

				res.items[0].customData.save(function(err) {
					testingTools.code.expect(err).to.be.null();
					services.verifyNewPassToken(data, function(err) {

						testingTools.expectError(err, 'ps-resettoken-inv');
						testingTools.code.expect(err.data.debuggingData.info).to.equal('invalidToken');
						done();
					});
				});
			});

		});

	});

	lab.test('should NOT give error if correct and not POST ', {timeout: 20000}, function(done) {

		var data = {
			query: {
				email: email,
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId()
		};

		services.getStormpathApplication(function(err, application) {
			testingTools.code.expect(err).to.be.null();
			application.getAccounts({email: email, expand: 'customData'}, function(err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.items.length).to.equal(1);

				var d = new Date();
				d.setHours(25);
				res.items[0].customData.passwordResetToken = {
					token: 'adssda',
					validUntil: d.toISOString()
				};

				res.items[0].customData.save(function(err) {
					testingTools.code.expect(err).to.be.null();
					services.verifyNewPassToken(data, function(err) {

						testingTools.code.expect(err).to.be.null();
						done();
					});
				});
			});

		});

	});

	lab.test('(IF LAST TEST PASSED) should give error if POST without required parameters', {timeout: 5000}, function(done) {
		var data = {
			query: {
				email: email,
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId(),
			method: 'POST',
			body: {
				passwordI: 'asdasd'
			}
		};

		services.verifyNewPassToken(data, function(err) {
			testingTools.code.expect(err.data.debuggingData.info).to.equal('missingInput');
			testingTools.expectError(err, 'ps-resetpass-inv');
			done();
		});
	});

	lab.test('(IF LAST TEST PASSED) should give error if POST and passwords do not match', {timeout: 5000}, function(done) {
		var data = {
			query: {
				email: email,
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId(),
			method: 'POST',
			body: {
				passwordI: 'asdasd',
				passwordII: '45f'
			}
		};

		services.verifyNewPassToken(data, function(err) {

			testingTools.expectError(err, 'ps-resetpassmatch-inv');
			done();
		});
	});

	lab.test('(IF LAST TEST PASSED) should give error if POST and password too weak', {timeout: 5000}, function(done) {
		var data = {
			query: {
				email: email,
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId(),
			method: 'POST',
			body: {
				passwordI: 'asd',
				passwordII: 'asd'
			}
		};

		services.verifyNewPassToken(data, function(err) {

			testingTools.expectError(err, 'ps-weak-pass');
			done();
		});
	});

	lab.test('(IF LAST TEST PASSED) should give error if POST and account save fails', {timeout: 5000}, function(done) {
		var data = {
			query: {
				email: email,
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId(),
			method: 'POST',
			body: {
				passwordI: 'asd',
				passwordII: 'asd'
			}
		};

		spAcc.prototype.save = function() {
			arguments[arguments.length - 1]({userMessage: 'sdasdd'});
		};

		services.verifyNewPassToken(data, function(err) {

			testingTools.expectError(err, 'ps-resetpass-inv');
			testingTools.code.expect(err.data.debuggingData.info).to.equal('accountSaveFailed');
			done();
		});
	});

	lab.test('(IF LAST TEST PASSED) should give error if POST and customData save fails', {timeout: 10000}, function(done) {
		var data = {
			query: {
				email: email,
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId(),
			method: 'POST',
			body: {
				passwordI: 'asd345AA',
				passwordII: 'asd345AA'
			}
		};

		spCustomData.prototype.save = function() {
			arguments[arguments.length - 1]({userMessage: 'sdasdd'});
		};

		services.verifyNewPassToken(data, function(err) {
			testingTools.expectError(err, 'ps-resetpass-inv');
			testingTools.code.expect(err.data.debuggingData.info).to.equal('customDataSaveFailed');
			done();
		});
	});

	lab.test('(IF LAST TEST PASSED) should succeed', {timeout: 20000}, function(done) {
		var data = {
			query: {
				email: email,
				sptoken: 'adssda'
			},
			hotelGroupId: testingTools.getHotelGroupId(),
			method: 'POST',
			body: {
				passwordI: '8u9fshu9IJNIJNO',
				passwordII: '8u9fshu9IJNIJNO'
			}
		};

		services.verifyNewPassToken(data, function(err) {

			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});

	});

});

lab.experiment('Profiles VerifyNewPassToken', function() {
	var called, token;
	var email = testingTools.createAccountForExperiment(lab).email;
	var data = {
		query: {},
		hotelGroupId: testingTools.getHotelGroupId(),
		method: 'POST',
		body: {
			email: email
		}
	};
	lab.before({timeout: 10000}, function(done) {
		called = 0;
		services.sendResetPass(data, function(err) {
			testingTools.code.expect(err).to.be.null();
			services.getStormpathApplication(function(errApp, application) {
				testingTools.code.expect(errApp).to.be.null();
				application.getAccounts({email: email, expand: 'customData'}, function(errAcc, accounts) {
					testingTools.code.expect(errAcc).to.be.null();
					token = accounts.items[0].customData.passwordResetToken.token;
					done();
				});
			});
		});
	});
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connect', function(socket) {
			if(called !== 2) {
				socket.bypass();
			}
			called++;
		});
		mitm.on('connection', function(socket) {
			socket.destroy();
		});

	});

	lab.test('should give network error with account.save', {timeout: 10000}, function(done) {
		data.query = {
			email: email,
			sptoken: token
		};
		data.body = {
			passwordI: '8u9fshu9IJNIJNO',
			passwordII: '8u9fshu9IJNIJNO'
		};

		services.verifyNewPassToken(data, function(err) {
			testingTools.expectError(err, 'ps-resetpass-inv');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
});

lab.experiment('Profiles CreateDirectories', function() {
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connection', function(socket) {
			socket.destroy();
		});

	});

	lab.test('should give network error with client.getDirectories', {timeout: 10000}, function(done) {
		services.createDirectories('Test hotel group info', testingTools.getHotelGroupId(), function(err) {
			testingTools.expectError(err, 'ps-dir-get');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
});

lab.experiment('Profiles CreateDirectories', function() {
	var called;
	lab.before(function(done) {
		called = 0;
		done();
	});
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connect', function(socket, opt) {
			if(called !== 3) {
				socket.bypass();
			}
			called += Math.pow(opt.host.indexOf('api.stormpath.com'), 0);
		});
		mitm.on('connection', function(socket) {
			socket.destroy();
		});

	});

	lab.test('should give network error with client.createDirectory', {timeout: 10000}, function(done) {

		services.createDirectories('Test hotel group info', 'Some Test hotel Group Id 1', function(err) {
			testingTools.expectError(err, 'ps-dir-add');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
});
lab.experiment('Profiles CreateDirectories', function() {
	var called, dirName = 'Some Test hotel Group Id 2';
	lab.before(function(done) {
		called = 0;
		done();
	});
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connect', function(socket, opt) {
			if(called !== 3) {
				socket.bypass();
			}
			called += Math.pow(opt.host.indexOf('api.stormpath.com'), 0);
		});
		mitm.on('connection', function(socket) {
			socket.destroy();
		});

	});

	//delete test dir after creation
	lab.after({timeout: 10000}, function(done) {
		services.getStormpathClient().getDirectories({name: process.env.STORMPATH_DIR_PREFIX + ' ' + dirName}, function(err, directories) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(directories.size, dirName).to.equal(1);
			directories.each(function(dir, cb) {
				dir.delete(function(err) {
					testingTools.code.expect(err).not.to.be.object();
					cb();
				});
			}, function(err) {
				testingTools.code.expect(err).not.to.be.object();
				done();
			});

		});
	});

	lab.test('should give network error with configureDirectory', {timeout: 10000}, function(done) {
		services.createDirectories('Test hotel group info', dirName, function(err) {
			testingTools.expectError(err, 'ps-dir-upd');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
});
lab.experiment('Profiles CreateDirectories', function() {
	var called,
		dirName = 'Some Test hotel Group Id 3';
	lab.before(function(done) {
		called = 0;
		done();
	});

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connect', function(socket, opt) {
			if(called !== 4) {
				socket.bypass();
			}
			called += Math.pow(opt.host.indexOf('api.stormpath.com'), 0);
		});
		mitm.on('connection', function(socket) {
			socket.destroy();
		});

	});

	//delete test dir after creation
	lab.after({timeout: 10000}, function(done) {
		services.getStormpathClient().getDirectories({name: process.env.STORMPATH_DIR_PREFIX + ' ' + dirName}, function(err, directories) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(directories.size, dirName).to.equal(1);

			directories.each(function(dir, cb) {

				dir.delete(function(err) {
					testingTools.code.expect(err).not.to.be.object();
					cb();
				});
			}, function(err) {
				testingTools.code.expect(err).not.to.be.object();
				done();
			});

		});
	});

	lab.test('should give network error with spApp.addAccountStore', {timeout: 10000}, function(done) {

		services.createDirectories('Test hotel group info', dirName, function(err) {
			testingTools.expectError(err, 'ps-as-add');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('inner');
			done();
		});
	});
});

lab.experiment('Profiles CreateDirectories', function() {

	var _getDirectories, _createDirectory, _saveResource, _getDirectory, _addAccountStore, _getResource;
	var hgInfo = 'Test hotel group info';
	var hgId = testingTools.getHotelGroupId();

	lab.before(function(done) {
		_getDirectories = client.prototype.getDirectories;
		_getDirectory = client.prototype.getDirectory;
		_createDirectory = client.prototype.createDirectory;
		_saveResource = ds.prototype.saveResource;
		_getResource = ds.prototype.getResource;
		_addAccountStore = spApp.prototype.addAccountStore;
		done();
	});

	lab.afterEach(function(done) {
		client.prototype.getDirectories = _getDirectories;
		client.prototype.getDirectory = _getDirectory;
		client.prototype.createDirectory = _createDirectory;
		ds.prototype.saveResource = _saveResource;
		ds.prototype.getResource = _getResource;
		spApp.prototype.addAccountStore = _addAccountStore;
		done();
	});

	lab.test('should give error with invalid REGULAR createDirectory client.getDirectories', function(done) {
		client.prototype.getDirectories = function(op, cb) {
			cb(new Error('Error Profile CreateDirectories REGULAR createDirectory client.getDirectories'));
		};

		services.createDirectories(hgInfo, hgId, function(err) {
			testingTools.expectError(err, 'ps-dir-get');
			done();
		});
	});

	lab.test('should give error with invalid BASE createDirectory client.getDirectories', {timeout: 10000}, function(done) {
		var c = 1;
		client.prototype.getDirectories = function(op, cb) {
			if(c === 1) {
				c++;
				cb(null, {
					items: [{
						spDirectoryHref: 'test-directory-url'
					}]
				});
			} else {
				cb(new Error('Error Profile CreateDirectories BASE createDirectory client.getDirectories'));
			}
		};

		services.createDirectories(hgInfo, hgId, function(err) {
			testingTools.expectError(err, 'ps-dir-get');
			done();
		});
	});
	lab.test('should return REGULAR and BASE directories href, which was created before', {timeout: 10000}, function(done) {
		client.prototype.getDirectories = function(op, cb) {
			cb(null, {
				items: [{
					spDirectoryHref: 'test-directory-url',
					spBaseDirectoryHref: 'test-base-directory-url'
				}]
			});
		};

		services.createDirectories(hgInfo, hgId, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['spDirectoryHref', 'spBaseDirectoryHref']);
			done();
		});
	});

	lab.test('should give error with invalid createDirectory client.createDirectory', {timeout: 5000}, function(done) {
		client.prototype.getDirectories = function(op, cb) {
			cb(null, {
				items: []
			});
		};

		client.prototype.createDirectory = function(op, cb) {
			cb(new Error('Error Profile CreateDirectories REGULAR createDirectory client.createDirectory'));
		};

		services.createDirectories(hgInfo, hgId, function(err) {
			testingTools.expectError(err, 'ps-dir-add');
			done();
		});
	});

	lab.test('should give error with invalid createDirectory accountCreationPolicy configureDirectory saveResource', {timeout: 5000}, function(done) {
		client.prototype.getDirectories = function(op, cb) {
			cb(null, {
				items: []
			});
		};

		client.prototype.createDirectory = function(op, cb) {
			cb(null, {
				accountCreationPolicy: {
					href: 'test-accountCreationPolicy-href'
				},
				passwordPolicy: {
					href: 'test-passwordPolicy-href'
				}
			});
		};

		services.createDirectories(hgInfo, hgId, function(err) {
			testingTools.expectError(err, 'ps-dir-upd');
			done();
		});
	});

	lab.test('should give error with invalid createDirectory accountCreationPolicy configureDirectory getResource saveResource', function(done) {
		var c1 = 1;
		client.prototype.getDirectories = function(op, cb) {
			cb(null, {
				items: []
			});
		};

		client.prototype.createDirectory = function(op, cb) {
			cb(null, {
				accountCreationPolicy: {
					href: 'test-accountCreationPolicy-href'
				},
				passwordPolicy: {
					href: 'test-passwordPolicy-href'
				}
			});
		};
		ds.prototype.saveResource = function(config, cb) {
			if(c1 === 1) {
				c1++;
				cb(null);
			} else {
				cb(new Error('Error Profile CreateDirectories createDirectory accountCreationPolicy configureDirectory getResource saveResource'));
			}
		};

		services.createDirectories(hgInfo, hgId, function(err) {
			testingTools.expectError(err, 'ps-dir-upd');
			done();
		});
	});

	lab.test('should give error with invalid createDirectory accountCreationPolicy spApp.addAccountStore', function(done) {
		client.prototype.getDirectories = function(op, cb) {
			cb(null, {
				items: []
			});
		};

		client.prototype.createDirectory = function(op, cb) {
			cb(null, {
				accountCreationPolicy: {
					href: 'test-accountCreationPolicy-href'
				},
				passwordPolicy: {
					href: 'test-passwordPolicy-href'
				}
			});
		};
		ds.prototype.saveResource = function(config, cb) {
			cb(null);
		};

		spApp.prototype.addAccountStore = function(dir, cb) {
			cb(new Error('Error Profile CreateDirectories createDirectory spApp.addAccountStore'));
		};
		services.createDirectories(hgInfo, hgId, function(err) {
			testingTools.expectError(err, 'ps-as-add');
			done();
		});
	});

	lab.test('should give error with invalid createDirectory passwordPolicy configureDirectory saveResource', function(done) {
		var c1 = 1;
		client.prototype.getDirectories = function(op, cb) {
			cb(null, {
				items: []
			});
		};

		client.prototype.createDirectory = function(op, cb) {
			cb(null, {
				accountCreationPolicy: {
					href: 'test-accountCreationPolicy-href'
				},
				passwordPolicy: {
					href: 'test-passwordPolicy-href'
				}
			});
		};
		ds.prototype.saveResource = function(config, cb) {
			if(c1 === 1) {
				c1++;
				cb(null);
			} else {
				cb(new Error('Error Profile CreateDirectories createDirectory passwordPolicy configureDirectory saveResource'));
			}
		};

		services.createDirectories(hgInfo, hgId, function(err) {
			testingTools.expectError(err, 'ps-dir-upd');
			done();
		});
	});

	lab.test('should work', function(done) {

		client.prototype.getDirectories = function(op, cb) {
			cb(null, {
				items: []
			});
		};

		client.prototype.createDirectory = function(op, cb) {
			cb(null, {
				accountCreationPolicy: {
					href: 'test-accountCreationPolicy-href'
				},
				passwordPolicy: {
					href: 'test-passwordPolicy-href'
				}
			});
		};
		ds.prototype.saveResource = function(config, cb) {
			cb(null);
		};

		spApp.prototype.addAccountStore = function(dir, cb) {
			cb(null);
		};
		services.createDirectories(hgInfo, hgId, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.include(['spDirectoryHref', 'spBaseDirectoryHref']);
			done();
		});
	});

});

lab.experiment('Profiles.Services', function() {
	var f1;
	var email = testingTools.createAccountForExperiment(lab).email;

	lab.before(function(done) {
		f1 = messagingServices.sendEmail;
		done();
	});

	lab.afterEach(function(done) {
		messagingServices.sendEmail = f1;
		done();
	});

	lab.test('sendVerificationEmail should send out an correct email', {timeout: 10000}, function(done) {
		var called = 0;
		messagingServices.sendEmail = function(emailTemplate, templateData, languageCode, recipients, callback) {
			called++;
			testingTools.code.expect(emailTemplate).to.be.object();
			testingTools.code.expect(emailTemplate).to.include('localized');
			testingTools.code.expect(emailTemplate.localized).to.include(languageCode);
			testingTools.code.expect(emailTemplate.localized[languageCode]).to.include('bodyText');
			testingTools.code.expect(emailTemplate.localized[languageCode].bodyText).to.include('{{emailVerificationUrl}}');
			testingTools.code.expect(templateData).to.include('emailVerificationUrl');
			testingTools.code.expect(templateData.emailVerificationUrl, templateData.emailVerificationUrl).to.include('sptoken=');
			testingTools.code.expect(templateData.emailVerificationUrl, templateData.emailVerificationUrl).to.include('email=' + encodeURIComponent(email));
			callback(null);
		};

		hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, hotelGroupData) {
			testingTools.code.expect(err).to.be.null();
			//send email
			services.sendVerificationEmail(email, hotelGroupData.data, function(err) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(called).to.equal(1);
				done();
			});
		});
	});

	lab.test('sendPasswordResetEmail should send out an correct email', {timeout: 10000}, function(done) {
		var called = 0;
		messagingServices.sendEmail = function(emailTemplate, templateData, languageCode, recipients, callback) {
			called++;
			testingTools.code.expect(emailTemplate).to.be.object();
			testingTools.code.expect(emailTemplate).to.include('localized');
			testingTools.code.expect(emailTemplate.localized).to.include(languageCode);
			testingTools.code.expect(emailTemplate.localized[languageCode]).to.include('bodyText');
			testingTools.code.expect(emailTemplate.localized[languageCode].bodyText).to.include('{{passwordResetUrl}}');
			testingTools.code.expect(templateData).to.include('passwordResetUrl');
			testingTools.code.expect(templateData.passwordResetUrl, templateData.passwordResetUrl).to.include('sptoken=');
			testingTools.code.expect(templateData.passwordResetUrl, templateData.passwordResetUrl).to.include('email=' + encodeURIComponent(email));
			callback(null);
		};

		hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, hotelGroupData) {
			testingTools.code.expect(err).to.be.null();
			//send email
			services.sendPasswordResetEmail(email, hotelGroupData.data, function(err) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(called).to.equal(1);
				done();
			});
		});
	});

});

lab.experiment('Profiles.Services', function() {
	var f1, f2, f3, u;
	var email = testingTools.createAccountForExperiment(lab).email;

	lab.before(function(done) {
		u = nuuid.v4;
		f1 = messagingServices.sendEmail;
		f2 = spCustomData.prototype.save;
		f3 = client.prototype.getDirectory;
		done();
	});

	lab.afterEach(function(done) {
		messagingServices.sendEmail = f1;
		spCustomData.prototype.save = f2;
		client.prototype.getDirectory = f3;
		nuuid.v4 = u;
		done();
	});

	lab.test('sendVerificationEmail should send out an correct email and correct old customDataformat to new', {timeout: 10000}, function(done) {
		var called = 0;
		messagingServices.sendEmail = function(emailTemplate, templateData, languageCode, recipients, callback) {
			called++;
			testingTools.code.expect(emailTemplate).to.be.object();
			testingTools.code.expect(emailTemplate).to.include('localized');
			testingTools.code.expect(emailTemplate.localized).to.include(languageCode);
			testingTools.code.expect(emailTemplate.localized[languageCode]).to.include('bodyText');
			testingTools.code.expect(emailTemplate.localized[languageCode].bodyText).to.include('{{emailVerificationUrl}}');
			testingTools.code.expect(templateData).to.include('emailVerificationUrl');
			testingTools.code.expect(templateData.emailVerificationUrl, templateData.emailVerificationUrl).to.include('sptoken=');
			testingTools.code.expect(templateData.emailVerificationUrl, templateData.emailVerificationUrl).to.include('email=' + encodeURIComponent(email));
			callback(null);
		};

		//update customData
		services.getStormpathApplication(function(err, application) {
			testingTools.code.expect(err).to.be.null();

			application.getAccounts({email: email, expand: 'customData'}, function(err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.items.length).to.equal(1);
				res.items[0].customData.emailVerificationToken = 'some-token';

				res.items[0].customData.save(function(err) {
					testingTools.code.expect(err).to.be.null();
					//start test
					hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, hotelGroupData) {
						testingTools.code.expect(err).to.be.null();
						//send email
						services.sendVerificationEmail(email, hotelGroupData.data, function(err) {
							testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
							testingTools.code.expect(called).to.equal(1);

							//verify that customData was changed to correct format
							application.getAccounts({email: email, expand: 'customData'}, function(err, res) {
								testingTools.code.expect(err).to.be.null();
								testingTools.code.expect(res.items.length).to.equal(1);
								testingTools.code.expect(res.items[0].customData.emailVerificationToken, JSON.stringify(res.items[0].customData)).to.be.array();
								testingTools.code.expect(res.items[0].customData.emailVerificationToken).to.include('some-token');
								done();
							});

						});
					});

				});
			});
		});

	});

	lab.test('sendVerificationEmail should fail if cannot save customData', {timeout: 10000}, function(done) {
		spCustomData.prototype.save = function() {
			arguments[arguments.length - 1](new Error('aadsdsd'));
		};
		hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, hotelGroupData) {
			testingTools.code.expect(err).to.be.null();
			services.sendVerificationEmail(email, hotelGroupData.data, function(err) {

				testingTools.expectError(err, 'ps-resend-failed');
				testingTools.code.expect(err.data.debuggingData.info).to.equal('errSaveCustomData');
				done();
			});
		});

	});

	lab.test('.sendPasswordResetEmail should fail if cannot save customData', {timeout: 10000}, function(done) {
		spCustomData.prototype.save = function() {
			arguments[arguments.length - 1](new Error('aadsdsd'));
		};
		hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, hotelGroupData) {
			testingTools.code.expect(err).to.be.null();
			services.sendPasswordResetEmail(email, hotelGroupData.data, function(err) {

				testingTools.expectError(err, 'ps-reset-failed');
				testingTools.code.expect(err.data.debuggingData.info).to.equal('errSaveCustomData');
				done();
			});
		});
	});

	lab.test('.sendPasswordResetEmail should fail on email encryption', function(done) {
		client.prototype.getDirectory = function(dUrl, email, cb) {
			cb(null, {
				getAccounts: function(op, cb2) {
					cb2(null, {
						'items': [{
							'email': true
						}]
					});
				}
			});
		};
		hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, hotelGroupData) {
			testingTools.code.expect(err).to.be.null();
			services.sendPasswordResetEmail('anything@mail.ru', hotelGroupData.data, function(err) {
				testingTools.expectError(err, 'cu-string-inv');
				testingTools.code.expect(err.data.statusCode).to.equal(500);
				done();
			});
		});

	});

	lab.test('.sendVerificationEmail should fail on uuid encryption', function(done) {

		nuuid.v4 = function() {
			throw new Error('smt wrong');
		};
		services.sendVerificationEmail({email: 'lkdf', customData: {}}, null, function(err) {
			testingTools.expectError(err, 'cu-string-inv');
			done();
		});

	});

});


lab.experiment('profiles.getAccountByHgid', function() {
	var account = testingTools.createAccountForExperiment(lab);
	var hgid = testingTools.getHotelGroupId();
	lab.test('getAccount should give one', {timeout: 10000}, function(done) {
		services.getAccountByHgid(hgid, account.email, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.email).to.be.equal(account.email);
			done();
		});
	});
	lab.test('GetAccount should give error with bad hgid', function(done) {
		services.getAccountByHgid('wronghgid', '', function(err) {
			testingTools.expectError(err, 'hg-not-found');
			done();
		});
	});
});

lab.experiment('profiles.createSession', function() {
	var apiRequest;
	var hgid = testingTools.getHotelGroupId();
	var account = testingTools.createAccountForExperiment(lab);
	lab.before(function(done) {
		testingTools.getUuid(function(err, uuid) {
			testingTools.code.expect(err).to.be.null();
			apiRequest = apiUtils.createApiRequest('', {'x-cardola-uuid': uuid}, {}, 'GET', {});
			apiUtils.populateApiRequest(apiRequest, testingTools.getApiVersion(), hgid, uuid);
			done();
		});
	});
	lab.after(function(done){
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape("DELETE FROM profile_session WHERE (data->'uuid')=%L", '"'+apiRequest.uuid+'"');
			client.query(query, function(err){
				doneDb();
				testingTools.code.expect(err).to.be.null();
				done();
			});
		});
	});

	lab.test('Create Session should go well', function(done) {
		services.createSession(apiRequest, hgid, account.email, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.account.email).to.be.equal(account.email);
			databaseUtils.getConnection(function(err, client, doneDb){
				testingTools.code.expect(err).to.be.null();
				var query = escape("SELECT * FROM profile_session WHERE (data->'uuid')=%L", '"'+apiRequest.uuid+'"');
				client.query(query, function(err, res){
					doneDb();
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rows[0].data.uuid).to.be.equal(apiRequest.uuid);
					done();
				});
			});
		});
	});
	lab.test('Should give error with wrong account', function(done){
		services.createSession(apiRequest, hgid, 'a@b.c', function(err){
			testingTools.expectError(err, 'ps-acc-get');
			done();
		});
	});
});


lab.experiment('profiles.validateSession', function() {
	var uuid, session, account, apiRequest, out, req;
	var hgId = testingTools.getHotelGroupId();
	var version = testingTools.getApiVersion();
	var accountEmail = testingTools.createAccountForExperiment(lab).email;

	lab.before({timeout: 5000}, function(done) {
		testingTools.getUuid(function(err, _uuid) {
			testingTools.code.expect(err).to.be.null();
			uuid = _uuid;
			req = apiUtils.createApiRequest('/', {}, {}, 'GET', {});
			apiUtils.populateApiRequest(req, version, hgId, uuid);
			services.createSession(req, hgId, accountEmail, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.account.email).to.be.equal(accountEmail);
				out = res;
				done();
			});
		});
	});
	lab.beforeEach(function(done){
		req.headers['x-cardola-access-token'] = out.accessToken;
		apiRequest = JSON.parse(JSON.stringify(req));
		session = JSON.parse(JSON.stringify(out.session));
		account = JSON.parse(JSON.stringify(out.account));
		done();
	});
	lab.after(function (done) {
		databaseUtils.getConnection(function(err, client, done1) {
			testingTools.code.expect(err).to.be.null();
			var query = escape("DELETE FROM profile_session WHERE (data->'uuid')=%L", '"'+ uuid +'"');
			client.query(query, function(err){
				done1();
				testingTools.code.expect(err).to.be.null();
				done();
			});
		});
	});
	lab.test('should return error if token invalid', {timeout: 10000}, function(done) {
		apiRequest.uuid = 'wrong-uuid';
		services.validateSession(apiRequest, session, function(err){
			testingTools.expectError(err, 'ps-session-inv');
			testingTools.code.expect(err.data.debuggingData.info).to.be.equal('Header uuid not equal to session uuid');
			done();
		});
	});
	lab.test('should return error if token invalid', {timeout: 10000}, function(done) {
		apiRequest.headers['x-cardola-access-token'] = 'wrong-token';
		services.validateSession(apiRequest, session, function(err){
			testingTools.expectError(err, 'ps-session-inv');
			testingTools.code.expect(err.data.debuggingData.info).to.be.equal('Invalid profile decryption');
			done();
		});
	});

	lab.test('should return empty account if access token and account details are missing', {timeout: 10000}, function(done){
		delete apiRequest.headers['x-cardola-access-token'];
		session.account = null;
		services.validateSession(apiRequest, session, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.include(['account']);
			testingTools.code.expect(res.account).to.be.null();
			done();
		});
	});

	lab.test('should return error  if access token is missing, but account details exists', {timeout: 10000}, function(done){
		delete apiRequest.headers['x-cardola-access-token'];
		services.validateSession(apiRequest, session, function(err){
			testingTools.expectError(err, 'ps-session-inv');
			testingTools.code.expect(err.data.debuggingData.info).to.be.equal('Access token is missing in header');
			done();
		});
	});

	lab.test('should return error if account details equal to null', {timeout: 10000}, function(done){
		session.account = null;
		services.validateSession(apiRequest, session, function(err){
			testingTools.expectError(err, 'ps-session-inv');
			testingTools.code.expect(err.data.debuggingData.info).to.be.equal('Profile details are missing in session');
			done();
		});
	});

	lab.test('should work and return decrypted account details', {timeout: 10000}, function(done){
		services.validateSession(apiRequest, session, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.include(['account']);
			testingTools.code.expect(res.account).to.deep.equal(account);
			done();
		});
	});
});
