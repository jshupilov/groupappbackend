/**
 * Created by jevgeni_shupilov on 04/11/15.
 */
'use strict';
var errors = require('../utils/errors');
var cryptoUtils = require('../utils/cryptoUtils');
var dao = require('./dao');
var services = require('./services');
var q = require('q');

errors.defineError('ral-inv-ln-qty', 'System error', 'Access log has invalid structure', 'Reporting.AccessLog', 400);
errors.defineError('ral-inv-ver', 'System error', 'Access log has invalid version', 'Reporting.AccessLog', 400);
errors.defineError('ral-inv-fields', 'System error', 'Access log has invalid fields', 'Reporting.AccessLog', 400);
errors.defineError('ral-inv-file', 'System error', 'Can not decode access log', 'Reporting.AccessLog', 400);
errors.defineError('ral-empty-file', 'System error', 'Access log is empty', 'Reporting.AccessLog', 400);

/**
 * Decompress and import access log file to database
 * @param accessLog
 * @param fileName
 * @param callback
 */
exports.decompressAndImportAccessLogFile = function(accessLog, fileName, callback) {
	services.decompressGzip(accessLog, function(err, accessLogFile) {
		if(err) {
			return callback(err);
		}
		return exports.parseAndStoreAccessLog(accessLogFile, fileName, callback);
	});
};

/**
 * Parse and store access log
 * @param accessLogFile
 * @param fileName
 * @param callback
 */
exports.parseAndStoreAccessLog = function(accessLogFile, fileName, callback) {
	exports.parseAccessLog(accessLogFile, fileName, function(err, accessLog) {
		if(err) {
			return callback(err);
		}
		var qtyPerTransaction = 1000;
		var promises = [];

		var doIt = function(p, accessLogData) {
			dao.addAccessLog(accessLogData, function(err) {
				if(err) {
					return p.reject(err);
				}
				return p.resolve();
			});
		};
		var out = {
			rows: accessLog.length
		};
		while(accessLog.length) {
			var p = q.defer();
			var data = accessLog.splice(0, qtyPerTransaction);
			promises.push(p.promise);
			doIt(p, data);
		}
		q.all(promises).done(function() {
			return callback(null, out);
		}, function(err) {
			return callback(err);
		});

	});
};

/**
 * Parse access log
 * @param accessLogFile
 * @param fileName
 * @param callback
 */
exports.parseAccessLog = function(accessLogFile, fileName, callback) {
	try {
		var accessLogString = new Buffer(accessLogFile, 'base64').toString('ascii');
	} catch(e) {
		return callback(errors.newError('ral-inv-file', {originalError: e}));
	}
	if(accessLogString.length > 0) {
		var accessLog = accessLogString.split('\n');
		accessLogString = undefined;
		if(accessLog.length > 2) {
			var version = exports.parseVersion(accessLog[0]);
			if(version === '1.0') {
				var fields = exports.parseFields(accessLog[1]);
				if(fields) {
					accessLog.splice(0, 2);
					var report = [];
					while(accessLog.length) {
						var raw = accessLog.splice(0, 1)[0];
						var row = exports.parseRow(fields, raw);
						if(!row) {
							continue;
						}
						row.version = version;
						row.fileName = fileName;
						row.hashKey = cryptoUtils.md5(JSON.stringify(row));
						report.push(row);
					}
					return callback(null, report);
				} else {
					return callback(errors.newError('ral-inv-fields', {givenFields: fields, line: accessLog[1]}));
				}
			} else {
				return callback(errors.newError('ral-inv-ver', {givenVersionNumber: version, line: accessLog[0]}));
			}
		} else {
			return callback(errors.newError('ral-inv-ln-qty', {lineQty: accessLog.length, accessLog: accessLog}));
		}
	} else {
		return callback(errors.newError('ral-empty-file'));
	}
};

/**
 * Parse access log version
 * @param line
 */
exports.parseVersion = function(line) {
	var out = null;
	if(line.length > 0) {
		if(line.indexOf('#Version:') === 0) {
			out = line.replace(/#Version: /g, '');
		}
	}
	return out;
};
/**
 * Parse access log field
 * @param line
 */
exports.parseFields = function(line) {
	var out = null;
	if(line.length > 0) {
		if(line.indexOf('#Fields:') === 0) {
			var parsedFields = [];
			var fields;
			line = line.replace(/#Fields: /g, '');
			fields = line.split(' ');

			for(var i in fields) {
				var value = fields[i];
				switch(value) {
					case 'c-ip':
						parsedFields.push('ip');
						break;
					case 'cs(Host)':
						parsedFields.push('host');
						break;
					case 'cs-method':
						parsedFields.push('method');
						break;
					case 'cs-uri-stem':
						parsedFields.push('path');
						break;
					case 'cs-uri-query':
						parsedFields.push('query');
						break;
					case 'cs-version':
						parsedFields.push('protocolVersion');
						break;
					case 'sc-status':
						parsedFields.push('statusCode');
						break;
					case 'sc-bytes':
						parsedFields.push('bytes');
						break;
					case 'time-taken':
						parsedFields.push('timeTaken');
						break;
					case 'cs(User-Agent)':
						parsedFields.push('userAgent');
						break;
					case 'cs(Referrer)':
						parsedFields.push('referrer');
						break;
					case 'cs-protocol':
						parsedFields.push('protocol');
						break;
					case 'x-cdn-location':
						parsedFields.push('cdnLocation');
						break;
					case 'x-cdn-status':
						parsedFields.push('cdnStatus');
						break;
					case 'x-host-header':
						parsedFields.push('hostHeader');
						break;
					default:
						parsedFields.push(value);
						break;
				}
			}
			parsedFields.push('timestamp');
			out = parsedFields;
		}
	}
	return out;
};

/**
 * Parse access log row
 * @param fields
 * @param line
 */
exports.parseRow = function(fields, line) {
	if(line.length > 0) {
		var date = 0;
		var time = 0;
		var parsedRow = {};
		var row = line.split(' ');
		var index = 0;

		for(var i in fields) {
			var field = fields[i];
			var value = row[index];
			switch(field) {
				case 'date':
					date = value;
					parsedRow[field] = value;
					break;
				case 'time':
					time = value;
					parsedRow[field] = value;
					break;
				case 'userAgent':
					parsedRow[field] = value + ' ' + row[index++] + ' ' + row[index++];
					break;
				default:
					parsedRow[field] = value;
			}
			index++;
		}
		parsedRow.timestamp = Math.floor(new Date(date + ' ' + time).getTime() / 1000);
		return parsedRow;
	}
	return null;
};
