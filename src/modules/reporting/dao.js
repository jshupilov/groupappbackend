'use strict';
var databaseUtils = require('../utils/databaseUtils');
var escape = require('pg-escape');
var hotelServices = require('../hotels/services');
var q = require('q');

/**
 * Get /hotels/{hotelId} quantity by hour
 * @param hotelGroupId {String}
 * @param startDate {Date}
 * @param endDate {Date}
 * @param callback {Function}
 */
exports.getHotelsImpressionQtyByHour = function (hotelGroupId, startDate, endDate, callback) {
	hotelServices.getHotelsRaw(hotelGroupId, function(err, hotels){
		if (err) {
			return callback(err);
		}
		databaseUtils.getConnectionStats(function (err, client, done) {
			if (err) {
				return callback(err);
			}
			var path = '/' + hotelGroupId + '/hotels/';
			var promises = [];
			var doIt = function (p, query, values, csvRow, csvIndex, date, chartRow, chartIndex) {
				client.query(query, values, function (err, res) {
					if(err){
						return p.reject(err);
					}
					var count = parseInt(res.rows[0].count);
					csvRow[csvIndex][date] = count;
					chartRow[chartIndex].Impressions += count;
					return p.resolve();
				});
			};

			var report = {csv: {data: []}, chart: []};
			if(startDate < endDate) {
				for (var i in hotels) {
					var sDate = new Date(startDate.getTime());
					var hotelId = hotels[i].data.hotelId;
					var hotelName = hotels[i].data.localized[0].name;
					var chartIndex = 0;
					report.csv.data[i] = {};
					report.csv.data[i]['Hotel name'] = hotelName;
					while (sDate < endDate) {
						var hour = sDate.getHours();
						var date = sDate.setHours(hour, 0, 0, 0);
						var start = Math.floor(date / 1000);
						var end = Math.ceil(sDate.setHours(hour, 59, 59, 999) / 1000);
						var p = q.defer();
						var values = [start, end, '%' + path + hotelId];
						var query = 'SELECT COUNT(data) FROM access_log ' +
							'WHERE (data->>\'timestamp\')::bigint >= $1 AND (data->>\'timestamp\')::bigint <= $2 AND ' +
							'(data->>\'path\') LIKE $3';

						if (i < 1) {
							report.chart[chartIndex] = {};
							report.chart[chartIndex].x = date;
							report.chart[chartIndex].Impressions = 0;
						}
						promises.push(p.promise);
						doIt(p, query, values, report.csv.data, i, sDate.toISOString(), report.chart, chartIndex);
						chartIndex++;
						sDate.setHours(hour + 1, 0, 0, 0);
					}
				}
				q.all(promises).done(function () {
					done();
					return callback(null, report);
				}, function (err) {
					done();
					return callback(err);
				});
			} else {
				return callback(null, report);
			}
		});
	});
};

/**
 * Get app registration quantity by hour
 * @param hotelGroupId {String}
 * @param startDate {Date}
 * @param endDate {Date}
 * @param callback {Function}
 */
exports.getAppRegistrationQtyByHour = function (hotelGroupId, startDate, endDate, callback) {
	databaseUtils.getConnectionStats(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		var path = '/' + hotelGroupId + '/app/registration';
		var promises = [];
		var report = {data: []};
		var doIt = function (p, query, values, report, date) {
			client.query(query, values, function (err, res) {
				if(err){
					return p.reject(err);
				}

				report.data.push({ date: date, count: res.rows[0].count });
				return p.resolve();
			});
		};
		while(startDate < endDate){
			var p = q.defer();
			var hour = startDate.getHours();
			var date = startDate.setHours(hour, 0, 0, 0);
			var start = Math.floor(date/1000);
			var end = Math.ceil(startDate.setHours(hour, 59, 59, 999)/1000);
			var values = [start, end, '%' + path + '%'];
			var query = 'SELECT COUNT(data) FROM access_log ' +
				'WHERE (data->>\'timestamp\')::bigint >= $1 AND (data->>\'timestamp\')::bigint <= $2 AND ' +
				'(data->>\'path\') LIKE $3';

			promises.push(p.promise);
			doIt(p, query, values, report, new Date(date).toISOString());
			startDate.setHours(hour + 1, 0, 0, 0);
		}

		q.all(promises).done(function () {
			done();
			return callback(null, report);
		}, function (err) {
			done();
			return callback(err);
		});
	});
};

/**
 * Add access log
 * @param accessLog
 * @param callback
 */
exports.addAccessLog = function (accessLog, callback) {
	if (accessLog.length > 0) {
		databaseUtils.getConnectionStats(function (err, client, done) {
			if (err) {
				return callback(err);
			}
			var values = accessLog.map(function (row) {
				return escape('(%L)', JSON.stringify(row));
			});
			var query = 'INSERT INTO access_log (data) VALUES ' + values.join(',');
			client.query(query, function (err, res) {
				done();
				return callback(err, res);
			});
		});
	} else {
		return callback(null);
	}
};
