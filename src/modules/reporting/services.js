/**
 * Created by jevgeni_shupilov on 04/11/15.
 */
'use strict';
var errors = require('../utils/errors');
var charts = require('../utils/charts');
var dao = require('./dao');
var json2csv = require('json2csv');
var validation = require('./validation');
var servicesMsg = require('../messaging/services');
var validationMsg = require('../messaging/validation');
var zlib = require('zlib');

errors.defineError('rprt-inv-csv', 'System error', 'Data set can not be converted to csv', 'Reporting.Services', 400);
errors.defineError('rprt-inv-date', 'System error', 'Sent date argument is invalid', 'Reporting.Services', 400);
errors.defineError('rprt-inv-gz', 'System error', 'Data set can not decompress gzip', 'Reporting.Services', 400);

/**
 * Get hotels impression quantity by hour JSON to CSV
 * @param hotelGroupId {String}
 * @param startDate {Date}
 * @param endDate {Date}
 * @param recipients {Object[]}
 * @param callback {Function}
 */
exports.getHotelsImpressionQtyByHour = function(hotelGroupId, startDate, endDate, recipients, callback){

    startDate = new Date(startDate);
    endDate = new Date(endDate);

    if(startDate.getTime() > 10) {
        if (endDate.getTime() > 10) {
            var sDate = startDate.toISOString();
            var eDate = endDate.toISOString();
            dao.getHotelsImpressionQtyByHour(hotelGroupId, startDate, endDate, function (err, report) {
                if (err) {
                    return callback(err);
                }
                var subject = 'Hotels Impressions report.';
                if (report.csv.data.length) {
                    var body = 'This report shows Group App hotels view (impressions) for the period ' + sDate + ' - ' + eDate + '.';
                    var axisNames = {x: 'Date', y: ''};
                    charts.createChartAndGetImage('impressions_report', report.chart, axisNames, function (err, image) {
                        if (err) {
                            return callback(err);
                        }
                        var imgAttachment = [{
                            type: 'image/jpg',
                            name: 'impressions_report.jpg',
                            content: new Buffer(image.body, 'binary').toString('base64')
                        }];
                        report.del = ',';
                        exports.sendEmail(report.csv, subject, recipients, body, imgAttachment, function (err, res) {
                            return callback(err, res);
                        });
                    });
                } else {
                    exports.sendEmail(report.csv, subject, recipients, 'There is no data for this period.', null, function (err, res) {
                        return callback(err, res);
                    });
                }
            });
        } else {
            return callback(errors.newError('rprt-inv-date', {
                endDate: endDate,
                providedBy: 'getHotelsImpressionQtyByHour'
            }));
        }
    } else {
        return callback(errors.newError('rprt-inv-date', {
            endDate: endDate,
            providedBy: 'getHotelsImpressionQtyByHour'
        }));
    }
};

/**
 * Get app/registration quantity by hour JSON to CSV
 * @param hotelGroupId {String}
 * @param startDate {Date}
 * @param endDate {Date}
 * @param recipients {Object[]}
 * @param callback {Function}
 */
exports.getAppRegistrationQtyByHour = function(hotelGroupId, startDate, endDate, recipients, callback){
    startDate = new Date(startDate);
    endDate = new Date(endDate);

    if(startDate.getTime() > 10) {
        if (endDate.getTime() > 10) {
            var sDate = startDate.toISOString();
            var eDate = endDate.toISOString();
            dao.getAppRegistrationQtyByHour(hotelGroupId, startDate, endDate, function (err, report) {
                if (err) {
                    return callback(err);
                }
                var subject = 'Activation report.';
                if(report.data.length) {
                    var body = 'This report shows Group App first runs (activations) for the period ' + sDate + ' - ' + eDate + '.';
                    var axisNames = {x: 'Date', y: ''};
                    var data = report.data.map(function (row) {
                        return {
                            x: new Date(row.date),
                            Activations: row.count
                        };
                    });
                    charts.createChartAndGetImage('activations_report', data, axisNames, function (err, image) {
                        if (err) {
                            return callback(err);
                        }
                        var imgAttachment = [{
                            type: 'image/jpg',
                            name: 'activations_report.jpg',
                            content: new Buffer(image.body, 'binary').toString('base64')
                        }];
                        report.fields = ['date', 'count'];
                        report.fieldNames = ['Date', 'Activations'];
                        report.del = ',';
                        exports.sendEmail(report, subject, recipients, body, imgAttachment, function (err, res) {
                            return callback(err, res);
                        });
                    });
                } else {
                    exports.sendEmail(report, subject, recipients, 'There is no data for this period.', null, function (err, res) {
                        return callback(err, res);
                    });
                }
            });
        } else {
            return callback(errors.newError('rprt-inv-date', {endDate: endDate, providedBy: 'getAppRegistrationQtyByHour'}));
        }
    } else {
        return callback(errors.newError('rprt-inv-date', {startDate: startDate, providedBy: 'getAppRegistrationQtyByHour'}));
    }
};

/**
 * Convert JSON to CSV
 * @param dataSet
 * @param callback
 */
exports.jsonToCsv = function(dataSet, callback){
    validation.validateReportData(dataSet, 'full', function(err){
        if(err){
            return callback(err);
        }
        if(dataSet.data.length) {
            json2csv(dataSet, function (err, csv) {
                if (err) {
                    return callback(errors.newError('rprt-inv-csv', {
                        originalError: err,
                        providedBy: 'dataSetValidation'
                    }));
                }
                return callback(null, csv);
            });
        } else {
            return callback(null);
        }
    });
};

/**
 * Send email to list of emails
 * @param report {Object}
 * @param subject {String}
 * @param recipients {Object}
 * @param body {String}
 * @param [imgAttachment] {Object}
 * @param callback {Function}
 */
exports.sendEmail = function(report, subject, recipients, body, imgAttachment, callback){

    validationMsg.validateRecipients(recipients, function(err){
        if(err){
            return callback(err);
        }
        exports.jsonToCsv(report, function(err, csv){
            if(err){
                return callback(err);
            }
            var attachments = [];
            if(csv){
                attachments.push({
                    type: 'text/csv',
                    name: 'report.csv',
                    content: new Buffer(csv).toString('base64')
                });
            }
            validationMsg.validateAttachments(imgAttachment, function(err){
                if(err){
                    return callback(err);
                }
                if(imgAttachment){
                    attachments = attachments.concat(imgAttachment);
                }
                //send the email
                var message = {
                    subject: subject,
                    from_email: 'no-reply@cardola.net',
                    from_name: 'Cardola GA Reports',
                    text: body,
                    to: recipients,
                    auto_text: true,
                    attachments: attachments
                };
                servicesMsg.sendRawEmail(message, callback);
            });
        });
    });
};

/**
 * Decompress gzip
 * @param input
 * @param callback
 */
exports.decompressGzip = function(input, callback){
    zlib.unzip(input, function(err, buffer){
        if(err){
            return callback(errors.newError('rprt-inv-gz', {originalError: err, providedBy: 'decompressGzip'}));
        }
        callback(null, buffer);
    });
};
