'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var accessLogs = require('../accessLogs');
var fs = require('fs');
var path = require('path');
var reportDataFile, reportDataString, reportData, fields, version, row;
var fileName = 'raw_access.log-2015110313';
var databaseUtils = require('../../utils/databaseUtils');

lab.experiment('reporting.decompressAndImportAccessLogFile', {timeout: 30000}, function() {
	var gzFile;

	var doneDb, client, maxId;

	lab.before(function(done) {
		databaseUtils.getConnectionStats(function(err, _client, _doneDB) {
			testingTools.code.expect(err).to.be.null();
			doneDb = _doneDB;
			client = _client;
			client.query('SELECT nextval(pg_get_serial_sequence(\'access_log\', \'id\'));', function(err, res) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				maxId = res.rows[0].nextval;
				done();
			});
		});
	});
	lab.after(function(done) {

		client.query('DELETE FROM access_log WHERE id > $1', [maxId], function(err) {
			doneDb();
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();

			done();
		});
	});

	lab.before(function(done) {
		var gzName = 'raw_access.log-2015110313.gz';
		fs.readFile(path.join(__dirname, gzName), function(err, data) {
			testingTools.code.expect(err).to.be.null();
			gzFile = data;
			done();
		});
	});

	lab.test('should failed', {timeout: 30000}, function(done) {
		accessLogs.decompressAndImportAccessLogFile(false, fileName, function(err) {
			testingTools.expectError(err, 'rprt-inv-gz');
			done();
		});
	});

	lab.test('should go well', {timeout: 30000}, function(done) {
		accessLogs.decompressAndImportAccessLogFile(gzFile, fileName, function(err) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});
	});
});

lab.experiment('reporting.parseAndStoreAccessLog', function() {
	var doneDb, client, maxId;

	lab.before(function(done) {
		databaseUtils.getConnectionStats(function(err, _client, _doneDB) {
			testingTools.code.expect(err).to.be.null();
			doneDb = _doneDB;
			client = _client;
			client.query('SELECT nextval(pg_get_serial_sequence(\'access_log\', \'id\'));', function(err, res) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				maxId = res.rows[0].nextval;
				done();
			});
		});
	});
	lab.after(function(done) {

		client.query('DELETE FROM access_log WHERE id > $1', [maxId], function(err, res) {
			doneDb();
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.be.equal(reportData.length - 2);

			done();
		});
	});

	lab.before(function(done) {
		fs.readFile(path.join(__dirname, fileName), function(err, data) {
			testingTools.code.expect(err).to.be.null();
			reportDataFile = data;
			reportDataString = new Buffer(data).toString('ascii');
			reportData = reportDataString.split('\n');
			version = reportData[0];
			fields = reportData[1];
			row = reportData[2];
			done();
		});
	});

	lab.test('should failed if can not parse access log', function(done) {
		accessLogs.parseAndStoreAccessLog('', fileName, function(err) {
			testingTools.expectError(err, 'ral-empty-file');
			done();
		});
	});

	lab.test('should go well', function(done) {
		accessLogs.parseAndStoreAccessLog(reportDataFile, fileName, function(err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res).to.include(['rows']);
			done();
		});
	});
});

lab.experiment('reporting.parseAndStoreAccessLog', function() {
	testingTools.killDbConnectionForExperiment(lab, true);

	lab.test('addAccessLog should fail if can not connect to db', function(done) {
		accessLogs.parseAndStoreAccessLog(reportDataFile, fileName, function(err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});

lab.experiment('reporting.parseVersion', function() {
	lab.test('should go well and return null if line is empty', function(done) {
		var parseVersion = accessLogs.parseVersion('');
		testingTools.code.expect(parseVersion).to.be.null();
		done();
	});
	lab.test('should go well', function(done) {
		var parseVersion = accessLogs.parseVersion(version);
		testingTools.code.expect(parseVersion).to.be.string().and.equal('1.0');
		done();
	});
});
lab.experiment('reporting.parseFields', function() {
	lab.test('should go well and return null if line is empty', function(done) {
		var parsedFields = accessLogs.parseFields('');
		testingTools.code.expect(parsedFields).to.be.null();
		done();
	});
	lab.test('should go well', function(done) {
		var parsedFields = accessLogs.parseFields(fields);
		testingTools.code.expect(parsedFields).to.be.array().and.not.empty();
		done();
	});
});

lab.experiment('reporting.parseRow', function() {

	var parsedFields;

	lab.before(function(done) {
		parsedFields = accessLogs.parseFields(fields);
		done();
	});

	lab.test('should go well and return null if line is empty', function(done) {
		var parsedRow = accessLogs.parseRow(parsedFields, '');
		testingTools.code.expect(parsedRow).to.be.null();
		done();
	});
	lab.test('should go well', function(done) {
		var parsedRow = accessLogs.parseRow(parsedFields, row);
		testingTools.code.expect(parsedRow).to.be.object().and.include(parsedFields);
		done();
	});
});

lab.experiment('reporting.parseAccessLog', function() {

	lab.test('should return error if can not convert access log', function(done) {
		accessLogs.parseAccessLog(null, fileName, function(err) {
			testingTools.expectError(err, 'ral-inv-file');
			done();
		});
	});

	lab.test('should return error if reportDataString is empty', function(done) {
		accessLogs.parseAccessLog('', fileName, function(err) {
			testingTools.expectError(err, 'ral-empty-file');
			done();
		});
	});

	lab.test('should return error if reportDataString has invalid line quantity', function(done) {
		var ver = new Buffer(version).toString('base64');
		accessLogs.parseAccessLog(ver, fileName, function(err) {
			testingTools.expectError(err, 'ral-inv-ln-qty');
			done();
		});
	});

	lab.test('should return error if reportDataString has invalid version', function(done) {
		var rds = reportDataString.replace(/#Version: /g, '');
		accessLogs.parseAccessLog(new Buffer(rds).toString('base64'), fileName, function(err) {
			testingTools.expectError(err, 'ral-inv-ver');
			done();
		});
	});

	lab.test('should return error if reportDataString has invalid fields', function(done) {
		var rds = reportDataString.replace(/#Fields: /g, '');
		accessLogs.parseAccessLog(new Buffer(rds).toString('base64'), fileName, function(err) {
			testingTools.expectError(err, 'ral-inv-fields');
			done();
		});
	});

	lab.test('should go well', function(done) {
		accessLogs.parseAccessLog(reportDataFile, fileName, function(err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.array();
			done();
		});
	});
});
