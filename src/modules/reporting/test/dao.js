'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var dao = require('../dao');
var accessLogs = require('../accessLogs');
var fs = require('fs');
var path = require('path');
var fileName = 'raw_access.log-2015110313';
var qtyPerTransaction = 1000;
var hotelGroupId = testingTools.getHotelGroupId();

lab.experiment('reporting.dao Interactions', {timeout: 30000}, function () {
	var accessLog;
	lab.before(function (done) {
		fs.readFile(path.join(__dirname, fileName), function(err, data){
			testingTools.code.expect(err).to.be.null();
			accessLogs.parseAccessLog(data, fileName, function(err, res){
				testingTools.code.expect(err).to.be.null();
				accessLog = res.splice(0, qtyPerTransaction);
				done();
			});
		});
	});

	lab.test('getHotelsImpressionQtyByHour should go well', function (done) {
		dao.getHotelsImpressionQtyByHour(hotelGroupId, new Date('2015-11-13 05:00:00'), new Date('2015-11-13 10:59:59'), function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.object().and.include(['csv', 'chart']);
			testingTools.code.expect(res.csv).to.be.object();
			testingTools.code.expect(res.chart).to.be.array();
			testingTools.code.expect(res.csv.data).to.be.array();
			for(var csvIndex in res.csv.data){
				testingTools.code.expect(res.csv.data[csvIndex]).to.be.object().and.include('Hotel name');
			}
			for(var chartIndex in res.chart){
				testingTools.code.expect(res.chart[chartIndex]).to.be.object().and.include('x');
			}
			done();
		});
	});
	lab.test('getHotelsImpressionQtyByHour should go well if report is empty', function (done) {
		dao.getHotelsImpressionQtyByHour(hotelGroupId, new Date('2015-11-13 05:00:00'), new Date('2015-11-13 05:00:00'), function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.object().and.include(['csv', 'chart']);
			testingTools.code.expect(res.csv).to.be.object();
			testingTools.code.expect(res.chart).to.be.array().and.empty();
			testingTools.code.expect(res.csv.data).to.be.array().and.empty();
			done();
		});
	});

	lab.test('getAppRegistrationQtyByHour should go well', function (done) {
		dao.getAppRegistrationQtyByHour(hotelGroupId, new Date('2015-11-03 12:00:00'), new Date('2015-11-03 23:59:59'), function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.object().and.include('data');
			testingTools.code.expect(res.data).to.be.array();
			for(var index in res.data){
				testingTools.code.expect(res.data[index]).to.be.object().and.include(['date', 'count']);
			}
			done();
		});
	});

	lab.test('addAccessLog should go well if access log data set is empty ', function (done) {
		dao.addAccessLog([], function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.undefined();
			done();
		});
	});

	lab.test('addAccessLog should go well if access log data set is not empty', function (done) {
		dao.addAccessLog(accessLog, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.be.equal(7);
			done();
		});
	});
});

lab.experiment('reporting.dao.getHotelsImpressionQtyByHour DB structure invalid', function () {
	testingTools.failDbQueryForExperiment(lab, 'hotel');

	lab.test('should fail if miss hotel table', function (done) {
		dao.getHotelsImpressionQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), new Date('2015-11-03 02:59:59'), function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});
lab.experiment('reporting.dao DB structure invalid', function () {
	testingTools.failDbQueryForExperiment(lab, 'access_log', true);

	lab.test('getAppRegistrationQtyByHour should fail', function (done) {
		dao.getAppRegistrationQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), new Date('2015-11-03 02:59:59'), function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
	lab.test('getHotelsImpressionQtyByHour should fail', function (done) {
		dao.getHotelsImpressionQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), new Date('2015-11-03 02:59:59'), function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});
lab.experiment('reporting.dao DB Connection', function () {
	var accessLog;
	lab.before(function (done) {
		fs.readFile(path.join(__dirname, fileName), function(err, data){
			testingTools.code.expect(err).to.be.null();
			accessLogs.parseAccessLog(data, fileName, function(err, res){
				testingTools.code.expect(err).to.be.null();
				accessLog = res.splice(0, qtyPerTransaction);
				done();
			});
		});
	});
	testingTools.killDbConnectionForExperiment(lab, true);

	lab.test('getAppRegistrationQtyByHour should fail', function (done) {
		dao.getAppRegistrationQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), new Date('2015-11-03 02:59:59'), function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('getHotelsImpressionQtyByHour should fail', function (done) {
		dao.getHotelsImpressionQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), new Date('2015-11-03 02:59:59'), function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('addAccessLog should fail', function (done) {
		dao.addAccessLog(accessLog, function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});
