'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var charts = require('../../utils/charts');
var fs = require('fs');
var path = require('path');
var hotelGroupId = testingTools.getHotelGroupId();
var recipients = [{email: 'group-app-dev@cardola.com'}];

function getDataSet (){
    return JSON.parse(JSON.stringify({
        data: [{
            test_Column1: 'some test info',
            test_Column2: 34,
            test_Column3: false,
            test_Column4: ''
        }, {
            test_Column1: 'some test info 3',
            test_Column2: 1,
            test_Column3: null,
            test_Column4: true
        }],
        fields: ['test_Column1', 'test_Column2', 'test_Column3', 'test_Column4']
    }));
}

lab.experiment('reporting.getHotelsImpressionQtyByHour', function () {
    testingTools.failDbQueryForExperiment(lab, 'access_log', true);

    lab.test('should fail if query is wrong', function (done) {
        services.getHotelsImpressionQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), new Date('2015-11-03 02:59:59'), recipients, function(err){
            testingTools.expectError(err, 'db-query-fails');
            done();
        });
    });
});

lab.experiment('reporting.getHotelsImpressionQtyByHour', {timeout: 30000}, function () {
   testingTools.mockResponsesForExperiment(lab, undefined, function(mitm){
        mitm.on('connection', function(socket){
            socket.destroy();
        });
    });

    lab.test('should fail if can not get chart image', function (done) {
        services.getHotelsImpressionQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), new Date('2015-11-03 23:59:59'), recipients, function(err){
            testingTools.expectError(err, 'iu-err-up-img');
            done();
        });
    });
});

lab.experiment('reporting.getHotelsImpressionQtyByHour', {timeout: 30000}, function() {


    lab.test('should fail, if startDate is not date', {timeout: 30000}, function (done) {
        services.getHotelsImpressionQtyByHour(hotelGroupId, true, new Date('2015-11-03 02:59:59'), recipients, function(err){
            testingTools.expectError(err, 'rprt-inv-date');
            done();
        });
    });

    lab.test('should fail, if endDate is not date', {timeout: 30000}, function (done) {
        services.getHotelsImpressionQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), true, recipients, function(err){
            testingTools.expectError(err, 'rprt-inv-date');
            done();
        });
    });

    lab.test('should go well and send msg if report is empty', {timeout: 30000}, function (done) {
        services.getHotelsImpressionQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), new Date('2015-11-03 00:00:00'), recipients, function(err, res){
            testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
            testingTools.code.expect(res[0].status).to.be.match(/^(sent|queued)$/);
            done();
        });
    });
    lab.test('should go well', {timeout: 30000}, function (done) {
        services.getHotelsImpressionQtyByHour(hotelGroupId, new Date('2015-11-05 00:00:00'), new Date('2015-11-05 23:59:59'), recipients, function(err, res){
            testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
            testingTools.code.expect(res[0].status).to.be.match(/^(sent|queued)$/);
            done();
        });
    });
});

lab.experiment('reporting.getAppRegistrationQtyByHour', function () {
    testingTools.failDbQueryForExperiment(lab, 'access_log', true);

    lab.test('should fail if query is wrong', function (done) {
        services.getAppRegistrationQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), new Date('2015-11-03 02:59:59'), recipients, function(err){
            testingTools.expectError(err, 'db-query-fails');
            done();
        });
    });
});

lab.experiment('reporting.getAppRegistrationQtyByHour', {timeout: 30000}, function () {
   testingTools.mockResponsesForExperiment(lab, undefined, function(mitm){
        mitm.on('connection', function(socket){
            socket.destroy();
        });
    });

    lab.test('should fail if can not get chart image', function (done) {
        services.getAppRegistrationQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), new Date('2015-11-03 23:59:59'), recipients, function(err){
            testingTools.expectError(err, 'iu-err-up-img');
            done();
        });
    });
});

lab.experiment('reporting.getAppRegistrationQtyByHour', {timeout: 30000}, function() {


    lab.test('should fail, if startDate is not date', {timeout: 30000}, function (done) {
        services.getAppRegistrationQtyByHour(hotelGroupId, true, new Date('2015-11-03 02:59:59'), recipients, function(err){
            testingTools.expectError(err, 'rprt-inv-date');
            done();
        });
    });

    lab.test('should fail, if endDate is not date', {timeout: 30000}, function (done) {
        services.getAppRegistrationQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), true, recipients, function(err){
            testingTools.expectError(err, 'rprt-inv-date');
            done();
        });
    });

    lab.test('should go well and send msg if report is empty', {timeout: 30000}, function (done) {
        services.getAppRegistrationQtyByHour(hotelGroupId, new Date('2015-11-03 00:00:00'), new Date('2015-11-03 00:00:00'), recipients, function(err, res){
            testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
            testingTools.code.expect(res[0].status).to.be.match(/^(sent|queued)$/);
            done();
        });
    });
    lab.test('should go well', {timeout: 30000}, function (done) {
        services.getAppRegistrationQtyByHour(hotelGroupId, new Date('2015-11-03 12:00:00'), new Date('2015-11-03 23:59:59'), recipients, function(err, res){
            testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
            testingTools.code.expect(res[0].status).to.be.match(/^(sent|queued)$/);
            done();
        });
    });
});


lab.experiment('reporting.decompressGzip', {timeout: 30000}, function() {
    var gzFile, csvFile;

    lab.before(function (done) {
        var gzName = 'raw_access.log-2015110313.gz';
        fs.readFile(path.join(__dirname, gzName), function(err, data){
            testingTools.code.expect(err).to.be.null();
            gzFile = data;
            done();
        });
    });

    lab.before(function (done) {
        var fileName = 'raw_access.log-2015110313';
        fs.readFile(path.join(__dirname, fileName), function(err, data) {
            testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
            csvFile = data;
            done();
        });
    });

    lab.test('should failed if invalid gzip file', {timeout: 30000}, function (done) {
        services.decompressGzip(false, function(err){
            testingTools.expectError(err, 'rprt-inv-gz');
            done();
        });
    });

    lab.test('should go well', {timeout: 30000}, function (done) {
        services.decompressGzip(gzFile, function(err, res){
            testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
            testingTools.code.expect(res, JSON.stringify(err)).to.deep.equal(csvFile);
            done();
        });
    });
});
lab.experiment('reporting.jsonToCsv', {timeout: 30000}, function() {
    var dataSet;

    lab.beforeEach(function (done) {
        dataSet = getDataSet();
        done();
    });

    lab.test('should give error if data set not as an object', function (done) {
        dataSet = [];
        services.jsonToCsv(dataSet, function (err) {
            testingTools.expectError(err, 'rpv-inv-type');
            done();
        });
    });

    lab.test('should give error if can not convert to csv', function (done) {
        dataSet.fieldNames = ['test-name'];
        services.jsonToCsv(dataSet, function (err) {
            testingTools.expectError(err, 'rprt-inv-csv');
            done();
        });
    });

    lab.test('should go well', function (done) {
        services.jsonToCsv(dataSet, function (err, res) {
            testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
            testingTools.code.expect(res).to.be.string().and.not.empty();
            done();
        });
    });
});

lab.experiment('reporting.sendEmail', {timeout: 30000}, function() {
    var dataSet;
    var subject = 'test subject';
    var body = 'Test text';
    lab.beforeEach(function (done) {
        dataSet = getDataSet();
        done();
    });

    lab.test('should give error if data set not as an object', function (done) {
        dataSet = [];
        services.sendEmail(dataSet, subject, recipients, body, null, function (err) {
            testingTools.expectError(err, 'rpv-inv-type');
            done();
        });
    });

    lab.test('should give error if data set not as an object', function (done) {
        services.sendEmail(dataSet, subject, [], body, null, function (err) {
            testingTools.expectError(err, 'msv-inv-val');
            testingTools.code.expect(err.data.debuggingData.providedBy).to.include('validateRecipients');
            done();
        });
    });

    lab.test('should go well if data of report object is empty', function (done) {
        dataSet.data = [];
        services.sendEmail(dataSet, subject, recipients, body, null, function (err, res) {
            testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
            testingTools.code.expect(res[0].status).to.be.match(/^(sent|queued)$/);
            done();
        });
    });

    lab.test('should go well if image attachment exists', function (done) {
        var imgAttachment = [{
            name: 'active_report.jpg',
            content: new Buffer('some binary stuff', 'binary').toString('base64')
        }];
        services.sendEmail(dataSet, subject, recipients, body, imgAttachment, function (err) {
            testingTools.expectError(err, 'msv-key-miss');
            done();
        });
    });

    lab.test('should go well if image attachment exists', function (done) {
        dataSet = {data: [ 
            { date: 'Tue Nov 03 2015 12:00:00 GMT+0200 (EET)', count: '0', something: '3' },
            { date: 'Tue Nov 03 2015 13:00:00 GMT+0200 (EET)', count: '0', something: '3' },
            { date: 'Tue Nov 03 2015 14:00:00 GMT+0200 (EET)', count: '0', something: '3' },
            { date: 'Tue Nov 03 2015 15:00:00 GMT+0200 (EET)', count: '0', something: '3' },
            { date: 'Tue Nov 03 2015 16:00:00 GMT+0200 (EET)', count: '13', something: '33' },
            { date: 'Tue Nov 03 2015 17:00:00 GMT+0200 (EET)', count: '5', something: '3' },
            { date: 'Tue Nov 03 2015 18:00:00 GMT+0200 (EET)', count: '3', something: '3' },
            { date: 'Tue Nov 03 2015 19:00:00 GMT+0200 (EET)', count: '0', something: '3' },
            { date: 'Tue Nov 03 2015 20:00:00 GMT+0200 (EET)', count: '0', something: '3' },
            { date: 'Tue Nov 03 2015 21:00:00 GMT+0200 (EET)', count: '0', something: '3' },
            { date: 'Tue Nov 03 2015 22:00:00 GMT+0200 (EET)', count: '0', something: '3' },
            { date: 'Tue Nov 03 2015 23:00:00 GMT+0200 (EET)', count: '0', something: '3' }
        ]};
        
        var axisNames = {x: 'Date', y: ''};
        var data = dataSet.data.map(function (row) {
            return {
                x: new Date(row.date),
                count: row.count,
                something: row.something
            };
        });
        charts.createChartAndGetImage('active_report_with_chart', data, axisNames, function (err, image) {
            testingTools.code.expect(err).to.be.null();
            var imgAttachment = [{
                type: 'image/jpg',
                name: 'active_report.jpg',
                content: new Buffer(image.body, 'binary').toString('base64')
            }];

            dataSet.fields = ['date', 'count'];
            dataSet.fieldNames = ['Date', 'Activations'];
            services.sendEmail(dataSet, subject, recipients, 'Test reporting 1', imgAttachment, function (err, res) {
                testingTools.code.expect(err).to.be.null();
                testingTools.code.expect(res[0].status).to.be.match(/^(sent|queued)$/);
                done();
            });
        });
    });

    lab.test('should go well', function (done) {
        services.sendEmail(dataSet, subject, recipients, body, null, function (err, res) {
            testingTools.code.expect(err).to.be.null();
            testingTools.code.expect(res[0].status).to.be.match(/^(sent|queued)$/);
            done();
        });
    });
});
