'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var validation = require('../validation');

lab.experiment('reporting.validateReportData', {timeout: 30000}, function() {
    var getDataSet, dataSet;
    lab.before(function (done) {
        getDataSet = function(){
            return JSON.parse(JSON.stringify({
                data: [{
                    test_Column1: 'some test info',
                    test_Column2: 34,
                    test_Column3: false,
                    test_Column4: ''
                }, {
                    test_Column1: 'some test info 3',
                    test_Column2: 1,
                    test_Column3: null,
                    test_Column4: true
                }],
                fields: ['test_Column1', 'test_Column2', 'test_Column3', 'test_Column4']
            }));
        };
        done();
    });
    lab.beforeEach(function (done) {
        dataSet = getDataSet();
        done();
    });
    lab.test('should give error if validation type is invalid', function (done) {
        dataSet = [];
        validation.validateReportData(dataSet, 'invalid-type', function (err) {
            testingTools.expectError(err, 'rpv-inv-val-type');
            done();
        });
    });
    lab.test('should give error if data set not as object', function (done) {
        dataSet = [];
        validation.validateReportData(dataSet, 'full', function (err) {
            testingTools.expectError(err, 'rpv-inv-type');
            testingTools.code.expect(err.data.debuggingData.key).to.be.equal('rootObject');
            done();
        });
    });
    lab.test('should give error if data set not contain data key ', function (done) {
        delete dataSet.data;
        validation.validateReportData(dataSet, 'full', function (err) {
            testingTools.expectError(err, 'rpv-key-miss');
            testingTools.code.expect(err.data.debuggingData.key).to.be.equal('report.data');
            done();
        });
    });
    lab.test('should give error if data key of the data set has invalid type', function (done) {
        dataSet.data = 34;
        validation.validateReportData(dataSet, 'full', function (err) {
            testingTools.expectError(err, 'rpv-inv-type');
            testingTools.code.expect(err.data.debuggingData.key).to.be.equal('report.data');
            done();
        });
    });

    lab.test('should go well if data key of the data set is empty', function (done) {
        dataSet.data = [];
        validation.validateReportData(dataSet, 'full', function (err, res) {
            testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
            testingTools.code.expect(res).to.be.object().and.deep.equal(dataSet);
            done();
        });
    });

    lab.test('should give error if one item of the data property in report data set is not an object', function (done) {

        var _dataSet = getDataSet();
        _dataSet.data[0] = 23;
        validation.validateReportData(_dataSet, 'full', function (err) {
            testingTools.expectError(err, 'rpv-inv-val-type');
            testingTools.code.expect(err.data.debuggingData.key).to.be.equal('report.data[0]');
            _dataSet = getDataSet();
            _dataSet.data[1] = 45;
            validation.validateReportData(_dataSet, 'full', function (err) {
                testingTools.expectError(err, 'rpv-inv-val-type');
                testingTools.code.expect(err.data.debuggingData.key).to.be.equal('report.data[1]');
                done();
            });
        });
    });

    lab.test('should go well', function (done) {
        validation.validateReportData(dataSet, 'full', function (err, res) {
            testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
            testingTools.code.expect(res).to.be.object().and.deep.equal(dataSet);
            done();
        });
    });
});
