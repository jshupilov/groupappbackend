/**
 * Created by jevgeni_shupilov on 06/11/15.
 */
'use strict';
var errors = require('../utils/errors');
var check = require('check-types');
var validationUtils = require('../utils/validationUtils');

errors.defineError('rpv-key-miss', 'System error', 'A key is missing from the data set', 'Reporting.Validation', 400);
errors.defineError('rpv-inv-type', 'System error', 'A value has a wrong data type', 'Reporting.Validation', 400);
errors.defineError('rpv-empty-arr', 'System error', 'Data set array keys can not be empty', 'Reporting.Validation', 400);
errors.defineError('rpv-inv-val-type', 'System error', 'Invalid validation type provided', 'Reporting.Validation', 500);

/**
 * Validate root keys of report data
 * @param report
 * @param callback
 */
function validateRootKeys(report, validationType, callback){
    validationUtils.validateHasKeys(['data'], report, function(missingKey){
        if(missingKey){
            return callback(errors.newError('rpv-key-miss', {key: 'report.' + missingKey, providedBy: 'dataSetValidation'}));
        }
        validationUtils.validateTypeOfKeys(['data'], ['array'], report, 'report.', 'rpv-inv-type', function(err){
            if (err) {
                return callback(errors.newError(err.code, err.details));
            }
            return callback(null, report);
        });
    });
};

/**
 * Validate report data field
 * @param report
 * @param callback
 */
function validateData(report, validationType, callback){
    if(check.emptyArray(report.data)){
        return callback(null, report);
    } else {
        validationUtils.validateArray(
            report.data,
            function(row, i, cb){
                if (!check.object(row)) {
                    return cb(errors.newError('rpv-inv-val-type', {key: 'report.data[' + i + ']', value: row }));
                }
                return cb(null);
            },
            function(err){
                return callback(err, report);
            }
        );
    }
};

/**
 * Validate the report data
 * @param report
 * @param validationType
 * @param callback
 */
exports.validateReportData = function(report, validationType, callback){

    var validationTypes = {
        full: {
            functions: [
                validateRootKeys,
                validateData
            ]
        }
    };


    if(validationTypes.hasOwnProperty(validationType)){

        var checkIndex = 0;
        var valCaller = function(){
            //call first check
            validationTypes[validationType].functions[checkIndex](report, validationType, function(err, res){

                if(err){
                    return callback(err);
                    //call next check
                }else if( validationTypes[validationType].functions[checkIndex+1] !== undefined ){
                    checkIndex++;
                    valCaller();
                }else{
                    return callback(null, res);
                }
            });
        };

        if( !check.object(report) ){
            return callback(errors.newError('rpv-inv-type', {key: 'rootObject', providedBy: 'validateReportData'}));
        }

        valCaller();

    }else{
        callback(errors.newError('rpv-inv-val-type', {providedBy: 'validateReportData'}));
    }

};
