/**
 * @apiDefine BookingsQuery
 * @apiVersion 1.7.4
 * @apiParam (Booking query) {String} dummy parameter
 */


/**
 * @apiDefine ReservationStructure
 * @apiVersion 1.7.4
 *
 * @apiParam (Reservation resource) {String} startDate Defines when reservation start.
 * @apiParam (Reservation resource) {String} hotelId Identification code of hotel.
 * @apiParam (Reservation resource) {String} hotelGroupId Identification code of hotel group.
 * @apiParam (Reservation resource) {Object} reservation Contain reservation details.
 * @apiParam (Reservation resource) {Object} reservation.UniqueID Used to provide PMS and/or CRS identifiers.
 * @apiParam (Reservation resource) {String} reservation.UniqueID.ID A unique identifying value assigned by the creating system.
 * This value is the actual reservation confirmation number.

 * @apiParam (Reservation resource) {Object} reservation.RoomStays Collection of room stays.
 * @apiParam (Reservation resource) {Object[]} reservation.RoomStays.RoomStay Room stay associated with this reservation.
 * @apiParam (Reservation resource) {Object} reservation.RoomStays.RoomStay.RoomTypes
 * @apiParam (Reservation resource) {Object[]} reservation.RoomStays.RoomStay.RoomTypes.RoomType
 * @apiParam (Reservation resource) {Integer} reservation.RoomStays.RoomStay.RoomTypes.RoomType.NumberOfUnits The number of this type of room booked.
 * @apiParam (Reservation resource) {Object} reservation.RoomStays.RoomStay.RoomTypes.RoomType.RoomDescription
 * @apiParam (Reservation resource) {String} reservation.RoomStays.RoomStay.RoomTypes.RoomType.RoomDescription.Name Room Type Name
 *
 *
 * @apiParam (Reservation resource) {Object} reservation.RoomStays.RoomStay.RatePlans Collection of Rate plans
 * @apiParam (Reservation resource) {Object[]} reservation.RoomStays.RoomStay.RatePlans.RatePlan All details pertaining to a specific rate plan.
 * If multiple rate plans are booked for a reservation and the channel supports reservations with more than one rate plan,
 * a RatePlan element will be added for each unique rate plan booked on the reservation.
 * @apiParam (Reservation resource) {String} reservation.RoomStays.RoomStay.RatePlans.RatePlan.RatePlanName Rate Type Name.
 *
 * @apiParam (Reservation resource) {Object} reservation.RoomStays.RoomStay.TimeSpan
 * @apiParam (Reservation resource) {Object} reservation.RoomStays.RoomStay.TimeSpan.Start The starting value of the time span
 * @apiParam (Reservation resource) {Object} reservation.RoomStays.RoomStay.TimeSpan.End The ending value of the time span.
 * @apiParam (Reservation resource) {Integer} reservation.RoomStays.RoomStay.TimeSpan.Duration Number of nights
 * @apiParam (Reservation resource) {Object} reservation.RoomStays.RoomStay.GuestCounts
 * @apiParam (Reservation resource) {Object[]} reservation.RoomStays.RoomStay.GuestCounts.GuestCount
 * @apiParam (Reservation resource) {Integer=8,10} [reservation.RoomStays.RoomStay.GuestCounts.GuestCount.AgeQualifyingCode=10] A code representing a business rule that determines the charges for a guest based upon age range
 *
 * Allowed values:
 * - 8 (Child)
 * - 10 (Adult)
 * @apiParam (Reservation resource) {Integer} reservation.RoomStays.RoomStay.GuestCounts.GuestCount.Count Count of guests

 *
 *
 */

/**
 * @api {get} - 1. Reservations resource
 * @apiName ReservationsRes
 * @apiVersion 1.7.4
 * @apiGroup 6_Reservations
 * @apiDescription Reservation is a room reservation in a hotel. This resource represents all important details about this Reservation.
 * @apiUse ReservationStructure

 @apiExample {json} Reservation example
 {
	"startDate": "2015-08-28T11:21:53Z",
	"hotelId": "ernst",
	"hotelGroupId": "test-hgid",
	"reservation": {
		"UniqueID": {
			"ID": "38nfks0"
		},
		"RoomStays": {
			"RoomStay": [
				{
					"RoomTypes": {
						"RoomType": [
							{
								"NumberOfUnits": 1,
								"RoomDescription": {
									"Name": "Junior Suite"
								}
							}
						]
					},
					"RatePlans": {
						"RatePlan": [
							{
								"RatePlanName": "Stay & play (Breakfast included)"
							}
						]
					},
					"TimeSpan": {
						"Start": "2015-01-12",
						"End": "2015-01-15",
						"Duration": 3
					},
					"GuestCounts": {
						"GuestCount": [
							{
								"AgeQualifyingCode": 10,
								"Count": 2
							},{
								"AgeQualifyingCode": 8,
								"Count": 1
							}
						]
					}
				}
			]
		}
	}
}
 *
 */


/**
 * @api {get} /reservations/search?ref=<ref>&lastname=<lastname> 2. Search for reservations
 * @apiName searchReservations
 * @apiVersion 1.7.4
 * @apiGroup 6_Reservations
 * @apiDescription Searches for reservations by entered reservation reference and last name. This method returns a Collection of Reservation objects.
 *
 * [About collections](#api-1_Basics-basicsCollections)
 * @apiParam (Query) {string} ref Reservation reference
 * @apiParam (Query) {string} lastname Last name of the booker
 *
 * @apiError (Possible error codes) {String} bk-src-inv-arg Invalid input arguments ([Definition](#error_bk-src-inv-arg))
 *
 * @apiSuccessExample {json} Example response:
 {
  "href": "http://localhost:8000/v1/test-hgid/reservations/search/?ref=123ABC&name=smith",
  "offset": 0,
  "limit": 10,
  "total": 1,
  "items": [
    {
      ... Reservation resource...
    }
  ]
 *
 *
*/

/**
 * @api {get} /profiles/xxx/reservations?hotel=<hotelId> 3. Get all reservations of a client
 * @apiName getReservations
 * @apiVersion 1.7.4
 * @apiGroup 6_Reservations
 * @apiDescription Returns list of all the future reservations of a client. This method returns a Collection of Reservation objects.
 *
 * [About collections](#api-1_Basics-basicsCollections)
 * @apiParam (Query) {string} [hotel] Optionally an hotel Id to receive only reservations to cretain hotel
  *
 * @apiError (Possible error codes) {String} bk-src-inv-arg Invalid input arguments ([Definition](#error_bk-src-inv-arg))
 *
 * @apiSuccessExample {json} Example response:
 {
  "href": "http://localhost:8000/v1/test-hgid/profiles/89da89ua/reservations/?hotel=test-hotel",
  "offset": 0,
  "limit": 10,
  "total": 1,
  "items": [
    {
      ... Reservation resource...
    }
  ]
 *
 *
*/
