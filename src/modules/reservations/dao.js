'use strict';
var databaseUtils = require('../utils/databaseUtils');

/**
 * Save item to reservation DB table
 * @param data
 * @param callback
 */
exports.save = function(data, callback){
	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query('INSERT INTO reservation (data) VALUES($1)',
			[data],
			function (err1, result) {
				done();
				return callback(err1, result);
			}
		);
	});
};

/**
 * Find reservation in DB table
 * @param data
 * @param callback
 */
exports.find = function(resHashKey, callback){
	var keys = ['$1'];
	var values = ['"' + resHashKey + '"'];
	if(Array.isArray(resHashKey)){
		for(var i = 0; i < resHashKey.length; i++){
			keys[i] = '$' + (i + 1);
			values[i] = '"' + resHashKey[i] + '"';
		}
	}
	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query(
			'SELECT data ' +
			'FROM reservation ' +
			'WHERE (data -> \'referenceHash\') in (' + keys.join(',') +') ' +
			'ORDER BY id ASC ',
			values,
			function (err1, result) {
				done();
				if(err1){
					return callback(err1);
				}
				return callback(null, result.rows);
			}
		);
	});
};
