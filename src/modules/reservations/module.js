'use strict';
var services = require('./services');
var errors = require('../utils/errors');
var apiUtils = require('../utils/apiUtils');

var bookingsModule = {
	register: function(server, options, next){
		/*		Search for reservations		 */
		server.route({
			method: 'GET',
			path: '/reservations/{referenceNumber}/{lastName}',
			handler: function(request, reply){
				apiUtils.ensureSecurity(3, request.apiRequest, function(securityError){
					if(securityError){
						return reply(errors.wrapToBoom(securityError));
					}
					services.searchReservation(request.apiRequest, function(err, res){
						apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res));
					});
				});

			}
		});
		/*		Get all reservations of a client		 */
		server.route({
			method: 'GET',
			path: '/profiles/{profileId}/reservations',
			handler: function(request, reply){
				apiUtils.ensureSecurity(3, request.apiRequest, function(securityError){
					if(securityError){
						return reply(errors.wrapToBoom(securityError));
					}
					services.getAllAccountReservations(request.apiRequest, function(err, res){
						apiUtils.addCachingHeaders(reply(errors.wrapToBoom(err), res));
					});
				});

			}
		});
		next();
	}
};

bookingsModule.register.attributes = {
	name: 'Bookings',
	version: '1.0.0'
};

exports.register = bookingsModule.register;
exports.services = services;
