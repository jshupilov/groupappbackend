'use strict';

var q = require('q');
var dao = require('./dao');
var errors = require('../utils/errors');
var profiles = require('../profiles/services');
var cryptoUtils = require('../utils/cryptoUtils');
var hotelGroups = require('../hotel_groups/services');
var secureStorage = require('../secure_storage/services');
var collectionUtils = require('../utils/collectionUtils');

errors.defineError('res-cd-save', 'System Error', 'Can not save reservation key to Stormpath custom data', 'Reservations');
errors.defineError('res-not-auth', 'System Error', 'This user not authenticated. Please log in and try again', 'Reservations');
errors.defineError('res-inv-email', 'System Error', 'This profile email is invalid', 'Reservations');

/**
 * Search reservation in our system
 * @param apiRequest
 * @param callback
 */

exports.searchReservation = function(apiRequest, callback) {
	var ref = { lastName: apiRequest.params.lastName, referenceNumber: apiRequest.params.referenceNumber };
	exports.findReservation(ref, function(err, reservation){
		if(err){
			return callback(err);
		}
		var out = collectionUtils.formatCollection(reservation, null, null, null, null, apiRequest);
		return callback(null, out);
	});
};

/**
 * Get all reservations which depends with given account
 * @param apiRequest
 * @param callback
 */
exports.getAllAccountReservations = function(apiRequest, callback) {
	var enEmail = apiRequest.params.profileId;
	try {
		var deEmail = profiles.decryptEmail(enEmail);
	} catch (e){
		return callback(errors.newError('res-inv-email', { originalError: e }));
	}
	exports.getAccountReservations(deEmail, apiRequest.hotelGroupId, function(err, reservations){
		var out = collectionUtils.formatCollection(reservations, null, null, null, null, apiRequest);
		return callback(err, out);
	});
};

/**
 * reservationFormat return formatted and decrypted reservation details
 * @param reservation
 * @param callback
 */

function reservationFormat(reservation, callback) {
	secureStorage.retrieveData(reservation.secureStorage, function(err, res) {
		if(err) {
			return callback(err);
		}
		var out = {
			startDate: reservation.startDate,
			hotelId: reservation.hotelId,
			hotelGroupId: reservation.hotelGroupId,
			reservation: res
		};
		return callback(null, out);
	});
}

/**
 * Save reservation to database
 * @param ref
 * @param hId
 * @param hgId
 * @param reservation
 * @param accountEmail
 * @param callback
 */
exports.saveReservation = function(ref, hId, hgId, reservation, accountEmail, callback) {
	secureStorage.storeData(reservation, function(err, ssId) {
		if(err) {
			return callback(err);
		}
		var refHashKey = cryptoUtils.sha256(JSON.stringify(ref));
		var reservationData = {
			startDate: new Date().toISOString(),
			hotelId: hId,
			hotelGroupId: hgId,
			referenceHash: refHashKey,
			secureStorage: ssId
		};
		dao.save(reservationData, function(err2) {
			if(err2) {
				return callback(err2);
			}
			if(accountEmail) {
				hotelGroups.getProfilesConfiguration(hgId, function (err3, pc) {
					if (err3) {
						return callback(err3, refHashKey);
					}
					profiles.getAccount(pc.spDirectoryHref, accountEmail, function (err4, account) {
						if (err4) {
							return callback(err4, refHashKey);
						}
						account.customData.reservations = account.customData.reservations || [];
						account.customData.reservations.push(refHashKey);
						account.customData.save(function (err5) {
							if(err5){
								return callback(errors.newError('res-cd-save', {originalError: err5}), refHashKey);
							}
							return callback(null, refHashKey);
						});
					});
				});
			} else {
				return callback(null, refHashKey);
			}
		});
	});
};

/**
 * Find reservation in database
 * @param ref
 * @param callback
 */
exports.findReservation = function(ref, callback) {
	var refHashKey = cryptoUtils.sha256(JSON.stringify(ref));
	dao.find(refHashKey, function(err, reservation) {
		if(err) {
			return callback(err);
		}
		if(reservation.length > 0) {
			reservationFormat(reservation[0].data, function (err1, out) {
				return callback(err1, [out]);
			});
		} else {
			return callback(null, []);
		}
	});
};

/**
 * Get reservation in database by Account
 * @param accountEmail
 * @param hgId
 * @param callback
 */
exports.getAccountReservations = function(accountEmail, hgId, callback){
    hotelGroups.getProfilesConfiguration(hgId, function(err, pc) {
        if (err) {
            return callback(err);
        }
        profiles.getAccount(pc.spDirectoryHref, accountEmail, function (err1, account) {
			if (err1) {
				return callback(err1);
			}
			if (account.customData.reservations) {
				dao.find(account.customData.reservations, function (err2, reservations) {
					if (err2) {
						return callback(err2);
					}
					var result = [];
					var promises = [];
					var doIt = function (p, data, index) {
						reservationFormat(data, function (err3, out) {
							if (err3) {
								return p.reject(err3);
							}
							result[index] = out;
							return p.resolve();
						});
					};
					for (var i = 0; i < reservations.length; i++) {
						var p = q.defer();
						promises.push(p.promise);
						doIt(p, reservations[i].data, i);
					}
					q.all(promises).done(function () {
						return callback(null, result);
					}, function (err3) {
						return callback(err3);
					});

				});
			} else {
				return callback(null, []);
			}
        });
    });
};
