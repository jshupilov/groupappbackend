'use strict';
var testingTools = require('../../../test/testingTools');
var databaseUtils = require('../../utils/databaseUtils');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var dao = require('../dao');
var data = {
	startDate: new Date().toISOString(),
	hotelId: 'test-hotel-id',
	hotelGroupId: testingTools.getHotelGroupId(),
	referenceHash: 'test-reference-hash-key',
	secureStorage: 'test-secure-storage'
};

lab.experiment('Reservations.dao Interactions', function() {
	lab.after(function(done){
		databaseUtils.getConnection(function(err, client, doneCon){
			testingTools.code.expect(err).to.be.null();
			client.query(
				'DELETE FROM reservation where (data->\'referenceHash\') = $1',
				['"' + data.referenceHash + '"'],
				function(err){
					doneCon();
					testingTools.code.expect(err).to.be.null();
					done();
				}
			);
		});
	});

	lab.test('save should save item to database table', function(done) {
		dao.save(data, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.be.equal(1);
			done();
		});
	});

	lab.test('find should find reservation hash keys in database table', function(done) {
		dao.find([data.referenceHash, data.referenceHash], function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array().and.have.length(1);
			testingTools.code.expect(res[0]).to.be.object().and.include(['data']);
			testingTools.code.expect(res[0].data).to.be.object().and.include(Object.keys(data));
			done();
		});
	});
});

lab.experiment('Reservations.dao Interactions with no DB', function() {
	testingTools.killDbConnectionForExperiment(lab);

	lab.test('save should return error, when doing a query', function(done) {
		dao.save(data, function(err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});

	});

	lab.test('find should return error, when doing a query', function(done) {
		dao.find(data.referenceHash, function(err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});

	});
});

lab.experiment('Reservations.dao Interactions with corrupted DB', function() {
	testingTools.failDbQueryForExperiment(lab, 'reservation');

	lab.test('save should return error, when doing a query', function(done) {
		dao.save(data, function(err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});

	});

	lab.test('find should return error, when doing a query', function(done) {
		dao.find(data.referenceHash, function(err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});

	});
});
