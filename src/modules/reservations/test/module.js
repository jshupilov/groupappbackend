'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var module = require('../module');
var services = require('../services');
var apiUtils = require('../../utils/apiUtils');
var profile = require('../../profiles/services');
var databaseUtils = require('../../utils/databaseUtils');

function deleteReservation(resHashKey, callback){
	databaseUtils.getConnection(function(err, client, doneCon){
		testingTools.code.expect(err).to.be.null();
		client.query(
			'DELETE FROM reservation where (data->\'referenceHash\') = $1',
			['"' + resHashKey + '"'],
			function(err1, res){
				doneCon();
				return callback(err1, res);
			}
		);
	});
}

lab.experiment('reservations.module', function(){
	var _uuid, resHashKey;
	var hgId = testingTools.getHotelGroupId();
	var hId = 'test-hotel-id';
	var reservation = { test_key: 'some-test-key' };
	var aVer = testingTools.getApiVersion();
	var accountEmail = testingTools.createAccountForExperiment(lab).email;
	var ref = { lastName: 'test-last-name', referenceNumber: 'test-ref-number' };
	var header = {'x-cardola-verify': process.env.TEST_CHECKSUM};
	var fakeServer = {
		routes: [],
		route: function(def){
			this.routes.push(def);
		}
	};

	lab.before({timeout: 10000}, function(done){
		testingTools.getUuid(function(err, uuid){
			testingTools.code.expect(err).to.be.null();
			_uuid = uuid;
			done();
		});
	});

	lab.before({timeout: 10000}, function (done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey) {
			testingTools.code.expect(err).to.be.null();
			resHashKey = hashKey;
			done();
		});
	});

	lab.after(function (done) {
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});
	lab.test('register should work', {timeout: 10000}, function(done){
		module.register(fakeServer, {}, function(){
			testingTools.code.expect(fakeServer.routes).to.have.length(2);

			testingTools.code.expect(fakeServer.routes[0]).to.be.object();
			testingTools.code.expect(fakeServer.routes[0].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[0].path).to.equal('/reservations/{referenceNumber}/{lastName}');

			testingTools.code.expect(fakeServer.routes[1]).to.be.object();
			testingTools.code.expect(fakeServer.routes[1].method).to.equal('GET');
			testingTools.code.expect(fakeServer.routes[1].path).to.equal('/profiles/{profileId}/reservations');

			done();
		});
	});

	lab.test('Search reservations should work', {timeout: 10000}, function(done){
		var req = { apiRequest: apiUtils.createApiRequest('https://reservations/', header, ref, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, aVer, hgId, _uuid);
		fakeServer.routes[0].handler(req, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.items).to.be.array().and.have.length(1);
			testingTools.code.expect(res.items[0]).to.be.object();
			testingTools.code.expect(res.items[0].hotelId).to.be.equal(hId);
			testingTools.code.expect(res.items[0].hotelGroupId).to.be.equal(hgId);
			return testingTools.expectCachingHeaders(done, 'default', res);
		});
	});

	lab.test('Get user reservations should work', {timeout: 10000}, function(done){
		var email = profile.encryptEmail(accountEmail);
		var req = { apiRequest: apiUtils.createApiRequest('https://profiles/xxx/reservations', header, {profileId: email}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, aVer, hgId, _uuid);
		fakeServer.routes[1].handler(req, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.items).to.be.array().and.have.length(1);
			testingTools.code.expect(res.items[0]).to.be.object();
			testingTools.code.expect(res.items[0].hotelId).to.be.equal(hId);
			testingTools.code.expect(res.items[0].hotelGroupId).to.be.equal(hgId);
			return testingTools.expectCachingHeaders(done, 'default', res);
		});
	});

	lab.test('Get user reservations wo https should not work', {timeout: 10000}, function(done){
		var req = { apiRequest: apiUtils.createApiRequest('/profiles/xxx/reservations', header, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, aVer, hgId, _uuid);
		fakeServer.routes[0].handler(req, function(err){
			testingTools.expectError(err, 'au-https-req');
			done();
		});
	});

	lab.test('Search reservations wo https should not work', {timeout: 10000}, function(done){
		var req = { apiRequest: apiUtils.createApiRequest('/reservations/123/Doe', header, {}, 'GET', {})};
		apiUtils.populateApiRequest(req.apiRequest, aVer, hgId, _uuid);
		fakeServer.routes[1].handler(req, function(err){
			testingTools.expectError(err, 'au-https-req');
			done();
		});
	});
});
