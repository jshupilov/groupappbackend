'use strict';

exports.labLib = require('lab');
var services = require('../services');
var apiUtils = require('../../utils/apiUtils');
var testingTools = require('../../../test/testingTools');
var profiles = testingTools.requireUncached('../modules/profiles/services');
var databaseUtils = require('../../utils/databaseUtils');
var lab = exports.lab = exports.labLib.script();

function deleteReservation(resHashKey, callback){
	databaseUtils.getConnection(function(err, client, doneCon){
		testingTools.code.expect(err).to.be.null();
		client.query(
			'DELETE FROM reservation where (data->\'referenceHash\') = $1',
			['"' + resHashKey + '"'],
			function(err1, res){
				doneCon();
				return callback(err1, res);
			}
		);
	});
}

lab.experiment('Reservations.services.saveReservation', function() {
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = 'test-email';
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	testingTools.failDbQueryForExperiment(lab, 'ss_data');

	lab.test('should return error, if cannot store secure data', {timeout: 10000}, function(done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});


lab.experiment('Reservations.services.saveReservation', function() {
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = 'test-email';
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	testingTools.failDbQueryForExperiment(lab, 'reservation');

	lab.test('should return error, if cannot save reservation data to table', {timeout: 10000}, function(done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});

lab.experiment('Reservations.services.saveReservation', function() {
	var resHashKey;
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = null;
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	lab.after(function(done){
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});
	lab.test('should work, if account email is null', function(done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hashKey).to.be.a.string().and.not.to.be.empty();
			resHashKey = hashKey;
			done();
		});
	});
});

lab.experiment('Reservations.services.saveReservation', function() {
	var resHashKey;
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = 'test-email';
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	testingTools.failDbQueryForExperiment(lab, 'hotel_group');

	lab.after(function(done){
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});
	lab.test('should return error, if cannot get profiles configurations', {timeout: 10000}, function(done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey){
			testingTools.expectError(err, 'db-query-fails');
			testingTools.code.expect(hashKey).to.be.string().and.not.to.be.empty();
			resHashKey = hashKey;
			done();
		});
	});
});

lab.experiment('Reservations.services.saveReservation', function() {
	var resHashKey;
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = 'test-email';
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	lab.after(function(done){
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});

	lab.test('should return error, if cannot get account', {timeout: 10000}, function(done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey){
			testingTools.expectError(err, 'ps-acc-get');
			testingTools.code.expect(hashKey).to.be.string().and.not.to.be.empty();
			resHashKey = hashKey;
			done();
		});
	});
});

lab.experiment('Reservations.services.saveReservation', function() {
	var resHashKey;
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = testingTools.createAccountForExperiment(lab).email;
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };
	var called;

	lab.before(function (done) {
		called = 0;
		done();
	});
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm){
		mitm.on('connect', function(socket) {
			if(called !== 3 ){
				socket.bypass();
			}
			called++;
		});
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});

	lab.after(function(done){
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});


	lab.test('should return error, if cannot save to customData', {timeout: 10000}, function(done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey){
			testingTools.expectError(err, 'res-cd-save');
			testingTools.code.expect(hashKey).to.be.string().and.not.to.be.empty();
			resHashKey = hashKey;
			done();
		});
	});
});

lab.experiment('Reservations.services.saveReservation', function() {
	var resHashKey;
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = testingTools.createAccountForExperiment(lab).email;
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	lab.afterEach(function(done){
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});

	lab.test('should work if customData.reservations is undefined', {timeout: 10000}, function(done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hashKey).to.be.string().and.not.to.be.empty();
			resHashKey = hashKey;
			done();
		});
	});

	lab.test('should work if customData.reservations is not undefined', {timeout: 10000}, function(done) {
		ref.refNumber = 'new-test-ref-number';
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hashKey).to.be.string().and.not.to.be.empty();
			resHashKey = hashKey;
			done();
		});
	});
});

lab.experiment('Reservations.services.findReservation', function() {
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	testingTools.killDbConnectionForExperiment(lab);

	lab.test('should return error, if cannot get reservation table', function(done) {
		services.findReservation(ref, function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});

lab.experiment('Reservations.services.findReservation', function() {
	var resHashKey = 'test-hash-key';
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = testingTools.createAccountForExperiment(lab).email;
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	lab.before({timeout: 10000}, function (done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey) {
			testingTools.code.expect(err).to.be.null();
			resHashKey = hashKey;
			done();
		});
	});

	testingTools.failDbQueryForExperiment(lab, 'ss_data');

	lab.after(function (done) {
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});

	lab.test('should return error, if cannot find reservation data in secure storage', {timeout: 10000}, function(done) {
		services.findReservation(ref, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});

lab.experiment('Reservations.services.findReservation', function() {
	var resHashKey = 'test-hash-key';
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = testingTools.createAccountForExperiment(lab).email;
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	lab.before({timeout: 10000}, function (done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey) {
			testingTools.code.expect(err).to.be.null();
			resHashKey = hashKey;
			done();
		});
	});

	lab.after(function(done){
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});

	lab.test('should work if reservation NOT empty', {timeout: 10000}, function(done) {
		services.findReservation(ref, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array().and.have.length(1);
			testingTools.code.expect(res[0]).to.include(['startDate', 'hotelId', 'hotelGroupId', 'reservation']);
			testingTools.code.expect(res[0].hotelId).to.be.a.string().and.to.equal(hId);
			testingTools.code.expect(res[0].hotelGroupId).to.be.a.string().and.to.equal(hgId);

			done();
		});
	});
});

lab.experiment('Reservations.services.findReservation', function() {
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	lab.test('should work if reservation empty', {timeout: 10000}, function(done) {
		services.findReservation(ref, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array().and.to.be.empty();
			done();
		});
	});
});

lab.experiment('Reservations.services.getAccountReservations', function() {
	var email = 'test-email';

	testingTools.killDbConnectionForExperiment(lab);

	lab.test('should return error, if cannot get ProfilesConfiguration', function(done) {
		services.getAccountReservations(email, testingTools.getHotelGroupId(), function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});


lab.experiment('Reservations.services.getAccountReservations', {timeout: 10000}, function() {
	lab.test('should return error, if cannot get account', function(done) {
		services.getAccountReservations('test-email', testingTools.getHotelGroupId(), function(err){
			testingTools.expectError(err, 'ps-acc-get');
			done();
		});
	});
});

lab.experiment('Reservations.services.getAccountReservations', function() {
	var hgId = testingTools.getHotelGroupId();
	var accountEmail = testingTools.createAccountForExperiment(lab).email;

	lab.test('should work and give empty array, if account reservation list is empty', {timeout: 10000}, function(done) {
		services.getAccountReservations(accountEmail, hgId, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.an.array().and.to.be.empty();
			done();
		});
	});
});

lab.experiment('Reservations.services.getAccountReservations', function() {
	var resHashKey = 'test-hash-key';
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = testingTools.createAccountForExperiment(lab).email;
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	lab.before({timeout: 10000}, function (done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey) {
			testingTools.code.expect(err).to.be.null();
			resHashKey = hashKey;
			done();
		});
	});

	testingTools.failDbQueryForExperiment(lab, 'reservation');

	lab.after(function (done) {
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});
	lab.test('should return error, if cannot find reservation data in database table', {timeout: 10000}, function(done) {
		services.getAccountReservations(accountEmail, hgId, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});

lab.experiment('Reservations.services.getAccountReservations', function() {
	var resHashKey = 'test-hash-key';
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = testingTools.createAccountForExperiment(lab).email;
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	lab.before({timeout: 10000}, function (done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey) {
			testingTools.code.expect(err).to.be.null();
			resHashKey = hashKey;
			done();
		});
	});

	testingTools.failDbQueryForExperiment(lab, 'ss_data');

	lab.after(function (done) {
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});
	lab.test('should return error, if cannot find reservation data in secure storage table', {timeout: 10000}, function(done) {
		services.getAccountReservations(accountEmail, hgId, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});

lab.experiment('Reservations.services.getAccountReservations', function() {
	var resHashKey = 'test-hash-key';
	var hId = 'test-hotel-id';
	var hgId = testingTools.getHotelGroupId();
	var reservation = { test_key: 'some-test-key' };
	var accountEmail = testingTools.createAccountForExperiment(lab).email;
	var ref = { lastName: 'test-last-name', refNumber: 'test-ref-number' };

	lab.before({timeout: 10000}, function (done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey) {
			testingTools.code.expect(err).to.be.null();
			resHashKey = hashKey;
			done();
		});
	});
	lab.after(function (done) {
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});
	lab.test('should work', {timeout: 10000}, function(done) {
		services.getAccountReservations(accountEmail, hgId, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array().and.have.length(1);
			testingTools.code.expect(res[0]).to.include(['startDate', 'hotelId', 'hotelGroupId', 'reservation']);
			testingTools.code.expect(res[0].hotelId).to.be.a.string().and.to.equal(hId);
			testingTools.code.expect(res[0].hotelGroupId).to.be.a.string().and.to.equal(hgId);
			testingTools.code.expect(res[0].reservation).to.be.an.object().and.to.deep.equal(reservation);
			done();
		});
	});
});

lab.experiment('Reservations.services.searchReservation', function() {
	var uuid;
	var hgId = testingTools.getHotelGroupId();
	var ref = { lastName: 'test-last-name', referenceNumber: 'test-ref-number' };

	lab.before({timeout: 10000}, function (done) {
		testingTools.getUuid(function(err, uuid2){
			testingTools.code.expect(err).to.be.null();
			uuid = uuid2;
			done();
		});
	});

	testingTools.failDbQueryForExperiment(lab, 'reservation');

	lab.test('should return error, if cannot find reservation data in database table', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, ref, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), hgId, uuid);
		services.searchReservation(req, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});
});

lab.experiment('Reservations.services.searchReservation', function() {
	var resHashKey, uuid;
	var hgId = testingTools.getHotelGroupId();
	var accountEmail = testingTools.createAccountForExperiment(lab).email;
	var ref = { lastName: 'test-last-name', referenceNumber: 'test-ref-number' };
	var reservation = { test_key: 'some-test-key' };
	var hId = 'test-hotel-id';

	lab.before({timeout: 10000}, function (done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey) {
			testingTools.code.expect(err).to.be.null();
			resHashKey = hashKey;
			done();
		});
	});

	lab.before({timeout: 10000}, function (done) {
		testingTools.getUuid(function(err, uuid2){
			testingTools.code.expect(err).to.be.null();
			uuid = uuid2;
			done();
		});
	});
	lab.after(function (done) {
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});
	lab.test('should work', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, ref, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), hgId, uuid);
		services.searchReservation(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.an.object().and.to.include(['items']);
			testingTools.code.expect(res.items).to.be.array().and.have.length(1);
			testingTools.code.expect(res.items[0]).to.include(['startDate', 'hotelId', 'hotelGroupId', 'reservation']);
			testingTools.code.expect(res.items[0].hotelId).to.be.a.string().and.to.equal(hId);
			testingTools.code.expect(res.items[0].hotelGroupId).to.be.a.string().and.to.equal(hgId);
			testingTools.code.expect(res.items[0].reservation).to.be.an.object().and.to.deep.equal(reservation);
			done();
		});
	});
});

lab.experiment('Reservations.services.getAllAccountReservations', function() {
	var resHashKey, uuid;
	var hgId = testingTools.getHotelGroupId();
	var accountEmail = testingTools.createAccountForExperiment(lab).email;
	var ref = { lastName: 'test-last-name', referenceNumber: 'test-ref-number' };

	lab.before({timeout: 10000}, function (done) {
		var hId = 'test-hotel-id';
		var reservation = { test_key: 'some-test-key' };
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey) {
			testingTools.code.expect(err).to.be.null();
			resHashKey = hashKey;
			done();
		});
	});

	lab.before({timeout: 10000}, function (done) {
		testingTools.getUuid(function(err, uuid2){
			testingTools.code.expect(err).to.be.null();
			uuid = uuid2;
			done();
		});
	});

	lab.after(function (done) {
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});
	lab.test('should return error if email is invalid', {timeout: 10000}, function(done) {
		var req = apiUtils.createApiRequest('/', {}, { profileId: false }, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), hgId, uuid);
		services.getAllAccountReservations(req, function(err){
			testingTools.expectError(err, 'res-inv-email');
			done();
		});
	});
});

lab.experiment('Reservations.services.getAllAccountReservations', function() {
	var resHashKey, uuid;
	var hgId = testingTools.getHotelGroupId();
	var accountEmail = testingTools.createAccountForExperiment(lab).email;
	var ref = { lastName: 'test-last-name', referenceNumber: 'test-ref-number' };
	var hId = 'test-hotel-id';
	var reservation = { test_key: 'some-test-key' };

	lab.before({timeout: 10000}, function (done) {
		services.saveReservation(ref, hId, hgId, reservation, accountEmail, function(err, hashKey) {
			testingTools.code.expect(err).to.be.null();
			resHashKey = hashKey;
			done();
		});
	});

	lab.before({timeout: 10000}, function (done) {
		testingTools.getUuid(function(err, uuid2){
			testingTools.code.expect(err).to.be.null();
			uuid = uuid2;
			done();
		});
	});
	lab.after(function (done) {
		deleteReservation(resHashKey, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(1);
			done();
		});
	});
	lab.test('should work', {timeout: 10000}, function(done) {
		var email = profiles.encryptEmail(accountEmail);
		var req = apiUtils.createApiRequest('/', {}, { profileId: email }, 'GET', {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), hgId, uuid);
		services.getAllAccountReservations(req, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.an.object().and.to.include(['items']);
			testingTools.code.expect(res.items).to.be.array().and.have.length(1);
			testingTools.code.expect(res.items[0]).to.include(['startDate', 'hotelId', 'hotelGroupId', 'reservation']);
			testingTools.code.expect(res.items[0].hotelId).to.be.a.string().and.to.equal(hId);
			testingTools.code.expect(res.items[0].hotelGroupId).to.be.a.string().and.to.equal(hgId);
			testingTools.code.expect(res.items[0].reservation).to.be.an.object().and.to.deep.equal(reservation);
			done();
		});
	});
});
