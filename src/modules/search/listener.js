/**
 * Created by jevgenishupilov on 19/01/15.
 */
'use strict';
var eventUtils = require('../utils/eventUtils');
var logging = require('../utils/logging');
var services = require('./services');
services.init();

//update hotel index when hotel is inserted

eventUtils.emitter.on('hotel.published', function(data, callback) {
	services.addHotelToIndex(data, function(err, res) {
		if(err) {
			logging.err('Search index entry NOT updated: ', data.hotelId, JSON.stringify(err));
			return callback(err);
		} else {
			logging.info('Search index entry updated ', data.hotelId);
		}
		callback(null, res);
	});

});

eventUtils.emitter.on('hotel.unpublished', function(hotelGroupId, hotelId, callback) {
	services.removeHotelFromIndex(hotelGroupId, hotelId, function(err, res) {
		if(err) {
			logging.err('Search index entry NOT updated ', hotelId, JSON.stringify(err));
			return callback(err);
		} else {
			logging.info('Search index entry item removed ', hotelId);
		}
		callback(null, res);
	});

});

eventUtils.emitter.on('hotelGroup.created', function(data, callback) {
	services.reCreateHotelGroupIndexes(data.hotelGroupId, function(err, res) {
		if(err) {
			logging.err('Search index not recreated ', data.hotelGroupId, JSON.stringify(err));
			return callback(err);
		}
		logging.info('Search index recreated ', data.hotelGroupId);
		callback(null, res);

	});

});
