'use strict';
var HttpsAgent = require('agentkeepalive').HttpsAgent;
var Algolia = require('algolia-search');
var errors = require('../utils/errors');
var q = require('q');
var collectionUtils = require('../utils/collectionUtils');
var langUtils = require('../utils/langUtils');
var hgServices = require('../hotel_groups/services');

errors.defineError('sr-err-sav-ix', 'System error occurred', 'Error saving to search index', 'Search');
errors.defineError('sr-err-lis-ix', 'System error occurred', 'Error listing search indexes', 'Search');
errors.defineError('sr-err-src', 'Error searching', 'Error searching', 'Search');
errors.defineError('sr-err-new', 'System error occurred', 'Error creating search index', 'Search');
errors.defineError('sr-err-del', 'System error occurred', 'Error deleting search index', 'Search');
errors.defineError('sr-itm-err-del', 'System error occurred', 'Error deleting item from search index', 'Search');

/**
 * Init client
 * @type {AlgoliaSearch}
 */
var algoliaClient = null;



exports.init = function(timeout, noAgent) {
	var agent;

	if(noAgent === undefined || noAgent !== true){
		agent = new HttpsAgent({
			keepAlive: true,
			//maxSockets: 20,
			maxKeepAliveRequests: 0, // no limit on max requests per keepalive socket
			keepAliveTimeout: 30000 // keepalive for 30 seconds
		});

	}
	algoliaClient = new Algolia(
		process.env.ALGOLIASEARCH_APPLICATION_ID,
		process.env.ALGOLIASEARCH_API_KEY,
		agent
	);

	if(timeout){
		algoliaClient.setTimeout(timeout);
	}
};

exports.getSearchConfiguration = function(hgid){
	var searchConfiguration = {
		'test-hgid': {},
		mchotels: {},
		slh: {
			resultOrdering: 'random'
		}
	};

		return searchConfiguration[hgid];
};

/**
 * Get hotels index name
 * @param hotelGroupId
 * @returns {string}
 */
function getHotelsIndexName(hotelGroupId) {
	return process.env.ALGOLIASEARCH_PREFIX + hotelGroupId + '_hotels';
}

/**
 * Get index
 * @param indexName
 * @returns {*}
 */
function getIndex(indexName) {
	return algoliaClient.initIndex(indexName);
}

/**
 * Get list of index names, which given hotel group uses
 * @param hotelGroupId
 * @returns {*[]}
 */
function getHotelGroupIndexNames(hotelGroupId) {
	return [
		getHotelsIndexName(hotelGroupId)
	];
}

/**
 * Get hotels index
 * @param hotelGroupId
 * @returns {*}
 */
function getHotelsIndex(hotelGroupId) {
	return getIndex(getHotelsIndexName(hotelGroupId));
}

/**
 * Get all hotel group indexes
 * @param hotelGroupId
 * @param callback callback(err, [index1, index2])
 */
exports.listHotelGroupIndexes = function (hotelGroupId, callback) {
	var out = [];
	var indexes = getHotelGroupIndexNames(hotelGroupId);
	algoliaClient.listIndexes(function(error, content) {
		if(error) {
			if(content.message.code){
				return callback(errors.newError('sr-err-lis-ix', {originalError: content.message, info: 'Network Error', providedBy: 'listHotelGroupIndexes'}) );
			}
			return callback(errors.newError('sr-err-lis-ix', {errorContent: content}));
		}
		content.items.forEach(function(index) {
			if( indexes.indexOf(index.name) > -1) {
				out.push(index);
			}
		});
		callback(null, out);
	});
};

/**
 * Initialize a new hotel index
 *
 * @param indexName
 * @param callback
 * @param waitForCreation
 */
function initHotelIndex(indexName, callback, waitForCreation) {
	var index = getIndex(indexName);
	index.setSettings({
		'attributesToIndex': ['name', 'location', 'categoryNames'], //all
		'customRanking': [
			'desc(vote_count)',
			'asc(name)',
			'asc(categoryNames)',
			'asc(location)'
		],
		queryType: 'prefixNone',
		typoTolerance: 'false',
		ignorePlurals: false,
		minWordSizefor1Typo: 5,
		attributesForFaceting: ['categoryIds', 'stars']
	}, function(err, res){
		if(err){
			if(res.message.code){
				return callback(errors.newError('sr-err-new', {originalError: res.message, info: 'Network Error', providedBy: 'initHotelIndex'}) );
			}
			return callback(errors.newError('sr-err-new', {errorMessage: res.message, indexName: indexName}));
		}
		if(waitForCreation){
			index.waitTask(res.taskID, function() {
				callback(null, res);
			});
		}else{
			callback(null, res);
		}


	});
}

/**
 * Save list of objects to index
 * @param indexName
 * @param objects
 * @param callback
 * @param [waitForIndexing] {boolean} set to true if you want the callback to be called when item has been written to index
 *
 */
function saveObjectsToIndex(indexName, objects, callback, waitForIndexing) {

	var index = getIndex(indexName);
	index.saveObjects(objects, function(err, res) {
		if(err) {
			if(res.message.code){
				return callback(errors.newError('sr-err-sav-ix', {originalError: res.message, info: 'Network Error', providedBy: 'saveObjectsToIndex'}) );
			}
			return callback(errors.newError('sr-err-sav-ix', {errorMessage: res.message}));
		}
		if(waitForIndexing){
			index.waitTask(res.taskID, function() {
				callback(null, {
					index: indexName,
					objects: objects,
					result: res
				});
			});
		}else{

			callback(null, {
				index: indexName,
				objects: objects,
				result: res
			});
		}
	});
}


/**
 * Format hotel for indexing
 * @param hotelData
 * @returns {{objectID: *, _geoloc: {lat: *, lng: *}}}
 */
function formatHotelForIndex(hotelData, callback) {
	var out = {
		objectID: hotelData.hotelId,
		_geoloc: {
			lat: hotelData.coordinates.lat,
			lng: hotelData.coordinates.long
		},
		categoryNames: [],
		categoryIds: hotelData.categoryIds,
		name: [],
		location: [],
		contacts: [],
		description: []
	};
	hgServices.getCategories(hotelData.hotelGroup.hotelGroupId, function(err, categories){
		if(err){
			return callback(err);
		}
		for(var categoryIdIndex in  hotelData.categoryIds){
			var categoryId = hotelData.categoryIds[categoryIdIndex];
			for(var categoryIndex in categories){
				var category = categories[categoryIndex];
				if(category.categoryId === categoryId) {
					var localized = langUtils.getLocalizedDataForAllLanguages(category);
					for(var locIndex in localized){
						var val = localized[locIndex];
						out.categoryNames.push(val.name);
					}
				}

			}
		}

		hotelData.localized.forEach(function(local){

			out.name.push(local.name);
			out.location.push(local.location);
			out.contacts.push(local.contacts);
			out.description.push(local.description);
		});
		return callback(null, out);
	});
}

/**
 * Delete hotel group indexes
 * @param hotelGroupId
 * @param callback
 * @param [waitForDelete] set to true to wait for deletion to be completed
 */
exports.deleteHotelGroupIndexes = function(hotelGroupId, callback, waitForDelete) {
	exports.listHotelGroupIndexes(hotelGroupId, function(err, indexes) {
		if(err) {
			return callback(err);
		}
		var promises = [];
		indexes.forEach(function(index) {
			var ip = q.defer();
			promises.push(ip.promise);
			algoliaClient.deleteIndex(index.name, function(error, content) {
				if(error) {
					if(content.message.code){
						ip.reject(errors.newError('sr-err-del', {originalError: content.message, info: 'Network Error', providedBy: 'deleteHotelGroupIndexes'}));
					} else {
						ip.reject(errors.newError('sr-err-del', {errorMessage: content.message, index: index.name}));
					}
				} else {
					if(waitForDelete){
						getIndex(index.name).waitTask(content.taskID, function() {
							ip.resolve();
						});
					}else{
						ip.resolve();
					}
				}
			});
		});

		q.all(promises).done(function() {
			callback(null, true);
		}, function(err) {
			callback(err);
		});

	});
};

/**
 * Create hotel group indexes
 * @param hotelGroupId
 * @param callback
 * @param [waitForCreation] set to true to confirm that indexes have been created
 */
exports.createHotelGroupIndexes = function(hotelGroupId, callback, waitForCreation) {
	initHotelIndex(getHotelsIndexName(hotelGroupId), function(err, res){
		if(err){
			return callback(err);
		}
		callback(null, res);
	}, waitForCreation);
};

/**
 * Delete and create hotel group indexes
 * @param hotelGroupId
 * @param callback
 * @param [deletedCallback] Callback to be called when items have been deleted
 */
exports.reCreateHotelGroupIndexes = function(hotelGroupId, callback, deletedCallback) {
	deletedCallback = deletedCallback ? deletedCallback : function(){};
	exports.deleteHotelGroupIndexes(hotelGroupId, function(err1, res1) {
		if(err1) {
			return callback(err1);
		}
		deletedCallback();
		exports.createHotelGroupIndexes(hotelGroupId, function(err2, res2) {
			if(err2) {
				return callback(err2);
			}
			callback(null, {deleteResult: res1, createResult: res2});
		});
	});
};

/**
 *
 * @param data
 * @param callback
 * @param [waitForIndexing] {boolean} set to true to wait for indexing
 */
exports.addHotelToIndex = function(data, callback, waitForIndexing) {
	var index = getHotelsIndexName(data.hotelGroup.hotelGroupId);
	formatHotelForIndex(data, function(err, hotelData){
		if(err){
			callback(err);
		} else {
			saveObjectsToIndex(index, [hotelData], callback, waitForIndexing);
		}
	});

};

/**
 * Cut search results to match the offset and limit
 * @param pageOffset
 * @param items
 * @param searchResult
 */
exports.cutSearchResults = function(pageOffset, items, searchResult) {
	searchResult.hits = searchResult.hits.slice(pageOffset, pageOffset + items);
};

/**
 * Search for hotels
 * @param searchFilters
 * @param hotelGroupId
 * @param callback
 */
exports.searchHotels = function(searchFilters, hotelGroupId, callback) {
	var index = getHotelsIndex(hotelGroupId);

	var conf = {
		hitsPerPage: 1000,
		facetFilters: [],
		attributesToRetrieve: ['objectID'],
		attributesToHighlight: [],
		attributesToSnippet: [],
		getRankingInfo: 0,
		facets: '*',
		queryType: 'prefixNone',
		typoTolerance: 'false',
		ignorePlurals: false,
		maxValuesPerFacet: 10000
	};
	var pageOffset = 0;

	if(searchFilters.geoLocation) {
		conf.aroundLatLng = searchFilters.geoLocation.latitude + ',' + searchFilters.geoLocation.longitude;
		//radius is not mandatory
		if(searchFilters.geoLocation.radius !== undefined){
			conf.aroundRadius = searchFilters.geoLocation.radius;
		}else{
			//by default 20 000 KM - which includes all the world
			conf.aroundRadius = 20000000;
		}
	}

	if(searchFilters.categoryIds && searchFilters.categoryIds.length > 0) {
		searchFilters.categoryIds.forEach(function(catId) {
			conf.facetFilters.push('categoryIds:' + catId);
		});
	}

	if(searchFilters.collectionQuery) {
		conf.hitsPerPage = searchFilters.collectionQuery.limit;
		if(searchFilters.collectionQuery.offset && searchFilters.collectionQuery.offset > 0) {
			var pagination = collectionUtils.offsetLimitToPageItems(searchFilters.collectionQuery.offset, searchFilters.collectionQuery.limit);
			conf.hitsPerPage = pagination.items;
			conf.page = pagination.page;
			pageOffset = pagination.pageOffset;
		}
	}


	index.search(searchFilters.keyword ? searchFilters.keyword : '', function(error, content) {
				if(error) {
					if(content.message.code) {
						return callback(errors.newError('sr-err-src', {
							originalError: content.message,
							info: 'Network Error',
							providedBy: 'searchHotels'
						}));
					}
					return callback(errors.newError('sr-err-src', {errorMessage: content.message}));
				}
				exports.cutSearchResults(pageOffset, conf.hitsPerPage, content);

				if(searchFilters.hasOwnProperty('keyword') && exports.getSearchConfiguration(hotelGroupId).resultOrdering === 'random') {
					var newHits = [];
					while(content.hits.length > 0) {
						var idx = Math.floor(Math.random() * content.hits.length);
						newHits.push(content.hits[idx]);
						content.hits.splice(idx, 1);
					}
					content.hits = newHits;
				}
				callback(null, content);
			}, conf
	);
};

/**
 * Remove hotel from index
 * @param hotelGroupId
 * @param hotelId
 * @param callback
 * @param [waitForIndexing] {boolean} set to true to wait for actual removal from index
 */
exports.removeHotelFromIndex = function(hotelGroupId, hotelId, callback, waitForIndexing){
	var index = getHotelsIndex(hotelGroupId);
	index.deleteObject(hotelId, function(err, res){
		if(err){
			if(res.message.code){
				return callback(errors.newError('sr-itm-err-del', {originalError: res.message, info: 'Network Error', providedBy: 'removeHotelFromIndex'}) );
			}
			return callback(errors.newError('sr-itm-err-del', {errorMessage: res.message}));
		}
		if(waitForIndexing){
			index.waitTask(res.taskID, function() {
				callback(null, {
					hotelId: hotelId,
					result: res,
					done: true
				});
			});
		}else{
			callback(null, {
				hotelId: hotelId,
				result: res,
				done: false
			});
		}
	});
};
