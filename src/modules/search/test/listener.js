/**
 * Created by jevgenishupilov on 10/02/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var eventUtils = require('../../utils/eventUtils');



lab.experiment('search listener', function(){
	var addHotelToIndexCalled = 0;
	var reCreateHotelGroupIndexesCalled = 0;
	var removeHotelFromIndexCalled = 0;
	var _addHotelToIndex, _reCreateHotelGroupIndexes, _removeHotelFromIndex;

	//modify functions
	lab.before(function(done){
		testingTools.enableEvents();
		_addHotelToIndex = services.addHotelToIndex;
		_reCreateHotelGroupIndexes = services.reCreateHotelGroupIndexes;
		_removeHotelFromIndex = services.removeHotelFromIndex;

		services.addHotelToIndex = function(data, callback){
			if(addHotelToIndexCalled){
				return callback(new Error('Some error'));
			}
			addHotelToIndexCalled++;
			callback(null, data);
		};

		services.reCreateHotelGroupIndexes = function(data, callback){
			if(reCreateHotelGroupIndexesCalled){
				return callback(new Error('Some error'));
			}
			reCreateHotelGroupIndexesCalled++;
			callback(null, data);
		};

		services.removeHotelFromIndex = function(hotelGroupId, hotelId, callback){
			if(removeHotelFromIndexCalled){
				return callback(new Error('Some error'));
			}
			removeHotelFromIndexCalled++;
			callback(null, [hotelGroupId, hotelId]);
		};

		testingTools.code.expect(services.addHotelToIndex).not.to.equal(_addHotelToIndex);
		testingTools.code.expect(services.reCreateHotelGroupIndexes).not.to.equal(_reCreateHotelGroupIndexes);
		testingTools.code.expect(services.removeHotelFromIndex).not.to.equal(_removeHotelFromIndex);

		eventUtils.emitter.removeAllListeners('hotel.unpublished');
		eventUtils.emitter.removeAllListeners('hotel.published');
		eventUtils.emitter.removeAllListeners('hotelGroup.created');
		testingTools.requireUncached('../modules/search/listener');
		done();
	});

	//set functions back to originals
	lab.after(function(done){
		testingTools.disableEvents();
		services.addHotelToIndex = _addHotelToIndex;
		services.reCreateHotelGroupIndexes = _reCreateHotelGroupIndexes;
		services.removeHotelFromIndex = _removeHotelFromIndex;
		done();
	});

	lab.test('should call services.addHotelToIndex on hotel.created', {timeout: 5000}, function(done){
		eventUtils.emitter.parallel('hotel.published', {}, function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(addHotelToIndexCalled).to.equal(1);
			done();
		});
	});

	lab.test('should call services.addHotelToIndex on hotel.created and get an error', function(done){
		eventUtils.emitter.parallel('hotel.published', {}, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});

	//
	lab.test('should call services.removeHotelFromIndex on hotel.unpublished', function(done){
		eventUtils.emitter.parallel('hotel.unpublished', 1, 2, function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(removeHotelFromIndexCalled).to.equal(1);
			done();
		});
	});

	lab.test('should call services.removeHotelFromIndex on hotel.unpublished and get an error', function(done){
		eventUtils.emitter.parallel('hotel.unpublished', 1, 2, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});

	//
	lab.test('should call services.reCreateHotelGroupIndexes on hotelGroup.created', function(done){
		eventUtils.emitter.parallel('hotelGroup.created', 'asdasd', function(err) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(reCreateHotelGroupIndexesCalled).to.equal(1);
			done();
		});
	});

	lab.test('should call services.reCreateHotelGroupIndexes on hotelGroup.created and get an error', function(done){
		eventUtils.emitter.parallel('hotelGroup.created', 'adsasd', function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});
	

});
