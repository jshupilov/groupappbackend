/**
 * Created by jevgenishupilov on 20/01/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var hotelServices = require('../../hotels/services');
var hgServices = require('../../hotel_groups/services');
var algoliaSearch = require('algolia-search');

lab.experiment('search.services.init', function() {
	lab.test('should work', function(done) {
		testingTools.code.expect(services.init).not.to.throw();
		done();
	});
});


lab.experiment('Search services.addHotelToIndex', function() {
	var hotel = null;
	lab.before(function(done) {
		hotelServices.getHotelRaw(
			testingTools.getHotelGroupId(),
			'stpeter',
			function(err, h) {
				testingTools.code.expect(err).to.be.null();
				hotel = h;
				done();
			}
		);
	});

	lab.beforeEach(function(done) {
		services.init(30000, false);
		done();
	});

	var f, f1, f2, called, called2;
	lab.before(function(done) {
		called = called2 = 0;
		f = algoliaSearch.prototype.Index.prototype.saveObjects;
		f1 = algoliaSearch.prototype.Index.prototype.waitTask;
		f2 = hgServices.getCategories;
		algoliaSearch.prototype.Index.prototype.saveObjects = function(obs, cb) {
			called++;
			cb(null, obs);
		};
		algoliaSearch.prototype.Index.prototype.waitTask = function(tid, cb) {
			called2++;
			cb(null, tid);
		};
		done();
	});

	lab.after(function(done) {
		algoliaSearch.prototype.Index.prototype.saveObjects = f;
		algoliaSearch.prototype.Index.prototype.waitTask = f1;
		hgServices.getCategories = f2;
		done();
	});

	lab.test('should add a new item to index', {timeout: 5000}, function(done) {
		hotel.data.hotelId += '_duplicate';
		hotel.data.localized[0].name = 'duplicate_st_peter';
		services.addHotelToIndex(hotel.data, function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(called).to.equal(1);
			testingTools.code.expect(called2).to.equal(1);
			done();
		}, true);
	});

	lab.test('should add a new item to index and not wait for result', function(done) {
		hotel.data.hotelId += '_1';
		hotel.data.localized[0].name = 'peter_smt_very_different';
		services.addHotelToIndex(hotel.data, function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(called).to.equal(2);
			testingTools.code.expect(called2).to.equal(1);
			done();
		});
	});
	lab.test('should given error if hotel group categories is empty array', function(done) {
		hgServices.getCategories = function(hgid, cb) {
			return cb(new Error('Error addHotelToIndex getCategories'));
		};
		services.addHotelToIndex(hotel.data, function(err) {
			testingTools.code.expect(err.message).to.equal('Error addHotelToIndex getCategories');
			done();
		});
	});
});

lab.experiment('Search services.addHotelToIndex 2', function() {

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});

	var hotel = null;
	lab.before(function(done) {
		hotelServices.getHotelRaw(
			testingTools.getHotelGroupId(),
			'stpeter',
			function(err, h) {
				testingTools.code.expect(err).to.be.null();
				hotel = h;
				done();
			}
		);
	});

	lab.beforeEach(function(done) {
		services.init(30000, false);
		done();
	});

	lab.test('should given network error', {timeout: 5000}, function(done) {
		hotel.data.hotelId += '_duplicate';
		hotel.data.localized[0].name = 'duplicate_st_peter';
		services.addHotelToIndex(hotel.data, function(err) {
			testingTools.expectError(err, 'sr-err-sav-ix');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('code');
			testingTools.code.expect(err.data.debuggingData.originalError.code).to.equal('ECONNRESET');
			done();
		}, true);
	});
});

lab.experiment('Search services.addHotelToIndex', function() {
	var hotel = null;

	lab.before(function(done) {
		hotelServices.getHotelRaw(
			testingTools.getHotelGroupId(),
			'stpeter',
			function(err, h) {
				testingTools.code.expect(err).to.be.null();
				hotel = h;
				done();
			}
		);
	});

	var f, called;
	lab.before(function(done) {
		called = 0;
		f = algoliaSearch.prototype.Index.prototype.saveObjects;
		algoliaSearch.prototype.Index.prototype.saveObjects = function(obs, cb) {
			called++;
			cb(new Error('some error'), {message: 'test'});
		};

		done();
	});

	lab.after(function(done) {
		algoliaSearch.prototype.Index.prototype.saveObjects = f;
		done();
	});

	lab.test('should give error if save fails', function(done) {
		services.addHotelToIndex(
			hotel.data,
			function(err) {
				testingTools.expectError(err, 'sr-err-sav-ix');
				testingTools.code.expect(called).to.equal(1);
				done();
			}
		);
	});

});

lab.experiment('Randomized result', function() {
	var hgid = testingTools.getHotelGroupId();
	var res1, searchConf;
	lab.before(function(done){
		searchConf = services.getSearchConfiguration;
		done();
	});
	lab.afterEach(function(done){
		services.getSearchConfiguration = searchConf;
		done();
	});
	lab.test('Should get randomized result', function(done) {
		services.searchHotels({keyword: 'hotel'}, hgid, function(err, res){
			testingTools.code.expect(err).to.be.null();
			res1 = res.hits;
			services.getSearchConfiguration = function(){
				return {resultOrdering: 'random'};
			};
			services.searchHotels({keyword: 'hotel'}, hgid, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.hits.length).to.be.equal(res1.length);
				testingTools.code.expect(res1).to.not.deep.equal(res.hits);
				done();
			});
		});
	});

	lab.test('Should NOT get randomized result if no keyword', function(done) {
		services.searchHotels({}, hgid, function(err, res){
			testingTools.code.expect(err).to.be.null();
			res1 = res.hits;
			services.searchHotels({}, hgid, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.hits.length).to.be.equal(res1.length);
				testingTools.code.expect(res1).to.deep.equal(res.hits);
				done();
			});
		});
	});

	lab.test('Double verify randomness', function(done){
		services.getSearchConfiguration = function(){
			return {resultOrdering: 'random'};
		};
		services.searchHotels({keyword: 'hotel'}, hgid, function(err, res){
			testingTools.code.expect(err).to.be.null();
			res1 = res.hits;
			services.searchHotels({keyword: 'hotel'}, hgid, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.hits.length).to.be.equal(res1.length);
				testingTools.code.expect(res1).to.not.deep.equal(res.hits);
				done();
			});
		});
	});

});




lab.experiment('Search services.searchHotels', function() {

	var f, called;
	lab.before(function(done) {
		called = null;
		services.init(30000, false);
		f = algoliaSearch.prototype.Index.prototype.search;
		algoliaSearch.prototype.Index.prototype.search = function(query, callback, args) {
			called = [query, args];
			callback(null, {hitsPerPage: 1, hits: [{}], nbHits: 1});
		};
		done();
	});

	lab.after(function(done) {
		algoliaSearch.prototype.Index.prototype.search = f;
		done();
	});

	lab.afterEach(function(done) {
		called = null;
		done();
	});

	lab.test('should work properly', function(done) {
		services.searchHotels(
			{
				keyword: 'hotel'
			},
			testingTools.getHotelGroupId(),
			function(err) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(called).to.be.array();
				testingTools.code.expect(called.length).to.equal(2);
				testingTools.code.expect(called[0]).to.equal('hotel');
				done();
			}
		);

	});

	lab.test('should work properly without search keyword', function(done) {

		services.searchHotels(
			{
				keyword: null
			},
			testingTools.getHotelGroupId(),
			function(err) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(called).to.be.array();
				testingTools.code.expect(called.length).to.equal(2);
				testingTools.code.expect(called[0]).to.equal('');
				done();
			}
		);

	});

	lab.test('should work properly with offset', function(done) {

		services.searchHotels(
			{
				keyword: null,
				collectionQuery: {
					offset: 2,
					limit: 2
				}
			},
			testingTools.getHotelGroupId(),
			function(err) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(called).to.be.array();
				testingTools.code.expect(called.length).to.equal(2);
				testingTools.code.expect(called[0]).to.equal('');
				testingTools.code.expect(called[1].hitsPerPage).to.equal(2);
				testingTools.code.expect(called[1].page).to.equal(1);
				done();
			}
		);

	});

	lab.test('should work properly with limit and without offset', function(done) {

		services.searchHotels(
			{
				keyword: null,
				categoryIds: [],
				collectionQuery: {
					offset: 0,
					limit: 2
				}
			},
			testingTools.getHotelGroupId(),
			function(err) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(called).to.be.array();
				testingTools.code.expect(called.length).to.equal(2);
				testingTools.code.expect(called[0]).to.equal('');
				testingTools.code.expect(called[1]).not.to.include(['page', 'hitsPerPage']);
				done();
			}
		);

	});

	lab.test('should work properly with categoryId', function(done) {

		services.searchHotels(
			{
				keyword: null,
				categoryIds: ['reg-eur-est']
			},
			testingTools.getHotelGroupId(),
			function(err) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(called).to.be.array();
				testingTools.code.expect(called.length).to.equal(2);
				testingTools.code.expect(called[0]).to.equal('');
				testingTools.code.expect(called[1].facetFilters).to.include(['categoryIds:reg-eur-est']);
				done();
			}
		);

	});

	lab.test('should work properly with only geolocation', function(done) {

		services.searchHotels(
			{
				keyword: null,
				geoLocation: {
					latitude: 50.941857,
					longitude: 6.956522,
					radius: 120
				}
			},
			testingTools.getHotelGroupId(),
			function(err) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(called).to.be.array();
				testingTools.code.expect(called.length).to.equal(2);
				testingTools.code.expect(called[0]).to.equal('');
				testingTools.code.expect(called[1].aroundLatLng).to.equal('50.941857,6.956522');
				testingTools.code.expect(called[1].aroundRadius).to.equal(120);
				done();
			}
		);

	});

	lab.test('should work properly with only geolocation and no radius', function(done) {

		services.searchHotels(
			{
				keyword: null,
				geoLocation: {
					latitude: 50.941857,
					longitude: 6.956522
				}
			},
			testingTools.getHotelGroupId(),
			function(err) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(called).to.be.array();
				testingTools.code.expect(called.length).to.equal(2);
				testingTools.code.expect(called[0]).to.equal('');
				testingTools.code.expect(called[1].aroundLatLng).to.equal('50.941857,6.956522');
				done();
			}
		);

	});

});

lab.experiment('Search services.searchHotels', function() {

	lab.before(function(done) {
		services.init(30000, true);
		done();
	});

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});

	lab.test('should given network error', {timeout: 10000}, function(done) {

		services.searchHotels(
			{
				keyword: {bla: 1},
				categoryIds: []
			},
			testingTools.getHotelGroupId(),
			function(err) {
				testingTools.expectError(err, 'sr-err-src');
				testingTools.code.expect(err.data.debuggingData.originalError).to.include('code');
				testingTools.code.expect(err.data.debuggingData.originalError.code).to.equal('ECONNRESET');
				done();
			}
		);
	});
});

lab.experiment('Search services.searchHotels', function() {

	lab.before(function(done) {
		services.init(30000, true);
		done();
	});

	testingTools.mockResponsesForExperiment(lab, function(req, res) {
		res.setHeader('content-type', 'application/json');
		res.statusCode = 503;
		res.end('{"error": "Callback is not given, this is random response", "message": "some msg"}', 'utf8');
	}, function() {
	});

	lab.test('should fail if index unreachable', {timeout: 10000}, function(done) {

		services.searchHotels(
			{
				keyword: {bla: 1},
				categoryIds: []
			},
			testingTools.getHotelGroupId(),
			function(err) {
				testingTools.expectError(err, 'sr-err-src');
				done();
			}
		);
	});
});

lab.experiment('Search services.cutSearchResults', function() {
	lab.test('should work properly', function(done) {
		var searchResult = {
			'hits': [
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				}
			],
			'page': 0,
			'nbHits': 1,
			'nbPages': 1,
			'hitsPerPage': 20,
			'processingTimeMS': 1,
			'query': 'iphone finger',
			'params': 'query=iphone+finger'
		};
		var len = searchResult.hits.length;
		services.cutSearchResults(3, 3, searchResult);
		testingTools.code.expect(searchResult.hits, len + '>' + searchResult.hits.length).to.have.length(3);

		searchResult = {
			'hits': [
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				}
			],
			'page': 0,
			'nbHits': 1,
			'nbPages': 1,
			'hitsPerPage': 20,
			'processingTimeMS': 1,
			'query': 'iphone finger',
			'params': 'query=iphone+finger'
		};

		services.cutSearchResults(0, 2, searchResult);
		testingTools.code.expect(searchResult.hits).to.have.length(2);

		searchResult = {
			'hits': [
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				},
				{
					'title': 'Apple iPhone 5S - 32GB'
				}
			],
			'page': 0,
			'nbHits': 1,
			'nbPages': 1,
			'hitsPerPage': 20,
			'processingTimeMS': 1,
			'query': 'iphone finger',
			'params': 'query=iphone+finger'
		};

		services.cutSearchResults(2, 3, searchResult);
		testingTools.code.expect(searchResult.hits).to.have.length(3);

		done();
	});
});

lab.experiment('search.services.deleteHotelGroupIndexes', function() {
	var f, f1, f2, called, called2;
	lab.before(function(done) {
		called = called2 = 0;
		f = algoliaSearch.prototype.deleteIndex;

		f1 = algoliaSearch.prototype.Index.prototype.waitTask;
		f2 = services.listHotelGroupIndexes;
		algoliaSearch.prototype.deleteIndex = function(name, cb) {
			called++;
			cb(null, {name: name, taskID: 1});
		};
		algoliaSearch.prototype.Index.prototype.waitTask = function(tid, cb) {
			called2++;
			cb(null, tid);
		};
		services.listHotelGroupIndexes = function(hotelGroupId, callback) {
			return callback(null, [{name: 'some_index-' + hotelGroupId}]);
		};
		done();
	});

	lab.after(function(done) {
		algoliaSearch.prototype.deleteIndex = f;
		algoliaSearch.prototype.Index.prototype.waitTask = f1;
		services.listHotelGroupIndexes = f2;
		done();
	});

	lab.test('should work', function(done) {
		services.deleteHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(called).to.equal(1);
			done();
		});
	});

	lab.test('should work and wait for results', function(done) {
		services.deleteHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(called).to.equal(2);
			testingTools.code.expect(called2).to.equal(1);
			done();
		}, true);
	});
});

lab.experiment('search.services.deleteHotelGroupIndexes', function() {
	var called;
	lab.before(function(done) {
		called = 0;
		done();
	});

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {

		mitm.on('connect', function(socket) {
			if(!called) {
				socket.bypass();
			}
			called++;
		});
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});

	lab.test('should give network error', {timeout: 10000}, function(done) {
		services.deleteHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.expectError(err, 'sr-err-del');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('code');
			testingTools.code.expect(err.data.debuggingData.originalError.code).to.equal('ECONNRESET');
			done();
		});
	});
});

lab.experiment('search.services.deleteHotelGroupIndexes', function() {

	var f, f1, called;
	lab.before(function(done) {
		called = 0;
		f = algoliaSearch.prototype.deleteIndex;
		f1 = services.listHotelGroupIndexes;
		algoliaSearch.prototype.deleteIndex = function(name, cb) {
			called++;
			cb(new Error('some error'), {message: 'test error'});
		};
		services.listHotelGroupIndexes = function(hotelGroupId, callback) {
			return callback(null, [{name: 'some_index-' + hotelGroupId}]);
		};
		done();
	});

	lab.after(function(done) {
		algoliaSearch.prototype.deleteIndex = f;
		services.listHotelGroupIndexes = f1;
		done();
	});

	lab.test('should give error if connection fails', function(done) {

		services.deleteHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.code.expect(called).to.equal(1);
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});

	});
});
lab.experiment('search.services.deleteHotelGroupIndexes', function() {

	var f, called;
	lab.before(function(done) {
		called = 0;
		f = services.listHotelGroupIndexes;
		services.listHotelGroupIndexes = function(hgId, cb) {
			called++;
			cb(new Error('some error'), {message: 'test error' + hgId});
		};
		done();
	});

	lab.after(function(done) {
		services.listHotelGroupIndexes = f;
		done();
	});

	lab.test('should give error if listing fails', function(done) {

		services.deleteHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.code.expect(called).to.equal(1);
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});

	});
});

lab.experiment('search.services.listHotelGroupIndexes', function() {

	var f, called;
	lab.before(function(done) {
		called = 0;
		f = algoliaSearch.prototype.listIndexes;
		algoliaSearch.prototype.listIndexes = function(cb) {
			if(called++) {
				return cb(new Error('some err'), {message: 'test error'});
			}
			cb(null, {items: [{name: 'bla'}, {name: process.env.ALGOLIASEARCH_PREFIX + testingTools.getHotelGroupId() + '_hotels'}]});
		};
		done();
	});

	lab.after(function(done) {
		algoliaSearch.prototype.listIndexes = f;
		done();
	});

	lab.test('should work', function(done) {
		services.listHotelGroupIndexes(testingTools.getHotelGroupId(), function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.array();
			testingTools.code.expect(res.length).to.equal(1);
			done();
		});

	});

	lab.test('should fail if listing fails', function(done) {
		services.listHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.expectError(err, 'sr-err-lis-ix');
			done();
		});
	});
});

lab.experiment('search.services.listHotelGroupIndexes', function() {

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});

	lab.test('should given network error', function(done) {
		services.listHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.expectError(err, 'sr-err-lis-ix');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('code');
			testingTools.code.expect(err.data.debuggingData.originalError.code).to.equal('ECONNRESET');
			done();
		});

	});
});

lab.experiment('Search services.removeHotelFromIndex', function() {
	var f, f2, called, called2;
	lab.before(function(done) {
		called = called2 = 0;
		f = algoliaSearch.prototype.Index.prototype.deleteObject;
		f2 = algoliaSearch.prototype.Index.prototype.waitTask;
		algoliaSearch.prototype.Index.prototype.deleteObject = function(id, cb) {
			called++;
			if(!id) {
				return cb(new Error('No Id'), {message: 'No Id!'});
			}
			cb(null, id);
		};
		algoliaSearch.prototype.Index.prototype.waitTask = function(tid, cb) {
			called2++;
			cb(null, tid);
		};
		done();
	});

	lab.after(function(done) {
		algoliaSearch.prototype.Index.prototype.deleteObject = f;
		algoliaSearch.prototype.Index.prototype.waitTask = f2;
		done();
	});

	lab.test('should remove item from index and not wait for actual deletion', function(done) {
		services.removeHotelFromIndex(testingTools.getHotelGroupId(), 'some-hotel', function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res.done).to.be.false();
			testingTools.code.expect(called).to.equal(1);
			testingTools.code.expect(called2).to.equal(0);
			done();
		});
	});

	lab.test('should remove item from index and wait for completion', function(done) {
		services.removeHotelFromIndex(testingTools.getHotelGroupId(), 'some-hotel', function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res.done).to.be.true();
			testingTools.code.expect(called).to.equal(2);
			testingTools.code.expect(called2).to.equal(1);
			done();
		}, true);
	});

	lab.test('should fail if no id given', function(done) {
		services.removeHotelFromIndex(testingTools.getHotelGroupId(), null, function(err) {
			testingTools.expectError(err, 'sr-itm-err-del');
			testingTools.code.expect(called).to.equal(3);
			testingTools.code.expect(called2).to.equal(1);
			done();
		});
	});

});

lab.experiment('Search services.removeHotelFromIndex', function() {

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});

	lab.test('should give network error', function(done) {
		services.removeHotelFromIndex(testingTools.getHotelGroupId(), 'some-hotel', function(err) {
			testingTools.expectError(err, 'sr-itm-err-del');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('code');
			testingTools.code.expect(err.data.debuggingData.originalError.code).to.equal('ECONNRESET');
			done();
		});
	});

});

lab.experiment('Search services.reCreateHotelGroupIndexes 1', function() {

	var _deleteHotelGroupIndexes, _createHotelGroupIndexes, c1, c2;
	lab.before(function(done) {
		_deleteHotelGroupIndexes = services.deleteHotelGroupIndexes;
		_createHotelGroupIndexes = services.createHotelGroupIndexes;
		c1 = c2 = 0;
		services.deleteHotelGroupIndexes = function(hgId, cb) {
			c1++;
			cb(null);
		};
		services.createHotelGroupIndexes = function(hgId, cb) {
			c2++;
			cb(null);
		};

		done();
	});

	lab.after(function(done) {
		services.deleteHotelGroupIndexes = _deleteHotelGroupIndexes;
		services.createHotelGroupIndexes = _createHotelGroupIndexes;
		done();
	});

	lab.test('should recreate hotel group index', function(done) {
		services.reCreateHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(c1).to.equal(1);
			testingTools.code.expect(c2).to.equal(1);
			done();
		});
	});

});

lab.experiment('Search services.reCreateHotelGroupIndexes 2', function() {

	var _deleteHotelGroupIndexes, c1;
	lab.before(function(done) {
		_deleteHotelGroupIndexes = services.deleteHotelGroupIndexes;
		c1 = 0;
		services.deleteHotelGroupIndexes = function(hgId, cb) {
			c1++;
			cb(new Error('err del'));
		};

		done();
	});

	lab.after(function(done) {
		services.deleteHotelGroupIndexes = _deleteHotelGroupIndexes;
		done();
	});

	lab.test('should give error if delete fails', function(done) {
		services.reCreateHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('err del');
			testingTools.code.expect(c1).to.equal(1);
			done();
		});
	});
});

lab.experiment('Search services.reCreateHotelGroupIndexes 3', function() {
	var _deleteHotelGroupIndexes, _createHotelGroupIndexes, c1, c2;
	lab.before(function(done) {
		_deleteHotelGroupIndexes = services.deleteHotelGroupIndexes;
		_createHotelGroupIndexes = services.createHotelGroupIndexes;
		c1 = c2 = 0;
		services.deleteHotelGroupIndexes = function(hgId, cb) {
			c1++;
			cb(null);
		};

		services.createHotelGroupIndexes = function(hgId, cb) {
			c2++;
			cb(new Error('err cr'));
		};

		done();
	});

	lab.after(function(done) {
		services.deleteHotelGroupIndexes = _deleteHotelGroupIndexes;
		services.createHotelGroupIndexes = _createHotelGroupIndexes;
		done();
	});

	lab.test('should give error if creation fails', function(done) {
		services.reCreateHotelGroupIndexes(null, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('err cr');
			testingTools.code.expect(c2).to.equal(1);
			done();
		}, function() {
			testingTools.code.expect(arguments.length).to.equal(0);
		});
	});
});

lab.experiment('Search services.createHotelGroupIndexes 4', function() {
	var f, called;
	lab.before(function(done) {
		called = 0;
		f = algoliaSearch.prototype.Index.prototype.setSettings;
		algoliaSearch.prototype.Index.prototype.setSettings = function(sets, cb) {
			if(called++) {
				return cb(new Error('some err'), {message: 'test error', sets: sets});
			}
			cb(null);
		};
		done();
	});

	lab.after(function(done) {
		algoliaSearch.prototype.Index.prototype.setSettings = f;
		done();
	});

	lab.test('should work', function(done) {
		services.createHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(called).to.equal(1);
			done();
		});

	});

	lab.test('should fail', function(done) {
		services.createHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.expectError(err, 'sr-err-new');
			testingTools.code.expect(called).to.equal(2);
			done();
		});

	});
});

lab.experiment('Search services.createHotelGroupIndexes 5', function() {

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm) {
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});

	lab.test('should given networ error', function(done) {
		services.createHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.expectError(err, 'sr-err-new');
			testingTools.code.expect(err.data.debuggingData.originalError).to.include('code');
			testingTools.code.expect(err.data.debuggingData.originalError.code).to.equal('ECONNRESET');
			done();
		});

	});
});

lab.experiment('Search services.createHotelGroupIndexes 6', function() {
	var f, f1, called, called2;
	lab.before(function(done) {
		called = called2 = 0;
		f = algoliaSearch.prototype.Index.prototype.setSettings;
		f1 = algoliaSearch.prototype.Index.prototype.waitTask;
		algoliaSearch.prototype.Index.prototype.setSettings = function(obs, cb) {
			called++;
			cb(null, obs);
		};
		algoliaSearch.prototype.Index.prototype.waitTask = function(tid, cb) {
			called2++;
			cb(null, tid);
		};
		done();
	});

	lab.after(function(done) {
		algoliaSearch.prototype.Index.prototype.setSettings = f;
		algoliaSearch.prototype.Index.prototype.waitTask = f1;
		done();
	});

	lab.test('should work and wait for completion', function(done) {
		services.createHotelGroupIndexes(testingTools.getHotelGroupId(), function(err) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(called).to.equal(1);
			testingTools.code.expect(called2).to.equal(1);
			done();
		}, true);

	});

});
