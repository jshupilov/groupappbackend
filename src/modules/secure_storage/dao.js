'use strict';

var databaseUtils = require('../utils/databaseUtils');
var errors = require('../utils/errors');
var uuid = require('node-uuid');

errors.defineError('ss-i-nf', 'System error', 'Item was not found from SS', 'SecureStorage');

/**
 * Save item to secure storage DB table
 * @param data
 * @param callback
 */
exports.save = function(data, callback){

	data.id = uuid.v4();

	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query('INSERT INTO ss_data (data) VALUES($1)', [data], function (err) {
			done();
			callback(err, data.id);
		});
	});
};

/**
 * Read row from secure storage DB table
 * @param id
 * @param callback
 */
exports.read = function(id, callback){
	databaseUtils.getConnection(function (err, client, done) {
		if (err) {
			return callback(err);
		}
		client.query('SELECT * FROM ss_data WHERE (data -> \'id\') = $1', [ '"' + id + '"' ], function (err, result) {
			done();
			if(err){
				return callback(err);
			}

			if( !result.rowCount ){
				return callback(errors.newError('ss-i-nf', {givenId: id}));
			}

			return callback(null, result.rows[0].data);
		});
	});
};
