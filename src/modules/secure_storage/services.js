'use strict';
var cryptoUtils = require('../utils/cryptoUtils');
var uuid = require('node-uuid');
var os = require('os');
var https = require('https');
var errors = require('../utils/errors');
var dao = require('./dao');
var check = require('check-types');

errors.defineError('ss-err-hpost', 'System Error', 'Error posting the hash to baking endpoint', 'SecureStorage');
errors.defineError('ss-err-enc', 'System Error', 'Error encoding data', 'SecureStorage');
errors.defineError('ss-inv-inp', 'System Error', 'Only objects are allowed for encryption', 'SecureStorage');
errors.defineError('ss-inv-dec-inp', 'System Error', 'Only secure storage type of objects are allowed for dencryption', 'SecureStorage');


/**
 * Create a random hash for usage as the base hash
 * @returns {*}
 */
exports._createBaseHash = function(){
	return cryptoUtils.sha256( (Math.random() * 100000000) + '89usfdj9njsdf' + os.hostname() + uuid.v4() + uuid.v1() + '98ufds897sdfy8sdf90');
};

/**
 * Turn the input hash into an encryption key
 * @param originalHash The original input hash
 * @param callback
 */
exports._getCryptionKey = function(originalHash, callback){

	var options = {
		hostname: process.env.SKEY_HOST,
		port: process.env.SKEY_PORT,
		path: '/modify',
		method: 'POST',
		headers: {
			'Content-Length': originalHash.length,
			'Content-Type': 'text/plain'
		},
		auth: process.env.SKEY_AUTH,
		rejectUnauthorized: process.env.NODE_ENV === 'production'
	};
	var responseBody = '';
	var req = https.request(options, function(res) {
		res.setEncoding('utf8');
		res.on('data', function (chunk) {
			responseBody += chunk;
		});
		res.on('end', function(){
			var error;
			try{
				error = errors.newError('ss-err-hpost', {originalError: JSON.parse(responseBody)}, false);
			}catch(e){
				error = null;
			}
			callback(error, responseBody);

		});
	});

	req.on('error', function(e) {
		return callback(errors.newError('ss-err-hpost', {originalError: e}, true));
	});

	// write data to request body
	req.write(originalHash);
	req.end();

};

/**
 * Secure the given data and return and Secure Storage object
 * @param data
 * @param callback
 */
exports._encryptData = function(data, callback){
	//validate input
	if( !check.object(data) ){
		return callback(errors.newError('ss-inv-inp', {reason: 'Not and object'}));
	}

	try{
		var text = JSON.stringify(data);
		data = cryptoUtils.getRandomValue(text.length);
	}catch(e){
		return callback(errors.newError('ss-inv-inp', {reason: 'Does not stringify', originalError: e}));
	}

		var out = {
		ssId: 'SecureStorageContainer',
		date: new Date().toISOString(),
		key: exports._createBaseHash(),
		iv: cryptoUtils.createSymCryptIv(),
		data: '',
		checkSum: cryptoUtils.sha256(text)
	};


	exports._getCryptionKey(out.key, function(err, key){
		if(err){
			text = cryptoUtils.getRandomValue(text.length);
			return callback(err);
		}
		out.data = cryptoUtils.strongEncryption(key, out.iv, text);
		text = cryptoUtils.getRandomValue(text.length);
		key = cryptoUtils.getRandomValue(key.length);
		return callback(null, out);
	});

};

/**
 * Decrypt data
 * @param data {Object} And Secure storage container
 * @param callback
 * @returns {*}
 */
exports._decryptData = function(data, callback){
	//validate input
	if( !check.object(data) ){
		return callback(errors.newError('ss-inv-dec-inp', {reason: 'Not an object'}));
	}else if(
		data.ssId !== 'SecureStorageContainer'
		|| !data.key
		|| !data.iv
		|| !data.data
	){
		return callback(errors.newError('ss-inv-dec-inp', {reason: 'Not a secure storage container'}));
	}

	exports._getCryptionKey(data.key, function(err, key){
		if(err){
			return callback(err);
		}
		var out = JSON.parse(cryptoUtils.strongDecryption(key, data.iv, data.data));
		data = cryptoUtils.getRandomValue(JSON.stringify(data).length);
		key = cryptoUtils.getRandomValue(key.length);
		return callback(null, out);
	});

};

/**
 * Store data securely
 * @param data {Object} The data object to store
 * @param callback {Function} The callback function, with error as first argument and ID as second
 */
exports.storeData = function(data, callback){
	exports._encryptData(data, function(err, encryptedData){
		if(err){
			return callback(err);
		}
		dao.save(encryptedData, function(err, id){
			encryptedData = cryptoUtils.getRandomValue(JSON.stringify(encryptedData).length);
			return callback(err, id);
		});
	});
};

/**
 * Retrieve securely stored data by ID
 * @param id {String} The ID of the item, returned by storeData
 * @param callback {Function} The callback function, with error as first argument and the original data object as second
 */
exports.retrieveData = function(id, callback){

	dao.read(id, function(err, encryptedData){
		if(err){
			return callback(err);
		}
		exports._decryptData(encryptedData, function(err, data){
			encryptedData = cryptoUtils.getRandomValue(JSON.stringify(encryptedData).length);
			if(err){
				return callback(err);
			}
			callback(err, data);
		});
	});
};

/**
 * Checks if the given data is the same as the copy in secure storage
 * @param data {Object} The data object to compare with
 * @param id {String} The secure storage item ID
 * @param callback {Function} The callback function, with error as first argument and boolean second
 * (The boolean is false, if data is the same, true otherwise)
 */
exports.checkIfDataChanged = function(data, id, callback){

	//create checksum
	try{
		var checkSum = cryptoUtils.sha256(JSON.stringify(data));
	}catch(e){
		return callback(errors.newError('ss-inv-inp', {originalError: e}));
	}

	dao.read(id, function(err, encryptedData) {
		if(err) {
			return callback(err);
		}
		var equal = encryptedData.checkSum === checkSum;
		encryptedData = cryptoUtils.getRandomValue(JSON.stringify(encryptedData).length);
		callback(null, !equal);
	});
};
