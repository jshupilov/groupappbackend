'use strict';
var testingTools = require('../../../test/testingTools');
var databaseUtils = require('../../utils/databaseUtils');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var dao = require('../dao');

lab.experiment('SecureStorage.dao.save', function() {

	lab.after(function(done){
		databaseUtils.getConnection(function(err, client, doneCon){
			testingTools.code.expect(err).to.be.null();
			client.query(
				'DELETE FROM ss_data where (data->\'test\') = $1',
				['"bla"'],
				function(err){
					doneCon();
					testingTools.code.expect(err).to.be.null();
					done();
				}
			);
		});
	});

	lab.test('should save item to database table', function(done) {
		dao.save({test: 'bla'}, function(err, id){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(id).to.be.string();

			databaseUtils.getConnection(function(err, client, doneCon){
				testingTools.code.expect(err).to.be.null();
				client.query(
					'SELECT count(*) as nr FROM ss_data where (data->\'id\') = $1',
					['"' + id + '"'],
					function(err, res){
						doneCon();
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(res.rows[0]).to.be.object();
						testingTools.code.expect(res.rows[0].nr).to.equal('1');
						done();
					}
				);
			});

		});
	});

});

lab.experiment('SecureStorage.dao.save with no DB', function() {
	testingTools.killDbConnectionForExperiment(lab);

	lab.test('should return error, when doing a query', function(done) {
		dao.save({test: 'bla'}, function(err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});

	});
});

lab.experiment('SecureStorage.dao.save with corrupted DB', function() {
	testingTools.failDbQueryForExperiment(lab, 'ss_data');

	lab.test('should return error, when doing a query', function(done) {
		dao.save({test: 'bla'}, function(err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});

	});
});


lab.experiment('SecureStorage.dao.read', function() {
	var itemId;
	var originalItem = {test: 'bla'};

	lab.before(function(done){
		dao.save(originalItem, function(err, id){
			testingTools.code.expect(err).to.be.null();
			itemId = id;
			done();
		});
	});

	lab.after(function(done){
		databaseUtils.getConnection(function(err, client, doneCon){
			testingTools.code.expect(err).to.be.null();
			client.query(
				'DELETE FROM ss_data where (data->\'test\') = $1',
				['"bla"'],
				function(err){
					doneCon();
					testingTools.code.expect(err).to.be.null();
					done();
				}
			);
		});
	});

	lab.test('should save item to database table', function(done) {
		dao.read(itemId, function(err, item){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(item).to.be.object();
			testingTools.code.expect(item).to.deep.equal(originalItem);
			done();
		});
	});

	lab.test('should give error if not found', function(done) {
		dao.read(123123213, function(err){
			testingTools.expectError(err, 'ss-i-nf');
			done();
		});
	});

});

lab.experiment('SecureStorage.dao.read with no DB', function() {
	testingTools.killDbConnectionForExperiment(lab);

	lab.test('should return error, when doing a query', function(done) {
		dao.read(1, function(err) {
			testingTools.expectError(err, 'db-cc-db');
			done();
		});

	});
});

lab.experiment('SecureStorage.dao.read with corrupted DB', function() {
	testingTools.failDbQueryForExperiment(lab, 'ss_data');

	lab.test('should return error, when doing a query', function(done) {
		dao.read(1, function(err) {
			testingTools.expectError(err, 'db-query-fails');
			done();
		});

	});
});
