'use strict';
var testingTools = require('../../../test/testingTools');
var databaseUtils = require('../../utils/databaseUtils');

exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var dao = require('../dao');


lab.experiment('SecureStorage.services._createBaseHash', function() {
	lab.test('should create a 64 byte random hash', function(done) {
		var h1 = services._createBaseHash();
		var h2 = services._createBaseHash();
		var h3 = services._createBaseHash();
		testingTools.code.expect( Buffer.byteLength(h1, 'ascii')).to.equal(64);
		testingTools.code.expect( Buffer.byteLength(h2, 'ascii')).to.equal(64);
		testingTools.code.expect( Buffer.byteLength(h3, 'ascii')).to.equal(64);

		testingTools.code.expect(h1).not.to.equal(h2);
		testingTools.code.expect(h1).not.to.equal(h3);
		testingTools.code.expect(h2).not.to.equal(h3);
		done();
	});


});

lab.experiment('SecureStorage.Services._getCryptionKey', function() {

	lab.test('should get the resulting hash if all systems work fine', function(done) {
		services._getCryptionKey('someHash', function(err, finalHash){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(finalHash).to.be.string();
			testingTools.code.expect(finalHash).not.to.equal('someHash');
			done();
		});
	});

});

lab.experiment('SecureStorage.Services._getCryptionKey when network down', function() {

	testingTools.mockResponsesForExperiment(lab, null, function(mitm){
		mitm.on('connection', function(socket){
			socket.destroy();
		});
	});

	lab.test('should get get error', function(done) {
		services._getCryptionKey('someHash', function(err){
			testingTools.expectError(err, 'ss-err-hpost');
			done();
		});
	});


});

lab.experiment('SecureStorage.Services._encryptData', function() {

	lab.test('should work as expected', function(done) {
		var input = {
			bla: {
				what: 'is',
				that: '?'
			},
			thing: 'awesome'
		};

		services._encryptData(input, function(err, result){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(result).to.be.object();
			testingTools.code.expect(result).to.be.include(['ssId', 'date', 'key', 'iv', 'data', 'checkSum']);
			testingTools.code.expect(result.ssId).to.equal('SecureStorageContainer');
			testingTools.code.expect(result.date).to.be.string();
			testingTools.code.expect(result.key).to.be.string();
			testingTools.code.expect(result.iv).to.be.string();
			testingTools.code.expect(result.data).to.be.string();
			testingTools.code.expect(result.data).not.equal(JSON.stringify(input));
			testingTools.code.expect(result.data.length).to.equal(JSON.stringify(input).length*2);
			done();
		});
	});

	lab.test('should not accept non object input', function(done) {
		services._encryptData('blabla', function(err){
			testingTools.expectError(err, 'ss-inv-inp');
			testingTools.code.expect(err.data.debuggingData.reason).to.equal('Not and object');
			done();
		});
	});

	lab.test('should not accept object with recursion', function(done) {
		var input = {
			bla: {
				what: 'is',
				that: '?'
			},
			thing: 'awesome'
		};

		input.bla = input;
		services._encryptData(input, function(err){
			testingTools.expectError(err, 'ss-inv-inp');
			testingTools.code.expect(err.data.debuggingData.reason).to.equal('Does not stringify');

			done();
		});
	});

});

lab.experiment('SecureStorage.Services._encryptData with broken hashMods', function() {
	testingTools.mockResponsesForExperiment(lab);
	lab.test('should give error', function(done) {
		services._encryptData({bla: 1}, function(err){
			testingTools.expectError(err, 'ss-err-hpost');
			testingTools.code.expect(err.data.isRetriable).to.equal(false);
			done();
		});
	});
});

lab.experiment('SecureStorage.Services._encryptData with broken network', function() {
	testingTools.mockResponsesForExperiment(lab, null, function(mitm){
		mitm.on('connection', function(socket){
			socket.destroy();
		});
	});
	lab.test('should give error', function(done) {
		services._encryptData({bla: 1}, function(err){
			testingTools.expectError(err, 'ss-err-hpost');
			testingTools.code.expect(err.data.isRetriable).to.equal(true);
			done();
		});
	});
});

lab.experiment('SecureStorage.Services._decryptData', function() {

	lab.test('should work as expected', function(done) {
		var input = {
			bla: {
				what: 'is',
				that: '?'
			},
			thing: 'awesome'
		};

		services._encryptData(input, function(err, result){
			testingTools.code.expect(err).to.be.null();
			services._decryptData(result, function(err, decryptedData){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(decryptedData).to.be.object();
				testingTools.code.expect(decryptedData).to.deep.equal(input);
				done();
			});
		});
	});

	lab.test('should not accept non object input', function(done) {
		services._decryptData('blabla', function(err){
			testingTools.expectError(err, 'ss-inv-dec-inp');
			testingTools.code.expect(err.data.debuggingData.reason).to.equal('Not an object');
			done();
		});
	});

	lab.test('should not accept object without ssId', {timeout: 5000}, function(done) {
		services._decryptData({}, function(err){
			testingTools.expectError(err, 'ss-inv-dec-inp');
			testingTools.code.expect(err.data.debuggingData.reason).to.equal('Not a secure storage container');
			done();
		});
	});

	lab.test('should not accept object without key', {timeout: 5000}, function(done) {
		services._encryptData({bla: 'bla'}, function(err, result) {
			delete result.key;
			testingTools.code.expect(err).to.be.null();
			services._decryptData(result, function(err){
				testingTools.expectError(err, 'ss-inv-dec-inp');
				testingTools.code.expect(err.data.debuggingData.reason).to.equal('Not a secure storage container');
				done();
			});
		});
	});

	lab.test('should not accept object without iv', {timeout: 5000}, function(done) {
		services._encryptData({bla: 'bla'}, function(err, result) {
			delete result.iv;
			testingTools.code.expect(err).to.be.null();
			services._decryptData(result, function(err){
				testingTools.expectError(err, 'ss-inv-dec-inp');
				testingTools.code.expect(err.data.debuggingData.reason).to.equal('Not a secure storage container');
				done();
			});
		});
	});

	lab.test('should not accept object without data', {timeout: 5000}, function(done) {
		services._encryptData({bla: 'bla'}, function(err, result) {
			delete result.data;
			testingTools.code.expect(err).to.be.null();
			services._decryptData(result, function(err){
				testingTools.expectError(err, 'ss-inv-dec-inp');
				testingTools.code.expect(err.data.debuggingData.reason).to.equal('Not a secure storage container');
				done();
			});
		});
	});

});

lab.experiment('SecureStorage.Services._encryptData with broken hashMods', function() {
	testingTools.mockResponsesForExperiment(lab);
	lab.test('should give error', function(done) {
		services._decryptData(
			{
				ssId: 'SecureStorageContainer',
				date: '2015-08-13T11:31:04.344Z',
				key: '5d9da227234237550e69d48542031a3a74c14dbc6b317f4a3499950eba91c3f3',
				iv: 'bfa6baeeea34eb538181f200a668f180',
				data: '3cfd015c32643e8ae2b025868a90e08fa80b5178f7faa2fea5eaff1b9e4f78b4e60a6035d44a1b0f2fe23e5162189c72c98a'
			},
			function(err){
				testingTools.expectError(err, 'ss-err-hpost');
				testingTools.code.expect(err.data.isRetriable).to.equal(false);
				done();
		});
	});
});

lab.experiment('SecureStorage.Services', function() {
	var itemId = 'y8ads89yds89udas';
	var itemData = {};
	lab.after(function(done){
		databaseUtils.getConnection(function(err, client, doneCon){
			testingTools.code.expect(err).to.be.null();
			client.query(
				'DELETE FROM ss_data where (data->\'id\') = $1',
				['"' + itemId +'"'],
				function(err){
					doneCon();
					testingTools.code.expect(err).to.be.null();
					done();
				}
			);
		});
	});

	lab.test('.storeData should give error if not valid input for encryption', function(done) {
		services.storeData('blabla', function(err){
			testingTools.expectError(err, 'ss-inv-inp');
			done();
		});
	});


	lab.test('.storeData should save data and return the ID', function(done) {
		services.storeData({test: 'bla'}, function(err, id){
			itemId = id;
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(id).to.be.string();
			dao.read(id, function(err, dataInDb){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(dataInDb).to.be.object();
				testingTools.code.expect(dataInDb.id).to.equal(id);
				done();
			});
		});
	});

	lab.test('.retrieveData should give error if not in DB', function(done) {
		services.retrieveData('blabla', function(err){
			testingTools.expectError(err, 'ss-i-nf');
			done();
		});
	});

	lab.test('.retrieveData should save data and return the ID', function(done) {
		services.retrieveData(itemId, function(err, data){
			itemData = data;
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(data).to.be.object();
			testingTools.code.expect(data).to.deep.equal({test: 'bla'});
			done();
		});
	});

	lab.test('.checkIfDataChanged should return false, if same data', function(done) {
		services.checkIfDataChanged(itemData, itemId, function(err, isChanged){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(isChanged).to.equal(false);
			done();
		});
	});

	//test data is modified
	lab.test('.retrieveData should give error if invalid object in db', function(done) {

		databaseUtils.getConnection(function(err, client, doneCon){
			testingTools.code.expect(err).to.be.null();
			itemData.ssId = 'Test';
			itemData.id = itemId;
			client.query(
				'UPDATE ss_data SET data = $1 where (data->\'id\') = $2',
				[itemData, '"' + itemId +'"'],
				function(err){
					doneCon();
					testingTools.code.expect(err).to.be.null();
					services.retrieveData(itemId, function(err){
						testingTools.expectError(err, 'ss-inv-dec-inp');
						done();
					});
				}
			);
		});
	});

	lab.test('.checkIfDataChanged should return true, if modified data', function(done) {
		itemData.asdsada = 123;
		services.checkIfDataChanged(itemData, itemId, function(err, isChanged){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(isChanged).to.equal(true);
			done();
		});
	});

	lab.test('.checkIfDataChanged should give error if invalid input', function(done) {
		var smt = {bla: 'bla'};
		smt.second = smt;
		services.checkIfDataChanged(smt, itemId, function(err){
			testingTools.expectError(err, 'ss-inv-inp');
			done();
		});
	});

	lab.test('.checkIfDataChanged should give error if invalid ID', function(done) {
		services.checkIfDataChanged(itemData, 'ji0adsfji', function(err){
			testingTools.expectError(err, 'ss-i-nf');
			done();
		});
	});


});
