'use strict';
var app = require('../../app');
var commonServices = require('../common/services');
var hgServices = require('../hotel_groups/services');
var errors = require('./errors');
var auth = require('./authUtils');
var logging = require('./logging');
var url = require('url');
var check = require('check-types');
var etag = require('etag');

var validHttpMethods = ['OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'TRACE', 'CONNECT'];

errors.defineError('au-inv-hgid', 'Invalid Hotel Group!', 'Invalid HotelGroup Id provided', 'Utils.apiUtils', 400);
errors.defineError('au-inv-ver', 'System version mismatch!', 'Invalid API version given', 'Utils.apiUtils', 400);
errors.defineError('au-no-uuid', 'Cannot identify!', 'No UUID given', 'Utils.apiUtils', 400);
errors.defineError('au-inv-uuid', 'Cannot identify!', 'Invalid UUID given', 'Utils.apiUtils', 400);
errors.defineError('au-inv-path', 'System call error!', 'Invalid api call URL', 'Utils.apiUtils', 400);
errors.defineError('au-inv-hrefqp', 'System error!', 'Invalid queryParams provided, must be an object', 'Utils.apiUtils', 400);
errors.defineError('au-inv-linkmethod', 'System error!', 'Invalid method provided for link, must be on of GET/PUT/POST/DELETE', 'Utils.apiUtils', 500);
errors.defineError('au-inv-req-method', 'System error!', 'Invalid request method', 'Utils.apiUtils', 400);
errors.defineError('au-inv-loc-inp', 'Invalid input!', 'Invalid location search input', 'Utils.apiUtils', 400);
errors.defineError('au-inv-req-bd', 'Invalid payload!', 'Invalid payload', 'Utils.apiUtils', 400);
errors.defineError('au-inv-req-ct', 'Invalid request!', 'Invalid request content type', 'Utils.apiUtils', 400);
errors.defineError('au-https-req', 'Invalid request!', 'Use of https protocol is required', 'Utils.apiUtils', 400);
errors.defineError('au-ver-req', 'Invalid request!', 'Use of x-cardola-verify header is required', 'Utils.apiUtils', 400);
errors.defineError('au-ver-inv', 'Invalid request!', 'Invalid x-cardola-verify header value', 'Utils.apiUtils', 400);
errors.defineError('au-inv-cache', 'System error', 'Invalid cache type provided', 'Utils.apiUtils');
errors.defineError('au-err-etag', 'System error', 'Cannot apply etag, maybe empty reponse body?', 'Utils.apiUtils');

exports.validateHotelGroupId = function(hotelGroupId, callback) {

	if(!hotelGroupId) {
		callback(errors.newError('au-inv-hgid', {missingInfo: 'hotelGroupId'}));
	} else {

		hgServices.validateHotelGroupId(hotelGroupId, function(err, isValid) {
			if(err) {
				return callback(err);
			}
			if(!isValid){
				return callback(errors.newError('au-inv-hgid', {givenHotelGroupId: hotelGroupId}));
			}
			return callback(null, hotelGroupId);
		});

	}

};

exports.validateApiVersion = function(apiVersion, callback) {
	if(app.apiVersion !== apiVersion) {
		callback(errors.newError('au-inv-ver', {givenVersion: apiVersion}));
	} else {
		callback(null, app.apiVersion);
	}
};

/**
 * Validate the UUID in the request
 * @param {Object} apiRequest The request object
 * @param {Function} callback
 */
exports.validateUuid = function(apiRequest, callback) {
	if(apiRequest.urlObj.path.indexOf('registration') > -1) {
		callback(null, undefined);
	} else {
		if(apiRequest.headers['x-cardola-uuid'] === undefined
			|| !apiRequest.headers['x-cardola-uuid']
		) {
			callback(errors.newError('au-no-uuid'));
			//callback(null, apiRequest.headers['x-cardola-uuid']);
		} else {
			commonServices.validateUuid(
				apiRequest.headers['x-cardola-uuid'],
				apiRequest.hotelGroupId,
				function(err, isValid) {
					if(err) {
						return callback(err);
					}
					if(!isValid) {
						callback(errors.newError('au-inv-uuid', {givenUuid: apiRequest.headers['x-cardola-uuid']}));
					} else {
						callback(null, apiRequest.headers['x-cardola-uuid']);
					}
				}
			);

		}
	}
};

/**
 * Get current request URL
 * @returns {internals.Connection.info.uri|*}
 */
exports.getCurrentHrefFromHapiRequest = function(hapiRequest) {
	return exports.getBaseUrl() + hapiRequest.raw.req.url;
};

/**
 * Validates the API call
 * @param {Object} apiRequest The request object
 * @param {Function} callback The callback function, accepting error as first and final path as second parameter
 * @returns {*}
 */
exports.validateApiCall = function(apiRequest, callback) {
	var pathArr = apiRequest.urlObj.path.split('/').slice(1);
	if(pathArr.length < 3) {
		return callback(errors.wrapToBoom(errors.newError('au-inv-path', {givenPath: apiRequest.urlObj.path}), 400));
	}

	//preflight
	if(['OPTIONS'].indexOf(apiRequest.method) > -1) {
		return callback(null);
	}

	//validate api authentication
	//validate api version
	exports.validateApiVersion(pathArr[0], function(err, apiVersion) {
		if(err) {
			callback(errors.wrapToBoom(err, 400));
		} else {

			exports.validateHotelGroupId(pathArr[1], function(err, hgId) {
				if(err) {
					callback(errors.wrapToBoom(err, 400));
				} else {
					exports.populateApiRequest(apiRequest, apiVersion, hgId);
					callback(null);
				}
			});
		}
	});

};

/**
 * Populate ApiRequest object with some general values
 * @param apiRequest
 * @param apiVersion
 * @param hotelGroupId
 * @param [uuid]
 */
exports.populateApiRequest = function(apiRequest, apiVersion, hotelGroupId, uuid) {
	apiRequest.apiVersion = apiVersion;
	apiRequest.hotelGroupId = hotelGroupId;
	apiRequest.uuid = uuid;
	if(apiRequest.uuid && !apiRequest.headers['x-cardola-uuid']){
		apiRequest.headers['x-cardola-uuid'] = apiRequest.uuid;
	}
};

/**
 * Populate ApiRequest with some general values from existing ApiRequest object
 * @param apiRequest
 * @param existingApiRequest
 */
exports.populateApiRequestFromExisting = function(apiRequest, existingApiRequest) {
	exports.populateApiRequest(apiRequest, existingApiRequest.apiVersion, existingApiRequest.hotelGroupId, existingApiRequest.uuid);
};

/**
 * Get href for API Service
 * @param {String} hotelGroupId Hotel group Id
 * @param {Array|String} languages Array of languages or String with one language
 * @param {String} servicePath Path to the service, for example 'user/login'
 * @param {Object} [queryParams] Object containing query parameters, which will be added to the URL (?x=y&...)
 * @returns String href
 * @throws Error if error occurs
 */
exports.getServiceHref = function(hotelGroupId, servicePath, queryParams) {
	var urlObj = {
		pathname: '',
		query: queryParams
	};

	var base = app.apiVersion + '/' + hotelGroupId;
	//remove base from servicePath if it is there
	if(servicePath.indexOf(base) > -1) {
		servicePath = servicePath.slice(servicePath.indexOf(base) + base.length);
	}

	urlObj.pathname += '/' + base;

	if(servicePath.indexOf('/') === 0) {
		servicePath = servicePath.substr(1);
	}
	urlObj.pathname += '/' + servicePath;

	var urlOut = exports.createUrl(urlObj);
	//replace tokens, if they were url-encoded
	for(var k in queryParams) {
		//if it is a token
		if(check.unemptyString(queryParams[k]) && queryParams[k].length > 2 && queryParams[k].indexOf('<') === 0 && queryParams[k].indexOf('>') === queryParams[k].length - 1) {
			var tokenName = queryParams[k].substr(1);
			tokenName = tokenName.substr(0, tokenName.length - 1);
			urlOut = urlOut.replace('%3C' + tokenName + '%3E', queryParams[k]);
		}
	}
	return urlOut;

};

/**
 * GEt service href with apiRequest object available
 * @param {Object} apiRequest The apiRequest object
 * @param {String} servicePath Path to the service, for example 'user/login'
 * @param {Object} [queryParams] Object containing query parameters, which will be added to the URL (?x=y&...)
 * @returns String href
 * @throws Error if error occurs
 */
exports.getServiceHrefWithRequest = function(apiRequest, servicePath, queryParams) {

	return exports.getServiceHref(apiRequest.hotelGroupId, servicePath, queryParams);

};

/**
 * Get service Link object
 * @param {String} hotelGroupId Hotel group Id
 * @param {Array|String} languages Array of languages or String with one language
 * @param {String} servicePath Path to the service, for example 'user/login'
 * @param {Object} [queryParams] Object containing query parameters, which will be added to the URL (?x=y&...)
 * @param {String=GET} [method] Define which method (GET/PUT/POST/DELETE) should be used to access this link
 * @throws Error if error occurs
 * @returns {{href: String}}

 */
exports.getServiceLink = function(hotelGroupId, servicePath, queryParams, method) {

	return exports.createLink(exports.getServiceHref(hotelGroupId, servicePath, queryParams), method);

};

/**
 * Create link Object
 * @param href
 * @param [method]
 * @returns {{href: *}}
 */
exports.createLink = function(href, method) {
	var link = {
		href: href
	};
	if(method) {
		if(['GET', 'POST', 'PUT', 'DELETE'].indexOf(method) < 0) {
			throw errors.newError('au-inv-linkmethod', {givenMethod: method});
		} else {
			link.method = method;
		}
	}
	return link;
};

/**
 * Get Service Link object with apiRequest object
 * @param {Object} apiRequest The apiRequest object
 * @param {String} servicePath Path to the service, for example 'user/login'
 * @param {Object} [queryParams] Object containing query parameters, which will be added to the URL (?x=y&...)
 * @param {String=GET} [method] Define which method (GET/PUT/POST/DELETE) should be used to access this link
 * @returns {{href: String}}
 */
exports.getServiceLinkWithRequest = function(apiRequest, servicePath, queryParams, method) {

	return exports.getServiceLink(apiRequest.hotelGroupId, servicePath, queryParams, method);

};

/**
 * Get current href, with modifications
 * @param apiRequest
 * @param queryParams
 * @returns {String}
 */
exports.getModifiedCurrentHref = function(apiRequest, queryParams) {
	var query = {};
	for(var i in apiRequest.urlObj.query) {
		query[i] = apiRequest.urlObj.query[i];
	}
	for(var b in queryParams) {
		query[b] = queryParams[b];
	}
	return exports.getServiceHref(apiRequest.hotelGroupId, apiRequest.urlObj.pathname, query);

};

/**
 * Get current href, with modifications as a Link object
 * @param apiRequest
 * @param queryParams
 * @returns {{href: *}}
 */
exports.getModifiedCurrentLink = function(apiRequest, queryParams) {
	return exports.createLink(exports.getModifiedCurrentHref(apiRequest, queryParams));
};

/**
 * Extract URL into logical parts in Object
 * @param urlString
 * @see http://nodejs.org/docs/latest/api/url.html#url_url_parse
 * @returns {Object} Url object
 */
exports.getUrlObj = function(urlString) {
	return url.parse(urlString, true, true);
};

/**
 * Create URL from url object
 * @param urlObj
 * @see http://nodejs.org/docs/latest/api/url.html#url_url_format_urlobj
 * @returns {String}
 */
exports.createUrl = function(urlObj) {
	if(!urlObj.protocol) {
		urlObj.protocol = process.env.PUBLIC_PROTOCOL;
	}
	if(!urlObj.host) {
		urlObj.host = process.env.PUBLIC_HOST;
	}
	if(process.env.PUBLIC_PORT !== '80' && process.env.PUBLIC_PORT !== '443') {
		urlObj.host += ':' + process.env.PUBLIC_PORT;
	}
	return url.format(urlObj);
};

/**
 * Get base url
 * @returns {string}
 */
exports.getBaseUrl = function() {
	var baseUrl = process.env.PUBLIC_PROTOCOL + '://' + process.env.PUBLIC_HOST;

	if(process.env.PUBLIC_PORT !== '80' && process.env.PUBLIC_PORT !== '443') {
		baseUrl += ':' + process.env.PUBLIC_PORT;
	}
	return baseUrl;
};

/**
 * Create apiRequest object
 * @param href
 * @param headers
 * @param params
 * @param method {String} get|post|put|delete
 * @param body {String|Object} Body as JSON
 * @param [info] Info of request
 * @returns {{href: *, urlObj: null, headers: (request.headers|*)}}
 */
exports.createApiRequest = function(href, headers, params, method, body, info) {

	method = method ? method.toUpperCase() : 'GET';
	if( validHttpMethods.indexOf(method) < 0 ){
		throw errors.newError('au-inv-req-method', {givenMethod: method});
	}

	if(body){
		if( typeof body !== 'object' ){
			if( !headers['content-type'] || headers['content-type'].indexOf('application/json') < 0 ){
				throw errors.newError('au-inv-req-ct', {message: 'Only Content-Type: application/json is supported!'})
			}
			try{
				body = JSON.parse(body);
			}catch (e){
				throw errors.newError('au-inv-req-bd', {parsingPayloadError: e});
			}
		}
	}
	var remoteAddress = info ? info.remoteAddress : null;
	if(headers['x-forwarded-for']){
		remoteAddress = headers['x-forwarded-for'].split(',')[0].trim();
	}
	if(headers['lswcdn-forwarded-for']){
		remoteAddress = headers['lswcdn-forwarded-for'].split(',')[0].trim();
	}
	var apiRequest = {
		href: href,
		query: {},
		urlObj: null,
		headers: headers,
		params: params,
		method: method,
		body: body,
		geoLocation: null,
		remoteAddress: remoteAddress
	};
	apiRequest.urlObj = exports.getUrlObj(apiRequest.href);
	apiRequest.query = apiRequest.urlObj.query;
	apiRequest.collectionQuery = {
		isDefaultOffset: true,
		isDefaultLimit: true,
		offset: 0,
		limit: 1000
	};
	if(apiRequest.query.hasOwnProperty('offset') && !isNaN(apiRequest.query.offset) && apiRequest.query.offset > 0) {
		apiRequest.collectionQuery.offset = parseInt(apiRequest.query.offset);
		apiRequest.collectionQuery.isDefaultOffset = false;
	}

	if(apiRequest.query.hasOwnProperty('limit') && !isNaN(apiRequest.query.limit) && apiRequest.query.limit > 0) {
		apiRequest.collectionQuery.limit = parseInt(apiRequest.query.limit);
		apiRequest.collectionQuery.isDefaultLimit = false;
	}


	if(apiRequest.query.lat || apiRequest.query.long) {
		if(apiRequest.query.lat && apiRequest.query.long) {
			var lat = parseFloat(apiRequest.query.lat);
			if(isNaN(lat) || lat < -180 || lat > 180) {
				throw errors.newError('au-inv-loc-inp', {invalidParam: 'lat', parsedValue: lat});
			}

			var long = parseFloat(apiRequest.query.long);
			if(isNaN(long) || long < -180 || long > 180) {
				throw errors.newError('au-inv-loc-inp', {invalidParam: 'long', parsedValue: long});
			}

			apiRequest.geoLocation = {
				latitude: lat,
				longitude: long
			};

			//if radius given then validate and add as search filter
			if(apiRequest.query.radius){
				var radius = parseInt(apiRequest.query.radius);
				if(isNaN(radius) || radius <= 0) {
					throw errors.newError('au-inv-loc-inp', {invalidParam: 'radius', parsedValue: radius});
				}
				apiRequest.geoLocation.radius = radius;
			}

		}else{
			throw errors.newError('au-inv-loc-inp', {missingSomeParams: [ 'lat', 'long']});
		}
	}

	return apiRequest;
};

/**
 * Create an ApiRequest object, with basic values from existing ApiRequest object
 * @param href
 * @param headers
 * @param existingApiRequest
 * @returns {{href: *, urlObj: null, headers: (request.headers|*)}}
 */
exports.createApiRequestFromExisting = function(href, headers, existingApiRequest) {
	var apiRequest = exports.createApiRequest(href, headers, existingApiRequest.params);
	exports.populateApiRequestFromExisting(apiRequest, existingApiRequest);
	return apiRequest;
};

exports.requestSecurity = {};
exports.requestSecurity.delay = function(apiRequest, callback){
    var rand = Math.floor(Math.random()*500)+500;
	setTimeout(function(){
		callback(null);
	}, rand);
};

exports.requestSecurity.uuidValidation = function(apiRequest, callback){
	exports.validateUuid(apiRequest, function(err, uuid) {
		if(err) {
			callback(errors.wrapToBoom(err, 400));
		} else {
			apiRequest.uuid = uuid;
			callback(null);
		}
	});
};

exports.requestSecurity.tlsValidation = function(apiRequest, callback){
	if(apiRequest.urlObj.protocol !== 'https:'){
		return callback(errors.newError('au-https-req'));
	}else{
		return callback(null);
	}
};

exports.requestSecurity.checksumValidation = function (apiRequest, callback){
	if( !apiRequest.headers['x-cardola-verify'] ){
		return callback(errors.newError('au-ver-req'));
	}

	//if test checksum is allowed and is given
	if(
		process.env.TEST_CHECKSUM
		&& process.env.TEST_CHECKSUM === apiRequest.headers['x-cardola-verify']
	){
		logging.warning('Test checksum given');
		return callback(null, process.env.TEST_CHECKSUM);
	}

	auth.validateAuthentication(apiRequest, function(err){
		if(err){
			return callback(err);
		}
		return callback(null, process.env.TEST_CHECKSUM);
	});
};

/**
 * Ensure security
 * @param level - The predefined security level required
 * @param apiRequest
 * @param callback
 */
exports.ensureSecurity = function(level, apiRequest, callback){

	var levels = {
		1: [exports.requestSecurity.uuidValidation],
		2: [exports.requestSecurity.uuidValidation, exports.requestSecurity.tlsValidation],
		3: [exports.requestSecurity.uuidValidation, exports.requestSecurity.tlsValidation, exports.requestSecurity.checksumValidation],
		4: [exports.requestSecurity.uuidValidation, exports.requestSecurity.tlsValidation, exports.requestSecurity.checksumValidation, exports.requestSecurity.delay],
		htmlFormSecure: [exports.requestSecurity.tlsValidation, exports.requestSecurity.delay]
	};

	if(levels.hasOwnProperty(level)){

		var checkIndex = 0;
		var secCaller = function(){
			//call first security check
			levels[level][checkIndex](apiRequest, function(err, res){
				if(err){
					callback(err);
				//call next security check
				}else if( levels[level][checkIndex+1] !== undefined ){
					checkIndex++;
					secCaller();
				}else{
					callback(null, res);
				}
			});
		};

		secCaller();

	}else{
		callback(null);
	}
};

/**
 * populate menu API structure
 * @param action
 * @param condition
 * @param keys
 * @param vals
 * @param instance
 * @param removeIfDisabled
 * @param name
 * @returns {{action: String, isEnabled: Boolean, name: String, data: {}}}
 */
exports.populateMenu = function (obj, condition, keys, vals, menu, name){
	var item = {
		action: obj.action,
		isEnabled: true,
		name: name,
		data: {}
	};
	if(condition){
		for (var i in keys) {
			var key = keys[i];
			item.data[key] = vals[i];
			if(obj.alwaysDisabled){
				item.isEnabled = false;
			}
		}
		menu.push(item);
	} else {
		if(!obj.removeIfDisabled){
			item.isEnabled = false;
			menu.push(item);
		}
	}
	return item;
};

exports.getCacheConfigurations = function(){
	/**
	 * PS!
	 * The values set here are validated by unit tests separately,
	 * but YOU must be 100% percent sure that the combination of values is correct!
	 */

	return {
		'default': {
			'Cache-Control': {
				flags: ['public', 'must-revalidate'], //everyone can CACHE, but verify that cache is correct each time
				'max-age': 0, //no cache on client side
				's-maxage': 86400 //one day cache for CDN

			}
		},
		'noCache': {
			'Cache-Control': {
				flags: ['public', 'no-store'], //no caching on any layers
				's-maxage': 0,
				'max-age': 0
			}
		}
	};
};

/**
 * Add caching headers to HapiJS reply() interface
 * @param replyInterface
 * @param [cachingType]
 * @returns {*}
 */
exports.addCachingHeaders = function(replyInterface, cachingType){
	cachingType = cachingType ? cachingType : 'default';
	//do add caching headers for error responses
	if(replyInterface.isBoom){
		return replyInterface;
	}

	var cachingConfiguration = exports.getCacheConfigurations()[cachingType];
	if( !cachingConfiguration ){
		throw errors.newError('au-inv-cache', {givenTYpe: cachingType});
	}
	for(var headerName in cachingConfiguration){
		var headerValArray = [];
		for(var headerValueKey in cachingConfiguration[headerName]){
			if(headerValueKey === 'flags'){
				headerValArray = headerValArray.concat(headerValArray, cachingConfiguration[headerName][headerValueKey]);
			}else{
				headerValArray.push(headerValueKey + '=' + cachingConfiguration[headerName][headerValueKey]);
			}
		}
		replyInterface.header(headerName, headerValArray.join(', '));
	}
	try{
		var checksum = etag(JSON.stringify(replyInterface.source)).replace(/"/g, '');
		replyInterface.etag(checksum);
	}catch(e){
		throw errors.newError('au-err-etag', {originalError: e.message, res: replyInterface});
	}


	return replyInterface;
};
