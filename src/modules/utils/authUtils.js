'use strict';
var cryptoUtils = require('./cryptoUtils');
var uuid = require('node-uuid');
var errors = require('./errors');
var logging = require('./logging');
var commonServices = require('../common/services');

errors.defineError('au-inv-verifykey', 'Invalid Verify Key!', 'Invalid Verify Key provided', 'Utils.authUtils', 400);
errors.defineError('au-len-verifykey', 'Invalid Verify Key!', 'Invalid Verify Key length. Length must be more then 10 symbols', 'Utils.authUtils', 400);

function sign(key1, key2) {
	return cryptoUtils.sha256([key1, key2].join(''));
}

function generateHashKey(apiKey, path, uuid, randomStr) {

	var h1 = sign(path, apiKey);
	var h2 = sign(h1, randomStr);
	var h3 = sign(h2, uuid);
	var hashKey = h3 + randomStr;
	return hashKey;
}

/**
 * Validate the authentication hash key
 * @param {Object} request The request object
 * @param callback
 * @returns Boolean if
 */
exports.validateAuthentication = function(request, callback) {
	var verifyKey = request.headers['x-cardola-verify'];
	if(verifyKey && verifyKey.length > 10) {
		var randomStr = verifyKey.substr(verifyKey.length - 10, 10);
		//var payload = request.body ? JSON.stringify(request.body) : '';
		//var cUuid = request.headers['x-cardola-uuid'] ? request.headers['x-cardola-uuid'] : '';
		var got = false;
		commonServices.getValidApiKeys(request.hotelGroupId, function(err, validApiKeys) {
			if(err){
				got = true;
				return callback(err);
			}
			if(validApiKeys.length < 1){
				return callback(errors.newError('au-inv-verifykey', {reason: 'No valid keys'}));
			}
			validApiKeys.forEach(function(apiKey) {
				if(got){
					return;
				}


				var hashKey = exports.getVerifyKey(request, apiKey, randomStr);

				if(verifyKey === hashKey) {
					got = true;
					callback(null, true);
				}else{
					logging.warning('invalid key given', apiKey);
				}
			});

			if(!got) {
				return callback(errors.newError('au-inv-verifykey', {reason: 'Invalid hash'}));
			}
		});

	}else{
		return callback(errors.newError('au-len-verifykey'));
	}

};

/*
 * This is just test function, what uses for HashKey generation
 */
exports.getVerifyKey = function(request, apiKey, salt) {

	var id = uuid.v4().replace(/-/g, '');
	salt = salt ? salt : id.substr( Math.round(Math.random()*(id.length - 10)), 10 );
	return generateHashKey(
		apiKey,
		request.href,
		request.headers['x-cardola-uuid'],
		salt
	);
};
