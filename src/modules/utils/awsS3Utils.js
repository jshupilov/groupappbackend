'use strict';
var errors = require('./errors');
var aws = require('aws-sdk');
var path = require('path');

errors.defineError('s3-list-err', 'System error', 'Error occurred listing AWS S3 files', 'AwsS3Utils');
errors.defineError('s3-upl-err', 'System error', 'Error occurred uploading file to AWS S3', 'AwsS3Utils');
errors.defineError('s3-get-err', 'System error', 'Error occurred downloading file from AWS S3', 'AwsS3Utils');
errors.defineError('s3-copy-err', 'System error', 'Error occurred copying file inside AWS S3', 'AwsS3Utils');
errors.defineError('s3-del-err', 'System error', 'Error occurred deleting file in AWS S3', 'AwsS3Utils');

/**
 * Create connection to amazon s3
 * @param bucket
 * @param callback
 * @returns {*}
 */
exports.createAwsS3Connection = function(bucket, callback) {
	var c = new aws.S3({params: {Bucket: bucket || process.env.AWS_S3_BUCKET}});
	if(callback){
		callback(null, c);
	}
	return c;
};

/**
 * List files of a folder in AWS S3
 * @param s3Connection {Object} Aws S3 Client instance
 * @param s3Prefix {String} Folder/Prefix where to look for files
 * @param callback
 */
exports.listAwsS3Files = function(s3Connection, s3Prefix, callback) {

	var out = {
		Contents: []
	};
	function getData(marker){
		s3Connection.listObjects({
			Prefix: path.join(process.env.AWS_S3_FOLDER, s3Prefix),
			Marker: marker
		}, function(err, result) {
			if(err) {
				return callback(errors.newError('s3-list-err', {originalError: err.stack}));
			}
			out.Contents = out.Contents.concat(result.Contents);
			if(result.IsTruncated){
				getData(result.Contents[result.Contents.length - 1].Key);
				result = null;
				return null;
			}
			result.Contents = out.Contents;
			out = null;
			callback(null, result);
		});
	}
	getData();

};

/**
 * Uploads file to AWS S3
 * @param s3Connection
 * @param s3Folder
 * @param fileName
 * @param fileContents
 * @param callback
 */
exports.uploadFileToAwsS3 = function(s3Connection, s3Folder, fileName, fileContents, callback){
	var params = {
		Key: path.join(process.env.AWS_S3_FOLDER, s3Folder, fileName),
		Body: fileContents
	};
	//upload file to S3
	s3Connection.upload(params, function(err, data) {
		if(err) {
			return callback(errors.newError('s3-upl-err', {originalError: err}));
		}
		data.Key = params.Key;
		callback(null, data);
	});
};

/**
 * Download file from AWS S3
 * @param s3Connection
 * @param s3Key
 * @param callback
 */
exports.downloadFileFromAwsS3 = function(s3Connection, s3Key, callback){
	s3Connection.getObject({Key: s3Key}, function(err, data) {
		if (err){
			return callback(errors.newError('s3-get-err', {originalError: err, key: s3Key}));
		}else{
			data.Key = s3Key;
			callback(null, data);
		}
	});
};

/**
 * Copy file inside AWS S3 bucket
 * @param s3Connection
 * @param s3KeyFrom
 * @param s3KeyTo
 * @param callback
 */
exports.copyFileInAwsS3 = function(s3Connection, s3KeyFrom, s3KeyTo, callback){
	//only copy inside same bucket
	var destinationBucket = s3Connection.config.params.Bucket;
	var copySource = encodeURI(path.join(destinationBucket, s3KeyFrom));
	s3Connection.copyObject({CopySource: copySource, Key: s3KeyTo}, function(err, data) {
		if (err){
			return callback(errors.newError('s3-copy-err', {originalError: err, from: s3KeyFrom, to: s3KeyTo}));
		}else{
			data.Key = s3KeyTo;
			callback(null, data);
		}
	});
};

/**
 * Delete file in AWS S3
 * @param s3Connection
 * @param s3Key
 * @param callback
 */
exports.deleteFileInAwsS3 = function(s3Connection, s3Key, callback){
	s3Connection.deleteObject({Key: s3Key}, function(err, data) {
		if (err){
			return callback(errors.newError('s3-del-err', {originalError: err, key: s3Key}));
		}else{
			data.Key = s3Key;
			callback(null, data);
		}
	});
};

/**
 * Move file inside AWS S3 (copy + delete old)
 * @param s3Connection
 * @param s3KeyFrom
 * @param s3KeyTo
 * @param callback
 */
exports.moveFileInAwsS3 = function(s3Connection, s3KeyFrom, s3KeyTo, callback){
	exports.copyFileInAwsS3(s3Connection, s3KeyFrom, s3KeyTo, function(err, copyRes){
		if(err){
			return callback(err);
		}
		exports.deleteFileInAwsS3(s3Connection, s3KeyFrom, function(err, delRes){
			if(err) {
				return callback(err);
			}
			callback(null, {copyResult: copyRes, deleteResult: delRes});
		});
	});
};
