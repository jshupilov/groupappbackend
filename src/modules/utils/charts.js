'use strict';
var d3 = require('d3');
var jsdom = require('jsdom');
var html = '<html><head></head><body><div id="dataviz-container"></div></body></html>';
var errors = require('../utils/errors');
var httpUtils = require('../utils/httpUtils');
var media = require('../media/services');

errors.defineError('cht-err-data', 'Charts Data error occurred', 'Wrong data structure sent to charts. data = [], which should include "x"', 'Charts');
errors.defineError('cht-err-names', 'Missing properties', 'names should inlcude "x" and "y" properties', 'Charts');
errors.defineError('cht-err-lines', 'Lines number exceeded', 'Too many lines on chart', 'Charts');
errors.defineError('cht-err-obj', 'Error, data incorrect', 'data object values are not equal', 'Charts');
errors.defineError('cht-err', 'Charts Error', 'Error accured on charts creation', 'Charts');
var chartLines = 10;

exports.generateCharts = function (data, names, callback) {

	jsdom.env({
		features: {QuerySelector: true}, html: html, done: function (err, window) {

			if (err) {
				return callback(err);
			}

			try {
				var el = window.document.querySelector('#dataviz-container');

				var margin = {top: 20, right: 80, bottom: 30, left: 50},
					width = 960 - margin.left - margin.right,
					height = 500 - margin.top - margin.bottom;

				var x = d3.time.scale()
					.range([0, width]);

				var y = d3.scale.linear()
					.range([height, 0]);

				var color = d3.scale.category10();

				var xAxis = function() {
					return d3.svg.axis()
						.scale(x)
						.orient('bottom');
				};
				var yAxis = function(){
					return d3.svg.axis()
						.scale(y)
						.orient('left');
				};

				var line = d3.svg.line()
					.interpolate('monotone')
					.x(function (d) {
						return x(d.x);
					})
					.y(function (d) {
						return y(d.y);
					});

				var svg = d3.select(el).append('svg')
					.attr('width', width + margin.left + margin.right)
					.attr('height', height + margin.top + margin.bottom)
					.append('g')
					.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

				color.domain(d3.keys(data[0]).filter(function (key) {
					return key !== 'x';
				}));

				var def = color.domain().map(function (name) {
					return {
						name: name,
						values: data.map(function (d) {
							return {x: d.x, y: +d[name]};
						})
					};
				});

				x.domain(d3.extent(data, function (d) {
					return d.x;
				}));

				y.domain([
					d3.min(def, function (c) {
						return d3.min(c.values, function (v) {
							return v.y;
						});
					}),
					d3.max(def, function (c) {
						return d3.max(c.values, function (v) {
							return v.y;
						});
					})
				]);

				svg.append('g')
					.attr('class', 'x-axis')
					.attr('transform', 'translate(0,' + height + ')')
					.call(xAxis())
					.append('text')
					.attr('x', 830)
					.attr('y', '-5')
					.style('text-anchor', 'end')
					.text(names.x);

				svg.append('g')
					.attr('class', 'y-axis')
					.call(yAxis())
					.append('text')
					.attr('transform', 'rotate(-90)')
					.attr('y', 6)
					.attr('dy', '.71em')
					.style('text-anchor', 'end')
					.text(names.y);

				//add gridlines
				svg.append('g')
					.attr('class', 'grid')
					.attr('transform', 'translate(0,' + height + ')')
					.call(xAxis()
						.tickSize(-height, 0, 0)
						.tickFormat('')
					);

				svg.append('g')
					.attr('class', 'grid')
					.call(yAxis()
						.tickSize(-width, 0, 0)
						.tickFormat('')
					);
				//style gridlines
				svg.selectAll('.grid').selectAll('path')
					.style('stroke-width', 0);

				svg.selectAll('.grid').selectAll('line')
					.style('stroke', 'gray')
					.style('opacity', 0.7);

				var lines = svg.selectAll('.city')
					.data(def)
					.enter().append('g')
					.attr('class', 'city');

				lines.append('path')
					.attr('class', 'line')
					.attr('fill', 'none')
					.attr('stroke-width', '1.5px')
					.attr('d', function (d) {
						return line(d.values);
					})
					.style('stroke', function (d) {
						return color(d.name);
					});

				lines.append('text')
					.datum(function (d) {
						return {name: d.name, value: d.values[d.values.length - 1]};
					})
					.attr('transform', function (d) {
						return 'translate(' + x(d.value.x) + ',' + y(d.value.y) + ')';
					})
					.attr('y', data[0].length)
					.attr('dy', '.35em')
					.text(function (d) {
						return d.name;
					});

				callback(null, window.document.getElementById('dataviz-container').innerHTML);

			} catch (e) {
				callback(e);
			}
		}
	});
};


/**
 * Check data for creating charts and run generateCharts fun.
 * @param data {Array} of object
 * @param names {Object}
 * @param callback receives a string of <svg> as second argument
 */
exports.createCharts = function (data, names, callback) {

	if (!names.hasOwnProperty('x') || !names.hasOwnProperty('y')) {
		return callback(errors.newError('cht-err-names'));
	}
	if (!data[0] || !data[0].hasOwnProperty('x')) {
		return callback(errors.newError('cht-err-data', {
			data: data[0]
		}));
	}

	if (Object.keys(data[0]).length > chartLines) {
		return callback(errors.newError('cht-err-lines', {
			max_lines: chartLines
		}));
	}

	for (var i = 0; i < data.length; i++) {

		if (JSON.stringify(Object.keys(data[i])) !== JSON.stringify(Object.keys(data[0]))) {
			return callback(errors.newError('cht-err-obj', {
				broken_object: data[i]
			}));
		}

	}

	exports.generateCharts(data, names, function (err, chart) {
		if (err) {
			return callback(errors.newError('cht-err', {
				originalError: err.message
			}));
		}
		callback(null, chart);
	});

};

/**
 * Upload chart to Cloudinary
 * @param chartName
 * @param svg
 * @param callback
 */
function uploadChart(chartName, svg, callback){
	var svgHeader = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n';
	media.uploadHotelGroupImage('slh', new Buffer(svgHeader + svg), {public_id: 'reports/charts/' + chartName.replace(/[^a-z0-9-_:\.]/ig, '-')}, callback);
}

/**
 * Create chart and get response an JPG image
 * @param chartName
 * @param data
 * @param axisNames
 * @param callback receives HTTP response object + body key with binary image data
 */
exports.createChartAndGetImage = function(chartName, data, axisNames, callback){
	exports.createCharts(data, axisNames, function(err, svg){
		if(err) {
			return callback(err);
		}
		uploadChart(chartName, svg, function(err, res){
			if(err) {
				return callback(err);
			}
			var url = media.parseImageObjectToUrl({source: res}, {format: 'jpg'});
			httpUtils.makeRequestToUrl(url, {encoding: 'binary'}, callback);
		});
	});
};
