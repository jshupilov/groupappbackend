'use strict';
var apiUtils = require('./apiUtils');
var errors = require('../utils/errors');
errors.defineError('sr-err-ol2pi-hl', 'System error occurred', 'Error converting offset and limit to pages, it took too many cycles', 'Search');

/**
 * Formats the collection for output
 *
 * @param items
 * @param offset
 * @param limit
 * @param href
 * @param totalCount
 * @param apiRequest
 * @param [parent]
 * @returns {{href: *, offset: *, limit: *, items: *}}
 */
exports.formatCollection = function(items, offset, limit, href, totalCount, apiRequest, parent) {

	var ret = {
		'href': href,
		'offset': offset ? parseInt(offset) : 0,
		'limit': limit ? parseInt(limit) : 1000,
		'total': totalCount ? parseInt(totalCount) : items.length
	};



	if(apiRequest) {
		if(ret.offset + ret.limit < ret.total) {
			ret.next = apiUtils.getModifiedCurrentHref(apiRequest, {offset: ret.offset + ret.limit});
		}
		if(ret.offset > 0) {
			ret.prev = apiUtils.getModifiedCurrentHref(apiRequest, {offset: ret.offset - 1});
		}
	}
	ret.items = items ? items : [];

	if(parent){
		ret.parent = parent;
	}
	return ret;

};

/**
 * Format a collection with ApiRequest object
 * @param items
 * @param apiRequest
 * @param totalCount
 * @param [parent]
 * @returns {{href: *, offset: *, limit: *, items: *}}
 */
exports.formatCollectionWithRequest = function(items, apiRequest, totalCount, parent) {
	var offset = apiRequest.collectionQuery.offset;
	var limit = apiRequest.collectionQuery.limit;
	return exports.formatCollection(items, offset, limit, apiRequest.href, totalCount, apiRequest, parent);
};

/**
 * Calculate page number and items per page from offset and limit input,
 * so that the resulting page includes all the needed elements
 *
 * @param offset
 * @param limit
 * @returns {{page: number, items: number, pageOffset: number}}
 */
exports.offsetLimitToPageItems = function(offset, limit) {
	var page = 0;
	var items = limit;
	var pageOffset = 0;

	var hardLimitO = 1000000;
	var hardLimit = hardLimitO;


	while((page * items + items < offset + limit ) && hardLimit--) {
		page++;
		if(page * items > offset) {
			page = 0;
			items++;
		}
	}

	if(hardLimit < 1){
		throw errors.newError('sr-err-ol2pi-hl', {
			givenOffset: offset,
			givenLimit: limit,
			page: page,
			items: items,
			hardLimit: hardLimitO
		});
	}else{
		pageOffset = offset - page * items;
	}


	return {
		page: page,
		items: items,
		pageOffset: pageOffset
	};
};


/**
 * Order list of objects by a reference array by a given field value
 * @param items {Array}
 * @param orderArray {Array}
 * @param propertyName {String}
 * @returns {Array}
 */
exports.orderByArray = function(items, orderArray, propertyName){
	var newArray = [];
	orderArray.forEach(function(val){
		items.forEach(function(item){
			if(item.hasOwnProperty(propertyName) && item[propertyName] === val){
				newArray.push(item);
			}
		});
	});
	return newArray;
};

/**
 * Order list of objects by a reference array by a given subfield value
 * @param items {Array}
 * @param orderArray {Array}
 * @param propertyName1 {String} Object in the item, where the order attribute is
 * @param propertyName2 {String} Order attribute
 * @returns {Array}
 */
exports.orderByArrayAndSubField = function(items, orderArray, propertyName1, propertyName2){
	var newArray = [];
	orderArray.forEach(function(val){
		items.forEach(function(item){
			if(item.hasOwnProperty(propertyName1) && item[propertyName1].hasOwnProperty(propertyName2) && item[propertyName1][propertyName2] === val){
				newArray.push(item);
			}
		});
	});
	return newArray;
};
