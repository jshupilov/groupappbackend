'use strict';
var cryptoLib = require('crypto');
var Buf = require('buffer').Buffer;
var errors = require('../utils/errors');
var uuid = require('node-uuid');
//var algorithm = ['sha1', 'md5', 'sha256', 'sha512', 'aes-256-ctr'];
errors.defineError('cu-string-inv', 'System error', 'TypeError: Bad input string', 'cryptoUtils', 500);
errors.defineError('cu-err-re-key', 'System error', 'The system is unable to generate a random', 'cryptoUtils', 500);
errors.defineError('cu-ex-enc', 'System error', 'And exception was thrown while encrypting the data', 'cryptoUtils', 500);
errors.defineError('cu-ex-dec', 'System error', 'And exception was thrown while decrypting the data', 'cryptoUtils', 500);

exports.md5 = function(string) {
	var digest = 'hex';
	string = new Buf(string);
	return cryptoLib.createHash('md5').update(string, 'utf8').digest(digest);
};

exports.sha256 = function(string) {
	var digest = 'hex';
	string = new Buf(string);
	return cryptoLib.createHash('sha256').update(string, 'utf8').digest(digest);
};

exports.sha1 = function(string) {
	var digest = 'hex';
	string = new Buf(string);
	return cryptoLib.createHash('sha1').update(string, 'utf8').digest(digest);
};

exports.encrypt = function(text, password){
	var cipher = cryptoLib.createCipher('aes-256-ctr', password);
	var enc = cipher.update(text, 'utf8', 'hex');
	enc += cipher.final('hex');
	return enc;
};
exports.decrypt = function(text, password){
	var decipher = cryptoLib.createDecipher('aes-256-ctr', password);
	var dec = decipher.update(text, 'hex', 'utf8');
	dec += decipher.final('utf8');
	return dec;
};

/**
 * Retrieve key for symmetric encryption
 * @param password
 */
exports.retrieveSymCryptKey = function(password){
	return cryptoLib.pbkdf2Sync(password, 'DaSFDTQFIYtq3pk5ijUIjzkshRH7NOsC4XgdoS', 1024, 32, 'sha256');
};

/**
 * Create ad initialization vector (IV) for symmetric encryption
 * @return string
 */
exports.createSymCryptIv = function(){
	return cryptoLib.randomBytes(16).toString('hex');
};

/**
 * Modify the IV a bit before using it
 * @param iv
 */
exports.bakeSymCryoptIv = function(iv){
	//do an bitwise XOR with 16 byte integer
	iv = exports.encrypt(new Buffer(iv, 'hex').toString('ascii'), '98ufsnsdf903f');
	return new Buffer(iv, 'hex');
};

/**
 * Create a random value with (at least) given length
 * @param size
 * @returns {string}
 */
exports.getRandomValue = function(size){
	var out = '';
	while(out.length <= size){
		out += exports.sha256(uuid.v1());
	}
	return out;
};

/**
 * Encrypt given text securely
 * @param key {String} Encryption key - used to retrieve actual encryption key
 * @param iv {String} Initialization vector - a random 16 byte string, which is used to get the final IV
 * @param text {String} The text to encrypt
 * @returns {*} Encrypted string
 */
exports.strongEncryption = function(key, iv, text){

	try{
		var cipher = cryptoLib.createCipheriv(
			'aes-256-ctr',
			exports.retrieveSymCryptKey(key),
			exports.bakeSymCryoptIv(iv)
		);
	}catch(e){
		throw errors.newError('cu-ex-enc', {originalError: e.message});
	}
	var enc = cipher.update(text, 'utf8', 'hex');
	enc += cipher.final('hex');
	//overwrite variables
	key = exports.getRandomValue(key.length);
	iv = exports.getRandomValue(iv.length);
	text = exports.getRandomValue(text.length);
	return enc;
};

/**
 * Decrypt given securely encrypted text (mirror function of strongEncryption)
 * @param key {String} Decryption key - used to retrieve actual decryption key (Must be same as used with strongEncryption)
 * @param iv {String} Initialization vector - a random 16 byte string, which is used to get the final IV (Must be same as used with strongEncryption)
 * @param text {String} The encoded text to decrypt
 * @returns {Object} The unencrypted object
 */
exports.strongDecryption = function(key, iv, text){

	try{
		var cipher = cryptoLib.createDecipheriv(
			'aes-256-ctr',
			exports.retrieveSymCryptKey(key),
			exports.bakeSymCryoptIv(iv)
		);
	}catch(e){
		throw errors.newError('cu-ex-dec', {originalError: e.message});
	}

	var enc = cipher.update(text, 'hex', 'utf8');
	enc += cipher.final('utf8');
	//overwrite variables
	key = exports.getRandomValue(key.length);
	iv = exports.getRandomValue(iv.length);
	text = exports.getRandomValue(text.length);
	return enc;
};
