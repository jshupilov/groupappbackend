'use strict';

/**
 * Created by jevgenishupilov on 19/12/14.
 */
var pg = require('pg');
var errors = require('./errors');
var logging = require('./logging');

errors.defineError('db-cc-db', 'System error!', 'No database connection', 'App');
errors.defineError('db-query-fails', 'System error!', 'Database query failed', 'Utils.DatabaseUtils');


/**
 * Get a connection to statistic database
 * @param callback
 */
exports.getConnectionStats = function(callback) {
	exports.getConnection(callback, process.env.STATS_DATABASE_URL);
};

/**
 * Get a connection to database
 * @param callback
 * @param [databaseUrl]
 */
exports.getConnection = function(callback, databaseUrl) {
	databaseUrl = databaseUrl || process.env.DATABASE_URL;
	var client = new pg.Client(databaseUrl);
	client.connect(function(err) {
		var done = function() {
			client.end();
		};
		if(err) {
			return callback(errors.newError('db-cc-db', {originalError: err}));
		} else {
			//do some modifications
			client._query = client.query;

			var errorHandler = function(error) {
				logging.alert('DB error ', error);
			};
			client.query = function() {
				//handle error on querying
				if(arguments.length > 1 && typeof arguments[arguments.length - 1] === 'function') {
					var callback2 = arguments[arguments.length - 1];
					arguments[arguments.length - 1] = function(err, result) {
						if(err) {
							errorHandler(err);
							err = errors.newError('db-query-fails', {originalError: err});
						}
						callback2(err, result);
					};
					return client._query.apply(this, arguments);
				} else {
					var query = client._query.apply(this, arguments);
					query.on('error', function(error) {
						error = errors.newError('db-query-fails', {originalError: error});
						errorHandler(error);
					});
					return query;
				}
			};

			callback(null, client, done);
		}

	});

};

/**
 * Execute piece of code inside a transaction.
 * @param inTransactionCallback {Function} is called with arguments (databaseClient, done) - done()
 * should be called with (err, res) when done with logics inside transaction
 * @param callback
 */
exports.doInTransaction = function(inTransactionCallback, callback) {
	exports.getConnection(function(err, client, done) {
		if(err) {
			return callback(err);
		}
		client.query('START TRANSACTION', function(err) {
			if(err) {
				done();
				return callback(err);
			}

			//call the function inside
			inTransactionCallback(client, function(err, res){

				if (err) {
					client.query('ROLLBACK', function () {
						done();
						//do not care about rollback the error
						return callback(err);
					});

				}else{
					client.query('COMMIT', function (err, commitRes) {
						done();
						if (err) {
							return callback(err);
						}
						callback(null, res, commitRes);
					});
				}
			});
		});
	});
};
