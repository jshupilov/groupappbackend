'use strict';
var util = require('util');
var boom = require('boom');
var userErrors = require('../../../data/defaults/user_errors.json');
var errors = {};

exports.baseUrl = '';

var getErrorMessageHelpPage = function(errorCode) {
	return exports.baseUrl + '/doc/#errors-' + errorCode;
};


function CustomError(message){
	this.name = 'CustomError';
	this.message = message;
	var trace = new Error('').stack.split('\n');
	var newTrace = trace.slice(3);

	this.stack = newTrace.join('\n');
}

util.inherits(CustomError, Error);


var newError = function(code, debuggingData, fileName, lineNumber, isRetriable) {
	var errDef = errors[code];
	var error = new CustomError(errDef.developerMessage + ' (' + code + ')' + '\nDebuggingData: ' + JSON.stringify(debuggingData), fileName, lineNumber);
	error.data = errDef;
	error.data.isCustomError = true;
	error.data.debuggingData = debuggingData;
	error.data.isRetriable = isRetriable || false;
	//by default the english message is defined
	error.data.localized = {
		en_GB: {
			userMessage: error.data.message
		}
	};
	for(var lang in userErrors.localized){
		if(userErrors.localized[lang][code]){
			error.data.localized[lang] = {
				userMessage: userErrors.localized[lang][code]
			};
		}
	}
	error.data.stack = error.stack;
	//console.log(error);
	//for(var e in  error.data.debuggingData){
	//	if( error.data.debuggingData[e] instanceof Error){
	//		console.log(error.data.debuggingData[e].stack);
	//	}
	//}
	return error;
};

/**
 * Define an error once
 * @param code
 * @param message
 * @param developerMessage
 * @param module
 * @param statusCode
 * @returns {*}
 */
exports.defineErrorOnce = function(){
	if(errors[arguments[0]] === undefined) {
		return exports.defineError.apply(this, arguments);
	}
};

/**
 * Define an Error
 * @param code Globally unique error code
 * @param message Error message - displayable to end user
 * @param developerMessage Error message for developer
 * @param module Module name, which defined the error
 * @param [statusCode] Http status code, defaults to 500
 * @param [isRetriable] Retriable flag, which mean, if true than error is retriable
 * @throws Error if error with same code already defined
 */
exports.defineError = function(code, message, developerMessage, module, statusCode) {

	errors[code] = {
		module: module,
		code: code,
		message: message,
		developerMessage: developerMessage,
		moreInfo: getErrorMessageHelpPage(code),
		statusCode: statusCode || 500
	};
};

exports.defineError('undefinedError', 'System error!', 'Error with such code is undefined!', 'Utils.Errors');
exports.defineError('duplicateError', 'System error!', 'Error with such code is already defined!', 'Utils.Errors');
exports.defineError('system-error', 'System error!', 'There is a problem with the code', 'CommonError');

/**
 * Create a new error instance
 * @param code
 * @param [debuggingData]
 * @param [isRetriable] Retriable flag, which mean, if true than error is retriable
 * @throws Error if undefined error code is given
 */
exports.newError = function(code, debuggingData, isRetriable) {
	if(errors[code] === undefined) {
		throw newError('undefinedError', {errorCodeRequired: code});
	}
	return newError(code, debuggingData, null, null, isRetriable);
};

/**
 * Create new boom error
 * @param code
 * @param statusCode
 * @param [debuggingData]
 */
exports.newBoomError = function(code, statusCode, debuggingData) {
	var e = exports.newError(code, debuggingData);
	return exports.wrapToBoom(e, statusCode);
};

/**
 * Wrap error object instance to boom object
 * @param error Error instance created with newError
 * @param [statusCode]
 * @returns {*}
 */
exports.wrapToBoom = function(error, statusCode) {
	if(error === undefined || !error){
		return null;
	}
	var msg = error.data ? error.data.message : error.message;
	var sc = statusCode || (error.data ? error.data.statusCode : '500');
	var err = boom.wrap(error, sc, msg);
	var finalStatusCode = err.output.payload.statusCode;

	err.output.payload = error.data ? error.data : {};
	err.output.payload.statusCode = finalStatusCode;
	return err;
};

/**
 * Get instance of all errors
 * @returns {Array}
 */
exports.getAllErrors = function(){
	var out = [];
	for(var ei in errors){
		out.push(exports.newError(ei));
	}
	return out;
};
