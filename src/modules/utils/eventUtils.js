/**
 * Created by jevgenishupilov on 19/01/15.
 */
'use strict';
var eventflow = require('eventflow');


function Emitter () {
	// Constructor stuff.
}

eventflow(Emitter);

Emitter.prototype = {

};

exports.emitter = Emitter;
