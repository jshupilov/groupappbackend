'use strict';

var FtpClient = require('ftp');
var errors = require('./errors');
var logging = require('./logging');
var awsS3Utils = require('./awsS3Utils');
var q = require('q');
var path = require('path');

errors.defineError('ftp-err-conn', 'System error', 'Error occurred connecting to FTP server', 'FtpUtils');
errors.defineError('ftp-get-err', 'System error', 'Error occurred fetching file from FTP server', 'FtpUtils');
errors.defineError('ftp-list-err', 'System error', 'Error occurred listing files from FTP server', 'FtpUtils');

/**
 * Create a ftp connection
 * @param config {Object} Configruation with host, port, user, password keys
 * @param callback
 */
exports.createFtpConnection = function(config, callback) {
	var ftpClientInstance = new FtpClient();
	ftpClientInstance.on('error', function(err) {
		return callback(errors.newError('ftp-err-conn', {originalError: err, causedBy: 'connection'}));
	});

	ftpClientInstance.on('ready', function() {
		callback(null, ftpClientInstance);
	});

	ftpClientInstance.connect(config);

};


/**
 * List files of a folder in FTP server
 * @param ftpClientInstance {Object} Ftp Client instance
 * @param folder {String} Folder to list
 * @param callback
 */
exports.listFtpFiles = function(ftpClientInstance, folder, callback) {
	ftpClientInstance.list(folder, function(err, list) {

		if(err) {
			return callback(errors.newError('ftp-list-err', {originalError: err.message, causedBy: 'list'}));
		}

		callback(null, list);
	});
};

/**
 * Copy ftp file to AWS S3
 * @param ftpConnection
 * @param s3Connection
 * @param ftpFilePath
 * @param s3Folder
 * @param callback
 */
exports.copyFtpFileToS3 = function(ftpConnection, s3Connection, ftpFilePath, s3Folder, callback) {
	ftpConnection.get(
		ftpFilePath,
		function(err, stream) {
			if(err) {
				return callback(errors.newError('ftp-get-err', {
					originalError: err.message,
					causedBy: 'get',
					ftpFilePath: ftpFilePath
				}));
			}
			stream.setEncoding('binary');
			var fileContents = '';
			stream.on('data', function(chunk) {
				fileContents +=chunk;
			});

			stream.on('end', function() {
				fileContents = new Buffer(fileContents, 'binary');

				awsS3Utils.uploadFileToAwsS3(s3Connection, s3Folder, ftpFilePath.split('/').pop(), fileContents, function(err, data){
					if(err) {
						return callback(err);
					}
					data.s3Key = path.join(process.env.AWS_S3_FOLDER, s3Folder, ftpFilePath.split('/').pop());
					data.fileName = ftpFilePath.split('/').pop();
					callback(null, data);
				});
			});
		}
	);
};

/**
 * Get missing and existing files in S3 folder, compared with FTP folder
 * @param ftpConnection {Object} Ftp Client
 * @param s3Connection {Object} S3 Connection
 * @param ftpFolder {String} Ftp server folder
 * @param s3Folder {String} AWS S3 folder
 * @param callback
 */
exports.getFilesDiffOfS3AndFtp = function(ftpConnection, s3Connection, ftpFolder, s3Folder, callback) {
	//get list of already downloaded files from AWS S3
	awsS3Utils.listAwsS3Files(s3Connection, s3Folder, function(err, data) {
		if(err) {
			return callback(err);
		}

		var existingFiles = data.Contents.map(function(item, index) {
			//add existing files to the completed items array
			data.Contents[index].fileName = item.Key.split('/').pop();
			return data.Contents[index].fileName;
		});

		//Get list of files in FTP server
		exports.listFtpFiles(ftpConnection, ftpFolder, function(err, list) {
			if(err) {
				return callback(err);
			}

			//filter out files, which need to be downloaded
			var missingFiles = [];
			list.forEach(function(file) {
				file.fileName = file.name;
				if(existingFiles.indexOf(file.name) < 0) {
					missingFiles.push(file);
				}
			});

			callback(null, {missingFiles: missingFiles, existingFiles: data.Contents, totalFilesCount: list.length});
		});
	});
};

/**
 * Copy files from foreign FTP server to S3 bucket. It will skip already existing files.
 * @param ftpConnection {Object} Ftp connection instance
 * @param s3Connection {Object} AWS S3 connection configuration
 * @param ftpFolder {String} The source FTP folder
 * @param s3Folder {String} The target S3 folder
 * @param callback
 * @param [s3KeySuffixCallback] Callback, gets fileName as argument, should return a suffix for the AWS S3 Key (for creating subfolders)
 * @param [progressCallback] Callback, which gets downloaded items count and total files count as parameters (DO NOT CHANGE them!)
 */
exports.copyFtpFilesToS3 = function(ftpConnection, s3Connection, ftpFolder, s3Folder, callback, s3KeySuffixCallback, progressCallback) {

	s3KeySuffixCallback = s3KeySuffixCallback || function(){return '';};
	progressCallback = progressCallback || function(){};
	exports.getFilesDiffOfS3AndFtp(
		ftpConnection,
		s3Connection,
		ftpFolder,
		s3Folder,
		function(err, filesDiff) {
			if(err){
				return callback(err);
			}
			var missingFiles = filesDiff.missingFiles;
			var results = [];
			filesDiff.existingFiles.forEach(function(s3Resource) {
				results.push({uploadRes: 'existing', file: s3Resource, s3Key: s3Resource.Key});
			});

			var promises = [];

			function copy(p, ftpFile, index) {
				promises[index-1].finally(function(){
					exports.copyFtpFileToS3(
							ftpConnection,
							s3Connection,
							path.join(ftpFolder, ftpFile.name),
							path.join(s3Folder, s3KeySuffixCallback(ftpFile.name)),
							function(err, uploadRes) {
								if(err) {
									results.push({error: err, file: ftpFile});
									p.reject(err);
								} else {
									results.push({uploadRes: uploadRes, file: ftpFile, s3Key: uploadRes.s3Key});
									p.resolve();
								}
								//report progress when have time
								setTimeout(function(){
									progressCallback(results.length, filesDiff.totalFilesCount);
								});
							}
					);
				});

			}

			var p1 = q.defer();
			promises.push(p1.promise);
			missingFiles.forEach(function(file) {
				var p = q.defer();
				promises.push(p.promise);
				copy(p, file, promises.length-1);
			});

			logging.info('Starting FTP to S3 download', ftpFolder, s3Folder, missingFiles.length);

			//resolve first dummy promise
			p1.resolve();

			//if all done
			q.allSettled(promises).done(function(snapshots) {
				//if error occurred, return first error object
				for(var si in snapshots) {
					var snapshot = snapshots[si];
					if(snapshot.state === 'rejected') {
						return callback(snapshot.reason, results);
					}
				}

				callback(null, results);
			});
		}
	);

};
