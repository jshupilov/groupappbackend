'use strict';
var http = require('http');
var https = require('https');
var errors = require('./errors');
var apiUtils = require('./apiUtils');

errors.defineError('http-inv-res', 'System error', 'Invalid response from http(s) request', 'HttpUtils');
errors.defineError('http-req-err', 'System error', 'Error occurred making a http(s) request', 'HttpUtils');
errors.defineError('http-inv-prot', 'System error', 'Invalid protocol', 'HttpUtils');

/**
 * Make request to given URL
 * @param url
 * @param options {Object} Parameters to http.request
 * @see https://nodejs.org/docs/latest/api/http.html#http_http_request_options_callback
 * @param callback
 */
exports.makeRequestToUrl = function(url, options, callback){
	var urlObj = apiUtils.getUrlObj(url);
	options.host = urlObj.host;
	options.path = urlObj.pathname;
	exports.makeRequest(options, urlObj.protocol.replace(/[^htp]/g, ''), callback);
};

/**
 * Make a request
 * @param options
 * @param protocol
 * @param callback
 */
exports.makeRequest = function(options, protocol, callback){
	var library;
	if(protocol === 'https'){
		library = https;
	}else if(protocol === 'http'){
		library = http;
	}else{
		return callback(errors.newError('http-inv-prot',  {protocol: protocol}));
	}

	var req = library.request(options, function (res) {
		var response = '';

		res.setEncoding(options.encoding ? options.encoding : 'utf8');
		res.on('data', function(chunk){
			response += chunk;
		});
		res.on('end', function(){
			res.body = response;
			var err = null;
			if(res.statusCode !== 200) {
				err = errors.newError('http-inv-res', {reason: 'Status code not 200'});
			}
			return callback(err, res);
		});
	});

	if(options.body) {
		req.write(options.body);
	}
	req.end();

	req.on('error', function(e) {
		return callback(errors.newError('http-req-err', {originalError: e.message, protocol: protocol}, e.code === 'ECONNRESET'));
	});

	return req;
};
