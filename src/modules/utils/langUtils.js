'use strict';
var errors = require('./errors');
var defaultTranslations = require('../../../data/defaults/translations.json');
var countries = require('../../../data/defaults/countries.json');
var languages = require('../../../data/defaults/languages.json');
var check = require('check-types');
var handlebars = require('handlebars');

errors.defineError('lu-no-langcode', 'System error', 'No language code provided', 'Utils.langUtils');
errors.defineError('lu-inv-langcode', 'System error', 'Invalid language code provided', 'Utils.langUtils');
errors.defineError('lu-no-countrycode', 'System error', 'No country code provided', 'Utils.langUtils');
errors.defineError('lu-inv-countrycode', 'System error', 'Invalid country code provided', 'Utils.langUtils');
errors.defineError('lu-no-trans', 'System error', 'No translation found', 'Utils.langUtils');
errors.defineError('lu-transf-merge-fail', 'System error', 'Failed to merge translations with defaults', 'Utils.langUtils');
errors.defineError('lu-vdt-inv-val', 'System error', 'Invalid translation object value', 'Utils.langUtils');
errors.defineError('lu-vdt-key-miss', 'System error', 'Key missing from translation object', 'Utils.langUtils');
errors.defineError('lu-vdt-inv-lang', 'System error', 'Invalid language in translations', 'Utils.langUtils');
errors.defineError('lu-vdt-inv-tpl', 'System error', 'Invalid translation value template', 'Utils.langUtils');

var langsObj = {};
var countriesObj = {};

/**
 * Get list of all default translation keys
 * @returns {Array}
 */
exports.getDefaultTranslationKeys = function() {
	var keys = [];
	for(var lk in defaultTranslations.localized) {
		for(var k in defaultTranslations.localized[lk]) {
			if(keys.indexOf(k) < 0) {
				keys.push(k);
			}
		}
	}
	return keys;
};

/**
 * Merge given translations with default translations
 * @param translations {Object} with key 'localized'
 * @param callback
 */
exports.mergeTranslationsWithDefault = function(translations, callback) {
	var out = {
		localized: {}
	};
	try {
		//take overwrites
		for(var lk in translations.localized) {
			out.localized[lk] = JSON.parse(JSON.stringify(translations.localized[lk]));
			//check default values and add if missing
			if(defaultTranslations.localized.hasOwnProperty(lk)) {
				for(var tk in defaultTranslations.localized[lk]) {
					if(!out.localized[lk].hasOwnProperty(tk)) {
						out.localized[lk][tk] = defaultTranslations.localized[lk][tk];
					}
				}
			}
		}
	} catch(e) {
		return callback(errors.newError('lu-transf-merge-fail', {originalError: e}));
	}

	callback(null, out);
};

/**
 * Load languages from file
 */
exports.loadLanguages = function() {

	languages.forEach(function(ar) {
		langsObj[ar[0]] = {
			code: ar[0],
			englishName: ar[1],
			rtl: ar[2].indexOf('rtl') > -1,
			nativeName: ar[3]
		};

	});

};

/**
 * Load countries from file
 */
exports.loadCountries = function() {

	countries.forEach(function(ar) {

		//English short name (upper/lower case)|Alpha-2 code|Alpha-3 code|Numeric code|ISO 3166-2 codes
		countriesObj[ar[1]] = {
			code: ar[1],
			englishName: ar[0]
		};

	});

};

/**
 * Get language object with or without location identifier ("en" OR "en_GB")
 * @param languageCode
 * @returns {*}
 */
exports.getLanguageObject = function(languageCode) {
	if(!languageCode) {
		throw errors.newError('lu-no-langcode');
	}
	var codeArr = languageCode.split('_');
	var langKey = codeArr[0];
	var countryKey = codeArr[1] !== undefined ? codeArr[1] : null;
	if(langsObj.hasOwnProperty(langKey)) {
		var obj = {
			code: langsObj[langKey].code,
			englishName: langsObj[langKey].englishName,
			nativeName: langsObj[langKey].nativeName,
			rtl: langsObj[langKey].rtl
		};
		if(countryKey) {
			var countryObj = exports.getCountryObject(countryKey.toUpperCase());
			obj.code += '_' + countryObj.code;
			obj.countryEnglishName = countryObj.englishName;
		}
		return obj;
	} else {
		throw errors.newError('lu-inv-langcode', {givenLanguageCode: languageCode});
	}

};

/**
 * Check if language code is invalid or not
 * @param languageCode
 * @returns {boolean}
 */
exports.isValidLanguageCode = function(languageCode) {
	try {
		exports.getLanguageObject(languageCode);
		return true;
	} catch(e) {
		return false;
	}
};

/**
 * Get country object
 * @param countryCode
 * @returns {*}
 */
exports.getCountryObject = function(countryCode) {
	if(!countryCode) {
		throw errors.newError('lu-no-countrycode');
	}
	if(countriesObj.hasOwnProperty(countryCode)) {
		return countriesObj[countryCode];
	} else {
		throw errors.newError('lu-inv-countrycode', {givenCountryCode: countryCode, iHave: countriesObj});
	}

};

/**
 * Get localised data from database object
 *
 * @param item
 * @param languageCode
 * @param [itemModifierCallback]
 * @param [languageCodeAttr] Attribute name inside localized data objects, which defines the language, key otherwise
 * @returns {{}}|boolean Returns null if not localized
 */
exports.getLocalizedData = function(item, languageCode, itemModifierCallback, languageCodeAttr) {
	var out = {};
	for(var i in item) {
		if(i !== 'localized') {
			out[i] = item[i];
		}
	}

	if(item.hasOwnProperty('localized')) {
		if(languageCodeAttr) {
			for(var nr in item.localized) {
				if(item.localized[nr].hasOwnProperty(languageCodeAttr) && item.localized[nr][languageCodeAttr] === languageCode) {
					for(var l in item.localized[nr]) {
						out[l] = item.localized[nr][l];
					}
				}
			}
		} else if(item.localized.hasOwnProperty(languageCode)) {
			for(var l2 in item.localized[languageCode]) {
				out[l2] = item.localized[languageCode][l2];
			}

		}
	} else {
		return null;
	}

	if(typeof itemModifierCallback === 'function') {
		out = itemModifierCallback(out, languageCode);
	}
	return out;
};

/**
 * Get localised data for all languages
 *
 * @param item
 * @param [itemModifierCallback]
 * @returns {{}}|boolean Returns null if not localized
 */
exports.getLocalizedDataForAllLanguages = function(item, itemModifierCallback) {
	var out = {};

	if(item.hasOwnProperty('localized')) {
		for(var l in item.localized) {
			out[l] = exports.getLocalizedData(item, l, itemModifierCallback);
		}
	}
	return out;
};

/**
 * Get translation of a string from the translations object
 * @param translations
 * @param key
 * @param language
 * @returns {*}
 */
exports.getTranslation = function(translations, key, language) {
	if(translations.localized[language] && translations.localized[language][key]) {
		return translations.localized[language][key];
	}

	if(defaultTranslations.localized[language]
		&& defaultTranslations.localized[language][key]
	) {
		return defaultTranslations.localized[language][key];
	}
	throw errors.newError('lu-no-trans', {key: key, language: language});
};

/**
 * Get translation of a string from the translations object
 * @param selectedLanguages
 * @param supportedLanguages
 * @returns {*}
 */
exports.validateSelectedLanguages = function(selectedLanguages, supportedLanguages) {
	if(!Array.isArray(selectedLanguages)) {
		return false;
	}
	for(var l in selectedLanguages) {
		if(typeof selectedLanguages[l] !== 'string' || supportedLanguages.indexOf(selectedLanguages[l]) < 0
		) {
			return false;
		}
	}
	return true;
};

/**
 * Get possible language, in which this object can be shown in
 * @param languagePreferences {String[]} Array of language codes
 * @param defaultLanguage {String} Default language
 * @param object
 * @returns {*}
 */
exports.getPossibleLanguageOfObject = function(languagePreferences, defaultLanguage, object) {
	for(var li in languagePreferences) {
		if(object.localized.hasOwnProperty(languagePreferences[li])) {
			return languagePreferences[li];
		}
	}
	if(object.localized.hasOwnProperty(defaultLanguage)) {
		return defaultLanguage;
	}
	return null;
};

/**
 * Validate translations
 * @param translations
 * @param callback
 * @returns {*}
 */
exports.validateTranslations = function(translations, callback) {
	if(!check.object(translations)) {
		return callback(errors.newError('lu-vdt-inv-val', {}));
	}
	if(!translations.hasOwnProperty('localized')) {
		return callback(errors.newError('lu-vdt-key-miss', {key: 'localized'}));
	}

	if(!check.object(translations.localized)) {
		return callback(errors.newError('lu-vdt-inv-val', {key: 'localized'}));
	}
	var keys = [];
	for(var l in translations.localized) {
		if(!exports.isValidLanguageCode(l)) {
			return callback(errors.newError('lu-vdt-inv-lang', {key: 'localized[' + l + ']'}));
		}

		if(!check.object(translations.localized[l])) {
			return callback(errors.newError('lu-vdt-inv-val', {key: 'localized[' + l + ']'}));
		}

		for(var k in translations.localized[l]){
			if(keys.indexOf(k) < 0){
				keys.push(k);
			}
		}
	}

	for(var lc in translations.localized) {
		for(var ki in keys){
			if( !translations.localized[lc].hasOwnProperty(keys[ki])){
				return callback(errors.newError('lu-vdt-key-miss', {key: 'localized[' + lc + '][' + keys[ki] + ']'}));
			}

			var val = translations.localized[lc][keys[ki]];

			if( !check.unemptyString(val) ){
				return callback(errors.newError('lu-vdt-inv-val', {key: 'localized[' + lc + '][' + keys[ki] + ']'}));
			}

			var re = new RegExp('(\\{|\\})', 'gi');
			if( re.test(val) ){
				try{
					var template = handlebars.compile(val, {strict: true});
					template();
				}catch(e){
					return callback(errors.newError('lu-vdt-inv-tpl', {key: 'localized[' + lc + '][' + keys[ki] + ']', originalError: e}));
				}
			}

		}
	}
	//all OK
	callback(null);
};

exports.validateDefaultTranslations = function(callback) {
	exports.validateTranslations(defaultTranslations, callback);
};

/**
 * Initialize this module immediately
 */
exports.init = function() {
	exports.loadLanguages();
	exports.loadCountries();
};

exports.init();
