'use strict';
var https = require('https');
var errors = require('../utils/errors');
var logging = require('../utils/logging');
var cryptoUtils = require('../utils/cryptoUtils');
var ftpUtils = require('../utils/ftpUtils');
var awsS3Utils = require('../utils/awsS3Utils');
var accessLogs = require('../reporting/accessLogs');
var path = require('path');
var q = require('q');

errors.defineError('lw-res-inv', 'System error', 'Invalid response from LeaseWeb.', 'Utils.leasewebSDK');
errors.defineError('lw-res-inv-frmt', 'System error', 'Invalid response formatting from LeaseWeb.', 'Utils.leasewebSDK');
errors.defineError('lw-query-inv', 'System error', 'Invalid request to LeaseWeb.', 'Utils.leasewebSDK');

function leasewebAuthDetails(path) {
	var time = Math.floor(new Date().getTime() / 1000);
	var hash = cryptoUtils.sha1(process.env.LEASEWEB_SECRET + time + path);
	return '/' + time + '/' + hash;
}

/**
 * Returns the subfolder of the access logs
 */
exports.getAccessLogsS3Folder = function () {
	return path.join('access_logs', process.env.PUBLIC_HOST);
};

function leasewebQuery(path, method, providedBy, body, callback, iM) {
	iM = iM || 0;
	var options = {
		hostname: 'api.leasewebcdn.com',
		path: path,
		headers: {
			'Content-Type': 'application/json'
		},
		method: method
	};
	if (body) {
		options.headers['Content-Length'] = body.length;
	}
	var req = https.request(options, function (res) {
		var response = '';

		res.setEncoding('utf8');
		res.on('data', function (chunk) {
			response += chunk;
		});
		res.on('end', function () {
			var err = null;
			try {
				if (res.statusCode === 200) {
					response = JSON.parse(response);
				} else {
					if (res.statusCode === 503 && ++iM < 10) {
						setTimeout(function () {
							leasewebQuery(path, method, providedBy, body, callback, iM);
						}, 100 * iM);
						return null;
					}
					err = errors.newError('lw-res-inv', {
						leasewebResponse: response,
						leasewebStatus: res.statusCode,
						providedBy: providedBy
					});
				}

			} catch (e) {
				return callback(errors.newError('lw-res-inv-frmt', {
					leasewebResponse: response,
					leasewebError: e,
					providedBy: providedBy
				}));
			}
			return callback(err, response);
		});
	});

	if (body) {
		req.write(body);
	}
	req.end();

	req.on('error', function (e) {
		return callback(errors.newError('lw-query-inv', {leasewebError: e.message}, e.code === 'ECONNRESET'));
	});
}


/**
 * Purge cache in leaseweb
 * @param urls
 * @param hgId
 * @param callback
 */
exports.purge = function (urls, callback) {
	var body = {
		urls: urls
	};
	body = body.urls ? JSON.stringify(body) : null;
	var purgePath = '/content/purge/' + process.env.LEASEWEB_CUSTOMER + '/' + process.env.LEASEWEB_ZONE;
	leasewebQuery(purgePath + leasewebAuthDetails(purgePath), 'POST', 'purge', body, function (err, res) {
		return callback(err, res);
	});
};

/**
 * Get the status of a purge job
 * @param jobId
 * @param callback
 */
exports.getPurgeStatus = function (jobId, callback) {
	var purgePath = '/content/purge/' + process.env.LEASEWEB_CUSTOMER + '/' + process.env.LEASEWEB_ZONE + '/' + jobId;
	leasewebQuery(purgePath + leasewebAuthDetails(purgePath), 'GET', 'purgeStatus', null, function (err, res) {
		return callback(err, res);
	});
};

/**
 * Warm up urls in leaseweb
 * @param urls
 * @param hgId
 * @param callback
 */
exports.warmUp = function (urls, hgId, callback) {
	var body = {
		urls: urls,
		safe: '/?=&'
	};
	body = body.urls ? JSON.stringify(body) : null;
	var warmUpPath = '/content/warmup/' + process.env.LEASEWEB_CUSTOMER + '/' + process.env.LEASEWEB_ZONE + '/http';
	leasewebQuery(warmUpPath + leasewebAuthDetails(warmUpPath), 'POST', 'warmUp', body, function (err, res) {
		callback(err, res);
	});
};

/**
 * Download access logs from Leaseweb FTP server into S3 bucket
 * @param callback
 */
exports.downloadAccessLogs = function (callback) {
	ftpUtils.createFtpConnection(
		{
			host: process.env.LEASEWEB_FTP_HOST,
			port: process.env.LEASEWEB_FTP_PORT,
			user: process.env.LEASEWEB_FTP_USER,
			password: process.env.LEASEWEB_FTP_PASS,
			secure: true

		},
		function (err, ftpConnection) {
			if (err) {
				return callback(err);
			}

			awsS3Utils.createAwsS3Connection(process.env.AWS_S3_BUCKET_ACCESS_LOGS, function (err, s3Connection) {
				if (err) {
					ftpConnection.end();
					return callback(err);
				}

				ftpUtils.copyFtpFilesToS3(ftpConnection, s3Connection, process.env.PUBLIC_HOST, exports.getAccessLogsS3Folder(), function (err, res) {
					ftpConnection.end();
					if (err) {
						return callback(err);
					}
					callback(null, res);

				}, function s3KeySuffixCallback(fileName) {
					//divide access logs into sub folders year/month/day
					var datePart = fileName.split('-').pop();
					var subFolders = [datePart.slice(0, 4), datePart.slice(4, 6), datePart.slice(6, 8)];
					return path.join.apply(path, subFolders);
				}, function (downloadedCount, totalCount) {
					logging.info('Access logs progress, downloading', downloadedCount, 'out of', totalCount);
				});

			});

		}
	);
};

/**
 * Return list of only unprocessed files access log files
 * @param s3Connection
 * @param callback
 */
exports.listNewAccessLogs = function (s3Connection, callback) {
	awsS3Utils.listAwsS3Files(s3Connection, exports.getAccessLogsS3Folder(), function (err, result) {
		if (err) {
			return callback(err);
		}
		var out = [];
		result.Contents.forEach(function (item) {
			if (item.Key.indexOf('/done/') < 0) {
				out.push(item);
			}
		});
		result = null;
		callback(null, out);
	});
};

/**
 *
 * @param s3Connection
 * @param s3Key
 * @param callback
 */
exports.importAccessLogFile = function (s3Connection, s3Key, callback) {
	awsS3Utils.downloadFileFromAwsS3(s3Connection, s3Key, function (err, dlRes) {
		if (err) {
			return callback(err);
		}
		var fileArr = dlRes.Key.split('/');
		var fileName = fileArr.pop();

		accessLogs.decompressAndImportAccessLogFile(dlRes.Body, fileName, function (err, impRes) {
			if (err) {
				return callback(err);
			}
			fileArr.push('done');
			var destination = path.join(fileArr.join('/'), fileName);
			awsS3Utils.moveFileInAwsS3(s3Connection, dlRes.Key, destination, function (err, mvRes) {
				if (err) {
					return callback(err);
				}
				callback(null, {importRes: impRes, moveRes: mvRes});
			});
		});

	});
};

/**
 * Import new access log files
 * @param callback
 */
exports.importNewAccessLogFiles = function (callback) {
	var s3Connection = awsS3Utils.createAwsS3Connection(process.env.AWS_S3_BUCKET_ACCESS_LOGS);
	exports.listNewAccessLogs(s3Connection, function (err, s3Files) {
		if (err) {
			return callback(err);
		}
		var promises = [];
		var results = [];

		function doImport(promise, s3File, i) {
			promises[i].finally(function () {
				exports.importAccessLogFile(s3Connection, s3File.Key, function (err, iRes) {
					results.push({s3Key: s3File.Key, error: err, importResult: iRes});
					logging.info('Imported access log file', ((i + 1) + '/' + (promises.length - 1)), s3File.Key, err);
					promise.resolve();
				});
			});
		}

		var p1 = q.defer();
		promises.push(p1.promise);
		s3Files.forEach(function (s3File, i) {
			var p = q.defer();
			promises.push(p.promise);
			doImport(p, s3File, i);
		});

		q.all(promises).done(function () {
			callback(null, results);
		}, callback);

		p1.resolve();
	});
};
