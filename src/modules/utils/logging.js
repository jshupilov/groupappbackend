'use strict';
var winston = require('winston');

var Papertrail = require('winston-papertrail').Papertrail;

function init(){
	var logger,
		logs,
		custom_levels = {
		silly: 0,
		debug: 1,
		notice: 2,
		info: 3,
		warning: 4,
		err: 5,
		error: 5,
		alert: 6,
		emerg: 7
	};

	if(process.env.PAPERTRAIL_HOST
		&& process.env.PAPERTRAIL_PORT){
		logs = new Papertrail(
			{
				host: process.env.PAPERTRAIL_HOST,
				port: process.env.PAPERTRAIL_PORT,
				hostname: process.env.APP_NAME
			});

		logs.on('error', function(err) {
			console.log('Cannot log to Papertrail', err, 'Logging to console', Object.keys(winston.transports), logger);
			logger.add(winston.transports.Console);
			logger.remove(logs);
		});
	} else {
		logs = new winston.transports.Console({level: 'debug'});
	}

	logger = new (winston.Logger)({
		levels: custom_levels,
		transports: [
			logs
		]
	});

	return logger;
}

module.exports = init();
module.exports.init = init;
