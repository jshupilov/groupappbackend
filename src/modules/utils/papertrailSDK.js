/*

 npm install request

 and place the following line in the dependencies of your package.json file.

 "request": "2.58.0"

 more information:
 request: https://github.com/request/request

 */
'use strict';
var https = require('https');
var errors = require('../utils/errors');
var check = require('check-types');
var url = require('url');

errors.defineError('pt-res-inv', 'System error', 'Invalid response from Papertrail.', 'Utils.papertrailSDK');
errors.defineError('pt-res-inv-frmt', 'System error', 'Invalid response formatting from Papertrail.', 'Utils.papertrailSDK');
errors.defineError('pt-query-inv', 'System error', 'Invalid request to Papertrail.', 'Utils.papertrailSDK');


function papertrailQuery(path, method, query, callback){

	var options = {
		hostname: 'papertrailapp.com',
		path: url.format({pathname: '/api/v1' + path, query: query}),
		headers: {
			'X-Papertrail-Token': process.env.PAPERTRAIL_APP_TOKEN,
			'Content-Type': 'application/json',
			'Cache-Control': 'no-cache'
		},
		method: method
	};
	var req = https.request(options, function (res) {
		var response = '';

		res.setEncoding('utf8');
		res.on('data', function(chunk){
			response += chunk;
		});
		res.on('end', function(){
			var err = null;
			try{
				if(res.statusCode === 200) {
					response = JSON.parse(response);
				} else {
					err = errors.newError('pt-res-inv', {papertrailError: response, paperTrailStatus: res.statusCode});
				}
			} catch (e) {
				return callback(errors.newError('pt-res-inv-frmt', {papertrailError: response, paperTrailStatus: res.statusCode}));
			}
			return callback(err, response);
		});
	});
	req.end();

	req.on('error', function(e) {
		return callback(errors.newError('pt-query-inv', {requestMessage: e.message}, e.code === 'ECONNRESET'));
	});

}


exports.search = function(word, callback, query) {
	query = query || {};
	if(!check.unemptyString(word)){
		return callback(errors.newError('pt-query-inv'));
	}
	query.q = word;
	papertrailQuery('/events/search.json', 'GET', query, function(err, json){
		if(err){
			return callback(err);
		}
		delete query.min_id;
		delete query.max_id;
		delete query.min_time;
		delete query.max_time;

		var response = {
			next: function (cb) {
				query.min_id = json.max_id;
				exports.search(word, cb, query);
			},
			prev: function (cb) {
				query.max_id = json.min_id;
				exports.search(word, cb, query);
			},
			Response: json
		};
		return callback(null, response);
	});
};
