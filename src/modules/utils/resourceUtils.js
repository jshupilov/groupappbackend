'use strict';

/**
 * Format resource for output
 * @param resource
 * @param apiRequest
 * @returns {*}
 */
exports.formatResource = function(resource, apiRequest) {

	resource.href = apiRequest.href;
	return resource;

};
