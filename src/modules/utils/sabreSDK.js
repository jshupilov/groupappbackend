/*

 npm install soap

 and place the following line in the dependencies of your package.json file.

 "soap": "0.9.1"

 more information:
 soap: https://www.npmjs.com/package/soap

 requires SABRESDK_USER_NAME and SABRESDK_USER_PASSWORD in .env file.

 */
'use strict';
var soap = require('soap');
var errors = require('../utils/errors');
var retry = 5;// retry X times
var deTimeOut = 1000;

errors.defineError('sb-call-inv', 'System error', 'SOAP client call faild', 'Utils.sabreSDK');
errors.defineError('sb-res-err', 'System error', 'SOAP Server returned error', 'Utils.sabreSDK');

/**
 * This function is made global for testing purposes.
 * Do not use it directly
 * @param funName string
 * @param attributes object
 * @param callback function
 * @param timeout int
 */
exports.soapCall = function(funName, attributes, callback, timeout, r) {
	r = r || 0;
	soap.createClient('test.wsdl', function(err, client) {
		if(err) {
			return callback(errors.newError('sb-call-inv', {originalError: err}));
		}
		client.addSoapHeader(
			{
				HTNGHeader: {
					From: {
						Credential: {
							userName: process.env.SABRESDK_USER_NAME,
							password: process.env.SABRESDK_USER_PASSWORD
						}
					}
				}
			},
			null,
			'h',
			'http://htng.org/1.1/Header/'
		);

		if(typeof (client[funName]) === 'function') {

			client[funName](attributes, function(err, result) {
				if(err) {
					if(['ETIMEDOUT', 'ESOCKETTIMEDOUT',	'ECONNRESET'].indexOf(err.code) > -1
						&& r++ <= retry
					) {

						return exports.soapCall(funName, attributes, callback, timeout, r);

					}
					callback(errors.newError('sb-res-err', {originalError: err.message, originalCode: err.code, info: 'too many retries'}));
				} else if(result.Errors) {
					callback(errors.newError('sb-res-err', {
						Errors: JSON.stringify(result.Errors),
						fullResult: JSON.stringify(result),
						info: 'error returned'
					}));
				}else{
					callback(null, result);
				}

			}, {timeout: timeout ? timeout : deTimeOut});

		} else {
			return callback(errors.newError('sb-call-inv'));
		}

	});
};

/**
 * Retrieve Hotel Reservations by passing identifying information.
 * @param userId string
 * @param password string
 * @param hotelCode string
 * @param chainCode string
 * @param callback function
 */
exports.getBookings = function(userId, password, hotelCode, chainCode, callback) {

	var attr = {
		HotelReadRequest: {
			attributes: {
				HotelCode: hotelCode,
				ChainCode: chainCode
			},
			UserID: {
				attributes: {
					ID: userId,
					PinNumber: password
				}
			}
		}
	};

	exports.soapCall('ReadReservations', attr, function(err, client) {
		if(err) {
			return callback(err);
		}
		return callback(null, client);
	});
};
