'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var apiUtils = require('../apiUtils');
var authUtils = require('../authUtils');
var commonServices = require('../../common/services');
var hgDao = require('../../hotel_groups/dao');
var check = require('check-types');
lab.experiment('Api utils hotel group id validation', function() {
	var f;

	lab.before(function(done){
		f = hgDao.checkIfActive;
		done();
	});

	lab.afterEach(function(done){
		hgDao.checkIfActive = f;
		done();
	});
	lab.test('should fail with 0', function(done) {
		apiUtils.validateHotelGroupId(0, function(err) {
			testingTools.expectError(err, 'au-inv-hgid');
			done();
		});
	});
	lab.test('should fail with ""', function(done) {
		apiUtils.validateHotelGroupId(undefined, function(err) {
			testingTools.expectError(err, 'au-inv-hgid');
			done();
		});
	});

	lab.test('should fail if err from db', function(done) {
		hgDao.checkIfActive = function(){
			arguments[arguments.length-1](new Error('asdasdadasd'));
		};
		apiUtils.validateHotelGroupId('asda', function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('asdasdadasd');
			done();
		});
	});

	lab.test('should fail with invalid id', function(done) {
		apiUtils.validateHotelGroupId('asdas2342dedasdsadsa', function(err) {
			testingTools.expectError(err, 'au-inv-hgid');
			done();
		});
	});

	lab.test('should pass with testing value', function(done) {
		apiUtils.validateHotelGroupId('test-hgid', function(err, hgid) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(hgid).to.be.string();
			done();
		});
	});
});


lab.experiment('Api utils api version validation', function() {
	lab.test('should fail with 0', function(done) {
		apiUtils.validateApiVersion(0, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});
	lab.test('should fail with undefined', function(done) {
		apiUtils.validateApiVersion(undefined, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});

	lab.test('should pass with v1', function(done) {
		apiUtils.validateApiVersion('v1', function(err, version) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(version).to.be.string();
			done();
		});
	});
});

lab.experiment('Api utils uuid validation', function() {

	lab.test('should fail with undefined uuid', function(done) {
		var req = apiUtils.createApiRequest('/smt', {}, {});

		apiUtils.validateUuid(req, function(err) {
			testingTools.expectError(err, 'au-no-uuid');
			done();
		});
	});

	lab.test('should pass with uuid', {timeout: 10000}, function(done) {
		testingTools.getUuid(function(err, uuid) {
			testingTools.code.expect(err).to.be.null();
			var req = apiUtils.createApiRequest('/smt', {'x-cardola-uuid': uuid}, {});
			apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

			apiUtils.validateUuid(req, function(err2, uuid2) {
				testingTools.code.expect(err2, JSON.stringify(err2)).to.be.null();
				testingTools.code.expect(uuid2).to.equal(uuid);
				done();
			});
		});

	});

	lab.test('should pass with registration', function(done) {
		var req = apiUtils.createApiRequest('/registration', {}, {});

		apiUtils.validateUuid(req, function(err, uuid) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(uuid).to.equal(undefined);
			done();
		});
	});
});

lab.experiment('apiUtils.validateUuid', function() {
	var f;
	lab.before(function(done){
		f = commonServices.validateUuid;
		commonServices.validateUuid = function(uuid, hgId, cb){
			cb(null, false);
		};
		done();
	});

	lab.after(function(done){
		commonServices.validateUuid = f;
		done();
	});

	lab.test('should fail if validation sais so', function(done) {
		var req = apiUtils.createApiRequest('/smt', {'x-carola-uuid': 'asdasdsda'}, {});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), 'asdsadda');
		apiUtils.validateUuid(req, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});
});

lab.experiment('apiUtils.validateUuid', function() {
	var cc;
	lab.before(function(done) {
		cc = process.env.DATABASE_URL;
		process.env.DATABASE_URL = 'none';
		done();
	});
	lab.after(function(done) {
		process.env.DATABASE_URL = cc;
		done();

	});
	lab.test('should fail with no DB connection', function(done) {
		var req = apiUtils.createApiRequest('/some/req', {'x-cardola-uuid': 'some-uuid'}, {});
		apiUtils.validateUuid(req, function(err) {
			testingTools.code.expect(err).to.be.object();

			done();
		});
	});
});

lab.experiment('Api utils api call validation', function() {

	lab.test('should pass with correct url and headers', function(done) {
		testingTools.getUuid(function(err, uuid) {
			testingTools.code.expect(err).to.be.null();
			var req = apiUtils.createApiRequest('/v1/test-hgid/smt', {'x-cardola-uuid': uuid}, {});
			apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
			apiUtils.validateApiCall(req, function(err2) {
				testingTools.code.expect(err2, JSON.stringify(err2)).to.be.null();
				done();
			});
		});

	});

	lab.test('should fail with incorrect url', function(done) {

		var req = apiUtils.createApiRequest('/v1/smt', {}, {});

		apiUtils.validateApiCall(req, function(err) {
			testingTools.expectError(err, 'au-inv-path');
			done();
		});
	});

	lab.test('should fail with incorrect url', function(done) {

		var req = apiUtils.createApiRequest('/v1//smt', {}, {});

		apiUtils.validateApiCall(req, function(err) {
			testingTools.expectError(err, 'au-inv-hgid');
			done();
		});
	});

	lab.test('should fail with invalid version', function(done) {

		var req = apiUtils.createApiRequest('/v-invalid/test-hgid/smt', {}, {});

		apiUtils.validateApiCall(req, function(err) {
			testingTools.expectError(err, 'au-inv-ver');
			done();
		});
	});


	lab.test('should not require uuid for registration', function(done) {

		var req = apiUtils.createApiRequest('/v1/test-hgid/registration', {}, {});

		apiUtils.validateApiCall(req, function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should not validate for OPTIONS call', function(done) {

		var req = apiUtils.createApiRequest('/v1/test-hgid/registration', {}, {}, 'OPTIONS');

		apiUtils.validateApiCall(req, function(err) {
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

});

lab.experiment('ApiUtils getServiceHref', function() {
	lab.test('Should work with custom port', function(done) {
		var oldP = process.env.PUBLIC_PORT;
		process.env.PUBLIC_PORT = 8000;
		testingTools.code.expect(apiUtils.getServiceHref(1, '/test', null)).to.include(['/test', '1', ':8000']);
		process.env.PUBLIC_PORT = oldP;
		done();
	});

});
lab.experiment('ApiUtils getServiceHrefWithRequest', function() {
	lab.test('Should throw without request(undefined)', function(done) {
		var t = function() {
			apiUtils.getServiceHrefWithRequest(undefined, '/test');
		};
		testingTools.code.expect(t).to.throw();
		done();
	});
	lab.test('Should throw without request(null)', function(done) {
		var t = function() {
			apiUtils.getServiceHrefWithRequest(null, '/test');
		};
		testingTools.code.expect(t).to.throw();

		done();
	});
	lab.test('Should throw with one param language as string from object request', function(done) {
		try {
			apiUtils.getServiceHrefWithRequest({languages: 'eng'}, '/test');
			testingTools.code.expect(1).to.equal(2);//will fail
		} catch(e) {
			testingTools.code.expect(e).to.be.instanceof(Error);
		}
		done();
	});
	lab.test('Should throw with one param language as array from object request', function(done) {
		try {
			apiUtils.getServiceHrefWithRequest({languages: ['eng']}, '/test');
			testingTools.code.expect(1).to.equal(2);//will fail
		} catch(e) {
			testingTools.code.expect(e).to.be.instanceof(Error);
		}
		done();
	});

	lab.test('Should work with single query param', function(done) {
		testingTools.code.expect(
			apiUtils.getServiceHrefWithRequest({hotelGroupId: 1, languages: []}, '/test', {bla: 'foo'})
		).to.include(['/test', '1', '?bla=foo']);
		done();
	});

	lab.test('Should encode query params', function(done) {
		testingTools.code.expect(
			apiUtils.getServiceHrefWithRequest({hotelGroupId: 1, languages: []}, '/test', {'bla 2': 'foo 3'})
		).to.include(['/test', '1', '?bla%202=foo%203']);
		done();
	});

	lab.test('Should work with two query param', function(done) {
		testingTools.code.expect(
			apiUtils.getServiceHrefWithRequest({hotelGroupId: 1, languages: []}, '/test', {bla: 'foo', bar: 'asd'})
		).to.include(['/test', '1', '?bla=foo&bar=asd']);
		done();
	});

	lab.test('Should work with no query param', function(done) {
		var href = apiUtils.getServiceHrefWithRequest({hotelGroupId: 1, languages: []}, '/test', {});
		testingTools.code.expect(href).to.include(['/test', '1']);
		testingTools.code.expect(href).not.to.include(['?', '&']);
		done();
	});

	lab.test('Should throw with invalid queryParams arg', function(done) {
		try {
			apiUtils.getServiceHrefWithRequest({hotelGroupId: 1, languages: []}, '/test', ['a=2', 'b=3']);
			testingTools.code.expect(1).to.equal(2);//will fail
		} catch(e) {
			testingTools.code.expect(e).to.be.instanceof(Error);
		}
		done();
	});

	lab.test('Should throw with invalid queryParams arg', function(done) {
		try {
			apiUtils.getServiceHrefWithRequest({hotelGroupId: 1, languages: []}, '/test', '?a=1');
			testingTools.code.expect(1).to.equal(2);//will fail
		} catch(e) {
			testingTools.code.expect(e).to.be.instanceof(Error);
		}
		done();
	});
});

lab.experiment('ApiUtils getServiceLink', function() {
	lab.test('should throw with invalid method', function(done) {
		var thrower = function() {
			apiUtils.getServiceLink('testhgid', 'some/method', {}, 'HEAD');
		};
		testingTools.code.expect(thrower).to.throw();
		done();
	});

	lab.test('should work with POST', function(done) {
		var href = apiUtils.getServiceLink('testhgid', 'some/method', {}, 'POST');
		testingTools.code.expect(href).to.be.object();
		testingTools.code.expect(href).to.include(['href', 'method']);
		testingTools.code.expect(href.href).to.include(['some/method']);
		testingTools.code.expect(href.method).to.include('POST');
		done();
	});

	lab.test('should work with default GET', function(done) {
		var href = apiUtils.getServiceLink('testhgid', 'some/method', {});
		testingTools.code.expect(href).to.be.object();
		testingTools.code.expect(href).to.include(['href']);
		testingTools.code.expect(href).to.not.include(['method']);
		testingTools.code.expect(href.href).to.include(['some/method']);
		done();
	});

});

lab.experiment('ApiUtils getServiceLinkWithRequest', function() {
	lab.test('should work', function(done) {
		var thrower = function() {
			return apiUtils.getServiceLinkWithRequest({hotelGroupId: 'testhgid'}, 'some/method', {});
		};
		testingTools.code.expect(thrower).not.to.throw();
		testingTools.code.expect(thrower()).to.be.object();
		done();
	});

});

lab.test('getCurrentHrefFromHapiRequest', function(done) {

	var p = '/custom/path';
	var u = apiUtils.getCurrentHrefFromHapiRequest({raw: {req: {url: p}}});
	testingTools.code.expect(u).to.include([p]);
	done();

});

lab.experiment('ApiUtils populateApiRequest', function() {
	lab.test('should work', function(done) {
		[{}, {offset: null, limit: null}, {offset: 'ads', limit: 'asd'}].forEach(function(q) {
			var res = {query: q, headers: {}};
			apiUtils.populateApiRequest(res, 'v1', 'test-hgid', 'test-uuid');
			testingTools.code.expect(res.apiVersion).to.equal('v1');
			testingTools.code.expect(res.hotelGroupId).to.equal('test-hgid');
			testingTools.code.expect(res.uuid).to.equal('test-uuid');
		});
		done();
	});

});

lab.experiment('ApiUtils populateApiRequestFromExisting', function() {
	lab.test('should work', function(done) {
		var ex = {headers: {}};
		var newReq = {headers: {}};
		apiUtils.populateApiRequest(ex, 'v1', 'test-hgid', 'test-uuid');
		apiUtils.populateApiRequestFromExisting(newReq, ex);
		testingTools.code.expect(newReq).to.deep.equal(ex);

		done();
	});

});

lab.experiment('ApiUtils getServiceHref', function() {
	lab.test('should remove base path', function(done) {

		var res = apiUtils.getServiceHref('test-hgid', '/v1/test-hgid/app/init');
		testingTools.code.expect(res.indexOf('v1/test-hgid')).equal(res.lastIndexOf('v1/test-hgid'));

		done();
	});

	lab.test('should keep tags', function(done) {

		var res = apiUtils.getServiceHref('test-hgid', '/v1/test-hgid/app/smt', {action: '<action>'});
		testingTools.code.expect(res).to.include(['<action>']);

		done();
	});

});

lab.experiment('ApiUtils getModifiedCurrentHref', function() {
	lab.test('should modify query', function(done) {
		var req = apiUtils.createApiRequest('test/path?action=1', {});
		var reqM = apiUtils.getModifiedCurrentHref(req, {action: 2});
		testingTools.code.expect(reqM).to.include(['action=2']);

		done();
	});
});

lab.experiment('ApiUtils getModifiedCurrentLink', function() {
	lab.test('should modify query', function(done) {
		var req = apiUtils.createApiRequest('test/path?action=1', {});
		var reqM = apiUtils.getModifiedCurrentLink(req, {action: 2});
		testingTools.code.expect(reqM.href).to.include(['action=2']);

		done();
	});
});

lab.experiment('ApiUtils createUrl', function() {

	lab.test('should add env values', function(done) {
		var req = apiUtils.createUrl({});
		testingTools.code.expect(req).to.include([process.env.PUBLIC_PROTOCOL, process.env.PUBLIC_HOST]);

		done();
	});

	lab.test('should not add env values', function(done) {
		var req = apiUtils.createUrl({protocol: 'https', host: 'wonderland'});
		testingTools.code.expect(req).to.not.include([process.env.PUBLIC_PROTOCOL, process.env.PUBLIC_HOST]);
		testingTools.code.expect(req).to.include(['https', 'wonderland']);

		done();
	});

	lab.test('should add port', function(done) {
		var oldP = process.env.PUBLIC_PORT;
		process.env.PUBLIC_PORT = 8000;
		var req = apiUtils.createUrl({protocol: 'https', host: 'wonderland'});
		process.env.PUBLIC_PORT = oldP;
		testingTools.code.expect(req).to.include([':8000']);
		done();
	});

	lab.test('should not add port 80', function(done) {
		var oldP = process.env.PUBLIC_PORT;
		process.env.PUBLIC_PORT = 80;
		var req = apiUtils.createUrl({protocol: 'https', host: 'wonderland'});
		process.env.PUBLIC_PORT = oldP;
		testingTools.code.expect(req).to.not.include([':80']);
		done();
	});
});

lab.experiment('ApiUtils getBaseUrl', function() {
	lab.test('should work', function(done) {
		var url = apiUtils.getBaseUrl();
		testingTools.code.expect(url).to.include([process.env.PUBLIC_PROTOCOL, process.env.PUBLIC_HOST]);

		var oldP = process.env.PUBLIC_PORT;
		process.env.PUBLIC_PORT = 8000;
		url = apiUtils.getBaseUrl();
		process.env.PUBLIC_PORT = oldP;
		testingTools.code.expect(url).to.include([':8000']);

		process.env.PUBLIC_PORT = 80;
		url = apiUtils.getBaseUrl();
		process.env.PUBLIC_PORT = oldP;
		testingTools.code.expect(url).not.to.include([':80']);

		done();
	});
});

lab.experiment('apiUtils.createApiRequest', function() {
	lab.test('should work', function(done) {
		var args = ['/href', {'x-forwarded-for': '12.12.12.12,', 'lswcdn-forwarded-for': '123.123.123.123,'}, {}, 'GET', {}, {remoteAddress: '1.2.3.4'}];
		var t = function() {
			return apiUtils.createApiRequest.apply(apiUtils, args);
		};
		testingTools.code.expect(t).not.to.throw();
		var res = t();
		testingTools.code.expect(res).to.include([
			'href',
			'query',
			'urlObj',
			'headers',
			'params',
			'method',
			'body',
			'collectionQuery',
			'remoteAddress'
		]);

		testingTools.code.expect(res.href).to.equal(args[0]);
		testingTools.code.expect(res.headers).to.equal(args[1]);
		testingTools.code.expect(res.params).to.equal(args[2]);
		testingTools.code.expect(res.method).to.equal(args[3]);
		testingTools.code.expect(res.body).to.equal(args[4]);
		testingTools.code.expect(res.collectionQuery).to.include(['offset', 'limit']);
		testingTools.code.expect(res.collectionQuery.offset).to.equal(0);
		testingTools.code.expect(res.collectionQuery.limit).to.equal(1000);
		testingTools.code.expect(res.remoteAddress).to.equal('123.123.123.123');

		done();
	});

	lab.test('should validate method', function(done) {
		var t = function() {
			apiUtils.createApiRequest('/href', {}, {}, 'GETS', {});
		};
		testingTools.expectError(t, 'au-inv-req-method');
		done();
	});

	lab.test('should validate missing content-type', function(done) {
		var t = function() {
			apiUtils.createApiRequest('/href', {}, {}, 'GET', '{"bla": 1}');
		};
		testingTools.expectError(t, 'au-inv-req-ct');
		done();
	});
	lab.test('should validate invalid content-type', function(done) {
		var t = function() {
			apiUtils.createApiRequest('/href', {'content-type': 'bla'}, {}, 'GET', '{"bla": 1}');
		};
		testingTools.expectError(t, 'au-inv-req-ct');
		done();
	});

	lab.test('should validate invalid json body', function(done) {
		var t = function() {
			apiUtils.createApiRequest('/href', {'content-type': 'application/json'}, {}, 'GET', '{"bla": [1}');
		};
		testingTools.expectError(t, 'au-inv-req-bd');
		done();
	});

	lab.test('should parse query', function(done) {
		var t = function() {
			return apiUtils.createApiRequest('/href?offset=1&limit=2', {}, {}, 'GET', {});
		};
		testingTools.code.expect(t).not.to.throw();
		var res = t();
		testingTools.code.expect(res.collectionQuery.offset).to.equal(1);
		testingTools.code.expect(res.collectionQuery.limit).to.equal(2);
		done();
	});

	lab.test('should not parse invalid query', function(done) {
		var t = function() {
			return apiUtils.createApiRequest('/href?offset=a&limit=b', {}, {}, 'GET', {});
		};
		testingTools.code.expect(t).not.to.throw();
		var res = t();
		testingTools.code.expect(res.collectionQuery.offset).to.equal(0);
		testingTools.code.expect(res.collectionQuery.limit).to.equal(1000);
		done();
	});

	lab.test('should fail with with invalid lat + long or radius 1', function(done) {

		var err = function(){
			apiUtils.createApiRequest('some/href?lat=-320.33&long=180&radius=1', {}, {}, 'GET', {});
		};
		err = testingTools.expectError(err, 'au-inv-loc-inp');
		testingTools.code.expect(err.data.debuggingData.invalidParam).to.equal('lat');
		done();
	});

	lab.test('should fail with with invalid lat + long or radius 2', function(done) {

		var err = function(){
			apiUtils.createApiRequest('some/href?lat=320.33&long=180&radius=1', {}, {}, 'GET', {});
		};
		err = testingTools.expectError(err, 'au-inv-loc-inp');
		testingTools.code.expect(err.data.debuggingData.invalidParam).to.equal('lat');
		done();

	});

	lab.test('should fail with with invalid lat + long or radius 3', function(done) {
		var err = function(){
			apiUtils.createApiRequest('some/href?lat=zero&long=180&radius=1', {}, {}, 'GET', {});
		};
		err = testingTools.expectError(err, 'au-inv-loc-inp');
		testingTools.code.expect(err.data.debuggingData.invalidParam).to.equal('lat');
		done();

	});

	lab.test('should fail with with  lat, invalid long + radius 1', function(done) {
		var err = function(){
			apiUtils.createApiRequest('some/href?lat=20.33&long=180.0001&radius=1', {}, {}, 'GET', {});
		};
		err = testingTools.expectError(err, 'au-inv-loc-inp');
		testingTools.code.expect(err.data.debuggingData.invalidParam).to.equal('long');
		done();
	});

	lab.test('should fail with with  lat, invalid long + radius 2', function(done) {
		var err = function(){
			apiUtils.createApiRequest('some/href?lat=20.33&long=-180.0001&radius=1', {}, {}, 'GET', {});
		};
		err = testingTools.expectError(err, 'au-inv-loc-inp');
		testingTools.code.expect(err.data.debuggingData.invalidParam).to.equal('long');
		done();
	});

	lab.test('should fail with with  lat, invalid long + radius 3', function(done) {
		var err = function(){
			apiUtils.createApiRequest('some/href?lat=20.33&long=dgf&radius=1', {}, {}, 'GET', {});
		};
		err = testingTools.expectError(err, 'au-inv-loc-inp');
		testingTools.code.expect(err.data.debuggingData.invalidParam).to.equal('long');
		done();
	});

	lab.test('should fail with with  lat, long + invalid radius 1', function(done) {
		var err = function(){
			apiUtils.createApiRequest('some/href?lat=20.33&long=11.4&radius=0', {}, {}, 'GET', {});
		};
		err = testingTools.expectError(err, 'au-inv-loc-inp');
		testingTools.code.expect(err.data.debuggingData.invalidParam).to.equal('radius');
		done();
	});

	lab.test('should fail with with  lat, long + invalid radius 2', function(done) {
		var err = function(){
			apiUtils.createApiRequest('some/href?lat=20.33&long=11.4&radius=bla', {}, {}, 'GET', {});
		};
		err = testingTools.expectError(err, 'au-inv-loc-inp');
		testingTools.code.expect(err.data.debuggingData.invalidParam).to.equal('radius');
		done();
	});

	lab.test('should NOT fail with with  lat, long + valid radius 200', function(done) {
		var err = function(){
			apiUtils.createApiRequest('some/href?lat=20.33&long=11.4&radius=200', {}, {}, 'GET', {});
		};
		testingTools.code.expect(err).not.to.throw();
		done();
	});

	lab.test('should work without radius', function(done) {
		var err = function(){
			apiUtils.createApiRequest('some/href?lat=20.33&long=11.4', {}, {}, 'GET', {});
		};
		testingTools.code.expect(err).not.to.throw();
		done();
	});


	lab.test('should fail with with partial set of (+long, lat, +radius) 2', function(done) {

		var err = function(){
			apiUtils.createApiRequest('some/href?radius=22&long=180', {}, {}, 'GET', {});
		};
		err = testingTools.expectError(err, 'au-inv-loc-inp');
		testingTools.code.expect(err.data.debuggingData).to.include(['missingSomeParams']);
		done();
	});

	lab.test('should fail with with partial set of (long, +lat, +radius) 3', function(done) {

		var err = function(){
			apiUtils.createApiRequest('some/href?radius=22&lat=180', {}, {}, 'GET', {});
		};
		err = testingTools.expectError(err, 'au-inv-loc-inp');
		testingTools.code.expect(err.data.debuggingData).to.include(['missingSomeParams']);
		done();
	});

});

lab.experiment('apiUtils.createApiRequestFromExisting', function() {
	lab.test('should work', function(done) {
		var req = apiUtils.createApiRequest('/some/href', {}, {}, null);
		var h = {'some-head': 'come-val'};
		var t = function() {
			return apiUtils.createApiRequestFromExisting('/other/href', h, req);
		};
		testingTools.code.expect(t).not.to.throw();
		var newR = t();
		testingTools.code.expect(newR.href).to.equal('/other/href');
		testingTools.code.expect(newR.headers).to.equal(h);
		done();
	});
});

lab.experiment('apiUtils.ensureSecurity', function() {
	var uuid, tc;

	lab.before(function(done){
		testingTools.getUuid(function(e, u){
			testingTools.code.expect(e).to.be.null();
			uuid = u;
		});
		tc = process.env.TEST_CHECKSUM;
		done();
	});

	lab.beforeEach(function(done){
		process.env.TEST_CHECKSUM = null;
		done();
	});

	lab.after(function(done){
		process.env.TEST_CHECKSUM = tc;
		done();
	});

	lab.test('should node require anything with level 0', function(done) {
		var req = apiUtils.createApiRequest('https://localhost:8000/some/path', {}, {}, null);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId());
		apiUtils.ensureSecurity(0, req, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});

	});

	lab.test('should require use of x-cardola-uuid with level 1', function(done) {
		var req = apiUtils.createApiRequest('https://localhost:8000/some/path', {}, {}, null);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId());
		apiUtils.ensureSecurity(1, req, function(err){
			testingTools.expectError(err, 'au-no-uuid');
			done();
		});

	});

	lab.test('should work with x-cardola-uuid on level 1', function(done) {
		var req = apiUtils.createApiRequest('https://localhost:8000/some/path', {}, {}, null);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
		apiUtils.ensureSecurity(1, req, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});

	});

	lab.test('should require use of https with level 2', function(done) {
		var req = apiUtils.createApiRequest('http://localhost:8000/some/path', {}, {}, null);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
		apiUtils.ensureSecurity(2, req, function(err){
			testingTools.expectError(err, 'au-https-req');
			done();
		});

	});

	lab.test('should work with https and x-cardola-uuid on level 2', function(done) {
		var req = apiUtils.createApiRequest('https://localhost:8000/some/path', {}, {}, null);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
		apiUtils.ensureSecurity(2, req, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});

	});

	lab.test('should require use of x-cardola-verify with level 3', function(done) {
		var req = apiUtils.createApiRequest('https://localhost:8000/some/path', {}, {}, null);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
		apiUtils.ensureSecurity(3, req, function(err){
			testingTools.expectError(err, 'au-ver-req');
			done();
		});

	});

	lab.test('should require valid x-cardola-verify', function(done) {
		var req = apiUtils.createApiRequest('https://localhost:8000/some/path', {'x-cardola-verify': 'aasdasdasdasdasd'}, {}, null);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
		apiUtils.ensureSecurity(3, req, function(err){
			testingTools.expectError(err, 'au-inv-verifykey');
			done();
		});
	});

	lab.test('should work with https and valid x-cardola-verify', function(done) {
		testingTools.getUuid(function(errU, uuid){
			testingTools.code.expect(errU).to.be.null();

			var req = apiUtils.createApiRequest('https://localhost:8000/some/path', {}, {}, null);
			apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
			var hash = authUtils.getVerifyKey(req, testingTools.getApiKey());
			delete process.env.TEST_CHECKSUM;
			req.headers['x-cardola-verify'] = hash;
			apiUtils.ensureSecurity(3, req, function(err){
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				done();
			});
		});

	});

	lab.test('should work with https and a test x-cardola-verify', function(done) {
		testingTools.getUuid(function(errU, uuid){
			testingTools.code.expect(errU).to.be.null();

			var req = apiUtils.createApiRequest('https://localhost:8000/some/path', {}, {}, null);
			apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
			process.env.TEST_CHECKSUM = 'some_string';
			req.headers['x-cardola-verify'] = 'some_string';
			apiUtils.ensureSecurity(3, req, function(err, res){
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(res).to.equal('some_string');
				done();
			});
		});

	});

	lab.test('should fail with https and and invalid test x-cardola-verify', function(done) {
		testingTools.getUuid(function(errU, uuid){
			testingTools.code.expect(errU).to.be.null();

			var req = apiUtils.createApiRequest('https://localhost:8000/some/path', {}, {}, null);
			apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
			process.env.TEST_CHECKSUM = 'some_string';
			req.headers['x-cardola-verify'] = 'some_other_string';
			apiUtils.ensureSecurity(3, req, function(err){
				testingTools.expectError(err);
				done();
			});
		});

	});

	lab.test('should work with https and valid x-cardola-verify on level 4', {timeout: 2000}, function(done) {
		testingTools.getUuid(function(errU, uuid){
			testingTools.code.expect(errU).to.be.null();

			var req = apiUtils.createApiRequest('https://localhost:8000/some/path', {}, {}, null);
			apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
			var hash = authUtils.getVerifyKey(req, testingTools.getApiKey());
			delete process.env.TEST_CHECKSUM;
			req.headers['x-cardola-verify'] = hash;
			var time = new Date().getTime();
			apiUtils.ensureSecurity(4, req, function(err){
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect( (new Date().getTime()) - time).to.be.above(500);
				done();
			});
		});

	});

	lab.test('should work with https and a test x-cardola-verify on level 4', {timeout: 2000}, function(done) {
		testingTools.getUuid(function(errU, uuid){
			testingTools.code.expect(errU).to.be.null();

			var req = apiUtils.createApiRequest('https://localhost:8000/some/path', {}, {}, null);
			apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
			process.env.TEST_CHECKSUM = 'some_string';
			req.headers['x-cardola-verify'] = 'some_string';
			var time = new Date().getTime();
			apiUtils.ensureSecurity(4, req, function(err){
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect( (new Date().getTime()) - time).to.be.above(500);
				done();
			});
		});

	});


});

lab.experiment('ApiUtils.getCacheConfigurations', function() {
	lab.test('should return a correct object', function(done) {
		var conf = apiUtils.getCacheConfigurations();
		testingTools.code.expect(conf).to.be.object();
		var cacheAbility = ['public', 'private', 'no-cache'];
		for(var ct in conf){
			testingTools.code.expect(conf[ct]).to.be.object();

			//cache on or off
			testingTools.code.expect(conf[ct]).to.include('Cache-Control');
			testingTools.code.expect(conf[ct]['Cache-Control']).to.be.object();
			testingTools.code.expect(conf[ct]['Cache-Control']).to.include('flags');
			testingTools.code.expect(conf[ct]['Cache-Control'].flags).to.be.array();
			testingTools.code.expect(conf[ct]['Cache-Control'].flags).to.part.include(cacheAbility);

			//cache time for CDN
			testingTools.code.expect(conf[ct]['Cache-Control']).to.include('s-maxage');
			testingTools.code.expect(check.integer(conf[ct]['Cache-Control']['s-maxage'])).to.equal(true);
			/**
			 * 31536000 (1 year) is max
			 * We can allow any values here really, because we can reset this data
			 */
			testingTools.code.expect(conf[ct]['Cache-Control']['s-maxage']).to.be.within(0, 31536000);

			//cache time for end client/browser
			testingTools.code.expect(conf[ct]['Cache-Control']).to.include('max-age');
			testingTools.code.expect(check.integer(conf[ct]['Cache-Control']['max-age'])).to.equal(true);
			/**
			 * 31536000 (1 year) is max,
			 * lets not allow values above 24 hours, since we cannot reset the private cache.
			 * 24 * 60 * 60 = 86 400
			 */
			testingTools.code.expect(conf[ct]['Cache-Control']['max-age']).to.be.within(0, 86400);

		}
		done();
	});
});

lab.experiment('ApiUtils.addCachingHeaders', function() {
	lab.test('should add caching headers', function(done) {
		var headers = {};
		var cacheType = 'default';
		apiUtils.addCachingHeaders(
			{
				header: function(key, val){
					headers[key] = val;
				},
				etag: function(val){
					headers.etag = val;
				},
				source: {}
			}
		);
		var cacheHeaders = apiUtils.getCacheConfigurations()[cacheType];
		for(var ci in cacheHeaders){
			testingTools.code.expect(headers).to.include(ci);
			for( var hi in cacheHeaders[ci]){
				if(hi === 'flags'){
					testingTools.code.expect(headers[ci]).to.include(cacheHeaders[ci][hi]);
				}else{
					testingTools.code.expect(headers[ci]).to.include(hi + '=' + cacheHeaders[ci][hi]);
				}
			}
		}

		done();
	});


	lab.test('should NOT add caching headers to error response', function(done) {
		var headers = {};
		var replyInterface = apiUtils.addCachingHeaders(
			{
				header: function(key, val){
					headers[key] = val;
				},
				isBoom: true
			}
		);
		replyInterface.header('some', 'header');
		testingTools.code.expect(Object.keys(headers).length).to.equal(1);

		done();
	});

	lab.test('should give error if invalid cacheType given', function(done) {

		var t = function() {
			apiUtils.addCachingHeaders(
				{
					statusCode: 200
				}
			, 'asdasd');
		};
		testingTools.expectError(t, 'au-inv-cache');

		done();
	});

	lab.test('should give error if no body', function(done) {

		var t = function() {
			apiUtils.addCachingHeaders(
				{
					statusCode: 200,
					header: function(){}
				}
			);
		};
		testingTools.expectError(t, 'au-err-etag');

		done();
	});
});

lab.experiment('ApiUtils populateMenu', function() {
	var menu, condition, keys, vals, name, refresh, obj;

	lab.before(function(done){
		refresh = function() {
			menu = [];
			condition = true;
			obj = {
				action: 'test-action',
				removeIfDisabled: false,
				alwaysDisabled: true

			};
			keys = ['test-keys'];
			vals = ['test-vals'];
			name = 'test-name';
		};
		refresh();
		done();
	});

	lab.afterEach(function(done){
		refresh();
		done();
	});

	lab.test('should work if condition positive', function(done) {

		var item = apiUtils.populateMenu(obj, condition, keys, vals, menu, name);

		testingTools.code.expect(menu).to.be.an.array().and.have.length(1);
		testingTools.code.expect(item).to.be.an.object();
		testingTools.code.expect(item).to.be.include(['action', 'isEnabled', 'name', 'data']);
		testingTools.code.expect(item.action).to.be.string().and.to.be.equal(obj.action);
		testingTools.code.expect(item.name).to.be.string().and.to.be.equal(name);
		testingTools.code.expect(item.isEnabled).to.be.equal(false);
		testingTools.code.expect(item.data).to.be.an.object().and.to.be.include(['test-keys']);
		testingTools.code.expect(item.data['test-keys']).to.be.equal('test-vals');
		testingTools.code.expect(menu[0]).to.be.an.object().and.to.be.equal(item);
		done();
	});
	lab.test('should work if condition positive and alwaysDisabled is false', function(done) {

		obj.alwaysDisabled = false;

		var item = apiUtils.populateMenu(obj, condition, keys, vals, menu, name);

		testingTools.code.expect(menu).to.be.an.array().and.have.length(1);
		testingTools.code.expect(item).to.be.an.object();
		testingTools.code.expect(item).to.be.include(['action', 'isEnabled', 'name', 'data']);
		testingTools.code.expect(item.action).to.be.string().and.to.be.equal(obj.action);
		testingTools.code.expect(item.name).to.be.string().and.to.be.equal(name);
		testingTools.code.expect(item.isEnabled).and.to.be.equal(true);
		testingTools.code.expect(item.data).to.be.an.object();
		done();
	});
	lab.test('should work if condition negative and removeIfDisabled is true', function(done) {

		condition = false;

		var item = apiUtils.populateMenu(obj, condition, keys, vals, menu, name);

		testingTools.code.expect(menu).to.be.an.array().and.have.length(1);
		testingTools.code.expect(item).to.be.an.object();
		testingTools.code.expect(item).to.be.include(['action', 'isEnabled', 'name', 'data']);
		testingTools.code.expect(item.action).to.be.string().and.to.be.equal(obj.action);
		testingTools.code.expect(item.name).to.be.string().and.to.be.equal(name);
		testingTools.code.expect(item.isEnabled).to.be.equal(false);
		testingTools.code.expect(item.data).to.be.an.object();
		done();
	});
	lab.test('should work if condition negative and removeIfDisabled is false', function(done) {

		condition = false;
		obj.removeIfDisabled = true;

		var item = apiUtils.populateMenu(obj, condition, keys, vals, menu, name);
		testingTools.code.expect(menu).to.be.an.array().and.have.length(0);
		testingTools.code.expect(item).to.be.an.object();
		testingTools.code.expect(item).to.be.include(['action', 'isEnabled', 'name', 'data']);
		testingTools.code.expect(item.action).to.be.string().and.to.be.equal(obj.action);
		testingTools.code.expect(item.name).to.be.string().and.to.be.equal(name);
		testingTools.code.expect(item.isEnabled).to.be.boolean().and.to.be.equal(true);
		testingTools.code.expect(item.data).to.be.an.object().and.to.be.not.include(['test-keys']);
		done();
	});
});
