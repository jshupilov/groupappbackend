'use strict';
var testingTools = require('../../../test/testingTools');
var hgServices = require('../../hotel_groups/services');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var authUtils = require('../authUtils');
var apiUtils = require('../apiUtils');
var databaseUtils = require('../../utils/databaseUtils');

lab.experiment('Authentication utils verify key validation', function() {
	lab.test('should fail with empty request', function(done) {
		authUtils.validateAuthentication({headers: {}}, function(err) {
			testingTools.expectError(err, 'au-len-verifykey');
			done();
		});
	});

	lab.test('should fail with short verify key', function(done) {
		authUtils.validateAuthentication({headers: {'x-cardola-verify': 'testkey'}}, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});
	lab.test('should fail with incorrect verify key', function(done) {
		authUtils.validateAuthentication({headers: {'x-cardola-verify': 'testlongkey'}}, function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});

	lab.test('should work with body', function(done) {
		var req = apiUtils.createApiRequest('/some/url', {'x-cardola-verify': 'somerandomstuffhere'}, {}, null, {some: 'data'});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId());

		authUtils.validateAuthentication(req, function(err) {
			testingTools.expectError(err, 'au-inv-verifykey');
			done();
		});
	});

	lab.test('should validate', function(done) {
		testingTools.getUuid(function(err, uuid) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(uuid).not.to.be.null();
			var req = apiUtils.createApiRequest('/some/url?offset=12', {'x-cardola-uuid': uuid});
			apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
			req.headers['x-cardola-verify'] = authUtils.getVerifyKey(req, testingTools.getApiKey());
			authUtils.validateAuthentication(req, function(err) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				done();
			});
		});
	});


});

lab.experiment('authUtils.validateAuthentication', function() {
	var uuid, hotelGroupRow, oldValidKeys;

	lab.before(function(done){
		testingTools.getUuid(function(err, uuid2) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(uuid2).not.to.be.null();
			uuid = uuid2;

			hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, hotelGroupRow2) {
				testingTools.code.expect(err).to.be.null();
				hotelGroupRow = hotelGroupRow2;

				oldValidKeys = hotelGroupRow.data.validApiKeys.slice();
				hotelGroupRow.data.validApiKeys = [];
				//remove valid api keys from hotel group
        databaseUtils.getConnection(function(err, client, doneDb){
          testingTools.code.expect(err).to.be.null();
          client.query('UPDATE hotel_group  SET data = $1 ' +
            "WHERE data -> 'hotelGroupId' = $2",
            [hotelGroupRow.data, '"' + hotelGroupRow.data.hotelGroupId + '"'], function(err){
              testingTools.code.expect(err).to.be.null();
              doneDb();
              done();
            });
        });
			});
		});
	});

	lab.after(function(done){
		//put the api keys back
		hotelGroupRow.data.validApiKeys = oldValidKeys;

    databaseUtils.getConnection(function(err, client, doneDb){
      testingTools.code.expect(err).to.be.null();
      client.query('UPDATE hotel_group  SET data = $1 ' +
        "WHERE data -> 'hotelGroupId' = $2",
        [hotelGroupRow.data, '"' + hotelGroupRow.data.hotelGroupId + '"'], function(err){
          testingTools.code.expect(err).to.be.null();
          doneDb();
          done();
        });
    });
	});

	lab.test('should not validate without valid api keys', function(done) {
		//get hotel group

		//test
		var req = apiUtils.createApiRequest('/some/url?offset=124', {'x-cardola-uuid': uuid});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
		req.headers['x-cardola-verify'] = authUtils.getVerifyKey(req, 'test-api-key');
		authUtils.validateAuthentication(req, function(err) {
			testingTools.expectError(err, 'au-inv-verifykey');
			testingTools.code.expect(err.data.debuggingData.reason).to.equal('No valid keys');
			done();
		});
	});

});

lab.experiment('authUtils.validateAuthentication', function() {
	var uuid, hotelGroupRow, oldValidKeys;

	lab.before(function(done){
		testingTools.getUuid(function(err, uuid2) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(uuid2).not.to.be.null();
			uuid = uuid2;

			hgServices.getHotelGroup(testingTools.getHotelGroupId(), function(err, hotelGroupRow2) {
				testingTools.code.expect(err).to.be.null();
				hotelGroupRow = hotelGroupRow2;

				oldValidKeys = hotelGroupRow.data.validApiKeys.slice();
				hotelGroupRow.data.validApiKeys = ['test-api-key1', 'test-api-key2', 'test-api-key3'];
				//remove valid api keys from hotel group

        databaseUtils.getConnection(function(err, client, doneDb){
          testingTools.code.expect(err).to.be.null();
          client.query('UPDATE hotel_group  SET data = $1 ' +
            "WHERE data -> 'hotelGroupId' = $2",
            [hotelGroupRow.data, '"' + hotelGroupRow.data.hotelGroupId + '"'], function(err){
              testingTools.code.expect(err).to.be.null();
              doneDb();
              done();
            });
        });




			});
		});
	});

	lab.after(function(done){
		//put the api keys back
		hotelGroupRow.data.validApiKeys = oldValidKeys;

    databaseUtils.getConnection(function(err, client, doneDb){
      testingTools.code.expect(err).to.be.null();
      client.query('UPDATE hotel_group  SET data = $1 ' +
        "WHERE data -> 'hotelGroupId' = $2",
        [hotelGroupRow.data, '"' + hotelGroupRow.data.hotelGroupId + '"'], function(err){
          testingTools.code.expect(err).to.be.null();
          doneDb();
          done();
        });
    });


	});


	lab.test('should validate with many valid api keys and not go insane', function(done) {
		var req = apiUtils.createApiRequest('/some/url?offset=12', {'x-cardola-uuid': uuid});
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);
		req.headers['x-cardola-verify'] = authUtils.getVerifyKey(req, 'test-api-key1');
		authUtils.validateAuthentication(req, function(err) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});
	});
});

lab.experiment('authUtils.getVerifyKey', function() {
	lab.test('should work without body', function(done) {
		testingTools.getUuid(function(err, uuid) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(uuid).not.to.be.null();

			var req = apiUtils.createApiRequest('https://localhost:8000/v1/test-hgid/profiles/status', {'x-cardola-uuid': 'b4cf2e90-b134-11e4-af5d-5d230ac74c0b'});
			apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

			var getKey = function(){
				return authUtils.getVerifyKey(req, 'test-api-key', '5324b42a7f');
			};

			testingTools.code.expect(getKey).not.to.throw();
			testingTools.code.expect(getKey()).to.be.string();
			done();

		});

	});

	lab.test('should work with body', function(done) {
		testingTools.getUuid(function(err, uuid) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(uuid).not.to.be.null();

			var req = apiUtils.createApiRequest('/some/url?offset=12', {'x-cardola-uuid': uuid}, {}, null, {some: 'data'});
			apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

			var getKey = function(){
				return authUtils.getVerifyKey(req, 'some-key');
			};

			testingTools.code.expect(getKey).not.to.throw();
			testingTools.code.expect(getKey()).to.be.string();
			done();

		});

	});
});
