'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var awsS3Utils = require('../awsS3Utils');


lab.experiment('awsS3Utils.createAwsS3Connection', function() {
	lab.test('should create connection to AWS S3', function(done) {
		awsS3Utils.createAwsS3Connection(null, function(err, s3Connection) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(s3Connection).to.be.object();
			done();
		});
	});
});

lab.experiment('awsS3Utils.listAwsS3Files', function() {
	var s3Folder = 'test-getFilesDiffOfS3AndFtp';
	lab.test('should give error if cannot list files in s3', function(done) {
		awsS3Utils.createAwsS3Connection('non-existing-bucket-asdadad', function(err, s3i) {
			testingTools.code.expect(err).to.be.null();
			awsS3Utils.listAwsS3Files(s3i, s3Folder, function(err) {
				testingTools.expectError(err, 's3-list-err');
				done();
			});
		});
	});

	lab.test('should not give error', {timeout: 10000}, function(done) {
		awsS3Utils.createAwsS3Connection(null, function(err, s3i) {
			testingTools.code.expect(err).to.be.null();
			awsS3Utils.listAwsS3Files(s3i, s3Folder, function(err, data) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(data).to.include('Contents');
				testingTools.code.expect(data.Contents).to.be.array();
				testingTools.code.expect(data.Contents).to.have.length(0);
				done();
			});
		});
	});

	lab.test('should not give error and return all items', {timeout: 30000}, function(done) {
		awsS3Utils.createAwsS3Connection(null, function(err, s3i) {
			testingTools.code.expect(err).to.be.null();
			awsS3Utils.listAwsS3Files(s3i, '', function(err, data) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(data).to.include('Contents');
				testingTools.code.expect(data.Contents).to.be.array();
				testingTools.code.expect(data.IsTruncated).to.equal(false);
				var existingKeys = [];
				data.Contents.forEach(function(item){
					testingTools.code.expect(existingKeys).not.to.include(item.Key);
					existingKeys.push(item.Key);
				});
				done();
			});
		});
	});
});

lab.experiment('awsS3Utils.uploadFileToAwsS3', function() {
	var s3Folder = 'test-getFilesDiffOfS3AndFtp';
	testingTools.clearS3FolderAfterEveryTest(lab, s3Folder);

	lab.test('should give error, if cannot upload to s3', {timeout: 5000}, function(done) {
		awsS3Utils.createAwsS3Connection('non-existing-bucket-asdadad', function(err, s3i) {
			testingTools.code.expect(err).to.be.null();
			awsS3Utils.uploadFileToAwsS3(s3i, 'test.txt', s3Folder, 'test', function(err) {
				testingTools.expectError(err, 's3-upl-err');
				done();
			});
		});
	});

	lab.test('should work', {timeout: 5000}, function(done) {
		awsS3Utils.createAwsS3Connection(null, function(err, s3i) {
			testingTools.code.expect(err).to.be.null();
			awsS3Utils.uploadFileToAwsS3(s3i, 'test.txt', s3Folder, 'test', function(err) {
				testingTools.code.expect(err).to.be.null();
				done();
			});
		});
	});
});

lab.experiment('awsS3Utils.downloadFileFromAwsS3', function() {
	var c, file;
	var s3Folder = 'test-downloadFileFromAwsS3';
	testingTools.clearS3FolderAfterExperiment(lab, s3Folder);

	lab.before(function(done){
		awsS3Utils.createAwsS3Connection(null, function(err, conn){
			testingTools.code.expect(err).to.be.null();
			c = conn;

			awsS3Utils.uploadFileToAwsS3(c, 'test.txt', s3Folder, 'test', function(err, res) {
				testingTools.code.expect(err).to.be.null();
				file = res;
				done();
			});
		});
	});

	lab.test('should give error if missing file', function(done) {
		awsS3Utils.downloadFileFromAwsS3(c, 'asd243esfsdf', function(err){
			testingTools.expectError(err, 's3-get-err');
			done();
		});
	});

	lab.test('should work', function(done) {
		awsS3Utils.downloadFileFromAwsS3(c, file.Key, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.Body.toString()).to.equal('test');
			done();
		});
	});
});

lab.experiment('awsS3Utils.copyFileInAwsS3', function() {
	var c, file;
	var s3Folder = 'test-copyFileInAwsS3';
	testingTools.clearS3FolderAfterExperiment(lab, s3Folder);

	lab.before(function(done){
		awsS3Utils.createAwsS3Connection(null, function(err, conn){
			testingTools.code.expect(err).to.be.null();
			c = conn;

			awsS3Utils.uploadFileToAwsS3(c, 'test.txt', s3Folder, 'testcopyFileInAwsS3', function(err, res) {
				testingTools.code.expect(err).to.be.null();
				file = res;
				done();
			});
		});
	});

	lab.test('should give error if missing file', function(done) {
		awsS3Utils.copyFileInAwsS3(c, 'asd243esfsdf', 'asdasd', function(err){
			testingTools.expectError(err, 's3-copy-err');
			done();
		});
	});

	lab.test('should work', function(done) {
		var newLoc = file.Key.replace('test.txt', 'subfolder/test_copy.txt');
		awsS3Utils.copyFileInAwsS3(c, file.Key, newLoc, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.Key).to.equal(newLoc);

			//verify
			awsS3Utils.downloadFileFromAwsS3(c, res.Key, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.Body.toString()).to.equal('testcopyFileInAwsS3');
				done();
			});
		});
	});
});


lab.experiment('awsS3Utils.deleteFileInAwsS3', function() {
	var c, file;
	var s3Folder = 'test-deleteFileInAwsS3';
	testingTools.clearS3FolderAfterExperiment(lab, s3Folder);

	lab.before(function(done){
		awsS3Utils.createAwsS3Connection(null, function(err, conn){
			testingTools.code.expect(err).to.be.null();
			c = conn;

			awsS3Utils.uploadFileToAwsS3(c, 'test.txt', s3Folder, 'testdeleteFileInAwsS3', function(err, res) {
				testingTools.code.expect(err).to.be.null();
				file = res;
				done();
			});
		});
	});

	lab.test('should give error if invalid bucket', function(done) {
		awsS3Utils.createAwsS3Connection('asd23fr23rr', function(err, s3i){
			testingTools.code.expect(err).to.be.null();
			awsS3Utils.deleteFileInAwsS3(s3i, file.Key, function(err){
				testingTools.expectError(err, 's3-del-err');
				done();
			});
		});

	});

	lab.test('should work', function(done) {

		awsS3Utils.deleteFileInAwsS3(c, file.Key, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.Key).to.equal(file.Key);

			//verify it does not exist
			awsS3Utils.downloadFileFromAwsS3(c, res.Key, function(err){
				testingTools.expectError(err, 's3-get-err');
				done();
			});
		});
	});
});


lab.experiment('awsS3Utils.moveFileInAwsS3', function() {
	var c, file, f1;
	var s3Folder = 'test-moveFileInAwsS3';
	testingTools.clearS3FolderAfterExperiment(lab, s3Folder);

	lab.before(function(done){
		f1 = awsS3Utils.deleteFileInAwsS3;
		awsS3Utils.createAwsS3Connection(null, function(err, conn){
			testingTools.code.expect(err).to.be.null();
			c = conn;

			awsS3Utils.uploadFileToAwsS3(c, 'test.txt', s3Folder, 'testmoveFileInAwsS3', function(err, res) {
				testingTools.code.expect(err).to.be.null();
				file = res;
				done();
			});
		});
	});

	lab.afterEach(function(done){
		awsS3Utils.deleteFileInAwsS3 = f1;
		done();
	});

	lab.test('should give error if copy fails bucket', function(done) {
		awsS3Utils.moveFileInAwsS3(c, 'adssadsddas', 'asad', function(err){
			testingTools.expectError(err, 's3-copy-err');
			done();
		});
	});

	lab.test('should give error if delete fails', function(done) {
		awsS3Utils.deleteFileInAwsS3 = function(c, k, cb){
			cb(new Error('i3hfr4njskdnfi9pu34hf4'));
		};
		awsS3Utils.moveFileInAwsS3(c, file.Key, 'asad', function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('i3hfr4njskdnfi9pu34hf4');
			done();
		});
	});

	lab.test('should work', function(done) {
		var newLoc = file.Key.replace('test.txt', 'subfolder2/test_copy.txt');

		awsS3Utils.moveFileInAwsS3(c, file.Key, newLoc, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res.deleteResult.Key).to.equal(file.Key);
			testingTools.code.expect(res.copyResult.Key).to.equal(newLoc);

			//verify it exist in new location
			awsS3Utils.downloadFileFromAwsS3(c, res.copyResult.Key, function(err, dlRes){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(dlRes.Body.toString()).to.equal('testmoveFileInAwsS3');
				//verify it is removed from old location
				awsS3Utils.downloadFileFromAwsS3(c, res.deleteResult.Key, function(err){
					testingTools.expectError(err, 's3-get-err');
					done();
				});
			});
		});
	});
});
