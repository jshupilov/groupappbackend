'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var charts = require('../charts');
var media = require('../../media/services');
var d3 = require('d3');
var jsdom = require('jsdom');

var parseDate = d3.time.format('%Y%m%d').parse;
var names = {
		x: 'Month',
		y: 'Temprature'
	},
	data = [
		{
			x: parseDate('20111001'),
			'New York': '63.4',
			'San Francisco': '62.7'
		},
		{
			x: parseDate('20111002'),
			'New York': '58.0',
			'San Francisco': '59.9'
		},
		{
			x: parseDate('20111003'),
			'New York': '53.3',
			'San Francisco': '59.1'
		}];

lab.experiment('Charts.createCharts', function () {

	lab.test('should fail, Missing properties names', function (done) {
		charts.createCharts(data, {y: names.y}, function (err) {
			testingTools.expectError(err, 'cht-err-names');
			done();
		});
	});

	lab.test('should fail, Charts data error', function (done) {
		charts.createCharts([], names, function (err) {
			testingTools.expectError(err, 'cht-err-data');
			done();
		});
	});

	lab.test('should fail, Lines number exceeded', function (done) {
		var d = [
			{
				x: parseDate('20111001'),
				'New York': '63.4',
				'test': '62.7',
				'test1': '62.7',
				'test2': '62.7',
				'test3': '62.7',
				'test4': '62.7',
				'test5': '62.7',
				'test6': '62.7',
				'test7': '62.7',
				'test8': '62.7',
				'test9': '62.7'
			}
		];
		charts.createCharts(d, names, function (err) {
			testingTools.expectError(err, 'cht-err-lines');
			done();
		});
	});

	lab.test('should Work', function (done) {
		charts.createCharts(data, names, function (err, data) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(data).to.be.string();
			testingTools.code.expect(data).to.include('svg');
			done();
		});
	});
	lab.test('should fail, data objects keys incorrect', function (done) {
		data.push({
			x: parseDate('20111002'),
			'New York': '58.0',
			'San Francisco': '59.9',
			'Austin': '333'
		});

		charts.createCharts(data, names, function (err) {
			testingTools.expectError(err, 'cht-err-obj');

			done();

		});
	});

});

lab.experiment('Charts.generateCharts', function () {
	var _jsdom, _d3;

	lab.before(function (done) {
		_jsdom = jsdom.env;
		_d3 = d3;
		data[3] = data[2];
		done();
	});

	lab.after(function (done) {
		jsdom.env = _jsdom;
		d3 = _d3;
		done();
	});

	lab.test('should fail d3', function(done) {
		d3 = null;
		charts.generateCharts(data, names, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});

	lab.test('should fail on createCharts', function(done) {
		charts.createCharts(data, names, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});

	lab.test('should fail jsdom', function (done) {
		jsdom.env = function (cb) {
			cb.done(new Error('Faild'));
		};
		charts.generateCharts(data, names, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});

	});

});

lab.experiment('ChartUtils.createChartAndGetImage', {timeout: 5000}, function() {
	var f1;

	lab.before(function(done){
		f1 = media.uploadImage;
		data = [
			{
				x: parseDate('20111001'),
				'New York': '63.4',
				'San Francisco': '62.7'
			},
			{
				x: parseDate('20111002'),
				'New York': '58.0',
				'San Francisco': '59.9'
			},
			{
				x: parseDate('20111003'),
				'New York': '53.3',
				'San Francisco': '59.1'
			}];
		done();
	});

	lab.afterEach(function(done){
		media.uploadImage = f1;
		done();
	});

	lab.test('should return create chart errors', function(done) {
		charts.createChartAndGetImage(
				'test-chart',
				{},
				names,
				function(err){
					testingTools.expectError(err, 'cht-err-data');
					done();
				}
		);
	});

	lab.test('should return upload image errors', function(done) {
		media.uploadImage = function(f, c, cb){
			cb(new Error('asdpoi32u4t8u32g'));
		};
		charts.createChartAndGetImage(
				'test-chart',
				data,
				names,
				function(err){
					testingTools.code.expect(err).to.be.instanceof(Error);
					testingTools.code.expect(err.message).to.equal('asdpoi32u4t8u32g');
					done();
				}
		);
	});

	lab.test('should work', function(done) {

		charts.createChartAndGetImage(
				'test-chart',
				data,
				names,
				function(err, res){
					testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
					testingTools.code.expect(res.body).to.be.string();
					done();
				}
		);
	});
});
