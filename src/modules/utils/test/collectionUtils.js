'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var collectionUtils = require('../collectionUtils');
var apiUtils = require('../apiUtils');

lab.test('CollectionUtil should be valid', function(done) {

	testingTools.code.expect(collectionUtils).to.be.object();
	testingTools.code.expect(collectionUtils).to.include(['formatCollection']);
	done();

});

lab.experiment('CollectionUtil.formatCollection should work as expected', function() {

	lab.test('without params', function(done) {
		var col = collectionUtils.formatCollection([]);
		testingTools.code.expect(col).to.be.object();
		testingTools.code.expect(col).to.include(['href', 'offset', 'limit', 'items']);
		testingTools.code.expect(col.href).to.be.undefined();
		testingTools.code.expect(col.offset).to.equal(0);
		testingTools.code.expect(col.limit).to.equal(1000);
		testingTools.code.expect(col.items).to.be.array();
		testingTools.code.expect(col.items).to.be.empty();
		done();
	});

	lab.test('with params', function(done) {
		var col = collectionUtils.formatCollection([1, 2], 1, 2, 'hhref', 3);
		testingTools.code.expect(col).to.be.object();
		testingTools.code.expect(col).to.include(['href', 'offset', 'limit', 'items']);
		testingTools.code.expect(col.href).to.equal('hhref');
		testingTools.code.expect(col.offset).to.equal(1);
		testingTools.code.expect(col.limit).to.equal(2);
		testingTools.code.expect(col.items).to.be.array();
		testingTools.code.expect(col.items).to.include([1, 2]);
		done();
	});

	lab.test('with apiRequest', function(done) {
		var apiRequest = apiUtils.createApiRequest('/test/items?limit=20&offset=10', {}, {}, 'GET', {});
		var col = collectionUtils.formatCollection(
			[1, 2],
			apiRequest.collectionQuery.offset,
			apiRequest.collectionQuery.limit,
			apiRequest.collectionQuery.href,
			30,
			apiRequest
		);
		testingTools.code.expect(col).to.be.object();
		testingTools.code.expect(col).to.not.include(['next']);
		testingTools.code.expect(col).to.include(['prev']);

		var col2 = collectionUtils.formatCollection(
			[1, 2],
			apiRequest.collectionQuery.offset,
			apiRequest.collectionQuery.limit,
			apiRequest.collectionQuery.href,
			31,
			apiRequest
		);
		testingTools.code.expect(col2).to.be.object();
		testingTools.code.expect(col2).to.include(['next']);
		testingTools.code.expect(col2).to.include(['prev']);

		var apiRequest2 = apiUtils.createApiRequest('/test/items', {}, {}, 'GET', {});
		var col3 = collectionUtils.formatCollection(
			[1, 2],
			apiRequest2.collectionQuery.offset,
			apiRequest2.collectionQuery.limit,
			apiRequest2.collectionQuery.href,
			10,
			apiRequest2
		);
		testingTools.code.expect(col3).to.be.object();
		testingTools.code.expect(col3).not.to.include(['prev']);


		done();
	});

	lab.test('should work with parent', function(done) {
		var par = {name: 'parent'};
		var col = collectionUtils.formatCollection([], 1, 1, 'href', 1, null, par);
		testingTools.code.expect(col).to.include(['parent']);
		testingTools.code.expect(col.parent).to.equal(par);
		done();
	});

	lab.test('should work without items', function(done) {
		var par = {name: 'parent'};
		var col = collectionUtils.formatCollection(null, 1, 1, 'href', 1, null, par);
		testingTools.code.expect(col.items).to.be.array();
		testingTools.code.expect(col.items).to.be.empty();
		done();
	});

});

lab.experiment('collectionUtils.formatCollectionWithRequest', function() {
	lab.test('should work', function(done) {
		var req = apiUtils.createApiRequest('/some/href?offset=1&limit=2', {}, {}, 'GET', {});
		var res = collectionUtils.formatCollectionWithRequest([], req);
		testingTools.code.expect(res).to.be.object();
		testingTools.code.expect(res.offset).to.equal(1);
		testingTools.code.expect(res.limit).to.equal(2);
		done();
	});
});

lab.experiment('CollectionUtil services.offsetLimitToPageItems', function() {

	lab.test('should work properly', function(done) {

		var res = collectionUtils.offsetLimitToPageItems(5, 10);

		testingTools.code.expect(res.page, JSON.stringify(res)).to.equal(0);
		testingTools.code.expect(res.items, JSON.stringify(res)).to.equal(15);
		testingTools.code.expect(res.pageOffset, JSON.stringify(res)).to.equal(5);

		res = collectionUtils.offsetLimitToPageItems(10, 10);
		testingTools.code.expect(res.page, JSON.stringify(res)).to.equal(1);
		testingTools.code.expect(res.items, JSON.stringify(res)).to.equal(10);
		testingTools.code.expect(res.pageOffset, JSON.stringify(res)).to.equal(0);

		res = collectionUtils.offsetLimitToPageItems(13, 4);
		testingTools.code.expect(res.page, JSON.stringify(res)).to.equal(2);
		testingTools.code.expect(res.items, JSON.stringify(res)).to.equal(6);
		testingTools.code.expect(res.pageOffset, JSON.stringify(res)).to.equal(1);

		res = collectionUtils.offsetLimitToPageItems(13, 11);
		testingTools.code.expect(res.page, JSON.stringify(res)).to.equal(1);
		testingTools.code.expect(res.items, JSON.stringify(res)).to.equal(12);
		testingTools.code.expect(res.pageOffset, JSON.stringify(res)).to.equal(1);

		res = collectionUtils.offsetLimitToPageItems(3, 12);
		testingTools.code.expect(res.page, JSON.stringify(res)).to.equal(0);
		testingTools.code.expect(res.items, JSON.stringify(res)).to.equal(15);
		testingTools.code.expect(res.pageOffset, JSON.stringify(res)).to.equal(3);

		res = collectionUtils.offsetLimitToPageItems(31, 12);
		testingTools.code.expect(res.page, JSON.stringify(res)).to.equal(2);
		testingTools.code.expect(res.items, JSON.stringify(res)).to.equal(15);
		testingTools.code.expect(res.pageOffset, JSON.stringify(res)).to.equal(1);

		var t = function(){
			try{
				collectionUtils.offsetLimitToPageItems(3111322132131, 121232135);
			}catch (e){
				return e;
			}

		};
		var e = t();
		testingTools.expectError(e, 'sr-err-ol2pi-hl');

		done();
	});

});

lab.experiment('CollectionUtil.orderByArray', function() {
	lab.test('Should work as expected', function(done) {
		var arr = [
			{
				id: 'cow'
			},
			{
				id: 'cat'
			},
			{
				id: 'dog'
			},
			{
				vid: 'monkey'
			}
		];
		var order = ['cat', 'dog'];
		var res = collectionUtils.orderByArray(arr, order, 'id');
		testingTools.code.expect(res).to.have.length(2);
		testingTools.code.expect(res[0].id).to.equal('cat');
		testingTools.code.expect(res[1].id).to.equal('dog');
		done();
	});
});

lab.experiment('CollectionUtil.orderByArrayAndSubField', function() {
	lab.test('Should work as expected', function(done) {
		var arr = [
			{
				data: {
					id: 'cow'
				}
			},
			{
				data: {
					id: 'cat'
				}
			},
			{
				data: {
					id: 'dog'
				}
			},
			{
				custom: {
					id: 'monkey'
				}
			},
			{
				data: {
					vid: 'horse'
				}
			}
		];
		var order = ['cat', 'dog'];
		var res = collectionUtils.orderByArrayAndSubField(arr, order, 'data', 'id');
		testingTools.code.expect(res).to.have.length(2);
		testingTools.code.expect(res[0].data.id).to.equal('cat');
		testingTools.code.expect(res[1].data.id).to.equal('dog');
		done();
	});
});
