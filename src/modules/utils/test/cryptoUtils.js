/**
 * Created by jevgenishupilov on 16/02/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var cryptoUtils = require('../cryptoUtils');

lab.experiment('utils.cryptoUtils.md5', function() {
	lab.test('should give valid output', function(done) {
		var input = 'adsasd';
		var out = cryptoUtils.md5(input);
		var out2 = cryptoUtils.md5(input + '1');
		testingTools.code.expect(out).not.to.equal(input);
		testingTools.code.expect(out2).not.to.equal(out);
		done();
	});
});

lab.experiment('utils.cryptoUtils.sha256', function() {
	lab.test('should give valid output', function(done) {
		var input = 'adsasd';
		var out = cryptoUtils.sha256(input);
		testingTools.code.expect(out).not.to.equal(input);
		testingTools.code.expect(out.length).to.equal(256/8*2); //256 bits with hex output(every character as 2 hex chars)
		done();
	});
});

lab.experiment('utils.cryptoUtils.sha1', function() {
	lab.test('should give valid output', function(done) {
		var input = 'adsasd';
		var out = cryptoUtils.sha1(input);
		testingTools.code.expect(out).not.to.equal(input);
		testingTools.code.expect(out.length).to.equal(160/8*2); //160 bits with hex output(every character as 2 hex chars)
		done();
	});
});

lab.experiment('utils.cryptoUtils.encrypt', function() {
	lab.test('should give valid output', function(done) {
		var input = 'adsasd';
		var out = cryptoUtils.encrypt(input, 'somePass');
		var out2 = cryptoUtils.encrypt(input, 'somePass');
		testingTools.code.expect(out).not.to.equal(input);
		testingTools.code.expect(out.length).to.equal(input.length*2); //CTR mode with hex output(every character as 2 hex chars)
		testingTools.code.expect(out2).to.equal(out);
		done();
	});
});

lab.experiment('utils.cryptoUtils.decrypt', function() {
	lab.test('should give valid output', function(done) {
		var input = 'adsasd';
		var out = cryptoUtils.encrypt(input, 'somePass');
		var outDec = cryptoUtils.decrypt(out, 'somePass');
		testingTools.code.expect(out).not.to.equal(input);
		testingTools.code.expect(outDec).to.equal(input);
		done();
	});
});


lab.experiment('utils.cryptoUtils.retrieveSymCryptKey', function() {
	lab.test('should give valid output', function(done) {
		var input = 'somePass';
		var out = cryptoUtils.retrieveSymCryptKey(input);

		testingTools.code.expect(out.toString()).not.to.equal(input);
		testingTools.code.expect(out).to.be.instanceof(Buffer);
		testingTools.code.expect(out.toString('hex')).to.have.length(32*2); //32 bytes with hex output
		done();
	});
});

lab.experiment('utils.cryptoUtils.createSymCryptIv', function() {
	lab.test('should give valid output', function(done) {
		var out = cryptoUtils.createSymCryptIv();
		var out2 = cryptoUtils.createSymCryptIv();

		testingTools.code.expect(out).not.to.equal(out2);
		testingTools.code.expect(out).to.have.length(16*2); //16 bytes with hex output
		done();
	});
});

lab.experiment('utils.cryptoUtils.bakeSymCryoptIv', function() {
	lab.test('should give valid output', function(done) {
		var out = cryptoUtils.createSymCryptIv();
		var out2 = cryptoUtils.bakeSymCryoptIv(out);

		testingTools.code.expect(out).not.to.equal(out2);
		testingTools.code.expect(out).to.have.length(16*2); //16 bytes with hex output

		var out3 = cryptoUtils.createSymCryptIv();
		var out4 = cryptoUtils.bakeSymCryoptIv(out3);

		testingTools.code.expect(out3).not.to.equal(out);
		testingTools.code.expect(out4).not.to.equal(out2);
		testingTools.code.expect(out4.toString('hex')).to.have.length(out3.length); //16 bytes with hex output
		done();
	});
});


lab.experiment('utils.cryptoUtils.getRandomValue', function() {
	lab.test('should give valid output', function(done) {
		var out = cryptoUtils.getRandomValue(2);
		var out2 = cryptoUtils.getRandomValue(2);
		var out3 = cryptoUtils.getRandomValue(200);

		testingTools.code.expect(out).not.to.equal(out2);
		testingTools.code.expect(out.length > 2).to.equal(true);
		testingTools.code.expect(out3.length > 200).to.equal(true);
		done();
	});
});

lab.experiment('utils.cryptoUtils.strongEncryption', function() {
	lab.test('should give valid output', function(done) {
		var key = 'asdsad';
		var iv = cryptoUtils.createSymCryptIv();
		var data = 'adsad';
		var out = cryptoUtils.strongEncryption(key, iv, data);
		var out2 = cryptoUtils.strongEncryption(key, iv, data);
		var out3 = cryptoUtils.strongEncryption('09ads90', iv, data);
		var out4 = cryptoUtils.strongEncryption(key, cryptoUtils.createSymCryptIv(), data);

		testingTools.code.expect(out).to.equal(out2);
		testingTools.code.expect(out).not.to.equal(data);
		testingTools.code.expect(out3).not.to.equal(out);
		testingTools.code.expect(out4).not.to.equal(out);
		testingTools.code.expect(out4).not.to.equal(out3);
		done();
	});

	lab.test('should fail if invalid IV lengh', function(done) {
		function t(){
			cryptoUtils.strongEncryption('asdas', 'aaa', {asd: 1});
		}

		testingTools.expectError(t, 'cu-ex-enc');
		done();
	});
});

lab.experiment('utils.cryptoUtils.strongDencryption', function() {
	lab.test('should give valid output', function(done) {
		var key = 'asdsad';
		var iv = cryptoUtils.createSymCryptIv();
		var data = 'adsad';
		var out = cryptoUtils.strongEncryption(key, iv, data);
		var outDec = cryptoUtils.strongDecryption(key, iv, out);

		testingTools.code.expect(out).not.to.equal(data);
		testingTools.code.expect(outDec).to.equal(data);
		done();
	});

	lab.test('should fail if invalid IV lengh', function(done) {
		function t(){
			cryptoUtils.strongDecryption('asdas', 'aaa', 'asdads');
		}

		testingTools.expectError(t, 'cu-ex-dec');
		done();
	});
});
