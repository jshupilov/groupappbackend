'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var databaseUtils = require('../databaseUtils');
var pg = require('pg');

lab.experiment('databaseUtils.getConnectionStats', function() {
	var url;
	lab.before(function (done) {
		url = process.env.STATS_DATABASE_URL;
		done();
	});

	lab.afterEach(function (done) {
		process.env.STATS_DATABASE_URL = url;
		done();
	});
	lab.test('should fail without database url in env', function (done) {
		process.env.STATS_DATABASE_URL = null;
		databaseUtils.getConnectionStats(function (error) {
			testingTools.code.expect(error).to.be.object();
			done();
		});

	});

	lab.test('should work', function (done) {
		databaseUtils.getConnectionStats(function (error, client, doneWithConnection) {
			console.log(error);
			testingTools.code.expect(error).to.be.null();
			doneWithConnection();
			done();
		});
	});

});
lab.experiment('databaseUtils.getConnection', function() {
	var df, url;
	lab.before(function(done){
		df = pg.defaults.poolSize;
		url = process.env.DATABASE_URL;
		done();
	});

	lab.afterEach(function(done){
		pg.defaults.poolSize = df;
		process.env.DATABASE_URL = url;
		done();
	});
	lab.test('should fail without database url in env', function(done) {
		process.env.DATABASE_URL = null;
		databaseUtils.getConnection(function(error){
			testingTools.code.expect(error).to.be.object();
			done();
		});

	});

	lab.test('should work', function(done) {
		databaseUtils.getConnection(function(error, client, doneWithConnection){
			testingTools.code.expect(error).to.be.null();
			doneWithConnection();
			done();
		});
	});

	lab.test('should apply custom error handler to query method', function(done) {
		databaseUtils.getConnection(function(error, client, doneWithConnection){
			testingTools.code.expect(error).to.be.null();
			testingTools.code.expect(client).to.be.object();
			//invalid syntax, should fail
			client.query('SELECT 1 as number,', function(errorQ){
				doneWithConnection();
				testingTools.expectError(errorQ, 'db-query-fails');

				//no callback, should modify error object via listener
				var q = client.query('SELECT 1 as number,');
				q.on('error', function(error){
					testingTools.code.expect(error).to.be.object();
					done();
				});
			});
		});
	});


	lab.test('should not leak open connections', {timeout: 15000}, function(done) {
		process.env.DATABASE_POOL_SIZE = 100;
		var t = process.env.DATABASE_POOL_SIZE * 2;
		var f = function(){

			if(t){
				t--;
				databaseUtils.getConnection(function(error, client, doneWithConnection){
					testingTools.code.expect(error, JSON.stringify(error)).to.be.null();
					client.query('SELECT 1 AS bla', function(err){
						testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
						doneWithConnection();
						f();
					});
				});
			}else{
				done();
			}
		};

		pg.end();
		f();

	});

});
