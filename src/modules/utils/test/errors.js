'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var errors = require('../errors');
var apiData = require('../../../../doc/api_data.json');
lab.test('Errors defineError should work', function(done) {

	testingTools.code.expect(errors.defineError('test-code', 'Mes1', 'Mes2', 'test')).to.be.undefined();
	done();

});



lab.test('Errors newError should work', function(done) {
	var err = errors.newError('test-code');
	testingTools.code.expect(err).to.be.instanceof(Error);
	testingTools.code.expect(err).to.include(['data']);
	testingTools.code.expect(err.data).to.include(['code']);
	testingTools.code.expect(err.data.code).to.equal('test-code');
	testingTools.code.expect(err.data.isRetriable).to.equal(false);
	done();

});

lab.test('Errors newError should work and isRetriable might be equal to true ', function(done) {
	var err = errors.newError('test-code', null, true);
	testingTools.code.expect(err).to.be.instanceof(Error);
	testingTools.code.expect(err).to.include(['data']);
	testingTools.code.expect(err.data).to.include(['code']);
	testingTools.code.expect(err.data.code).to.equal('test-code');
	testingTools.code.expect(err.data.isRetriable).to.equal(true);
	done();

});

lab.test('Errors newError should fail for undefined error code', function(done) {
	try {
		errors.newError('test-code2343244', 'Mes1', 'Mes2');
	} catch(e) {
		testingTools.code.expect(e).to.be.instanceof(Error);
		done();
	}

});

lab.test('Errors newBoomError should return a boom object', function(done) {
	var err = errors.newBoomError('test-code', 400);
	testingTools.code.expect(err).to.be.object();
	testingTools.code.expect(err).to.be.include(['isBoom']);
	testingTools.code.expect(err.isBoom).to.equal(true);
	done();

});

lab.test('Errors wrapToBoom should return a boom object', function(done) {
	var err = errors.newError('test-code');
	testingTools.code.expect(err).to.be.object();
	var errBoom = errors.wrapToBoom(err, 400);
	testingTools.code.expect(errBoom).to.be.object();
	testingTools.code.expect(errBoom).to.be.include(['isBoom']);
	testingTools.code.expect(errBoom.isBoom).to.equal(true);
	done();

});

lab.test('Errors wrapToBoom should return a boom object without status accurate', function(done) {
	var err = errors.newError('test-code');
	testingTools.code.expect(err).to.be.object();
	var errBoom = errors.wrapToBoom(err);
	testingTools.code.expect(errBoom).to.be.object();
	testingTools.code.expect(errBoom).to.be.include(['isBoom']);
	testingTools.code.expect(errBoom.isBoom).to.equal(true);
	done();

});

lab.test('Errors wrapToBoom should return a boom object without data', function(done) {
	var errBoom = errors.wrapToBoom(new Error({ message: 'test' }));
	testingTools.code.expect(errBoom).to.be.object();
	testingTools.code.expect(errBoom).to.be.include(['isBoom']);
	testingTools.code.expect(errBoom.isBoom).to.equal(true);
	done();

});

lab.test('Errors wrapToBoom should return a boom object with default Error instance', function(done) {
	var err = new Error('test-message');
	var errBoom = errors.wrapToBoom(err, 400);
	testingTools.code.expect(errBoom).to.be.object();
	testingTools.code.expect(errBoom).to.be.include(['isBoom']);
	testingTools.code.expect(errBoom.isBoom).to.equal(true);
	done();

});

lab.test('Errors wrapToBoom should return null without error object', function(done) {
	var errBoom = errors.wrapToBoom(null, 400);
	testingTools.code.expect(errBoom).to.be.null();
	errBoom = errors.wrapToBoom(undefined, 400);
	testingTools.code.expect(errBoom).to.be.null();

	done();

});
lab.test('Errors wrapToBoom should return null without error object', function(done) {
	var errBoom = errors.wrapToBoom(null, 400);
	testingTools.code.expect(errBoom).to.be.null();
	errBoom = errors.wrapToBoom(undefined, 400);
	testingTools.code.expect(errBoom).to.be.null();

	done();

});

lab.test('Errors defineErrorOnce should define', function(done) {
	var err = errors.defineErrorOnce('test-define-error', '', '', 'test');
	testingTools.code.expect(err).to.be.undefined();
	done();
});
lab.test('Errors defineErrorOnce should not define', function(done) {
	var err = errors.defineErrorOnce('test-code');
	testingTools.code.expect(err).to.be.undefined();
	done();
});

lab.experiment('All errors', function() {
	var ignoredModules = [
		'Utils.Errors', 'Worker.JobExecuter', 'Worker.jobHandler', 'Worker.Scheduler',
		'test', 'Utils.papertrailSDK', 'Utils.sabreSDK', 'Utils.leasewebSDK', 'SecureStorage',
		'cryptoUtils', 'SecureStorage', 'Integration', 'Reporting.Validation', 'Reporting.Services',
		'Reporting.AccessLog', 'FtpUtils', 'AwsS3Utils', 'HttpUtils', 'Charts'
	];
	function getAllErrorsExceptIgnored(){
		var allErrors = errors.getAllErrors();
		var out = [];
		for(var ei in allErrors){
			if( ignoredModules.indexOf(allErrors[ei].data.module) > -1){
				continue;
			}
			out.push(allErrors[ei]);
		}
		return out;
	}

	lab.test('should be defined in apiDoc', function(done) {

		function tpl(code, developerMessage) {
			return '@apiError (Possible error codes) {String} ' + code + ' <span id="error_' + code + '"></span> ' + developerMessage + '';
		}

		var errorsApiDefinition = null;
		apiData.forEach(function(apiDefinition){
			if(apiDefinition.name === 'basicsErrors'){
				apiDefinition.intVersion = parseInt(apiDefinition.version.replace(/\./g, ''));
				if( !errorsApiDefinition || apiDefinition.intVersion > errorsApiDefinition.intVersion ){
					errorsApiDefinition = apiDefinition;
				}
			}
		});

		function checker(code){
			for( var eci in errorsApiDefinition.error.fields['Possible error codes']){
				var codeDef = errorsApiDefinition.error.fields['Possible error codes'][eci];
				if(codeDef.field === code){
					return codeDef;
				}
			}
		}

		var allErrors = getAllErrorsExceptIgnored();

		for(var ei in allErrors){
			var error = allErrors[ei];

			var info = tpl(error.data.code, error.data.developerMessage);

			var errDef = checker(error.data.code);
			
			testingTools.code.expect(errDef, info).to.be.object();
			testingTools.code.expect(errDef.description, info).to.include('error_' + error.data.code);
			testingTools.code.expect(errDef.description, info).to.include(error.data.developerMessage);

		}

		done();
	});

	lab.test('should have translations', function(done) {
		var userErrors = require('../../../../data/defaults/user_errors.json');
		var allErrors = getAllErrorsExceptIgnored();
		for(var lang in userErrors.localized){
			for(var ei in allErrors){
				var info = lang + ': "' + allErrors[ei].data.code + '": "' + allErrors[ei].data.localized.en_GB.userMessage +'",';
				testingTools.code.expect(userErrors.localized[lang], info).to.include(allErrors[ei].data.code);
			}
		}

		done();
	});
});
