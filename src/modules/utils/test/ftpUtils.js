'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var ftpUtils = require('../ftpUtils');
var awsS3Utils = require('../awsS3Utils');

lab.experiment('FtpUtils.createFtpConnection', function() {

	var ftpConfig = testingTools.createFtpServerForExperiment(lab);

	lab.test('should connect to an existing server', function(done) {
		ftpUtils.createFtpConnection(ftpConfig, function(err, ftpConn) {
			testingTools.code.expect(err).to.be.null();
			ftpConn.end();
			done();
		});
	});

	lab.test('should give error if cannot connect', function(done) {
		ftpUtils.createFtpConnection({}, function(err) {
			testingTools.expectError(err, 'ftp-err-conn');
			done();
		});
	});

});



lab.experiment('FtpUtils.listFtpFiles', function() {
	var ftpConfig = testingTools.createFtpServerForExperiment(lab);
	var ftpInstance;

	lab.before(function(done) {
		ftpUtils.createFtpConnection(ftpConfig, function(err, inst) {
			testingTools.code.expect(err).to.be.null();
			ftpInstance = inst;
			done();
		});
	});

	lab.after(function(done) {
		ftpInstance.end();
		ftpInstance = undefined;
		done();
	});

	lab.test('should work', function(done) {
		ftpUtils.listFtpFiles(ftpInstance, '/', function(err, list) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(list).to.be.array();
			testingTools.code.expect(list[0]).to.include(
				['type', 'name', 'target', 'sticky', 'rights', 'acl', 'owner', 'group', 'size', 'date']
			);
			done();
		});
	});

	lab.test('should give error', function(done) {
		ftpUtils.listFtpFiles(ftpInstance, '/adasdda', function(err) {
			testingTools.expectError(err, 'ftp-list-err');
			done();
		});
	});
});

lab.experiment('FtpUtils.copyFtpFileToS3', {timeout: 20000}, function() {
	var ftpConfig = testingTools.createFtpServerForExperiment(lab, 'sample_files_small');
	var ftpInstance, s3Instance;
	var s3Folder = 'test-copyFtpFileToS3';
	testingTools.clearS3FolderAfterEveryTest(lab, s3Folder);

	lab.before(function(done) {
		ftpUtils.createFtpConnection(ftpConfig, function(err, inst) {
			testingTools.code.expect(err).to.be.null();
			ftpInstance = inst;
			awsS3Utils.createAwsS3Connection(null, function(err, s3i) {
				testingTools.code.expect(err).to.be.null();
				s3Instance = s3i;
				awsS3Utils.uploadFileToAwsS3(s3i, s3Folder, 'test-file.txt', 'test contents', function(err){
					testingTools.code.expect(err).to.be.null();
					done();
				});

			});
		});
	});

	lab.after(function(done) {
		ftpInstance.end();
		ftpInstance = undefined;
		done();
	});

	lab.test('should copy file from ftp to s3', function(done) {
		ftpUtils.listFtpFiles(ftpInstance, '/', function(err, list) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(list).to.be.array().and.not.empty();
			testingTools.code.expect(list[0].name).to.be.string();
			ftpUtils.copyFtpFileToS3(ftpInstance, s3Instance, list[0].name, s3Folder, function(err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res).to.be.object();
				done();
			});
		});
	});

	lab.test('should give error, if cannot get file from ftp', function(done) {
		ftpUtils.copyFtpFileToS3(ftpInstance, s3Instance, 'smtasdasds', s3Folder, function(err) {
			testingTools.expectError(err, 'ftp-get-err');
			done();
		});
	});

	lab.test('should give error, if cannot upload to s3', function(done) {
		awsS3Utils.createAwsS3Connection('non-existing-bucket-asdadad', function(err, s3i) {
			testingTools.code.expect(err).to.be.null();
			ftpUtils.listFtpFiles(ftpInstance, '/', function(err, list) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(list).to.be.array().and.not.empty();
				testingTools.code.expect(list[0].name).to.be.string();
				ftpUtils.copyFtpFileToS3(ftpInstance, s3i, list[0].name, s3Folder, function(err) {
					testingTools.expectError(err, 's3-upl-err');
					done();
				});
			});
		});

	});

});

lab.experiment('FtpUtils.getFilesDiffOfS3AndFtp', function() {
	var ftpConfig = testingTools.createFtpServerForExperiment(lab, 'sample_files_small');
	var ftpInstance, s3Instance;
	var s3Folder = 'test-getFilesDiffOfS3AndFtp';
	testingTools.clearS3FolderAfterEveryTest(lab, s3Folder);

	lab.before(function(done) {
		ftpUtils.createFtpConnection(ftpConfig, function(err, inst) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			ftpInstance = inst;
			awsS3Utils.createAwsS3Connection(null, function(err, s3i) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				s3Instance = s3i;
				done();
			});
		});
	});

	lab.after(function(done) {
		ftpInstance.end();
		ftpInstance = undefined;
		done();
	});

	lab.test('should give diff and all files should be missing', {timeout: 30000}, function(done) {
		ftpUtils.listFtpFiles(ftpInstance, '/', function(err, ftpFiles) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(ftpFiles).to.be.array();
			ftpUtils.getFilesDiffOfS3AndFtp(ftpInstance, s3Instance, '/', s3Folder, function(err, diff) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(diff).to.be.object();
				testingTools.code.expect(diff).to.include(['existingFiles', 'missingFiles']);
				testingTools.code.expect(diff.existingFiles).to.be.array();
				testingTools.code.expect(diff.missingFiles).to.be.array();
				testingTools.code.expect(diff.missingFiles).to.have.length(ftpFiles.length);
				testingTools.code.expect(diff.existingFiles).to.have.length(0);
				done();
			});
		});
	});

	lab.test('should give diff and all files should be missing', {timeout: 30000}, function(done) {
		ftpUtils.listFtpFiles(ftpInstance, '/', function(err, ftpFiles) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(ftpFiles).to.be.array();

			ftpUtils.copyFtpFileToS3(ftpInstance, s3Instance, ftpFiles[0].name, s3Folder, function(err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res).to.be.object();

				ftpUtils.getFilesDiffOfS3AndFtp(ftpInstance, s3Instance, '/', s3Folder, function(err, diff) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(diff).to.be.object();
					testingTools.code.expect(diff).to.include(['existingFiles', 'missingFiles']);
					testingTools.code.expect(diff.existingFiles).to.be.array();
					testingTools.code.expect(diff.missingFiles).to.be.array();
					testingTools.code.expect(diff.missingFiles).to.have.length(ftpFiles.length - 1);
					testingTools.code.expect(diff.existingFiles).to.have.length(1);
					testingTools.code.expect(diff.existingFiles[0].fileName).to.equal(ftpFiles[0].name);
					diff.existingFiles.forEach(function(exitingFile) {
						testingTools.code.expect(exitingFile).to.include(
							['fileName', 'Key', 'LastModified', 'ETag', 'Size', 'StorageClass', 'Owner']
						);
					});

					diff.missingFiles.forEach(function(missingFile) {
						testingTools.code.expect(missingFile).to.include(
							['fileName']
						);
					});
					done();
				});

			});
		});
	});

	lab.test('should give error if cannot list files in s3', {timeout: 5000}, function(done) {
		awsS3Utils.createAwsS3Connection('non-existing-bucket-asdadad', function(err, s3i) {
			testingTools.code.expect(err).to.be.null();
			ftpUtils.getFilesDiffOfS3AndFtp(ftpInstance, s3i, '/', s3Folder, function(err) {
				testingTools.expectError(err, 's3-list-err');
				done();
			});
		});
	});

	lab.test('should give error if cannot list files in ftp', function(done) {
		ftpUtils.getFilesDiffOfS3AndFtp(ftpInstance, s3Instance, 'blablalasdadad', s3Folder, function(err) {
			testingTools.expectError(err, 'ftp-list-err');
			done();
		});
	});

});

lab.experiment('FtpUtils.copyFtpFilesToS3', {timeout: 5000}, function() {
	var ftpConfig = testingTools.createFtpServerForExperiment(lab, 'sample_files_small');
	var ftpInstance, s3Instance, existingFile, f1;
	var s3Folder = 'test-copyFtpFilesToS3';
	testingTools.clearS3FolderAfterEveryTest(lab, s3Folder);

	lab.before({timeout: 5000}, function(done) {
		f1 = ftpUtils.copyFtpFileToS3;
		ftpUtils.createFtpConnection(ftpConfig, function(err, inst) {
			testingTools.code.expect(err).to.be.null();
			ftpInstance = inst;
			awsS3Utils.createAwsS3Connection(null, function(err, s3i) {
				testingTools.code.expect(err).to.be.null();
				s3Instance = s3i;
				done();
			});
		});
	});

	lab.before({timeout: 30000}, function(done) {
		//copy one file over, so there is one existing file
		ftpUtils.listFtpFiles(ftpInstance, '/', function(err, list) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(list).to.be.array().and.not.empty();
			testingTools.code.expect(list[0].name).to.be.string();
			ftpUtils.copyFtpFileToS3(ftpInstance, s3Instance, list[0].name, s3Folder, function(err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res).to.be.object();
				existingFile = res;
				done();
			});
		});
	});

	lab.afterEach(function(done){
		ftpUtils.copyFtpFileToS3 = f1;
		done();
	});

	lab.after(function(done) {
		ftpInstance.end();
		ftpInstance = undefined;
		done();
	});

	lab.test('should work', {timeout: 20000}, function(done) {
		ftpUtils.copyFtpFilesToS3(ftpInstance, s3Instance, '/', s3Folder, function(err, results) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(results).to.be.array();
			results.forEach(function(result) {
				if(result.file.fileName === existingFile.fileName) {
					testingTools.code.expect(result.uploadRes).to.equal('existing');
				} else {
					testingTools.code.expect(result.uploadRes).to.be.object();
				}
			});
			done();
		}, function(){
			return 'subfolder-test';
		}, function(doneCount, totalCount){
			testingTools.code.expect(doneCount).to.be.above(1);
			testingTools.code.expect(doneCount).to.be.below(totalCount+1);
		});
	});

	lab.test('should pass on diff error', {timeout: 5000}, function(done) {
		awsS3Utils.createAwsS3Connection('non-existing-bucket-asd43g', function(err, s3i){
			testingTools.code.expect(err).to.be.null();
			ftpUtils.copyFtpFilesToS3(ftpInstance, s3i, '/', s3Folder, function(err) {
				testingTools.expectError(err, 's3-list-err');
				done();
			});
		});

	});

	lab.test('should pass on copy error', {timeout: 5000}, function(done) {
		ftpUtils.copyFtpFileToS3 = function(p1, p2, p3, p4, cb){
			cb(new Error('o[isdfu902n3dd02geyr'));
		};
		ftpUtils.copyFtpFilesToS3(ftpInstance, s3Instance, '/', s3Folder, function(err, results) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('o[isdfu902n3dd02geyr');
			testingTools.code.expect(results).to.be.array();
			results.forEach(function(result) {
				testingTools.code.expect(result.error).to.be.instanceof(Error);
				testingTools.code.expect(result.error.message).to.equal('o[isdfu902n3dd02geyr');
			});
			done();
		});
	});

});
