/**
 * Created by jevgenishupilov on 16/02/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var geoUtils = require('../geoUtils');

lab.experiment('utils.geoUtils.getDistanceFromLatLonInKm', function() {
	lab.test('should work with small distance', function(done) {
		//from 57.637565, 27.342067
		//to 59.599284, 24.511650
		//google maps gives Total distance: 272.79 km (169.5 mi)
		var dist = geoUtils.getDistanceFromLatLonInKm(57.637565, 27.342067, 59.599284, 24.511650);
		testingTools.code.expect(dist).to.be.about(272.79, 0.01);
		done();
	});


	lab.test('should work with big distance', function(done) {
		//from -33.147601, 149.326575
		//to 65.995529, -23.266551
		//google maps gives Total distance: 16,329.34 km (10,146.58 mi)
		var dist = geoUtils.getDistanceFromLatLonInKm(-33.147601, 149.326575, 65.995529, -23.266551);
		testingTools.code.expect(dist).to.be.about(16329.34, 0.05);
		done();
	});
});
