'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var httpUtils = require('../httpUtils');

lab.experiment('HttpUtils.makeRequest', function() {

	lab.test('should do https request', function(done) {
		httpUtils.makeRequest({host: 'localhost'}, 'https', function(err){
			testingTools.expectError(err, 'http-req-err');
			testingTools.code.expect(err.data.debuggingData.protocol).to.equal('https');
			done();
		});
	});

	lab.test('should do http request', function(done) {
		httpUtils.makeRequest({host: 'localhost'}, 'http', function(err){
			testingTools.expectError(err, 'http-req-err');
			testingTools.code.expect(err.data.debuggingData.protocol).to.equal('http');
			done();
		});
	});

	lab.test('should give error', function(done) {
		httpUtils.makeRequest({host: 'localhost'}, 'ftp', function(err){
			testingTools.expectError(err, 'http-inv-prot');
			done();
		});
	});

});

lab.experiment('HttpUtils.makeRequest with mocked responses 503', function() {
	testingTools.mockResponsesForExperiment(lab);

	lab.test('should give error', function(done) {
		httpUtils.makeRequest({host: 'localhost'}, 'http', function(err){
			testingTools.expectError(err, 'http-inv-res');
			done();
		});
	});
});

lab.experiment('HttpUtils.makeRequest with mocked responses 200', function() {
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 200;
		res.end('test-res', 'utf8');
	});

	lab.test('should work', function(done) {
		httpUtils.makeRequest({host: 'localhost', encoding: 'utf8'}, 'http', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.body).to.equal('test-res');
			done();
		});
	});
});

lab.experiment('HttpUtils.makeRequest with mocked responses - socket error', function() {
	testingTools.mockResponsesForExperiment(lab, null, function(mitm){
		mitm.on('connection', function(socket){
			socket.destroy();
		});
	});

	lab.test('should give error', function(done) {
		httpUtils.makeRequest({host: 'localhost', body: 'some content'}, 'http', function(err){
			testingTools.expectError(err, 'http-req-err');
			done();
		});
	});
});

lab.experiment('HttoUtils.makeRequestToUrl', function() {
	testingTools.mockResponsesForExperiment(lab);
	lab.test('should make a call', function(done) {
		httpUtils.makeRequestToUrl('http://localhost', {}, function(err){
			testingTools.expectError(err, 'http-inv-res');
			done();
		});
	});
});
