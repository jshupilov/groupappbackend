'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var langUtils = testingTools.requireUncached('../modules/utils/langUtils');
var defaultTranslations = require('../../../../data/defaults/translations.json');
var q = require('q');

lab.test('langUtils should be valid', function(done) {

	testingTools.code.expect(langUtils).to.be.object();
	testingTools.code.expect(langUtils).to.include(['getLanguageObject']);
	done();

});

lab.experiment('langUtils.loadLanguages', function() {
	lab.test('should work', function(done) {

		var t = function() {
			return langUtils.getLanguageObject('en');
		};


		langUtils.loadLanguages();
		testingTools.code.expect(t).not.to.throw();
		done();
	});

	lab.test('should work with country identifier', function(done) {

		var t = function() {
			return langUtils.getLanguageObject('en_GB');
		};


		langUtils.loadLanguages();
		testingTools.code.expect(t).not.to.throw();
		done();
	});

	lab.test('should fail with invalid code', function(done) {

		var t = function() {
			return langUtils.getLanguageObject('asda');
		};


		langUtils.loadLanguages();
		testingTools.expectError(t, 'lu-inv-langcode');
		done();
	});

	lab.test('should fail with no langCode', function(done) {

		var t = function() {
			return langUtils.getLanguageObject();
		};

		testingTools.code.expect(t).to.throw();

		done();
	});
});

lab.experiment('langUtils.loadCountries', function() {
	lab.test('should work', function(done) {

		var t = function() {
			return langUtils.getLanguageObject('en_GB');
		};

		langUtils.loadCountries();
		testingTools.code.expect(t).not.to.throw();
		done();
	});
});

lab.experiment('langUtils.getCountryObject', function() {
	lab.test('should work', function(done) {

		var t1 = function() {
			langUtils.getCountryObject();
		};
		var t2 = function() {
			langUtils.getCountryObject('GBERR');
		};
		var t3 = function() {
			langUtils.getCountryObject('GB');
		};
		testingTools.code.expect(t1).to.throw();
		testingTools.code.expect(t2).to.throw();
		testingTools.code.expect(t3).not.to.throw();

		done();
	});
});

lab.experiment('langUtils.getLanguageObject should work as expected', function() {

	lab.test('without params', function(done) {
		var thrower = function() {
			langUtils.getLanguageObject();
		};
		testingTools.code.expect(thrower).to.throw();
		done();
	});

	lab.test('with params', function(done) {
		var lang = langUtils.getLanguageObject('en');
		testingTools.code.expect(lang).to.be.object();
		testingTools.code.expect(lang).to.include(['code', 'englishName', 'nativeName', 'rtl']);
		testingTools.code.expect(lang.code).to.equal('en');
		testingTools.code.expect(lang.rtl).to.equal(false);
		done();
	});

	lab.test('with params and country', function(done) {
		var lang = langUtils.getLanguageObject('en_GB');
		testingTools.code.expect(lang).to.be.object();
		testingTools.code.expect(lang).to.include(['code', 'englishName', 'nativeName', 'rtl']);
		testingTools.code.expect(lang.code).to.equal('en_GB');
		testingTools.code.expect(lang.rtl).to.equal(false);
		done();
	});

	lab.test('with rtl', function(done) {
		var lang = langUtils.getLanguageObject('ar');
		testingTools.code.expect(lang).to.be.object();
		testingTools.code.expect(lang).to.include(['code', 'englishName', 'nativeName', 'rtl']);
		testingTools.code.expect(lang.code).to.equal('ar');
		testingTools.code.expect(lang.rtl).to.equal(true);
		done();
	});

});

lab.experiment('langUtils.getLocalizedData', function() {

	lab.test('should work with localized attr', function(done) {
		var item1 = {
			name: 'item1',
			localized: {
				en_GB: {
					name: 'item1_en'
				}
			}
		};
		var data = langUtils.getLocalizedData(item1, 'en_GB');
		testingTools.code.expect(data).to.include(['name']);
		testingTools.code.expect(data).not.to.include(['localized']);
		done();
	});

	lab.test('should have default values if localized not available', function(done) {
		var item1 = {
			name: 'item1',
			localized: {
				en_US: {
					name: 'item1_en'
				}
			}
		};
		var data = langUtils.getLocalizedData(item1, 'en_GB');
		testingTools.code.expect(data).not.to.be.null();
		testingTools.code.expect(data).to.include(['name']);
		testingTools.code.expect(data.name).to.equal('item1');
		done();
	});

	lab.test('should work with modifierCallback', function(done) {
		var item1 = {
			name: 'item1',
			localized: {
				en_GB: {
					name: 'item1_en'
				}
			}
		};
		var data = langUtils.getLocalizedData(item1, 'en_GB', function(out){
			return {
				myAttr: 'yes' + out.name
			};
		});
		testingTools.code.expect(data).to.include(['myAttr']);
		testingTools.code.expect(data).not.to.include(['name']);
		done();
	});

	lab.test('should give null without localized attr', function(done) {
		var item1 = {
			name: 'item1'
		};
		var data = langUtils.getLocalizedData(item1, 'en_GB');
		testingTools.code.expect(data).to.be.null();
		done();
	});

	lab.test('should work with given language attr name', function(done) {
		var item1 = {
			name: 'item1',
			localized: [
				{
					language: 'en_GB',
					name: 'item one'
				},
				{
					languageWithTypo: 'en_GB',
					name: 'item 2'
				}
			]
		};
		var data = langUtils.getLocalizedData(item1, 'en_GB', null, 'language');
		testingTools.code.expect(data).not.to.be.null();
		testingTools.code.expect(data.name).to.equal('item one');
		done();
	});

});

lab.experiment('langUtils.getLocalizedDataForAllLanguages', function(){

	lab.test('should work with localized attr given', function(done){
		var item1 = {
			name: 'item1',
			localized: {
				en_GB: {
					name: 'item1_en'
				}
			}
		};
		var data = langUtils.getLocalizedDataForAllLanguages(item1);
		testingTools.code.expect(data).to.include('en_GB');
		done();
	});

	lab.test('should work with localized attr not given', function(done){
		var item1 = {
			name: 'item1'
		};
		var data = langUtils.getLocalizedDataForAllLanguages(item1);
		testingTools.code.expect(data).not.to.include('en_GB');
		testingTools.code.expect(data).to.be.empty();
		done();
	});

});

lab.experiment('langUtils.init', function(){
	lab.test('should work like a sharm', function(done){
		testingTools.code.expect(langUtils.init).not.to.throw();
		done();
	});
});

lab.experiment('langUtils.getTranslation', function() {
	lab.test('should work', function(done) {
		var translations = {
			localized: {
				en_GB: {
					someKey: 'someValue'
				}
			}
		};
		var r = langUtils.getTranslation(translations, 'someKey', 'en_GB');
		testingTools.code.expect(r).to.equal('someValue');
		done();
	});

	lab.test('should give error if language is missing', function(done) {
		var translations = {
			localized: {
				en_US: {
					someKey: 'someValue'
				}
			}
		};
		var r = function(){
			langUtils.getTranslation(translations, 'someKey', 'en_GB');
		};
		testingTools.expectError(r, 'lu-no-trans');
		done();
	});

	lab.test('should give error if key is missing', function(done) {
		var translations = {
			localized: {
				en_GB: {
					someKey2: 'someValue'
				}
			}
		};
		var r = function(){
			langUtils.getTranslation(translations, 'someKey', 'en_GB');
		};
		testingTools.expectError(r, 'lu-no-trans');
		done();
	});

	lab.test('should give error if language is missing', function(done) {
		var translations = {
			localized: {
				en_GB: {
					someKey2: 'someValue'
				}
			}
		};
		var r = function(){
			langUtils.getTranslation(translations, 'someKey', 'en');
		};
		testingTools.expectError(r, 'lu-no-trans');
		done();
	});
});

lab.experiment('langUtils.validateSelectedLanguages', function() {

	lab.test('should work and return false if not array of langs given', function(done) {
		var res = langUtils.validateSelectedLanguages({langs: 1}, ['aa']);
		testingTools.code.expect(res).to.equal(false);
		done();
	});

	lab.test('should work and return false if not array of strings given', function(done) {
		var res = langUtils.validateSelectedLanguages([{langCode: 'en_GB'}], ['aa']);
		testingTools.code.expect(res).to.equal(false);
		done();
	});

	lab.test('should work and return false', function(done) {
		var res = langUtils.validateSelectedLanguages(['asd'], ['aa']);
		testingTools.code.expect(res).to.equal(false);
		done();
	});

	lab.test('should work and return true', function(done) {
		var res = langUtils.validateSelectedLanguages(['asd'], ['asd']);
		testingTools.code.expect(res).to.equal(true);
		done();
	});
});

lab.experiment('langUtils.getPossibleLanguageOfObject', function() {

	lab.test('should return language code, if it exists in localized', function(done) {
		var langs = ['et_EE', 'en_GB', 'en_US'];
		var lang = langUtils.getPossibleLanguageOfObject(langs, 'en_US', {localized: {en_GB: {}, en_US: {}}});
		testingTools.code.expect(lang).to.equal('en_GB');
		done();
	});

	lab.test('should return default language code', function(done) {
		var langs = ['et_EE', 'en_GB'];
		var lang = langUtils.getPossibleLanguageOfObject(langs, 'en_US', {localized: {en_US: {}}});
		testingTools.code.expect(lang).to.equal('en_US');
		done();
	});

	lab.test('should return null if language code is not in localized  and default also not', function(done) {
		var langs = ['et_EE', 'en_GB', 'en_US'];
		var lang = langUtils.getPossibleLanguageOfObject(langs, 'en_US', {localized: {}});
		testingTools.code.expect(lang).to.be.null();
		done();
	});

});

lab.experiment('langUtils.mergeTranslationsWithDefault', function() {

	lab.test('Should work', function(done) {
		var translations = {localized: {en_GB: { bla: 'bla', PhoneText: 'Phone text blabla'}, blablabla: {}}};
		langUtils.mergeTranslationsWithDefault( JSON.parse(JSON.stringify(translations)), function(err, result){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(result.localized.en_GB.PhoneText).to.equal('Phone text blabla');
			testingTools.code.expect(result.localized.en_GB.bla).to.equal('bla');
			testingTools.code.expect( Object.keys(result.localized.en_GB).length ).to.be.above(2);
		});
		done();
	});

	lab.test('should take default value if translations object is missing the value', function(done) {
		langUtils.mergeTranslationsWithDefault({localized: {en_GB: {}}}, function(err, translations){
			testingTools.code.expect(err).to.be.null();
			var keys = Object.keys(translations.localized.en_GB);
			var testKey = keys[0];
			var origVal = translations.localized.en_GB[testKey];
			delete translations.localized.en_GB[testKey];
			var trans = langUtils.getTranslation(translations, testKey, 'en_GB');
			testingTools.code.expect(trans).to.equal(origVal);
			done();
		});
	});

	lab.test('should give error if something fails', function(done) {
		langUtils.mergeTranslationsWithDefault(null, function(err){
			testingTools.expectError(err, 'lu-transf-merge-fail');
			done();
		});
	});

});

lab.experiment('langUtils.getDefaultTranslationKeys', function() {
	lab.test('should work', function(done) {
		var keys = langUtils.getDefaultTranslationKeys();
		testingTools.code.expect(keys).to.be.array();
		testingTools.code.expect(keys).not.to.be.empty();
		done();
	});
});

lab.experiment('langUtils.isValidLanguageCode', function() {
	lab.test('should give false if invalid language', function(done) {
		var res = langUtils.isValidLanguageCode('asdasddasd');
		testingTools.code.expect(res).to.equal(false);
		done();
	});

	lab.test('should give true if valid language', function(done) {
		var res = langUtils.isValidLanguageCode('en_GB');
		testingTools.code.expect(res).to.equal(true);
		done();
	});

	lab.test('should give true if valid language', function(done) {
		var res = langUtils.isValidLanguageCode('en');
		testingTools.code.expect(res).to.equal(true);
		done();
	});
});

lab.experiment('langUtils.validateTranslations', function() {
	var dt = JSON.stringify(defaultTranslations);
	function getData(){
		return JSON.parse(dt);
	}

	lab.test('should give error if not object', function(done) {
		var translations = 1;
		langUtils.validateTranslations(translations, function(err){
			testingTools.expectError(err, 'lu-vdt-inv-val');
			testingTools.code.expect(err.data.debuggingData).not.to.include('key');
			done();
		});
	});

	lab.test('should give error if missing localized', function(done) {
		var translations = {};
		langUtils.validateTranslations(translations, function(err){
			testingTools.expectError(err, 'lu-vdt-key-miss');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized');
			done();
		});
	});

	lab.test('should give error if invalid localized', function(done) {
		var translations = {localized: 1};
		langUtils.validateTranslations(translations, function(err){
			testingTools.expectError(err, 'lu-vdt-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized');
			done();
		});
	});

	lab.test('should give error if invalid language code', function(done) {
		var translations = {localized: {asdasd: 1}};
		langUtils.validateTranslations(translations, function(err){
			testingTools.expectError(err, 'lu-vdt-inv-lang');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized[asdasd]');
			done();
		});
	});

	lab.test('should give error if invalid localized object', function(done) {
		var translations = {localized: {en_GB: 1}};
		langUtils.validateTranslations(translations, function(err){
			testingTools.expectError(err, 'lu-vdt-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('localized[en_GB]');
			done();
		});
	});

	lab.test('should give error if something wrong with translations', function(done) {
		var translations = getData();

		function doVal(p, data, exKey, exErr){
			langUtils.validateTranslations(data, function(err){
				testingTools.expectError(err, exErr, JSON.stringify([exKey, exErr]));
				testingTools.code.expect(err.data.debuggingData.key).to.equal(exKey);
				p.resolve();
			});
		}
		var promises = [];
		for( var li in translations.localized ){
			for(var key in translations.localized[li] ){
				//key-miss
				var trans = getData();
				delete trans.localized[li][key];
				var p = q.defer();
				promises.push(p.promise);
				doVal(p, trans, 'localized[' + li + '][' + key + ']', 'lu-vdt-key-miss');

				//inv-val
				var trans2 = getData();
				trans2.localized[li][key] = '';
				var p2 = q.defer();
				promises.push(p2.promise);
				doVal(p2, trans2, 'localized[' + li + '][' + key + ']', 'lu-vdt-inv-val');

				//inv-tpl
				var trans3 = getData();
				trans3.localized[li][key] = 'asd as{{asda}';
				var p3 = q.defer();
				promises.push(p3.promise);
				doVal(p3, trans3, 'localized[' + li + '][' + key + ']', 'lu-vdt-inv-tpl');

			}
		}

		q.all(promises).done(function(){
			done();
		});

	});

});

lab.experiment('langUtils.validateDefaultTranslations', function() {
	lab.test('should say that default translations are OK', function(done) {
		langUtils.validateDefaultTranslations(function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

});
