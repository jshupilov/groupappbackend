'use strict';

var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var leasewebSDK = require('../leasewebSDK');
var ftpUtils = require('../ftpUtils');
var awsS3Utils = require('../awsS3Utils');
var databaseUtils = require('../databaseUtils');
var fs = require('fs');
var path = require('path');

lab.experiment('utils.leasewebSDK', {timeout: 10000}, function () {
	var r = 0;
	testingTools.mockResponsesForExperiment(lab, function (req, res) {
		res.setHeader('content-type', 'application/json');
		res.statusCode = 503;
		res.end('{"message":"Sad panda leasewebSDK"}', 'utf8');
		r += (req.headers.host.indexOf('api.leasewebcdn.com')+1);
	});

	lab.test('should fail with statusCode 503', function (done) {
		leasewebSDK.purge(null, function (err) {
			testingTools.expectError(err, 'lw-res-inv');
			testingTools.code.expect(r).to.equal(10);
			done();
		});
	});
});

lab.experiment('utils.leasewebSDK', function () {

	testingTools.mockResponsesForExperiment(lab, undefined, function (mitm) {
		mitm.on('connection', function (socket) {
			socket.destroy();
		});
	});

	lab.test('should fail with network error', function (done) {
		leasewebSDK.purge(null, function (err) {
			testingTools.expectError(err, 'lw-query-inv');
			done();
		});
	});
});

lab.experiment('utils.leasewebSDK', function () {

	testingTools.mockResponsesForExperiment(lab, function (req, res) {
		res.setHeader('content-type', 'application/json');
		res.statusCode = 200;
		res.end('Test response', 'utf8');
	});

	lab.test('should fail if incorrect response format', function (done) {
		leasewebSDK.purge(null, function (err) {
			testingTools.expectError(err, 'lw-res-inv-frmt');
			done();
		});
	});
});

lab.experiment('utils.leasewebSDK purge', {timeout: 10000}, function () {

	lab.before(function (done) {
		testingTools.disableEvents();
		done();
	});
	lab.test('should give error if body is null', function (done) {
		leasewebSDK.purge(null, function (err) {
			testingTools.expectError(err, 'lw-res-inv');
			done();
		});
	});
	lab.test('should work with purge all', function (done) {
		leasewebSDK.purge(['.*'], function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res).to.include('success');
			done();
		});
	});
	lab.test('should work with purge specific hotelGroupId', function (done) {
		leasewebSDK.purge(['.*' + testingTools.getHotelGroupId() + '.*'], function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res).to.include('success');
			done();
		});
	});
});

lab.experiment('utils.leasewebSDK warmUp', function () {
	var hgId = testingTools.getHotelGroupId();
	lab.test('should give error if body is null', function (done) {
		leasewebSDK.warmUp(null, hgId, function (err) {
			testingTools.expectError(err, 'lw-res-inv');
			done();
		});
	});
	lab.test('should work', {timeout: 10000}, function (done) {
		leasewebSDK.warmUp(['.*'], hgId, function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.object();
			testingTools.code.expect(res).to.include('success');
			done();
		});
	});
});

lab.experiment('utils.leasewebSDK.downloadAccessLogs', {timeout: 5000}, function () {
	var v1, v2, v3, v4, f1, f2;
	testingTools.clearS3FolderAfterEveryTest(lab, 'access_logs', process.env.AWS_S3_BUCKET_ACCESS_LOGS);

	lab.before(function (done) {
		v1 = process.env.LEASEWEB_FTP_USER;
		v2 = process.env.LEASEWEB_FTP_PASS;
		v3 = process.env.AWS_S3_BUCKET_ACCESS_LOGS;
		v4 = process.env.PUBLIC_HOST;
		f1 = awsS3Utils.createAwsS3Connection;
		f2 = ftpUtils.copyFtpFileToS3;
		done();
	});

	lab.afterEach(function (done) {
		process.env.LEASEWEB_FTP_USER = v1;
		process.env.LEASEWEB_FTP_PASS = v2;
		process.env.AWS_S3_BUCKET_ACCESS_LOGS = v3;
		process.env.PUBLIC_HOST = v4;
		awsS3Utils.createAwsS3Connection = f1;
		ftpUtils.copyFtpFileToS3 = f2;

		done();
	});

	lab.test('should get error with invalid ftp conection params', function (done) {
		process.env.LEASEWEB_FTP_USER = 'test';
		process.env.LEASEWEB_FTP_PASS = 'test';
		leasewebSDK.downloadAccessLogs(function (err) {
			testingTools.expectError(err, 'ftp-err-conn');
			done();
		});
	});

	lab.test('should get error with invalid s3 conection params', function (done) {
		awsS3Utils.createAwsS3Connection = function (b, cb) {
			cb(new Error('6546546652346423dssf'));
		};
		leasewebSDK.downloadAccessLogs(function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('6546546652346423dssf');
			done();
		});
	});

	lab.test('should return error from copy processs', {timeout: 10000}, function (done) {
		process.env.AWS_S3_BUCKET_ACCESS_LOGS = 'not-exsitingig-bucket';
		leasewebSDK.downloadAccessLogs(function (err) {
			testingTools.expectError(err, 's3-list-err');
			done();
		});
	});

	lab.test('should work and download access logs', {timeout: 10000}, function (done) {
		process.env.PUBLIC_HOST = 'api-test.cardola.net';
		var uplRes = {s3Key: 'blabla', smt: 'else'};
		ftpUtils.copyFtpFileToS3 = function (f, s, ff, sf, cb) {
			cb(null, uplRes);
		};
		leasewebSDK.downloadAccessLogs(function (err, results) {
			testingTools.code.expect(err).to.be.null();
			results.forEach(function (res) {
				testingTools.code.expect(res.uploadRes).to.equal(uplRes);
			});
			done();
		});
	});

});

lab.experiment('LeaseWebSdk.getPurgeStatus', function () {
	lab.test('should work and return error about invalid job id', function (done) {
		leasewebSDK.getPurgeStatus(213424234, function (err) {
			testingTools.expectError(err, 'lw-res-inv');
			done();
		});
	});
});

lab.experiment('LeaseWebSdk.listNewAccessLogs', function () {
	var s3Connection;
	var s3Folder = 'istNewAccessLogs-test';
	var f1;
	//make sure we clean after ourselves
	testingTools.clearS3FolderAfterExperiment(lab, s3Folder, process.env.AWS_S3_BUCKET_ACCESS_LOGS);

	lab.before(function (done) {
		f1 = leasewebSDK.getAccessLogsS3Folder;
		awsS3Utils.createAwsS3Connection(process.env.AWS_S3_BUCKET_ACCESS_LOGS, function (err, c) {
			testingTools.code.expect(err).to.be.null();
			s3Connection = c;
			done();
		});
	});

	lab.afterEach(function (done) {
		leasewebSDK.getAccessLogsS3Folder = f1;
		done();
	});

	lab.before(function (done) {
		//upload sample new file
		awsS3Utils.uploadFileToAwsS3(
			s3Connection,
			s3Folder,
			'raw_access.log-2015110316.gz',
			fs.readFileSync(path.join(__dirname, 'raw_access.log-2015110316.gz')),
			function (err) {
				testingTools.code.expect(err).to.be.null();
				done();
			});
	});

	lab.before(function (done) {
		//upload sample processed file
		awsS3Utils.uploadFileToAwsS3(
			s3Connection,
			path.join(s3Folder, 'done'),
			'raw_access.log-2015110316-2.gz',
			fs.readFileSync(path.join(__dirname, 'raw_access.log-2015110316.gz')),
			function (err) {
				testingTools.code.expect(err).to.be.null();
				done();
			});
	});

	lab.test('should pass on listing errors', function (done) {
		awsS3Utils.createAwsS3Connection('asd234rt2r3afa', function (err, c) {
			testingTools.code.expect(err).to.be.null();
			leasewebSDK.listNewAccessLogs(c, function (err) {
				testingTools.expectError(err, 's3-list-err');
				done();
			});
		});
	});

	lab.test('should return list of files', function (done) {
		leasewebSDK.getAccessLogsS3Folder = function () {
			return s3Folder;
		};
		leasewebSDK.listNewAccessLogs(s3Connection, function (err, s3Files) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(s3Files).to.be.array();
			testingTools.code.expect(s3Files).to.have.length(1);
			testingTools.code.expect(s3Files[0].Key).to.include('raw_access.log-2015110316.gz');
			done();
		});
	});
});

lab.experiment('LeaseWebSdk.importAccessLogFile', function () {
	var s3Connection, f1, testFile, testFile2, f2;
	var s3Folder = 'importAccessLogFile-test';

	lab.before(function (done) {
		f1 = leasewebSDK.getAccessLogsS3Folder;
		f2 = awsS3Utils.copyFileInAwsS3;

		awsS3Utils.createAwsS3Connection(process.env.AWS_S3_BUCKET_ACCESS_LOGS, function (err, c) {
			testingTools.code.expect(err).to.be.null();
			s3Connection = c;
			done();
		});
	});

	lab.after(function (done) {
		leasewebSDK.getAccessLogsS3Folder = f1;

		done();
	});

	lab.afterEach(function (done) {
		awsS3Utils.copyFileInAwsS3 = f2;
		done();
	});

	lab.before(function (done) {
		//upload sample new file
		awsS3Utils.uploadFileToAwsS3(
			s3Connection,
			s3Folder,
			'raw_access.log-2015110316.gz',
			fs.readFileSync(path.join(__dirname, 'raw_access.log-2015110316.gz')),
			function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testFile = res;
				done();
			});
	});

	lab.before(function (done) {
		//upload sample new file
		awsS3Utils.uploadFileToAwsS3(
			s3Connection,
			s3Folder,
			'raw_access.log-2015110316-2.gz',
			'asdasdsdd',
			function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testFile2 = res;
				done();
			});
	});

	lab.test('should pass on download errors', function (done) {
		awsS3Utils.createAwsS3Connection('asdwer23rasdfasd', function (err, c) {
			testingTools.code.expect(err).to.be.null();
			leasewebSDK.importAccessLogFile(c, 'asdadsd', function (err) {
				testingTools.expectError(err, 's3-get-err');
				done();
			});
		});
	});

	lab.test('should pass on import(unpack error) errors', function (done) {
		leasewebSDK.importAccessLogFile(s3Connection, testFile2.Key, function (err) {
			testingTools.expectError(err, 'rprt-inv-gz');
			done();
		});
	});

	lab.test('should pass on S3 moving errors', function (done) {
		awsS3Utils.copyFileInAwsS3 = function (a1, a2, a3, cb) {
			cb(new Error('asdiu3489theiuf'));
		};
		leasewebSDK.importAccessLogFile(s3Connection, testFile.Key, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('asdiu3489theiuf');
			done();
		});
	});

});

lab.experiment('LeaseWebSdk.importNewAccessLogFiles', {timeout: 30000}, function () {
	var s3Connection;
	var s3Folder = 'importNewAccessLogFiles-test';
	var f1, doneDb, client, maxId, e1;
	//make sure we clean after ourselves
	testingTools.clearS3FolderAfterExperiment(lab, s3Folder, process.env.AWS_S3_BUCKET_ACCESS_LOGS);

	lab.before(function (done) {
		f1 = leasewebSDK.getAccessLogsS3Folder;
		e1 = process.env.AWS_S3_BUCKET_ACCESS_LOGS;
		leasewebSDK.getAccessLogsS3Folder = function () {
			return s3Folder;
		};
		awsS3Utils.createAwsS3Connection(process.env.AWS_S3_BUCKET_ACCESS_LOGS, function (err, c) {
			testingTools.code.expect(err).to.be.null();
			s3Connection = c;
			done();
		});
	});

	lab.afterEach(function (done) {
		process.env.AWS_S3_BUCKET_ACCESS_LOGS = e1;
		done();
	});

	lab.before(function (done) {
		databaseUtils.getConnectionStats(function (err, _client, _doneDB) {
			testingTools.code.expect(err).to.be.null();
			doneDb = _doneDB;
			client = _client;
			client.query('SELECT nextval(pg_get_serial_sequence(\'access_log\', \'id\'));', function (err, res) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				maxId = res.rows[0].nextval;
				done();
			});
		});
	});
	lab.after(function (done) {
		leasewebSDK.getAccessLogsS3Folder = f1;
		client.query('DELETE FROM access_log WHERE id >= $1', [maxId], function (err) {
			doneDb();
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			done();
		});
	});

	lab.before(function (done) {
		//upload sample new file
		awsS3Utils.uploadFileToAwsS3(
			s3Connection,
			s3Folder,
			'raw_access.log-2015110316.gz',
			fs.readFileSync(path.join(__dirname, 'raw_access.log-2015110316.gz')),
			function (err) {
				testingTools.code.expect(err).to.be.null();
				done();
			});
	});

	lab.before(function (done) {
		//upload sample processed file
		awsS3Utils.uploadFileToAwsS3(
			s3Connection,
			path.join(s3Folder, 'done'),
			'raw_access.log-2015110316-2.gz',
			fs.readFileSync(path.join(__dirname, 'raw_access.log-2015110316.gz')),
			function (err) {
				testingTools.code.expect(err).to.be.null();
				done();
			});
	});


	lab.test('should work and import one file with some rows', function (done) {
		leasewebSDK.importNewAccessLogFiles(function (err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.array();
			testingTools.code.expect(res).not.to.be.empty();
			testingTools.code.expect(res).to.have.length(1);
			testingTools.code.expect(res[0].s3Key).to.include('raw_access.log-2015110316.gz');
			//verify that there was and entry into the database
			client.query(
				'SELECT * FROM access_log WHERE id >= $1',
				[maxId],
				function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rows).to.have.length(8);
					testingTools.code.expect(res.rows[0].data.path).to.equal('/v1/slh/categories/slh-exp-bere/hotels');
					testingTools.code.expect(res.rows[7].data.path).to.equal('/v1/slh/categories/slh-des-eur-gre-myto/hotels');
					done();
				}
			);


		});
	});

	lab.test('should work and import 0 files', function (done) {
		leasewebSDK.importNewAccessLogFiles(function (err, res) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.be.array();
			testingTools.code.expect(res).to.be.empty();
			done();
		});
	});

	lab.test('should give error if cannot list', function (done) {
		process.env.AWS_S3_BUCKET_ACCESS_LOGS = 'adsoij2893u4f';
		leasewebSDK.importNewAccessLogFiles(function (err) {
			testingTools.expectError(err, 's3-list-err');
			done();
		});
	});
});
