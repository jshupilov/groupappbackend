'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var logging = testingTools.requireUncached('../modules/utils/logging');

lab.experiment('utils.logging', function(){
	var v1, v2;

	lab.before(function(done){
		v1 = process.env.PAPERTRAIL_HOST;
		v2 = process.env.PAPERTRAIL_PORT;
		logging.close(); //close the logger
		done();
	});

	lab.afterEach(function(done){
		process.env.PAPERTRAIL_HOST = v1;
		process.env.PAPERTRAIL_PORT = v2;
		logging.close(); //close the logger
		done();
	});

	lab.test('should have standart functions and papetrail activated', function(done){
		process.env.PAPERTRAIL_HOST = 'blabla';
		process.env.PAPERTRAIL_PORT = 'blabla2';
		var loggingInstance = logging.init();
		testingTools.code.expect(loggingInstance).to.include(['silly', 'debug', 'notice', 'info', 'warning', 'err', 'alert', 'emerg']);
		testingTools.code.expect(loggingInstance.transports.Papertrail).to.be.object();
		done();
	});

	lab.test('Should log to console 1', function(done){
		delete process.env.PAPERTRAIL_PORT;
		var loggingInstance = logging.init();
		testingTools.code.expect(loggingInstance.transports.console).to.be.object();
		done();
	});
	lab.test('Should log to console 2', function(done){
		delete process.env.PAPERTRAIL_HOST;
		var loggingInstance = logging.init();
		testingTools.code.expect(loggingInstance.transports.console).to.be.object();
		done();
	});

	lab.test('Should log to console 3', function(done){
		delete process.env.PAPERTRAIL_PORT;
		delete process.env.PAPERTRAIL_HOST;

		var loggingInstance = logging.init();
		testingTools.code.expect(loggingInstance.transports.console).to.be.object();
		done();
	});
});
