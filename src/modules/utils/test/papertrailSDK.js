'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var papertrailSDK = require('../papertrailSDK');


lab.experiment('utils.papertrailSDK', function() {

	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm){
		mitm.on('connection', function(socket) {
			socket.destroy();
		});
	});

	lab.test('should fail on https request', function(done){
		papertrailSDK.search('test', function(err){
			testingTools.expectError(err, 'pt-query-inv');
			done();
		});
	});
});

lab.experiment('utils.papertrailSDK', function() {

	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.setHeader('content-type', 'application/json');
		res.statusCode = 200;
		res.end('Test response', 'utf8');
	});

	lab.test('should fail if incorrect response format', function(done){
		papertrailSDK.search('test', function(err){
			testingTools.expectError(err, 'pt-res-inv-frmt');
			done();
		});
	});
});

lab.experiment('utils.papertrailSDK search', function(){

	var v;

	lab.before(function(done){
		v = process.env.PAPERTRAIL_APP_TOKEN;
		done();
	});

	lab.afterEach(function(done){
		process.env.PAPERTRAIL_APP_TOKEN = v;
		done();
	});

	lab.test('should fail on empty request', function(done){
		papertrailSDK.search('', function(err){
			testingTools.expectError(err, 'pt-query-inv');
			done();
		});
	});

	lab.test('should fail if missing PAPERTRAIL_APP_TOKEN', {timeout: 5000},  function(done){
		delete process.env.PAPERTRAIL_APP_TOKEN;
		papertrailSDK.search('test', function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.data.debuggingData.papertrailError).to.be.string();
			done();
		}, {});
	});

	lab.test('should work', {timeout: 10000}, function(done) {
		var query = 'info';
		papertrailSDK.search(query, function (err, res) {
			testingTools.code.expect(err).to.equal(null);
			testingTools.code.expect(res).to.be.object();
			papertrailSDK.search = function(q, cb, a){
				//check if next params correct
				testingTools.code.expect(a.min_id).to.equal(res.Response.max_id);
				cb(null);
			};
			res.next(function(){
				papertrailSDK.search = function(q, cb, a){
					//check if prev params correct
					testingTools.code.expect(a.max_id).to.equal(res.Response.min_id);
					cb(null);
				};
				res.prev(function(){
					done();
				});
			});

		});
	});
});
