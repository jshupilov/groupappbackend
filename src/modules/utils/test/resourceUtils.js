/**
 * Created by jevgenishupilov on 21/01/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var resourceUtils = require('../resourceUtils');

lab.experiment('resourceUtils.formatResource', function(){
	lab.test('should work', function(done){

		var formatted = resourceUtils.formatResource({}, {href: 'somehref'});
		testingTools.code.expect(formatted).to.include(['href']);
		testingTools.code.expect(formatted.href).to.equal('somehref');
		done();
	});
});
