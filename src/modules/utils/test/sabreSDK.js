'use strict';

var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var soap = require('soap');
var sabreSDK = require('../sabreSDK');
var attr = {
	HotelReadRequest: {
		attributes: {
			HotelCode: 'test-data',
			ChainCode: 'test-data'
		},
		UserID: {
			attributes: {
				ID: 'test-data',
				PinNumber: 'test-data'
			}
		}
	}
};



lab.experiment('utils.sabreSDK', function() {
	var soapClient, e1, e2;
	lab.before(function (done) {
		soapClient = soap.createClient;
		e1 = process.env.SABRESDK_USER_NAME;
		e2 = process.env.SABRESDK_USER_PASSWORD;
		//use some creds
		process.env.SABRESDK_USER_NAME = 'blabla';
		process.env.SABRESDK_USER_PASSWORD = 'blabla';
		done();
	});
	lab.afterEach(function (done) {
		soap.createClient = soapClient;
		process.env.SABRESDK_USER_NAME = e1;
		process.env.SABRESDK_USER_PASSWORD = e2;
		done();
	});

	lab.after(function(done){
		done();
	});

	lab.test('soap createClient should fail, given function dose not exist', function(done) {

		sabreSDK.soapCall('test-data', attr, function(err){
			testingTools.expectError(err, 'sb-call-inv');

			done();
		});
	});
	lab.test('soap createClient should fail on fetching url', function(done) {
		soap.createClient = function (url, cb) {
			cb(new Error('Error'));
		};
		sabreSDK.soapCall('ReadReservations', attr, function(err){
			testingTools.expectError(err, 'sb-call-inv');

			done();
		});
	});


	lab.test('soap createClient call should work', function(done) {
		soap.createClient = function (url, cb) {
			cb(null, {
				addSoapHeader: function(){

				},
				ReadReservations: function(attr, cb2){
					cb2(null, {});
				}
			});
		};
		sabreSDK.getBookings('test-data', 'test-data', 'test-data', 'test-data', function(err, result){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(result).to.be.object();
			done();
		});
	});

	lab.test('should fail with too many retries, soap call', function(done) {
		soap.createClient = function (url, cb) {
			cb(null, {
				addSoapHeader: function(){

				},
				ReadReservations: function(attr, cb2){
					cb2(true, {});
				}
			});
		};
		sabreSDK.soapCall('ReadReservations', attr, function(e) {
			testingTools.expectError(e, 'sb-res-err');
			testingTools.code.expect(e.data.debuggingData.info).to.equal('too many retries');

			done();
		}, 1000, 10);
	});
});

lab.experiment('utils.sabreSDK with error response', function() {
	testingTools.mockResponsesForExperiment(lab, function(req, res){
		res.end('<?xml version="1.0"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s0="http://www.opentravel.org/OTA/2003/05"               xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:tns="http://synxis.com/OTA2004AService/"               xmlns:s1="http://htng.org/1.1/Header/">    <soap:Header>        <h:HTNGHeader xmlns:h="http://htng.org/1.1/Header/" xmlns="http://htng.org/1.1/Header/">            <h:From>                <h:Credential>                    <h:userName>blabla</h:userName>                    <h:password>blabla</h:password>                </h:Credential>            </h:From>        </h:HTNGHeader>    </soap:Header>    <soap:Body>        <s0:OTA_HotelResRS xmlns:s0="http://www.opentravel.org/OTA/2003/05" xmlns="http://www.opentravel.org/OTA/2003/05">            <s0:Errors>                <Error Code="497" Type="1" ShortText="Authorization_Error" />            </s0:Errors>        </s0:OTA_HotelResRS>    </soap:Body></soap:Envelope>');
	});

	lab.test('soap createClient call should fail with wrong attr', {timeout: 10000}, function(done) {

		sabreSDK.getBookings('test-data', 'test-data', 'test-data', 'test-data', function(err){
			testingTools.expectError(err, 'sb-res-err');
			testingTools.code.expect(err.data.debuggingData.fullResult, JSON.stringify(err)).to.be.string();
			testingTools.code.expect(err.data.debuggingData.info, JSON.stringify(err)).equal('error returned');
			done();
		});
	});
});
lab.experiment('utils.sabreSDK CONNECTION', function() {
	var bypass = false;

	testingTools.mockResponsesForExperiment(lab, null, function(mitm){
		mitm.on('connect', function(socket){
			if(bypass){
				setTimeout(socket.bypass(), 100);
			}
		});
		mitm.on('connection', function(socket) {
			socket.destroy();
		});

	});

	lab.test('should give network error', function(done){
		sabreSDK.getBookings(null, null, null, null, function(err){
			testingTools.expectError(err, 'sb-res-err');
			testingTools.code.expect(err.data.statusCode).to.equal(500);
			testingTools.code.expect(err.data.debuggingData.originalCode).to.equal('ECONNRESET');
			testingTools.code.expect(err.data.debuggingData.info).to.equal('too many retries');

			done();
		});
	});


	lab.test('should give timeout error', function(done){
		bypass = true;

		sabreSDK.soapCall('ReadReservations', attr, function(err){
			testingTools.code.expect(err.data.debuggingData.originalError).to.equal('ETIMEDOUT');
			done();
		}, 10);
	});
});
