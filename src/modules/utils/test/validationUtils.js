'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var validationUtils = require('../validationUtils');
var q = require('q');

lab.experiment('validationUtils', function(){
	var getObject, dateKeys, requiredKeys, requiredTypes, prefix;
	lab.before(function(done){
		prefix = 'validationUtils.';
		getObject = function(){
			var data = {
				stringKey: 'string',
				objectKey: {},
				numberKey: 34,
				arrayKey: [],
				dateKeyOne: '2015-06-01T10:16:16.568Z',
				dateKeyTwo: '2015-06-02T10:16:16.568Z',
				dateKeyThree: '2015-06-06T10:16:16.568Z',
				localized: [{
					stringKey: 'string',
					objectKey: {},
					numberKey: 34,
					arrayKey: [],
					dateKeyOne: '2015-06-01T10:16:16.568Z'
				}, {
					stringKey: 'string',
					objectKey: {},
					numberKey: 34,
					arrayKey: [],
					dateKeyOne: '2015-06-01T10:16:16.568Z'
				}]
			};
			return JSON.parse(JSON.stringify(data));
		};
		dateKeys = ['dateKeyOne', 'dateKeyTwo', 'dateKeyThree'];
		requiredTypes = ['string', 'object', 'number', 'array', 'string'];
		requiredKeys = ['stringKey', 'objectKey', 'numberKey', 'arrayKey', 'dateKeyOne'];
		done();
	});
	lab.afterEach(function(done){
		requiredKeys = ['stringKey', 'objectKey', 'numberKey', 'arrayKey', 'dateKeyOne'];

		done();
	});
	lab.test('validateHasKeys should given error if required key is missing', function(done){
		var promises = [];
		var validate = function(keys, obj, key, p){
			validationUtils.validateHasKeys(keys, obj, function(err){
				p.resolve();
				testingTools.code.expect(err).to.equal(key);
			});
		};
		for(var i in requiredKeys){
			var key = requiredKeys[i];
			var p = q.defer();
			var data = getObject();
			delete data[key];
			validate(requiredKeys, data, key, p);
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('validateHasKeys should work', function(done){
		validationUtils.validateHasKeys(requiredKeys, getObject(), function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('validateTypeOfKeys should given error if object key types is not equal to required list of types', function(done){
		var promises = [];
		var error = 'validateTypeOfKeys-error';
		var validate = function(keys, types, obj, pref, errCode, key, p){
			validationUtils.validateTypeOfKeys(keys, types, obj, pref, errCode, function(err){
				p.resolve();
				testingTools.code.expect(err.code, JSON.stringify(err)).to.equal(errCode);
				testingTools.code.expect(err.details.key, JSON.stringify(err)).to.equal(key);
			});
		};

		for(var i in requiredKeys){
			var key = requiredKeys[i];
			var p = q.defer();
			var data = getObject();
			data[key] = undefined;
			validate(requiredKeys, requiredTypes, data, prefix, error, prefix + key, p);
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('validateTypeOfKeys should work', function(done){
		var error = 'validateTypeOfKeys-error';
		validationUtils.validateTypeOfKeys(requiredKeys, requiredTypes, getObject(), prefix, error, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('validateDateKeys should given error if value of key not string', function(done){
		var promises = [];
		var error = 'validateDateKeys-string-error';
		var validate = function(keys, obj, pref, errs, errCode, key, p){
			validationUtils.validateDateKeys(keys, obj, pref, errs, function(err){
				p.resolve();
				testingTools.code.expect(err.code, JSON.stringify(err)).to.equal(errCode);
				testingTools.code.expect(err.details.key, JSON.stringify(err)).to.equal(key);
			});
		};
		for(var i in dateKeys){
			var key = dateKeys[i];
			var p = q.defer();
			var data = getObject();
			data[key] = 23;
			validate(dateKeys, data, prefix, {invString: error}, error, prefix + key, p);
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('validateDateKeys should given error if value of key is empty string', function(done){
		var promises = [];
		var error = 'validateDateKeys-empty-string-error';
		var validate = function(keys, obj, pref, errs, errCode, key, p){
			validationUtils.validateDateKeys(keys, obj, pref, errs, function(err){
				p.resolve();
				testingTools.code.expect(err.code, JSON.stringify(err)).to.equal(errCode);
				testingTools.code.expect(err.details.key, JSON.stringify(err)).to.equal(key);
			});
		};
		for(var i in dateKeys){
			var key = dateKeys[i];
			var p = q.defer();
			var data = getObject();
			data[key] = '';
			validate(dateKeys, data, prefix, {notEmpty: error}, error, prefix + key, p);
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('validateDateKeys should given error if value of key can not be convert to date', function(done){
		var promises = [];
		var error = 'validateDateKeys-inv-date-error';
		var validate = function(keys, obj, pref, errs, errCode, key, p){
			validationUtils.validateDateKeys(keys, obj, pref, errs, function(err){
				p.resolve();
				testingTools.code.expect(err.code, JSON.stringify(err)).to.equal(errCode);
				testingTools.code.expect(err.details.key, JSON.stringify(err)).to.equal(key);
			});
		};
		for(var i in dateKeys){
			var key = dateKeys[i];
			var p = q.defer();
			var data = getObject();
			data[key] = 'not date time';
			validate(dateKeys, data, prefix, {invDate: error}, error, prefix + key, p);
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('validateDateKeys should work', function(done){
		validationUtils.validateDateKeys(dateKeys, getObject(), prefix, {}, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('validateLocalizedOrDefaultKeys should work if one of key miss in object and in localized', function(done){
		var promises = [];

		var validate = function(keys, obj, loc, key, p){
			validationUtils.validateLocalizedOrDefaultKeys(keys, obj, loc, function(err){
				p.resolve();
				testingTools.code.expect(err).to.be.null();
			});
		};
		requiredKeys = [['numberKey', 'arrayKey'], 'stringKey', 'objectKey', 'dateKeyOne'];
		for(var i in requiredKeys[0]){
			var key = requiredKeys[0][i];
			var p = q.defer();
			var data = getObject();
			delete data[key];
			validate(requiredKeys, data, data, key, p);
		}
		q.all(promises).done(function(){
			done();
		});
	});
	lab.test('validateLocalizedOrDefaultKeys should work if all of array keys are missing in object and in localized', function(done){

		requiredKeys = [['numberKey', 'arrayKey'], 'stringKey', 'objectKey', 'dateKeyOne'];
		validationUtils.validateLocalizedOrDefaultKeys(requiredKeys, {}, {}, function(err){
			testingTools.code.expect(err, JSON.stringify(err)).to.equal(requiredKeys[0].join(','));
			done();
		});
	});
	lab.test('validateLocalizedOrDefaultKeys should given error if key miss in object and in localized', function(done){
		var promises = [];

		var validate = function(keys, obj, loc, key, p){
			validationUtils.validateLocalizedOrDefaultKeys(keys, obj, loc, function(err){
				p.resolve();
				testingTools.code.expect(err, JSON.stringify(err)).to.equal(key);
			});
		};

		for(var i in requiredKeys){
			var key = requiredKeys[i];
			var p = q.defer();
			var data = getObject();
			delete data[key];
			validate(requiredKeys, data, data, key, p);
		}
		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('validateLocalizedOrDefaultKeys should work ', function(done){
		var data = getObject();
		validationUtils.validateLocalizedOrDefaultKeys(requiredKeys, data, data, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('validateWithArrayOfFunctionsWithCallbacks should given error if one of function return error', function(done){
		var validationFunctions = [
			function(item, cb){
				cb(null);
			},
			function(item, cb){
				cb(new Error('Error validateWithArrayOfFunctionsWithCallbacks'));
			}
		];
		validationUtils.validateWithArrayOfFunctionsWithCallbacks(validationFunctions, [getObject], function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error validateWithArrayOfFunctionsWithCallbacks');
			done();
		});
	});

	lab.test('validateWithArrayOfFunctionsWithCallbacks should work', function(done){
		var validationFunctions = [
			function(item, cb){
				cb(null);
			},
			function(item, cb){
				cb(null);
			}
		];
		validationUtils.validateWithArrayOfFunctionsWithCallbacks(validationFunctions, [getObject], function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('collectLocalizedOrDefaultKeys should work if one of default keys is missing', function(done){
		var promises = [];

		var validate = function(keys, obj, p){
			p.resolve();
			var collection = validationUtils.collectLocalizedOrDefaultKeys(keys, obj);
			testingTools.code.expect(collection, JSON.stringify(collection)).to.be.object();
		};

		for(var i in requiredKeys){
			var key = requiredKeys[i];
			var p = q.defer();
			var data = getObject();
			delete data[key];
			validate(requiredKeys, data, p);
		}
		q.all(promises).done(function(){
			done();
		});
	});
	lab.test('collectLocalizedOrDefaultKeys should work if one of localized[x] keys is missing', function(done){
		var promises = [];

		var validate = function(keys, obj, p){
			p.resolve();
			var collection = validationUtils.collectLocalizedOrDefaultKeys(keys, obj);
			testingTools.code.expect(collection, JSON.stringify(collection)).to.be.object();
		};
		var localized = getObject().localized;
		for(var i in requiredKeys){
			for(var li in localized){
				var key = requiredKeys[i];
				var p = q.defer();
				var data = getObject();
				delete data.localized[li][key];
				validate(requiredKeys, data, p);
			}
		}
		q.all(promises).done(function(){
			done();
		});
	});
	lab.test('validateArray should given error if itemValidatorCallback is invalid', function(done){
		var itemValidatorCallback = function(item, index, cb){
			cb(new Error('Error validateArray'));
		};
		validationUtils.validateArray(getObject(), itemValidatorCallback, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Error validateArray');
			done();
		});
	});
	lab.test('validateArray should work', function(done){
		var itemValidatorCallback = function(item, index, cb){
				cb(null);
		};
		validationUtils.validateArray(getObject(), itemValidatorCallback, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
});

lab.experiment('Utils.isoString validations', function(){
	var dateString = new Date().toISOString();
	var wrongString = '20153-13-37T25:61:61.610U';
	lab.test('Validate iso string with correct date', function(done){
		validationUtils.validateISODate(dateString, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('Validate iso string with wrong date', function(done){
		validationUtils.validateISODate(wrongString, function(err){
			testingTools.expectError(err, 'val-inv-iso-date');
			done();
		});
	});
	lab.test('Validate iso string with wrong string', function(done){
		validationUtils.validateISODate('thisiswrongstring', function(err){
			testingTools.expectError(err, 'val-inv-iso-date');
			done();
		});
	});
});
