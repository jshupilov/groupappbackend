'use strict';
var check = require('check-types');
var q = require('q');
var errors = require('../utils/errors');
errors.defineError('val-inv-iso-date', 'Input error', 'Invalid date format', 'Utils.Validation', 400);
/**
 * Validates if some key is missing from the object.
 * Calls callback with field name, which is missing or null, if all OK
 * @param requiredKeys
 * @param object
 * @param callback
 * @returns {*}
 */
exports.validateHasKeys = function(requiredKeys, object, callback){
	for( var ki in requiredKeys ){
		if( !object.hasOwnProperty(requiredKeys[ki]) ){
			return callback(requiredKeys[ki]);
		}
	}
	return callback(null);
};

/**
 * Validates if some key from the object has correct type .
 * Calls callback with field name, which is missing or null, if all OK
 * @param requiredKeys
 * @param object
 * @param callback
 * @returns {*}
 */
exports.validateTypeOfKeys = function(keys, types, object, prefix, error, callback){
	for(var i = 0; i < keys.length; i++ ){
		if (types[i] === 'array') {
			if (!Array.isArray(object[keys[i]])) {
				return callback({
					code: error,
					details: {
						key: prefix + keys[i],
						value: object[keys[i]],
						type: types[i],
						givedType: typeof object[keys[i]]
					}
				});
			}
		} else if (typeof object[keys[i]] !== types[i]) {
			return callback({
				code: error,
				details: {
					key: prefix + keys[i],
					value: object[keys[i]],
					type: types[i],
					givedType: typeof object[keys[i]]
				}
			});
		}
	}
	return callback(null, object);
};

/**
 * Validates if some key is missing from the object.
 * Calls callback with field name, which is missing or null, if all OK
 * @param dateKeys
 * @param object
 * @param prefix
 * @param errors
 * @param callback
 * @returns {*}
 */
exports.validateDateKeys = function(dateKeys, object, prefix, errors, callback){
	for(var i = 0; i < dateKeys.length; i++ ){
		var key = dateKeys[i];
		var val = object[key];
		if (!check.string(val)) {
			return callback({
				code: errors.invString,
				details: {
					key: prefix + key,
					value: val,
					type: typeof val
				}
			});
		} else if(!check.unemptyString(val)) {
			return callback({
				code: errors.notEmpty,
				details: {
					key: prefix + key,
					value: val,
					type: typeof val
				}
			});
		} else if (!check.date(new Date(val))) {
			return callback({
				code: errors.invDate,
				details: {
					key: prefix + key,
					value: val,
					type: typeof val
				}
			});
		}
	}
	return callback(null);
};

/**
 * Validate if localized object has given fields (or if fields exist in defaults object)
 * @param requiredKeys
 * @param object Defaults object
 * @param localizedObj
 * @param callback
 * @returns {*}
 */
exports.validateLocalizedOrDefaultKeys = function(requiredKeys, object, localizedObj, callback){

	for( var ki in requiredKeys ){

		//if array - means one or other must exist
		if(Array.isArray(requiredKeys[ki])){
			var has = false;

			for(var oi in requiredKeys[ki]){

				if( object.hasOwnProperty(requiredKeys[ki][oi]) || localizedObj.hasOwnProperty(requiredKeys[ki][oi]) ){
					has = true;
				}
			}
			if(!has){
				return callback(requiredKeys[ki].join(','));
			}

		}else{

			if( !object.hasOwnProperty(requiredKeys[ki]) && !localizedObj.hasOwnProperty(requiredKeys[ki]) ){
				return callback(requiredKeys[ki]);
			}
		}
	}
	return callback(null);
};

/**
 * Do validation with array of functions, where last argument must be a callback,
 * which is called with error as first argument.
 *
 * @param validationFunctions {Function[]} Array of functions, where last argument is callback
 * @param validationFunctionArguments {*[]} Array of arguments, where last argument (callback) is NOT SET
 * @param callback Final callback, which is called if a first validation fails or if all pass
 */
exports.validateWithArrayOfFunctionsWithCallbacks = function(validationFunctions, validationFunctionArguments, callback){

	var index = 0;

	function valCaller(){
		var args = [];
		validationFunctionArguments.forEach(function(val){
			args.push(val);
		});

		//add last argument as a callback
		args.push(function(err){
			if(err){
				return callback(err);
			}else if( validationFunctions[index+1] !== undefined ){
				index++;
				valCaller();
			}else{
				return callback(null);
			}
		});
		//call the validation function
		validationFunctions[index].apply(this, args);

	}

	valCaller();

};

/**
 *
 * @param keys {Array}
 * @param object {Object} with localized
 */
exports.collectLocalizedOrDefaultKeys = function(keys, object){
	var collectedKeys = {};
	for(var ki in keys){
		collectedKeys[keys[ki]] = [];
		if(object.hasOwnProperty(keys[ki])){
			collectedKeys[keys[ki]].push([object[keys[ki]], keys[ki]]);
		}
	}
	for( var li in object.localized ){

		for(var ki2 in keys){
			if(object.localized[li].hasOwnProperty(keys[ki2])){
				collectedKeys[keys[ki2]].push([object.localized[li][keys[ki2]], 'localized[' + li +'].' + keys[ki2]]);
			}
		}
	}
	return collectedKeys;
};

/**
 *
 * @param array Array|Object of items
 * @param itemValidatorCallback function(item, callback){}
 * @param finalCallback function(err){}
 */
exports.validateArray = function(array, itemValidatorCallback, finalCallback){
	var promises = [];
	function doStuff(p, i){
		itemValidatorCallback(array[i], i, function(err){
			if(err){
				return p.reject(err);
			}
			p.resolve();
		});
	}
	for(var i in array){
		var p = q.defer();
		promises.push(p.promise);
		doStuff(p, i);
	}
	q.all(promises).done(function(){
		finalCallback(null);
	}, function(err){
		finalCallback(err);
	});
};
/**
 *
 * @param instance Array|Object of items
 * @param keys Array of keys
 * @param values Array of values
 * @param itemValidatorCallback function(item, callback){}
 * @param finalCallback function(err){}
 */
exports.skipValidationIfKeysEqualTo = function(instance, keys, values, itemValidatorCallback, finalCallback){
	var promises = [];
	function doStuff(p, key){
		itemValidatorCallback(key, function(err){
			if(err){
				return p.reject(err);
			}
			p.resolve();
		});
	}
	for(var i in keys){
		var p = q.defer();
		promises.push(p.promise);
		if(instance[keys[i]] !== values[i]){
			doStuff(p, keys[i]);
		} else {
			p.resolve();
		}
	}
	q.all(promises).done(function(){
		finalCallback(null);
	}, function(err){
		finalCallback(err);
	});
};

/**
 * Is dateString valid ISO format.
 * @param dateString
 * @param callback
 */
exports.validateISODate = function(dateString, callback){
	var errorCode = 10;
	var tz = dateString.substring(dateString.length - 1, dateString.length);
	var origDate = dateString;
	dateString = dateString.substring(0, dateString.length - 1);
	var parts = dateString.split('T');

	if (parts.length === 2) {
		errorCode=0;
		var d = parts[0];
		var t = parts[1];
		var dparts = d.split('-');
		var tparts = t.split(':');

		//Check date parts
		//Check year
		if (!check.hasLength(dparts[0], 4)) {
			errorCode++;
		}
		//Check month
		if(!check.inRange(parseInt(dparts[1]), 1, 12 )){
			errorCode++;
		}
		//Check day
		if(!check.inRange(parseInt(dparts[2]), 1, 31 )){
			errorCode++;
		}
		//Check time parts
		//Check hour
		if(!check.inRange(parseInt(tparts[0]), 0, 23)){
			errorCode++;
		}
		//Check minute
		if(!check.inRange(parseInt(tparts[1]), 0, 59)){
			errorCode++;
		}
		//Check second
		if(!check.inRange(parseInt(tparts[2]), 0, 59)){
			errorCode++;
		}
		//Check Tz
		if(!check.match(tz, 'Z')){
			errorCode++;
		}
	}
	if(errorCode>0){
		return callback(errors.newError('val-inv-iso-date', {string: origDate, sample: '2015-12-03T13:09:40.610Z'}));
	}
	callback(null);
};
