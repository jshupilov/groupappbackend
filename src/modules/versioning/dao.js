'use strict';
var databaseUtils = require('../utils/databaseUtils');
var escape = require('pg-escape');
var errors = require('../utils/errors');

errors.defineError('ver-no-prev', 'No previous version', 'Previous version not found in system', 'Versioning.Services');

/**
* Create entry in versioned table
 * @param data
 * @param tableName
 * @param callback
 */

exports.create = function(data, tableName, callback){
    databaseUtils.getConnection(function(err, client, done){
        if(err){
            return callback(err);
        }
        var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', tableName, JSON.stringify(data));
        client.query(query, function(err, res){
            done();
            callback(err, res);
        });
    });
};

exports.update = function(item, tableName, callback){
	databaseUtils.getConnection(function(err, client, done){
		if(err){
			return callback(err);
		}
		var query = escape('UPDATE %I SET data = %L WHERE id=%L', tableName, JSON.stringify(item.data), item.id);
		client.query(query, function(err, res){
			done();
			callback(err, res);
		});
	});
};

/**
 * Get all versioned items what may (or may not) go to live.
 * Thus items where scheduledAt has some date and appliedAt has no date
 * @param tableName
 * @param callback
 */
exports.getDataToLive = function(tableName, callback){
  databaseUtils.getConnection(function(err, client, done){
    if(err){
      return callback(err);
    }
    var query = escape("SELECT id, data FROM %I WHERE  (data ->'meta'->'scheduledAt') != 'null' AND (data ->'meta'->'appliedAt') = 'null'", tableName);
    client.query(query, function(err, res){
      done();
      callback(err, res);
    });
  });
};

/**
 * Checks wether GUID allready exists or not.
 * @param table
 * @param guid
 * @param callback
 */
exports.checkIfGuidExists = function(table, guid, callback){
	databaseUtils.getConnection(function(err, client, done){
		if(err){
			return callback(err);
		}
		var query = escape("SELECT id, data FROM %I WHERE  (data ->'meta'->'guid') = %L", table, '"' + guid + '"');
		client.query(query, function(err, res){
			done();
			if(err){
				return callback(err);
			}
			if(res.rowCount >0){
				return callback(null, true);
			}
			callback(null, false);
		});
	});
};

/**
 * Get latest versioned item from database
 * @param guid
 * @param table
 * @param callback
 */
exports.getLatestVersion = function(guid, table, callback){
	databaseUtils.getConnection(function(err, client, done){
		if(err){
			return callback(err);
		}
		var query = escape("SELECT id, data FROM %I WHERE  (data ->'meta'->'guid') = %L ORDER BY id DESC LIMIT 1", table, '"' + guid + '"');
		client.query(query, function(err, res){
			done();
			if(err){
				return callback(err);
			}
			if(res.rowCount > 0){
				callback(err, res.rows[0].data);
			}else{
				callback(errors.newError('ver-no-prev'));
			}
		});
	});
};

/**
 * Get latest versioned item from database
 * @param guid
 * @param table
 * @param offset {Number} How many versions to go back
 * @param callback
 */
exports.getPreviousVersion = function(guid, table, offset, callback){
	databaseUtils.getConnection(function(err, client, done){
		if(err){
			return callback(err);
		}
		var query = escape("SELECT id, data FROM %I WHERE  (data ->'meta'->'guid') = %L ORDER BY id DESC LIMIT 1 OFFSET $1", table, '"' + guid + '"');
		client.query(query, [offset], function(err, res){
			done();
			if(err){
				return callback(err);
			}
			if(res.rowCount > 0){
				callback(err, res.rows[0].data);
			}else{
				callback(errors.newError('ver-no-prev'));
			}
		});
	});
};
