'use strict';
var escape = require('pg-escape');
exports.makeVersion = function (table, pgm, callback) {
	var verTbl = table + '_v';
	var verSeq = verTbl + '_id_seq';

	pgm.sql(escape('CREATE TABLE %I ( LIKE %I INCLUDING DEFAULTS INCLUDING CONSTRAINTS INCLUDING INDEXES);', verTbl, table));
	pgm.sql(escape('ALTER TABLE %I ALTER id DROP DEFAULT;', verTbl));
	pgm.sql(escape('CREATE sequence %I;', verSeq));
	pgm.sql(escape('ALTER TABLE %I ALTER ID SET DEFAULT nextval(%L);', verTbl, verSeq));

	callback();
};

exports.dropVersions = function (table, pgm, callback) {
	var verTbl = table + '_v';
	var verSeq = verTbl + '_id_seq';
	pgm.sql(escape('DROP TABLE %I;', verTbl));
	pgm.sql(escape('DROP SEQUENCE %I;', verSeq));
	callback();
};

exports.versionIdx = function(table, pgm, callback){
	var verTbl = table + '_v';
	pgm.createIndex(verTbl, "(data -> 'meta' -> 'guid'::text)", {
			name: verTbl + '_guid_idx',
			method: 'btree'
		}
	);
	callback();
};
