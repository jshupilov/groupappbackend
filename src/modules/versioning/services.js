'use strict';
var dao = require('./dao');
var errors = require('../utils/errors');
var logging = require('../utils/logging');
var eventUtils = require('../utils/eventUtils');
var hotel = require('../hotels/services');
var hotelgroup = require('../hotel_groups/services');
var q = require('q');
var validation = require('./validation');
var jsondiffpatch = require('jsondiffpatch').create({});

errors.defineError('ver-no-entity', 'Entity not specified', 'Entity not specified', 'Versioning.Services');
errors.defineError('ver-no-status', 'Unknown item status', 'Status missing in item data', 'Versioning.Services');
errors.defineError('ver-unk-table', 'Unknown table', 'Unknown table specified for add item', 'Versioning.Services');
errors.defineError('ver-no-change', 'No changes', 'New item is same as old one', 'Versioning.Services');

function removeKeys(data){
	var item = JSON.parse(JSON.stringify(data));
	delete item.meta.createdAt;
	delete item.meta.appliedAt;
	delete item.meta.scheduledAt;
	delete item.publishing;
	return item;
}

/**
 * Check the diff of newly created item
 * @param data
 * @param guid
 * @param table
 * @param callback
 */
exports.checkDiff = function (data, guid, table, callback) {
	dao.getLatestVersion(guid, table, function (err, res) {
		if (err) {
			return callback(err);
		}
		var diff = jsondiffpatch.diff(removeKeys(data), removeKeys(res));
		if (diff === undefined) {
			return callback(errors.newError('ver-no-change'));
		} else {
			return callback(null);
		}
	});
};

/**
 * Get the diff between given data and some version of the data
 * @param data {Object}
 * @param guid {String}
 * @param table {String}
 * @param offset {Number} Which version in history, 0 is current, 1 is previous and so on
 * @param callback
 */
exports.getDiff = function (data, guid, table, offset, callback) {
	dao.getPreviousVersion(guid, table, offset, function (err, res) {
		if (err) {
			return callback(err);
		}
		var diff = jsondiffpatch.diff(removeKeys(res), removeKeys(data));
		if (diff === undefined) {
			return callback(errors.newError('ver-no-change'));
		} else {
			return callback(null, diff);
		}
	});
};

/**
 * Generate meta data for an object
 * @param data
 * @param table
 * @param callback
 * @returns {*}
 */

exports.generateMeta = function(data, table, callback){
	var d = new Date();
	var guid;
	//If new entity goes to versioning, add your guid definition here
	switch (table) {
		case 'hotel_v':
			guid = data.hotelId + '_' + data.hotelGroup.hotelGroupId;
			break;
		case 'hotel_group_v':
			guid = data.hotelGroupId;
			break;
		default:
			return callback(errors.newError('ver-unk-table'));
	}

	data.meta.createdAt = d.toISOString();
	data.meta.guid = guid;
	data.meta.appliedAt = null;

	callback(null, data);
};

/**
 * Add version of new item
 * @param data
 * @param table
 * @param callback
 */
exports.addItem = function (data, table, callback) {

	exports.generateMeta(data, table, function(err, data){
		if(err){
			return callback(err);
		}
		validation.validateMetaData(data.meta, table, 'add', function (err) {
			if (err) {
				return callback(err);
			}
			dao.create(data, table, function (err, res) {
				callback(err, res);
			});
		});
	});
};

/**
 * Add version of updated item
 * @param data
 * @param table
 * @param callback
 */
exports.updateItem = function(data, table, callback){
	exports.generateMeta(data, table, function(err, data){
		if(err){
			return callback(err);
		}
		exports.checkDiff(data, data.meta.guid, table, function(err){
			if(err){
				return callback(err);
			}

			validation.validateMetaData(data.meta, table, 'update', function (err) {
				if (err) {
					return callback(err);
				}
				dao.create(data, table, function (err, res) {
					callback(err, res);
				});
			});
		});
	});
};


/**
 * Get latest data that should go to live
 */
exports.getLiveData = function (verTbl, callback) {
	/**
	 * Extract newest unique entries from db result
	 * except those what have scheduledAt in the future
	 * @param source Array with all elements where liveAt is before now
	 * @returns {Array} elements what will go live
	 */
	var grepNewest = function (source) {
		var now = new Date().getTime();
		var result = {};

		for (var i = 0; i < source.length; i++) {
			var base = source[i];
			if (new Date(base.data.meta.scheduledAt).getTime() > now) {
				continue;
			}
			var guid = base.data.meta.guid;
			var compare = result[guid];
			if (typeof compare !== 'undefined') {
				var baseDate = new Date(base.data.meta.createdAt).getTime();
				var compareDate = new Date(compare.data.meta.createdAt).getTime();

				if (compareDate < baseDate) {
					result[guid] = base;
				}
			} else {
				result[guid] = base;
			}
		}
		var data = [];
		for (var k in result) {
			data.push(result[k]);
			//delete ref from original results array, so it will only contain old versions, which should be dumped
			source.splice(source.indexOf(result[k]), 1);
		}
		return data;
	};


	/**
	 * Get versioned data
	 */
	dao.getDataToLive(verTbl, function (err, res) {
		if (err) {
			return callback(err);
		}
		var result = grepNewest(res.rows); //Filter unique elements
		callback(null, result, res.rows);
	});
};


/**
 * Handle golive for specified entity (type), add/update in live, or remove from Live
 * @param verTable
 * @param type typeOfEntity
 * @param callback
 * @param [guids] Array of guids, which only need to be handled
 */
exports.handleGoLive = function (verTable, type, callback, guids) {
	var publish = 0;
	var unpublish = 0;
	exports.getLiveData(verTable, function (err, res, oldVersions) {
		if (err) {
			return callback(err);
		}
		logging.info('Live data selected from DB', res.length);
		var now = new Date();
		var promises = [];
		var doIt = function (p, item) {

			function doNow() {
				logging.info('Go live for', item.id, item.data.meta.guid);
				var status = item.data.meta.status;
				var entityStatusAction = {
					hotel: {
						draft: hotel.unPublishHotel,
						published: hotel.publishHotel
					},
					hotel_group: {
						draft: hotelgroup.unPublishItem,
						published: hotelgroup.publishHotelGroup
					}
				};

				if (!entityStatusAction.hasOwnProperty(type)) {
					return p.reject(errors.newError('ver-no-entity'));
				} else if (!entityStatusAction[type].hasOwnProperty(status)) {
					return p.reject(errors.newError('ver-no-status'));
				}

				logging.info('Go live for', item.id, item.data.meta.guid);

				entityStatusAction[type][status].call(null, item, function (err) {
					if (err) {
						return p.reject(err);
					}
					if (status === 'published') {
						publish++;
					} else {
						unpublish++;
					}
					item = null;
					p.resolve();
				});
			};

			promises[promises.length - 2].then(function () {
				doNow();
			});

		};

		//create a fake promise as first one
		var p1 = q.defer();
		promises.push(p1.promise);
		p1.resolve();

		for (var i = 0; i < res.length; i++) {
			var item = res[i];
			delete res[i];
			if(guids && guids.indexOf(item.data.meta.guid) < 0){
				continue;
			}
			item.data.meta.appliedAt = now.toISOString();
			var p = q.defer();
			promises.push(p.promise);
			doIt(p, item);
			item = null;
		}

		var denyOldVersion = function (p, item) {
			item.data.meta.appliedAt = false;
			logging.info('Deny old version', item.id, item.data.meta.guid);

			dao.update(item, verTable, function(err){
				item = null;
				if(err){
					return p.reject(err);
				}
				p.resolve();
			});
		};

		//dump old versions
		for (var i2 = 0; i2 < oldVersions.length; i2++) {
			var itemOld = oldVersions[i2];
			delete oldVersions[i2];
			if(guids && guids.indexOf(itemOld.data.meta.guid) < 0){
				continue;
			}
			var p2 = q.defer();
			promises.push(p2.promise);
			denyOldVersion(p2, itemOld);
			itemOld = null;
		}

		q.all(promises).done(function () {
			eventUtils.emitter.parallel('versioning.goLiveDone', function() {
				callback(null, [publish, unpublish]);
			});
		}, function (err) {
			eventUtils.emitter.parallel('versioning.goLiveDone', function() {
				callback(err);
			});
		});

	});
};

/**
 * Update the row with appliedAt datetime in versioning table
 * @param item must be already existing version table entry
 * @param versionTableName
 * @param callback
 */
exports.updateVersion = function (item, versionTableName, callback) {
	dao.update(item, versionTableName, function (err) {
		if (err) {
			return callback(err);
		}
		callback(null);
	});
};
