'use strict';
var testingTools = require('../../../test/testingTools');
var databaseUtils = require('../../utils/databaseUtils');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var dao = require('../dao');
var q = require('q');
var escape = require('pg-escape');

lab.experiment('versioning.dao', function () {
  var origGConn;
  var hotelId = Math.floor(Math.random() * 100 + 100);
  var hotelGroupId = Math.floor(Math.random() * 100 + 100);
  var d = new Date();
  var data = {
    'meta': {
      'createdAt': d.toISOString(),
      'scheduledAt': d.toISOString(),
      'appliedAt': null,
      'status': 'draft',
      'guid': Math.floor(Math.random() * 100 + 100)+'_'+Math.floor(Math.random() * 100 + 100)
    },
    'hotelId': hotelId.toString(),
    'hotelGroup': {
      'hotelGroupId': hotelGroupId.toString()
    }
  };
  var tableName = 'hotel_v';
  var doneDb, dBclient;
  lab.before(function (done) {
    databaseUtils.getConnection(function (err, client, doneDb1) {
      testingTools.code.expect(err).to.be.null();
      dBclient = client;
      doneDb = doneDb1;
      dBclient.query('BEGIN TRANSACTION', function (err) {
        testingTools.code.expect(err).to.be.null();
        var query = escape('INSERT INTO %I (data) VALUES(%L)', tableName, JSON.stringify(data));
        dBclient.query(query, function(err, res){
          testingTools.code.expect(err).to.be.null();
	        testingTools.code.expect(res.rowCount).to.be.equal(1);
          done();
        });
      });
    });
    origGConn = databaseUtils.getConnection;

    databaseUtils.getConnection = function () {
      arguments[arguments.length - 1](
        null,
        dBclient,
        function () {
        }
      );
    };
  });

  lab.after(function (done) {
    databaseUtils.getConnection = origGConn;
    dBclient.query('ROLLBACK', function (err) {
      testingTools.code.expect(err).to.be.null();
      doneDb();
      done();
    });
  });

  lab.test('Create data should add db entry', function (done) {
    var doIt = function (p) {
      data.hotelId = 'test-create';
      dao.create(data, tableName, function (err) {
        testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
        p.resolve();
      });
    };
    var promises = [];
    for (var i = 0; i < 4; i++) {
      var p = q.defer();
      promises.push(p.promise);
      doIt(p);
    }

    q.all(promises).done(function () {
      var query = escape("SELECT * FROM %I WHERE (data->'hotelGroup'->'hotelGroupId') = '%I' AND (data->'hotelId')= '%I'", tableName, data.hotelGroup.hotelGroupId, 'test-create');
      dBclient.query(query, function (err, result) {
          testingTools.code.expect(err).to.be.null();
          testingTools.code.expect(result.rows).to.be.length(4);
          done();
        }
      );
    });
  });
  lab.test('Should give some data to put live', function (done) {

    dao.getDataToLive(tableName, function (err, res) {
      testingTools.code.expect(err).to.be.null();
      testingTools.code.expect(res.rows.length).to.be.above(0);
      for (var i = 0; i < res.rows.length; i++) {
        testingTools.code.expect(res.rows[i].data.meta.appliedAt).to.be.equal(null);
        testingTools.code.expect(res.rows[i].data.meta.scheduledAt).not.equal(null);
        testingTools.code.expect(res.rows[i].data.meta.guid).not.undefined();
      }
      done();
    });
  });
});

lab.experiment('versioning.dao.noDb', function () {
  var tableName = 'hotel_v';
  testingTools.killDbConnectionForExperiment(lab);

  lab.test('Create entry should fail big time', function (done) {
    dao.create(null, null, function (err) {
      testingTools.expectError(err);
      done();
    });
  });
  lab.test('Get data should fail', function (done) {
    dao.getDataToLive(tableName, function (err) {
      testingTools.expectError(err);
      done();
    });
  });
	lab.test('Update should fail', function(done){
		dao.update({}, 'someTable', function(err){
			testingTools.expectError(err);
			done();
		});
	});
	lab.test('Get last version should give DB error', function(done){
		dao.getLatestVersion('', '', function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('Get previous version should give DB error', function(done){
		dao.getPreviousVersion('', '', 0, function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('Check Guid should give an error', function(done){
		dao.checkIfGuidExists('table', 'guid', function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});

lab.experiment('versioning.dao.update', function(){
	lab.test('should work', function(done) {
		dao.update({id: '234242342342', data: {}}, 'hotel', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.rowCount).to.equal(0);
			done();
		});
	});
});

lab.experiment('versioning.dao.checkGuid', function(){
	var table = 'hotel_v';
	lab.test('Check existing guid', function(done){
		dao.checkIfGuidExists(table, 'gpr_mchotels', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.equal(true);
			done();
		});
	});
	lab.test('Check non existing guid', function(done){
		dao.checkIfGuidExists(table, 'some_other_guid', function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.equal(false);
			done();
		});
	});
});
lab.experiment('versioning.dao.woDb', function(){
	var table = 'hotel_v';
	testingTools.failDbQueryForExperiment(lab, table);
	lab.test('Check guid should give DB error', function(done){
		dao.checkIfGuidExists(table, 'gpr_mchotels', function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();
		});
	});

});
lab.experiment('versioning.dao.getLatestVer', function(){
	var tableName = 'hotel_v';
	var hotelId = Math.floor(Math.random() * 100 + 100);
	var hotelGroupId = Math.floor(Math.random() * 100 + 100);
	var guid = hotelId.toString()+'_'+hotelGroupId.toString();
	var d = new Date();
	var data = {
		'meta': {
			'createdAt': d.toISOString(),
			'scheduledAt': d.toISOString(),
			'appliedAt': null,
			'status': 'draft',
			'guid': guid
		},
		'hotelId': hotelId.toString(),
		'hotelGroup': {
			'hotelGroupId': hotelGroupId.toString()
		}
	};
	var itemId;
	lab.before(function(done){
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', tableName, JSON.stringify(data));
			client.query(query, function(err, res){
				testingTools.code.expect(err).to.be.null();
				itemId = res.rows[0].id;
				doneDb();
				done();
			});
		});
	});
	lab.after(function(done){
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM %I WHERE id = %L', tableName, itemId);
			client.query(query, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				doneDb();
				done();
			});
		});
	});
	lab.test('Get latest version should offer one', function(done){
		dao.getLatestVersion(guid, tableName, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.deep.equal(data);
			done();
		});
	});
	lab.test('Get non exixtent version should give error', function(done){
		dao.getLatestVersion('non-exist-guid', tableName, function(err){
			testingTools.expectError(err, 'ver-no-prev');
			done();
		});
	});

	lab.test('Get previous version should offer one', function(done){
		dao.getPreviousVersion(guid, tableName, 0, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.deep.equal(data);
			done();
		});
	});
	lab.test('Get previous version should give error', function(done){
		dao.getPreviousVersion('non-exist-guid', tableName, 0, function(err){
			testingTools.expectError(err, 'ver-no-prev');
			done();
		});
	});

});

lab.experiment('versioning.dao.invDb', function() {
	testingTools.failDbQueryForExperiment(lab);

	lab.test('getLatestVersion should give query error', function(done) {
		dao.getLatestVersion('12313213131', 'hotel', function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();

		});
	});

	lab.test('getPreviousVersion should give query error', function(done) {
		dao.getPreviousVersion('12313213131', 'hotel', 1, function(err){
			testingTools.expectError(err, 'db-query-fails');
			done();

		});
	});
});
