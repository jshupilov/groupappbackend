'use strict';
var testingTools = require('../../../test/testingTools');
var databaseUtils = require('../../utils/databaseUtils');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var migr = require('../migrations');
var escape = require('pg-escape');
var q = require('q');
//var apiUtils = require('../../utils/apiUtils');

lab.experiment('versioning.migrations', function(){
  var pgm;
  var tableName = 'dummy_test2';
  var schema = 'public';

  testingTools.startTransactionAndCreateDummyTableForExperiment(lab, tableName);

  pgm = {
    queries: [],
    sql: function(sql){
      pgm.queries.push(sql);
    },
    doIt: function(q, p){
      databaseUtils.getConnection(function(err, client){
        testingTools.code.expect(err).to.be.null();
        client.query(q, function (err) {
          testingTools.code.expect(err).to.be.null();
          p.resolve();
        });
      });
    },
	  createIndex: function(table, field, params){
		  var query = escape('CREATE INDEX %I ON %I (%s)', params.name, table, field);
		  databaseUtils.getConnection(function(err, client, doneDb){
				client.query(query, function(){
					testingTools.code.expect(err).to.be.null();
					doneDb();
				});
		  });
	  }
  };


  lab.test('Make new table, should create one', function(done){
    migr.makeVersion(tableName, pgm, function(){
      var promises = [];
      for( var qi in pgm.queries){
        var p = q.defer();
        promises.push(p.promise);
        pgm.doIt(pgm.queries[qi], p);
      }
      q.all(promises).done(function(){
        pgm.queries=[];
        databaseUtils.getConnection(function(err, client){
          testingTools.code.expect(err).to.be.null();
          client.query(
            'SELECT EXISTS (' +
            'SELECT 1 ' +
            'FROM information_schema.tables ' +
            'WHERE table_schema= $1 ' +
            'AND table_name= $2)',
            [schema, tableName+'_v'],
            function(err, result){
              testingTools.code.expect(err).to.be.null();
              testingTools.code.expect(result.rows[0].exists).to.equal(true);
              done();
            });
        });

      });
    });
  });
	lab.test('Add index', function(done){
		migr.versionIdx(tableName, pgm, function(){
			databaseUtils.getConnection(function(err, client, doneDb){
				testingTools.code.expect(err).to.be.null();
				var query = escape('SELECT * FROM pg_indexes WHERE indexname = %L', tableName+'_v_guid_idx');
				client.query(query, function(err, res){
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.equal(1);
					doneDb();
					done();
				});
			});
		});
	});
  lab.test('Remove versioning should drop table', function(done){
    migr.dropVersions(tableName, pgm, function(){
      var promises = [];
      for(var qi in pgm.queries){
        var p = q.defer();
        promises.push(p.promise);
        pgm.doIt(pgm.queries[qi], p);
      }

      q.all(promises).done(function(){
        pgm.queries=[];
        databaseUtils.getConnection(function (err, client) {
          testingTools.code.expect(err).to.be.null();
          client.query(
            'SELECT EXISTS (' +
            'SELECT 1 ' +
            'FROM information_schema.tables ' +
            'WHERE table_schema= $1 ' +
            'AND table_name= $2)',
            [schema, tableName+'_v'],
            function(err, result){
              testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
              testingTools.code.expect(result.rows[0].exists).to.equal(false);
              done();
            });
        });
      });
    });
  });


});
