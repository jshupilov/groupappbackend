'use strict';
var testingTools = require('../../../test/testingTools');
var databaseUtils = require('../../utils/databaseUtils');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var services = require('../services');
var escape = require('pg-escape');
var htlSrv = require('../../hotels/services');
var htlDao = require('../../hotels/dao');
var hgSrv = require('../../hotel_groups/services');
var hgDao = require('../../hotel_groups/dao');
var dao = require('../dao');

lab.experiment('versioning.additem', function () {
	var f1;
	var hotelId = Math.floor(Math.random() * 100 + 100);
	var hotelGroupId = Math.floor(Math.random() * 100 + 100);
	var data = {
		meta: {
			scheduledAt: '2015-12-03T13:09:40.610Z',
			status: 'draft'
		},
		hotelId: hotelId.toString(),
		hotelGroup: {
			hotelGroupId: hotelGroupId.toString()
		}
	};
	var hgData = {
		meta: {
			scheduledAt: '2015-12-03T13:09:40.610Z',
			status: 'draft'
		},
		hotelGroupId: hotelGroupId.toString()
	};
	var dataMeta = {
		meta: {
			createdAt: null,
			scheduledAt: '2015-12-03T13:09:40.610Z',
			status: 'draft'
		},
		hotelId: Math.floor(Math.random() * 100 + 100).toString(),
		hotelGroup: {
			hotelGroupId: Math.floor(Math.random() * 100 + 100).toString()
		}
	};
	var table = 'hotel_v';
	var doneDb, dBclient;
	lab.before(function (done) {
		databaseUtils.getConnection(function (err, client, doneDb1) {
			testingTools.code.expect(err).to.be.null();
			dBclient = client;
			doneDb = doneDb1;
			dBclient.query('BEGIN TRANSACTION', function (err) {
				testingTools.code.expect(err).to.be.null();
				done();
			});
		});
		f1 = databaseUtils.getConnection;

		databaseUtils.getConnection = function () {
			arguments[arguments.length - 1](
				null,
				dBclient,
				function () {
				}
			);
		};
	});

	lab.after(function (done) {
		databaseUtils.getConnection = f1;
		dBclient.query('ROLLBACK', function (err) {
			testingTools.code.expect(err).to.be.null();
			doneDb();
			done();
		});
	});

	lab.test('Add item should add entry with createdAt time', function (done) {
		var startDate = new Date().getTime();
		services.addItem(data, table, function (err) {
			testingTools.code.expect(err).to.be.null();
			var query = escape("SELECT * FROM %I WHERE (data->'hotelGroup'->'hotelGroupId') = '%I' AND (data->'hotelId')= '%I'",
				table, data.hotelGroup.hotelGroupId, data.hotelId);
			dBclient.query(query, function (err, result) {
				testingTools.code.expect(err).to.be.null();
				var toTest = new Date(result.rows[0].data.meta.createdAt).getTime();
				var currMoment = new Date().getTime();
				testingTools.code.expect(toTest).to.be.within(startDate, currMoment);
				done();
			});
		});
	});

	lab.test('Add item without guid for hotel_group', function (done) {
		var startDate = new Date().getTime();
		services.addItem(hgData, 'hotel_group_v', function (err) {
			testingTools.code.expect(err).to.be.null();
			var query = escape("SELECT * FROM %I WHERE (data->'hotelGroupId') = '%I'",
				'hotel_group_v', hgData.hotelGroupId);
			dBclient.query(query, function (err, result) {
				testingTools.code.expect(err).to.be.null();
				var toTest = new Date(result.rows[0].data.meta.createdAt).getTime();
				var currMoment = new Date().getTime();
				testingTools.code.expect(toTest).to.be.within(startDate, currMoment);
				done();
			});
		});
	});

	lab.test('Add item to unknown entity, should give error', function (done) {
		var data2 = {
			hotelId: hotelId.toString(),
			hotelGroup: {
				hotelGroupId: hotelGroupId.toString()
			}
		};
		services.addItem(data2, 'other_table', function (err) {
			testingTools.expectError(err, 'ver-unk-table');
			done();
		});
	});

	lab.test('Add item with meta shoul work', function (done) {
		var startDate = new Date().getTime();
		services.addItem(dataMeta, table, function (err) {
			testingTools.code.expect(err).to.be.null();
			var query = escape("SELECT * FROM %I WHERE (data->'hotelGroup'->'hotelGroupId') = '%I' AND (data->'hotelId')= '%I'",
				table, dataMeta.hotelGroup.hotelGroupId, dataMeta.hotelId);
			dBclient.query(query, function (err, result) {
				testingTools.code.expect(err).to.be.null();
				var toTest = new Date(result.rows[0].data.meta.createdAt).getTime();
				var currMoment = new Date().getTime();
				testingTools.code.expect(toTest).to.be.within(startDate, currMoment);
				done();
			});
		});
	});
	lab.test('Add item with invalid meta should give an error', function (done) {
		dataMeta.meta.status = 'some_unknown_status';
		services.addItem(dataMeta, table, function (err) {
			testingTools.expectError(err, 'ver-inv-status');
			done();
		});
	});
});

lab.experiment('versioning.withoutdb', function () {
	var tableName = 'dummy_test';
	testingTools.killDbConnectionForExperiment(lab);
	lab.test('Get data should get error', function (done) {
		services.getLiveData(tableName, function (err) {
			testingTools.code.expect(err).to.be.object();
			done();
		});
	});
});


lab.experiment('versioning.getLiveData', function () {
	testingTools.buildTestHotelsForVersioning(lab, 'hotel_v', 20, false);
	lab.test('Get live data should work', {timeout: 10000}, function (done) {
		services.getLiveData('hotel_v', function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.length).to.be.above(0);
			done();
		});
	});
});

lab.experiment('versioning.services.handleGolive.publishHotel', function () {
	var verTable = 'hotel_v';
	var liveTable = 'hotel';
	var hotelId = 'test-publish';
	var hotelIdOther = 'test-publish-otherHotel';
	var hotelGroupId = 'test-hgid';
	var now = new Date();
	var yesterday = now.setDate(now.getDate() - 1);


	var hotel = {
		'meta': {
			'createdAt': new Date(yesterday).toISOString(),
			'scheduledAt': new Date(yesterday).toISOString(),
			'appliedAt': null,
			'status': 'published',
			'guid': hotelId + '_' + hotelGroupId
		},
		'hotelId': hotelId,
		'hotelGroup': {
			'hotelGroupId': hotelGroupId
		},
		'coordinates': {
			'lat': 50.941857,
			'long': 6.956522
		},
		'categoryIds': ['reg-eur', 'reg-eur-ger', 'reg-eur-ger-col', 'ex-cty'],
		'localized': [
			{
				'language': 'en_GB',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Cologne',
					'country': 'Germany',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Cologne, Germany',
					'shortAddress': 'Alstadt-Nord, Cologne, Germany'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'About us'}],
						'text': [{
							'style': null,
							'value': 'The Excelsior Hotel Ernst is Cologne’s Grand Hotel next to the Cathedral. We provide individualized services to respond to the uniqueness of every guest and employee. We maintain the tradition of hospitality and remain modern by applying innovation. As Grand Hotel in the heart of Cologne, we actively participate in urban life and we are committed to art and culture. Your individuality is our Excellence.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'History'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, Royal Restorer of the central station, was builder and owner of the Hotel Ernst in the city center. The opening ceremony took place on May 16th 1863. In 1871, after as little as eight years, he sold the Hotel Ernst to Friedrich Kracht. Friedrich Kracht moved from Belgium to Cologne to manage the hotel but he died four years later. His wife and his son Carl took over the management of the house.  In those days, the Grand Hotel was already the first choice of prominent guests.'
						}]
					}
				]
			},
			{
				'language': 'it_IT',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Colonia',
					'country': 'Germania',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Colonia, Germania',
					'shortAddress': 'Alstadt-Nord, Colonia, Germania'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'Chi siamo'}],
						'text': [{
							'style': null,
							'value': 'LExcelsior Hotel Ernst è di Colonia Grand Hotel vicino alla cattedrale. Forniamo servizi personalizzati per rispondere alla unicità di ogni ospite e dipendenti. Manteniamo la tradizione di ospitalità e rimaniamo moderna, applicando linnovazione. Come Grand Hotel, nel cuore di Colonia, partecipiamo attivamente alla vita urbana e siamo impegnati a arte e cultura. La vostra individualità è la nostra eccellenza.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'Storia'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, Reale Restauratore della stazione centrale, è stato costruttore e proprietario dellHotel Ernst nel centro della città. La cerimonia di apertura si è svolta il 16 maggio 1863. Nel 1871, dopo appena otto anni, ha venduto lHotel Ernst Friedrich Kracht. Friedrich Kracht trasferito dal Belgio a Colonia per gestire lhotel, ma morì quattro anni dopo. Sua moglie e suo figlio Carl ha assunto la gestione della casa. In quei giorni, il Grand Hotel era già la prima scelta degli ospiti di spicco.'
						}]
					}
				]
			},
			{
				'language': 'fr_FR',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Cologne',
					'country': 'Allemagne',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Cologne, Allemagne',
					'shortAddress': 'Alstadt-Nord, Cologne, Allemagne'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'À propos de nous'}],
						'text': [{
							'style': null,
							'value': 'LHôtel Excelsior Ernst est Grand Hôtel de Cologne à côté de la cathédrale. Nous fournissons des services individualisés pour répondre à la spécificité de chaque client et lemployé. Nous maintenons la tradition de lhospitalité et restons moderne en appliquant linnovation. Comme Grand Hôtel au coeur de Cologne, nous participons activement à la vie urbaine et nous nous sommes engagés à lart et la culture. Votre individualité est notre excellence.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'Histoire'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, le Royal Restaurateur de la gare centrale, était constructeur et propriétaire de lHôtel Ernst dans le centre-ville. La cérémonie douverture a eu lieu le 16 mai 1863. En 1871, après aussi peu que huit ans, il a vendu lHôtel Ernst Friedrich Kracht. Friedrich Kracht déplacé de la Belgique vers Cologne pour gérer lhôtel mais il est mort quatre ans plus tard. Sa femme et son fils Carl prennent la direction de la maison. Dans ces jours, le Grand Hôtel était déjà le premier choix des invités de marque.'
						}]
					}
				]
			},
			{
				'language': 'de_DE',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Köln',
					'country': 'Deutschland',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Köln, Deutschland',
					'shortAddress': 'Alstadt-Nord, Köln, Deutschland'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'Wir über uns'}],
						'text': [{
							'style': null,
							'value': 'Das Excelsior Hotel Ernst ist Kölns Grand Hotel direkt neben der Kathedrale. Wir bieten individuelle Dienstleistungen für die Einzigartigkeit eines jeden Gastes und Mitarbeiter reagieren. Wir pflegen die Tradition der Gastfreundschaft und bleiben durch die Anwendung moderner Innovation. Als Grand Hotel im Herzen von Köln, wir aktiv im städtischen Leben teilzunehmen, und wir sind entschlossen, Kunst und Kultur. Ihre Individualität ist unsere Exzellenz.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'Geschichte'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, Königs Restorer der Hauptbahnhof, war Baumeister und Besitzer des Hotel Ernst in der Innenstadt. Die Eröffnungsfeier fand am 16. Mai 1863. Im Jahr 1871 nach nur acht Jahren verkaufte er das Hotel Ernst Friedrich Kracht. Friedrich Kracht bewegt von Belgien nach Köln, um das Hotel zu verwalten, aber er vier Jahre später starb. Seine Frau und sein Sohn Carl die Leitung des Hauses übernahm. In jenen Tagen war das Grand Hotel bereits die erste Wahl von prominenten Gästen.'
						}]
					}
				]
			}
		]
	};

	var newerHotel = {
		'meta': {
			'createdAt': new Date().toISOString(),
			'scheduledAt': new Date(yesterday).toISOString(),
			'appliedAt': null,
			'status': 'published',
			'guid': hotelId + '_' + hotelGroupId,
			'var': 'newerHotel'
		},
		'hotelId': hotelId,
		'hotelGroup': {
			'hotelGroupId': hotelGroupId
		},
		'coordinates': {
			'lat': 50.941857,
			'long': 6.956522
		},
		'categoryIds': ['reg-eur', 'reg-eur-ger', 'reg-eur-ger-col', 'ex-cty'],
		'localized': [
			{
				'language': 'en_GB',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Cologne',
					'country': 'Germany',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Cologne, Germany',
					'shortAddress': 'Alstadt-Nord, Cologne, Germany'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'About us'}],
						'text': [{
							'style': null,
							'value': 'The Excelsior Hotel Ernst is Cologne’s Grand Hotel next to the Cathedral. We provide individualized services to respond to the uniqueness of every guest and employee. We maintain the tradition of hospitality and remain modern by applying innovation. As Grand Hotel in the heart of Cologne, we actively participate in urban life and we are committed to art and culture. Your individuality is our Excellence.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'History'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, Royal Restorer of the central station, was builder and owner of the Hotel Ernst in the city center. The opening ceremony took place on May 16th 1863. In 1871, after as little as eight years, he sold the Hotel Ernst to Friedrich Kracht. Friedrich Kracht moved from Belgium to Cologne to manage the hotel but he died four years later. His wife and his son Carl took over the management of the house.  In those days, the Grand Hotel was already the first choice of prominent guests.'
						}]
					}
				]
			},
			{
				'language': 'it_IT',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Colonia',
					'country': 'Germania',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Colonia, Germania',
					'shortAddress': 'Alstadt-Nord, Colonia, Germania'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'Chi siamo'}],
						'text': [{
							'style': null,
							'value': 'LExcelsior Hotel Ernst è di Colonia Grand Hotel vicino alla cattedrale. Forniamo servizi personalizzati per rispondere alla unicità di ogni ospite e dipendenti. Manteniamo la tradizione di ospitalità e rimaniamo moderna, applicando linnovazione. Come Grand Hotel, nel cuore di Colonia, partecipiamo attivamente alla vita urbana e siamo impegnati a arte e cultura. La vostra individualità è la nostra eccellenza.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'Storia'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, Reale Restauratore della stazione centrale, è stato costruttore e proprietario dellHotel Ernst nel centro della città. La cerimonia di apertura si è svolta il 16 maggio 1863. Nel 1871, dopo appena otto anni, ha venduto lHotel Ernst Friedrich Kracht. Friedrich Kracht trasferito dal Belgio a Colonia per gestire lhotel, ma morì quattro anni dopo. Sua moglie e suo figlio Carl ha assunto la gestione della casa. In quei giorni, il Grand Hotel era già la prima scelta degli ospiti di spicco.'
						}]
					}
				]
			},
			{
				'language': 'fr_FR',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Cologne',
					'country': 'Allemagne',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Cologne, Allemagne',
					'shortAddress': 'Alstadt-Nord, Cologne, Allemagne'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'À propos de nous'}],
						'text': [{
							'style': null,
							'value': 'LHôtel Excelsior Ernst est Grand Hôtel de Cologne à côté de la cathédrale. Nous fournissons des services individualisés pour répondre à la spécificité de chaque client et lemployé. Nous maintenons la tradition de lhospitalité et restons moderne en appliquant linnovation. Comme Grand Hôtel au coeur de Cologne, nous participons activement à la vie urbaine et nous nous sommes engagés à lart et la culture. Votre individualité est notre excellence.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'Histoire'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, le Royal Restaurateur de la gare centrale, était constructeur et propriétaire de lHôtel Ernst dans le centre-ville. La cérémonie douverture a eu lieu le 16 mai 1863. En 1871, après aussi peu que huit ans, il a vendu lHôtel Ernst Friedrich Kracht. Friedrich Kracht déplacé de la Belgique vers Cologne pour gérer lhôtel mais il est mort quatre ans plus tard. Sa femme et son fils Carl prennent la direction de la maison. Dans ces jours, le Grand Hôtel était déjà le premier choix des invités de marque.'
						}]
					}
				]
			},
			{
				'language': 'de_DE',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Köln',
					'country': 'Deutschland',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Köln, Deutschland',
					'shortAddress': 'Alstadt-Nord, Köln, Deutschland'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'Wir über uns'}],
						'text': [{
							'style': null,
							'value': 'Das Excelsior Hotel Ernst ist Kölns Grand Hotel direkt neben der Kathedrale. Wir bieten individuelle Dienstleistungen für die Einzigartigkeit eines jeden Gastes und Mitarbeiter reagieren. Wir pflegen die Tradition der Gastfreundschaft und bleiben durch die Anwendung moderner Innovation. Als Grand Hotel im Herzen von Köln, wir aktiv im städtischen Leben teilzunehmen, und wir sind entschlossen, Kunst und Kultur. Ihre Individualität ist unsere Exzellenz.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'Geschichte'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, Königs Restorer der Hauptbahnhof, war Baumeister und Besitzer des Hotel Ernst in der Innenstadt. Die Eröffnungsfeier fand am 16. Mai 1863. Im Jahr 1871 nach nur acht Jahren verkaufte er das Hotel Ernst Friedrich Kracht. Friedrich Kracht bewegt von Belgien nach Köln, um das Hotel zu verwalten, aber er vier Jahre später starb. Seine Frau und sein Sohn Carl die Leitung des Hauses übernahm. In jenen Tagen war das Grand Hotel bereits die erste Wahl von prominenten Gästen.'
						}]
					}
				]
			}
		]
	};

	var verId, verIdOld, newerHotel2, otherHotel, otherHotel2;

	lab.before(function (done) {
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(hotel));
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);

				newerHotel2 = JSON.parse(JSON.stringify(newerHotel));
				newerHotel2.meta.var = 'newerHotel2';
				newerHotel.meta.createdAt = new Date().toISOString();
				newerHotel = JSON.parse(JSON.stringify(newerHotel));

				otherHotel = JSON.parse(JSON.stringify(newerHotel).replace(new RegExp(hotelId, 'gi'), hotelIdOther));
				otherHotel2 = JSON.parse(JSON.stringify(otherHotel));
				otherHotel.meta.var = 'otherHotel';
				otherHotel2.meta.var = 'otherHotel2';
				otherHotel.meta.createdAt = new Date().toISOString();

				delete newerHotel2.localized[0].description[1];

				query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(newerHotel2));
				client.query(query, function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.equal(1);
					verIdOld = res.rows[0].id;
					query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(newerHotel));
					client.query(query, function (err, res) {
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(res.rowCount).to.be.equal(1);
						verId = res.rows[0].id;
						query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(otherHotel));
						client.query(query, function (err, res) {
							testingTools.code.expect(err).to.be.null();
							testingTools.code.expect(res.rowCount).to.be.equal(1);
							query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(otherHotel2));
							client.query(query, function (err, res) {
								testingTools.code.expect(err).to.be.null();
								testingTools.code.expect(res.rowCount).to.be.equal(1);
								doneDb();
								done();
							});
						});
					});
				});
			});
		});
	});

	lab.after(function (done) {
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			//Clean up version table
			var query = escape("DELETE FROM %I WHERE (data->'hotelGroup'->'hotelGroupId') = '%I' AND ((data->'hotelId')= '%I' OR (data->'hotelId')= '%I')",
				verTable, hotelGroupId, hotelId, hotelIdOther);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(5);
				//Clean up live table
				query = escape("DELETE FROM %I WHERE (data->'hotelGroup'->'hotelGroupId') = '%I' AND ((data->'hotelId')= '%I' OR (data->'hotelId')= '%I')",
					liveTable, hotelGroupId, hotelId, hotelIdOther);
				client.query(query, function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.equal(1);
					doneDb();
					done();
				});
			});
		});
	});


	lab.test('Handle publish should go well', {timeout: 20000}, function (done) {
		services.handleGoLive(verTable, liveTable, function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res[0]).to.be.equal(1);
			databaseUtils.getConnection(function (err, client, doneDb) {
				testingTools.code.expect(err).to.be.null();
				//Check is everyting ok on live table
				var query = escape("SELECT * FROM %I WHERE (data->'hotelGroup'->'hotelGroupId') = '%I' AND (data->'hotelId')= '%I'",
					liveTable, hotelGroupId, hotelId);
				client.query(query, function (err, res) {
					var later = new Date().getTime();
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount, query).to.equal(1);
					testingTools.code.expect(res.rows[0].data).to.be.object();
					testingTools.code.expect(res.rows[0].data.meta.var).to.equal('newerHotel');
					testingTools.code.expect(new Date(res.rows[0].data.meta.appliedAt).getTime()).to.be.within(now.getTime(), later);
					//Check is everything ok in version table (applied date is updated)
					query = escape('SELECT * FROM %I WHERE id=%L', verTable, verId);
					client.query(query, function (err, res) {
						later = new Date().getTime();
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(new Date(res.rows[0].data.meta.appliedAt).getTime()).to.be.within(now.getTime(), later);
						//check old version
						query = escape('SELECT * FROM %I WHERE id=%L', verTable, verIdOld);
						client.query(query, function (err, res) {
							later = new Date().getTime();
							testingTools.code.expect(err).to.be.null();
							testingTools.code.expect(res.rows[0].data.meta.appliedAt).to.equal(false);
							doneDb();
							done();
						});

					});
				});
			});
		}, [newerHotel.meta.guid]);
	});
});

lab.experiment('versioning.services.handleGolive.unpublishHotel', function () {
	var tableName = 'hotel_v';
	var liveTable = 'hotel';
	var hotelId = 'test-unpublish';
	var hotelGroupId = 'test-hgid';
	var now = new Date();
	var yesterday = now.setDate(now.getDate() - 1);

	var hotel = {
		'meta': {
			'createdAt': new Date(yesterday).toISOString(),
			'scheduledAt': new Date(yesterday).toISOString(),
			'appliedAt': null,
			'status': 'draft',
			'guid': hotelId + '_' + hotelGroupId
		},
		'hotelId': hotelId,
		'hotelGroup': {
			'hotelGroupId': hotelGroupId
		},
		'coordinates': {
			'lat': 50.941857,
			'long': 6.956522
		}, localized: []
	};
	var verId;

	lab.before(function (done) {
		//Create some test data in version and live table
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			//Insert data in version table
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', tableName, JSON.stringify(hotel));
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				verId = res.rows[0].id;
				hotel.meta.appliedAt = new Date().toISOString();
				//Insert data in live table
				query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', liveTable, JSON.stringify(hotel));
				client.query(query, function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.equal(1);
					doneDb();
					done();
				});
			});
		});
	});
	lab.after(function (done) {
		//Clean up test data
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM %I WHERE id=%L', tableName, verId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				query = escape("DELETE FROM %I WHERE (data->'hotelGroup'->'hotelGroupId') = '%I' AND (data->'hotelId')= '%I'",
					liveTable, hotelGroupId, hotelId);
				client.query(query, function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.equal(0);
					doneDb();
					done();
				});
			});
		});
	});

	lab.test('Unpublish should remove from live table', {timeout: 10000}, function (done) {
		services.handleGoLive(tableName, liveTable, function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res[1]).to.be.equal(1);
			databaseUtils.getConnection(function (err, client, doneDb) {
				testingTools.code.expect(err).to.be.null();
				//Check on live table, should be removed
				var query = escape("SELECT * FROM %I WHERE (data->'hotelGroup'->'hotelGroupId') = '%I' AND (data->'hotelId')= '%I'",
					liveTable, hotelGroupId, hotelId);
				client.query(query, function (err, res) {
					var later = new Date().getTime();
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.equal(0);
					query = escape('SELECT * FROM %I WHERE id=%L', tableName, verId);
					client.query(query, function (err, res) {
						later = new Date().getTime();
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(new Date(res.rows[0].data.meta.appliedAt).getTime()).to.be.within(now.getTime(), later);
						doneDb();
						done();
					});
				});
			});
		}, [hotel.meta.guid]);
	});
});

lab.experiment('versioning.services.handleGolive.publishHotelGroup', function () {
	var liveTable = 'hotel_group';
	var verTable = 'hotel_group_v';
	var hotelGroupId = 'test-publish';
	var today = new Date();
	var now = new Date();
	var yesterday = today.setDate(today.getDate() - 1);
	var dataOlder = {
		meta: {
			'createdAt': new Date(yesterday).toISOString(),
			'scheduledAt': new Date(yesterday).toISOString(),
			'appliedAt': null,
			'status': 'published',
			'guid': hotelGroupId
		},
		'hotelGroupId': hotelGroupId
	};
	var dataNewer = {
		meta: {
			'createdAt': now,
			'scheduledAt': new Date(yesterday).toISOString(),
			'appliedAt': null,
			'status': 'published',
			'guid': hotelGroupId
		},
		'hotelGroupId': hotelGroupId
	};
	var newerId;

	lab.before(function (done) {
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(dataOlder));
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(dataNewer));
				client.query(query, function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.equal(1);
					newerId = res.rows[0].id;
					doneDb();
					done();
				});
			});
		});
	});
	lab.after(function (done) {
		//Clean up test data in versioning (live table should be cleared by tests)
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape("DELETE FROM %I WHERE (data->'hotelGroupId') = '%I'", verTable, hotelGroupId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(3);
				doneDb();
				done();
			});
		});
	});
	lab.test('Publish hotelGroup should go well', {timeout: 10000}, function (done) {

		services.handleGoLive('hotel_group_v', 'hotel_group', function (err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res[0]).to.be.equal(1);
			databaseUtils.getConnection(function (err, client, doneDb) {
				testingTools.code.expect(err).to.be.null();
				var query = escape('SELECT * FROM %I WHERE id=%L', verTable, newerId);
				client.query(query, function (err, res) {
					var later = new Date().getTime();
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.above(0);
					testingTools.code.expect(new Date(res.rows[0].data.meta.appliedAt).getTime()).to.be.within(now.getTime(), later);
					query = escape("SELECT * FROM %I WHERE (data->'hotelGroupId') = '%I'", liveTable, hotelGroupId);
					client.query(query, function (err, res) {
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(new Date(res.rows[0].data.meta.appliedAt).getTime()).to.be.within(now.getTime(), later);
						doneDb();
						done();
					});
				});
			});
		}, [dataOlder.meta.guid]);
	});
	lab.test('Unpublish hotelGroup should go well', {timeout: 20000}, function (done) {
		dataNewer.meta.status = 'draft';
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			//Add item to versioning to unpublish
			var query = escape('INSERT INTO %I (data) VALUES (%L) RETURNING id', verTable, JSON.stringify(dataNewer));
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				//Do the goLive thing
				services.handleGoLive('hotel_group_v', 'hotel_group', function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res[1]).to.be.equal(1);
					//Check is it gone
					query = escape("SELECT * FROM %I WHERE (data->'hotelGroupId') = '%I'", liveTable, hotelGroupId);
					client.query(query, function (err, res) {
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(res.rowCount).to.be.equal(0);
						doneDb();
						done();
					});
				});
			});
		});
	});
});

lab.experiment('versiongin.services.handleGolive.publish.faliures', function () {
	var now = new Date();
	var yesterday = now.setDate(now.getDate() - 1);
	var hotelId = 'test-failures';
	var hotelGroupId = 'test-hgid';
	var verTable = 'hotel_v';
	var hotel = {
		'meta': {
			'createdAt': new Date(yesterday).toISOString(),
			'scheduledAt': new Date(yesterday).toISOString(),
			'appliedAt': null,
			'status': 'published',
			'guid': hotelId + '_' + hotelGroupId
		},
		'hotelId': hotelId,
		'hotelGroup': {
			'hotelGroupId': hotelGroupId
		},
		'coordinates': {
			'lat': 50.941857,
			'long': 6.956522
		},
		'categoryIds': ['reg-eur', 'reg-eur-ger', 'reg-eur-ger-col', 'ex-cty'],
		'localized': [
			{
				'language': 'en_GB',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Cologne',
					'country': 'Germany',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Cologne, Germany',
					'shortAddress': 'Alstadt-Nord, Cologne, Germany'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'About us'}],
						'text': [{
							'style': null,
							'value': 'The Excelsior Hotel Ernst is Cologne’s Grand Hotel next to the Cathedral. We provide individualized services to respond to the uniqueness of every guest and employee. We maintain the tradition of hospitality and remain modern by applying innovation. As Grand Hotel in the heart of Cologne, we actively participate in urban life and we are committed to art and culture. Your individuality is our Excellence.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'History'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, Royal Restorer of the central station, was builder and owner of the Hotel Ernst in the city center. The opening ceremony took place on May 16th 1863. In 1871, after as little as eight years, he sold the Hotel Ernst to Friedrich Kracht. Friedrich Kracht moved from Belgium to Cologne to manage the hotel but he died four years later. His wife and his son Carl took over the management of the house.  In those days, the Grand Hotel was already the first choice of prominent guests.'
						}]
					}
				]
			},
			{
				'language': 'it_IT',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Colonia',
					'country': 'Germania',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Colonia, Germania',
					'shortAddress': 'Alstadt-Nord, Colonia, Germania'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'Chi siamo'}],
						'text': [{
							'style': null,
							'value': 'LExcelsior Hotel Ernst è di Colonia Grand Hotel vicino alla cattedrale. Forniamo servizi personalizzati per rispondere alla unicità di ogni ospite e dipendenti. Manteniamo la tradizione di ospitalità e rimaniamo moderna, applicando linnovazione. Come Grand Hotel, nel cuore di Colonia, partecipiamo attivamente alla vita urbana e siamo impegnati a arte e cultura. La vostra individualità è la nostra eccellenza.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'Storia'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, Reale Restauratore della stazione centrale, è stato costruttore e proprietario dellHotel Ernst nel centro della città. La cerimonia di apertura si è svolta il 16 maggio 1863. Nel 1871, dopo appena otto anni, ha venduto lHotel Ernst Friedrich Kracht. Friedrich Kracht trasferito dal Belgio a Colonia per gestire lhotel, ma morì quattro anni dopo. Sua moglie e suo figlio Carl ha assunto la gestione della casa. In quei giorni, il Grand Hotel era già la prima scelta degli ospiti di spicco.'
						}]
					}
				]
			},
			{
				'language': 'fr_FR',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Cologne',
					'country': 'Allemagne',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Cologne, Allemagne',
					'shortAddress': 'Alstadt-Nord, Cologne, Allemagne'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'À propos de nous'}],
						'text': [{
							'style': null,
							'value': 'LHôtel Excelsior Ernst est Grand Hôtel de Cologne à côté de la cathédrale. Nous fournissons des services individualisés pour répondre à la spécificité de chaque client et lemployé. Nous maintenons la tradition de lhospitalité et restons moderne en appliquant linnovation. Comme Grand Hôtel au coeur de Cologne, nous participons activement à la vie urbaine et nous nous sommes engagés à lart et la culture. Votre individualité est notre excellence.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'Histoire'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, le Royal Restaurateur de la gare centrale, était constructeur et propriétaire de lHôtel Ernst dans le centre-ville. La cérémonie douverture a eu lieu le 16 mai 1863. En 1871, après aussi peu que huit ans, il a vendu lHôtel Ernst Friedrich Kracht. Friedrich Kracht déplacé de la Belgique vers Cologne pour gérer lhôtel mais il est mort quatre ans plus tard. Sa femme et son fils Carl prennent la direction de la maison. Dans ces jours, le Grand Hôtel était déjà le premier choix des invités de marque.'
						}]
					}
				]
			},
			{
				'language': 'de_DE',
				'name': 'Excelsior Hotel Ernst',
				'location': {
					'city': 'Köln',
					'country': 'Deutschland',
					'fullAddress': 'Domplatz/Trankgasse 1-5, 50667 Köln, Deutschland',
					'shortAddress': 'Alstadt-Nord, Köln, Deutschland'
				},
				'contacts': {
					'primary': {
						'phone': '+49 (0)221 270 1',
						'email': ' info@excelsior-hotel-ernst.de',
						'website': 'http://www.excelsiorhotelernst.com/en'
					}

				},

				'description': [
					{
						'heading': [{'style': null, 'value': 'Wir über uns'}],
						'text': [{
							'style': null,
							'value': 'Das Excelsior Hotel Ernst ist Kölns Grand Hotel direkt neben der Kathedrale. Wir bieten individuelle Dienstleistungen für die Einzigartigkeit eines jeden Gastes und Mitarbeiter reagieren. Wir pflegen die Tradition der Gastfreundschaft und bleiben durch die Anwendung moderner Innovation. Als Grand Hotel im Herzen von Köln, wir aktiv im städtischen Leben teilzunehmen, und wir sind entschlossen, Kunst und Kultur. Ihre Individualität ist unsere Exzellenz.'
						}]
					},
					{
						'heading': [{'style': null, 'value': 'Geschichte'}],
						'text': [{
							'style': null,
							'value': 'Carl Ernst, Königs Restorer der Hauptbahnhof, war Baumeister und Besitzer des Hotel Ernst in der Innenstadt. Die Eröffnungsfeier fand am 16. Mai 1863. Im Jahr 1871 nach nur acht Jahren verkaufte er das Hotel Ernst Friedrich Kracht. Friedrich Kracht bewegt von Belgien nach Köln, um das Hotel zu verwalten, aber er vier Jahre später starb. Seine Frau und sein Sohn Carl die Leitung des Hauses übernahm. In jenen Tagen war das Grand Hotel bereits die erste Wahl von prominenten Gästen.'
						}]
					}
				]
			}
		]
	};

	var publishHotel, daoUpdate, getLiveData, daoPublishHotel;
	var hotel2;
	lab.beforeEach(function (done) {
		getLiveData = services.getLiveData;
		publishHotel = htlSrv.publishHotel;
		daoUpdate = dao.update;
		daoPublishHotel = htlDao.publishHotel;
		hotel2 = JSON.parse(JSON.stringify(hotel));
		hotel2.meta.var = 'hotel2';
		delete hotel2.localized[0].description[0];
		hotel.meta.createdAt = new Date(yesterday).toISOString();
		hotel.meta.var = 'hotel';
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			//insert old version
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(hotel2));
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);

				query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(hotel));
				client.query(query, function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.equal(1);
					doneDb();
					done();
				});
			});
		});
	});
	lab.afterEach(function (done) {
		services.getLiveData = getLiveData;
		htlSrv.publishHotel = publishHotel;
		htlDao.publishHotel = daoPublishHotel;
		dao.update = daoUpdate;
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM %I WHERE (data->>\'hotelId\') = %L', verTable, hotelId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(2);
				query = escape('DELETE FROM hotel WHERE (data->>\'hotelId\') = %L', hotelId);
				client.query(query, function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rowCount).to.be.equal(1);
					doneDb();
					done();
				});
			});
		});
	});


	lab.test('Get live data should give error', function (done) {
		services.getLiveData = function (verTable, callback) {
			callback(new Error('unable to get live data'));
		};
		services.handleGoLive('hotel_v', 'hotel', function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('unable to get live data');
			done();
		});
	});

	lab.test('denyOldVersion data should give error', function (done) {
		dao.update = function(i, t, cb){
			if(i.data.meta.var === 'hotel2'){
				return cb(new Error('update error for old version'));
			}
			cb(null);
		};
		services.handleGoLive('hotel_v', 'hotel', function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('update error for old version');
			done();
		});
	});

	lab.test('Publish hotel should give error', function (done) {
		htlSrv.publishHotel = function (item, callback) {
			callback(new Error('publish failure'));
		};
		services.handleGoLive('hotel_v', 'hotel', function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('publish failure');
			done();
		});
	});
	lab.test('Dao update should give failure', function (done) {
		htlDao.publishHotel = function (item, callback) {
			callback(null);
		};
		dao.update = function (item, table, cb) {
			cb(new Error('updatefailure'));
		};
		services.handleGoLive('hotel_v', 'hotel', function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('updatefailure');
			done();
		});
	});
});

lab.experiment('versiongin.services.handleGoliveHotel.unPublish.faliures', function () {
	var verTable = 'hotel_v';
	var hotelId = 'test-unpublish-fail';
	var hotelGroupId = 'test-hgid';
	var now = new Date();
	var yesterday = now.setDate(now.getDate() - 1);
	var hotel = {
		'meta': {
			'createdAt': new Date(yesterday).toISOString(),
			'scheduledAt': new Date(yesterday).toISOString(),
			'appliedAt': null,
			'status': 'draft',
			'guid': hotelId + '_' + hotelGroupId
		},
		'hotelId': hotelId,
		'hotelGroup': {
			'hotelGroupId': hotelGroupId
		},
		'coordinates': {
			'lat': 50.941857,
			'long': 6.956522
		}
	};
	var hotel2 = {
		'meta': {
			'createdAt': new Date(yesterday).toISOString(),
			'scheduledAt': new Date(yesterday).toISOString(),
			'appliedAt': null,
			'status': 'draft',
			'guid': hotelId + '2' + '_' + hotelGroupId
		},
		'hotelId': hotelId + '2',
		'hotelGroup': {
			'hotelGroupId': hotelGroupId
		},
		'coordinates': {
			'lat': 50.941857,
			'long': 6.956522
		}
	};
	var verId, verId2, hotelUnPub, daoUpdate;
	lab.beforeEach(function (done) {
		hotelUnPub = htlSrv.unPublishHotel;
		daoUpdate = dao.update;
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();

			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(hotel2));
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rows[0].id).to.be.above(0);
				verId2 = res.rows[0].id;

				query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(hotel));
				client.query(query, function (err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.rows[0].id).to.be.above(0);
					verId = res.rows[0].id;
					doneDb();
					done();
				});
			});
		});
	});
	lab.afterEach(function (done) {
		htlSrv.unPublishHotel = hotelUnPub;
		dao.update = daoUpdate;
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM %I WHERE id= %L OR id= %L', verTable, verId, verId2);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(2);
				doneDb();
				done();
			});
		});
	});
	lab.test('Hotel unpublish should give failure', function (done) {
		htlSrv.unPublishHotel = function (item, cb) {
			cb(new Error('Unpublishfailure'));
		};
		services.handleGoLive('hotel_v', 'hotel', function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		}, [hotel.meta.guid]);
	});
	lab.test('Dao update should give failure', function (done) {
		dao.update = function (item, table, cb) {
			cb(new Error('updatefailure'));
		};
		services.handleGoLive('hotel_v', 'hotel', function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		}, [hotel.meta.guid]);
	});
});



lab.experiment('versiongin.services.handleGoliveHotelGroup.unPublish.faliures', function () {
	var verTable = 'hotel_group_v';
	var hotelGroupId = 'test-hgid-fail1';
	var now = new Date();
	var yesterday = now.setDate(now.getDate() - 1);
	var verId, hgUnPub, daoUpdate;
	var hotelGroup = {
		meta: {
			'createdAt': now,
			'scheduledAt': new Date(yesterday).toISOString(),
			'appliedAt': null,
			'status': 'draft',
			'guid': hotelGroupId
		},
		'hotelGroupId': hotelGroupId
	};
	lab.beforeEach(function (done) {
		hgUnPub = hgSrv.unPublishItem;
		daoUpdate = dao.update;
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(hotelGroup));
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rows[0].id).to.be.above(0);
				verId = res.rows[0].id;
				doneDb();
				done();
			});
		});
	});

	lab.afterEach(function (done) {
		hgSrv.unPublishItem = hgUnPub;
		dao.update = daoUpdate;
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM %I WHERE id= %L', verTable, verId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				doneDb();
				done();
			});
		});
	});
	lab.test('HotelGroup unpublish should give failure', function (done) {
		hgSrv.unPublishItem = function (item, cb) {
			cb(new Error('Unpublishfailure'));
		};
		services.handleGoLive('hotel_group_v', 'hotel_group', function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});
	lab.test('Dao update should give failure', function (done) {
		dao.update = function (item, table, cb) {
			cb(new Error('updatefailure'));
		};
		services.handleGoLive('hotel_group_v', 'hotel_group', function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});
	lab.test('No Entity failure is the one what we want to get', {timeout: 10000}, function(done){
		services.handleGoLive('hotel_group_v', 'some_other_entity', function(err){
			testingTools.expectError(err, 'ver-no-entity');
			done();
		});
	});


});

lab.experiment('versiongin.services.handleGoliveHotelGroup.publish.faliures', function () {
	var verTable = 'hotel_group_v';
	var hotelGroupId = 'test-hgid-fail2';
	var now = new Date();
	var yesterday = now.setDate(now.getDate() - 1);
	var verId, hgUnPub, daoUpdate, daoPub;
	var hotelGroup = {
		meta: {
			'createdAt': now,
			'scheduledAt': new Date(yesterday).toISOString(),
			'appliedAt': null,
			'status': 'published',
			'guid': hotelGroupId
		},
		'hotelGroupId': hotelGroupId
	};
	lab.beforeEach(function (done) {
		hgUnPub = hgSrv.publishHotelGroup;
		daoPub = hgDao.publishHotelGroup;
		daoUpdate = dao.update;
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(hotelGroup));
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rows[0].id).to.be.above(0);
				verId = res.rows[0].id;
				doneDb();
				done();
			});
		});
	});

	lab.afterEach(function (done) {
		hgSrv.publishHotelGroup = hgUnPub;
		hgDao.publishHotelGroup = daoPub;
		dao.update = daoUpdate;
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM %I WHERE id= %L', verTable, verId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				doneDb();
				done();
			});
		});
	});

	lab.test('HotelGroup publish should give failure', function (done) {
		hgSrv.publishHotelGroup = function (item, cb) {
			cb(new Error('Publishfailure'));
		};
		services.handleGoLive('hotel_group_v', 'hotel_group', function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			done();
		});
	});
	lab.test('Dao update should give failure', function (done) {
		hgDao.publishHotelGroup = function (item, cb) {
			cb(null);
		};
		dao.update = function (item, table, cb) {
			cb(new Error('updatefailure'));
		};
		services.handleGoLive('hotel_group_v', 'hotel_group', function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.be.equal('updatefailure');
			done();
		});
	});
	lab.test('No Entity failure is the one what we want to get', {timeout: 10000}, function(done){
		services.handleGoLive('hotel_group_v', 'some_other_entity', function(err){
			testingTools.expectError(err, 'ver-no-entity');
			done();
		});
	});
});

lab.experiment('versioning.unknown.status.failure', function(){
	var verTable = 'hotel_group_v';
	var hotelGroupId = 'test-hgid-fail3';
	var now = new Date();
	var yesterday = now.setDate(now.getDate() - 1);
	var hotelGroup = {
		meta: {
			'createdAt': now,
			'scheduledAt': new Date(yesterday).toISOString(),
			'appliedAt': null,
			'status': 'some-wierd-status',
			'guid': hotelGroupId
		},
		'hotelGroupId': hotelGroupId
	};
	var verId;
	lab.beforeEach(function (done) {
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', verTable, JSON.stringify(hotelGroup));
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rows[0].id).to.be.above(0);
				verId = res.rows[0].id;
				doneDb();
				done();
			});
		});
	});

	lab.afterEach(function (done) {
		databaseUtils.getConnection(function (err, client, doneDb) {
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM %I WHERE id= %L', verTable, verId);
			client.query(query, function (err, res) {
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				doneDb();
				done();
			});
		});
	});

	lab.test('Time for unknown status error', function(done){
		services.handleGoLive('hotel_group_v', 'hotel_group', function(err){
			testingTools.expectError(err, 'ver-no-status');
			done();
		});
	});
});

lab.experiment('versioning.diff', function(){
	var hotelId = 'test-diff-thing';
	var hotelGroupId = 'test-hgid';
	var tableName = 'hotel_v';
	var liveTable = tableName.substring(0, tableName.length-2);
	var guid = hotelId + '_' + hotelGroupId;
	var data;
	var itemId;
	lab.before(function(done){
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query1 = escape("SELECT data FROM %I WHERE (data->'hotelGroup'->'hotelGroupId') = %L", liveTable, '"'+hotelGroupId+'"');
			client.query(query1, function(err, res){
				testingTools.code.expect(err).to.be.null();
				data = res.rows[0].data;
				data.hotelId= hotelId;
				data.hotelGroup.hotelGroupId = hotelGroupId;
				data.meta.guid = guid;

				var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', tableName, JSON.stringify(data));
				client.query(query, function(err, res){
					testingTools.code.expect(err).to.be.null();
					itemId = res.rows[0].id;
					doneDb();
					done();
				});
			});
		});
	});
	lab.after(function(done){
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape('DELETE FROM %I WHERE id = %L', tableName, itemId);
			client.query(query, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(1);
				doneDb();
				done();
			});
		});
	});
	lab.test('Identical data should get error', function(done){
		services.checkDiff(data, guid, tableName, function(err){
			testingTools.expectError(err, 'ver-no-change');
			done();
		});
	});
	lab.test('Changed data should not get an error', function(done){
		data.meta.status='draft';
		services.checkDiff(data, guid, tableName, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('Get error from latest version', function(done){
		services.checkDiff(data, 'someotherguid', tableName, function(err){
			testingTools.expectError(err, 'ver-no-prev');
			done();
		});
	});

	lab.test('GetDiff Identical data should get error', function(done){
		data.meta.status='published';
		services.getDiff(data, guid, tableName, 0, function(err){
			testingTools.expectError(err, 'ver-no-change');
			done();
		});
	});
	lab.test('GetDiff Changed data should not get an error', function(done){
		data.meta.status='draft';
		services.getDiff(data, guid, tableName, 0, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
	lab.test('GetDiff Get error from latest version', function(done){
		services.getDiff(data, 'someotherguid', tableName, 0, function(err){
			testingTools.expectError(err, 'ver-no-prev');
			done();
		});
	});
});

lab.experiment('versioning.updateItem.hotel', function(){
	var now = new Date();
	var table = 'hotel_v';
	var hotelId = 'test-hotel-update';
	var hotelGroupId = 'test-hgid';
	var guid = hotelId + '_' + hotelGroupId;
	var hotel ={
		'meta': {
			'scheduledAt': now.toISOString(),
			'status': 'draft',
			'guid': guid
		},
		'hotelId': hotelId,
		'hotelGroup': {
			'hotelGroupId': hotelGroupId
		}
	};

	lab.before(function(done){
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', table, JSON.stringify(hotel));
			client.query(query, function(err){
				testingTools.code.expect(err).to.be.null();
				doneDb();
				done();
			});
		});
	});
	lab.after(function(done){
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape("DELETE FROM %I WHERE (data->'meta'->'guid') = %L", table, '"' + guid + '"');
			client.query(query, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(2);
				doneDb();
				done();
			});
		});
	});
	
	lab.test('Update hotel should go well', function(done){
		hotel.meta.status = 'published';
		services.updateItem(hotel, table, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
	  });
	});
	lab.test('Update with unknown table shoud give an error', function(done){
		hotel.meta.status = 'draft';
		services.updateItem(hotel, 'one-unknown-table', function(err){
			testingTools.expectError(err, 'ver-unk-table');
			done();
		});
	});
	lab.test('Update with identical data shoud give an error', function(done){
		hotel.meta.status = 'published';
		services.updateItem(hotel, table, function(err){
			testingTools.expectError(err, 'ver-no-change');
			done();
		});
	});
	lab.test('Update with invalid data shoud give an error', function(done){
		hotel.meta.status = 'draft';
		hotel.meta.scheduledAt = '1234';
		services.updateItem(hotel, table, function(err){
			testingTools.expectError(err, 'val-inv-iso-date');
			done();
		});
	});
});


lab.experiment('versioning.updateItem.hotelGroup', function(){
	var now = new Date();
	var table = 'hotel_group_v';
	var hotelGroupId = 'test-updtate-hgid';

	var hotelGroup = {
		'meta': {
			'scheduledAt': now.toISOString(),
			'status': 'draft',
			'guid': hotelGroupId
		},
		'hotelGroupId': hotelGroupId
	};

	lab.before(function(done){
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', table, JSON.stringify(hotelGroup));
			client.query(query, function(err){
				testingTools.code.expect(err).to.be.null();
				doneDb();
				done();
			});
		});
	});
	lab.after(function(done){
		databaseUtils.getConnection(function(err, client, doneDb){
			testingTools.code.expect(err).to.be.null();
			var query = escape("DELETE FROM %I WHERE (data->'meta'->'guid') = %L", table, '"' + hotelGroupId + '"');
			client.query(query, function(err, res){
				testingTools.code.expect(err).to.be.null();
				testingTools.code.expect(res.rowCount).to.be.equal(2);
				doneDb();
				done();
			});
		});
	});
	lab.test('Update hotelGroup should go well', function(done){
		hotelGroup.meta.status = 'published';
		services.updateItem(hotelGroup, table, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});
});
