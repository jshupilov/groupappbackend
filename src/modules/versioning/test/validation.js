'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var validation = require('../validation');

lab.experiment('Versioning.validation.general', function(){
	var data;
	lab.beforeEach(function(done){
		data = {
			'createdAt': '2015-12-03T13:09:40.610Z',
			'scheduledAt': '2015-12-03T13:09:40.610Z',
			'appliedAt': null,
			'status': 'published',
			'guid': 'some_guid'
		};
		done();
	});

	lab.test('Should not accept invalid data type', function(done){
		validation.validateMetaData(123, 'hotel_v', 'add', function(err){
			testingTools.expectError(err, 'ver-inv-type');
			done();
		});
	});
	lab.test('Should not accept invalid validation type', function(done){
		validation.validateMetaData({}, 'hotel_v', 'somewrongtype', function(err){
			testingTools.expectError(err, 'ver-inv-val-type');
			done();
		});
	});

	lab.test('Should pass all validations', function(done){
		validation.validateMetaData(data, 'hotel_v', 'add', function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('Wrong status should give an error', function(done){
		data.status = 'unknown_status';
		validation.validateMetaData(data, 'hotel_v', 'add', function(err){
			testingTools.expectError(err, 'ver-inv-status');
			done();
		});
	});
	lab.test('Wrong guid should give an error', function(done){
		data.guid = 1234;
		validation.validateMetaData(data, 'hotel_v', 'add', function(err){
			testingTools.expectError(err, 'ver-inv-guid');
			done();
		});
	});

	lab.test('Add with existing guid', function(done){
		data.guid = 'gpr_mchotels';
		validation.validateMetaData(data, 'hotel_v', 'add', function(err){
			testingTools.expectError(err, 'ver-inv-guid');
			done();
		});
	});

	lab.test('Update with non existing guid', function(done){
		data.guid = 'no_such_guid_available';
		validation.validateMetaData(data, 'hotel_v', 'update', function(err){
			testingTools.expectError(err, 'ver-inv-guid');
			done();
		});
	});


	lab.test('Add with non-existing guid', function(done){
		data.guid = 'no_such_guid_available';
		validation.validateMetaData(data, 'hotel_v', 'add', function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('Update with existing guid', function(done){
		data.guid = 'gpr_mchotels';
		validation.validateMetaData(data, 'hotel_v', 'update', function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});





	lab.test('Add with wrong guid', function(done){
		data.guid = 1234;
		validation.validateMetaData(data, 'hotel_v', 'update', function(err){
			testingTools.expectError(err, 'ver-inv-guid');
			done();
		});
	});
	lab.test('Update with wrong guid', function(done){
		data.guid = 'some_unknown_guid';
		validation.validateMetaData(data, 'hotel_v', 'update', function(err){
			testingTools.expectError(err, 'ver-inv-guid');
			done();
		});
	});

	lab.test('Missing scheduledAt should fail', function(done){
		delete data.scheduledAt;
		validation.validateMetaData(data, 'hotel_v', 'add', function(err){
			testingTools.expectError(err, 'ver-mis-sched');
			done();
		});
	});
	lab.test('When scheduledAt equals null should pass', function(done){
		data.scheduledAt=null;
		validation.validateMetaData(data, 'hotel_v', 'add', function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

});

lab.experiment('Versioning.validation.voDb', function(){
	testingTools.killDbConnectionForExperiment(lab);
	var data = {
		'createdAt': '2015-12-03T13:09:40.610Z',
		'scheduledAt': '2015-12-03T13:09:40.610Z',
		'appliedAt': null,
		'status': 'published',
		'guid': 'some_guid'
	};
	lab.test('Add should shold give error', function(done){
		validation.validateMetaData(data, 'hotel_v', 'add', function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
	lab.test('Update should shold give error', function(done){
		validation.validateMetaData(data, 'hotel_v', 'update', function(err){
			testingTools.expectError(err, 'db-cc-db');
			done();
		});
	});
});
