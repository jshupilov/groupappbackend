'use strict';
var errors = require('../utils/errors');
var validationUtils = require('../utils/validationUtils');
var check = require('check-types');
var dao = require('./dao');

errors.defineError('ver-inv-val-type', 'System error', 'Invalid validation type provided', 'Versioning.Validation', 500);
errors.defineError('ver-inv-type', 'System error', 'A value has a wrong data type', 'Versioning.Validation', 400);
errors.defineError('ver-inv-status', 'Input error', 'A value has a invalid status', 'Versioning.Validation', 400);
errors.defineError('ver-inv-applied', 'Input error', 'A value can not be applied', 'Versioning.Validation', 400);
errors.defineError('ver-inv-guid', 'Input error', 'A value has a invalid guid', 'Versioning.Validation', 400);
errors.defineError('ver-mis-sched', 'Missing value', 'Scheduled at key missing', 'Versioning.Validation', 400);


/**
 * ScheduledAt date must be ISO string or null, but key must exist.
 * @param meta
 * @param entity
 * @param validationType
 * @param callback
 * @returns {*}
 */
function validateScheduledAt(meta, entity, validationType, callback){
	var date = meta.scheduledAt;
	if(typeof date !== 'undefined'){
		if(date !== null){
			validationUtils.validateISODate(date, function(err){
				callback(err);
			});
		}else{
			callback(null);
		}
	}else{
		return callback(errors.newError('ver-mis-sched'));
	}
}

function validateStatus(meta, entity, validationType, callback){
	var status = meta.status;
	if(['published', 'draft'].indexOf(status) < 0){
		return callback(errors.newError('ver-inv-status', {status: status}));
	}
	callback(null);
}
function validateCreatedAt(meta, entity, validationType, callback){
	var date = meta.createdAt;
	validationUtils.validateISODate(date, function(err){
		callback(err);
	});
}

/**
 * Check guid
 * @param meta
 * @param entity
 * @param validationType
 * @param callback
 * @returns {*}
 */
function validateGuid(meta, entity, validationType, callback){
	var guid = meta.guid;
	if(!check.string(guid)){
		return callback(errors.newError('ver-inv-guid', {applied: guid}));
	}
	dao.checkIfGuidExists(entity, guid, function(err, res){
		if(err){
			return callback(err);
		}
		if((res !== false &&
			validationType === 'add')||
			(res !== true &&
			validationType === 'update')){
			return callback(errors.newError('ver-inv-guid', {applied: guid}));
		}
		callback(null);
	});



}

exports.validateMetaData = function(meta, entity, validationType, callback){
	var validationTypes = {
		add: {
			functions: [
				validateScheduledAt,
				validateStatus,
				validateCreatedAt,
				validateGuid
			]
		},
		update: {
			functions: [
				validateScheduledAt,
				validateStatus,
				validateCreatedAt,
				validateGuid
			]
		}
	};
	if(validationTypes.hasOwnProperty(validationType)){
		var checkIndex = 0;
		var valCaller = function(){
			var context = validationTypes[validationType].context;
			validationTypes[validationType].functions[checkIndex](meta, entity, validationType, function(err, res){
				if(err){
					callback(err);
				} else if(validationTypes[validationType].functions[checkIndex+1] !== undefined){
					checkIndex++;
					valCaller();
				}else{
					callback(null, res);
				}
			}, context);
		};
		if( !check.object(meta) ){
			return callback(errors.newError('ver-inv-type', {key: 'rootObject'}));
		}
		valCaller();
	}else{
		callback(errors.newError('ver-inv-val-type'));
	}

};
