'use strict';
var errors = require('../utils/errors');
var version = require('../versioning/services');
var logging = require('../utils/logging');
errors.defineError('je-inv-job-cd', 'System error', 'Invalid job code given', 'Worker.JobExecuter');
errors.defineError('je-inv-job-inp', 'System error', 'Invalid job input given', 'Worker.JobExecuter');
errors.defineError('je-job-ex', 'System error', 'Error thrown while executing the job', 'Worker.JobExecuter');

/**
 * Validates if required input parameters exist for job
 * @param job
 * @param requiredInput
 * @throws {Error} if input not valid
 */
exports.validateInputParameters = function(job, requiredInput){
	for( var ri in requiredInput){
		if(!job.input.hasOwnProperty(requiredInput[ri])){
			throw errors.newError('je-inv-job-inp', {missingInput: requiredInput[ri]});
		}
	}
};

/**
 * This function is for the worker, who is executing a given job, do not use this for other purposes!
 * @param job
 * @param callback
 */
exports.executeJob = function (job, callback) {
	try{
		switch (job.code) {
			case 'test-job':
				process.stderr.write('Test job error (do not panic, just testing it!)');
				callback(null, 'Test job complete!');
				break;
			case 'hotelgolive':
	    version.handleGoLive('hotel_v', 'hotel', function(err, res){
					callback(err, 'Hotel goLive complete! ' + res);
				});
				break;
			case 'htlgrpgolive':
	    version.handleGoLive('hotel_group_v', 'hotel_group', function(err, res){
					callback(err, 'Hotel Group goLive complete! '+res);
				});
				break;
			case 'htlndgrouplive':
      version.handleGoLive('hotel_group_v', 'hotel_group', function(err){
					if(err){
						return callback(err);
					}
        version.handleGoLive('hotel_v', 'hotel', function(err, res) {
						callback(err, 'Group and hotel goLive complete! '+res);
					});
				});
				break;
			case 'trustContentImport':
				require('../integration/TrustContent/services').startCoordinatedImportFromFtp(job.input.hotelGroupId, function(err, res){
					//if error occurred, retry
					if(err && err.data.retry){
						require('./scheduler').scheduleJob(job, {delay: 10}, function(errS){
							logging.info('Rescheduled TrustContent importer', err, errS);
							callback(err, res);
						});
					}else{
						callback(err, res);
					}
				});
				break;
			case 'updateBlacklist':
				require('../integration/TrustContent/services').updateHotelBlackList(job.input.hotelGroupId, callback);
				break;
			case 'unpublishBlacklistedHotels':
				require('../integration/TrustContent/services').unpublishBlacklistedHotels(job.input.hotelGroupId, callback);
				break;
			case 'trustContentImportPart':
				require('../integration/TrustContent/services').importFromS3(job.input.hotelGroupId, callback, 0, 10000, job.input.files);
				break;
			case 'websiteImport':
				require('../integration/TrustContent/services').startCoordinatedImportFromWebsite(job.input.hotelGroupId, function(err, res){
					//if error occurred, retry
					if(err && err.data.retry){
						require('./scheduler').scheduleJob(job, {delay: 10}, function(errS){
							logging.info('Rescheduled Website importer', err, errS);
							callback(err, res);
						});
					}else{
						callback(err, res);
					}
				});
				break;
			case 'websiteImportPart':
				require('../integration/TrustContent/services').importWebFromS3(job.input.hotelGroupId, callback, job.input.hotels);
				break;
      case 'importslhcategories':
	      require('../integration/importer').startImport(job.input.hgid, function(err, res){
          if(err){
            return callback(err);
          }else if(res !== 'Nothing to import'){
            version.handleGoLive('hotel_group_v', 'hotel_group', function(err){
              callback(err, 'Category import done');
            });
          }else{
            callback(null, res);
          }
        });
        break;
			case 'generateReportImpressions':

				exports.validateInputParameters(job, ['hotelGroupId', 'recipients', 'days']);

				var startImpressions = new Date();
				var endImpressions = new Date();
				startImpressions.setDate( startImpressions.getDate() - job.input.days );
				//beginning of day
				startImpressions.setHours(0, 0, 0, 0);

				require('../reporting/services').getHotelsImpressionQtyByHour(
					job.input.hotelGroupId,
					startImpressions,
					endImpressions,
					job.input.recipients,
					callback
				);
				break;
			case 'generateReportActivations':

				exports.validateInputParameters(job, ['hotelGroupId', 'recipients', 'days']);

				var startActivations = new Date();
				var endActivations = new Date();
				startActivations.setDate( startActivations.getDate() - job.input.days );
				//beginning of day
				startActivations.setHours(0, 0, 0, 0);

				require('../reporting/services').getAppRegistrationQtyByHour(
					job.input.hotelGroupId,
					startActivations,
					endActivations,
					job.input.recipients,
					callback
				);
				break;
			case 'downloadAccessLogs':
				require('../utils/leasewebSDK').downloadAccessLogs(callback);
				break;
			case 'importAccessLogs':
				require('../utils/leasewebSDK').importNewAccessLogFiles(callback);
				break;
			default:
				callback(errors.newError('je-inv-job-cd', {givenJobCode: job.code}));
				break;
		}
	}catch(e){
		callback(errors.newError('je-job-ex', {originalError: e.stack}));
	}
};
