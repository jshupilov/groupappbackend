'use strict';
var validationUtils = require('../utils/validationUtils');
var errors = require('../utils/errors');
var check = require('check-types');

errors.defineError('wjv-key-miss', 'System error', 'Worker job is missing a required key', 'Worker.jobHandler');
errors.defineError('wjv-inv-val', 'System error', 'Worker job has an invalid value', 'Worker.jobHandler');
errors.defineError('wjv-inv-type', 'System error', 'Worker job has a value with invalid data type', 'Worker.jobHandler');

/**
 *
 * @param name
 * @param code
 * @param input {Object}
 * @param priority
 * @param callback
 */
exports.createJob = function(name, code, input, priority, callback){

	var job = {
		name: name,
		code: code,
		input: input,
		priority: priority | 0
	};

	callback(null, job);
};

exports.validateJob = function(job, callback){
	var requiredFields = ['name', 'code', 'priority', 'input'];

	validationUtils.validateWithArrayOfFunctionsWithCallbacks(
		[
			function(job, cb){
				validationUtils.validateHasKeys(requiredFields, job, function(missingKey){
					if(missingKey){
						return cb(errors.newError('wjv-key-miss', {key: missingKey}));
					}
					cb(null);
				});
			},
			//validate name
			function(job, cb){
				if( !check.unemptyString(job.name) ){
					return cb(errors.newError('wjv-inv-val', {key: 'name'}));
				}
				cb(null);
			},
			//validate code
			function(job, cb){
				if( !check.unemptyString(job.code) ){
					return cb(errors.newError('wjv-inv-val', {key: 'code'}));
				}
				cb(null);
			},
			//validate priority
			function(job, cb){
				if( !check.number(job.priority) ){
					return cb(errors.newError('wjv-inv-type', {key: 'priority'}));
				}
				cb(null);
			},
			//validate input
			function(job, cb){
				if( !check.object(job.input) ){
					return cb(errors.newError('wjv-inv-type', {key: 'input'}));
				}
				cb(null);
			}
		],
		[
			job
		],
		function(err){
			callback(err);
		}
	);

};
