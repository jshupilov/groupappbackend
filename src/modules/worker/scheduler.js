'use strict';
var errors = require('../utils/errors');
var logging = require('../utils/logging');
var jobHandler = require('./jobHandler');
var check = require('check-types');
var iron_mq = require('iron_mq');
var imq = new iron_mq.Client();
var CronJob = require('cron').CronJob;
var q = require('q');
var heroku = new (require('heroku-client'))({token: process.env.HEROKU_API_TOKEN});
var cronJobs = [];
var coordinatorStarted = false;

errors.defineError('ws-jb-sch-err', 'System error', 'Error posting a given job to MQ', 'Worker.Scheduler');
errors.defineError('ws-jb-get-err', 'System error', 'Error getting message from MQ', 'Worker.Scheduler');
errors.defineError('ws-jb-not-done', 'Job is not done', 'Job is still in progress', 'Worker.Scheduler');
errors.defineError('ws-jb-sch-inv-inp', 'System error', 'Invalid input given for job scheduling', 'Worker.Scheduler');
errors.defineError('ws-inv-jb-sch', 'System error', 'Invalid jobs schedule, must be an array', 'Worker.Scheduler');
errors.defineError('ws-inv-jb-sch-def', 'System error', 'Error occurred while scheduling a static job - invalid definition', 'Worker.Scheduler');
errors.defineError('ws-err-jb-sch', 'System error', 'Error occurred while scheduling a cronjob with given schedule string', 'Worker.Scheduler');
errors.defineError('ws-err-jb-sch-jb', 'System error', 'Error occurred while scheduling a cronjob with given job', 'Worker.Scheduler');
errors.defineError('ws-err-sch-fail', 'System error', 'Error occurred while scheduling a failed job to queue', 'Worker.Scheduler');
errors.defineError('ws-cmpl-sch-fail', 'System error', 'Error occurred while scheduling a completed job to queue', 'Worker.Scheduler');
errors.defineError('ws-cmpl-del-fail', 'System error', 'Error occurred while deleting a completed job from queue', 'Worker.Scheduler');
errors.defineError('ws-err-del-fail', 'System error', 'Error occurred while deleting a failed job from queue', 'Worker.Scheduler');
errors.defineError('ws-err-new-dyn', 'System error', 'Error starting a new dyno', 'Worker.Scheduler');
errors.defineError('ws-jb-st-to', 'System error', 'Timeout, while getting the job status', 'Worker.Scheduler');
errors.defineError('ws-jb-no-res', 'System error', 'No job result found, maybe still in progress.', 'Worker.Scheduler');
errors.defineError('ws-jb-err-peek-res', 'System error', 'Error occurred while fetching result of job from MQ', 'Worker.Scheduler');
errors.defineError('ws-err-jb-res-parse', 'System error', 'Error occurred while parsing the job result', 'Worker.Scheduler');

/**
 * Will schedule a job to a queue (iron MQ) for immediate execution (depending on priority)
 * @param job {Object}
 * @param options {Object}
 * @param callback {Function}
 */
exports.scheduleJob = function(job, options, callback){
	//validate job
	jobHandler.validateJob(job, function(err){
		if(err){
			return callback(err);
		}

		if(!check.object(options)){
			return callback(errors.newError('ws-jb-sch-inv-inp', {key: 'options'}));
		}

		//validate options
		if(options.hasOwnProperty('timeout')){
			if(!check.integer(options.timeout)){
				return callback(errors.newError('ws-jb-sch-inv-inp', {key: 'timeout'}));
			}
			if( options.timeout < 0 || options.timeout > 86400){
				return callback(errors.newError('ws-jb-sch-inv-inp', {key: 'timeout', reason: 'out of range 0 - 86400'}));
			}
		}else{
			options.timeout = 60;
		}

		if(options.hasOwnProperty('delay')){
			if(!check.integer(options.delay)){
				return callback(errors.newError('ws-jb-sch-inv-inp', {key: 'delay'}));
			}
			if( options.delay < 0 || options.delay > 604800){
				return callback(errors.newError('ws-jb-sch-inv-inp', {key: 'delay', reason: 'out of range 0 - 604800'}));
			}
		}else{
			options.delay = 0;
		}

		if(options.hasOwnProperty('expires_in')){
			if(!check.integer(options.expires_in)){
				return callback(errors.newError('ws-jb-sch-inv-inp', {key: 'expires_in'}));
			}
			if( options.expires_in < 0 || options.expires_in > 2592000){
				return callback(errors.newError('ws-jb-sch-inv-inp', {key: 'expires_in', reason: 'out of range 0 - 2592000'}));
			}
		}else{
			options.expires_in = 604800;
		}

		//make up the queue name
		options.body = JSON.stringify(job);
		var queue = imq.queue('scheduled_jobs');

		queue.post(options, function(error, body) {
			if(error){
				return callback(errors.newError('ws-jb-sch-err', {originalError: error.message}));
			}
			callback(null, body);
		});
	});

};

exports.stopStaticJobs = function(callback){
	for(var i in cronJobs){
		cronJobs[i].stop();
	}
	callback();
};

exports.latestQueuedJobs = [];

/**
 * Handle static jobs schedule
 * @see https://github.com/ncb000gt/node-cron
 * @param jobSchedule {Object[]} Array of objects like {job: {...}, scheduleInCrontabFormat: '* * ...'}
 * @param callback
 */
exports.handleStaticJobSchedule = function(jobSchedule, callback){
	cronJobs = [];
	exports.latestQueuedJobs = [];
	if(!check.array(jobSchedule)){
		return callback(errors.newError('ws-inv-jb-sch'));
	}

	function schedule(jobDef){
		//schedule when right time is here
		return new CronJob(jobDef.scheduleInCrontabFormat, function(){
			exports.scheduleJob(jobDef.job, {}, function(err, messageId){
				if(err){
					logging.err('Scheduled of static job (' + jobDef.job.name + ') failed: ' + err.message);
					return;
				}
				logging.info('Scheduled static job (' + jobDef.job.name + ') for execution');
				//keep latest queue under 10 elements
				exports.latestQueuedJobs.splice(9, Number.MAX_VALUE, {messageId: messageId, jobDefinition: jobDef});
			});
		}, null, false);
	}

	function doVal(p, jobDef){
		jobHandler.validateJob(jobDef.job, function(err){
			if(err){
				return p.reject(err);
			}
			p.resolve();
		});
	}
	//validate jobs
	var promises = [];
	for( var ji in jobSchedule ){
		var p = q.defer();
		promises.push(p.promise);
		doVal(p, jobSchedule[ji]);
	}

	//all jobs are validated
	q.all(promises).done(function(){
		for( var si in jobSchedule ){
			var jobDef = jobSchedule[si];
			//if crontab pattern
			if(jobDef.scheduleInCrontabFormat){
				try{
					var cJob = schedule(jobDef);
					cronJobs.push(cJob);
				}catch (e){
					return callback(errors.newError('ws-err-jb-sch', {originalError: e}));
				}
			}else{
				return callback(errors.newError('ws-inv-jb-sch-def'));
			}

		}
		//start all the cronjobs
		for(var ci in cronJobs){
			cronJobs[ci].start();
		}
		callback(null, {totalCronJobs: cronJobs.length});
	}, function(err){
		return callback(errors.newError('ws-err-jb-sch-jb', {originalError: err}));
	});


};

/**
 * Fail the job
 * @param job
 * @param messageId
 * @param errorCode
 * @param error
 * @param callback
 */
function failJob(job, messageId, errorCode, error, callback){
	var queueScheduled = imq.queue('scheduled_jobs');
	console.log('failJob', messageId);

	queueScheduled.del(messageId, function(err){
		if(err){
			console.log('Failed to delete failed job from scheduled_jobs queue (assuming it is gone already)', err);
		}

		var queue = imq.queue('failed_jobs');
		var options = {
			body: {
				job: job,
				errorCode: errorCode,
				error: error,
				messageId: messageId
			}
		};
		options.body = JSON.stringify(options.body);
		queue.post(options, function(err){
			if(err){
				console.log( 'Failed to post a message to failed_jobs queue', err );
			}
			callback(null);
		});
	});

}

var coordinatorStopPromise = null;

/**
 *  Stop the background Coordinator
 */
exports.stopCoordinator = function(callback){
	if(coordinatorStopPromise === null){
		coordinatorStopPromise = q.defer();
	}

	coordinatorStopPromise.promise.done(function(){
		coordinatorStopPromise = null;
		callback();
	});

	//in case coordinator does not work
	if(!coordinatorStarted){
		coordinatorStopPromise.resolve();
	}
};

/**
 * Background worker/coordinator, which monitors the jobs MQ and executes the incoming jobs
 * @param [waitTime] {Number} Time to wait for a message in seconds, default is 30
 * @returns {boolean}
 */
exports.coordinateScheduledJobs = function(waitTime){
	waitTime = waitTime !== undefined ? waitTime : 30;
	if(coordinatorStarted){
		return false;
	}
	coordinatorStarted = true;
	var queue = imq.queue('scheduled_jobs');
	//time for the process to run
	var timeout = 60;


	function listener(){
		//if stop is required
		if(coordinatorStopPromise !== null){
			coordinatorStarted = false;
			return coordinatorStopPromise.resolve();
		}

		queue.get({wait: waitTime, timeout: timeout}, function(error, message) {

			if(error){
				logging.warning('Error listening for scheduled jobs', error);
				return listener();
			}
			//no message
			if(!message){
				return listener();
			}

			try{
				var job = JSON.parse(message.body);
			}catch(e){
				return failJob(message.body, message.id, 'invalid-body', e, function(){
					logging.error(e.message, e.data);
					//look for next job
					listener();
				});
			}

			jobHandler.validateJob(job, function(err){
				//if invalid job
				if(err){
					return failJob(job, message.id, 'invalid-job', err, function(){
						logging.error(err.message, err.data);
						//look for next job
						listener();
					});
				}else{
					//is valid job, execute it
					setTimeout(function(){
						exports.executeJobInIsolation(job, message.id, function(err){
							if(err){
								return failJob(job, message.id, 'cannot-start-job', err, function(){
									logging.error(err.message, err.data);
									//look for next job
								});
							}

						});
					});
					listener();

				}

			});
		});
	}

	listener();
	return true;

};

/**
 * Check job status
 * @param messageId
 * @param callback
 */
exports.checkJobStatus = function(messageId, callback){
	var queue = imq.queue('scheduled_jobs');

	queue.msg_get(
		messageId,
		function(error, message) {
			if(error){
				return callback(errors.newError('ws-jb-get-err', {originalError: error.message}));
			}
			if( message.status === 'deleted' ){
				callback(null, {done: true});
			}else{
				callback(null, {done: false});
			}
		}
	);

};

/**
* Get the result of the job
* @param messageId
* @param callback
* @param [maxChecks] Maximum amount of calls to get the status
*/
exports.getJobResult = function(messageId, callback, maxChecks){
	var queueCompleted = imq.queue('completed_jobs');
	maxChecks = check.integer(maxChecks) ? maxChecks : 10;
	function checker(messageId){
		var numberOfMessages = 100;

		/**
		 * Release message approximately 5 seconds after all checks have been made
		 * @param id
		 * @param p
		 */
		function release(id, p){
			queueCompleted.msg_release(id, {delay: maxChecks + 5}, function(){
				p.resolve();
			});
		}

		function delAndEnd(message, completedJob){
			queueCompleted.del(message.id, function(delerr){
				//do not panic if delete fails, it does not really matter
				return callback(null, completedJob, delerr);
			});
		}
		queueCompleted.get_n({timeout: 30, n: numberOfMessages}, function(err, messages){
			var possibleErr;
			var promises = [];
			if(!err){
				for( var mi in messages){
					var message = messages[mi];
					var completedJob;
					try{
						completedJob = JSON.parse(message.body);
					}catch(e){
						logging.error( JSON.stringify(errors.newError('ws-err-jb-res-parse', {originalError: e.message, message: message})) );
						continue;
					}
					if(completedJob.messageId === messageId){
						return delAndEnd(message, completedJob);
					}else{
						var p = q.defer();
						promises.push(p.promise);
						release(message.id, p);
					}
				}

				possibleErr = errors.newError('ws-jb-no-res');
			}else{
				possibleErr = errors.newError('ws-jb-err-peek-res', {originalError: err});
				logging.error( JSON.stringify(possibleErr) );
			}

			q.all(promises).finally(function(){
				if(--maxChecks){
					setTimeout(function(){
						checker(messageId);
					}, 1000);
				}else{
					callback(possibleErr);
				}
			});


		});
	}

	checker(messageId);
};

/**
 * Wait until job is finished
 * @param messageId
 * @param callback
 * @param [maxChecks] Maximum amount of test calls to do - default 300/5 mins (1 test is at least 1 sec)
 */
exports.waitForJobCompletion = function(messageId, callback, maxChecks){
	maxChecks = maxChecks ? maxChecks : 300; //wait for 5 minutes max
	function checker(){
		exports.checkJobStatus(messageId, function(err, res){
			if(err){
				return callback(err);
			}else if(res.done){
				return callback(null);
			}else if(maxChecks--){
				//wait 1 sec before retrying
				setTimeout(checker, 1000);
			}else{
				return callback(errors.newError('ws-jb-st-to'));
			}
		});
	}
	checker();
};

/**
 * Execute a given job right away and wait for the completion
 * @param job
 * @param callback - will get err as first param and messageId as second param
 * @param [maxChecks] Maximum amount of test calls to do - default 300/5 mins (1 test is at least 1 sec)
 */
exports.executeJobAndWaitForCompletion = function(job, callback, maxChecks){

	exports.scheduleJob(job, {delay: 60}, function(err, messageId){
		if(err){
			return callback(err, messageId);
		}
		exports.executeJobInIsolation(job, messageId, function(err){
			if(err){
				return callback(err, messageId);
			}
			exports.waitForJobCompletion(messageId, function(err){
				if(err){
					return callback(err, messageId);
				}
				return exports.getJobResult(messageId, callback);
			}, maxChecks);
		});
	});

};


/**
 * Starts a new dyno and executes a given job/command in it
 * @param job
 * @param messageId
 * @param callback
 */
exports.executeJobInIsolation = function(job, messageId, callback){
	//validate job first
	jobHandler.validateJob(job, function(err){
		if(err){
			return callback(err);
		}

		//command to execute
		var command = 'node';
		var args = ['--expose-gc', 'src/worker.js', JSON.stringify(job), messageId];

		//if we are in heroku, start a new dyno
		if(process.env.APP_NAME){
			var argsA = [];
			for(var ai in args){
				argsA.push( '"' + (('' + args[ai]).replace(/"/g, '\x5C"')) + '"' );
			}
			heroku.apps(process.env.APP_NAME).dynos().create({ command: command + ' ' + argsA.join(' '), attach: null }, function (err, res) {

				if(err){
					return callback(errors.newError('ws-err-new-dyn', {originalError: err}));
				}
				console.log('dyno started', res);
				callback(err, res);

			});
			//locally (if we test) we will spawn a child
		}else{

			var env = {};
			for(var ei in process.env){
				env[ei] = process.env[ei];
			}

			//do not log to papertrail
			delete env.PAPERTRAIL_HOST;
			delete env.PAPERTRAIL_PORT;

			var spawn = require('child_process').spawn;
			var childProcess= spawn(command, args, {env: env, cwd: process.cwd() });

			childProcess.stdout.on('data', function (data) {
				console.log('CHILD: stdout: ' + data);
			});

			childProcess.stderr.on('data', function (data) {
				console.log('CHILD: stderr: ' + data);
			});

			childProcess.on('close', function (code) {
				console.log('CHILD: child process exited with code ' + code);
			});
			callback(null);

		}

	});

};
