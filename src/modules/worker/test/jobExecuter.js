'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var jobExecuter = require('../jobExecuter');
var scheduler = require('../scheduler');
var htlSrv = require('../../hotels/services');
var htlGrpSrv = require('../../hotel_groups/services');
var rprtSrv = require('../../reporting/services');
var integration = require('../../integration/TrustContent/services');
var integrationImporter = require('../../integration/importer');
var leaseWebSDK = require('../../utils/leasewebSDK');
var version = require('../../versioning/services');

lab.experiment('Worker.jobExecuter.executeJob', {timeout: 10000}, function(){
	var htlGetLive, htlGrpGetMeLive, _handleGoLive, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11;
	lab.before(function(done){
		htlGetLive = htlSrv.getMeLive;
		htlGrpGetMeLive = htlGrpSrv.getMeLive;
		_handleGoLive = version.handleGoLive;
		f1 = integration.updateHotelBlackList;
		f2 = integration.unpublishBlacklistedHotels;
		f3 = integrationImporter.startImport;
		f4 = integration.startCoordinatedImportFromFtp;
		f5 = integration.startCoordinatedImportFromWebsite;
		f6 = scheduler.scheduleJob;
		f7 = integration.importFromS3;
		f8 = integration.importWebFromS3;
		f9 = version.getLiveData;
		f10 = rprtSrv.getHotelsImpressionQtyByHour;
		f11 = rprtSrv.getAppRegistrationQtyByHour;
		done();
	});

	lab.afterEach(function(done){
		version.handleGoLive = _handleGoLive;
		integration.updateHotelBlackList = f1;
		integration.unpublishBlacklistedHotels = f2;
		integrationImporter.startImport = f3;
		integration.startCoordinatedImportFromFtp = f4;
		integration.startCoordinatedImportFromWebsite = f5;
		scheduler.scheduleJob = f6;
		integration.importFromS3 = f7;
		integration.importWebFromS3 = f8;
		version.getLiveData = f9;
		rprtSrv.getHotelsImpressionQtyByHour = f10;
		rprtSrv.getAppRegistrationQtyByHour = f11;
		done();
	});
	lab.after(function(done){
		htlSrv.getMeLive = htlGetLive;
		htlGrpSrv.getMeLive = htlGrpGetMeLive;
		done();
	});
	lab.test('should complain about invalid code', function(done){
		jobExecuter.executeJob({code: 'blabla'}, function(err){
			testingTools.expectError(err, 'je-inv-job-cd');
			done();
		});
	});

	lab.test('should complain about invalid code', function(done){
		jobExecuter.executeJob({code: 'blabla'}, function(err){
			testingTools.expectError(err, 'je-inv-job-cd');
			done();
		});
	});

	lab.test('should execute job with correct code', function(done){
		jobExecuter.executeJob({code: 'test-job'}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal('Test job complete!');
			done();
		});
	});
	lab.test('Run hotelGolive should go well', function(done){
		version.handleGoLive = function(version, entity, cb){
			cb(null, 'done');
		};
		jobExecuter.executeJob({code: 'hotelgolive'}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.equal('Hotel goLive complete! done');
			done();
		});
	});
	lab.test('Run hotelGroupGolive should go well', {timeout: 10000}, function(done){
		version.handleGoLive = function(version, entity, cb){
			cb(null, 'done');
		};
		jobExecuter.executeJob({code: 'htlgrpgolive'}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.equal('Hotel Group goLive complete! done');
			done();
		});
	});
	lab.test('Run hotel and group goLive should go well', {timeout: 5000}, function(done){
		version.getLiveData = function(v, cb){
			cb(null, [], []);
		};
		jobExecuter.executeJob({code: 'htlndgrouplive'}, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.match(/Group and hotel goLive complete/g);
			done();
		});
	});

	lab.test('Run hotel and group should give group failure', {timeout: 5000}, function(done){
		version.handleGoLive = function(version, entity, cb){
			cb(new Error('Something is wrong in state of denmark'));
		};
		jobExecuter.executeJob({code: 'htlndgrouplive'}, function(err){
			testingTools.code.expect(err.message).to.be.equal('Something is wrong in state of denmark');
			done();
		});
	});

	lab.test('Run updateBlacklist should go well', function(done){
		integration.updateHotelBlackList = function(h, cb){
			cb(null, '89-sdfnsjd234432fip9wfe');
		};
		jobExecuter.executeJob({code: 'updateBlacklist', input: {hotelGroupId: 'slh'}}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal('89-sdfnsjd234432fip9wfe');
			done();
		});
	});

	lab.test('Run unpublishBlacklistedHotels should go well', function(done){
		integration.unpublishBlacklistedHotels = function(h, cb){
			cb(null, '89-sdfnsjdfip9wfe');
		};
		jobExecuter.executeJob({code: 'unpublishBlacklistedHotels', input: {hotelGroupId: 'slh'}}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal('89-sdfnsjdfip9wfe');
			done();
		});
	});

	lab.test('importslhcategories should go well', {timeout: 100000}, function(done){
		integrationImporter.startImport = function(h, cb){
			cb(null, 'Category import done');
		};
		version.handleGoLive = function(v, e, cb){
			cb(null);
		};
		jobExecuter.executeJob({code: 'importslhcategories', input: {hgid: 'slh'}}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.equal('Category import done');
			done();
		});
	});

	lab.test('importslhcategories should give nothing to import failure', {timeout: 120000}, function(done){
		integrationImporter.startImport = function(h, cb){
			cb(null, 'Nothing to import');
		};
		jobExecuter.executeJob({code: 'importslhcategories', input: {hgid: 'slh'}}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.be.equal('Nothing to import');
			done();
		});
	});

	lab.test('importslhcategories should give importer failure', function(done){
		integrationImporter.startImport = function(h, cb){
			cb(new Error('s;ldfk90-u2h-rpw'));
		};
		jobExecuter.executeJob({code: 'importslhcategories', input: {hgid: 'someotherhgid'}}, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('s;ldfk90-u2h-rpw');
			done();
		});
	});


	lab.test('trustContentImport Should work', function(done){
		integration.startCoordinatedImportFromFtp = function(h, cb){
			cb(null);
		};
		jobExecuter.executeJob({code: 'trustContentImport', input: {hotelGroupId: 'someotherhgid'}}, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('trustContentImport Should give error', function(done){
		integration.startCoordinatedImportFromFtp = function(h, cb){
			var e = new Error('s;ldfk90343443-u2h-rpw');
			e.data = {};
			cb(e);
		};
		jobExecuter.executeJob({code: 'trustContentImport', input: {hotelGroupId: 'someotherhgid'}}, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('s;ldfk90343443-u2h-rpw');
			done();
		});
	});

	lab.test('trustContentImport Should give error and retry', function(done){
		integration.startCoordinatedImportFromFtp = function(h, cb){
			var e = new Error('s;ldfk90343443-u2h-rpw');
			e.data = {retry: true};
			cb(e);
		};
		scheduler.scheduleJob = function(j, c, cb){
			cb();
		};
		jobExecuter.executeJob({code: 'trustContentImport', input: {hotelGroupId: 'someotherhgid'}}, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.data.retry).to.equal(true);
			done();
		});
	});

	lab.test('trustContentImportPart Should work', function(done){

		integration.importFromS3 = function(h, cb){
			cb(null, 'importFromS3RES');
		};
		jobExecuter.executeJob({code: 'trustContentImportPart', input: {hotelGroupId: 'someotherhgid'}}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal('importFromS3RES');
			done();
		});
	});


	lab.test('websiteImport Should work', function(done){
		integration.startCoordinatedImportFromWebsite = function(h, cb){
			cb(null);
		};
		jobExecuter.executeJob({code: 'websiteImport', input: {hotelGroupId: 'someotherhgid'}}, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('websiteImport Should give error', function(done){
		integration.startCoordinatedImportFromWebsite = function(h, cb){
			var e = new Error('s;ldfk903433333443-u2h-rpw');
			e.data = {};
			cb(e);
		};
		jobExecuter.executeJob({code: 'websiteImport', input: {hotelGroupId: 'someotherhgid'}}, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('s;ldfk903433333443-u2h-rpw');
			done();
		});
	});

	lab.test('websiteImport Should give error and retry', function(done){
		integration.startCoordinatedImportFromWebsite = function(h, cb){
			var e = new Error('s;ldfk9034344sdfsfdsdf3-u2h-rpw');
			e.data = {retry: true};
			cb(e);
		};
		scheduler.scheduleJob = function(j, c, cb){
			cb();
		};
		jobExecuter.executeJob({code: 'websiteImport', input: {hotelGroupId: 'someotherhgid'}}, function(err){
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('s;ldfk9034344sdfsfdsdf3-u2h-rpw');
			testingTools.code.expect(err.data.retry).to.equal(true);
			done();
		});
	});

	lab.test('websiteImportPart Should work', function(done){

		integration.importWebFromS3 = function(h, cb){
			cb(null, 'websiteImportPartRES');
		};
		jobExecuter.executeJob({code: 'websiteImportPart', input: {hotelGroupId: 'someotherhgid'}}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal('websiteImportPartRES');
			done();
		});
	});

	lab.test('if exception occurs, it should handle it', function(done) {
		jobExecuter.executeJob(undefined, function(err){
			testingTools.expectError(err, 'je-job-ex');
			done();
		});
	});

	lab.test('generateReportImpressions Should work', function(done){

		rprtSrv.getHotelsImpressionQtyByHour = function(h, sd, ed, r, cb){
			cb(null, 'getHotelsImpressionQtyByHourRES');
		};

		jobExecuter.executeJob({
			code: 'generateReportImpressions',
			input: {
				hotelGroupId: 'someotherhgid',
				days: 2,
				recipients: []
			}
		}, function(err, res){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(res).to.equal('getHotelsImpressionQtyByHourRES');
			done();
		});
	});

	lab.test('generateReportActivations Should work', function(done){

		rprtSrv.getAppRegistrationQtyByHour = function(h, sd, ed, r, cb){
			cb(null, 'generateReportActivationsRES');
		};

		jobExecuter.executeJob({
			code: 'generateReportActivations',
			input: {
				hotelGroupId: 'someotherhgid',
				days: 2,
				recipients: [{email: 'group-app-dev@cardola.com'}]
			}
		}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal('generateReportActivationsRES');
			done();
		});
	});
});

lab.experiment('jobExecutor AccessLog processes', function() {
	var f1, f2;

	lab.before(function(done){
		f1 = leaseWebSDK.downloadAccessLogs;
		f2 = leaseWebSDK.importNewAccessLogFiles;
		done();
	});

	lab.afterEach(function(done){
		leaseWebSDK.downloadAccessLogs = f1;
		leaseWebSDK.importNewAccessLogFiles = f2;
		done();
	});

	lab.test('downloadAccessLogs should work', function(done) {
		leaseWebSDK.downloadAccessLogs = function(cb){
			cb(null, 'downloadAccessLogsDone');
		};

		jobExecuter.executeJob({code: 'downloadAccessLogs', input: {}}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal('downloadAccessLogsDone');
			done();
		});

	});

	lab.test('importAccessLogs should work', function(done) {
		leaseWebSDK.importNewAccessLogFiles = function(cb){
			cb(null, 'importAccessLogsDone');
		};

		jobExecuter.executeJob({code: 'importAccessLogs', input: {}}, function(err, res){
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res).to.equal('importAccessLogsDone');
			done();
		});

	});
});
