/**
 * Created by jevgenishupilov on 30/04/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var q = require('q');
var jobHandler = require('../jobHandler');


lab.experiment('Worker.jobHandler module', function() {
	lab.test('should provide methods for dealing with jobs', function(done) {
		testingTools.code.expect(jobHandler).to.include([
			'createJob',
			'validateJob'
		]);
		testingTools.code.expect(jobHandler.createJob).to.be.function();
		testingTools.code.expect(jobHandler.validateJob).to.be.function();
		done();
	});
});

lab.experiment('Worker.jobHandler.validateJob', function() {
	var getValidJob = null;
	lab.before(function(done){

		getValidJob = function(){
			return {
				name: 'Sample job',
				code: 'sample-job',
				priority: 0, //lowest
				input: {
					some: 'object with stuff'
				}
			};

		};

		done();
	});

	lab.test('should not give error about correct JOB ', function(done) {
		jobHandler.validateJob(getValidJob(), function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should give error if is missing some keys', function(done) {
		var job = getValidJob();

		function doVal(promise, i, job){
			jobHandler.validateJob(job, function(err){
				testingTools.expectError(err, 'wjv-key-miss');
				testingTools.code.expect(err.data.debuggingData.key).to.equal(i);
				promise.resolve();
			});
		}

		var promises = [];
		for(var i in job){
			var job2 = getValidJob();
			delete job2[i];

			var p = q.defer();
			promises.push(p.promise);
			doVal(p, i, job2);
		}

		q.all(promises).done(function(){
			done();
		});
	});

	lab.test('should give error if name is incorrect', function(done) {
		var job = getValidJob();
		job.name = null;
		jobHandler.validateJob(job, function(err){
			testingTools.expectError(err, 'wjv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('name');
			done();
		});
	});

	lab.test('should give error if code is incorrect', function(done) {
		var job = getValidJob();
		job.code = null;
		jobHandler.validateJob(job, function(err){
			testingTools.expectError(err, 'wjv-inv-val');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('code');
			done();
		});
	});

	lab.test('should give error if code is incorrect', function(done) {
		var job = getValidJob();
		job.priority = null;
		jobHandler.validateJob(job, function(err){
			testingTools.expectError(err, 'wjv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('priority');
			done();
		});
	});

	lab.test('should give error if input is not object', function(done) {
		var job = getValidJob();
		job.input = null;
		jobHandler.validateJob(job, function(err){
			testingTools.expectError(err, 'wjv-inv-type');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('input');
			done();
		});
	});
	
});
