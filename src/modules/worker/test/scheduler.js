/**
 * Created by jevgenishupilov on 30/04/15.
 */
'use strict';
var testingTools = require('../../../test/testingTools');
var logging = require('../../utils/logging');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var iron_mq = require('iron_mq');
var imq = new iron_mq.Client();
var scheduler = require('../scheduler');
var jobHandler = require('../jobHandler');
var heroku = new (require('heroku-client'))({token: process.env.HEROKU_API_TOKEN});
var q = require('q');

lab.test('Clearing queues for testing....', {timeout: 20000}, function(done) {
	var queue1 = imq.queue('scheduled_jobs');
	var queue2 = imq.queue('completed_jobs');
	var queue3 = imq.queue('failed_jobs');

	queue1.clear(function() {
		queue2.clear(function() {
			queue3.clear(function() {
				done();
			});
		});
	});
});

lab.experiment('Worker.Scheduler.stopCoordinator', function() {
	lab.before(function(done){
		scheduler.stopCoordinator(done);
	});

	lab.afterEach(function(done){
		scheduler.stopCoordinator(done);
	});

	lab.test('should stop the coordinator', function(done) {
		testingTools.code.expect(scheduler.coordinateScheduledJobs(0)).to.equal(true);
		//verify coordinator is running
		testingTools.code.expect(scheduler.coordinateScheduledJobs(0)).to.equal(false);

		scheduler.stopCoordinator(function(){
			//verify it will be started again - means it was stoppem meanwhile
			testingTools.code.expect(scheduler.coordinateScheduledJobs(0)).to.equal(true);
			done();
		});
	});

	lab.test('should work if called twice', {timeout: 5000}, function(done) {
		testingTools.code.expect(scheduler.coordinateScheduledJobs(0)).to.equal(true);
		//verify coordinator is running
		testingTools.code.expect(scheduler.coordinateScheduledJobs(0)).to.equal(false);
		var p1 = q.defer();
		var p2 = q.defer();

		scheduler.stopCoordinator(function(){
			//verify it will be started again - means it was stopped meanwhile
			testingTools.code.expect(scheduler.coordinateScheduledJobs(0)).to.equal(true);
			p1.resolve();
		});

		scheduler.stopCoordinator(function(){
			p2.resolve();
		});

		q.all([p1.promise, p2.promise]).done(function(){
			done();
		});
	});

});

lab.experiment('Worker.Scheduler module', function() {
	lab.test('should provide methods for dealing with scheduling', function(done) {
		var requiredFunctions = [
			'scheduleJob',
			'handleStaticJobSchedule',
			'coordinateScheduledJobs'
		];
		testingTools.code.expect(scheduler).to.include(requiredFunctions);
		//expect functions
		for(var i in requiredFunctions) {
			testingTools.code.expect(scheduler[requiredFunctions[i]]).to.be.function();
		}
		done();
	});
});

lab.experiment('Worker.Scheduler.scheduleJob', function() {
	var f1;
	lab.before(function(done) {
		f1 = jobHandler.validateJob;
		done();
	});

	lab.afterEach(function(done) {
		jobHandler.validateJob = f1;
		done();
	});

	lab.test('should validate given job', function(done) {
		var c1 = [];
		jobHandler.validateJob = function() {
			c1.push(arguments[0]);
			arguments[arguments.length - 1]('validated');
		};

		var job = {};
		scheduler.scheduleJob(job, {}, function(err) {
			testingTools.code.expect(c1).to.include(job);
			testingTools.code.expect(err).to.equal('validated');
			done();
		});
	});

	lab.test('should put a job to a MQ and return an message ID', {timeout: 10000}, function(done) {

		jobHandler.createJob('Some job', 'some-job', {}, 0, function(err, job) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			var queue = imq.queue('scheduled_jobs');

			scheduler.scheduleJob(job, {expires_in: 3}, function(err, messageId) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(messageId).to.be.string();
				//test if is in queue
				queue.msg_get(messageId, function(err, message) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(message.body).to.equal(JSON.stringify(job));
					//delete from queue
					queue.del(messageId, function(err) {
						testingTools.code.expect(err).to.be.null();
						done();
					});
				});
			});
		});

	});

});

lab.experiment('Worker.scheduler should fail', function() {

	testingTools.mockResponsesForExperiment(lab);

	lab.test('if posting to MQ failed', {timeout: 30000}, function(done) {

		jobHandler.createJob('Some job', 'some-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			scheduler.scheduleJob(job, {expires_in: 10}, function(err) {
				testingTools.expectError(err, 'ws-jb-sch-err');
				done();
			});
		});

	});
});


lab.experiment('Worker.scheduler should validate options', function() {
	var job, getOptions;

	lab.before(function(done) {

		getOptions = function() {
			return {
				timeout: 0,
				delay: 0,
				expires_in: 0
			};
		};

		jobHandler.createJob('Some job', 'some-job', {}, 0, function(err, joba) {
			testingTools.code.expect(err).to.be.null();
			job = joba;
			done();
		});

	});
	lab.after(function(done) {
		done();
	});

	lab.test('and give error if options is not an object', function(done) {
		scheduler.scheduleJob(job, null, function(err) {
			testingTools.expectError(err, 'ws-jb-sch-inv-inp');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('options');
			done();
		});
	});


	lab.test('and give error if timeout is not numeric', function(done) {
		var options = getOptions();
		options.timeout = null;
		scheduler.scheduleJob(job, options, function(err) {
			testingTools.expectError(err, 'ws-jb-sch-inv-inp');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('timeout');
			done();
		});
	});

	lab.test('and give error if timeout is too small', function(done) {
		var options = getOptions();
		options.timeout = -1;
		scheduler.scheduleJob(job, options, function(err) {
			testingTools.expectError(err, 'ws-jb-sch-inv-inp');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('timeout');
			done();
		});
	});

	lab.test('and give error if timeout is too big', function(done) {
		var options = getOptions();
		options.timeout = 242342424;
		scheduler.scheduleJob(job, options, function(err) {
			testingTools.expectError(err, 'ws-jb-sch-inv-inp');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('timeout');
			done();
		});
	});

	lab.test('and give error if delay is not numeric', function(done) {
		var options = getOptions();
		options.delay = null;
		scheduler.scheduleJob(job, options, function(err) {
			testingTools.expectError(err, 'ws-jb-sch-inv-inp');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('delay');
			done();
		});
	});

	lab.test('and give error if delay is too small', function(done) {
		var options = getOptions();
		options.delay = -1;
		scheduler.scheduleJob(job, options, function(err) {
			testingTools.expectError(err, 'ws-jb-sch-inv-inp');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('delay');
			done();
		});
	});

	lab.test('and give error if delay is too big', function(done) {
		var options = getOptions();
		options.delay = 242342424;
		scheduler.scheduleJob(job, options, function(err) {
			testingTools.expectError(err, 'ws-jb-sch-inv-inp');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('delay');
			done();
		});
	});

	lab.test('and give error if expires_in is not numeric', function(done) {
		var options = getOptions();
		options.expires_in = null;
		scheduler.scheduleJob(job, options, function(err) {
			testingTools.expectError(err, 'ws-jb-sch-inv-inp');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('expires_in');
			done();
		});
	});

	lab.test('and give error if expires_in is too small', function(done) {
		var options = getOptions();
		options.expires_in = -1;
		scheduler.scheduleJob(job, options, function(err) {
			testingTools.expectError(err, 'ws-jb-sch-inv-inp');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('expires_in');
			done();
		});
	});

	lab.test('and give error if expires_in is too big', function(done) {
		var options = getOptions();
		options.expires_in = 242342424;
		scheduler.scheduleJob(job, options, function(err) {
			testingTools.expectError(err, 'ws-jb-sch-inv-inp');
			testingTools.code.expect(err.data.debuggingData.key).to.equal('expires_in');
			done();
		});
	});
});

lab.experiment('Worker.Scheduler.handleStaticJobSchedule', function() {
	var job, f1, jobs, le;
	lab.before(function(done) {
		jobs = [];
		le = logging.err;
		f1 = scheduler.scheduleJob;

		jobHandler.createJob('Some job', 'some-code', {}, 0, function(e, j) {
			testingTools.code.expect(e).to.be.null();
			job = j;
			done();
		});
	});

	lab.beforeEach(function(done) {
		scheduler.scheduleJob = function() {
			jobs.push(arguments[0]);
			arguments[arguments.length - 1](null);
		};
		done();
	});

	lab.afterEach(function(done) {
		logging.err = le;
		scheduler.scheduleJob = f1;
		scheduler.stopStaticJobs(function() {
			done();
		});
	});

	lab.test('should give error if input is incorrect', function(done) {
		scheduler.handleStaticJobSchedule(null, function(err) {
			testingTools.expectError(err, 'ws-inv-jb-sch');
			done();
		});
	});

	lab.test('should add all scheduled jobs in file as cronjobs', function(done) {
		//every second
		var jobSchedule = [
			{
				scheduleInCrontabFormat: '* * * * * *',
				job: job
			}
		];
		jobs = [];

		scheduler.handleStaticJobSchedule(jobSchedule, function(err, res) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(res.totalCronJobs).to.equal(jobSchedule.length);
			setTimeout(function() {
				testingTools.code.expect(jobs.length).to.equal(1);
				done();
			}, 1000);
		});
	});

	lab.test('should give error about invalid crontab pattern', function(done) {
		var jobSchedule = [
			{
				scheduleInCrontabFormat: 'a * * * * *',
				job: job
			}
		];

		scheduler.handleStaticJobSchedule(jobSchedule, function(err) {
			testingTools.expectError(err, 'ws-err-jb-sch');
			done();
		});
	});

	lab.test('should give error about invalid job description', function(done) {
		var jobSchedule = [
			{
				scheduleInCrontabFormat: '* * * * * *',
				job: {}
			}
		];

		scheduler.handleStaticJobSchedule(jobSchedule, function(err) {
			testingTools.expectError(err, 'ws-err-jb-sch-jb');
			done();
		});
	});

	lab.test('should give error if no crontab pattern', function(done) {
		var jobSchedule = [
			{
				scheduleInCrontabFormat1: '* * * * * *',
				job: job
			}
		];

		scheduler.handleStaticJobSchedule(jobSchedule, function(err) {
			testingTools.expectError(err, 'ws-inv-jb-sch-def');
			done();
		});
	});



	lab.test('should log error if no crontab pattern', {timeout: 20000}, function(done) {
		var jobSchedule = [
			{
				scheduleInCrontabFormat: '* * * * * *',
				job: job
			}
		];
		logging.err = function(msg) {
			testingTools.code.expect(msg).to.include('blasd34fadsf');
			testingTools.code.expect(msg).to.include('failed');
			testingTools.code.expect(msg).to.include('Scheduled of static job');
			logging.err = le;
			done();
		};

		scheduler.scheduleJob = function() {
			arguments[arguments.length - 1](new Error('blasd34fadsf'));
		};

		scheduler.handleStaticJobSchedule(jobSchedule, function(err) {
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();

		});
	});

});


lab.experiment('Worker.Scheduler.executeJobInIsolation', function() {

	lab.afterEach(function(done) {
		delete process.env.APP_NAME;
		done();
	});

	lab.test('Should call some command, and return the result if APP_NAME is NOT set', {timeout: 30000}, function(done) {
		delete process.env.APP_NAME;
		jobHandler.createJob('Some job', 'test-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();

			//schedule job, so it gets to queue
			scheduler.scheduleJob(job, {}, function(err, messageId) {
				testingTools.code.expect(err).to.be.null();
				var queueCompleted = imq.queue('completed_jobs');
				//execute job
				scheduler.executeJobInIsolation(job, messageId, function(err) {
					testingTools.code.expect(err).to.be.null();

					function checker(messageId) {
						queueCompleted.get({timeout: 30}, function(err, message) {
							testingTools.code.expect(err).to.be.null();
							try {
								var completedJob = JSON.parse(message.body);
								//skip other messages
								testingTools.code.expect(completedJob.messageId).to.equal(messageId);
								//correct message, delete from queue and
								queueCompleted.del(message.id, function(err) {
									testingTools.code.expect(err).to.be.null();
									done();
								});
							} catch(e) {
								checker(messageId);
							}
						});
					}

					checker(messageId);

				});
			});
		});
	});

	lab.test('should validate invalid job', function(done) {
		scheduler.executeJobInIsolation({}, 123, function(err) {
			testingTools.expectError(err);
			done();
		});
	});

	lab.test('Should give error if cannot start a new dyno', {timeout: 5000}, function(done) {
		process.env.APP_NAME = 'some-blablab-app-not-exisiting';
		jobHandler.createJob('Some job', 'test-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();

			scheduler.executeJobInIsolation(job, 4567, function(err) {
				testingTools.expectError(err, 'ws-err-new-dyn');
				done();
			});
		});
	});

	lab.test('Should execute a command in a new dyno and the dyno should be closed after the command was completed', {timeout: 80000}, function(done) {
		//lets use dev env
		process.env.APP_NAME = 'group-app-dev';
		var queue = imq.queue('scheduled_jobs');

		heroku.apps(process.env.APP_NAME).dynos().list(function(err, oldDynos) {
			testingTools.code.expect(err).to.be.null();
			testingTools.code.expect(oldDynos.length).to.be.below(4);
			jobHandler.createJob('Some job', 'iuhb8uhu8gyu8u9huu89h-non-existing-job', {}, 0, function(err, job) {
				testingTools.code.expect(err).to.be.null();

				//schedule job, so it gets to queue
				queue.post({body: JSON.stringify(job)}, function(err, messageId) {
					testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
					//execute job, outcome is unexpected, just check if dyno was started, and delete message
					scheduler.executeJobInIsolation(job, messageId, function(err, res) {
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(res).to.be.object();
						//check if new dyno was started
						function checker() {
							heroku.get('/apps/' + process.env.APP_NAME + '/dynos/' + res.id, function(err) {
								if(err) {
									queue.del(messageId, function(errD) {
										testingTools.code.expect(errD).to.be.null();
										//expect got error because dyno was killed
										testingTools.code.expect(err.body.id).to.equal('not_found');
										done();
									});
								} else {
									console.log('dyno still alive, keep checking...');
									//dyno still alive
									setTimeout(checker, 1000);
								}

							});
						}
						checker();

					});
				});
			});
		});
	});
});

lab.experiment('Worker.Scheduler.checkJobStatus ', function() {

	var messageId;

	lab.afterEach(function(done) {
		var queue = imq.queue('scheduled_jobs');
		if(messageId) {
			queue.del(messageId, function() {
				done();
			});
		} else {
			done();
		}
	});

	lab.test('should give error if message with this messageId does not exist', function(done) {
		scheduler.checkJobStatus('asdsadd', function(err) {
			testingTools.expectError(err, 'ws-jb-get-err');
			done();
		});

	});

	lab.test('should say that work still in progress', {timeout: 5000}, function(done) {
		var queue = imq.queue('scheduled_jobs');
		jobHandler.createJob('Some job', '44-non-existing-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			queue.post({body: JSON.stringify(job)}, function(err, messageId2) {
				testingTools.code.expect(err).to.be.null();
				messageId = messageId2;
				scheduler.checkJobStatus(messageId, function(err, res) {
					testingTools.code.expect(err).to.be.null();
					testingTools.code.expect(res.done).to.equal(false);
					done();
				});
			});
		});
	});

	lab.test('should say that work is completed', {timeout: 5000}, function(done) {
		var queue = imq.queue('scheduled_jobs');
		jobHandler.createJob('Some job', '44-non-existing-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			queue.post({body: JSON.stringify(job)}, function(err, messageId2) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				messageId = messageId2;

				queue.del(messageId, function() {
					scheduler.checkJobStatus(messageId, function(err, res) {
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(res.done).to.equal(true);
						done();
					});
				});
			});
		});
	});

});

lab.experiment('Worker.Scheduler.waitForJobCompletion', function() {

	var messageId, f;

	lab.before(function(done) {
		f = global.setTimeout;
		done();
	});

	lab.afterEach({timeout: 5000}, function(done) {
		global.setTimeout = f;
		var queue = imq.queue('scheduled_jobs');
		if(messageId) {
			queue.del(messageId, function() {
				done();
			});
		} else {
			done();
		}
	});

	lab.test('should give error if job does not exist', function(done) {
		scheduler.waitForJobCompletion('i09i90fs90i', function(err) {
			testingTools.expectError(err, 'ws-jb-get-err');
			done();
		});
	});

	lab.test('should give DONE if job done', function(done) {
		var queue = imq.queue('scheduled_jobs');

		jobHandler.createJob('Some job', '44-non-existing-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			queue.post({body: JSON.stringify(job)}, function(err, messageId2) {
				testingTools.code.expect(err).to.be.null();
				messageId = messageId2;

				queue.del(messageId, function() {
					scheduler.waitForJobCompletion(messageId, function(err) {
						testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
						done();
					});
				});

			});
		});
	});

	lab.test('should give DONE if job is done after some time', {timeout: 10000}, function(done) {
		var queue = imq.queue('scheduled_jobs');
		jobHandler.createJob('Some job', '44-non-existing-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			queue.post({body: JSON.stringify(job)}, function(err, messageId2) {
				testingTools.code.expect(err).to.be.null();
				messageId = messageId2;
				scheduler.waitForJobCompletion(messageId, function(err) {
					testingTools.code.expect(err).to.be.null();
					done();
				});
				//delete after while
				setTimeout(function() {
					queue.del(messageId, function() {
					});
				}, 1000);
			});
		});
	});

	lab.test('should give error if maximum timeout is reached', {timeout: 5000}, function(done) {
		global.setTimeout = function(func) {
			return f.call(this, func, 1);
		};
		var queue = imq.queue('scheduled_jobs');
		jobHandler.createJob('Some job', '44-non-existing-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			queue.post({body: JSON.stringify(job)}, function(err, messageId2) {
				testingTools.code.expect(err).to.be.null();
				messageId = messageId2;
				scheduler.waitForJobCompletion(messageId, function(err) {
					testingTools.expectError(err, 'ws-jb-st-to');
					done();
				}, 2);

			});
		});
	});

});

lab.experiment('Worker.Scheduler.executeJobAndWaitForCompletion', {timeout: 5000}, function() {
	var f;
	lab.before(function(done) {
		f = scheduler.scheduleJob;
		done();
	});

	lab.afterEach(function(done) {
		scheduler.scheduleJob = f;
		done();
	});

	lab.test('should give error if scheduling fails', function(done) {
		scheduler.executeJobAndWaitForCompletion({}, function(err) {
			testingTools.expectError(err, 'wjv-key-miss');
			done();
		});
	});

	lab.test('should give error if executing fails', function(done) {
		scheduler.scheduleJob = function() {
			arguments[arguments.length - 1](null);
		};
		scheduler.executeJobAndWaitForCompletion({}, function(err) {
			testingTools.expectError(err, 'wjv-key-miss');
			done();
		});
	});

	lab.test('should give error if waiting fails', {timeout: 5000}, function(done) {
		jobHandler.createJob('Test job', 'test-job', {}, 0, function(err, job) {
			scheduler.executeJobAndWaitForCompletion(job, function(err) {
				testingTools.expectError(err, 'ws-jb-st-to');
				done();
			}, 1);
			testingTools.code.expect(err).to.be.null();
		});

	});

	lab.test('should work', {timeout: 30000}, function(done) {
		jobHandler.createJob('Test job', 'test-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			scheduler.executeJobAndWaitForCompletion(job, function(err) {
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				done();
			});
		});
	});

});

lab.experiment('Worker.Scheduler.coordinateScheduledJobs', function() {
	var job, f1, f2, f3, f4, f5;
	lab.before(function(done) {
		f1 = scheduler.executeJobInIsolation;
		f2 = iron_mq.Client.prototype.del;
		f3 = console.log;
		f4 = iron_mq.Client.prototype.post;
		f5 = iron_mq.Client.prototype.get;

		//override the executer
		scheduler.executeJobInIsolation = function() {
			arguments[arguments.length - 1](null);
		};

		jobHandler.createJob('Some job', 'some-code', {}, 1, function(e, j) {
			testingTools.code.expect(e).to.be.null();
			job = j;
			done();
		});

	});

	lab.afterEach({timeout: 5000}, function(done) {
		iron_mq.Client.prototype.del = f2;
		iron_mq.Client.prototype.post = f4;
		iron_mq.Client.prototype.get = f5;
		console.log = f3;
		scheduler.stopStaticJobs(function() {
			done();
		});

	});

	lab.after({timeout: 10000}, function(done) {
		scheduler.executeJobInIsolation = f1;

		scheduler.stopCoordinator(function() {
			done();
		});
	});

	lab.test('Should reserve a message from the queue', {timeout: 7000}, function(done) {


		//we test the coordinator through queues
		var queue1 = imq.queue('scheduled_jobs');
		var start = new Date().getTime();
		queue1.post({body: JSON.stringify(job), expires_in: 7}, function(err, messageId) {
			testingTools.code.expect(err).to.be.null();

			function doCheck() {
				queue1.msg_get(messageId, function(err, message) {
					testingTools.code.expect(err).to.be.null();
					if(message.reserved_count > 0) {
						queue1.del(messageId, function(err) {
							testingTools.code.expect(err).to.be.null();
							done();
						});
					} else {
						testingTools.code.expect(new Date().getTime() - start).to.be.below(6000);
						setTimeout(doCheck, 200);
					}

				});
			}

			doCheck();
			//start the coordinator
			scheduler.coordinateScheduledJobs(3);

		});

	});

	lab.test('should not start the coordinator the second time, when called ', function(done) {
		testingTools.code.expect(scheduler.coordinateScheduledJobs()).to.equal(false);
		done();
	});

	lab.test('should put invalid jobs (invalid JSON) to failed_jobs queue ', {timeout: 7000}, function(done) {
		var queue1 = imq.queue('scheduled_jobs');
		var queueFailed = imq.queue('failed_jobs');
		queue1.post({body: 'arse', expires_in: 7}, function(err, messageId) {
			testingTools.code.expect(err).to.be.null();

			function doCheck() {
				queueFailed.get({wait: 30}, function(err, message) {
					testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
					var body = JSON.parse(message.body);
					testingTools.code.expect(body.messageId).to.equal(messageId);
					queueFailed.del(message.id, function(err) {
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(body.errorCode).to.equal('invalid-body');
						done();
					});
				});
			}

			doCheck();

		});
	});

	lab.test('should put invalid jobs (invalid JSON) to failed_jobs queue and report error if deletion from scheduled_jobs queue fails', {timeout: 7000}, function(done) {
		var queue1 = imq.queue('scheduled_jobs');
		var queueFailed = imq.queue('failed_jobs');
		var logs = [];

		var ol = console.log;
		console.log = function() {
			logs.push(arguments[0]);
			ol.apply(this, arguments);
		};

		//overwrite del to produce error
		iron_mq.Client.prototype.del = function() {
			arguments[arguments.length - 1](new Error('aadsad234'));
		};

		queue1.post({body: 'arse', expires_in: 7}, function(err, messageId) {

			testingTools.code.expect(err).to.be.null();

			function doCheck() {
				queueFailed.get({wait: 30}, function(err, message) {
					testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
					var body = JSON.parse(message.body);
					testingTools.code.expect(body.messageId).to.equal(messageId);
					//we need original del function now!
					iron_mq.Client.prototype.del = f2;
					queueFailed.del(message.id, function(err) {
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(body.errorCode).to.equal('invalid-body');
						//check if error was logged
						testingTools.code.expect(logs).to.include('Failed to delete failed job from scheduled_jobs queue (assuming it is gone already)');
						done();
					});
				});
			}

			doCheck();

		});
	});

	lab.test('should put invalid jobs (invalid JSON) to failed_jobs queue and report error if posting to failed_jobs queue fails', {timeout: 10000}, function(done) {
		var queue1 = imq.queue('scheduled_jobs');
		var logs = [];

		var ol = console.log;
		console.log = function() {
			logs.push(arguments[0]);
			ol.apply(this, arguments);
		};

		queue1.post({body: 'arse', expires_in: 5}, function(err, messageId) {
			iron_mq.Client.prototype.post = function() {
				arguments[arguments.length - 1](new Error('asdfgh43'));
			};

			testingTools.code.expect(err).to.be.null();
			var maxInterval = 20;

			function doCheck() {
				queue1.msg_get(messageId, function(err, message) {
					testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
					if(message.status === 'deleted') {
						setTimeout(function() {
							testingTools.code.expect(logs, JSON.stringify(logs)).to.include('Failed to post a message to failed_jobs queue');
							done();
						}, 100);
					} else {
						maxInterval--;
						testingTools.code.expect(maxInterval).to.be.above(0);
						setTimeout(doCheck, 200);
					}
				});
			}

			doCheck();

		});
	});

	lab.test('should put invalid jobs (invalid job) to failed_jobs queue ', {timeout: 40000}, function(done) {
		var queue1 = imq.queue('scheduled_jobs');
		var queueFailed = imq.queue('failed_jobs');
		queue1.clear(function(){
			queue1.post({body: '{"job": 1}', expires_in: 5}, function(err, messageId) {
				testingTools.code.expect(err).to.be.null();

				function doCheck() {
					queueFailed.get({wait: 30}, function(err, message) {
						testingTools.code.expect(err).to.be.null();
						var body = JSON.parse(message.body);
						testingTools.code.expect(body.messageId).to.equal(messageId);
						queueFailed.del(message.id, function(err) {
							testingTools.code.expect(err).to.be.null();
							testingTools.code.expect(body.errorCode).to.equal('invalid-job');
							done();
						});
					});
				}

				doCheck();

			});
		});

	});

	lab.test('should put invalid jobs (execution fails) to failed_jobs queue ', {timeout: 10000}, function(done) {
		var queue1 = imq.queue('scheduled_jobs');
		var queueFailed = imq.queue('failed_jobs');

		scheduler.executeJobInIsolation = function() {
			arguments[arguments.length - 1](new Error('345dffs'));
		};
		queue1.clear(function(){
			queue1.post({body: JSON.stringify(job), expires_in: 5}, function(err, messageId) {
				testingTools.code.expect(err).to.be.null();

				function doCheck() {
					queueFailed.get({wait: 30}, function(err, message) {
						testingTools.code.expect(err).to.be.null();
						var body = JSON.parse(message.body);
						testingTools.code.expect(body.messageId).to.equal(messageId);
						queueFailed.del(message.id, function(err) {
							testingTools.code.expect(err).to.be.null();
							testingTools.code.expect(body.errorCode).to.equal('cannot-start-job');
							done();
						});
					});
				}

				doCheck();

			});
		});

	});

});

lab.experiment('Scheduler.coordinateScheduledJobs with mocked response', {timeout: 10000}, function() {

	var cl;

	lab.before({timeout: 10000}, function(done) {
		cl = logging.warning;
		scheduler.stopCoordinator(function(){
			done();
		});
	});

	lab.after({timeout: 10000}, function(done) {
		logging.warning = cl;
		scheduler.stopCoordinator(function(){
			done();
		});
	});

	testingTools.mockResponsesForExperiment(lab, function(req, res) {
		res.setHeader('content-type', 'application/json');
		res.statusCode = 500;
		res.end('{"error": "Callback is not given, this is random response"}', 'utf8');
	});



	lab.test('should report, if cannot get messaged from queue (mocked response)', {timeout: 7000}, function(done) {

		logging.warning = function() {
			logging.warning = cl;
			testingTools.code.expect(arguments[0]).to.equal('Error listening for scheduled jobs');
			done();
		};

		//should be already started from previous tests
		scheduler.coordinateScheduledJobs(1);
	});
});

lab.experiment('Scheduler.getJobResult', {timeout: 20000}, function() {
	var f1, delMsgId;

	lab.before(function(done){
		f1 = logging.error;
		done();
	});

	lab.afterEach(function(done){
		logging.error = f1;
		done();
	});

	lab.after({timeout: 5000}, function(done){
		var queue = imq.queue('completed_jobs');
		queue.del(delMsgId, function(err){
			testingTools.code.expect(err).to.be.null();
			done();
		});
	});

	lab.test('should give error for unexisting message with max 2 checks', function(done) {
		scheduler.getJobResult('adasdasd', function(err){
			testingTools.expectError(err, 'ws-jb-no-res');
			done();
		}, 2);
	});

	lab.test('should give error for unexisting message with default (10) checks', {timeout: 40000}, function(done) {
		scheduler.getJobResult('adasdasd', function(err){
			testingTools.expectError(err, 'ws-jb-no-res');
			done();
		});
	});

	lab.test('should log error about invalid job response object', function(done) {
		var queue = imq.queue('completed_jobs');

		logging.error = function(msg){
			testingTools.code.expect(msg).to.include('ws-err-jb-res-parse');
		};


		queue.post({body: 'asdoijhaiopf8w9e'}, function(err, messageId) {
			testingTools.code.expect(err).to.be.null();
			delMsgId = messageId;
			scheduler.getJobResult(messageId, function(err){
				testingTools.expectError(err, 'ws-jb-no-res');
				done();
			}, 1);

		});

	});

	lab.test('should take second (minimum) item from MQ and return it', function(done) {
		var queue = imq.queue('completed_jobs');

		queue.post([
			{body: JSON.stringify({messageId: 'asdasd'})},
			{body: JSON.stringify({messageId: 'asdasd222', res: 'smt'})}
		], function(err) {
			testingTools.code.expect(err).to.be.null();

			scheduler.getJobResult('asdasd222', function(err, res){
				testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
				testingTools.code.expect(res.res).to.equal('smt');
				done();
			}, 1);

		});

	});

});

lab.experiment('Scheduler.getJobResult with broken network', {timeout: 30000}, function() {
	testingTools.mockResponsesForExperiment(lab);

	lab.test('should complain about not being able to peek from MQ', function(done) {
		scheduler.getJobResult('adasdasd', function(err){
			testingTools.expectError(err, 'ws-jb-err-peek-res');
			done();
		}, 1);
	});
});
