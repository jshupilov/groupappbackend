'use strict';
var q = require('q');
var htlServices = require('../modules/hotels/services');
var hotelGroupid = 'slh';
require('../modules/integration/TrustContent/services').unpublishBlacklistedHotels(hotelGroupid, function(err, res){
	if(err){
		return console.log(err);
	}
	if(res.length > 0){
		var getHotel = function(p, hotelId){
			htlServices.getHotelRaw(hotelGroupid, hotelId, function(err, hotel){
				if(err){
					return p.reject(err);
				}
				console.log(hotelId, hotel.data.localized[0].name, ' ...DONE');
				return p.resolve();
			});
		};
		var promises = [];
		res.forEach(function(hotelId){
			var p = q.defer();
			promises.push(p.promise);
			getHotel(p, hotelId);
		});
		q.all(promises).done(function(){
			console.log('Unpublishing blacklisted hotels is Successful DONE!!!');
			process.exit(0);

		}, function(err){
			console.log(err);
			process.exit(1);

		});
	} else {
		console.log('List of blacklisted hotels is empty');
		console.log('Unpublishing blacklisted hotels is Successful DONE!!!');
		process.exit(0);
	}
});

