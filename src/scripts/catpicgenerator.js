/*
Script for generating category pictures and gategory map json
from list of images

Sample image names:
 Destination_Americas.png
 Experience_City_Centre_Hotels.png


 */
var fs = require('fs');

//Config
var source = './source/'; //NB: images will be removed from here by script
var dest = './dest/';
var prefix = 'slh'; //Hotel group prefix

var list = fs.readdirSync(source);
console.log(list);

var catType;
var categories = [];
list.splice(0,1);
for(var i=0; i<list.length; i++){
	var catId =prefix;
	var name = '';
	var item=list[i];
	var name1 = item.split('.');
	var name2=name1[0];
	var name3=name2.split('_');
	if(name3[0]=="Destination"){
		catId+='-reg-';
		catType='regions'
	}else if(name3[0]=='Experience'){
		catId+='-ex-';
		catType='experiences'

	}
	name3.splice(0,1);
	for(var k=0; k<name3.length; k++){
		name+=name3[k].toUpperCase()+' ';
		if(name3.length>2){
			catId += name3[k].charAt(0).toLowerCase();
		}else if(name3.length>1){
			catId += name3[k].charAt(0).toLowerCase();
			catId += name3[k].charAt(1).toLowerCase();
		}else{
			catId += name3[k].charAt(0).toLowerCase();
			catId += name3[k].charAt(1).toLowerCase();
			catId += name3[k].charAt(2).toLowerCase();
		}
	}
	var cat =  {
			"categoryId":catId,
			"type":catType,
			"localized": { "en_GB": {"name": toTitleCase(name).trim()
			}}};
	categories.push(cat);
	fs.mkdirSync('./categories/'+catId);
	fs.renameSync('./slh/'+item, dest+catId+'/button.png');
}
console.log(JSON.stringify(categories));

function toTitleCase(str)
{
	return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}