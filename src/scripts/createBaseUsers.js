'use strict';
var path = require('path');
var databaseUtils = require(path.join(__dirname, '../modules/utils/databaseUtils'));
var errors = require(path.join(__dirname, '../modules/utils/errors'));
var profileServices = require(path.join(__dirname, '../modules/profiles/services'));
var hgServices = require(path.join(__dirname, '../modules/hotel_groups/services'));
var q = require('q');

var sp = require('stormpath');
var apiKey = new sp.ApiKey(process.env.STORMPATH_API_KEY_ID, process.env.STORMPATH_API_KEY_SECRET);
var client = new sp.Client({apiKey: apiKey});

var test = true;
var skip = 0;
var onlyUuid = false;
if(process.argv[2]) {
	test = process.argv[2] ? false : true;
}

if(process.argv[3]) {
	skip = parseInt(process.argv[3]);
}

if(process.argv[4]) {
	onlyUuid = process.argv[4];
}

if(test){
	console.log('TEST MODE');
}

function getBaseProfileEmail(uuid){
	return uuid + '@cardola.net';

}

function getAccount(directoryUrl, email, callback){

	client.getDirectory(directoryUrl, {expand: 'accounts'}, function(err, directory) {

		if (err) {
			return callback(errors.newError('ps-dir-get', {originalError: err}));
		}
		directory.getAccounts({email: email, expand: 'customData'}, function (err, accounts) {
			if (err) {
				callback(errors.newError('ps-acc-get', {stormPathError: err}), null);
			} else {
				if (accounts.items.length === 0) {
					callback(errors.newError('ps-acc-get', {reason: 'no accounts from stormpath'}));
				} else {
					callback(null, accounts.items[0]);
				}
			}
		});
	});
}

if(!process.env.STORMPATH_BASE_DIR_URL){
	console.log('NO env STORMPATH_BASE_DIR_URL');
	process.exit(1);
}

var delQ = 'DELETE FROM profile_session';
var delM = 'All sessions deleted';
if(skip){
	delQ = 'SELECT 1 as num';
	delM = 'Sessions deletion was skipped';
}

databaseUtils.getConnection(function(err, client, done) {
	if(err) {
		callback(err);
	} else {

		client.query(delQ, function(err){
			if(err){
				console.log('Cannot delete sessions!');
				process.exit(1);
			}
			console.log(delM);

			client.query(
				'SELECT * FROM app_instance',
				function (err, res) {
					done();
					var promises = [];
					function doIt(row, p){


						console.log(row.data.uuid, row.data.hotelGroupId, '...');
						//get old account
						getAccount(process.env.STORMPATH_BASE_DIR_URL, getBaseProfileEmail(row.data.uuid), function(err, oldAccount){
							if(err){
								console.log('No old account, take next');
								return p.reject(err);
							}

							if(test){
								console.log('TEST MODE no account creation or save', row.data.uuid);
								p.resolve();
								return;
							}

							//create new account
							profileServices.createBaseAccount({uuid: row.data.uuid, hotelGroupId: row.data.hotelGroupId}, function(err, account){
								if(err){
									if(err.data.code === 'ps-ex-email'){
										console.log('Already has a new account, skip...');
										return p.resolve();
									}
									return p.reject(err);
								}
								var meta = ['href'];
								for(var ci in oldAccount.customData){
									if( meta.indexOf(ci) > -1 ){
										continue;
									}
									account.customData[ci] = oldAccount.customData[ci];
								}
								//save new account settings
								account.customData.save(function(err){
									if(err){
										console.log('Cannot update custom data', err);
										return p.reject(err);
									}
									p.resolve();
									console.log('Done!');

								});

							});
						});
					}

					if(err){
						return console.log('Error', err);
					} else {

						console.log('Creating BASE accounts for ' + res.rowCount + ' app_instances');
						if(skip){
							console.log('Skipping', skip);
						}
						res.rows.forEach(function(row, rowIndex){
							if(rowIndex < skip){
								return;
							}
							if(onlyUuid && onlyUuid !== row.data.uuid){
								return;
							}
							var p = q.defer();
							promises.push(p.promise);

							if(promises.length > 1){
								promises[promises.length-2].finally(function(){
									console.log('Starting nr ', rowIndex);
									doIt(row, p);
								});
							}else{
								console.log('Starting nr ', rowIndex);
								doIt(row, p);
							}

						});

						q.allSettled(promises).done(function(){
							done();
							console.log('DONE');
							process.exit(0);

						}, function(e){
							done();
							console.log('Error', e);
							process.exit(1);
						});
					}

				}
			);
		});


	}
});