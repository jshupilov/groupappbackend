'use strict';
var dir = __dirname;
var fs = require('fs');
var q = require('q');
var glob = require('glob');
var path = require('path');
var profile = require('../modules/profiles/services');
var mediaServices = require(path.join(__dirname, '../modules/media/services'));
var hgServices = require(path.join(__dirname, '../modules/hotel_groups/services'));
var hotelsServices = require(path.join(__dirname, '../modules/hotels/services'));
var searchServices = require(path.join(__dirname, '../modules/search/services'));
searchServices.init();
var listenerFiles = glob.sync(path.join(__dirname, '/modules/*/listener.js'));
listenerFiles.forEach(function(listenerFile) {
	require(listenerFile);
});

var hgCount = 10;
var hotelCount = 250;

if(process.argv[2]) {
	hgCount = process.argv[2];
} else {
	console.log('First arg must be number of hotel groups');
	process.exit(1);
}
if(process.argv[3]) {
	hotelCount = process.argv[3];
} else {
	console.log('Second arg must be number of hotels in every group');
	process.exit(1);
}

var testMode = true;

if(process.argv[4]) {
	testMode = false;
} else {
	console.log('TEST MODE!');
}

console.log('Generating ' + hgCount + ' hotel groups with ' + hotelCount + ' hotels each');

function loopHotelCount(itemValidatorCallback, finalCallback){
	var promises = [];
	function doStuff(p, i){
		itemValidatorCallback(i, function(err){
			if(err){
				return p.reject(err);
			}
			p.resolve();
		});
	}
	for(var hNr = 1; hNr <= hotelCount; hNr++) {
		var p = q.defer();
		promises.push(p.promise);
		doStuff(p, hNr);
	}
	q.all(promises).done(function(){
		finalCallback(null);
	}, function(err){
		finalCallback(err);
	});
}
function cb1(hotel, hotelGroup, p) {
	hotelsServices.createHotel(hotel, hotelGroup, function(err) {
		if(err) {
			p.reject(err);
			return 	console.log('Hotel save failed: ', JSON.stringify(err));

		}
		console.log('Hotel saved ' + hotel.hotelId);
		p.resolve();
	});
}

function cb2(hotelGroup, p1, hgNr, baseHotels){
	var hotetGroupId = hotelGroup.hotelGroupId;
	var newHotelGroupId = 'clone-' + hotelGroup.hotelGroupId + '-' + hgNr;

	hotelGroup = JSON.parse(JSON.stringify(hotelGroup).replace(new RegExp(hotetGroupId, 'gi'), newHotelGroupId));

	hgServices.createTestHotelGroup(hotelGroup, function(err) {
		var hgPromises = [];
		function dos(lastIndex, p, hotelGroup, hotel) {
			if (lastIndex >= 0) {

				hgPromises[lastIndex].then(function () {
					cb1(hotel, hotelGroup, p);
				}, function () {
					console.log('Failed create hotel');
				});
			} else {
				cb1(hotel, hotelGroup, p);
			}
		}
		if(err) {
			console.log('Hotel group save failed: ', JSON.stringify(err));
			return p1.reject();
		}
		loopHotelCount(
			function(hNr, cb){
				var hotel = JSON.parse(JSON.stringify(baseHotels[Math.floor(Math.random() * baseHotels.length)].data));
				var hotelId = hotel.hotelId;
				var newHotelId = 'clone-' + hotel.hotelId + '-' + hgNr + '_' + hNr;
				var searchStr = process.env.CLOUDINARY_BASE_FOLDER + '/group-app/' + hotetGroupId + '/hotels/' + hotelId;
				var replaceStr = process.env.CLOUDINARY_BASE_FOLDER + '/group-app/' + newHotelGroupId + '/hotels/' + newHotelId;

				hotel.hotelId = newHotelId;
				hotel.hotelGroup.hotelGroupId = newHotelGroupId;

				hotel = JSON.parse(JSON.stringify(hotel).replace(new RegExp(searchStr, 'gi'), replaceStr));
				hotel.localized.forEach(function(loc) {
					loc.name += ' ' + hNr;
				});
				var p = q.defer();
				var lastIndex = hgPromises.length-1;
				hgPromises.push(p.promise);

				dos(lastIndex, p, hotelGroup, hotel);
				cb(null);
			},
			function(){
				q.all(hgPromises).then(function() {
					p1.resolve();
				}, function() {
					p1.reject();
				});
			}
		);
	});
}

hgServices.getAllRaw(function(err, hotelGroups) {
	var baseHotelGroup = hotelGroups[0].data;
	if(testMode) {
		console.log('test mode, not saving hotel group ' + baseHotelGroup.hotelGroupId);
	} else {
		hotelsServices.getHotelsRaw(baseHotelGroup.hotelGroupId, function (err, baseHotels) {
			var promises = [];
			function dos(lastIndex, p1, hotelGroup, hgNr, baseHotels) {
				if (lastIndex >= 0) {

					promises[lastIndex].then(function () {
						cb2(hotelGroup, p1, hgNr, baseHotels);
					}, function () {
						console.log('Failed create hotel group');
						process.exit(1);
					});
				} else {
					cb2(hotelGroup, p1, hgNr, baseHotels);
				}
			}
			if(err) {
				console.log('Hotel group get failed: ', JSON.stringify(err));
			} else {
				profile.init(function(err) {
					if(err) {
						logging.emerg('Profile init failed', err);
					} else {
						for (var hgNr = 1; hgNr <= hgCount; hgNr++) {
							console.log('Creating hotel group nr ' + hgNr);
							var p1 = q.defer();
							var hotelGroup = JSON.parse(JSON.stringify(baseHotelGroup));

							var lastIndex = promises.length - 1;
							promises.push(p1.promise);
							dos(lastIndex, p1, hotelGroup, hgNr, baseHotels);

						}

						q.all(promises).then(function () {
							console.log('Done!');
							process.exit(0);
						}, function () {
							console.log('Failed');
							process.exit(1);
						});
					}
				});

			}
		});
	}
});
