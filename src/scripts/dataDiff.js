'use strict';
var versioning = require('../modules/versioning/services');
var hotelServices = require('../modules/hotels/services');
var hgServices = require('../modules/hotel_groups/services');
var jsondiffpatch = require('jsondiffpatch');

var hotelGroupId = null;
var hotelId = null;
var offset = 1;

var usage = 'Usage: [hotelGroupId] [hotelId] [versionDelta=1]';

if(process.argv[2]){
	hotelGroupId = process.argv[2];
}else{
	console.log('hotel group Id missing', usage);
	process.exit(1);
}

if(process.argv[3]){
	if(parseInt(process.argv[3])){
		offset = parseInt(process.argv[3]);
	}else{
		hotelId = process.argv[3];
	}
}

if(parseInt(process.argv[4])){
	offset = parseInt(process.argv[4]);
}



if(hotelId){
	console.log('Getting diff for hotel ', hotelId, 'versionDelta', offset);
	hotelServices.getHotelRaw(hotelGroupId, hotelId, function(err, res){
		if(err){
			console.log('Error getting hotel', err);
			process.exit(1);
		}
		versioning.getDiff(res.data, res.data.meta.guid, 'hotel_v', offset, function(err, diff){
			if(err){
				console.log('Error getting diff', err);
				process.exit(1);
			}
			jsondiffpatch.console.log(diff);
			process.exit(0);
		});

	})
}else{
	console.log('Getting diff for hotel group ', hotelGroupId, 'versionDelta', offset);
	hgServices.getHotelGroup(hotelGroupId, function(err, res){
		if(err){
			console.log('Error getting hotel group', err);
			process.exit(1);
		}
		versioning.getDiff(res.data, res.data.meta.guid, 'hotel_group_v', offset, function(err, diff){
			if(err){
				console.log('Error getting diff', err);
				process.exit(1);
			}
			jsondiffpatch.console.log(diff);
			process.exit(0);
		});

	})
}
