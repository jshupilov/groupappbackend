'use strict';
var dir = __dirname;
var glob = require('glob');
var path = require('path');
var q = require('q');
var hgServices = require(path.join(__dirname, '../modules/hotel_groups/services'));
var mediaServices = require(path.join(__dirname, '../modules/media/services'));
var databaseUtils = require('../modules/utils/databaseUtils');
var version = require('../modules/versioning/services');

var listenerFiles = glob.sync(path.join(__dirname, '/modules/*/listener.js'));
listenerFiles.forEach(function(listenerFile) {
	require(listenerFile);
});

console.log('Deleting data...');
var now = new Date().getTime();

var before = new Date(now - 3600).toISOString();



databaseUtils.getConnection(function(err, client, doneDb) {

	var mProm = [];

	var setHotelsDead = function(m) {
		var addHotelVersion = function(p, item) {
			client.query('INSERT INTO hotel_v (data) VALUES($1)', [item.data], function(err) {
				if(err) {
					p.reject(err);
				}
				p.resolve();
			});
		};
		var promises1 = [];

		client.query('SELECT * FROM hotel', function(err, res) {
			for(var i = 0; i < res.rowCount; i++) {
				var p = q.defer();
				var item = res.rows[i];
				if(typeof item.data.meta === 'undefined') {
					var guid = item.data.hotelId + '_' + item.data.hotelGroup.hotelGroupId;
					item.data.meta = {
						'createdAt': before,
						'scheduledAt': before,
						'appliedAt': null,
						'status': 'draft',
						'guid': guid
					};
				} else {
					item.data.meta.status = 'draft';
					item.data.meta.createdAt = before;
					item.data.meta.scheduledAt = before;
					item.data.meta.appliedAt = null;
				}

				promises1.push(p.promise);
				addHotelVersion(p, item);
			}
		});

		q.all(promises1).done(function() {
			version.handleGoLive('hotel_v', 'hotel', function(err) {
				if(err) {
					return m.reject(err);
				}
				m.resolve();
			})
		}, function(err) {
			console.log('ERROR', err);
			process.exit(1);
		});
	};


	var setHotelGroupsDead = function(m) {
		var addGroupVersion = function(p, item) {
			client.query('INSERT INTO hotel_group_v (data) VALUES($1)', [item.data], function(err) {
				if(err) {
					return p.reject(err);
				}
				//also delete images from cloudinary
				//console.log(item.data);
				mediaServices.deleteHotelGroupImages(item.data.hotelGroupId, function(err) {
					if(err) {
						return p.reject(err);
					}
					p.resolve();
				});
			});

		};
		var promises2 = [];
		client.query('SELECT * FROM hotel_group', function(err, res) {
			for(var i = 0; i < res.rowCount; i++) {
				var p2 = q.defer();
				var item = res.rows[i];


				if(typeof item.data.meta === 'undefined') {
					var guid = item.data.hotelGroupId;
					item.data.meta = {
						'createdAt': before,
						'scheduledAt': before,
						'appliedAt': null,
						'status': 'draft',
						'guid': guid
					};
				} else {
					item.data.meta.status = 'draft';
					item.data.meta.createdAt = before;
					item.data.meta.scheduledAt = before;
					item.data.meta.appliedAt = null;
				}
				promises2.push(p2.promise);
				addGroupVersion(p2, item);
			}
		});

		q.all(promises2).done(function() {
			version.handleGoLive('hotel_group_v', 'hotel_group', function(err) {
				if(err) {
					return m.reject();
				}
				m.resolve();
			})
		}, function(err) {
			console.log('ERROR', err);
			process.exit(1);
		});
	};

	for(var k = 0; k < 2; k++) {
		var m = q.defer();
		mProm.push(m.promise);
		switch(k) {
			case 0:
				setHotelsDead(m);
				break;
			case 1:
				setHotelGroupsDead(m);
				break;

		}

	}
	q.all(mProm).done(function() {

		var querys = [
			//In case you need to delte from somewhere else, just add query here.
			'DELETE FROM hotel',
			'DELETE FROM hotel_v',
			'DELETE FROM hotel_group',
			'DELETE FROM hotel_group_v',
			'DELETE FROM reservation',
			'DELETE FROM ss_data'
		];
		var promises = [];
		var doInDb = function(p, q) {
			//console.log(q);
			client.query(q, function(err) {
				if(err) {
					console.log('Error executing query: ', q);
					return p.reject(err);
				}
				p.resolve();
			});
		};

		for(var i = 0; i < querys.length; i++) {
			var p = q.defer();
			promises.push(p.promise);
			doInDb(p, querys[i]);
		}

		q.all(promises).done(function() {
			console.log('All deleted...');
			doneDb();
			process.exit();
		}, function(err) {
			console.log('ERROR', err);
			process.exit(1);
		});

	}, function(err) {
		console.log('ERROR', err);
		process.exit(1);
	});

});

