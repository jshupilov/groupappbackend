#!/bin/sh
cd ${0%/*}

envFileName=.env
envFile=../../$envFileName
envFileBackup=../../$envFileName
envFileBackup+=_old
if [ ! -f $envFile ]; then
    echo "$envFileName file not found!"
    exit 1;
fi

if [ ! -w $envFile ]; then
	echo "$envFileName is not writable!"
    exit 1;
fi

echo "Reading ENV..."
export $(cat $envFile)
if [ -z "$TEST_HEROKU_APP_NAME" ]; then
	echo "TEST_HEROKU_APP_NAME is not set in $envFileName"
	exit 1
fi

echo "Delete heroku instance ($TEST_HEROKU_APP_NAME)?...(y/n)"
read confirm
if [ $confirm != "y" ]; then
	echo "Quitting"
	exit 0
fi
heroku apps:destroy --app $TEST_HEROKU_APP_NAME --confirm $TEST_HEROKU_APP_NAME

grep -v "TEST_HEROKU_APP_NAME=" $envFile > tmp && mv tmp $envFile

echo "Recovering $envFileName"
old=$(cat $envFile)
mv -f $envFileBackup $envFile
echo "$old" > $envFileBackup