/**
 * Created by jevgenishupilov on 19/10/15.
 */
var leasewebSDK = require('../modules/utils/leasewebSDK');
var jobHandler = require('../modules/worker/jobHandler');
var scheduler = require('../modules/worker/scheduler');

var start = new Date();
console.log("Starting Leaseweb Access logs downloader...");
leasewebSDK.downloadAccessLogs(function(err, result){
	//if error occurred, retry
	console.log("Access logs download complete", err, (new Date().getTime() - start.getTime()), 'ms');
	console.log("Total files", result.length);

	process.exit(0);

});