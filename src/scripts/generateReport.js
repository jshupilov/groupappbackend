/**
 * Created by jevgenishupilov on 08/12/15.
 */
var jobHandler = require('../modules/worker/jobHandler');
var scheduler = require('../modules/worker/scheduler');

var startDate = new Date(new Date().setHours(0));
var endDate = new Date(new Date().setHours(23));
var jobCode = 'generateReport';
var recipients = [];

var period = null;
var hotelGroupid = null;
var reportType = null;
var emails = null;

if(process.argv[2]){
	hotelGroupid = process.argv[2];
} else {
	console.log('First arg must be id of hotel group');
	process.exit(1);
}

if(process.argv[3]){
	reportType = process.argv[3];
	reportType = reportType[0].toUpperCase() + reportType.slice(1);
	jobCode += reportType;
} else {
	console.log('Second arg must be report type (Activations|Impressions)');
	process.exit(1);
}

if(process.argv[4]){
	period = parseInt(process.argv[4]);
	startDate.setDate(startDate.getDate() - period);
} else {
	console.log('Fourth arg must be count of report days - period ');
	process.exit(1);
}

if(process.argv[5]){
	emails = process.argv[5];
	recipients = emails.split(',').map(function(email){
		return {email: email};
	});
} else {
	console.log('Last arg must be comma separated recipients emails');
	process.exit(1);
}

console.log("Starting Report generation...");

jobHandler.createJob(
	'Reporting Job',
	jobCode,
	{
		hotelGroupId: hotelGroupid,
		days: period,
		recipients: recipients
	},
	0,
	function(err, job) {
		console.log("Executing the reporting process", err);

		scheduler.executeJobAndWaitForCompletion(job, function(err, res){
			console.log('Report ' + reportType + ' send', err, res);
			process.exit(0);
		}, 6000);
	}
);