/**
 * Created by jevgenishupilov on 19/10/15.
 */
var jobHandler = require('../modules/worker/jobHandler');
var scheduler = require('../modules/worker/scheduler');

jobHandler.createJob('Hotel and group data go live from script', 'htlndgrouplive', {}, 0, function(err, job){
	console.log("Executing the go live process", err);
	scheduler.executeJobAndWaitForCompletion(job, function(err, res){
		console.log("go live done", err, res);
		process.exit(0);
	}, 6000);

});
