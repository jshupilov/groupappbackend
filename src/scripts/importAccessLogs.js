/**
 * Created by jevgenishupilov on 19/10/15.
 */
var leasewebSDK = require('../modules/utils/leasewebSDK');
var jobHandler = require('../modules/worker/jobHandler');
var scheduler = require('../modules/worker/scheduler');

var start = new Date();
console.log("Starting Leaseweb Access logs importer...");
leasewebSDK.importNewAccessLogFiles(function(err, result){
	//if error occurred, retry
	console.log("Access logs import complete", err, (new Date().getTime() - start.getTime()), 'ms');
	console.log("Result was", result);

	process.exit(0);

});