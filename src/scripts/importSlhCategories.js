'use strict';
var importer = require('../modules/integration/importer');
console.log('Starting to import. This will take a while');
setInterval(function(){
	process.stdout.write(".");
}, 1000);

importer.startImport('slh', function(err, res){
	if(err){
		console.log('Error happened during the import', err);
	}else{
		console.log('Done', res);
	}
	process.exit();
});