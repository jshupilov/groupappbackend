#!/bin/sh
cd ${0%/*}

envFileName=.env
envFile=../../$envFileName
envFileBackup=../../$envFileName
envFileBackup+=_old

if [ ! -f $envFile ]; then
    echo "$envFileName File not found!"
    exit 1;
fi

if [ ! -w $envFile ]; then
	echo "$envFileName is not writable!"
    exit 1;
fi


#if branch=$(git symbolic-ref --short -q HEAD)
#then
#  echo on branch $branch
#  echo "Have you commited your changes, which you would like to be uploaded to Heroku (y/n)?"
#  read confirm
#  if [ $confirm == "n" ]; then
#    echo "Will stop then..."
#    exit 0
#  fi
#else
#  echo not on any branch
#  exit;
#fi


echo "Creating heroku instance..."
create=$(heroku create --region us)

app=$(echo $create | egrep -o "Creating\s[^\.]*\.\.\." | cut -d " " -f 2 | egrep -o "[^\.]*" )
echo "created app '$app'"

grep -v "TEST_HEROKU_APP_NAME=" $envFile > tmp && mv tmp $envFile
echo "TEST_HEROKU_APP_NAME=$app" >> $envFile

echo "Adding addons:"
heroku addons:add heroku-postgresql --version 9.4 --app $app
heroku addons:add algoliasearch --app $app
heroku addons:add cloudinary --app $app
heroku addons:add stormpath --app $app

config=$(heroku config --shell --app $app | egrep -o "^[A-Z_]*=.*$")
env=$(cat $envFile)
echo "Modifying $envFileName"
while read -r line; do
	key=$(echo $line | egrep -o "^[A-Z_]*")
	env=$(echo "$env" | grep -v "$key=")
	env+="\n$line"
done <<< "$config"

echo "Backuping $envFileName and overwriting $envFileName with variables from heroku"

mv -f $envFile $envFileBackup
echo "$env" > $envFile


#echo "Pushing to heroku"
#git push git@heroku.com:$app.git $branch:master

#echo "Creating a dyno"
#heroku ps:scale web=1 --app $app

echo "Loading new enviroment variables.."
export $(cat $envFile)

#echo "Setting some .env variables in Heroku"
#heroku config:set PUBLIC_PROTOCOL=http PUBLIC_HOST=$app.herokuapp.com PUBLIC_PORT=80 CMD=node --app $app

echo "Initializing the DB..."
node initDatabase.js

echo "Populating data..."
node populateData.js
