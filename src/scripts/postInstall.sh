#!/usr/bin/env bash

echo "Running post-install script"

cd ${0%/*}
if [ -f ../../.env ]
then
   export $(cat ../../.env | xargs)
fi

if [ $NODE_ENV == "production" ]
then
	echo "We are in PRODUCTION mode!"
	export PGSSLMODE=require
else
	echo "We are in dev"
fi

echo "Running DB migrations"
if npm run pg-migrate up && npm run pg-migrate-stats up
then
	echo "DB Migrations done"
else
	echo "DB Migration failed, failing..."
	echo "Done with post-install script"
	exit 1
fi

echo "Done with post-install script"