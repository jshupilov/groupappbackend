#!/bin/sh
cd ${0%/*}
if [ ! -f ../../.env ]; then
    echo ".env File not found!"
fi
export $(cat ../../.env | xargs) && node deleteData.js
export $(cat ../../.env | xargs) && sh reset_database.sh $DATABASE_URL
export $(cat ../../.env | xargs) && node populateData.js