#!/bin/sh

if [ $# == 0 ]; then
	echo "No database name given!"
	exit 1
else
	database=$1
fi

if [[ $database == *"postgres://"* ]]
then
	echo "Stripping database name from $database"
	re='\/(.*)$'
	while [[ $database =~ $re ]]; do
	  database=${BASH_REMATCH[1]}
	done
	echo "Database name is $database"
fi

#drop database
echo "Dropping local PostgreSQL database ($database)"
dropdb --if-exists $database
drop=$(cat ud_drop.log)
echo $drop


if echo $drop | grep "database dropping failed" -c; then
    echo "Error occurred while dropping database, STOPPING.";
    exit 1
fi

#create database
echo "Creating local PostgreSQL database ($database)"
createdb $database

#create tables
echo "Creating tables:"
node initDatabase.js
echo "DONE!"