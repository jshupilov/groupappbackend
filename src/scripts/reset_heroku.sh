#!/bin/sh
cd ${0%/*}

echo "Enter heroku application name"
read app

echo "Loading ENV from heroku..."
config=$(heroku config --shell --app $app | egrep -o "^[A-Z_]*=.*$")
export $( echo "$config" )

echo "Deleting data..."
node deleteData.js

echo "Resetting database"
heroku pg:reset DATABASE_URL --confirm $app --app $app

echo "Initializing the DB..."
node initDatabase.js

echo "Populating data..."
node populateData.js

echo "Done!"
