/**
 * Created by jevgenishupilov on 19/10/15.
 */
var trustContentServices = require('../modules/integration/TrustContent/services');
var jobHandler = require('../modules/worker/jobHandler');
var scheduler = require('../modules/worker/scheduler');

var limit = null;

if(process.argv[2]){
	limit = process.argv[2];
}

console.log("Starting Trust Content importer...", process.argv);
trustContentServices.startCoordinatedImportFromFtp('slh', function(err, res){
	//if error occurred, retry
	console.log("Coordinated importer done");

	jobHandler.createJob('Hotel and group data go live from script', 'htlndgrouplive', {}, 0, function(err, job){
		console.log("Executing the go live process", err);
		scheduler.executeJobAndWaitForCompletion(job, function(err, res){
			console.log("go live done", err, res);
			process.exit(0);
		}, 6000);

	});

}, limit);