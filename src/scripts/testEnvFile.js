/**
 * Created by jevgenishupilov on 04/02/15.
 */
'use strict';
var testingTools = require('./../test/testingTools');
var envTest = require('./../test/env');
var fs = require('fs');
var path = require('path');
exports.labLib = envTest.labLib;
var lab = exports.lab = envTest.lab;


/**
 * Required environment variables, inherit from other test
 * @type {string[]}
 */
var requiredEnv = envTest.requiredEnv;

lab.experiment('Check for remote environment variables', function() {
	var envFile;
	var filePath;
	var fileName;

	lab.before(function(done) {
		console.log('Check env file HEROKU_APP_NAME');
		testingTools.code.expect(process.env.HEROKU_APP_NAME).to.be.string();
		testingTools.code.expect(process.env.HEROKU_APP_NAME).not.to.be.empty();
		fileName = process.env.HEROKU_APP_NAME + '.env';
		console.log('Checking env file', fileName);
		done();
	});

	lab.test('file path should be made', function(done) {
		filePath = fileName;
		done();
	});

	lab.test('the given file should exist', function(done) {
		fs.readFile( filePath, 'utf8', function(err, fileContents){
			testingTools.code.expect(err, JSON.stringify(err)).to.be.null();
			testingTools.code.expect(fileContents).to.be.string();
			testingTools.code.expect(fileContents).not.to.be.empty();
			envFile = fileContents;
			done();
		});
	});
	
	lab.test('all required env variables should exist', function(done) {
		for(var i in requiredEnv){
			var pattern = requiredEnv[i] + '=[^\\s]+';
			var regExp = new RegExp(pattern, 'g');
			testingTools.code.expect(envFile, 'Env variable is missing in ' + fileName).to.include(requiredEnv[i] + '=');
			testingTools.code.expect(regExp.test(envFile), 'Env variable not set ' + pattern + ' in ' + fileName).to.equal(true);
		}
		done();
	});
});

