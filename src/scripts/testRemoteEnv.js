/**
 * Created by jevgenishupilov on 04/02/15.
 */
'use strict';
var testingTools = require('./../test/testingTools');
var envTest = require('./../test/env');
var heroku = new (require('heroku-client'))({token: process.env.HEROKU_API_TOKEN});

exports.labLib = envTest.labLib;
var lab = exports.lab = envTest.lab;

/**
 * Required environment variables, inherit from other test
 * @type {string[]}
 */
var requiredEnv = envTest.requiredEnv;

lab.experiment('Check for remote environment variables', function() {

	var appName;
	lab.before(function(done) {
		testingTools.code.expect(process.env.HEROKU_APP_NAME, 'Target app name not provided in env as HEROKU_APP_NAME').to.be.string();
		testingTools.code.expect(process.env.HEROKU_APP_NAME, 'App name must contain "group-app"' ).to.include('group-app');
		appName = process.env.HEROKU_APP_NAME;
		done();
	});
	
	lab.test('all required env variables should exist', function(done) {
		heroku.get('/apps/' + appName + '/config-vars', function (err, vars) {
			testingTools.code.expect(err, 'Invalid app name ' + appName + JSON.stringify(err)).to.be.null();
			//a loop to see the real missing value
			for(var i in requiredEnv){
				testingTools.code.expect(vars, 'Environment variable is missing in ' + appName).to.include(requiredEnv[i]);
			}
			done();
		});
		

	});
});

