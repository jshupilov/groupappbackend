/**
 * Created by jevgenishupilov on 19/10/15.
 */
var trustContentServices = require('../modules/integration/TrustContent/services');
var jobHandler = require('../modules/worker/jobHandler');
var scheduler = require('../modules/worker/scheduler');
var start = new Date();
console.log("Starting Blacklist importer...");
trustContentServices.updateHotelBlackList('slh', function(err, blacklist){
	//if error occurred, retry
	console.log("Blacklist updater done", err, (new Date().getTime() - start.getTime()), 'ms');
	console.log("New blacklist is", blacklist);

	jobHandler.createJob('Hotel and group data go live from script', 'htlndgrouplive', {}, 0, function(err, job){
		console.log("Executing the go live process", err);
		scheduler.executeJobAndWaitForCompletion(job, function(err, res){
			console.log("go live done", err, res);
			process.exit(0);
		}, 6000);

	});

});