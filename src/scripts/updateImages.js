'use strict';
var dir = __dirname;
var fs = require('fs');
var q = require('q');
var glob = require('glob');
var path = require('path');
var cloudinary = require('cloudinary');

var profile = require('../modules/profiles/services');
var reservations = require('../modules/reservations/services');
var mediaServices = require(path.join(__dirname, '../modules/media/services'));
var hgServices = require(path.join(__dirname, '../modules/hotel_groups/services'));
var hotelsServices = require(path.join(__dirname, '../modules/hotels/services'));
var searchServices = require(path.join(__dirname, '../modules/search/services'));
var databaseUtils = require(path.join(__dirname, '../modules/utils/databaseUtils'));
var jobhandler = require(path.join(__dirname, '../modules/worker/jobHandler'));
var scheduler = require(path.join(__dirname, '../modules/worker/scheduler'));
searchServices.init();
var listenerFiles = glob.sync(path.join(__dirname, '/modules/*/listener.js'));
listenerFiles.forEach(function(listenerFile) {
	require(listenerFile);
});

var hg = null;

if(process.argv[4]) {
	hg = process.argv[4];
}

if(process.argv.length === 3){
	hg = process.argv[2];
}

console.log('Hotel group specified?', hg ? hg : 'NO');

function populate(){

	console.log('Updating images...');
	/**
	 * For every hotel group
	 */
	var hgPromises = [];
	hgServices.getAllRaw(function(err, hotelGroupRows){

		hotelGroupRows.forEach(function(hotelGroupRow) {
			var data = hotelGroupRow.data;

			var hgId = data.hotelGroupId;

			if(hg && hg !== hgId){
				console.log('Skipping', hgId);
				return;
			}
			var hgPromise = q.defer();
			hgPromises.push(hgPromise.promise);
			var promises = [];


			console.log('Updating hotel group images', hgId);

			data.meta={
				'scheduledAt': "2015-07-10T11:40:24.404Z",
				'status': 'published'
			};

			var groupFile = dir + '/../../data/hotel_groups/' + hgId;

			/**
			 * Upload logo file
			 */
			var logoFiles = glob.sync(groupFile + '/logo.*');
			if(err) {
				console.log('Cannot find logo file');
			} else if(logoFiles.length > 0) {
				var upLogo = q.defer();
				promises.push(upLogo.promise);
				mediaServices.uploadHotelGroupImage(hgId, logoFiles[0], {public_id: 'logo'}, function(err, upRes) {
					if(err) {
						console.log('Cannot upload logo file', err);
						upLogo.reject();
						return;
					}

					for( var lang in data.info.localized){
						var localized = data.info.localized[lang];
						localized.logo = {};


						mediaServices.setImageObjectSource(localized.logo, upRes);
					}
					delete data.info.logo;


					upLogo.resolve(true);
				});
			}

			var bgFiles = glob.sync(groupFile + '/bg.*');
			if(err) {
				console.log('Cannot find bg file');
			} else if(bgFiles.length > 0) {
				var upBg = q.defer();
				promises.push(upBg.promise);
				mediaServices.uploadHotelGroupImage(hgId, bgFiles[0], {public_id: 'bg'}, function(err, upRes) {
					if(err) {
						console.log('Cannot upload logo file');
						upBg.reject();
						return;
					}
					for( var lang in data.info.localized){
						var localized = data.info.localized[lang];
						localized.backgroundImage = {};

						mediaServices.setImageObjectSource(localized.backgroundImage, upRes);
					}
					delete data.info.backgroundImage;

					upBg.resolve(true);
				});
			}

			var extraAssets = glob.sync(groupFile + '/extra_assets/*.*');
			data.mediaConfiguration.extraAssets = [];
			if(extraAssets.length > 0) {
				extraAssets.forEach(function(file){
					var upBgo = q.defer();
					var fn = file.split('/').pop().split('.').shift();
					promises.push(upBgo.promise);
					var public_id = 'extra_assets/' + fn;
					mediaServices.uploadHotelGroupImage(hgId, file, {public_id: public_id}, function(err, upRes) {
						if(err) {
							console.log('Cannot upload file', fn);
							upBgo.reject();
							return;
						}
						var img = {
							id: public_id
						};
						mediaServices.setImageObjectSource(img, upRes);
						data.mediaConfiguration.extraAssets.push(img);
						upBgo.resolve(true);
					});
				});

			}

			/**
			 * Upload placeholder file
			 */
			var phFiles = glob.sync(groupFile + '/placeholder.*');
			if(err) {
				console.log('Cannot find placeholder file');
			} else if(phFiles.length > 0) {
				var upPh = q.defer();
				promises.push(upPh.promise);
				mediaServices.uploadHotelGroupImage(hgId, phFiles[0], {public_id: 'placeholder'}, function(err, upRes) {
					if(err) {
						console.log('Cannot upload placeholder file');
						upPh.reject();
						return;
					}
					data.mediaConfiguration.placeholderImage = {};
					mediaServices.setImageObjectSource(data.mediaConfiguration.placeholderImage, upRes);
					upPh.resolve(true);
				});
			}

			/**
			 * Upload category images
			 * @param cat
			 */
			function im4cat(cat) {
				console.log('Images for category', cat.categoryId);
				//category background
				var bgFiles = glob.sync(groupFile + '/categories/' + cat.categoryId + '/bg.*');
				if(bgFiles.length > 0) {
					var catD = q.defer();
					promises.push(catD.promise);
					mediaServices.uploadHotelGroupImage(hgId, bgFiles[0], {public_id: 'categories/' + cat.categoryId + '/bg'}, function(err, upRes) {
						if(err) {
							return catD.reject();
						}

						for( var lang in cat.localized){
							var localized = cat.localized[lang];
							localized.backgroundImage = {};

							mediaServices.setImageObjectSource(localized.backgroundImage, upRes);
						}
						delete cat.backgroundImage;
						catD.resolve(true);
					});
				}
				//category button image
				var bgFiles2 = glob.sync(groupFile + '/categories/' + cat.categoryId + '/button.*');
				if(bgFiles2.length > 0) {
					var catD2 = q.defer();
					promises.push(catD2.promise);
					mediaServices.uploadHotelGroupImage(hgId, bgFiles2[0], {public_id: 'categories/' + cat.categoryId + '/button'}, function(err, upRes) {
						if(err) {
							return catD2.reject();
						}
						for( var lang in cat.localized){
							var localized = cat.localized[lang];
							localized.buttonImage = {};

							mediaServices.setImageObjectSource(localized.buttonImage, upRes);
						}
						delete cat.buttonImage;
						catD2.resolve(true);
					});
				}
			}

			/**
			 * Images for main menu items
			 */
			function im4Mm(item, imageFile, bgFile, ssFiles) {
				if(imageFile){
					var mmD = q.defer();
					promises.push(mmD.promise);
					mediaServices.uploadHotelGroupImage(hgId, imageFile, {public_id: 'main_menu/' + item.menuItemId + '/image'}, function(err, upRes) {
						if(err) {
							return mmD.reject();
						}
						for( var lang in item.localized){
							var localized = item.localized[lang];
							localized.buttonImage = {};

							mediaServices.setImageObjectSource(localized.buttonImage, upRes);
						}
						delete item.buttonImage;
						mmD.resolve(true);
					});
				}

				if(bgFile){
					console.log('HAS BG',item.menuItemId);
					var mmD2 = q.defer();
					promises.push(mmD2.promise);
					mediaServices.uploadHotelGroupImage(hgId, bgFile, {public_id: 'main_menu/' + item.menuItemId + '/bg'}, function(err, upRes) {
						if(err) {
							return mmD2.reject();
						}
						for( var lang in item.localized){
							var localized = item.localized[lang];
							localized.backgroundImage = {};

							mediaServices.setImageObjectSource(localized.backgroundImage, upRes);
						}
						delete item.backgroundImage;
						mmD2.resolve(true);
					});
				}


				function uplSlide(mmD3, img, file, index){
					mediaServices.uploadHotelGroupImage(hgId, file, {public_id: img.id}, function (err, upRes) {
						if (err) {
							console.log('Cannot upload file', fn);
							return mmD3.reject();
						}
						mediaServices.setImageObjectSource(img, upRes);

						for (var lang in item.localized) {
							var localized = item.localized[lang];
							localized.slideshowImages[index] = img;
						}
						delete item.slideshowImages[index];
						mmD3.resolve(true);
					});
				}

				if(ssFiles){
					if(ssFiles.length > 1) {
						console.log('HAS SS', item.menuItemId);
						item.slideshowImages = [];
						ssFiles.forEach(function (file, index) {
							var mmD3 = q.defer();
							var fn = 'slideshow/' + index;
							var public_id = 'main_menu/' + item.menuItemId + '/' + fn;
							var img = {	id: public_id };

							promises.push(mmD3.promise);
							uplSlide(mmD3, img, file, index);

						});
					}
				}

			}

			data.categories.forEach(function(cat) {
				im4cat(cat);
			});

			function parseMenuItem(item){
				console.log('Images for main menu item', item.menuItemId);
				//category button image
				var mmFiles = glob.sync(groupFile + '/main_menu/' + item.menuItemId + '/image.*');
				var mmFiles2 = glob.sync(groupFile + '/main_menu/' + item.menuItemId + '/bg.*');
				var mmFiles3 = glob.sync(groupFile + '/main_menu/' + item.menuItemId + '/ssImage*.*');
				im4Mm(item, mmFiles.length > 0 ? mmFiles[0] : null, mmFiles2.length > 0 ? mmFiles2[0] : null, mmFiles3.length > 0 ? mmFiles3 : null);
			}

			for(var m in data.mainMenu.items) {
				parseMenuItem(data.mainMenu.items[m]);
			}
			for(var c in data.mainMenu.customItems) {
				parseMenuItem(data.mainMenu.customItems[c]);
			}

			/**
			 * If all done, save in database
			 */
			var allDone1 = q.allSettled(promises);
			allDone1.catch(function() {
				console.log('Some hotel group promises failed');
			});
			allDone1.done(function() {
				console.log('Saving hotel group...', data.hotelGroupId);
				hgServices.updateHotelGroup(data, function(err) {
					if(err) {
						if(err.data.code === 'ver-no-change'){
							console.log('No changes', data.hotelGroupId);
						}else{
							console.log('Failed', data.hotelGroupId, err);
							return hgPromise.reject();
						}

					}
					console.log('Done', data.hotelGroupId);
					hgPromise.resolve();
				});
			});
		});
		q.allSettled(hgPromises).done(function() {
			console.log('All done, time to hotel group goLive!');
			jobhandler.createJob('hotelGroupgolive', 'htlgrpgolive', {}, 0, function(err, job){
				if(err){
					console.log('Hotel Group golive scheduled job creation failed', err);
					process.exit(0);
				}
				scheduler.executeJobAndWaitForCompletion(job, function(err){
					if(err){
						console.log('Hotel Group golive job execution failed', err);
						process.exit(0);
					}
					console.log('Hotel Group goLive completed. All done');
					process.exit(0);
				});
			});
		},function(e){
			console.log('Error!', e);
		});

	});
}

console.log('Testing database connection...');
databaseUtils.getConnection(function(err, client, done){
	if(err){
		console.log('NO DB connection', err);
		process.exit(1);
	}
	client.query('SELECT 1 as b', function(e){
		done();
		if(e){
			console.log('DB query failed', e);
			process.exit(1);
		}else{
			console.log('DB OK!');
			profile.init(function(err) {
				if(err) {
					logging.emerg('Profile init failed', err);
				} else {
					populate();
				}
			});
		}
	})

});