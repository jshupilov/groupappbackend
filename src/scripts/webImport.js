/**
 * Created by jevgenishupilov on 19/10/15.
 */
var trustContentServices = require('../modules/integration/TrustContent/services');
var jobHandler = require('../modules/worker/jobHandler');
var scheduler = require('../modules/worker/scheduler');

var limit = null;
var onlyNew = false;
var hotelIds = false;

if(process.argv[2]){
	limit = parseInt(process.argv[2]);
}

if(process.argv[3]){
	onlyNew = parseInt(process.argv[3]) ? true : false;
}

if(process.argv[4]){
	hotelIds = process.argv[4].split(',').map(function(hid){
		return hid.trim();
	});
}

console.log("Starting Trust Content importer...");
console.log('Only new hotels?', onlyNew);
console.log('Only list of hotels?', hotelIds);
trustContentServices.startCoordinatedImportFromWebsite('slh', function(err, res){
	//if error occurred, retry
	console.log("Coordinated importer done");

	jobHandler.createJob('Hotel and group data go live from script', 'htlndgrouplive', {}, 0, function(err, job){
		console.log("Executing the go live process", err);
		scheduler.executeJobAndWaitForCompletion(job, function(err, res){
			console.log("go live done", err, res);
			process.exit(0);
		}, 6000);

	});

}, limit, null, onlyNew, hotelIds);