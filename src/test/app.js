'use strict';
var testingTools = require('./testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var profiles = require('../modules/profiles/services');
var urlPrefix = process.env.PUBLIC_PROTOCOL+'://'+process.env.PUBLIC_HOST;
testingTools.disableEvents();

lab.test('The app should register a server', function(done) {
	var app = testingTools.requireApp();
	testingTools.code.expect(app).to.be.an.object();
	testingTools.code.expect(app).to.include('server');
	testingTools.code.expect(app.server).to.be.an.instanceof(testingTools.hapi.Server);
	done();
});

lab.experiment('The connection', function() {
	var e1, e2, e3;

	lab.before(function(done){
		e1 = process.env.TLS_KEY;
		e2 = process.env.TLS_CERT;
		e3 = process.env.PUBLIC_PROTOCOL;
		done();
	});

	lab.afterEach(function(done){
		process.env.TLS_KEY = e1;
		process.env.TLS_CERT = e2;
		process.env.PUBLIC_PROTOCOL = e3;
		done();
	});

	lab.after(function(done){
		testingTools.requireApp();
		done();
	});

	lab.test('should be registered as http if no TLS_KEY', function(done) {
		delete process.env.TLS_KEY;
		process.env.PUBLIC_PROTOCOL = 'https';
		process.env.TLS_CERT = 'test_server.crt';
		var app = testingTools.requireApp();
		testingTools.code.expect(app.server.connections.length).to.equal(1);
		testingTools.code.expect(app.server.connections[0].info.protocol).to.equal('http');
		done();
	});

	lab.test('should be registered as http if no TLS_CERT', function(done) {
		delete process.env.TLS_CERT;
		process.env.PUBLIC_PROTOCOL = 'https';
		process.env.TLS_KEY = 'test_server.key';
		var app = testingTools.requireApp();
		testingTools.code.expect(app.server.connections.length).to.equal(1);
		testingTools.code.expect(app.server.connections[0].info.protocol).to.equal('http');
		done();
	});

	lab.test('should be registered as http if PUBLIC_PROTOCOL == http ', function(done) {
		process.env.PUBLIC_PROTOCOL = 'http';
		process.env.TLS_KEY = 'test_server.key';
		process.env.TLS_CERT = 'test_server.crt';
		var app = testingTools.requireApp();
		testingTools.code.expect(app.server.connections.length).to.equal(1);
		testingTools.code.expect(app.server.connections[0].info.protocol).to.equal('http');
		done();
	});

	lab.test('should be registered as https if all conditions are met', function(done) {
		process.env.PUBLIC_PROTOCOL = 'https';
		process.env.TLS_KEY = 'test_server.key';
		process.env.TLS_CERT = 'test_server.crt';
		var app = testingTools.requireApp();
		testingTools.code.expect(app.server.connections.length).to.equal(1);
		testingTools.code.expect(app.server.connections[0].info.protocol).to.equal('https');
		done();
	});
});


lab.experiment('The modules registration callback', function() {
	var app = testingTools.requireApp();
	lab.test('callback should return true', function(done) {
		testingTools.code.expect(app.modulesLoaded()).to.equal(true);
		done();
	});
	lab.test('callback should return false if error provided', function(done) {
		testingTools.code.expect(app.modulesLoaded({error: 1})).to.equal(false);
		done();
	});
});

lab.experiment('The modules registration should succeed', function() {
	var app = testingTools.requireApp();
	var orig;

	lab.before(function(done){
		orig = app.modulesLoaded;

		done();

	});
	lab.after(function(done){
		app.modulesLoaded = orig;
		done();
	});

	lab.test('if called', function(done) {
		app.modulesLoaded = function(err) {
			testingTools.code.expect(err).to.be.undefined();
			done();
		};
		testingTools.code.expect(app.registerModules).not.to.throw();
	});

	lab.test('and not be duplicated', function(done) {
		testingTools.code.expect(app.registerModules()).to.equal(false);
		done();
	});

});

lab.experiment('The modules registration should fail', function() {
	var app = testingTools.requireApp();

	lab.test('with invalid module definition', function(done) {
		app.modulesToLoad = [{register: {bla: 1}, options: {}}];
		testingTools.code.expect(app.registerModules).to.throw(Error);
		done();
	});
});

lab.experiment('The server', function() {
	var app = testingTools.requireApp();
	var req = {
		method: 'GET',
		url: '/',
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/json',
			'x-cardola-uuid': 'test-uuid',
			'x-cardola-origin': 'testing'
		}
	};

	lab.test('should register', {timeout: 20000}, function(done) {
		app = testingTools.requireApp();
		app.registerModules();
		req.url = urlPrefix+testingTools.getBasePath() + 'app/registration';
		app.server.inject(
			req,
			function(response) {
				var result = response.result;
				testingTools.code.expect(result, JSON.stringify(result)).to.be.an.object();
				testingTools.code.expect(result, JSON.stringify(result)).to.include(['uuid']);
				req.headers['x-cardola-uuid'] = result.uuid;
				app = testingTools.requireApp();
				done();
			}
		);
	});

	lab.test('should handle invalid URL', function(done) {
		req.url = urlPrefix + '/invalid/path';
		app.server.inject(
			req,
			function(response) {
				var result = response.result;
				testingTools.code.expect(result).to.be.an.object();
				testingTools.code.expect(result).to.include(['statusCode', 'message']);
				testingTools.code.expect(result.statusCode).to.equal(400);
				done();
			}
		);
	});

	lab.test('should fallback with valid nonexistent URL', function(done) {
		var resUrl = '/path';
		req.url = urlPrefix + testingTools.getBasePath() + resUrl;
		app.server.inject(
			req,
			function(response) {
				var result = response.result;
				testingTools.code.expect(result).to.be.an.object();
				testingTools.code.expect(result).to.include(['code', 'message']);
				testingTools.code.expect(result.code, JSON.stringify(result)).to.equal('app-inv-route');
				done();
			}
		);
	});

	lab.test('should handle URL with invalid api version', function(done) {
		req.url = urlPrefix + '/version-invalid/some/path';
		app.server.inject(
			req,
			function(response) {
				var result = response.result;
				testingTools.code.expect(result).to.be.an.object();
				testingTools.code.expect(result).to.include(['statusCode', 'message']);
				testingTools.code.expect(result.statusCode).to.equal(400);
				done();
			}
		);
	});

	lab.test('should handle URL with invalid hgid', function(done) {
		req.url = urlPrefix + '/' + app.apiVersion + '/some/path';
		app.server.inject(
			req,
			function(response) {
				var result = response.result;
				testingTools.code.expect(result).to.be.an.object();
				testingTools.code.expect(result).to.include(['statusCode', 'message']);
				testingTools.code.expect(result.statusCode).to.equal(400);
				done();
			}
		);
	});
});

lab.experiment('Documentation', function() {
	var app = testingTools.requireApp();
	var req = {
		method: 'GET',
		url: urlPrefix + '/',
		headers: {
			'x-cardola-origin': 'testing'
		}
	};
	lab.test('should ask for basic auth', function(done) {
		req.url = urlPrefix + '/doc/index.html';
		app.server.inject(
			req,
			function(response) {
				testingTools.code.expect(response.statusCode).to.equal(401);
				done();
			}
		);
	});

	lab.test('should work with correct basic auth', function(done) {
		req.url = urlPrefix + '/doc/index.html';
		req.headers.authorization = 'Basic Y2FyZG9sYTpzMW0wbg==';
		app.server.inject(
			req,
			function(response) {
				var result = response.result;
				testingTools.code.expect(result).to.be.an.string();
				testingTools.code.expect(response.statusCode).to.equal(200);
				done();
			}
		);
	});
});

lab.experiment('Profiles Verify', function() {
	var _verify;
	lab.before(function(done){
		_verify = profiles.verify;
		done();
	});
	lab.after(function(done){
		profiles.verify = _verify;
		done();
	});
	lab.test('should work', function(done) {
		var app = testingTools.requireApp();
		app.registerModules();
		var req = {
			method: 'GET',
			url: urlPrefix + '/profiles/verify?sptoken=Profile-Email-Verify-Token&email=test@email.net',
			headers: {
				'x-cardola-origin': 'testing'
			}
		};
		var c2 = 0;
		profiles.verify = function(r, cb){
			c2++;
			cb(null, {success: true});
		};
		app.server.inject(req, function(response) {
			var result = response.result;
			testingTools.code.expect(c2).to.equal(1);
			testingTools.code.expect(result).to.be.an.string();
			testingTools.code.expect(response.statusCode).to.equal(200);
			done();
		});
	});
});

lab.experiment('Server redirect to cardola.net', function(){
	lab.test('Redirection should work', function(done){
		var app = testingTools.requireApp();
		var suffix = '?test=LoremIpsum';
		var options = {
			url: 'http://group-app.herokuapp.com/profiles/verify' + suffix
		};
		app.server.inject(options, function(res){
			testingTools.code.expect(res.statusCode === 301);
			testingTools.code.expect(res.headers.location).to.be.equal(process.env.PUBLIC_PROTOCOL+'://'+process.env.PUBLIC_HOST+'/profiles/verify'+suffix);
			done();
		});
	});
	lab.test('Redirect should not redirect', function(done){
		var app = testingTools.requireApp();
		app.registerModules();
		var options = {
			url: 'https://'+process.env.PUBLIC_HOST+'/profiles/verify',
			headers: {
				'x-cardola-origin': 'testing'
			}
		};
		app.server.inject(options, function(res){
			testingTools.code.expect(res.statusCode).to.be.equal(200);
			done();
		});
	});

});
