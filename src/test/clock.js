'use strict';
var testingTools = require('./testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var timemachine = require('timemachine');
var scheduler = require('../modules/worker/scheduler');
var logging = require('../modules/utils/logging');
var clock = require('../clock');


clock.schedule = [];

var iron_mq = require('iron_mq');
var imq = new iron_mq.Client();
lab.test('Clearing queues for testing....', {timeout: 20000}, function(done) {
	var queue1 = imq.queue('scheduled_jobs');
	var queue2 = imq.queue('completed_jobs');
	var queue3 = imq.queue('failed_jobs');

	queue1.clear(function() {
		queue2.clear(function() {
			queue3.clear(function() {
				done();
			});
		});
	});
});

lab.experiment('Testing the clock', function(){
	var loggemerg, loginfo, handlejob, f;
	var schedule;

	lab.beforeEach(function(done){
		loggemerg = logging.emerg;
		loginfo = logging.info;
		handlejob = scheduler.handleStaticJobSchedule;
		f = global.setInterval;
		//stop the static jobs
		scheduler.stopStaticJobs(function(){
			done();
		});

	});
	lab.before({timeout: 40000}, function(done){
		schedule = clock.schedule;
		timemachine.config({
			dateString: 'December 25, 2015 12:00:00',
			tick: true
		});
		scheduler.stopCoordinator(function(){
			done();
		});

	});

	lab.afterEach(function(done){
		clock.schedule = schedule;
		logging.emerg = loggemerg;
		logging.info = loginfo;
		scheduler.handleStaticJobSchedule = handlejob;
		global.setInterval = f;
		timemachine.reset();
		done();
	});

	lab.after({timeout: 30000}, function(done){
		timemachine.reset();
		scheduler.stopStaticJobs(function(){
			scheduler.stopCoordinator(function(){
				done();
			});
		});
	});

	lab.test('Start clock process', {timeout: 6000}, function(done){

		//set job for exact time
		clock.schedule = [
			{
				scheduleInCrontabFormat: '* * 12 * * *', //every 6h
				job: {
					name: 'test',
					code: 'test-job',
					priority: 0, //does not really matter
					input: {
						some: null
					}
				}
			}, {
				scheduleInCrontabFormat: '* * 13 * * *', //every 6h
				job: {
					name: 'test',
					code: 'test-job',
					priority: 0, //does not really matter
					input: {
						some: null
					}
				}
			}
		];

		//start handler
		clock.startCronjobs();

		//set time
		timemachine.config({
			dateString: 'December 25, 2015 12:00:00',
			tick: true
		});


		setTimeout(function(){
			testingTools.code.expect(scheduler.latestQueuedJobs).to.have.length(1);
			testingTools.code.expect(scheduler.latestQueuedJobs[0].jobDefinition).to.equal(clock.schedule[0]);
			done();
		}, 2000);
	});

	lab.test('Get clock process error', {timeout: 4000}, function(done){
		var msg;
		logging.emerg = function(mess){
			msg = mess;
		};
		scheduler.handleStaticJobSchedule = function(job, cb){
			cb(new Error('jobhasaproblem'));
		};
		clock.startCronjobs();
		setTimeout(function(){
			testingTools.code.expect(msg).to.be.equal('Clock process got an error at startup!');
			done();
		}, 100);

	});
	lab.test('Dry run on clock', {timeout: 4000}, function(done){
		scheduler.handleStaticJobSchedule = function(job, cb){
			cb(null);
		};
		logging.info = function(mess){
			testingTools.code.expect(mess).to.be.equal('Clock process started');
			done();
		};
		clock.schedule = [];
		clock.startCronjobs();
	});

	lab.test('Get keepalive message', {timeout: 3000}, function(done){
		var msg;
		global.setInterval = function(func) {
			global.setTimeout.call(this, func, 1);

		};
		logging.info = function(mess){
			msg = mess;
		};
		clock.schedule = [];
		clock.startCronjobs();
		setTimeout(function(){
			testingTools.code.expect(msg).to.be.equal('Clock process alive...');
			done();
		}, 10);

	});
});
