/**
 * Created by jevgenishupilov on 04/02/15.
 */
'use strict';
var testingTools = require('./testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
/**
 * Required environment variables
 * @type {string[]}
 */
exports.requiredEnv = [
	//'HOST',
	//'PORT',
	'PUBLIC_PROTOCOL',
	'PUBLIC_HOST',
	'PUBLIC_PORT',
	//'TLS_KEY',
	//'TLS_CERT',
	'CMD',
	'ALGOLIASEARCH_API_KEY',
	'ALGOLIASEARCH_API_KEY_SEARCH',
	'ALGOLIASEARCH_APPLICATION_ID',
	'ALGOLIASEARCH_PREFIX',
	'CLOUDINARY_URL',
	'CLOUDINARY_BASE_FOLDER',
	'DATABASE_URL',
	'STORMPATH_API_KEY_ID',
	'STORMPATH_API_KEY_SECRET',
	'STORMPATH_URL',
	'STORMPATH_DIR_PREFIX',
	//'NEW_RELIC_LICENSE_KEY',
	//'NEW_RELIC_APP_NAME',
	//'IP_WHITELIST',
	//'LOGENTRIES_TOKEN',
	//'TEST_CHECKSUM',
	'IRON_TOKEN',
	'IRON_PROJECT_ID',
	'IRON_HOST',
	'HEROKU_API_TOKEN',
	'MANDRILL_API_KEY',
	'LEASEWEB_CUSTOMER',
	'LEASEWEB_SECRET',
	'LEASEWEB_ZONE',
	'PAPERTRAIL_APP_TOKEN',
	'LEASEWEB_ZONE',

	'SKEY_HOST',
	'SKEY_PORT',
	'SKEY_AUTH',

	'TRUST_CONTENT_FTP_HOST',
	'TRUST_CONTENT_FTP_PORT',
	'TRUST_CONTENT_FTP_USER',
	'TRUST_CONTENT_FTP_PASS',

	'AWS_ACCESS_KEY_ID',
	'AWS_SECRET_ACCESS_KEY',
	'AWS_S3_BUCKET',
	'AWS_S3_FOLDER',

	'GEOIP_USER_ID',
	'GEOIP_API_KEY',

	'LEASEWEB_FTP_HOST',
	'LEASEWEB_FTP_USER',
	'LEASEWEB_FTP_PASS',
	'AWS_S3_BUCKET_ACCESS_LOGS',
	'STATS_DATABASE_URL',

	'REPORTS_RECIPIENTS'

	//'PAPERTRAIL_HOST',
	//'PAPERTRAIL_PORT'
];

lab.experiment('Check for environment variables', function() {
	lab.test('all should exist', function(done) {
		//a loop to see the real missing value
		for(var i in exports.requiredEnv) {
			testingTools.code.expect(process.env, 'Environment variable is missing!').to.include(exports.requiredEnv[i]);
		}
		done();
	});
});
