'use strict';
var code = exports.code = require('code');   // assertion library
var app = require('../app');
var http = require('http');
var escape = require('pg-escape');
var eventUtils = require('../modules/utils/eventUtils');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
exports.hapi = require('hapi');
exports.rewire = require('rewire');
var databaseUtils = require('../modules/utils/databaseUtils');
var apiUtils = require('../modules/utils/apiUtils');
var profileServices = require('../modules/profiles/services');
var testingTools = require('./testingTools');
var q= require('q');
var sp = require('stormpath');
var FtpClient = require('ftp');
var apiKey = new sp.ApiKey(process.env.STORMPATH_API_KEY_ID, process.env.STORMPATH_API_KEY_SECRET);
var spClient = new sp.Client({apiKey: apiKey});

/**
 * Tests for all the functions in this file!!
 */

lab.experiment('event functions', function() {

  lab.after(function(done){
    testingTools.disableEvents();
    done();
  });
  lab.test('testingTools.disableEvents', function(done) {
    var t = 0;
    var c = function(callback) {
      t++;
      callback(null, t);
    };

    code.expect(t).to.equal(0);
    eventUtils.emitter.on('testEvent', c);
    testingTools.disableEvents();
    eventUtils.emitter.parallel('testEvent', function(err) {
      code.expect(err).to.be.undefined();
      code.expect(t).to.equal(0);
      done();
    });
  });

  lab.test('testingTools.enableEvents', function(done) {
    var t = 0;
    var c = function(callback) {
      t++;
      callback(null, t);
    };

    code.expect(t).to.equal(0);
    eventUtils.emitter.on('testEvent', c);
    testingTools.enableEvents();
    eventUtils.emitter.parallel('testEvent', function(err) {
      code.expect(err).to.be.null();
      code.expect(t).to.equal(1);
      done();
    });
  });
});

lab.test('testingTools.requireUncached', function(done) {
  var t = function() {
    return testingTools.requireUncached('../app');
  };
  code.expect(t).not.to.throw();
  code.expect(t()).to.be.object();
  done();
});

lab.test('testingTools.getHotelGroupId', function(done) {
  code.expect(testingTools.getHotelGroupId()).to.be.string();
  done();
});

lab.test('testingTools.generateUuid', function(done) {
  code.expect(testingTools.generateUuid()).to.be.string();
  done();
});

lab.test('testingTools.getApiKey', function(done) {
  code.expect(testingTools.getApiKey()).to.be.string();
  done();
});

lab.test('testingTools.getApiVersion', function(done) {
  code.expect(testingTools.getApiVersion()).to.equal(app.apiVersion);
  code.expect(testingTools.getApiVersion().length > 0).to.equal(true);
  done();
});

lab.test('testingTools.getBasePath', function(done) {
  code.expect(testingTools.getBasePath().length > 0).to.equal(true);
  code.expect(testingTools.getBasePath()).to.include([testingTools.getApiVersion(), testingTools.getHotelGroupId()]);
  done();
});

lab.experiment('testingTools.getUuid', {timeout: 5000}, function() {

  lab.test('should work', {timeout: 20000}, function(done) {
    testingTools.uuid = null;
    testingTools.getUuid(function(err, uuid2) {
      code.expect(err).to.be.null();
      code.expect(uuid2).to.be.string();
      code.expect(uuid2).to.equal(testingTools.uuid);
      done();
    });
  });

  lab.test('should work if uuid is set', {timeout: 20000}, function(done) {
    testingTools.getUuid(function(err, uuid2) {
      code.expect(err).to.be.null();
      code.expect(uuid2).to.be.string();
      code.expect(uuid2).to.equal(testingTools.uuid);
      done();
    });
  });
});

lab.experiment('testingTools.getRequestStructure', function() {
  lab.test('should work', {timeout: 20000}, function(done) {
    testingTools.getRequestStructure(function(err, res) {
      code.expect(err).to.be.null();
      code.expect(res).to.be.object();
      done();
    });
  });
});

lab.test('testingTools.requireApp', function(done) {
  var t = function() {
    return testingTools.requireApp();
  };
  code.expect(t).not.to.throw();
  code.expect(t()).to.be.object();
  done();
});

lab.test('testingTools.expectCollection', function(done) {
  var t = function() {
    testingTools.expectCollection({href: '', offset: 0, limit: 0, items: []});
  };
  code.expect(t).not.to.throw();
  done();
});


lab.test('testingTools.expectNonEmptyCollection', function(done) {
  var t = function() {
    testingTools.expectNonEmptyCollection({href: '', offset: 0, limit: 0, items: [{smt: 1}]});
  };
  code.expect(t).not.to.throw();
  done();
});


lab.test('testingTools.expectResource', function(done) {
  var t = function() {
    testingTools.expectResource({href: 'asd'});
  };
  code.expect(t).not.to.throw();
  done();
});


lab.experiment('testingTools.expectError', function() {

  lab.test('should work', function(done) {

		var t = function() {
			var err = new Error();
			err.data = {
				code: 1,
				isCustomError: true
			};
			testingTools.expectError(err, 1, 'some-info');
		};

    var t2 = function() {
      var err = new Error();
      err.data = {
        code: 1,
        isCustomError: true
      };
      testingTools.expectError(err);
    };

    var t3 = function() {
      testingTools.expectError(function() {
        var err = new Error();
        err.data = {
          code: 1,
          isCustomError: true
        };
        throw err;
      });
    };

    var t4 = function() {
      testingTools.expectError(function() {
      });
    };
    code.expect(t).not.to.throw();
    code.expect(t2).not.to.throw();
    code.expect(t3).not.to.throw();
    code.expect(t4).to.throw();
    done();
  });

});

lab.experiment('testingTools.failDbQueryForExperiment with specific table', function() {
  testingTools.failDbQueryForExperiment(lab, 'hotel_group');

  lab.test('should fail queries to given table', function(done) {
    databaseUtils.getConnection(function(err, client, dbDone){
      testingTools.code.expect(err).to.be.null();
      client.query('select * FROM hotel_group', function(err){
        dbDone();
        testingTools.expectError(err, 'db-query-fails');
        done();
      });
    });
  });

  lab.test('should pass queries to other tables', function(done) {
    databaseUtils.getConnection(function(err, client, dbDone){
      testingTools.code.expect(err).to.be.null();
      client.query('select * FROM hotel limit 1', function(err){
        dbDone();
        testingTools.code.expect(err).to.be.null();
        done();
      });
    });
  });

});

lab.experiment('testingTools.failDbQueryForExperiment with specific table and statistics database', function() {
  testingTools.failDbQueryForExperiment(lab, 'access_log', true);

  lab.test('should fail queries to given table', function(done) {
    databaseUtils.getConnectionStats(function(err, client, dbDone){
      testingTools.code.expect(err).to.be.null();
      client.query('select * FROM access_log', function(err){
        dbDone();
        testingTools.expectError(err, 'db-query-fails');
        done();
      });
    });
  });

  lab.test('should pass queries to other tables', function(done) {
    databaseUtils.getConnectionStats(function(err, client, dbDone){
      testingTools.code.expect(err).to.be.null();
      client.query('select * FROM pgmigrations limit 1', function(err){
        dbDone();
        testingTools.code.expect(err).to.be.null();
        done();
      });
    });
  });

});

lab.experiment('testingTools.failDbQueryForExperiment with specific table after experiment', function() {

  lab.test('should not fail queries to given table', function(done) {
    databaseUtils.getConnection(function(err, client, dbDone){
      testingTools.code.expect(err).to.be.null();
      client.query('select * FROM hotel_group LIMIT 1', function(err){
        dbDone();
        testingTools.code.expect(err).to.be.null();
        done();
      });
    });
  });
});

lab.experiment('testingTools.failDbQueryForExperiment without specific table', function() {
  testingTools.failDbQueryForExperiment(lab);

  lab.test('should fail all queries', function(done) {
    databaseUtils.getConnection(function(err, client, dbDone){
      testingTools.code.expect(err).to.be.null();
      client.query('select 1 as c', function(err){
        dbDone();
        testingTools.expectError(err, 'db-query-fails');
        done();
      });
    });
  });
});

lab.experiment('testingTools.failDbQueryForExperiment without specific table after experiment', function() {

  lab.test('should not fail queries', function(done) {
    databaseUtils.getConnection(function(err, client, dbDone){
      testingTools.code.expect(err).to.be.null();
      client.query('select 1 as c', function(err){
        dbDone();
        testingTools.code.expect(err).to.be.null();
        done();
      });
    });
  });
});


lab.test('testingTools.expectQueryResultsWithFields', function(done) {
  testingTools.expectQueryResultsWithFields({rows: [], fields: [{name: 'name'}]}, ['name']);
  done();
});

lab.test('testingTools.killDb', function(done) {
  var t = function(){
    testingTools.killDb();
  };
  code.expect(t).not.to.throw();
  code.expect(process.env.DATABASE_URL).to.equal('null');
  done();
});

lab.test('testingTools.recoverDb', function(done) {
  var t = function(){
    testingTools.recoverDb();
  };
  code.expect(t).not.to.throw();
  code.expect(process.env.DATABASE_URL).not.to.be.null();
  done();
});

lab.experiment('testingTools.killDbConnectionForExperiment', function() {
  testingTools.killDbConnectionForExperiment(lab);

  lab.test('should kill the db connection for test', function(done) {
    databaseUtils.getConnection(function(err){
      code.expect(err).to.be.instanceof(Error);
      done();
    });

  });
});

lab.experiment('testingTools.killDbConnectionForExperiment', function() {
  testingTools.killDbConnectionForExperiment(lab, true);

  lab.test('should kill the statistics db connection for test', function(done) {
    databaseUtils.getConnectionStats(function(err){
      code.expect(err).to.be.instanceof(Error);
      done();
    });

  });
});

lab.experiment('testingTools.killDbConnectionForExperiment', function() {

  lab.test('should revert the change at the end', function(done) {
    databaseUtils.getConnection(function(err){
      code.expect(err).to.be.null();
      done();
    });
  });
});

lab.experiment('testingTools.updateDbRowForExperiment', function() {
  var where = ["(data -> 'hotelId') = %L", '"stpeter"'];
  testingTools.updateDbRowForExperiment(
    lab,
    'hotel',
    function(row){
      row.data.testVar = true;
    },
    where
  );

  lab.test('should updated a row for test', function(done) {
    databaseUtils.getConnection(function(err, client){
      code.expect(err).to.be.null();
      var w2 = [where[0], where[1]];
      w2[0] = 'SELECT * FROM hotel WHERE ' + w2[0];
      client.query(
        escape.apply(escape, w2),
        function(err, res){
          code.expect(err).to.be.null();
          testingTools.expectQueryResultsWithFields(res, ['id', 'data']);
          code.expect(res.rows).not.to.be.empty();
          code.expect(res.rows[0]).to.include(['data']);
          code.expect(res.rows[0].data).to.include(['testVar']);
          code.expect(res.rows[0].data.testVar).to.be.true();
          done();
        }
      );
    });
  });
});

lab.experiment('testingTools.updateDbRowForExperiment', function() {
  var where = ["(data -> 'hotelId') = %L", '"stpeter"'];

  lab.test('should have reverted the row', function(done) {
    databaseUtils.getConnection(function(err, client){
      code.expect(err).to.be.null();
      var w2 = [where[0], where[1]];
      w2[0] = 'SELECT * FROM hotel WHERE ' + w2[0];
      client.query(
        escape.apply(escape, w2),
        function(err, res){
          code.expect(err).to.be.null();
          testingTools.expectQueryResultsWithFields(res, ['id', 'data']);
          code.expect(res.rows).not.to.be.empty();
          code.expect(res.rows[0]).to.include(['data']);
          code.expect(res.rows[0].data).not.to.include(['testVar']);
          done();
        }
      );
    });
  });
});

lab.experiment('testingTools.mockResponsesForExperiment', function() {
  testingTools.mockResponsesForExperiment(lab, function(req, res){
    res.setHeader('content-type', 'application/json');
    res.statusCode = 503;
    res.end('{"test": "test response"}', 'utf8');
  }, function(){});

  lab.test('should mock request to anything', function(done) {
    var data = '';
    var parse = function(){
      data = JSON.parse(data);
    };
    http.get('http://www.google.com', function(res){
      res.on('data', function (chunk) {
        data += chunk;
      });
      res.on('end', function () {
        code.expect(parse).not.to.throw();
        code.expect(data).to.be.object();
        code.expect(data).to.include(['test']);
        code.expect(data.test).to.equal('test response');
        done();
      });

    });
  });
});

lab.experiment('testingTools.mockResponsesForExperiment', function() {
  testingTools.mockResponsesForExperiment(lab);

  lab.test('should mock a default request to anything', function(done) {
    var data = '';
    var parse = function(){
      data = JSON.parse(data);
    };
    http.get('http://www.google.com', function(res){
      res.on('data', function (chunk) {
        data += chunk;
      });
      res.on('end', function () {
        code.expect(parse).not.to.throw();
        code.expect(data).to.be.object();
        done();
      });

    });
  });
});

lab.experiment('testingTools.mockResponsesForExperiment', function() {
  lab.test('should end replying to requests after experiment has ended', function(done) {
    var data = '';
    var parse = function(){
      return JSON.parse(data);
    };
    http.get('http://www.google.com', function(res){
      res.on('data', function (chunk) {
        data += chunk;
      });
      res.on('end', function () {
        code.expect(data).to.be.string();
        code.expect(parse).to.throw();
        done();
      });
    });
  });
});
lab.experiment('testingTools.startTransactionAndCreateDummyTableForExperiment', function(){
  var tableName='dummy_table';
  var schema='public';
  testingTools.startTransactionAndCreateDummyTableForExperiment(lab, tableName);

  var transactor = {
    txid: [],
    getId: function (p) {
      databaseUtils.getConnection(function(err, client){
        testingTools.code.expect(err).to.be.null();
        client.query('SELECT txid_current();', function(err, result){
          testingTools.code.expect(err).to.be.null();
          transactor.txid.push(result.rows[0].txid_current);
          p.resolve();
        });
      });
    }
  };

  lab.test('should initiate new transaction', function(done){
    var promises = [];

    for(var i=0; i<2; i++){
      var p = q.defer();
      promises.push(p.promise);
      transactor.getId(p);
    }
    q.all(promises).done(function(){
      testingTools.code.expect(transactor.txid[0]).to.be.equal(transactor.txid[1]);
      done();
    });
  });

  lab.test('test table should exists by now.', function(done){
    databaseUtils.getConnection(function(err, client){

      testingTools.code.expect(err).to.be.null();
      client.query(
        'SELECT EXISTS (' +
        'SELECT 1 ' +
        'FROM information_schema.tables ' +
        'WHERE table_schema= $1 ' +
        'AND table_name= $2)',
        [schema, tableName],
        function(err, result){
          testingTools.code.expect(err).to.be.null();
          testingTools.code.expect(result.rows[0].exists).to.equal(true);
          done();
        });
    });

  });
});
lab.experiment('testingTools.startTransactionAndDummyTableCleanup', function(){
  var transactor = {
    txid: [],
    getId: function (p) {
      databaseUtils.getConnection(function(err, client){
        testingTools.code.expect(err).to.be.null();
        client.query('SELECT txid_current();', function(err, result){
          testingTools.code.expect(err).to.be.null();
          transactor.txid.push(result.rows[0].txid_current);
          p.resolve();
        });
      });
    }
  };


  var tableName='dummy_table';
  var schema='public';
  lab.test('Dummy test table should be gone by now', function(done){
    databaseUtils.getConnection(function(err, client, doneDb){
      testingTools.code.expect(err).to.be.null();
      client.query(
        'SELECT EXISTS (' +
        'SELECT 1 ' +
        'FROM information_schema.tables ' +
        'WHERE table_schema= $1 ' +
        'AND table_name= $2)',
        [schema, tableName],
        function(err, result){
          testingTools.code.expect(err).to.be.null();
          testingTools.code.expect(result.rows[0].exists).to.equal(false);
          doneDb();
          done();
        });
    });
  });
  lab.test('Transaction should be gone also', function(done){
    var promises = [];

    for(var i=0; i<2; i++){
      var p = q.defer();
      promises.push(p.promise);
      transactor.getId(p);
    }
    q.all(promises).done(function(){
      testingTools.code.expect(transactor.txid[0]).to.be.below(transactor.txid[1]);
      done();
    });
  });
	lab.test('should end replying to requests after experiment has ended', function(done) {
		var data = '';
		var parse = function(){
			return JSON.parse(data);
		};
		http.get('http://www.google.com', function(res){
			res.on('data', function (chunk) {
				data += chunk;
			});
			res.on('end', function () {
				code.expect(data).to.be.string();
				code.expect(parse).to.throw();
				done();
			});
		});
	});
});

var createAccountForExperimentEmail;
lab.experiment('testingTools.createAccountForExperiment', function() {
	createAccountForExperimentEmail = testingTools.createAccountForExperiment(lab).email;

	lab.test('should create an user for experiment', {timeout: 5000}, function(done) {
		spClient.getApplication(process.env.STORMPATH_URL, function(err, application) {
			code.expect(err, JSON.stringify(err)).to.be.null();
			application.getAccounts({email: createAccountForExperimentEmail}, function(err, res){
				code.expect(err).to.be.null();
				code.expect(res.items).to.have.length(1);
				done();
			});
		});
	});

});

lab.experiment('testingTools.createAccountForExperiment after experiment', function() {
	lab.test('should have deleted the user', {timeout: 5000}, function(done) {
		spClient.getApplication(process.env.STORMPATH_URL, function(err, application) {
			code.expect(err).to.be.null();
			code.expect(createAccountForExperimentEmail).not.to.be.empty();
			application.getAccounts({email: createAccountForExperimentEmail}, function(err, res){
				code.expect(err).to.be.null();
				code.expect(res.items).to.have.length(0);
				done();
			});
		});
	});
});

lab.experiment('testingTools.expectCachingHeaders', function() {
	var f, f2;

	lab.before(function(done){
		f = global.setTimeout;
		f2 = apiUtils.getCacheConfigurations;

		done();
	});


	lab.afterEach(function(done){
		global.setTimeout = f;
		apiUtils.getCacheConfigurations = f2;
		done();
	});

	lab.test('should give error if not all required headers set', function(done) {
		//overwrite to get syncronous behaviour
		global.setTimeout = function(f){
			f();
		};
		var t = function(cb){
			testingTools.expectCachingHeaders(cb, 'default');
		};

		testingTools.expectNativeError(t);
		done();
	});



	lab.test('should give error if headers are not set according to configuration', function(done) {
		var cacheType = 'default';

		global.setTimeout = function(){};

		apiUtils.getCacheConfigurations = function(){
			return {
				'default': {
					'cache-control': {
						flags: ['blablablaasd'],
						'max-age': '30'
					}
				}
			};
		};

		var replyInterface = testingTools.expectCachingHeaders(null, cacheType, {});

		function t1(){
			replyInterface.header('vary', 'origin');
			replyInterface.header('cache-control', 'max-age=30');
		}

		function t2(){
			replyInterface.header('cache-control', 'blablablaasd');
		}

		function t3(){
			replyInterface.header('cache-control', 'max-age=30,blablablaasd');
		}

		testingTools.expectNativeError(t1);
		testingTools.expectNativeError(t2);
		testingTools.code.expect(t3).not.to.throw();

		done();
	});

	lab.test('should work if all headers are set correctly', function(done) {

		var replyInterface = testingTools.expectCachingHeaders(done, 'default', {some: 'thing'});
		apiUtils.addCachingHeaders(replyInterface, 'default');

	});

});

var deleteAccountAfterExperimentEmail = (new Date().getTime()) + '-test-deleteAccountAfterExperiment@cardola.net';
lab.experiment('testingTools.deleteAccountAfterExperiment', function() {
	var _sendVerificationEmail, uuid;
	var body = {
		email: deleteAccountAfterExperimentEmail,
		password: 'SecurePassword123',
		givenName: 'ads',
		surname: 'asss',
		customData: {
			settings: {}
		}
	};

	lab.before(function(done){
		_sendVerificationEmail = profileServices.sendVerificationEmail;

		testingTools.getUuid(function(err, id){
			testingTools.code.expect(err).to.be.null();
			uuid = id;
			done();
		});
	});

	lab.afterEach(function(done){
		profileServices.sendVerificationEmail = _sendVerificationEmail;
		done();
	});

	testingTools.deleteAccountAfterExperiment(lab, body.email, true);
	lab.test('should delete user after creation', {timeout: 30000}, function(done) {

		//this will fail, overwrite it
		profileServices.sendVerificationEmail = function(){
			arguments[arguments.length-1](new Error('9ij-ji'));
		};
		//create a user
		var req = apiUtils.createApiRequest('/', {}, {}, 'GET', body);
		apiUtils.populateApiRequest(req, testingTools.getApiVersion(), testingTools.getHotelGroupId(), uuid);

		profileServices.signUp(req, function (err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message, JSON.stringify(err)).to.equal('9ij-ji');
			done();
		});

	});

});

lab.experiment('testingTools.deleteAccountAfterExperiment after', function() {
	testingTools.deleteAccountAfterEveryTestInExperiment(lab, deleteAccountAfterExperimentEmail, false);
	lab.test('the account should be actually deleted', function(done) {

		spClient.getApplication(process.env.STORMPATH_URL, function(err, application) {
			code.expect(err).to.be.null();
			code.expect(deleteAccountAfterExperimentEmail).not.to.be.empty();
			application.getAccounts({email: deleteAccountAfterExperimentEmail}, function(err, res){
				code.expect(err).to.be.null();
				code.expect(res.items).to.have.length(0);
				done();
			});
		});
	});
});
lab.experiment('testingTools.buildTestHotelGroupForVersioning', function(){
  var tableName = 'hotel_group_v';
  var hotelGroupId = 'known';
  testingTools.buildTestHotelGroupForVersioning(lab, tableName, 10, false);
  lab.test('Check is known group done', function(done){
    databaseUtils.getConnection(function(err, client, doneDb){
      testingTools.code.expect(err).to.be.null();
      client.query("SELECT count(*) as c from hotel_group_v where data -> 'hotelGroupId' = $1",
        ['"' + hotelGroupId + '"'], function(err, res){
        testingTools.code.expect(err).to.be.null();
        testingTools.code.expect(res.rowCount).to.be.above(0);
        doneDb();
        done();
      });
    });
  });
});

lab.experiment('testingTools.buildTestHotelGroupForVersioning.withTransaction', function(){
  var tableName = 'dummygrouptable';
  var hotelGroupId = 'known';
  testingTools.startTransactionAndCreateDummyTableForExperiment(lab, tableName);
  testingTools.buildTestHotelGroupForVersioning(lab, tableName, 10, true);
  lab.test('Check is known group done', function(done){
    databaseUtils.getConnection(function(err, client, doneDb){
      testingTools.code.expect(err).to.be.null();
      client.query("SELECT count(*) as c from dummygrouptable where data -> 'hotelGroupId' = $1",
        ['"' + hotelGroupId + '"'], function(err, res){
          testingTools.code.expect(err).to.be.null();
          testingTools.code.expect(res.rowCount).to.be.above(0);
          doneDb();
          done();
        });
    });
  });
});


lab.experiment('testingTools.buildTestHotelsForVersioning', function(){
  var tableName = 'hotel_v';
  var hotelGroupId = 'test-hgid';
  var id='known';
  testingTools.buildTestHotelsForVersioning(lab, tableName, 10, false);
  lab.test('Check is known group done', function(done){
    databaseUtils.getConnection(function(err, client, doneDb){
      testingTools.code.expect(err).to.be.null();
      client.query('SELECT * ' +
        'FROM hotel_v ' +
        "WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 AND (data -> 'hotelId') = $2",
        ['"' + hotelGroupId + '"', '"' + id + '"'], function(err, res){
          testingTools.code.expect(err).to.be.null();
          testingTools.code.expect(res.rowCount).to.be.above(0);
          doneDb();
          done();
        });
    });
  });
});

lab.experiment('testingTools.buildTestHotelsForVersioning.withTransaction', function(){
  var tableName = 'dummyhoteltable';
  var hotelGroupId = 'test-hgid';
  var id='known';
  testingTools.startTransactionAndCreateDummyTableForExperiment(lab, tableName);
  testingTools.buildTestHotelsForVersioning(lab, tableName, 10, true);
  lab.test('Check is known group done', function(done){
    databaseUtils.getConnection(function(err, client, doneDb){
      testingTools.code.expect(err).to.be.null();
      client.query('SELECT * ' +
        'FROM dummyhoteltable ' +
        "WHERE (data -> 'hotelGroup' -> 'hotelGroupId') = $1 AND (data -> 'hotelId') = $2",
        ['"' + hotelGroupId + '"', '"' + id + '"'], function(err, res){
          testingTools.code.expect(err).to.be.null();
          testingTools.code.expect(res.rowCount).to.be.above(0);
          doneDb();
          done();
        });
    });
  });
});
var ftpConfig;

lab.experiment('testingUtils.createFtpServerForExperiment', function() {
  ftpConfig = testingTools.createFtpServerForExperiment(lab);
  lab.test('should have prepared an connectable server', function(done) {
		var ftpClientInstance = new FtpClient();

		ftpClientInstance.on('ready', function() {
			done();
		});

		ftpClientInstance.connect(ftpConfig);
	});

	lab.test('should give error if user invalid/missing', function(done) {
		var ftpClientInstance = new FtpClient();
		var ftpConfigCopy = JSON.parse(JSON.stringify(ftpConfig));
		ftpConfigCopy.connTimeout = 1000;
		delete ftpConfigCopy.user;
		ftpClientInstance.on('error', function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Not logged in.');
			done();
		});

		ftpClientInstance.connect(ftpConfigCopy);
	});

	lab.test('should give error if password invalid/missing', function(done) {
		var ftpClientInstance = new FtpClient();
		var ftpConfigCopy = JSON.parse(JSON.stringify(ftpConfig));
		ftpConfigCopy.connTimeout = 1000;
		delete ftpConfigCopy.password;
		ftpClientInstance.on('error', function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('Not logged in.');
			done();
		});

		ftpClientInstance.connect(ftpConfigCopy);
	});

});

lab.experiment('testingUtils.createFtpServerForExperiment After closed', function() {
	lab.test('should have been closed', function(done) {
		var ftpClientInstance = new FtpClient();
		var ftpConfigCopy = JSON.parse(JSON.stringify(ftpConfig));
		ftpConfigCopy.connTimeout = 1000;
		ftpClientInstance.on('error', function(err) {
			testingTools.code.expect(err).to.be.instanceof(Error);
			testingTools.code.expect(err.message).to.equal('connect ECONNREFUSED');
			done();
		});
		ftpClientInstance.connect(ftpConfigCopy);
	});
});
