'use strict';
var code = exports.code = require('code');   // assertion library
var app = require('../app');
var q = require('q');
var mitm = require('mitm');
var escape = require('pg-escape');
var eventUtils = require('../modules/utils/eventUtils');
var apiUtils = require('../modules/utils/apiUtils');
var profileServices = require('../modules/profiles/services');
var commonServices = require('../modules/common/services');
var errors = require('../modules/utils/errors');
exports.labLib = require('lab');
exports.hapi = require('hapi');
exports.rewire = require('rewire');
var databaseUtils = require('../modules/utils/databaseUtils');
var _uuid = require('node-uuid');
var sp = require('stormpath');
var aws = require('aws-sdk');
var path = require('path');

var apiKey = new sp.ApiKey(process.env.STORMPATH_API_KEY_ID, process.env.STORMPATH_API_KEY_SECRET);
var spClient = new sp.Client({apiKey: apiKey});

exports.requireUncached = function(module) {
	return exports.rewire(module);
};

exports.getHotelGroupId = function() {
	return 'test-hgid';
};

exports.getApiKey = function() {
	return 'test-api-key';
};

exports.getApiVersion = function() {
	return app.apiVersion;
};

exports.generateUuid = function() {
	return _uuid.v4();
};

/**
 * Get base path for testing
 * @returns {string}
 */
exports.getBasePath = function() {
	return '/' + exports.getApiVersion() + '/' + exports.getHotelGroupId() + '/';
};

exports.uuid = null;
/**
 * Get Uuid by calling app/registration
 * @param callback
 */
exports.getUuid = function(callback) {
	if(exports.uuid) {
		setTimeout(function() {
			callback(null, exports.uuid);
		});
		return;
	}
	var req = {
		method: 'GET',
		url: '/',
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/json',
			'x-cardola-uuid': 'test-uuid'
		},
		body: {
			manufacturer: 'Tester',
			model: 'testingTools',
			os: 'lab',
			appVersion: '1.0.0',
			isTablet: false,
			configuredLanguages: [],
			carrierName: 'nodeJS',
			screenResolution: {
				width: 1,
				height: 1,
				scale: 1
			}
		},
		hotelGroupId: exports.getHotelGroupId()
	};
	commonServices.registration2(req, function(err, result) {
		code.expect(err, JSON.stringify(err)).to.be.null();
		code.expect(result).to.include(['uuid']);
		exports.uuid = result.uuid;
		callback(null, exports.uuid);
	});

};

/**
 * Returns request structure with uuid
 * @param callback
 */
exports.getRequestStructure = function(callback) {
	exports.getUuid(function(err, uuid) {
		code.expect(err).to.be.null();
		return callback(null, {
			method: 'GET',
			url: '/',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				'x-cardola-uuid': uuid
			}
		});
	});
};

exports.requireApp = function() {
	return exports.requireUncached('../app');
};

/**
 * Assert the parameter to be a collection item
 * @param param
 */
exports.expectCollection = function(param) {
	var stack = new Error().stack.split('\n')[2];
	code.expect(param, stack).to.be.object();
	code.expect(param, stack).to.include(['href', 'offset', 'limit', 'items']);
	code.expect(param.items, stack).to.be.array();
};

/**
 * Assert the collection to ve a non empty collection item
 * @param param
 */
exports.expectNonEmptyCollection = function(param) {
	var stack = new Error().stack.split('\n')[2];
	exports.expectCollection(param);
	code.expect(param.items, stack).not.to.be.empty();
};

/**
 * Expect the parameter to be a resource
 * @param param
 */
exports.expectResource = function(param) {
	var stack = new Error().stack.split('\n')[2];
	code.expect(param, stack).to.be.object();
	code.expect(param, stack).to.include(['href']);
	code.expect(param, stack).to.not.include(['offset', 'limit', 'items']);
};

/**
 * Expect native error to be thrown
 * @param err
 * @param extraInfo
 */
exports.expectNativeError = function(err, extraInfo) {

	var stack = (extraInfo ? extraInfo + ', ' : '') + JSON.stringify(err) + new Error().stack.split('\n')[2];
	if(typeof err === 'function') {
		try {
			err();
			err = null;
		} catch(e) {
			err = e;
		}
	}

	code.expect(err, stack).to.be.instanceof(Error);
	return err;
};

/**
 * Expect error
 * @param err
 * @param [errorCode]
 * @param extraInfo Extra information for output
 * @return err {Error} Error instance
 */
exports.expectError = function(err, errorCode, extraInfo) {

	var stack = (extraInfo ? extraInfo + ', ' : '') + JSON.stringify(err) + new Error().stack.split('\n')[2];

	err = exports.expectNativeError(err, stack);
	code.expect(err, stack).to.include(['data']);

	code.expect(err.data, stack).to.include(['code', 'isCustomError']);
	code.expect(err.data.isCustomError, stack).to.be.true();
	if(errorCode) {
		code.expect(err.data.code, stack).to.equal(errorCode);
	}
	return err;
};

/**
 * Check if query result is valid and has given fields
 * @param res
 * @param fields
 */
exports.expectQueryResultsWithFields = function(res, fields) {

	var stack = new Error().stack.split('\n')[2];
	code.expect(res, stack).to.be.include(['rows', 'fields']);
	code.expect(res.fields, stack).to.be.array();
	code.expect(res.fields, stack).to.have.length(fields.length);
	fields.forEach(function(f, i) {
		code.expect(res.fields[i].name, stack).to.equal(f);
	});
};

var dbUrl = process.env.DATABASE_URL;
var dbStatUrl = process.env.STATS_DATABASE_URL;
/**
 * FLush db connection string to test for failure
 */
exports.killDb = function(isStatistics) {
	if(isStatistics){
		process.env.STATS_DATABASE_URL = null;
	} else {
		process.env.DATABASE_URL = null;
	}
};

/**
 * Recover db connection string
 */
exports.recoverDb = function(isStatistics) {
	if(isStatistics){
		process.env.STATS_DATABASE_URL = dbStatUrl;
	} else {
		process.env.DATABASE_URL = dbUrl;
	}
};

/**
 * Fail database queries for an experiment
 * @param lab
 * @param [table] optional table name, into which queries need to fail
 */
exports.failDbQueryForExperiment = function(lab, table, isStats) {
	var f1, query;
	isStats = isStats || false;
	lab.before(function(done) {
		query = function () {
			var args = arguments;
			//if this table should work as expected, use the original methods
			var rx = new RegExp('[from|update|into]\\s+' + table + '([,\\s\\(]+|$){1,}', 'gi');
			//NB! do not call .test on same value twice, it will continue from the last position
			if (table && !rx.test(arguments[0])) {

				f1(function (err, client, conDone) {
					code.expect(err).to.be.null();
					//overwrite query callback to end connection
					var cb = function () {
						conDone();
						args[args.length - 1].apply(this, arguments);
					};
					//call original query
					return client.query(args[0], args[1], cb);
				});

			} else {
				arguments[arguments.length - 1](errors.newError('db-query-fails'));
			}
		};
		if (!isStats){
			f1 = databaseUtils.getConnection;
			databaseUtils.getConnection = function () {
				arguments[arguments.length - 1](
					null,
					{
						query: query
					},
					function () {
					}
				);
			};
		} else {
			f1 = databaseUtils.getConnectionStats;
			databaseUtils.getConnectionStats = function () {
				arguments[arguments.length - 1](
					null,
					{
						query: query
					},
					function () {
					}
				);
			};
		}
		done();
	});

	lab.after(function(done) {
		if(!isStats){
			databaseUtils.getConnection = f1;
		} else {
			databaseUtils.getConnectionStats = f1;
		}
		done();
	});
};

/**
 * Kill Db connection string for experiment
 * @param lab
 */
exports.killDbConnectionForExperiment = function(lab, isStatistics) {
	lab.before(function(done) {
		exports.killDb(isStatistics);
		done();
	});

	lab.after(function(done) {
		exports.recoverDb(isStatistics);
		done();
	});
};

/**
 * Update db row for the experiment
 * @param lab
 * @param tableName Name of the database table
 * @param modifyCallback callback(row), which modifies the row
 * @param whereArrayForEscape {Array} where clause as array, given straight to escape() - aka [queryString, param1, param2]
 * @see https://github.com/segmentio/pg-escape for whereArrayForEscape syntax
 */
exports.updateDbRowForExperiment = function(lab, tableName, modifyCallback, whereArrayForEscape) {
	var rows = [];

	function updateRow(client, row, tableName, callback) {
		var params = [];
		var sets = [];
		var i = params.length;
		for(var f in row) {
			sets.push(f + '=$' + (++i));
			params.push(row[f]);
		}
		var sql = escape('UPDATE %I SET ' + sets.join(', ') + ' WHERE id=%L', tableName, row.id);
		client.query(
			sql,
			params,
			function(errU, resU) {
				callback(errU, resU);
			}
		);
	}

	lab.before(function(done) {
		databaseUtils.getConnection(function(err, client, donewc) {
			code.expect(err).to.be.null();
			var query = escape('SELECT * FROM %I WHERE ', tableName) + escape.apply(escape, whereArrayForEscape);
			client.query(
				query,
				function(err, result) {
					code.expect(err, JSON.stringify(err)).to.be.null();
					code.expect(result.rows.length > 0).to.equal(true);
					var promises = [];
					result.rows.forEach(function(row) {
						code.expect(row).to.include(['id']);

						//for each row
						var p = q.defer();
						promises.push(p.promise);
						//save old version
						rows.push(JSON.parse(JSON.stringify(row)));
						//let user modify
						modifyCallback(row);
						//update
						updateRow(client, row, tableName, function(errU, resU) {
							code.expect(errU).to.be.null();
							p.resolve(resU);
						});
					});

					var allS = q.allSettled(promises);
					allS.done(function() {
						donewc();
						done();
					});
				}
			);
		});
	});

	lab.after(function(done) {
		var promises = [];
		databaseUtils.getConnection(function(err, client, donewc) {
			code.expect(err).to.be.null();
			rows.forEach(function(row) {
				var p = q.defer();
				promises.push(p.promise);
				updateRow(client, row, tableName, function(errB, resB) {
					code.expect(errB).to.be.null();
					p.resolve(resB);
				});
			});

			var allS = q.allSettled(promises);
			allS.done(function() {
				donewc();
				done();
			});

		});

	});
};

/**
 * Mock responses to all outgoing requests for one experiment
 *
 * @param lab
 * @param [responseCallback] A callback function, which receives (Http.IncomingMessage req , Http.ServerResponse res)
 * parameters and should respond to the requests. By default returns 503 status code wiht some json in body
 * @param beforeCallback A callback, which is called in after hook with argument of Mitm intance
 * @see https://github.com/moll/node-mitm And see usages of "mitm.on("request", function(req, res) {}"
 * to understand how should you respond with responseCallback
 */
exports.mockResponsesForExperiment = function(lab, responseCallback, beforeCallback) {
	var mitmInstance = null;
	beforeCallback = beforeCallback ? beforeCallback : function() {
	};
	responseCallback = responseCallback ? responseCallback : function(req, res) {
		res.setHeader('content-type', 'application/json');
		res.statusCode = 503;
		res.end('{"error": "Callback is not given, this is random response"}', 'utf8');
	};

	lab.before(function(done) {
		//init without keepalive agent
		mitmInstance = mitm();
		mitmInstance.on('request', responseCallback);
		beforeCallback(mitmInstance);
		done();
	});

	lab.after(function(done) {
		mitmInstance.disable();
		done();
	});
};

var originalEventEmitter = eventUtils.emitter.listeners;
/**
 * Disable event emitter
 */
exports.disableEvents = function() {
	eventUtils.emitter.listeners = function() {
		return [];
	};
};

/**
 * Enable event emitter
 */
exports.enableEvents = function() {
	eventUtils.emitter.listeners = originalEventEmitter;
};

function deleteUser(email, strict, callback) {
	strict = strict ? true : false;

	function doDel(p, account) {
		account.delete(function(err) {
			code.expect(err).to.be.null();
			p.resolve();
		});
	}

	spClient.getApplication(process.env.STORMPATH_URL, function(err, application) {
		code.expect(err).to.be.null();
		application.getAccounts({username: email}, function(err, accounts) {
			code.expect(err).to.be.null();
			if(strict) {
				code.expect(accounts.items.length, JSON.stringify(accounts)).to.equal(1);
			}

			var promises = [];
			for(var ii in accounts.items) {
				var p = q.defer();
				promises.push(p.promise);
				doDel(p, accounts.items[ii]);
			}

			q.all(promises).done(function() {
				callback(null);
			});
		});
	});
}

/**
 * Deletes an account after experiment
 * @param lab
 * @param email
 * @param strict {Boolean} wheter to strictly delete 1 user
 */
exports.deleteAccountAfterExperiment = function(lab, email, strict) {

	lab.after({timeout: 30000}, function(done) {
		deleteUser(email, strict, function() {
			done();
		});
	});
};

/**
 * Delete account after every test in experiment
 * @param lab
 * @param email
 * @param strict {Boolean} wheter to strictly delete 1 user
 */
exports.deleteAccountAfterEveryTestInExperiment = function(lab, email, strict) {

	lab.afterEach({timeout: 30000}, function(done) {
		deleteUser(email, strict, function() {
			done();
		});
	});
};

/**
 * Create an user account for a experiment
 * @param lab
 * @return {Object} Account of the user
 */
exports.createAccountForExperiment = function(lab) {
	var uuid;
	var email = 'test-account-' + (new Date().getTime() + _uuid.v4()) + '@cardola.net';
	var account = {
		email: email,
		givenName: 'Test',
		surname: 'User',
		password: 'Password123'
	};
	lab.before({timeout: 60000}, function(done) {

		exports.getUuid(function(err, uuid1) {
			code.expect(err).to.be.null();
			uuid = uuid1;

			var apiRequest = apiUtils.createApiRequest('/ads/ads/', {}, {}, 'POST', account);

			apiUtils.populateApiRequest(apiRequest, 'v1', exports.getHotelGroupId(), uuid);

			//create new test user
			profileServices.signUp(apiRequest, function(err) {
				code.expect(err, JSON.stringify(err)).to.be.null();
				done();
			});

		});
	});

	//delete after experiment
	exports.deleteAccountAfterExperiment(lab, email, true);

	return account;
};

/**
 * Expect caching headers on a response
 * @param done - test done() callback
 * @param cachingMode
 * @param response The Response body
 * @returns {Object} HapiJS reply() interface
 */
exports.expectCachingHeaders = function(done, cachingMode, response) {
	var cacheHeaders = apiUtils.getCacheConfigurations()[cachingMode];
	code.expect(cacheHeaders).to.be.object();
	var stack = new Error().stack.split('\n')[2];
	var foundHeaders = {};

	var replyInterface = {
		header: function(key, val) {

			if(cacheHeaders.hasOwnProperty(key)) {
				foundHeaders[key] = val;
			}
			var inf = 'Invalid header "' + key + '" value(' + val + ')' + stack;

			for(var hi in cacheHeaders[key]) {
				if(hi === 'flags') {
					code.expect(val, inf).to.include(cacheHeaders[key][hi]);
				} else {
					code.expect(val, inf).to.include(hi + '=' + cacheHeaders[key][hi]);
				}
			}
			return replyInterface;
		},
		etag: function(val) {
			foundHeaders.etag = val;
		},
		source: response
	};

	setTimeout(function() {
		cacheHeaders.etag = 1;
		var info = 'Got headers ' + JSON.stringify(foundHeaders) + ' (required: ' + JSON.stringify(cacheHeaders) + ')' + stack;
		code.expect(Object.keys(foundHeaders).length, info).to.equal(Object.keys(cacheHeaders).length);
		done();
	}, 50);

	return replyInterface;

};
/**
 * Start transaction for an experiment
 * @param lab
 * @param tableName
 */

exports.startTransactionAndCreateDummyTableForExperiment = function(lab, tableName) {

	var doneT;
	var gClient;
	var oDbc;
	lab.before(function(done) {

		databaseUtils.getConnection(function(err, client, doneDb) {
			code.expect(err).to.be.null();
			client.query('BEGIN TRANSACTION', function(err) {
				code.expect(err).to.be.null();
				done();
			});
			doneT = doneDb;
			gClient = client;
			oDbc = databaseUtils.getConnection;
			databaseUtils.getConnection = function() {
				arguments[arguments.length - 1](
					null,
					gClient,
					function() {
					}
				);
			};

		});
	});
	lab.before(function(done) {
		databaseUtils.getConnection(function(err, client) {
			code.expect(err).to.be.null();
			var query = 'CREATE TABLE ' + tableName + ' (ID bigserial PRIMARY KEY NOT NULL, data JSONB);';
			client.query(query, function(err) {
				code.expect(err).to.be.null();
				done();
			});
		});
	});

	lab.after(function(done) {
		gClient.query('ROLLBACK', function(err) {
			code.expect(err).to.be.null();
			doneT();
			databaseUtils.getConnection = oDbc;
			done();
		});
	});
};
/**
 * Build ghost hotels just for testing
 * @param lab
 * @param tableName
 * @param nrOfHtl
 * @param withTrans
 */

exports.buildTestHotelsForVersioning = function(lab, tableName, nrOfHtl, withTrans) {
	var htlIds = [];
	lab.before(function(done) {
		var data = {
			'meta': {
				'createdAt': '2014-11-01T00:00:00+00:00',
				'scheduledAt': '2015-05-27T09:00:00+00:00',
				'appliedAt': null,
				'status': 'published',
				'guid': 'testhotelsforversioning'
			},
			'hotelId': '',
			'hotelGroup': {
				'hotelGroupId': 'test-hgid'
			},
			'coordinates': {
				'lat': 59.436968,
				'long': 24.742877
			},
			'categoryIds': [
				'br-mch',
				'br-shr',
				'reg-eur',
				'reg-eur-est',
				'reg-eur-est-har',
				'ex-cty',
				'ex-rus'
			],
			'localized': [
				{
					'name': 'St Petersbourg',
					'gallery': [
						{
							'source': {
								'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1433919827/dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/01430981250000.jpg',
								'etag': 'e940268ac892bb3af863df7a84e50647',
								'tags': [],
								'type': 'upload',
								'bytes': 123085,
								'width': 1000,
								'format': 'jpg',
								'height': 596,
								'version': 1433919827,
								'public_id': 'dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/01430981250000',
								'signature': '8a94ec87f16477d25e68f3f866e11536cd6c8493',
								'created_at': '2015-06-10T07:03:47Z',
								'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1433919827/dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/01430981250000.jpg',
								'isCloudinary': true,
								'resource_type': 'image',
								'original_filename': 'gallery1'
							}
						},
						{
							'source': {
								'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1433919827/dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/11430981250000.jpg',
								'etag': 'e8ed3dc7a3b2b48a9a2111adc6594567',
								'tags': [],
								'type': 'upload',
								'bytes': 419062,
								'width': 2048,
								'format': 'jpg',
								'height': 1152,
								'version': 1433919827,
								'public_id': 'dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/11430981250000',
								'signature': '6196b06ee88a6b136ae16c28ff78a243a78f8770',
								'created_at': '2015-06-10T07:03:47Z',
								'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1433919827/dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/11430981250000.jpg',
								'isCloudinary': true,
								'resource_type': 'image',
								'original_filename': 'gallery2'
							}
						}
					],
					'contacts': {
						'primary': {
							'email': 'stp@schlossle-hotels.com',
							'phone': '+372 628 6500',
							'website': 'http://www.hotelhotel-blabla-db-error-testsbourg.com/'
						}
					},
					'language': 'en_GB',
					'location': {
						'city': 'Tallinn',
						'country': 'Estonia',
						'fullAddress': 'Rataskaevu 7, 10123 Tallinn, Estonia',
						'shortAddress': 'Old town, Tallinn, Estonia'
					},
					'description': [
						{
							'text': [
								{
									'style': null,
									'value': 'Hotel St. Petersbourg'
								},
								{
									'style': null,
									'value': ' – a unique gem among Tallinn hotels. The history of the hotel building at Rataskaevu 7 dates back to the 14th century – it was mentioned for the first time in 1373 in connection with Burgermeister Peter Stokstrop’s widow. In 1850 the hotel was redesigned by prestigious architect Christian August Gabler, making the St. Petersbourg the oldest operating hotel in Tallinn, Estonia.'
								}
							],
							'heading': [
								{
									'style': null,
									'value': 'History'
								}
							]
						},
						{
							'text': [
								{
									'style': null,
									'value': 'The St. Petersbourg Hotel'
								},
								{
									'style': null,
									'value': ' is a luxurious 4-star hotel in Tallinn’s Old Town. The 27-room boutique hotel offers you the chance to disappear under the arches of the romantic Old Town and enjoy the charms of its historic building.'
								}
							],
							'heading': [
								{
									'style': null,
									'value': 'Services'
								}
							]
						}
					]
				},
				{
					'name': 'St Petersbourg',
					'gallery': [
						{
							'source': {
								'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1433919827/dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/01430981250000.jpg',
								'etag': 'e940268ac892bb3af863df7a84e50647',
								'tags': [],
								'type': 'upload',
								'bytes': 123085,
								'width': 1000,
								'format': 'jpg',
								'height': 596,
								'version': 1433919827,
								'public_id': 'dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/01430981250000',
								'signature': '8a94ec87f16477d25e68f3f866e11536cd6c8493',
								'created_at': '2015-06-10T07:03:47Z',
								'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1433919827/dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/01430981250000.jpg',
								'isCloudinary': true,
								'resource_type': 'image',
								'original_filename': 'gallery1'
							}
						},
						{
							'source': {
								'url': 'http://res.cloudinary.com/cardola-estonia/image/upload/v1433919827/dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/11430981250000.jpg',
								'etag': 'e8ed3dc7a3b2b48a9a2111adc6594567',
								'tags': [],
								'type': 'upload',
								'bytes': 419062,
								'width': 2048,
								'format': 'jpg',
								'height': 1152,
								'version': 1433919827,
								'public_id': 'dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/11430981250000',
								'signature': '6196b06ee88a6b136ae16c28ff78a243a78f8770',
								'created_at': '2015-06-10T07:03:47Z',
								'secure_url': 'https://res.cloudinary.com/cardola-estonia/image/upload/v1433919827/dev_kristjan/group-app/test-hgid/hotels/hotel-blabla-db-error-test/gallery/11430981250000.jpg',
								'isCloudinary': true,
								'resource_type': 'image',
								'original_filename': 'gallery2'
							}
						}
					],
					'contacts': {
						'primary': {
							'email': 'stp@schlossle-hotels.com',
							'phone': '+372 628 6500',
							'website': 'http://www.hotelhotel-blabla-db-error-testsbourg.com/et/'
						}
					},
					'language': 'et_EE',
					'location': {
						'city': 'Tallinn',
						'country': 'Eesti',
						'fullAddress': 'Rataskaevu 7, 10123 Tallinn, Eesti',
						'shortAddress': 'Vanalinn, Tallinn, Eesti'
					},
					'description': [
						{
							'text': [
								{
									'style': null,
									'value': 'St. Petersbourg'
								},
								{
									'style': null,
									'value': ' – unikaalne Tallinna hotellide seas! Aadressil Rataskaevu 7 asuva hoone ajalugu ulatub tagasi 14. sajandisse, kui seda mainiti easiest korda aastal 1373 seoses burgermeister Peter Stokstropi lesega. Aastal 1850 kujundas hoone hotelliks ümber toona mainekas arhitekt Christian August Gabler, ning tegutsedes tänapäevani on St. Petersbourg hotelli näol tegu vanima hotelliga Tallinnas.'
								}
							],
							'heading': [
								{
									'style': null,
									'value': 'Ajalugu'
								}
							]
						},
						{
							'text': [
								{
									'style': null,
									'value': 'Hotell St. Petersbourg'
								},
								{
									'style': null,
									'value': ' on luksuslik neljatärnihotell Tallinna vanalinnas. 27 toga butiikhotell pakub Sulle võimaluse uidata mööda vanalinna romantilisi tänavaid ning nautida ajaloolise hoone võlusid.'
								}
							],
							'heading': [
								{
									'style': null,
									'value': 'Teenused'
								}
							]
						}
					]
				}
			]
		};

		var addDaysToNow = function(days) {
			var dat = new Date();
			dat.setDate(dat.getDate() + days);
			return dat;
		};

		var custId = ['alf', 'bra', 'cha', 'del'];

		databaseUtils.getConnection(function(err, client, doneDb) {
			code.expect(err).to.be.null();
			var buildHotel = function(known) {
				if(known) {
					data.meta.liveAt = addDaysToNow(-1);
					data.meta.dieAt = addDaysToNow(3);
					data.meta.createdAt = addDaysToNow(-2);
					data.hotelId = 'known';
				} else {
					data.meta.liveAt = addDaysToNow(Math.floor(Math.random() * -20 + 10));
					data.meta.dieAt = ((Math.floor(Math.random() * 2 + 1) % 2 === 1) ? addDaysToNow(Math.floor(Math.random() * -20 + 12)) : null);
					data.meta.createdAt = addDaysToNow(Math.floor(Math.random() * -20));
					data.meta.scheduledAt = addDaysToNow(Math.floor(Math.random() * 2));
					data.hotelId = custId[Math.floor(Math.random() * custId.length)];
				}
			};

			var addToDb = function(p) {
				var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', tableName, JSON.stringify(data));
				client.query(query, function(err, res) {
					code.expect(err).to.be.null();
					code.expect(res.rows[0].id).to.be.above(0);
					htlIds.push(res.rows[0].id);
					p.resolve();
				});
			};

			var promises = [];
			for(var i = 0; i < nrOfHtl; i++) {
				var p = q.defer();
				promises.push(p.promise);
				if(i === Math.floor(nrOfHtl / 2)) {
					buildHotel(true);
				} else {
					buildHotel();
				}
				addToDb(p);
			}
			q.all(promises).done(function() {
				doneDb();
				done();
			});
		});
	});
	lab.after(function(done) {
		databaseUtils.getConnection(function(err, client, doneDb) {
			code.expect(err).to.be.null();
			var destroyHotel = function(p, id) {
				var query = escape('DELETE FROM %I WHERE id=%L', tableName, id);
				client.query(query, function(err) {
					if(!withTrans) {
						code.expect(err).to.be.null();
					}
					p.resolve();
				});
			};
			var promises = [];
			for(var i = 0; i < htlIds.length; i++) {
				var p = q.defer();
				promises.push(p.promise);
				destroyHotel(p, htlIds[i]);
			}
			q.all(promises).done(function() {
				doneDb();
				done();
			});

		});

	});

};

exports.buildTestHotelGroupForVersioning = function(lab, tableName, nrOfGroup, withTrans) {
	var groupIds = [];
	lab.before(function(done) {
		var data = {
			'meta': {
				'createdAt': '2014-11-01T00:00:00+00:00',
				'liveAt': '2015-05-27T09:00:00+00:00',
				'dieAt': null,
				'actualLive': null,
				'actualDie': null
			},
			'hotelGroupId': 'test-hgid',
			'viewConfiguration': {
				'hotelMenu': [{
					'action': 'web',
					'removeIfDisabled': true
				}, {
					'action': 'phone',
					'removeIfDisabled': true
				}, {
					'action': 'map',
					'removeIfDisabled': true
				}, {
					'action': 'share',
					'removeIfDisabled': true
				}, {
					'action': 'book',
					'removeIfDisabled': true
				}],
				'profileMenu': [{
					'action': 'settings',
					'removeIfDisabled': true
				}, {
					'action': 'languages',
					'removeIfDisabled': true
				}, {
					'action': 'signOut',
					'removeIfDisabled': true
				}, {
					'action': 'conditions',
					'removeIfDisabled': true
				}, {
					'action': 'signIn',
					'removeIfDisabled': true
				}, {
					'action': 'signUp',
					'removeIfDisabled': true
				}]
			}
		};

		var addDaysToNow = function(days) {
			var dat = new Date();
			dat.setDate(dat.getDate() + days);
			return dat;
		};

		var custId = ['alf', 'bra', 'cha', 'del'];

		databaseUtils.getConnection(function(err, client, doneDb) {
			code.expect(err).to.be.null();
			var buildGroup = function(known) {
				if(known) {
					data.meta.liveAt = addDaysToNow(-1);
					data.meta.dieAt = addDaysToNow(3);
					data.meta.createdAt = addDaysToNow(-2);
					data.hotelGroupId = 'known';
				} else {
					data.meta.liveAt = addDaysToNow(Math.floor(Math.random() * -20 + 10));
					data.meta.dieAt = ((Math.floor(Math.random() * 2 + 1) % 2 === 1) ? addDaysToNow(Math.floor(Math.random() * -20 + 12)) : null);
					data.meta.createdAt = addDaysToNow(Math.floor(Math.random() * -20));
					data.hotelGroupId = custId[Math.floor(Math.random() * custId.length)];
				}
			};

			var addToDb = function(p) {
				var query = escape('INSERT INTO %I (data) VALUES(%L) RETURNING id', tableName, JSON.stringify(data));
				client.query(query, function(err, res) {
					code.expect(err).to.be.null();
					code.expect(res.rows[0].id).to.be.above(0);
					groupIds.push(res.rows[0].id);
					p.resolve();
				});
			};

			var promises = [];
			for(var i = 0; i < nrOfGroup; i++) {
				var p = q.defer();
				promises.push(p.promise);
				if(i === Math.floor(nrOfGroup / 2)) {
					buildGroup(true);
				} else {
					buildGroup();
				}
				addToDb(p);
			}
			q.all(promises).done(function() {
				done();
				doneDb();
			});
		});
	});
	lab.after(function(done) {
		databaseUtils.getConnection(function(err, client, doneDb) {
			code.expect(err).to.be.null();
			var destroyHotel = function(p, id) {
				var query = escape('DELETE FROM %I WHERE id=%L', tableName, id);
				client.query(query, function(err) {
					if(!withTrans) {
						code.expect(err).to.be.null();
					}
					p.resolve();
				});
			};
			var promises = [];
			for(var i = 0; i < groupIds.length; i++) {
				var p = q.defer();
				promises.push(p.promise);
				destroyHotel(p, groupIds[i]);
			}
			q.all(promises).done(function() {
				doneDb();
				done();
			});

		});

	});

};

/**
 * Clear AWS S3 folder
 * @param folder
 * @param callback
 * @param [bucket]
 */
exports.clearS3Folder = function(folder, callback, bucket) {

	var s3 = new aws.S3({params: {Bucket: bucket || process.env.AWS_S3_BUCKET}});

	//get list of already downloaded files from AWS S3
	s3.listObjects({
		Prefix: path.join(process.env.AWS_S3_FOLDER, folder)
	}, function(err, data) {
		code.expect(err).to.be.null();
		var promises = [];
		data.Contents.forEach(function(item) {
			var p = q.defer();
			promises.push(p.promise);
			s3.deleteObject({Key: item.Key}, function(err, data) {
				code.expect(err).to.be.null();
				code.expect(data).not.to.be.null();
				p.resolve();
			});
		});

		q.all(promises).done(function() {
			callback(null);
		});

	});
};

/**
 * Cleared AWS S3 folder after every test in experiment
 * @param lab
 * @param folder
 * @param [bucket]
 */
exports.clearS3FolderAfterEveryTest = function(lab, folder, bucket) {
	lab.afterEach({timeout: 100000}, function(done) {
		exports.clearS3Folder(folder, done, bucket);
	});
};

/**
 * Cleared AWS S3 folder after the experiment
 * @param lab
 * @param folder
 * @param [bucket]
 */
exports.clearS3FolderAfterExperiment = function(lab, folder, bucket) {
	lab.after({timeout: 100000}, function(done) {
		exports.clearS3Folder(folder, done, bucket);
	});
};

/**
 * Create test ftp server for an experiment
 * @param lab
 * @param subFolder {String} path to subfolder`
 */
exports.createFtpServerForExperiment = function(lab, subFolder) {
	var ftpd = require('ftpd');
	var server,
		options = {
			host: '127.0.0.1',
			port: 7002,
			user: 'test',
			password: 'test',
			tls: null
		};

	lab.before({timeout: 5000}, function(done) {
		server = new ftpd.FtpServer(options.host, {
			getInitialCwd: function() {
				return '/';
			},
			getRoot: function() {
				var dir = __dirname;
				if(subFolder){
					dir += '/' + subFolder;
				}
				return dir;
			},
			pasvPortRangeStart: 1025,
			pasvPortRangeEnd: 1050,
			tlsOptions: options.tls,
			allowUnauthorizedTls: true,
			useWriteFile: false,
			useReadFile: false,
			uploadMaxSlurpSize: 7000 // N/A unless 'useWriteFile' is true.
		});

		server.on('client:connected', function(connection) {
			var username = null;
			console.log('client connected: ' + connection.remoteAddress);
			connection.on('command:user', function(user, success, failure) {
				if(user === options.user) {
					username = user;
					success();
				} else {
					failure();
				}
			});

			connection.on('command:pass', function(pass, success, failure) {
				if(pass === options.password) {
					success(username);
				} else {
					failure();
				}
			});
		});

		server.debugging = 4;
		server.listen(options.port);
		console.log('Listening on port ' + options.port);
		done();
	});

	lab.after(function(done) {
		server.close();
		done();
	});

	return options;

};
