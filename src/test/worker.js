'use strict';
var testingTools = require('./testingTools');
exports.labLib = require('lab');
var lab = exports.lab = exports.labLib.script();
var iron_mq = require('iron_mq');
var imq = new iron_mq.Client();
var logging = require('../modules/utils/logging');
var queueIncoming = imq.queue('scheduled_jobs');
var queueCompleted = imq.queue('completed_jobs');
var scheduler = require('../modules/worker/scheduler');
var jobHandler = require('../modules/worker/jobHandler');
var jobExecuter = require('../modules/worker/jobExecuter');

var pe = process.exit;
process.argv[4] = null;
process.exit = function(val){
	process.exit = pe;
	return val;
};

var worker = require('../worker');
lab.experiment('The executeJob', {timeout: 30000}, function() {
	var called, li, info;

	lab.before(function(done){
		called = 0;
		li = logging.info;
		done();
	});

	lab.afterEach(function(done){
		logging.info = li;
		done();
	});
	testingTools.mockResponsesForExperiment(lab, undefined, function(mitm){
		mitm.on('connect', function(socket, opt){
			if(called++ !== 3){
				socket.bypass();
			}

		});
		mitm.on('connection', function(socket){
			socket.destroy();
		});
	});
	lab.test('should work return err log msg', function(done) {
		logging.info = function(){
			info = JSON.parse(arguments[2]);
			console.log('info', info);
		};
		jobHandler.createJob('Some job', 'test-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			//schedule job, so it gets to queue
			scheduler.scheduleJob(job, {}, function(err, messageId) {
				testingTools.code.expect(err).to.be.null();
				queueIncoming.msg_get(messageId, function (err, message) {
					testingTools.code.expect(err).to.be.null();

					//execute job
					worker.executeJob(job, message, function (err) {
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(info[2]).to.be.equal(messageId);
						function checker(messageId) {
							queueCompleted.get({timeout: 30}, function (err, message) {
								testingTools.code.expect(err).to.be.null();
								try {
									var completedJob = JSON.parse(message.body);
									//skip other messages
									testingTools.code.expect(completedJob.messageId).to.equal(messageId);
									//correct message, delete from queue and
									queueCompleted.del(message.id, function (err) {
										testingTools.code.expect(err).to.be.null();
										done();
									});
								} catch (e) {
									checker(messageId);
								}
							});
						}

						checker(messageId);
					});
				});
			});
		});

	});
});
lab.experiment('The executeJob', {timeout: 30000}, function() {
	var je, li, si, info;

	lab.before(function(done){
		je = jobExecuter.executeJob;
		li = logging.info;
		si = global.setInterval;
		done();
	});

	lab.afterEach(function(done){
		jobExecuter.executeJob = je;
		logging.info = li;
		global.setInterval = si;
		done();
	});

	lab.test('should clear interval and return error', function(done) {
		jobExecuter.executeJob = function(){
			return arguments[arguments.length-1]({
				data: {
					code: 'je-inv-job-cd'
				}
			});
		};
		global.setInterval = function(){
			return arguments[0]();
		};
		jobHandler.createJob('Some job', 'test-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			//schedule job, so it gets to queue
			scheduler.scheduleJob(job, {}, function(err, messageId) {
				testingTools.code.expect(err).to.be.null();
				queueIncoming.msg_get(messageId, function (err, message) {
					testingTools.code.expect(err).to.be.null();
					//execute job
					worker.executeJob(job, message, function (err) {
						testingTools.code.expect(err).to.be.deep.equal({ data: { code: 'je-inv-job-cd' } });
						done();
					});
				});
			});
		});
	});

	lab.test('should work', function(done) {
		logging.info = function(){
			info = arguments[2];
		};
		jobHandler.createJob('Some job', 'test-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			//schedule job, so it gets to queue
			scheduler.scheduleJob(job, {}, function(err, messageId) {
				testingTools.code.expect(err).to.be.null();
				queueIncoming.msg_get(messageId, function (err, message) {
					testingTools.code.expect(err).to.be.null();

					//execute job
					worker.executeJob(job, message, function (err) {
						testingTools.code.expect(err).to.be.null();
						testingTools.code.expect(info).to.be.equal(messageId);
						function checker(messageId) {
							queueCompleted.get({timeout: 30}, function (err, message) {
								testingTools.code.expect(err).to.be.null();
								try {
									var completedJob = JSON.parse(message.body);
									//skip other messages
									testingTools.code.expect(completedJob.messageId).to.equal(messageId);
									//correct message, delete from queue and
									queueCompleted.del(message.id, function (err) {
										testingTools.code.expect(err).to.be.null();
										done();
									});
								} catch (e) {
									checker(messageId);
								}
							});
						}

						checker(messageId);
					});
				});
			});
		});

	});

});

lab.experiment('The init', {timeout: 30000}, function() {
	var p2, p3, p4, ej, li, info;
	lab.before(function(done){
		p2 = process.argv[2];
		p3 = process.argv[3];
		p4 = process.argv[4];
		ej = worker.executeJob;
		li = logging.info;
		done();
	});

	lab.afterEach(function(done){
		process.argv[2] = p2;
		process.argv[3] = p3;
		process.argv[4] = p4;
		logging.info = li;
		worker.executeJob = ej;
		done();
	});

	lab.test('should stopped if job or messageId not given', function(done) {

		logging.info = function(){
			info = JSON.parse(arguments[2]);
		};
		delete process.argv[2];
		delete process.argv[3];
		process.argv[2] = null;
		process.argv[3] = null;
		worker.init(function(val){
			testingTools.code.expect(val).to.be.equal(1);
			testingTools.code.expect(info).to.be.object();
			testingTools.code.expect(info[0]).to.be.equal('No job or messageId given!');
			done();
		});

	});

	lab.test('should stopped if job incorrect', function(done) {
		logging.info = function(){
			info = JSON.parse(arguments[2]);
		};
		delete process.argv[2];
		process.argv[2] = 'test-value';
		process.argv[3] = 'test-value';
		worker.init(function(val){
			testingTools.code.expect(val).to.be.equal(1);
			testingTools.code.expect(info).to.be.object();
			testingTools.code.expect(info[0]).to.be.equal('Error parsing job JSON');
			done();
		});

	});

	lab.test('should stopped if messageId is empty string', function(done) {
		logging.info = function(){
			info = JSON.parse(arguments[2]);
		};
		delete process.argv[2];
		delete process.argv[3];
		jobHandler.createJob('Some job', 'test-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			process.argv[2] = JSON.stringify(job);
			process.argv[3] = 3;
			worker.init(function (val) {
				testingTools.code.expect(val).to.be.equal(1);
				testingTools.code.expect(info).to.be.object();
				testingTools.code.expect(info[0]).to.be.equal('Message ID must be a string with some value');
				done();
			});
		});

	});

	lab.test('should stopped if job has wrong description', function(done) {
		logging.info = function(){
			info = JSON.parse(arguments[2]);
		};
		delete process.argv[2];
		process.argv[2] = JSON.stringify({});
		process.argv[3] = 'test-value';
		worker.init(function(val){
			testingTools.code.expect(val).to.be.equal(1);
			testingTools.code.expect(info).to.be.object();
			testingTools.code.expect(info[0]).to.be.equal('Invalid job description');
			done();
		});
	});

	lab.test('should stopped if messageId incorrect', function(done) {
		logging.info = function(){
			info = JSON.parse(arguments[2]);
		};
		delete process.argv[2];
		delete process.argv[3];
		jobHandler.createJob('Some job', 'test-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			scheduler.scheduleJob(job, {}, function(err) {
				testingTools.code.expect(err).to.be.null();
				process.argv[2] = JSON.stringify(job);
				process.argv[3] = 'some-value';
				worker.init(function (val) {
					testingTools.code.expect(val).to.be.equal(1);
					testingTools.code.expect(info).to.be.object();
					testingTools.code.expect(info[0]).to.be.equal('Cannot find the job in the queue');
					done();
				});
			});
		});

	});

	lab.test('should failed if can not execute job', function(done) {
		logging.info = function(){
			info = JSON.parse(arguments[2]);
		};
		delete process.argv[2];
		worker.executeJob = function(){
			return arguments[arguments.length-1](new Error('Test Error executeJob'));
		};
		jobHandler.createJob('Some job', 'test-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			//schedule job, so it gets to queue
			scheduler.scheduleJob(job, {}, function(err, messageId) {
				testingTools.code.expect(err).to.be.null();
				process.argv[2] = JSON.stringify(job);
				process.argv[3] = messageId;
				worker.init(function(val){
					testingTools.code.expect(val).to.be.equal(1);
					testingTools.code.expect(info).to.be.object();
					testingTools.code.expect(info[0]).to.be.equal('Error executing the task');
					done();
				});
			});
		});

	});

	lab.test('should work', function(done) {
		logging.info = function(){
			info = JSON.parse(arguments[2]);
		};
		delete process.argv[2];
		jobHandler.createJob('Some job', 'test-job', {}, 0, function(err, job) {
			testingTools.code.expect(err).to.be.null();
			//schedule job, so it gets to queue
			scheduler.scheduleJob(job, {}, function(err, messageId) {
				testingTools.code.expect(err).to.be.null();
				//execute job
				process.argv[2] = JSON.stringify(job);
				process.argv[3] = messageId;
				worker.init(function(val){
					testingTools.code.expect(val).to.be.equal(0);
					testingTools.code.expect(info).to.be.object();
					testingTools.code.expect(info[0]).to.be.equal('Job done!');
					function checker(messageId) {
						queueCompleted.get({timeout: 30}, function (err, message) {
							testingTools.code.expect(err).to.be.null();
							var completedJob = JSON.parse(message.body);
							//skip other messages
							testingTools.code.expect(completedJob.messageId).to.equal(messageId);
							//correct message, delete from queue and
							queueCompleted.del(message.id, function (err) {
								testingTools.code.expect(err).to.be.null();

								done();
							});
						});
					}
					checker(messageId);
				});

			});
		});

	});
});
