'use strict';
var jobHandler = require('./modules/worker/jobHandler');
var jobExecuter = require('./modules/worker/jobExecuter');
var logging = require('./modules/utils/logging');
var check = require('check-types');
var q = require('q');

var iron_mq = require('iron_mq');
var imq = new iron_mq.Client();
var queueIncoming = imq.queue('scheduled_jobs');
var queueCompleted = imq.queue('completed_jobs');
var test = false;
var job, messageId;

require('./listeners');


/**
 * Local logging function
 */
function log(){
	logging.info('#WORKER#', JSON.stringify(job), JSON.stringify(arguments));
}

/**
 * Execute a job
 * @param job
 * @param message
 * @param callback
 */
function executeJob(job, message, callback){
	//keep the message reserved as long as we are processing it
	log('Executing job');
	var touchTimer = function(){
		queueIncoming.msg_touch(message.id, function(err){
			logging.info('#WORKER#', 'Message touched', message.id, err);
		});
	};
	//start the toucher, aka keep-alive
	var touchInterval = setInterval(function(){
		touchTimer();
	}, (message.timeout-1) * 1000);

	//touch it right now to ensure it is reserved for us
	queueIncoming.msg_touch(message.id, function(err){
		logging.info('#WORKER#', 'Message touched', message.id, err);
	});
	
	//Call the correct endpoint and return with results
	function resultsCallback(err, res){
		log('Job done', err);
		//job was not found, let it fall back to queue
		if(err && err.data.code === 'je-inv-job-cd'){
			clearInterval(touchInterval);
			return callback(err, res);
		}

		var p1 = q.defer();
		var p2 = q.defer();

		var completeMessage = {
			body: {
				job: job,
				messageId: message.id,
				error: err,
				response: res
			}
		};

		completeMessage.body = JSON.stringify(completeMessage.body);
		//add results message to correct queue

		queueCompleted.post(completeMessage, function(errCmpl){

			if(errCmpl){
				console.log('errCmpl', errCmpl);
				log('Error adding a message to completed jobs queue', errCmpl.message, completeMessage);
			}
			p1.resolve();
			//delete the incoming message
			queueIncoming.del(message.id, function(errDel){
				clearInterval(touchInterval);
				if(errDel){
					log('Error deleting message from scheduled jobs queue', errDel.message);
				}
				p2.resolve();
			});
		});

		q.all([p1.promise, p2.promise]).done(function(){
			callback(err, res);
		});
	}

	if(test){
		resultsCallback(null, {testMode: true, wasJobExecuted: false});
	}else{
		jobExecuter.executeJob(job, function(){

			resultsCallback.apply(this, arguments);
		});
	}

}

function init(callback) {
	job = null;
	messageId = null;
	if (process.argv[2]) {
		job = process.argv[2];
	}


	if (process.argv[3]) {
		messageId = process.argv[3];
	}

	if (process.argv[4]) {
		test = process.argv[4] ? true : false;
	}
	if (job && messageId) {
		if (test) {
			log('TEST RUN...');
		}

		try {
			job = JSON.parse(job);
		} catch (e) {
			log('Error parsing job JSON', e, 'json:', job);
			return callback(1);
		}

		if (!check.unemptyString(messageId)) {
			log('Message ID must be a string with some value');
			return callback(1);
		}
		jobHandler.validateJob(job, function (err) {
			if (err) {
				log('Invalid job description');
				return callback(1);
			}
			queueIncoming.msg_get(messageId, function (err, message) {

				if (err) {
					log('Cannot find the job in the queue', err, messageId);
					return callback(1);
				}
				exports.executeJob(job, message, function (err, res) {
					if (err) {
						log('Error executing the task', err);
						return callback(1);
					}
					log('Job done!', require('util').inspect(res, {colors: true, depth: Infinity}));
					return callback(0);
				});

			});

		});

	} else {
		log('No job or messageId given!');
		return callback(1);
	}
}

exports.init = init;
exports.executeJob = executeJob;
init(function(value){
	return process.exit(value);
});
